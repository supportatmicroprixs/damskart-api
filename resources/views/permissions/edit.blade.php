@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Add Permission</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Permissions
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Transfer Fund</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT', 'class' => 'form-vertical')) }}
                           <!-- <form action="{{ route('permissions.update', $permission->id) }}" method="PUT" class="form form-vertical"> -->
                              <!-- {{csrf_field()}} -->
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Permission Name</label>
                                          <div class="position-relative has-icon-left">
                                              <input type="text" class="form-control" id="permissions" name="name" onkeyup="messageDetected()" value="{{$permission->name}}" >
                                              <div class="form-control-position">
                                                  <i class="feather icon-user"></i>
                                              </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" id="sendmessage" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           <!-- </form> -->
                           {{ Form::close() }}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   function messageDetected(){
       var role = $("#permissions").val();
         if (role.length > 0) {
           $('#sendmessage').removeAttr('disabled');
         } else {
           $('#sendmessage').attr('disabled', 'disabled');
         }
   }
</script>
@endsection