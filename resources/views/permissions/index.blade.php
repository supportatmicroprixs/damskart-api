@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Permission's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Permissions
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->

            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <strong>{{ $message }}</strong>
         </div>
         @endif 
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Column selectors with Export and Print Options</h4> -->
                        @can('Role.Create')
                        <a href="/permissions/create" class="btn btn-primary">Add Permission</a>
                        @endcan

                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th >Permissions</th>
                                       <th >Operation</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($permissions as $permission)
                                       <tr>
                                          <td>{{ $permission->name }}</td> 
                                          <td>
                                          @if(Auth::User()->role  == "super admin")
                                          <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>
                                          {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                          {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                          {!! Form::close() !!}
                                          @endif

                                          </td>
                                       </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   function editMember(status, id, cid, sid) {
     if(id) {
         $.ajax({
             url: '/changeuser',
             type: 'get',
             data: {id : id,status : status},
             dataType: 'json',
             success:function(response) {
               console.log(response.success)
                
               if (response.status == 1) {
                  $(cid).hide();
                  $(sid).show();
               }else{
                  $(cid).hide();
                  $(sid).show();
               }
   
             }
         }); 
   
     } else {
         alert("Error : Refresh the page again");
     }
   }
</script>
@endsection