<!--Modal body-->
<input type="hidden" name="status_id" id="status_id" value="{{$id}}">
<div class="modal-body text-danger">
  {{$message}}
</div>
<!--Modal footer-->
<div class="modal-footer">
  <button data-dismiss="modal" class="btn btn-default btn-xs btn-corner" type="button">Cancel</button>
  <button data-dismiss="modal" type="button" id="OK" class="btn btn-primary btn-xs btn-corner" >{{$button_name}}</button>
</div>