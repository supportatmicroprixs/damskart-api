   @extends('admin.layouts.main')
@section('css')
<style type="text/css">
    .divWaiting {
            position: fixed;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }
        @media print{
         body *{
            visibility: hidden;
         }
         .print-container, .print-container *{
            visibility: visible;
         }
         .print-container, #cl_button *{
            visibility: hidden;
         }
        }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
 <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Transfer</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Service
                                    </li>
                                    <li class="breadcrumb-item active">Payout Transfer
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <p id="resp_message"></p>
                    </div>
                </div>
                <!-- Column selectors with Export Options and print table -->
                @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
                 <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title" id="errormessage"></h5>
                                    <!-- <h4 class="card-title">Transfer Fund</h4> -->
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form form-vertical" action="/pay/{{$customer->id}}" method="POST" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="contact-info-icon">Benificiary Name</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="benificiary_name" id="benificiary_name" value="{{$customer->benificiary_name}}" readonly="">
                                                                <div class="form-control-position">
                                                                    <i class="feather icon-user"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="contact-info-icon">Account Number</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="account" id="account" value="{{$customer->account}}" readonly="">
                                                                <div class="form-control-position">
                                                                    <i class="feather icon-credit-card"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="contact-info-icon">IFSC Code</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="ifsc_code" id="ifsc_code" value="{{$customer->ifsc_code}}" readonly="">
                                                                <div class="form-control-position">
                                                                    <i class="feather icon-edit-2"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="contact-info-icon">Amount</label>
                                                            <div class="position-relative has-icon-left">
                                                                <input type="text" class="form-control"  value="" id="amount" name="amount" placeholder="0">
                                                                <div class="form-control-position">
                                                                    <i class="fa fa-inr"></i>
                                                                </div>
                                                            </div>
                                                            <small>Available Balance:&nbsp;<b>&#x20B9;{{Auth::user()->ewallet}}</b></small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                       <div class="form-group d-flex align-items-center pt-md-2">
                                                          <label class="mr-2">Payment Mode :</label>
                                                          <div class="c-inputs-stacked">
                                                             <div class="d-inline-block mr-2">
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                   <input type="radio" name="payment_mode" value="IMPS">
                                                                   <span class="vs-checkbox">
                                                                   <span class="vs-checkbox--check">
                                                                   <i class="vs-icon feather icon-check"></i>
                                                                   </span>
                                                                   </span>
                                                                   <span class="">IMPS</span>
                                                                </div>
                                                             </div>
                                                             <div class="d-inline-block mr-2" >
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                   <input type="radio" name="payment_mode" value="NEFT">
                                                                   <span class="vs-checkbox">
                                                                   <span class="vs-checkbox--check">
                                                                   <i class="vs-icon feather icon-check"></i>
                                                                   </span>
                                                                   </span>
                                                                   <span class="">NEFT</span>
                                                                </div>
                                                             </div>
                                                             <div class="d-inline-block mr-2" >
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                   <input type="radio" name="payment_mode" value="RTGS">
                                                                   <span class="vs-checkbox">
                                                                   <span class="vs-checkbox--check">
                                                                   <i class="vs-icon feather icon-check"></i>
                                                                   </span>
                                                                   </span>
                                                                   <span class="">RTGS</span>
                                                                </div>
                                                             </div> 
                                                          </div>
                                                       </div>
                                                    </div>
                                                    <input type="hidden" name="id" id="id" value="{{$customer->id}}">
                                                    <div class="col-12">
                                                        <button type="button" onclick="payouttransferButton()" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- getreceiptModal -->
                            <div class="modal fade" id="getreceiptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                                   <div class="modal-content">
                                      <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                         <h4 class="modal-title" style=" color: white;" id="Resp_transfer_type"></h4>
                                      </div> 
                                      <div class="modal-body  print-container" id="getCode" >
                                         <div class="row">
                                            <div class="col-md-12">
                                               <div class="content" style="text-align: center; margin-left: 0px;">
                                                      <div id="invoice-template" class="card-body">
                                                          <div id="invoice-company-details" class="row">
                                                              <div class="col-sm-6 col-12 text-left pt-1">
                                                                  <div class="media pt-1">
                                                                      <img src="{{asset('images/payout_icon (1).png')}}" style="width: 120px;">
                                                                  </div>
                                                              </div>
                                                              <div class="col-sm-6 col-12 text-right">
                                                                  <h1>Payment Details</h1>
                                                                  <div class="invoice-details mt-2">
                                                                      <h6>UTR No.</h6>
                                                                      <p id="Resp_utrnumber"></p>
                                                                      <h6>Txn Date & Time</h6>
                                                                      <p><h5 id="Resp_txn_dateTime"></h5></p>
                                                                      <h6>Transfer Mode</h6>
                                                                      <p id="Resp_transfer_mode"></p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div id="invoice-customer-details" class="row pt-2 ">
                                                              <div class="col-sm-6 col-12 text-left">
                                                                  <div class="recipient-info my-2">
                                                                      <h6>Txn Status</h6>
                                                                      <p id="Resp_txn_status"></p>
                                                                      <h6>Sender Number</h6>
                                                                      <p id="Resp_sender_mobile"></p>
                                                                  </div>
                                                              </div>
                                                              <div class="col-sm-6 col-12 text-right">
                                                                  <div class="company-info my-2">
                                                                      <h6>Account No.</h6>
                                                                      <p id="Resp_account"></p>
                                                                      <h6>Bank Name</h6>
                                                                      <p id="Resp_bank_name"></p>
                                                                      <h6>IFSC Code</h6>
                                                                      <p id="Resp_ifsc_code"></p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                              <div class="row">
                                                                  <div class="table-responsive col-12">
                                                                      <table class="table table-borderless">
                                                                          <tbody>
                                                                              <tr>
                                                                                  <td class="text-left"><b>Transaction Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                                  <td class="text-right"><h5 id="Resp_txn_amount"></h5></td>
                                                                              </tr>
                                                                              <tr>
                                                                                  <td class="text-left"><b>Surcharge (<i class="fa fa-inr"></i>)</b></td>
                                                                                  <td class="text-right" id="Resp_surcharge"></td>
                                                                              </tr>
                                                                              <tr>
                                                                                  <td class="text-left"><b>Wallet Balance (<i class="fa fa-inr"></i>)</b></td>
                                                                                  <td class="text-right" id="Resp_wallet_bal"></td>
                                                                              </tr>
                                                                             
                                                                          </tbody>
                                                                      </table>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                      <p id="countdown" class="text-center"></p>
                                      <div class="modal-footer" >
                                         <!-- <h5>Thanks for your Business!</h5> -->
                                         <button type="button" class="btn btn-secondary" onClick="window.location.reload();"  data-dismiss="modal">Close</button>
                                         <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                                      </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="divWaiting" id="Dv_Loader" style="display: none;">
                  <button class="btn btn-primary mb-1 round" type="button" >
                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
                  &nbsp;Loading... 
                  </button>
                </div>
                <!-- Column selectors with Export Options and print table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
</script>
<script type="text/javascript">
function payouttransferButton() {
    var SITEURL = '{{URL::to('')}}';
     var benificiary_name = $('#benificiary_name').val();
     var account = $('#account').val();
     var ifsc_code = $('#ifsc_code').val();
     var amount = $('#amount').val();
     var id = $('#id').val();
     var payment_mode = $("input[type='radio'][name='payment_mode']:checked").val();
      
    Swal.fire({
        title: 'Do you want to continue?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Continue',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
       }).then((data) => {
        if (data.value) {
          $('#Dv_Loader').show();
            $.ajax({
              type:"POST",
              url:"/pay", 
              data : {'benificiary_name':benificiary_name,'account':account,'ifsc_code':ifsc_code,'amount':amount,'payment_mode':payment_mode,'id':id,},
              dataType: "json",
              success:function(data)
              {   
                $('#Dv_Loader').hide();
                if (data.permission_code == "PER111") {
                  $("#resp_message").empty();
                  $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                    
                    setTimeout(function () {
                      window.location.href = SITEURL + '/index';
                    }, 3000);
                 }else{
                    if (data.code == "000" ) {
                        // alert(data.code);
                        $("#errormessage").empty();
                        var ResMessage = data.message;
                        var ResPaymode = data.pay_mode;
                        var Result = data.payouttransfer_response;
                      // $('#bbps_logo').hide();
                      // $("#PRESuccessinputparamdisplay").empty();
                      var transfer_type = Result.transfer_type;
                      var utrnumber = Result.utrnumber;
                      var txn_dateTime = moment(Result.created_at).format('DD-MM-YYYY hh:mm:ss A');
                      var transfer_mode = ResPaymode;
                      var txn_status = ResMessage;
                      var sender_mobile = Result.sender_mobile;
                      var account = Result.account;
                      var bank_name = Result.bank_name;
                      var ifsc_code = Result.ifsc_code;
                      var txn_amount = Result.amount;
                      var surcharge = Result.surcharge;
                      var wallet_bal = Result.current_balance;
                      
                      $('#Resp_transfer_type').html(transfer_type);
                      $('#Resp_utrnumber').html(utrnumber);
                      $('#Resp_txn_dateTime').html(txn_dateTime);
                      $('#Resp_transfer_mode').html(transfer_mode);
                      $('#Resp_txn_status').html(txn_status);
                      $('#Resp_sender_mobile').html(sender_mobile);
                      $('#Resp_account').html(account);
                      $('#Resp_bank_name').html(bank_name);
                      $('#Resp_ifsc_code').html(ifsc_code);
                      $('#Resp_txn_amount').html(txn_amount);
                      $('#Resp_surcharge').html(surcharge);
                      $('#Resp_wallet_bal').html(wallet_bal);

                      $('#getreceiptModal').modal({backdrop: 'static', keyboard: false});

                      // var timeleft = 10;
                      // var downloadTimer = setInterval(function(){
                      //   if(timeleft <= 0){
                      //     clearInterval(downloadTimer);
                      //     // document.getElementById("countdown").innerHTML = "Finished";
                      //     $("#countdown").empty();
                      //     $("#countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');

                      //   } else {
                      //     // document.getElementById("countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";
                      //     $("#countdown").empty();
                      //     $("#countdown").append('<p>This Page is reload after '+timeleft+' seconds.</p><span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                      //   }
                      //   timeleft -= 1;
                      // }, 1000);
                      // setTimeout(function () {
                      //    window.location.href = SITEURL + '/payout';
                      // }, 10000);
                    }else{
                      var error = data.message;
                        $("#errormessage").empty();
                        $('#errormessage').html(error).css('color', 'red');
                    }
                  }
              }
           });
        } else if (data.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
              title: 'Cancelled',
              type: 'error',
              confirmButtonClass: 'btn btn-success',
            })
        }
    })
}
</script>
@endsection