 <!-- BEGIN: Header--> 
@inject('User','App\User')
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow" style="border-bottom: 3px solid #FF7600;">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            @if(Auth::user()->role == 'super admin')
                                <li class="nav-item d-none d-lg-block mr-2 "><img src="{{asset('images/aeps.png')}}"  class="img-radius" width="60" alt="aeps-walllet"> &#x20B9;
                                {{number_format($aeps_wallet,3)}} 
                                </li>
                            @else
                                @php
                                    $icici_aeps_count = \DB::table('user_services')->leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->where('service_id', 1)->where('user_id', Auth::user()->id)->where('user_services.status', 1)->where('service_packages.status', 1)->count();
                                    $icici_aeps_count1 = \DB::table('user_services')->leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->where('service_id', 2)->where('user_id', Auth::user()->id)->where('user_services.status', 1)->where('service_packages.status', 1)->count();

                                    if($icici_aeps_count > 0 || $icici_aeps_count1 > 0):
                                @endphp
                                    <li class="nav-item d-none d-lg-block mr-2 "><img src="{{asset('images/aeps.png')}}"  class="img-radius" width="60" alt="aeps-walllet"> &#x20B9;
                                    {{number_format(Auth::user()->aeps_wallet,3)}}
                                @php
                                    endif;
                                @endphp
                            @endif


                            <li class="nav-item d-none d-lg-block mr-2"><img src="{{asset('images/wallet1600.png')}}"  class="img-radius" width="30" alt="e-walllet"> &#x20B9; @if(Auth::user()->role == 'super admin') {{number_format($ewallet,3)}} @else {{number_format(Auth::user()->ewallet,3)}} @endif</li>
                            
                            @if(Auth::user()->role == 'super admin')
                              <?php  
                                  $apiname = 'BalanceInquiry';
                                $guid = 'GUID'.date('YmdHis');
                                $params = [
                                    "CORPID"    =>  "574327555",
                                    "USERID"    =>  "VEERENDR",
                                    "AGGRID"    =>  "OTOE0077",
                                    "URN"       =>  "SR194499369",
                                    "ACCOUNTNO" => "678405600727",
                                ];
                                $source = json_encode($params);
                                $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                                $pub_key_string=fread($fp,8192);
                                fclose($fp);
                                openssl_get_publickey($pub_key_string);
                                openssl_public_encrypt($source,$crypttext,$pub_key_string);
                                $request = json_encode(base64_encode($crypttext));
                                $header = [
                                    'apikey:1032da18afb74ccf8247f971aaae24ac',
                                    'Content-type:text/plain'
                                ];
                                $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/BalanceInquiry';
                                $file = public_path().'/logs/'.$apiname.'.txt';
                                $log = "\n\n".'GUID - '.$guid."================================================================\n";
                                $log .= 'URL - '.$httpUrl."\n\n";
                                $log .= 'HEADER - '.json_encode($header)."\n\n";
                                $log .= 'REQUEST - '.json_encode($params)."\n\n";
                                $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                                file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                                $curl = curl_init();
                                curl_setopt_array($curl, array(
                                    CURLOPT_PORT => "443",
                                    CURLOPT_URL => $httpUrl,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 60,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_POSTFIELDS => $request,
                                    CURLOPT_HTTPHEADER => $header
                                ));
                                $response = curl_exec($curl);
                                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                                $err = curl_error($curl);
                                curl_close($curl);
                                $fp= fopen(public_path()."/keys/damskart.final.key","r");
                                $priv_key=fread($fp,8192);
                                fclose($fp);
                                $res = openssl_get_privatekey($priv_key, "");
                                openssl_private_decrypt(base64_decode($response), $newsource, $res);
                                $log = "\n\n".'GUID - '.$guid."================================================================ \n";
                                $log .= 'URL - '.$httpUrl."\n\n";
                                $log .= 'RESPONSE - '.json_encode($response)."\n\n";
                                $log .= 'REQUEST DECRYPTED - '.$newsource."\n\n";
                                file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                                $data = json_decode($newsource, TRUE);
                                $icicibal = $data['EFFECTIVEBAL'] ;
                               ?>

                                <li class="nav-item d-none d-lg-block mr-2"><img src="{{asset('/images/ICICI-Logo-1200x1227.png')}}"  class="img-radius" width="30" alt="user-walllet"> &#x20B9;  {{number_format($icicibal,3)}}</li>
                            @endif
                            <?php

                                $users_wallet = $User->where('role','!=','super admin')->where('role','!=','admin')->where('created_by','=',Auth::user()->id)->get()->sum('ewallet'); 
                                $users_wallet1 = $User->where('role','!=','super admin')->where('role','!=','admin')->where('created_by','=',Auth::user()->id)->get()->sum('aeps_wallet'); 

                                $users_wallet_sum = $users_wallet + $users_wallet1;

                                $users_commission = 0;
                                $users_commissionData = \DB::table('upi_qr_txns')->selectRaw('SUM(commission) as total_commission')->where('user_id','=',Auth::user()->id)->groupBy('user_id')->first();
                                if($users_commissionData)
                                {
                                    $users_commission+= $users_commissionData->total_commission;
                                }
                                $users_commissionData = \DB::table('bbps_transactions')->selectRaw('SUM(commission) as total_commission')->where('user_id','=',Auth::user()->id)->groupBy('user_id')->first();
                                if($users_commissionData)
                                {
                                    $users_commission+= $users_commissionData->total_commission;
                                }
                                $users_commissionData = \DB::table('aeps_txn_news')->selectRaw('SUM(commission) as total_commission')->where('user_id','=',Auth::user()->id)->groupBy('user_id')->first();
                                if($users_commissionData)
                                {
                                    $users_commission+= $users_commissionData->total_commission;
                                }
                                $users_commissionData = \DB::table('yesbank_transactions')->selectRaw('SUM(commission) as total_commission')->where('user_id','=',Auth::user()->id)->groupBy('user_id')->first();
                                if($users_commissionData)
                                {
                                    $users_commission+= $users_commissionData->total_commission;
                                }
                                

                                
                                
                                //dd($users_commission);

                            ?>

                            @if(Auth::user()->role != 'super admin' && Auth::user()->role != 'retailer')
                                <li class="nav-item d-none d-lg-block mr-2"><img src="{{asset('images/user-wallet-icon.png')}}"  class="img-radius" width="30" alt="user-walllet"> &#x20B9;  {{number_format($users_wallet_sum,3)}}</li>
                            @endif

                            @if(Auth::user()->role != 'super admin')
                                <li class="nav-item d-none d-lg-block "><img src="{{asset('images/user-wallet-icon1.png')}}"  class="img-radius" width="25" alt="user-walllet"> &#x20B9; {{number_format($users_commission,3)}} </li>
                            @endif
                        </ul>
                        <ul class="nav navbar-nav">
                            
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none" ><span class="user-name text-bold-600" style="margin-bottom: 0rem;">{{ ucfirst(Auth::user()->first_name ).' '.ucfirst(Auth::user()->last_name )}}</span>
                                         <!-- <?php if(Auth::user()->role == "super admin"): ?>
                                          <small>(Super Admin)</small>
                                         <?php endif ?>
                                         <?php if(Auth::user()->role == "sdistributor"): ?>
                                          <small>(Super Distributor)</small>
                                         <?php endif ?>
                                         <?php if(Auth::user()->role == "mdistributor"): ?>
                                          <small>(Master Distributor)</small>
                                         <?php endif ?>
                                         <?php if(Auth::user()->role == "distributor"): ?>
                                          <small>(Distributor)</small>
                                         <?php endif ?> -->
                                </div>
                                <span>
                                     <?php if(Auth::user()->profile_pic == null): ?>
                                        <img src="{{asset('images/placeholder-profile.png')}}" class="img-radius" height="40" width="40" alt="User-Profile-Image">
                                      <?php else: ?>
                                        <img class="round" src="{{ asset((Auth::user()->profile_pic) )}}" alt="avatar" height="40" width="40">
                                      <?php endif ?>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{route('profile')}}"><i class="feather icon-user"></i> My Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                <i class="feather icon-power"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <ul class="main-search-list-defaultlist d-none">
        <li class="d-flex align-items-center"><a class="pb-25" href="#">
                <h6 class="text-primary mb-0">Files</h6>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('admin/app-assets/images/icons/xls.png')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('admin/app-assets/images/icons/jpg.png')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('admin/app-assets/images/icons/pdf.png')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('admin/app-assets/images/icons/doc.png')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>
            </a></li>
        <li class="d-flex align-items-center"><a class="pb-25" href="#">
                <h6 class="text-primary mb-0">Members</h6>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('admin/app-assets/images/portrait/small/avatar-s-8.jpg')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('admin/app-assets/images/portrait/small/avatar-s-1.jpg')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('admin/app-assets/images/portrait/small/avatar-s-14.jpg')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('admin/app-assets/images/portrait/small/avatar-s-6.jpg')}}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                    </div>
                </div>
            </a></li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>No results found.</span></div>
            </a></li>
    </ul>
    <!-- END: Header