<link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/extensions/tether-theme-arrows.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/extensions/tether.min.css')}}">
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/extensions/shepherd-theme-default.css')}}"> -->
<!-- END: Vendor CSS-->
{{--<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/tables/ag-grid/ag-grid.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/tables/ag-grid/ag-theme-material.css')}}">--}}
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/app-user.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/aggrid.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/users.css')}}">
<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/wizard.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/dashboard-analytics.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/card-analytics.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/tour/tour.css')}}">
<!-- END: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/forms/select/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/extensions/swiper.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/dashboard-ecommerce.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/coming-soon.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/animate/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/app-todo.css')}}">

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/invoice.css')}}">

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">
<!-- END: Custom CSS-->

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet">