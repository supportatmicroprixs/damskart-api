<!-- BEGIN: Vendor JS-->
<script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('admin/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/extensions/tether.min.js')}}"></script>
<!-- <script src="{{ asset('admin/app-assets/vendors/js/extensions/shepherd.min.js')}}"></script> -->
<!-- END: Page Vendor JS-->
<script src="{{ asset('admin/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
{{--<script src="{{ asset('admin/app-assets/vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js')}}"></script>--}}
<script src="{{ asset('admin/app-assets/js/scripts/forms/wizard-steps.js')}}"></script>
<!-- BEGIN: Theme JS-->
<script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/user-profile.js')}}"></script>
<!-- END: Theme JS-->
<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<!-- END: Page Vendor JS-->
<script src="{{ asset('admin/app-assets/js/scripts/datatables/datatable.js')}}"></script>
{{--<script src="{{ asset('admin/app-assets/js/scripts/ag-grid/ag-grid.js')}}"></script>--}}
<script src="{{ asset('admin/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/extensions/dropzone.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/account-setting.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/navs/navs.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/app-chat.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/app-user.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/coming-soon/jquery.countdown.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/coming-soon.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/extensions/swiper.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/extensions/polyfill.min.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/extensions/sweet-alerts.js')}}"></script>
    <script src="{{ asset('admin/app-assets/js/scripts/pages/app-user.js')}}"></script>
    <script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js')}}"></script>
<!-- BEGIN: Page JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/dashboard-analytics.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pages/app-todo.js')}}"></script>
<!-- <script src="{{ asset('admin/app-assets/js/scripts/forms/form-tooltip-valid.js')}}"></script> -->
<script src="{{ asset('admin/app-assets/js/scripts/pages/invoice.js')}}"></script>
<script src="{{ asset('admin/custom.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$('#example').DataTable( {
    dom: 'Bfrtip',
    "pageLength": 50,
    //scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    fixedColumns:   {
        leftColumns: 2,
        //rightColumns: 1
    },
    buttons: [
        'excel', 'pdf', 'print', 
    ]
} );

$('#example11').DataTable( {
    dom: 'Bfrtip',
    "pageLength": 50,
    buttons: [
        'excel', 'pdf', 'print', 
    ]
} );
</script>
<!-- END: Page JS-->