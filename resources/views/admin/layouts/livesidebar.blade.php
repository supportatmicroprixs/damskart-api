@inject('User','App\User')
@inject('catalog','App\Models\Catalog')
@inject('package','App\Models\Package')
@inject('Services','App\Services')
@inject('Service_packages','App\Service_packages')
@inject('User_services','App\User_services')
 <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('index')}}">
                        <!-- <div class="brand-logo"></div> -->
                        <img src="{{asset('images/Damskart Logo.png')}}" class="img-responsive block  mx-auto" width="" height="30" alt="bg-img">
                        <h2 class="brand-text mb-0" style="padding-left: 0.4rem;font-size: 1.35rem;">Damskart Business</h2>
                    </a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="      menu-navigation">
                <?php if (Auth::user()->id == 2): ?>
                <li class=" nav-item {{ (request()->is('users-qr')) ? 'active' : '' }}"><a href="{{route('users-qr')}}"><i class="fa fa-qrcode"></i><span class="menu-title" data-i18n="Colors">User's QR</span></a></li>
                <?php else: ?>
                <li class=" nav-item {{ (request()->is('index')) ? 'active' : '' }}"><a href="{{route('index')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Colors">Dashboard</span></a></li>
                <li class=" navigation-header"><span>Apps</span>
                </li>
                <?php if (Auth::user()->role == 'super admin'  || Auth::user()->role == 'mdistributor' ||Auth::user()->role == 'sdistributor'||Auth::user()->role == 'distributor'): ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="User">Member</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is('add-user')) ? 'active' : '' }}"><a href="{{route('add-user')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Add Member">Add Member</span></a>
                        </li>
                         
                        <li class="{{ (request()->is('member-list')) ? 'active' : '' }}"><a href="{{route('member-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Member's List</span></a>
                        </li>
                        <?php if (Auth::user()->role == 'super admin' ): ?>
                            <li class="{{ (request()->is('admin-list')) ? 'active' : '' }}"><a href="{{route('admin-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Admin's List</span></a>
                            </li>
                        <?php endif ?>
                        <?php if (Auth::user()->role != 'super admin' ): ?>
                        <li class="{{ (request()->is('tokenbuy-list')) ? 'active' : '' }}"><a href="{{route('tokenbuy-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Buy Token</span></a>
                        <?php endif ?>

                    </ul>
                </li>
                
                <?php endif ?>
                <?php if (Auth::user()->role == 'mdistributor' ||Auth::user()->role == 'sdistributor'||Auth::user()->role == 'distributor'||Auth::user()->role == 'retailer' && Auth::user()->package_id != NULL ): ?>
                <li class=" nav-item"><a href="#"><i class="fa fa-th"></i><span class="menu-title" data-i18n="User">Services</span></a>
                    <ul class="menu-content">
                        <?php 
                          $user = Auth::user();
                          if ($user != null) {
                            $catelogs = $catalog->where('status', 1)->get();
                            foreach ($catelogs as $key => $value) {
                              $service_list = $Services->leftJoin('catelogs', 'catelogs.id', '=', 'services.catelog_id')->leftJoin('service_packages', 'service_packages.service_id', '=', 'services.id')->leftJoin('user_services', 'user_services.servicepack_id', '=', 'service_packages.id')->where('services.catelog_id', $value->id)->where('services.status', 1)->where('services.parent_id', null)->where('service_packages.status', 1)->where('user_services.status', 1)->where('user_services.user_id', $user->id)->where('service_packages.package_id', $user->package_id)->get(['services.id', 'services.service_name', 'services.icon', 'services.url']);
                                //print_r($service_list);exit;
                                foreach ($service_list as $key1 => $value1) {
                                   $service_child = $Services->where('parent_id', $value1->id)->where('status', 1)->get(['id', 'service_name', 'icon', 'url']);
                        ?>
                            <li class="{{ (request()->is($value1->url)) ? 'active' : '' }}"><a href="{{route($value1->url)}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Add Member">{{$value1->service_name}}</span></a></li>
                        <?php }
                            }
                          }
                        ?>
                        <!-- <li class="{{ (request()->is('')) ? 'active' : '' }}"><a href="{{route('aeps-wallet')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">DMT</span></a> -->
                        </li>
                    </ul>
                </li> 
                <?php endif ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-credit-card"></i><span class="menu-title" data-i18n="User">Wallet</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is('main-wallet')) ? 'active' : '' }}"><a href="{{route('e-wallet')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Main Wallet</span></a>
                        </li>
                        @php
                            $icici_aeps_count = \DB::table('services')->where('id', 1)->where('status', 1)->count();
                            $icici_aeps_count1 = \DB::table('services')->where('id', 2)->where('status', 1)->count();
                            if($icici_aeps_count > 0 || $icici_aeps_count1 > 0):
                        @endphp
                        <li class="{{ (request()->is('aeps-wallet')) ? 'active' : '' }}"><a href="{{route('aeps-wallet')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">AEPS Wallet</span></a>
                        </li>
                        @php
                            endif;
                        @endphp
                        <?php if (Auth::user()->role == 'retailer' || Auth::user()->role == 'mdistributor' ||Auth::user()->role == 'sdistributor'||Auth::user()->role == 'distributor' ): ?>
                            <li class="{{ (request()->is('addfundrequest')) ? 'active' : '' }}"><a href="{{route('add-fundrequest')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Add Fund Request</span></a>
                            </li>
                            <li class="{{ (request()->is('fundrequest-list')) ? 'active' : '' }}"><a href="{{route('fundrequest-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Fund Request List</span></a></li>
                        <?php endif ?>
                        <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'admin'|| Auth::user()->role == 'mdistributor' || Auth::user()->role == 'sdistributor'||Auth::user()->role == 'distributor'): ?>
                            <li class="{{ (request()->is('fundrequests')) ? 'active' : '' }}" ><a href="{{route('admin-fundrequestlist')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Fund Requests</span></a>
                        </li>
                        <?php endif ?>
                    </ul>
                </li>
                <?php 
                    $packagereport = $package->where('id', Auth::user()->package_id)->where('report', '1')->first();
                    if (Auth::user()->role == "super admin" || (Auth::user()->role != "super admin" && !empty($packagereport))) {
                ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-layers"></i><span class="menu-title" data-i18n="User">Report</span></a>
                    <ul class="menu-content">

                        <li class="{{ (request()->is('ewallet-report')) ? 'active' : '' }}"><a href="{{route('ewallet-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Main Wallet</span></a>
                        </li>
                        @php
                            $icici_aeps_count = \DB::table('services')->where('id', 1)->where('status', 1)->count();
                            $icici_aeps_count1 = \DB::table('services')->where('id', 2)->where('status', 1)->count();
                            if($icici_aeps_count > 0 || $icici_aeps_count1 > 0):
                        @endphp
                        <li class="{{ (request()->is('aepswalllet-report')) ? 'active' : '' }}"><a href="{{route('aeps-wallet-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">AEPS Wallet</span></a>
                        </li>
                        @php
                            endif;
                            $icici_aeps_count = \DB::table('services')->where('id', 3)->where('status', 1)->count();
                            if($icici_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('fundtransfer-report')) ? 'active' : '' }}"><a href="{{route('fundtransfer-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Fund Transfer Report</span></a>
                        </li>
                        @php
                            endif;
                            $icici_aeps_count = \DB::table('services')->where('id', 6)->where('status', 1)->count();
                            if($icici_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('express-payout-report')) ? 'active' : '' }}"><a href="{{route('express-payout-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Express Payout Report</span></a>
                        </li>
                        @php
                            endif;
                            $icici_aeps_count = \DB::table('services')->where('id', 4)->where('status', 1)->count();
                            if($icici_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('payout-report')) ? 'active' : '' }}"><a href="{{route('payout-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Payout Report</span></a>
                        @php
                            endif;
                            $icici_aeps_count = \DB::table('services')->where('id', 1)->where('status', 1)->count();
                            if($icici_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('aeps-report')) ? 'active' : '' }}"><a href="{{route('aeps-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">ICICI AEPS Report</span></a>
                        </li>
                        @php
                            endif;
                            $yesbank_aeps_count = \DB::table('services')->where('id', 2)->where('status', 1)->count();
                            if($yesbank_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('yesbank-report')) ? 'active' : '' }}"><a href="{{route('yesbank-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">YesBank Report</span></a>
                        </li>
                        @php
                            endif;
                            $yesbank_aeps_count = \DB::table('services')->where('id', 7)->where('status', 1)->count();
                            if($yesbank_aeps_count > 0):
                        @endphp
                        <!-- <li class="{{ (request()->is('')) ? 'active' : '' }}"><a href="#!"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Mobile Report</span></a>
                        </li> -->
                        <li class="{{ (request()->is('bbps-report')) ? 'active' : '' }}"><a href="{{route('bbps-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">BBPS Report</span></a>
                        </li>
                        @php
                            endif;
                            $icici_aeps_count = \DB::table('services')->where('id', 5)->where('status', 1)->count();
                            if($icici_aeps_count > 0):
                        @endphp
                        <li class="{{ (request()->is('upi-report')) ? 'active' : '' }}"><a href="{{route('upi-report')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">UPI Report</span></a>
                        </li>
                        @php
                            endif;
                        @endphp
                        <?php if (Auth::user()->role == 'super admin'  ): ?>
                        <li class="{{ (request()->is('cib-report')) ? 'active' : '' }}"><a href="{{route('cib-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">CIB Report</span></a>
                        </li>
                        <li class="{{ (request()->is('users-list')) ? 'active' : '' }}"><a href="{{route('users-login-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">User's Login Report</span></a>
                        </li> 
                        <!-- <li class="{{ (request()->is('bbps-deposit-transactions')) ? 'active' : '' }}"><a href="{{route('bbps-deposit-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">BBPS Deposit Report</span></a>
                        </li> -->
                        <?php endif ?>
                    </ul>
                </li>
                <?php }  ?> 
                <?php if (Auth::user()->role == 'super admin'  ): ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-settings"></i><span class="menu-title" data-i18n="User">Setting</span></a>
                    <ul class="menu-content">
                        
                        <li class="nav-item {{ (request()->is('token-list')) ? 'active' : '' }}"><a href="{{route('token-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Add Token">Token List</span></a>
                        </li>
               
                        <li class="{{ (request()->is('service-list')) ? 'active' : '' }}"><a href="{{route('service-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Service List</span></a>
                        </li>
                        <li class="{{ (request()->is('package-list')) ? 'active' : '' }}"><a href="{{route('package-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Packages List</span></a>
                        </li>
                        <li class="{{ (request()->is('upgraderole')) ? 'active' : '' }}"><a href="{{route('users-rolelist')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Upgrade User's Role</span></a>
                        </li>
                        
                    </ul>
                </li>
                <?php endif ?>
                <li class=" nav-item"><a href="#"><i class="fa fa-comments-o"></i><span class="menu-title" data-i18n="User">Query</span></a>
                    <ul class="menu-content">
                        <?php if (Auth::user()->role != 'retailer'): ?>
                        <li  class="{{ (request()->is('users-query')) ? 'active' : '' }}"><a href="{{route('users-query')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">User's Query</span></a></li>
                        <?php endif ?>
                        <?php if (Auth::user()->role == 'retailer' || Auth::user()->role == 'mdistributor' ||Auth::user()->role == 'sdistributor'||Auth::user()->role == 'distributor' ): ?>
                            <li  class="{{ (request()->is('query-list')) ? 'active' : '' }}"><a href="{{route('query-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Query List</span></a></li>
                            <li  class="{{ (request()->is('addquery')) ? 'active' : '' }}"><a href="{{route('add-query')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Add Query</span></a></li>
                        <?php endif ?>
                    </ul>
                </li>
                <?php if (Auth::user()->role == 'super admin'  ): ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-bell"></i><span class="menu-title" data-i18n="User">Notification</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is('notification-list')) ? 'active' : '' }}"><a href="{{route('notification-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Notification List</span></a>
                        </li>
                        <li class="{{ (request()->is('add-notification')) ? 'active' : '' }}"><a href="{{route('add-notification')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Add Notification</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-mail"></i><span class="menu-title" data-i18n="User">Message</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is('message-list')) ? 'active' : '' }}"><a href="{{route('message-list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Message List</span></a>
                        </li>
                        <li class="{{ (request()->is('add-message')) ? 'active' : '' }}"><a href="{{route('add-message')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Edit">Add Message</span></a>
                        </li>
                    </ul>
                </li>
                <?php endif ?>
                <li class=" nav-item"><a href="#"><i class="feather icon-shield"></i><span class="menu-title" data-i18n="User">Support</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is('search-transactions')) ? 'active' : '' }}"><a href="{{route('search-transactions')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">BBPS Search Transaction</span></a>
                        </li>
                        <li class="{{ (request()->is('complaint-registration')) ? 'active' : '' }}"><a href="{{route('complaint-registration')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">BBPS Complaint Registration</span></a>
                        </li>
                    </ul>
                </li>
                <?php endif ?>
                
            </ul>
            <br>
        </div>
    </div>
    <!-- END: Main Menu