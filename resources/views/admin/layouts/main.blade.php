<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
          <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Damskart Pay">
    <meta name="keywords" content="Damskart Pay">
    <meta name="author" content="Damskart Pay">
    <title>Dashboard - Damskart Business</title>
   
   @include('admin.layouts.header')
        @yield('css')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    @include('admin.layouts.nav')
    @include('admin.layouts.sidebar')
    <!-- BEGIN: Content-->
     @yield('content')
    <!-- END: Content-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2021. All rights Reserved</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->
    @include('admin.layouts.script')
   
    @yield('script')
     <script>
       
        var damskart = {'loadModal':function(payload,callback=''){

                    $(".common").modal('show');
                    $.ajax({
                        type:'POST',
                        url: payload.url,
                        data:payload,
                        success:function(response){                                               
                            $("#modal_view").html(response);
                            $("#common_title").text(payload.title);
                            $(".common").attr('id',payload.modal_id);
                            if(callback!=''){
                                $("#OK").attr('onclick',callback);
                            }
                        },
                        error: function(response){
                        }
                    });
                },'alertModal':function(payload){
                    $(".alertmodal").modal('show'); 
                    $("#alert_title").text(payload.title);
                    $("#alert_message").text(payload.message);
                }
        }
    </script>
    
    <div class="modal fade common"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="common_title"></h5>
                    
                </div>
                 <div id="modal_view"></div>
            </div>
        </div>
    </div>

    <div class="modal fade alertmodal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title"></h5>
                    
                </div>
                 <div id="alert_message" class="modal-body text-danger"></div>
                 <div class="modal-footer">
                  <button data-dismiss="modal" type="button" class="btn btn-primary btn-xs btn-corner" >Ok</button>
                </div>
            </div>
        </div>
    </div>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/60d1d54165b7290ac6374642/1f8pqi2gn';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
<!-- END: Body-->
</html>