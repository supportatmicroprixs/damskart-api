@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">UPI Commission</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">Setting
                        </li>
                        <li class="breadcrumb-item">Package
                        </li>
                        <li class="breadcrumb-item active">Package Service's
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Center</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                           <ul class="nav nav-tabs justify-content-center" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#Generate-Qr" aria-controls="home-center" role="tab" aria-selected="true">Generate Qr</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#Send-Request " aria-controls="service-center" role="tab" aria-selected="false">Send Request </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" id="account-tab-center" data-toggle="tab" href="#My-QR" aria-controls="account-center" role="tab" aria-selected="false">My QR</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="Generate-Qr" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">Generate Qr</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updategenqr',$upi_genqr_com->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="dynamicTable" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Commission</th>
                                                               <th scope="col">Commission Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                                $commission = json_decode($upi_genqr_com->commission);
                                                                 if ($upi_genqr_com->commission == 'null') {
                                                                  echo "
                                                                 
                                                                        <tr>
                                                               
                                                                           <td>
                                                                              <input type='text' name='store[0][start_price]' placeholder='Start Price'  class='form-control round start_value' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][end_price]' placeholder='End Price' class='form-control round end_value' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][commission]' placeholder='Commission' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                             
                                                                              <select class='form-control round' name='store[0][commission_type]' required>
                                                                                 <option>Select Type</option>
                                                                                 <option value='flat'>Flat</option>
                                                                                 <option value='percentage'>Percentage</option>
                                                                              </select>
                                                                           </td>
                                                                           <td>
                                                                              <button type='button' name='add' id='add' class='btn btn-primary round'>Add More</button>
                                                                           </td>
                                                                        </tr>
                                                                   ";
                                                                }else{
                                                                 foreach ($commission as $key => $value) {
                                                                    $abc = $key;
                                                                  echo "<tr>
                                                                  <td><input type='text' name='store[".$key."][start_price]' placeholder='Start Price' value='$value->start_price' class='form-control round start_value'  required/></td>
                                                                  <td><input type='text' name='store[".$key."][end_price]' placeholder='End Price' value='$value->end_price'  class='form-control round end_value' required/></td>  
                                                                  <td><input type='text' name='store[".$key."][commission]' placeholder='Commission'  value='$value->commission' class='form-control round' required /></td>  
                                                                  <td>
                                                                      
                                                                     <select class='form-control round' name='store[".$key."][commission_type]'>";
                                                                       echo '<option value="flat"';
                                                                        if ($value->commission_type =='flat') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Flat</option>';
                                                                        echo '<option value="percentage"';
                                                                        if ($value->commission_type =='percentage') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Percentage</option>';
                                                                      
                                                                  echo"  </select>
                                                                  </td>
                                                                  ";
                                                                  ?>
                                                            <td>
                                                               <?php if ($key == 0): ?>
                                                               <button type='button' name='add' id='add' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="amount(<?php echo $i; ?>)">Remove</button>
                                                               <!-- id="price<?php echo $i; ?>" -->
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue' value=".$abc.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane " id="Send-Request" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">Send Request</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updatesendreq',$upi_sendreq_com->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="sendReqdynamicTable" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Commission</th>
                                                               <th scope="col">Commission Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                                $commission2 = json_decode($upi_sendreq_com->commission);
                                                                 if ($upi_sendreq_com->commission == 'null') {
                                                                  echo "
                                                                 
                                                                        <tr>
                                                               
                                                                           <td>
                                                                              <input type='text' name='store[0][start_price]' placeholder='Start Price'  class='form-control round start_value' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][end_price]' placeholder='End Price' class='form-control round end_value' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][commission]' placeholder='Commission' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                             
                                                                              <select class='form-control round' name='store[0][commission_type]' required>
                                                                                 <option>Select Type</option>
                                                                                 <option value='flat'>Flat</option>
                                                                                 <option value='percentage'>Percentage</option>
                                                                              </select>
                                                                           </td>
                                                                           <td>
                                                                              <button type='button' name='addsendrequest' id='addsendrequest' class='btn btn-primary round'>Add More</button>
                                                                           </td>
                                                                        </tr>
                                                                   ";
                                                                }else{
                                                                 foreach ($commission2 as $key2 => $value) {
                                                                    $abc2 = $key2;
                                                                  echo "<tr>
                                                                  <td><input type='text' name='store[".$key2."][start_price]' placeholder='Start Price' value='$value->start_price' class='form-control round start_value'  required/></td>
                                                                  <td><input type='text' name='store[".$key2."][end_price]' placeholder='End Price' value='$value->end_price'  class='form-control round end_value' required/></td>  
                                                                  <td><input type='text' name='store[".$key2."][commission]' placeholder='Commission'  value='$value->commission' class='form-control round' required /></td>  
                                                                  <td>
                                                                      
                                                                     <select class='form-control round' name='store[".$key2."][commission_type]'>";
                                                                       echo '<option value="flat"';
                                                                        if ($value->commission_type =='flat') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Flat</option>';
                                                                        echo '<option value="percentage"';
                                                                        if ($value->commission_type =='percentage') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Percentage</option>';
                                                                      
                                                                  echo"  </select>
                                                                  </td>
                                                                  ";
                                                                  ?>
                                                            <td>
                                                               <?php if ($key2 == 0): ?>
                                                               <button type='button' name='addsendrequest' id='addsendrequest' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="amount(<?php echo $i; ?>)">Remove</button>
                                                               <!-- id="price<?php echo $i; ?>" -->
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue2' value=".$abc2.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane " id="My-QR" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">My QR</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form  class="form form-vertical" action="{{route('updatenyqr',$upi_myqr_com->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="aadhardynamicTable" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Commission</th>
                                                               <th scope="col">Commission Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $j = 1;
                                                               $commission1 = json_decode($upi_myqr_com->commission);
                                                               if ($upi_myqr_com->commission == 'null') {
                                                                // alert('hello');
                                                                 echo "
                                                                 
                                                                       <tr>
                                                               
                                                                          <td>
                                                                             <input type='text' name='store[0][start_price]' placeholder='Start Price' class='form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                             <input type='text' name='store[0][end_price]' placeholder='End Price' class='form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                             <input type='text' name='store[0][commission]' placeholder='Commission' class='form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                            
                                                                             <select class='form-control round' name='store[0][commission_type]' required>
                                                                                <option>Select Type</option>
                                                                                <option value='flat'>Flat</option>
                                                                                <option value='percentage'>Percentage</option>
                                                                             </select>
                                                                          </td>
                                                                          <td>
                                                                             <button type='button' name='addaadhar' id='addaadhar' class='btn btn-primary round'>Add More</button>
                                                                          </td>
                                                                       </tr>
                                                                   ";
                                                               }else{
                                                               
                                                               
                                                                  foreach ($commission1 as $key1 => $value1) {
                                                                      $abc1 = $key1;
                                                                   echo "<tr>
                                                                   <td><input type='text' id='start_price' value='$value1->start_price' name='store[".$key1."][start_price]' placeholder='Start Price' value='$value1->start_price' class='form-control round'  required /></td>
                                                                   <td><input type='text' name='store[".$key1."][end_price]' placeholder='End Price' value='$value1->end_price' class='form-control round' required /></td>  
                                                                   <td><input type='text' name='store[".$key1."][commission]' placeholder='Commission' value='$value1->commission' class='form-control round' required /></td>  
                                                                   <td>
                                                                       
                                                                      <select class='form-control round' name='store[".$key1."][commission_type]' required>";
                                                                        echo '<option value="flat"';
                                                                         if ($value1->commission_type =='flat') {
                                                                            echo ' selected';
                                                                         }
                                                                         echo '>Flat</option>';
                                                                         echo '<option value="percentage"';
                                                                         if ($value1->commission_type =='percentage') {
                                                                            echo ' selected';
                                                                         }
                                                                         echo '>Percentage</option>';
                                                                       
                                                                   echo"  </select>
                                                                   </td>
                                                                   ";
                                                                   ?>
                                                            <td>
                                                               <?php if ($key1 == 0): ?>
                                                               <button type='button' name='addaadhar' id='addaadhar' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?> 
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="addmore(<?php echo $j; ?>)">Remove</button>
                                                               <!-- id="price<?php echo $j; ?>" -->
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue1' value=".$abc1.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection