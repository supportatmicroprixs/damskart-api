@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Member's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Member
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <strong>{{ $message }}</strong>
         </div>
         @endif 
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <!-- <div class="card">
            <div class="card-header">
               <h4 class="card-title">Filters</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <ul class="list-inline mb-0">
                     <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                     <li><a data-action=""><i class="feather icon-rotate-cw users-data-filter"></i></a></li>
                     <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                  </ul>
               </div>
            </div>
            <div class="card-content collapse show">
               <div class="card-body">
                  <div class="users-list-filter">
                     <form>
                        <div class="row">
                           <div class="col-12 col-sm-6 col-lg-3">
                              <label for="users-list-role">Role</label>
                              <fieldset class="form-group">
                                 <select class="form-control" id="users-list-role">
                                    <option value="">All</option>
                                    <option value="user">User</option>
                                    <option value="staff">Staff</option>
                                 </select>
                              </fieldset>
                           </div>
                           <div class="col-12 col-sm-6 col-lg-3">
                              <label for="users-list-status">Status</label>
                              <fieldset class="form-group">
                                 <select class="form-control" id="users-list-status">
                                    <option value="">All</option>
                                    <option value="Active">Active</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="deactivated">Deactivated</option>
                                 </select>
                              </fieldset>
                           </div>
                           <div class="col-12 col-sm-6 col-lg-3">
                              <label for="users-list-verified">Verified</label>
                              <fieldset class="form-group">
                                 <select class="form-control" id="users-list-verified">
                                    <option value="">All</option>
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                 </select>
                              </fieldset>
                           </div>
                           <div class="col-12 col-sm-6 col-lg-3">
                              <label for="users-list-department">Department</label>
                              <fieldset class="form-group">
                                 <select class="form-control" id="users-list-department">
                                    <option value="">All</option>
                                    <option value="Sales">Sales</option>
                                    <option value="Devlopment">Devlopment</option>
                                    <option value="Management">Management</option>
                                 </select>
                              </fieldset>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div> -->
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Column selectors with Export and Print Options</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th >Name</th>
                                       <th >Owner</th>
                                       <th >Email</th>
                                       <th >User Role</th>
                                       <th >Assigned Package</th>
                                       <th >Main Wallet</th>
                                       <th >AEPS Wallet</th>
                                       <th >Onboarding</th>
                                       <th >Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($userdistributor as $item)
                                    <tr>
                                       <td ><a href="{{route('view-member', $item->id)}}">{{ucfirst($item->first_name).' '.ucfirst($item->last_name) }}<span> ({{$item->mobile}})</span></a></td>
                                       
                                       <td>
                                       @foreach($owner as $item1)
                                       <?php if ($item->created_by == $item1->id): ?>
                                          <?php if (Auth::user()->role == "super admin"): ?>
                                             <a href="{{route('view-member', $item1->id)}}">{{ucfirst($item1->first_name).' '.ucfirst($item1->last_name) }}
                                                <small>({{$item1->mobile}})</small></a>
                                             
                                          <?php else: ?>
                                             <a href="{{route('profile')}}">{{$item1->first_name.' '.$item1->last_name }}
                                                <small>({{$item1->mobile}})</small></a>
                                             
                                          <?php endif ?>
                                       <?php endif; ?>
                                       @endforeach
                                    </td>

                                       <td >{{$item->email}}</td>
                                       <td >
                                          <?php if ($item->role == 'admin'): ?>
                                          <label  >Admin</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'sdistributor'): ?>
                                          <label  >Super Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'mdistributor'): ?>
                                          <label >Master Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'distributor'):?>
                                          <label >Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'retailer'): ?>
                                          <label >Retailer</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'manager'): ?>
                                          <label >Manager</label>
                                          <?php endif ?>

                                          	<button type="button" class="dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border: none; background: transparent;">
	                                         </button>
	                                         <div class="dropdown-menu">
                                             @if($item->role != "sdistributor")
                                             <a class="dropdown-item" href="/upgrade/{{$item->id}}/sdistributor">Super Distributor</a>
                                             @endif
                                             @if($item->role != "mdistributor")
                                             <a class="dropdown-item" href="/upgrade/{{$item->id}}/mdistributor">Master Distributor</a>
                                             @endif
                                             @if($item->role != "distributor")
                                             <a class="dropdown-item" href="/upgrade/{{$item->id}}/distributor">Distributor</a>
                                             @endif
                                             @if($item->role != "retailer")
                                             <a class="dropdown-item" href="/upgrade/{{$item->id}}/retailer">Retailer</a>
                                             @endif
                                             @if($item->role != "manager")
                                             <!--<a class="dropdown-item" href="/upgrade/{{$item->id}}/manager">Manager</a>-->
                                             @endif
                                          </div>
                                       </td>
                                       <td>
                                             <?php if ($item->role == 'manager'): ?>
                                             <?php else: ?>
                                          		{{$item->package_name}}

                                          		<button type="button" class="dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border: none; background: transparent;">
   	                                         </button>

   	                                         <div class="dropdown-menu">
   	                                         	@foreach($package as $itemPackage)
   	                                         	<a class="dropdown-item" href="upgradePackage/{{$item->id}}/{{ $itemPackage->id }}">{{ $itemPackage->package_name }}</a>
   	                                         	@endforeach
   	                                         </div>
                                             <?php endif; ?>
                                       	</td>
                                       <td >&#x20B9; {{number_format($item->ewallet,3)}}</td>
                                       <td >&#x20B9; {{number_format($item->aeps_wallet,3)}}</td>
                                       <td >
                                          <?php if ($item->aeps_ekyc_status == 1): ?>
                                          <label class="label label-success label-lg">Done</label>
                                          <?php elseif($item->aeps_ekyc_status == 0): ?>
                                          <label class="label label-danger label-lg">Pending</label>
                                          <?php else: ?>
                                          <!-- <label class="label label-primary label-lg">Not Registered Yet!</label> -->
                                          <p style="color: red;">Not Registered Yet!</p>
                                          <?php endif ?>
                                       </td>
                                       <!--<td ><span style="display: none;">{{date('YmdHis', strtotime($item->last_login))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->last_login))}}</td>-->
                                       <td >
                                          <?php $active = '#active'.$item->id; ?>
                                          <?php $inactive = '#inactive'.$item->id; ?>
                                          <?php if($item->status == 1): ?>
                                          <button  type="button"   onclick="editMember(0,<?= $item->id ;?>,'<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success ">Active</button>
                                          <button  type="button"   onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger " style="display: none;">Inactive</button>
                                          <?php elseif($item->status == 0): ?>
                                          <button  type="button" onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger ">Inactive</button>
                                          <button  type="button" onclick="editMember(0,<?= $item->id ;?>, '<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success " style="display: none;">Active</button>
                                          <?php endif ?>
                                        </td>
                                          <!--  <a href="/editmember/{{$item->id}}" style="color: #3c8dbc;" title="Edit" class="feather icon-edit"></a>
                                             &nbsp; <a href="/deletemember/{{$item->id}}" style="color:#FF0000;" title="Delete" class=" feather icon-trash"></a> -->
                                          <!--<a href="/viewmember/{{$item->id}}" title="View" class="btn btn-primary">View</a>-->
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   function editMember(status, id, cid, sid) {
     if(id) {
         $.ajax({
             url: '/changeuser',
             type: 'get',
             data: {id : id,status : status},
             dataType: 'json',
             success:function(response) {
               console.log(response.success)
                
               if (response.status == 1) {
                  $(cid).hide();
                  $(sid).show();
               }else{
                  $(cid).hide();
                  $(sid).show();
               }
   
             }
         }); 
   
     } else {
         alert("Error : Refresh the page again");
     }
   }
</script>
@endsection