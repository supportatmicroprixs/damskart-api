@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Add Query</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Query
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Transfer Fund</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('add-query') }}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                              {{csrf_field()}}
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Name</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="text" class="form-control" placeholder="Name" value="{{ucfirst($user->first_name).' '.ucfirst($user->last_name)}}" name="user_name" id="user_name" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-edit-2"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Mobile</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="number" id="mobile" class="form-control" name="mobile" value="{{$user->mobile}}" placeholder="Mobile" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-smartphone"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Email</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="email" id="email" class="form-control" name="email" value="{{$user->email}}" placeholder="Email" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-mail"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group">
                                          <label for="password-icon">Query</label>
                                          <div class="position-relative has-icon-left">
                                             <textarea class="form-control" cols="3" rows="3" placeholder="Your Query" name="query" required=""></textarea>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection