@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Add Fund Request</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Wallet
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Transfer Fund</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('add-fundrequest') }}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                              {{csrf_field()}}
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Amount</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="number" id="amount" class="form-control" name="amount"  value="{{ old('amount') }}" placeholder="Amount" required="">
                                             <div class="form-control-position">
                                                <i class="fa fa-inr"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon"> Transaction Id (if any)</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="text" id="proof_txnid" class="form-control" name="proof_txnid" value="{{ old('proof_txnid') }}" placeholder="Proof Transaction Id (if any)">
                                             <div class="form-control-position">
                                                <i class="fa fa-check-circle-o"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group d-flex align-items-center pt-md-2">
                                          <label class="mr-2">Payment Type :</label>
                                          <div class="c-inputs-stacked">
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="payment_type" value="IMPS">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">IMPS</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block mr-2" >
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="payment_type" value="NEFT">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">NEFT</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="payment_type" value="CASH">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">CASH</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Upload Transaction Reciept</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="file" id="proof_image" class="form-control" name="proof_image" value="{{ old('proof_image') }}" >
                                             <div class="form-control-position">
                                                <i class="feather icon-file"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group">
                                          <label for="password-icon">Remarks</label>
                                          <div class="position-relative has-icon-left">
                                             <textarea class="form-control" cols="3" rows="3" placeholder="Remarks" name="remarks" id="remarks" required=""></textarea>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection