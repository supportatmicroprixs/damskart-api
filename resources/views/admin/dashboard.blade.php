@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .card_border{
   border-bottom: 3px solid #FF7600;
   }
   .card_align{
   display: grid;
   }
</style>
@endsection
@section('content')
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <!-- Dashboard Analytics Start -->
         <section id="dashboard-analytics">
            <div class="row">
              
               @if(Auth::user()->role != "retailer")
               <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                  <div class="card card_border">
                     <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                           <div class="col-sm-12 text-center">
                              <img class="img-fluid" src="{{asset('images/user-group-512.png')}}" style="width: 50px;" alt="Member">
                              <br />
                              <h3>Members</h3>
                           </div>
                        </div>
                     </div>
                     <div class="card-content">
                        <div class="card-body text-center mx-auto">
                           <div class="d-flex justify-content-between mt-1">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                              </div>
                           </div>
                           <hr>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">
                                    <?php echo $todaymember; ?>
                                 </p>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">
                                    <?php echo $totalmember; ?>
                                 </p>
                              </div>
                           </div>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todaymemberlogin; ?></p>
                                 <span class="">Today Login</span>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totalmemberlogin; ?></p>
                                 <span class="">Month Login</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endif

               @php
                  if (Auth::user()->role != 'super admin')
                  {
                  		$icici_aeps_count = \DB::table('user_services')->leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->where('service_id', 1)->where('user_id', Auth::user()->id)->where('user_services.status', 1)->where('service_packages.status', 1)->count();
                        $icici_aeps_count1 = \DB::table('user_services')->leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->where('service_id', 2)->where('user_id', Auth::user()->id)->where('user_services.status', 1)->where('service_packages.status', 1)->count();

                       if($icici_aeps_count > 0 || $icici_aeps_count1 > 0):
               @endphp
               <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                  <div class="card card_border">
                     <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                           <div class="col-sm-12 text-center">
                              <img class="img-fluid" src="{{asset('images/aeps.png')}}" width="100" title="AEPS Logo" alt="Aeps Logo">
                              <br />
                              <h3>AEPS</h3>
                           </div>
                        </div>
                     </div>
                     <div class="card-content">
                        <div class="card-body text-center mx-auto">
                           <div class="d-flex justify-content-between mt-1">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                              </div>
                           </div>
                           <hr>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($todayiciciaepsamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($totoaliciciaepsamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                           </div>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todayiciciaepstxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoaliciciaepstxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @php
               			endif;

                        $bbps_count2 = \DB::table('user_services')->leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->where('service_id', 7)->where('user_id', Auth::user()->id)->where('user_services.status', 1)->where('service_packages.status', 1)->count();

                       if($bbps_count2 > 0):
               @endphp
               <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                     <div class="card card_border">
                        <div class="card-header mx-auto pb-0">
                           <div class="row m-0">
                              <div class="col-sm-12 text-center">
                                 <img class="img-fluid" src="{{asset('images/bbps_logo_hor_colored%20(1).png')}}" width="100" title="BBPS Logo" alt="Aeps Logo" style="margin-bottom: 15px;">
                                 <br />
                                 <h3>BBPS</h3>
                              </div>
                           </div>
                        </div>
                        <div class="card-content">
                           <div class="card-body text-center mx-auto">
                              <div class="d-flex justify-content-between mt-1">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                                 </div>
                              </div>
                              <hr>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($todaybbpsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($totoalbbpsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                              </div>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todaybbpstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoalbbpstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  @php

                       endif;
               		}
               		else
               		{
               	@endphp
               	<div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                     <div class="card card_border">
                        <div class="card-header mx-auto pb-0">
                           <div class="row m-0">
                              <div class="col-sm-12 text-center">
                                 <img class="img-fluid" src="{{asset('images/aeps.png')}}" width="100" title="AEPS Logo" alt="Aeps Logo">
                                 <br />
                                 <h3>AEPS</h3>
                              </div>
                           </div>
                        </div>
                        <div class="card-content">
                           <div class="card-body text-center mx-auto">
                              <div class="d-flex justify-content-between mt-1">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                                 </div>
                              </div>
                              <hr>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($todayiciciaepsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($totoaliciciaepsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                              </div>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todayiciciaepstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoaliciciaepstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                     <div class="card card_border">
                        <div class="card-header mx-auto pb-0">
                           <div class="row m-0">
                              <div class="col-sm-12 text-center">
                                 <img class="img-fluid" src="{{asset('images/bbps_logo_hor_colored%20(1).png')}}" width="100" title="AEPS Logo" alt="Aeps Logo" style="margin-bottom: 15px;">
                                 <br />
                                 <h3>BBPS</h3>
                              </div>
                           </div>
                        </div>
                        <div class="card-content">
                           <div class="card-body text-center mx-auto">
                              <div class="d-flex justify-content-between mt-1">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                                 </div>
                              </div>
                              <hr>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($todaybbpsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                       {{ number_format($totoalbbpsamt, 2, '.', ',') }}
                                    </p>
                                    <!-- <span class="">Amount</span> -->
                                 </div>
                              </div>
                              <div class="d-flex justify-content-between mt-2">
                                 <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todaybbpstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                                 <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoalbbpstxn; ?></p>
                                    <span class="">Transactions</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               	@php
               		}
               @endphp
               <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                  <div class="card card_border">
                     <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                           <div class="col-sm-12 text-center">
                              <img class="img-fluid" src="{{ asset('images/fund_transfer_icon.png') }}" width="50" alt="Aeps Logo">
                              <br />
                              <h3>Fund Transfer</h3>
                           </div>
                        </div>
                     </div>
                     <div class="card-content">
                        <div class="card-body text-center mx-auto">
                           <div class="d-flex justify-content-between mt-1">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                              </div>
                           </div>
                           <hr>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($todayFundamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($totoalFundamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                           </div>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todayFundtxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoalFundtxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-6 col-sm-12 profile-card-2 card_align">
                  <div class="card card_border">
                     <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                           <div class="col-sm-12 text-center">
                              <img class="img-fluid" src="{{ asset('images/payout_icon.png') }}" width="50" alt="Aeps Logo">
                              <br />
                              <h3>Payout</h3>
                           </div>
                        </div>
                     </div>
                     <div class="card-content">
                        <div class="card-body text-center mx-auto">
                           <div class="d-flex justify-content-between mt-1">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">TODAY</p>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">TOTAL</p>
                              </div>
                           </div>
                           <hr>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($todayPayoutamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0">&#x20B9; 
                                    {{ number_format($totoalPayoutamt, 2, '.', ',') }}
                                 </p>
                                 <!-- <span class="">Amount</span> -->
                              </div>
                           </div>
                           <div class="d-flex justify-content-between mt-2">
                              <div class="uploads">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $todayPayouttxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                              <div class="following">
                                 <p class="font-weight-bold font-medium-2 mb-0"><?php echo $totoalPayouttxn; ?></p>
                                 <span class="">Transactions</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </section>
         <!-- <div class="content-body">
            <div id="user-profile">
                <div class="row">
                    <div class="col-3">
                        <div class="profile-header mb-2">
                            <div class="relative">
                               
                                <div class="card-body text-center" style="background: #7b70f1;color: white;border-radius: 8px;border-bottom-right-radius: 32px;border-bottom-left-radius: 32px;">
                                    <span class="float-right" style="font-size: 26px;"><b>20</b></span>
                                    <div class=""> 
                                         TODAY Member 
                                    </div>
                               </div>
                                <div class="profile-img-container d-flex align-items-center justify-content-between">
                                    <img src="{{asset('admin/app-assets/images/portrait/small/avatar-s-12.jpg')}}"  class="rounded-circle img-border box-shadow-1" alt="Card image">
                                    <div class="float-right">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end align-items-center profile-header-nav">
                                <div class=" text-center">
                             <h4 class="my-1" style="color: #7b70f1;"><b>672</b></h4>
                             <p class="">TOTAL Member</p>
                         </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div> -->
         <!-- Dashboard Analytics end -->
         <!-- <section>
            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-xl-5 col-md-8 col-sm-10 col-12 px-md-0 px-2">
                    <div class="card text-center w-100 mb-0">
                        <div class="card-header justify-content-center pb-0">
                            <div class="card-title">
                                <h2 class="mb-0">We are launching soon</h2>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                <img src="{{asset('images/Damskart Logo.png')}}" class="img-responsive block width-100 mx-auto" alt="bg-img">
                                <div  id="date_count" class="card-text text-center getting-started pt-2 d-flex justify-content-center flex-wrap"></div>
                                <div class="divider" style="margin: 0px;">
                                    <div class="divider-text"><img src="{{asset('images/comming-soon-2-unscreen (1).gif')}}" class="img-responsive block width-100 mx-auto" alt="bg-img"></div>
                                </div>
                                <p class="text-center ">
                                    All the services will be live from 1st April 2021.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section> -->
      </div>
   </div>
</div>
@endsection
@section('script')
<!-- <script>
   // Set the date we're counting down to
   var countDownDate = new Date("Apr 1, 2021").getTime();
   
   // Update the count down every 1 second
   var x = setInterval(function() {
   
     // Get today's date and time
     var now = new Date().getTime();
       
     // Find the distance between now and the count down date
     var distance = countDownDate - now;
       
     // Time calculations for days, hours, minutes and seconds
     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
       
     document.getElementById("date_count").innerHTML = "<div class='clockCard px-1'> <span>"+days+"</span> <br> <p class='bg-amber clockFormat lead px-1 black'> Days </p> </div><div class='clockCard px-1'> <span>"+hours+"</span> <br> <p class='bg-amber clockFormat lead px-1 black'> Hours </p> </div><div class='clockCard px-1'> <span>"+minutes+"</span> <br> <p class='bg-amber clockFormat lead px-1 black'> Minutes </p> </div><div class='clockCard px-1'> <span>"+seconds+"</span> <br> <p class='bg-amber clockFormat lead px-1 black'> Seconds </p> </div>";
       
     // If the count down is over, write some text 
     if (distance < 0) {
       clearInterval(x);
       document.getElementById("date_count").innerHTML = "EXPIRED";
     }
   }, 1000);
   </script> -->
@endsection