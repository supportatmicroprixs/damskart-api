@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
 
    @media print{
         body *{
         visibility: hidden;
         }
         .print-container, .print-container *{
         visibility: visible;
         }
         .print-container, #cl_button *{
         visibility: hidden;
         }
         }  
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Fund Transaction's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Reports
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <form action="{{ route('fundtransfer-report') }}" method="post" name="fund_transfer_form" class="form-horizontal">
                           @csrf
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php if($user->role == "super admin"): ?>
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                             </div>
                                             <select name="package_id" id="package_id" class="form-control">
                                                <option value="" selected="">Select Package</option>
                                                @foreach($package as $item)
                                                <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th >Txn ID</th>
                                       <th >Txn Date</th>
                                       <th >Txn Type</th>
                                       <th >Beneficiary</th>
                                       <th >UTR No.</th>
                                       <th >Amount</th>
                                       <th >Status</th>
                                       <th >Message</th>
                                       <th >Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($fundtransferlist as $item)
                                    <tr>
                                       <td >XX{{ substr($item->dms_txn_id, 9) }}</td>
                                       <td ><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                       <td >{{$item->transfer_type}}</td>
                                       <td data-placement="left" data-html="true" title="{{$item->benificiary_name}} <br />{{$item->rmobile}} <br /> {{$item->account}}<br /> {{$item->bank_name}}" data-toggle="tooltip"> {{$item->benificiary_name}} <br />
                                       </td>
                                       <td >
                                          <?php if($item->utrnumber == null): ?>
                                             <p>-</p>
                                          <?php else: ?>
                                             {{$item->utrnumber}}
                                          <?php endif ?>
                                       </td>
                                       <td >&#x20B9; {{number_format($item->amount,3)}}</td>
                                       <td >{{$item->status}}</td>
                                       <td >{{$item->statusMessage}}</td>
                                       <td>
                                          @if($item->status == "PENDING")
                                             <button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" href="/searchFundtransactionsInfo?txn_ref_id={{ $item->orderId }}" data-target="#getPendingView{{$item->orderId}}"><i class="fa fa-eye"></i></button>

                                          @else
                                          <button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" id="view_receipt" data-target="#getreceipt{{$item->id}}"><i class="fa fa-eye"></i></button>
                                          @endif
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- search transactions table-->
                     <div class="card-content" id="searchtxn">
                     </div>
                     <!-- search transactions table-->
                     @foreach($fundtransferlist as $item)
                     <div class="modal fade" id="getPendingView{{$item->orderId}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 50%;">
                           <div class="modal-content">
                              <div class="modal-header" style="background: linear-gradient(to right, #7767F0, #cc2FF4);">
                                 <h4 class="modal-title" style=" color: white;" id="myModalLabel">Transaction Details</h4>
                              </div> 
                              <div class="modal-body print-container">
                                 <div class="row">
                                    <div class="col-md-12">
                                          <div class="transaction_result{{$item->orderId}}"></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="modal-footer" style="justify-content: center;">
                                 <h5>Thanks for your Business!</h5>
                                 <button type="button" class="btn btn-secondary" onclick="location.reload(true);" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="getreceipt{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                           <div class="modal-content">
                              <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                 <h4 class="modal-title" style=" color: white;" id="myModalLabel">{{$item->transfer_type}}</h4>
                              </div> 
                              <div class="modal-body  print-container" id="getCode" >
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="content" style="text-align: center; margin-left: 0px;">
                                              <div id="invoice-template" class="card-body">
                                                  <div id="invoice-company-details" class="row">
                                                      <div class="col-sm-6 col-12 text-left pt-1">
                                                          <div class="media pt-1">
                                                              <img src="{{asset('images/fund_transfer_icon.png')}}" style="width: 120px;">
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-6 col-12 text-right">
                                                          <h1>Payment Details</h1>
                                                          <div class="invoice-details mt-2">
                                                              <h6>UTR No.</h6>
                                                              <?php if($item->utrnumber == null): ?>
                                                                 <p>-</p>
                                                              <?php else: ?>
                                                                 <p>{{$item->utrnumber}}</p>
                                                              <?php endif ?>
                                                              <h6>Txn Date & Time</h6>
                                                              <p><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span><h5>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</h5></p>
                                                              <h6>Transfer Mode</h6>
                                                              <?php if($item->selected_pay_mode == NULL): ?>
                                                                <?php if($item->transferMode == "TPA"): ?>
                                                                <p>NEFT</p>
                                                                <?php elseif($item->transferMode == "IFS"): ?>
                                                                <p>IMPS</p>
                                                                <?php elseif($item->transferMode == "RGS"): ?>
                                                                <p>RTGS</p>
                                                                <?php endif ?>
                                                              <?php else: ?>
                                                                <p>{{$item->selected_pay_mode}}</p>
                                                              <?php endif ?>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div id="invoice-customer-details" class="row pt-2 ">
                                                      <div class="col-sm-6 col-12 text-left">
                                                          <div class="recipient-info my-2">
                                                              <h6>Txn Status</h6>
                                                              <p>{{$item->status}}</p>
                                                              <h6>Sender Number</h6>
                                                              <p>{{$item->rmobile}}</p>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-6 col-12 text-right">
                                                          <div class="company-info my-2">
                                                              <h6>Account No.</h6>
                                                              <p>{{$item->account}}</p>
                                                              <h6>Bank Name</h6>
                                                              <p>{{$item->bank_name}}</p>
                                                              <h6>IFSC Code</h6>
                                                              <p>{{$item->ifsc_code}}</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                      <div class="row">
                                                          <div class="table-responsive col-12">
                                                              <table class="table table-borderless">
                                                                  <tbody>
                                                                      <tr>
                                                                          <td class="text-left"><b>Transaction Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                          <td class="text-right"><h5>{{number_format($item->amount , 3)}}</h5></td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td class="text-left"><b>Surcharge (<i class="fa fa-inr"></i>)</b></td>
                                                                          <td class="text-right" >{{number_format($item->surcharge , 3)}}</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td class="text-left"><b>Wallet Balance (<i class="fa fa-inr"></i>)</b></td>
                                                                          <td class="text-right" >{{number_format($item->current_balance , 3)}}</td>
                                                                      </tr>
                                                                     
                                                                  </tbody>
                                                              </table>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="modal-footer" >
                                 <!-- <h5>Thanks for your Business!</h5> -->
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')

@foreach($fundtransferlist as $item)
<script type="text/javascript">
   // Fill modal with content from link href
   $("#getPendingView{{$item->orderId}}").on("show.bs.modal", function(e) {
       var link = $(e.relatedTarget);
       $(".transaction_result{{$item->orderId}}").load(link.attr("href"));
   });
</script>
@endforeach

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>

@endsection