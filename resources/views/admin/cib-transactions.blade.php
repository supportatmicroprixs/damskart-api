@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">CIB Transaction's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Reports
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                          </div>
                                       </fieldset>
                                    </div>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>

                                    <div class="col-md-12 col-12 mb-1" id="errormessage"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Transaction Date</th>
                                       <th>Transaction ID</th>
                                       <th>REMARKS</th>
                                       <th>Transaction Amount</th>
                                       <th>Transaction Type</th>
                                       <th>Account Balance</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($cibtransaction as $item)
                                    <tr>
                                       <td class="text-center"><span style="display: none;">{{date('YmdHis', strtotime($item['TXNDATE']))}}</span>{{date('d-m-Y h:i:s A', strtotime($item['TXNDATE']))}}</td>
                                       <td class="text-center">{{$item['TRANSACTIONID']}}</td>
                                       <td class="">{{$item['REMARKS']}}</td>
                                       <td class="text-center"> {{$item['AMOUNT']}}</td>
                                       <td class="text-center">{{$item['TYPE']}}</td>
                                       <td class="text-center"> {{$item['BALANCE']}}</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- search transactions table-->
                     <div class="card-content" id="searchtxn">
                     </div>
                     <!-- search transactions table-->
                     
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')

<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
      var filter_date = $('#filter_date').val();
      var date_arr = filter_date.split('- ');

      var start_date_arr = date_arr[0].split('/');
      var end_date_arr = date_arr[1].split('/');
      var start_date = start_date_arr[1]+"-"+start_date_arr[0]+"-"+start_date_arr[2];
      var end_date = end_date_arr[1]+"-"+end_date_arr[0]+"-"+end_date_arr[2];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filtercibtransactions", 
           data : {'start_date':start_date,'end_date':end_date},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                 var Result = data;
                 if (Result.RESPONSE == "Failure") {
                    var error =Result.MESSAGE;
                    $("#errormessage").empty();
                     $("#errormessage").append('<p class="btn btn-danger">'+error+'</p>');
                 }else{
                  $("#alltxn").empty();
                  $("#searchtxn").empty();
                    $("#errormessage").empty();
                  $("#transactionresult").empty();
                  $("#searchtxn").append(' <div class="card-body card-dashboard"><div class="table-responsive"><table id="example1" class="table table-striped table-bordered ">   <thead>      <tr>  <th>Transaction Date</th><th>Transaction ID</th><th>Transaction Amount</th><th>Transaction Type</th><th>REMARKS</th><th>Account Balance</th>       </tr>   </thead>   <tbody id="transactionresult">        </tbody></table></div></div>');
                  
                  var isArray = Array.isArray(Result);
                   if (isArray) {
                      $.each(Result,function(key,value){
                         $("#transactionresult").append(' <tr> <td class="text-center">'+value.TXNDATE+'</td><td class="text-center">'+value.TRANSACTIONID+'</td><td class="text-center"> '+value.AMOUNT+'</td><td class="text-center">'+value.TYPE+'</td><td class="">'+value.REMARKS+'</td><td class="text-center"> '+value.BALANCE+'</td></tr>');
                      });
                   }else{
                      $("#transactionresult").append(' <tr> <td class="text-center">'+Result.TXNDATE+'</td><td class="text-center">'+Result.TRANSACTIONID+'</td><td class="text-center"> '+Result.AMOUNT+'</td><td class="text-center">'+Result.TYPE+'</td><td class="">'+Result.REMARKS+'</td><td class="text-center"> '+Result.BALANCE+'</td></tr>');
                   }

                 }
               }
   
                $('#example1').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>

@endsection