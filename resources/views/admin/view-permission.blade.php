@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   #doc_img{
      width: 20%;border-radius: 3px;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Permission</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Permission</a>
                        </li>
                        <li class="breadcrumb-item active"> View
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @if ($errors->any())
      <div  class="alert alert-danger alert-block">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      @endif
      @if ($message = Session::get('info'))
      <div class="alert alert-primary alert-block">
         <strong>{{ $message }}</strong>
      </div>
      @endif 
      @if ($message = Session::get('danger'))
      <div class="alert alert-danger alert-block">
         <strong>{{ $message }}</strong>
      </div>
      @endif
      <div class="content-body">
         <!-- account setting page start -->
         <section id="page-account-settings">
            <div class="row">
               <!-- left menu section -->
               <div class="col-md-6">
                  <div class="card">
                     <div class="card-content">
                        <div class="card-body">
                           <div class="row">
                              <div class="col-6">
                                 <div class="media">
                                    <div class="media-body mt-75">
                                       <p>{{ucfirst($permission->name)}}
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-6">
                              </div>
                           </div>
                           <hr>
                           <form  action="{{route('updatepermission',$permission->id)}}" method="POST" enctype="multipart/form-data" novalidate>
                              {{csrf_field()}}
                              <div class="row">
                                 <div class="col-12">
                                    <div class="form-group">
                                       <div class="controls">
                                          <label for="account-username">Name</label>
                                          <input type="text" class="form-control" id="account-username" placeholder="First Name" value="{{ucfirst($permission->name)}}" name="name" required data-validation-required-message="This field is required">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                    <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                    changes</button>
                                    <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- account setting page end -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
  function editMember(status, id, cid, sid) {
    if(id) {
        $.ajax({
            url: '/userservicestatus',
            type: 'get',
            data: {id : id,status : status},
            dataType: 'json',
            success:function(response) {
              console.log(response.success)
               
              if (response.status == 1) {
                 $(cid).hide();
                 $(sid).show();
              }else{
                 $(cid).hide();
                 $(sid).show();
              }

            }
        }); 
 
    } else {
        alert("Error : Refresh the page again");
    }
}
</script>
<script type="text/javascript">
  function updateMemberStatus(status, id, cid, sid) {
    if(id) {
        $.ajax({
            url: '/changeuser',
            type: 'get',
            data: {id : id,status : status},
            dataType: 'json',
            success:function(response) {
              console.log(response.success)
               
              if (response.status == 1) {
                 $(cid).hide();
                 $(sid).show();
              }else{
                 $(cid).hide();
                 $(sid).show();
              }

            }
        }); 
 
    } else {
        alert("Error : Refresh the page again");
    }
}
</script>

<script type="text/javascript">
   $('#example1').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
   $('#example2').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
</script>
@endsection