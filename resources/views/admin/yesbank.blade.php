@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Yes Bank AEPS</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Services
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
           <section id="basic-examples">
                    <div class="row match-height">
                        <div class="col-xl-4 col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{asset('images/yes_bank_edit.png')}}" style="padding: 17px 17px 0px 17px;" alt="Card image cap">
                                    <div class="card-body">
                                        <div class="card-btns d-flex justify-content-between mt-2">
                                          @if(!empty($yesbank_access))
                                            <a href="{{$yesbank_access}}" target="_blank" class="btn gradient-light-primary btn-block mt-2">Go to Yes Bank AEPS <i class="feather icon-arrow-right"></i></a>
                                          @elseif(!empty($outletRegister))
                                            <a href="{{route('profile')}}" class="btn gradient-light-primary btn-block mt-2">Click To Register with Yes Bank AEPS <i class="feather icon-arrow-right"></i></a>
                                          @else 
                                            <p class="text-center" style="flex: auto;"><b>KYC Not Updated Yet !</b></p>
                                          @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Profile Cards Ends -->
                    </div>
                </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection