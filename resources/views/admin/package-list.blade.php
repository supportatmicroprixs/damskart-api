@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Package</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Settings
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title"></h4> -->
                        <button type="button" style="margin-left: auto;" class="btn btn-relief-primary mr-1 mb-1" data-backdrop="false" data-toggle="modal" data-target="#exampleModalScrollable">
                        <i class="feather icon-plus"></i> Add Package
                        </button>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Package Name</th>
                                       <th>Amount</th>
                                       <th>Role</th>
                                       <th>Report Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($package as $item)
                                    <tr>
                                       <td>{{$item->package_name }}</td>
                                       <td>{{number_format($item->amount,3)}}</td>
                                       <td>
                                          <?php if ($item->role == 'all'): ?>
                                          <label  >All</label>
                                          <?php endif ?> 
                                          <?php if ($item->role == 'sdistributor'): ?>
                                          <label  >Super Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'mdistributor'): ?>
                                          <label >Master Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'distributor'):?>
                                          <label >Distributor</label>
                                          <?php endif ?>
                                          <?php if ($item->role == 'retailer'): ?>
                                          <label >Retailer</label>
                                          <?php endif ?>
                                       </td>
                                       <td>
                                          <?php $active = '#active'.$item->id; ?>
                                          <?php $inactive = '#inactive'.$item->id; ?>
                                          <?php if($item->report == 1): ?>
                                          <button  type="button"   onclick="editMember(0,<?= $item->id ;?>,'<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success ">Active</button>
                                          <button  type="button"   onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger " style="display: none;">Inactive</button>
                                          <?php elseif($item->report == 0): ?>
                                          <button  type="button" onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger ">Inactive</button>
                                          <button  type="button" onclick="editMember(0,<?= $item->id ;?>, '<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success " style="display: none;">Active</button>
                                          <?php endif ?>
                                       </td>
                                       <td>
                                          <!-- <a href="/editpackage/{{$item->id}}" style="color: #3c8dbc;" title="Edit" class="feather icon-edit"></a> -->
                                          <!-- &nbsp; <a href="/deletepackage/{{$item->id}}" style="color:#FF0000;" title="Delete" class=" feather icon-trash"></a> -->
                                          &nbsp; <a href="/viewservice/{{$item->id}}"  title="View" class=" btn btn-primary">View</a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- Modal -->
                     <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                           <div class="modal-content">
                              <div class="modal-header"  style="background: linear-gradient(to right, #7367F0, #968df4);">
                                 <h5 class="modal-title" style=" color: white;" id="exampleModalScrollableTitle">Add Package</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <form  action="{{route('package-list')}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <label>Package Name </label>
                                    <div class="form-group">
                                       <input type="text" placeholder="Package Name" class="form-control " name="package_name" required="">
                                    </div>
                                    <label>Amount </label>
                                    <div class="form-group">
                                       <input type="text" name="amount" placeholder="Package Amount" class="form-control" required="" >
                                    </div>
                                    <label>Role</label>
                                    <div class="form-group">
                                       <select class="form-control" name="role" required="">
                                          <option value="">Select Role</option>
                                          <option value="sdistributor">Super Distributor</option>
                                          <option value="mdistributor">Master Distributor</option>
                                          <option value="distributor">Distributor</option>
                                          <option value="retailer">Retailor</option>
                                       </select>
                                    </div>
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="form-control" name="status" required="">
                                          <option value="">Select Status</option>
                                          <option value="1">Active</option>
                                          <option value="0">Inactive</option>
                                       </select>
                                    </div>
                              </div>
                              <div class="modal-footer">
                              <button type="submit" class="btn btn-primary"><i class="feather icon-plus"></i> Add </button>
                              </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   function editMember(report, id, cid, sid) {
     if(id) {
         $.ajax({
             url: '/changepackagereport',
             type: 'get',
             data: {id : id,report : report},
             dataType: 'json',
             success:function(response) {
               console.log(response.success)
                
               if (response.report == 1) {
                  $(cid).hide();
                  $(sid).show();
               }else{
                  $(cid).hide();
                  $(sid).show();
               }
   
             }
         }); 
   
     } else {
         alert("Error : Refresh the page again");
     }
   }
</script>
@endsection