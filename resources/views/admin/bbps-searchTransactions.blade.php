@extends('admin.layouts.main')
@section('css')
<style type="text/css">
    .divWaiting {
            position: fixed;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">BBPS</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">Service
                        </li>
                        <li class="breadcrumb-item active">Search Transactions
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Search Transactions</h4>
                        <img src="{{asset('images/bbps_logo_hor_colored (1).png')}}" style="width: 9%;margin-left: auto;">
                     </div>
                     
                     <div class="card-content">
                        <div class="card-body">
                              <div class="form-body">
                                <div id="errormessage"></div>
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Transaction ref ID</label>
                                          <div class="position-relative has-icon-left">
                                             <!-- <input type="text" name="txn_ref_id" onkeyup="txn_ref_idDetect()" class="form-control" placeholder="Transaction Reference Id" id="txn_ref_id" > -->
                                             <select class="form-control" name="txn_ref_id" onchange="txn_ref_idDetect()" id="txn_ref_id"> 
                                               <option value="">Select Option</option>
                                                @foreach($bbps_txn_data as $item)
                                                  <option value="{{$item->bbps_txnid}}">{{$item->bbps_txnid}}</option>
                                                @endforeach
                                             </select>
                                             <div class="form-control-position">
                                                <i class="feather icon-hash"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="divider divider-left-center">
                                    <div class="divider-text">
                                       <div class="badge badge-pill badge-secondary">OR</div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Mobile Number</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="text" name="mobile_number" onkeyup="mobileDetect()"  id="mobile_number" class="form-control" placeholder="Customer Mobile Number" value="">
                                             <div class="form-control-position">
                                                <i class="feather icon-smartphone"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Select Date</label>
                                          <div class="position-relative has-icon-left">
                                             <!-- <input type="date" name="start_date" id="start_date" onchange="mobileDetect()" class="form-control"> -->
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                             <div class="form-control-position">
                                                <i class="feather icon-calendar"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                 </div>
                              </div>
                              <div class="col-12">
                                 <button type="submit" id="searchbutton" onclick="serchTxn()" class="btn btn-primary mr-1 mb-1">Search</button>
                                 <!-- <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button> -->
                              </div>
                              <div class="row">
                                  <div class="col-6" id="txn_idresult"></div>
                              </div>
                        </div>
                       
                        <div class="col-12" id="txn_date_res">
                            
                        </div>
                        <br>
                     </div>
                  </div>
               </div>
            </div>
      </div>
   </div>
   </section>
   <div class="divWaiting" id="Dv_Loader" style="display: none;">
      <button class="btn btn-primary mb-1 round" type="button" >
      <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
      &nbsp;Loading... 
      </button>
   </div>
   <!-- Column selectors with Export Options and print table -->
</div>
</div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
  function txn_ref_idDetect(){
    var txn_ref_id = $('#txn_ref_id').val();
        if (txn_ref_id.length > 0) {
          $('#mobile_number').attr('disabled', 'disabled');
          $('#filter_date').attr('disabled', 'disabled');
        } else {
          $('#mobile_number').removeAttr('disabled');
          $('#filter_date').removeAttr('disabled');
        }
  }
</script>
<script type="text/javascript">
  function mobileDetect(){
    var mobile_number = $('#mobile_number').val();
    var filter_date = $('#filter_date').val();
        if (mobile_number.length > 0 || filter_date.length > 0) {
          $('#txn_ref_id').attr('disabled', 'disabled');
        } else {
          $('#txn_ref_id').removeAttr('disabled');
        }
  }
</script>
<script type="text/javascript">
   function serchTxn(){
    var txn_ref_id = $('#txn_ref_id').val();
    var mobile_number = $('#mobile_number').val();
    var filter_date = $('#filter_date').val();
    var date_arr = filter_date.split('- ');
    var start_date = date_arr[0];
    var end_date = date_arr[1];
     $('#Dv_Loader').show();
     $.ajax({
        type:"get",
        url:"/searchtransactions", 
        data : {'mobile_number':mobile_number,'txn_ref_id':txn_ref_id,'start_date':start_date,'end_date':end_date,},
        dataType: "json",
        success:function(data)
        {   
          if(data)
            {
               $('#Dv_Loader').hide();
               // $('#billbutton').hide();
               // $('#txn_ref_id').attr('disabled', 'disabled');
               // $('#complaint_Disposition').attr('disabled', 'disabled');
               // $('#complaint_Desc').attr('disabled', 'disabled');
               // $('#billbutton').attr('disabled', 'disabled');
               var Result = data.bbps_TransactionStatus;

               var TransactionData = data.bbps_TransactionStatus.txnList;
               if (Result.responseCode == "000") {
                  if (data.searchtype == "TRANS_REF_ID") {
                    $("#txn_idresult").empty();
                  $("#transactionresult").empty();
                  $("#errormessage").empty();
                  $("#txn_date_res").empty();
                    
                  var txnamount = TransactionData.amount / 100;
                    $("#txn_idresult").append(' <table class="table table-striped table-responsive ">    <thead>         <tr>              <th colspan="3">                   <h5 id="complaintmessage">Transaction Details</h5>              </th>         </tr>    </thead>    <tbody><td>Amount</td>             <td>:</td>              <td id="amount"> '+txnamount+'</td>         </tr>      <tr>              <td>Biller Name</td>             <td>:</td>              <td id="biller_name">'+TransactionData.billerId+'</td>         </tr>      <tr>              <td>Transaction Date</td>             <td>:</td>              <td id="transaction_date">'+moment(TransactionData.txnDate).format('DD-MM-YYYY hh:mm:ss A')+'</td>         </tr>      <tr>              <td>Txn Ref ID</td>             <td>:</td>              <td id="txn_ref_id">'+TransactionData.txnReferenceId+'</td>         </tr>      <tr>              <td>Transaction Status</td>             <td>:</td>              <td id="transaction_status">'+TransactionData.txnStatus+'</td>         </tr>     </tbody></table>');
                  }else{
                    $("#txn_idresult").empty();
                    $("#errormessage").empty();
                    $("#txn_date_res").empty();
                    $("#txn_date_res").append('<div class="table-responsive" id="allmyqrtxn">   <table id="dom-jqry2" class="table table-striped table-bordered zero-configuration">      <thead>         <tr>             <th>Amount</th>             <th>Biller Name</th>             <th>Transaction Date</th>             <th>Txn Ref ID</th>             <th>Transaction Status</th>         </tr>      </thead>      <tbody id="transactionresult">                </tbody>   </table></div>');
                      

                      var isArray = Array.isArray(TransactionData);
                      if (isArray) {
                        $("#transactionresult").empty();
                        $.each(TransactionData,function(key,value){
                          var txn_amount =  value.amount /100;
                        $("#transactionresult").append(' <tr>  <td id="amount"> '+txn_amount+'</td>           <td id="biller_name">'+value.billerId+'</td>           <td id="transaction_date">'+moment(value.txnDate).format('DD-MM-YYYY hh:mm:ss A')+'</td>           <td id="txn_ref_id">'+value.txnReferenceId+'</td>           <td id="transaction_status">'+value.txnStatus+'</td>         </tr> ');
                        });
                      }else{
                        $("#transactionresult").empty();
                        
                          var txn_amount =  TransactionData.amount /100;
                        $("#transactionresult").append(' <tr> <td id="amount"> '+txn_amount+'</td>           <td id="biller_name">'+TransactionData.billerId+'</td>           <td id="transaction_date">'+moment(TransactionData.txnDate).format('DD-MM-YYYY hh:mm:ss A')+'</td>           <td id="txn_ref_id">'+TransactionData.txnReferenceId+'</td>           <td id="transaction_status">'+TransactionData.txnStatus+'</td>         </tr> ');
                       
                      }


                  }

               }else{
                   var error = Result.errorInfo.error.errorMessage;
                   // alert(error);
                   $("#txn_idresult").empty();
                  $("#transactionresult").empty();
                  $("#txn_date_res").empty();
                  $("#errormessage").empty();
                  $("#errormessage").append('<p class="text-white btn btn-danger">'+error+'</p>');
               }
            }
             $('#dom-jqry2').dataTable();
        }
     });
   }
</script>
@endsection