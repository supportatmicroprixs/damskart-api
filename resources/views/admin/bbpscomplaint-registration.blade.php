   @extends('admin.layouts.main')
@section('css')
<style type="text/css">
    .divWaiting {
            position: fixed;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }

</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
 <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">BBPS</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Service
                                    </li>
                                    <li class="breadcrumb-item active">Complaint Registration and Tracking
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
                    </div>
                </div>
                <!-- Column selectors with Export Options and print table -->
                
                 <section id="basic-and-outline-pills">
                    <div class="row match-height">
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Complaint Registration and Tracking</h4>
                                    <img src="{{asset('images/bbps_logo_hor_colored (1).png')}}" style="width: 9%;margin-left: auto;">
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        
                                        <ul class="nav nav-pills nav-active-bordered-pill">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                                    Register Complaint
                                                </a>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item active" id="dropdown1-tab" href="#dropdown1" data-toggle="pill" aria-expanded="true"> Transaction Complaint</a>
                                                    <a class="dropdown-item" id="dropdown2-tab" href="#dropdown2" data-toggle="pill" aria-expanded="true">Service Complaint</a>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="about-tab"  data-toggle="pill" href="#about" aria-expanded="false">Complaint Tracking</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="dropdown1" role="tabpanel" aria-labelledby="dropdown1-tab" aria-expanded="false">
                                                  <div class="form-body">
                                                     <div class="row">
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Transaction Reference Id</label>
                                                              <div class="position-relative has-icon-left">
                                                                <!-- <input type="text" name="txn_ref_id" class="form-control" placeholder="Transaction Reference Id" onkeyup="registerDetected()" id="txn_ref_id" > -->
                                                                <select class="form-control" name="txn_ref_id" class="form-control" onchange="registerDetected()" id="txn_ref_id"> 
                                                                   <option value="">Select Option</option>
                                                                    @foreach($bbps_txn_data as $item)
                                                                      <option value="{{$item->bbps_txnid}}">{{$item->bbps_txnid}}</option>
                                                                    @endforeach
                                                                 </select>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-hash"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Select Disposition *</label>
                                                              <div class="position-relative has-icon-left">
                                                                <select class="form-control" name="operator_code" id="complaint_Disposition" onchange="registerDetected()">
                                                                    <option value="">Select Disposition</option>
                                                                    <option value="Transaction Successful, account not updated">Transaction Successful, account not updated</option>
                                                                </select>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-info"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        
                                                        <div class="col-12">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Description</label>
                                                              <div class="position-relative has-icon-left">
                                                                <textarea class="form-control" id="complaint_Desc" onkeyup="registerDetected()" placeholder="Description" name="" cols="3" rows="3">Complaint initiated through API</textarea>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-edit"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-12">
                                                           <button type="submit" id="billbutton" onclick="register()"  class="btn btn-primary mr-1 mb-1" disabled>Register</button>
                                                           <!-- <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button> -->
                                                        </div>
                                                        <div class="col-12">
                                                          <div id="complaintresponse">
                                                          </div>
                                                          <div id="errormessage">
                                                          </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                            </div>
                                            <div class="tab-pane" id="dropdown2" role="tabpanel" aria-labelledby="dropdown2-tab" aria-expanded="false">
                                                  <div class="form-body">
                                                     <div class="row">
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Participation Type</label>
                                                              <div class="position-relative has-icon-left">
                                                                <select class="form-control" name="service_participation_type" id="service_participation_type" onchange="registerDetected()" required>
                                                                    <option value="">Select Participation Type</option>
                                                                    <option value="AGENT">Agent</option>
                                                                    <option value="BILLER">Biller</option>
                                                                </select>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-info"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Agent Id *</label>
                                                              <div class="position-relative has-icon-left">
                                                                 <input type="text" name="service_agent_id" class="form-control" placeholder="Agent Id" onkeyup="registerDetected()" id="service_agent_id" value="CC01CC01513515340681">
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-user"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Biller Id *</label>
                                                              <div class="position-relative has-icon-left">
                                                                 <input type="text" name="service_biller_id" class="form-control" placeholder="Biller Id" onkeyup="registerDetected()" id="service_biller_id" value="">
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-hash"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Service Reason *</label>
                                                              <div class="position-relative has-icon-left">
                                                                <select class="form-control" name="service_reason" id="service_reason" onchange="registerDetected()">
                                                                    <option value="">Select Service Reason</option>
                                                                    <option value="Agent overcharging">Agent overcharging</option>
                                                                </select>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-edit-2"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-12">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Description</label>
                                                              <div class="position-relative has-icon-left">
                                                                <textarea class="form-control" name="service_complaint_Desc" placeholder="Description" id="service_complaint_Desc" onkeyup="registerDetected()" cols="3" rows="3">Complaint initiated through API</textarea>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-edit"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-12">
                                                           <button type="submit" id="service_regbutton" onclick="serviceregister()" class="btn btn-primary mr-1 mb-1">Register</button>
                                                           <!-- <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button> -->
                                                        </div>
                                                        <div class="col-12">
                                                          <div id="service_complaintresponse">
                                                          </div>
                                                          <div id="service_errormessage">
                                                          </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                            </div>
                                            <div class="tab-pane" id="about" role="tabpanel" aria-labelledby="about-tab" aria-expanded="false">
                                                  <div class="form-body">
                                                     <div class="row">
                                                        <div class="col-6">
                                                           <div class="form-group">
                                                              <label for="contact-info-icon">Complaint Id</label>
                                                              <div class="position-relative has-icon-left">
                                                                 <!-- <input type="text" name="complaint_id" class="form-control" placeholder="Complaint Id" id="complaint_id" onkeyup="trackDetected()" > -->
                                                                 <select name="complaint_id" class="form-control" placeholder="Complaint Id" id="complaint_id" onchange="trackDetected()"> 
                                                                   <option value="">Select Option</option>
                                                                    @foreach($bbps_txn_complaint as $item)
                                                                      <option value="{{$item->complaint_id}}">{{$item->bbps_txnid}}</option>
                                                                    @endforeach
                                                                 </select>
                                                                 <div class="form-control-position">
                                                                    <i class="feather icon-hash"></i>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div class="col-12">
                                                           <button type="submit" id="trackbutton" onclick="complainttrack()" class="btn btn-primary mr-1 mb-1" disabled>Check Status</button>
                                                           <!-- <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button> -->
                                                        </div>
                                                        <div class="col-12">
                                                           <div id="trackingresponse">
                                                           </div>
                                                           <br>
                                                           <div id="trackingerrormessage">
                                                           </div>
                                                         </div>
                                                     </div>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="divWaiting" id="Dv_Loader" style="display: none;">
                  <button class="btn btn-primary mb-1 round" type="button" >
                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
                  &nbsp;Loading... 
                  </button>
               </div>
                <!-- Column selectors with Export Options and print table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
  function registerDetected(){
    var txn_ref_id = $('#txn_ref_id').val();
    var complaint_Disposition = $('#complaint_Disposition').val();
    var complaint_Desc = $('#complaint_Desc').val();
        if (txn_ref_id.length > 0 && complaint_Disposition.length > 0 && complaint_Desc.length > 0) {
          $('#billbutton').removeAttr('disabled');
        } else {
          $('#billbutton').attr('disabled', 'disabled');
        }
  }
</script>
<script type="text/javascript">
  function trackDetected(){
    var complaint_id = $('#complaint_id').val();
        if (complaint_id.length > 0) {
          $('#trackbutton').removeAttr('disabled');
        } else {
          $('#trackbutton').attr('disabled', 'disabled');
        }
  }
</script>
<script type="text/javascript">
  document.getElementById('service_participation_type').onchange = function(){
          document.getElementById('service_agent_id').disabled = (this.value === '' || this.value === 'BILLER');
          document.getElementById('service_biller_id').disabled = (this.value === '' || this.value === 'AGENT');
      }
</script>
<script type="text/javascript">
   function register(){
    var register_transaction = "Transaction";
    // var register_transaction = $('#register_transaction').val();
    var txn_ref_id = $('#txn_ref_id').val();
    var complaint_Desc = $('#complaint_Desc').val();
    var complaint_Disposition = $('#complaint_Disposition').val();
     $('#Dv_Loader').show();
     $.ajax({
        type:"get",
        url:"/complaintregistration", 
        data : {'register_transaction':register_transaction,'txn_ref_id':txn_ref_id,'complaint_Desc':complaint_Desc,'complaint_Disposition':complaint_Disposition,},
        dataType: "json",
        success:function(data)
        {   
          if(data)
            {
               $('#Dv_Loader').hide();
               // $('#billbutton').hide();
               $('#txn_ref_id').attr('disabled', 'disabled');
               $('#complaint_Disposition').attr('disabled', 'disabled');
               $('#complaint_Desc').attr('disabled', 'disabled');
               $('#billbutton').attr('disabled', 'disabled');
               var Result = data;
               $("#complaintresponse").empty();
               $("#errormessage").empty();
               if (Result.responseCode == "000") {
                  $("#complaintresponse").append('<div class="table-responsive border rounded px-1 " style="border: 2px solid #EDEDED !important;"> <table class="table table-border" style="color:black;">   <thead>      <tr>         <th colspan="3">            <h5 id="complaintmessage">Complaint Registratered Successfully</h5>         </th>      </tr>   </thead>   <tbody>      <tr>         <td>Complaint ID</td>         <td>:</td>         <td id="complaintid"><b>'+Result.complaintId+'</b></td>      </tr>      <tr>         <td>Complaint Assigned</td>         <td>:</td>         <td id="complaintasssign"><b>'+Result.complaintAssigned+'</b></td>      </tr>   </tbody></table></div>');
               }else{
                   var error = Result.errorInfo.error.errorMessage;
                  $("#errormessage").empty();
                  $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
               }
            }
        }
     });
   }
</script>
<script type="text/javascript">
   function serviceregister(){
      var register_transaction = "Service";
      // var register_transaction = $('#service_complaint').val();
    var service_participation_type = $('#service_participation_type').val();
    var service_agent_id = $('#service_agent_id').val();
    var service_biller_id = $('#service_biller_id').val();
    var service_reason = $('#service_reason').val();
    var service_complaint_Desc = $('#service_complaint_Desc').val();
     $('#Dv_Loader').show();
     $.ajax({
        type:"get",
        url:"/complaintregistration", 
        data : {'register_transaction':register_transaction,'service_participation_type':service_participation_type,'service_agent_id':service_agent_id,'service_biller_id':service_biller_id,'service_reason':service_reason,'service_complaint_Desc':service_complaint_Desc,},
        dataType: "json",
        success:function(data)
        {   
          if(data)
            {
               $('#Dv_Loader').hide();
               // $('#billbutton').hide();
               $('#service_participation_type').attr('disabled', 'disabled');
               $('#service_agent_id').attr('disabled', 'disabled');
               $('#service_biller_id').attr('disabled', 'disabled');
               $('#service_reason').attr('disabled', 'disabled');
               $('#service_complaint_Desc').attr('disabled', 'disabled');
               $('#service_regbutton').attr('disabled', 'disabled');
               var Result = data;
               $("#service_complaintresponse").empty();
               $("#service_errormessage").empty();
               if (Result.responseCode == "000") {
                  $("#service_complaintresponse").append(' <div class="table-responsive border rounded px-1 " style="border: 2px solid #EDEDED !important;"><table class="table " style="color:black;">   <thead>      <tr>         <th colspan="3">            <h5 id="complaintmessage">Complaint Registratered Successfully</h5>         </th>      </tr>   </thead>   <tbody>      <tr>         <td>Complaint ID</td>         <td>:</td>         <td id="complaintid"><b>'+Result.complaintId+'</b></td>      </tr>      <tr>         <td>Complaint Assigned</td>         <td>:</td>         <td id="complaintasssign"><b>'+Result.complaintAssigned+'</b></td>      </tr>   </tbody></table></div>');
               }else{
                   var error = Result.errorInfo.error.errorMessage;
                  $("#service_errormessage").empty();
                  $("#service_errormessage").append('<p class="btn btn-danger">'+error+'</p>');
               }
            }
        }
     });
   }
</script>
<script type="text/javascript">
   function complainttrack(){
    var complaint_track = "Transaction";
    var complaint_id = $('#complaint_id').val();
     $('#Dv_Loader').show();
     $.ajax({
        type:"get",
        url:"/bbpsComplaintTracking", 
        data : {'complaint_track':complaint_track,'complaint_id':complaint_id,},
        dataType: "json",
        success:function(data)
        {   
          if(data)
            {
               $('#Dv_Loader').hide();
               // $('#billbutton').hide();
               // $('#complaint_id').attr('disabled', 'disabled');
               // $('#trackbutton').attr('disabled', 'disabled');
               var Result = data;
               $("#trackingresponse").empty();
               $("#trackingerrormessage").empty();

               if (Result.responseCode == "000") {
                  $("#trackingresponse").append('<div class="table-responsive border rounded px-1 " style="border: 2px solid #EDEDED !important;"> <table class="table " style="color:black;">   <thead>      <tr>         <th colspan="3">            <h5 id="complaintmessage">Complaint Status</h5>         </th>      </tr>   </thead>   <tbody>      <tr>         <td>Complaint ID</td>         <td>:</td>         <td id="complaintid"><b>'+Result.complaintId+'</b></td>      </tr>      <tr>         <td>Complaint Assigned</td>         <td>:</td>         <td id="complaintasssign"><b>'+Result.complaintAssigned+'</b></td>      </tr><tr>         <td>Complaint Status</td>         <td>:</td>         <td id="complaintasssign"><b>'+Result.complaintStatus+'</b></td>      </tr>   </tbody></table></div>');
               }else{
                  var error = Result.errorInfo.error.errorMessage;
                  $("#trackingerrormessage").empty();
                  $("#trackingerrormessage").append('<p class="btn btn-danger">'+error+'</p>');
               }
            }
        }
     });
   }
</script>

@endsection