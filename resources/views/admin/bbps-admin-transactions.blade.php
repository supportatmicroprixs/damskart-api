@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
    
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      	<div class="content-header row">
        	<div class="content-header-left col-md-9 col-12 mb-2">
	            <div class="row breadcrumbs-top">
	               <div class="col-12">
	                  <h2 class="content-header-title float-left mb-0">BBPS Transaction's List</h2>
	                  <div class="breadcrumb-wrapper col-12">
	                     <ol class="breadcrumb">
	                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
	                        </li>
	                        <li class="breadcrumb-item active">Reports
	                        </li>
	                     </ol>
	                  </div>
	               </div>
	            </div>
         	</div>
      	</div>
      	<div class="content-body">
			<div class="row">
				<div class="col-12">
				   <p id="resp_message"></p>
				</div>
			</div>

         	<!-- Column selectors with Export Options and print table -->
         	@if ($errors->any())
         	<div class="alert alert-danger alert-block">
            	<button type="button" class="close" data-dismiss="alert">×</button> 
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
         	</div>
         	@endif

			@if ($message = Session::get('info'))
			<div class="alert alert-primary alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			@if ($message = Session::get('danger'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

         	<section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php if($user->role == "super admin"): ?>
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                             </div>
                                             <select name="package_id" id="package_id" class="form-control">
                                                <option value="" selected="">Select Package</option>
                                                @foreach($package as $item)
                                                <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Transaction Date</th>
                                       <th>Transaction ID</th>
                                       <th>Service</th>
                                       <th>Operator Name</th>
                                       <th>Mobile</th>
                                       <th>Customer Name</th>
                                       <th>Bill Amount</th>
                                       <th>Commission</th>
                                       <?php if($user->role == "super admin"): ?>
                                       <th>Admin Commission</th>
                                       <?php endif; ?>
                                       <th>Current Balance</th>
                                       <th>Status</th>
                                       <th>Complaint ID</th>
                                       <th>Complaint Status</th>
                                       <th>View Receipt</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($bbpstransaction as $item)
                                    <tr>
                                      @if(Auth::user()->role == 'super admin') 
                                         <td><a href="{{route('view-member', $item->user_id)}}">{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}<span> ({{$item->umobile}})</span></a></td>
                                        @else 
                                         <td>{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}<span> ({{$item->umobile}})</span></td>
                                        @endif
                                       <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                       <td>{{$item->dms_txn_id}}</td>
                                       <td>{{$item->biller_cat_name}}</td>
                                       <td>{{$item->biller_name}}</td>
                                       <td>{{$item->cust_mobile}}</td>
                                       <td>
                                          <?php if($item->cus_name == "NA"): ?>
                                          <p>-</p>
                                          <?php else: ?>
                                          <p>{{$item->cus_name}}</p>
                                          <?php endif ?>
                                       </td>
                                       <td> {{number_format($item->bill_amount , 3)}}</td>
                                       <td> {{number_format($item->commission , 3)}}</td>
                                       <?php if($user->role == "super admin"): ?>
                                       <td> {{number_format($item->admin_commission , 3)}}</td>
                                       <?php endif; ?>
                                       <td> {{number_format($item->current_balance , 3)}}</td>
                                       <td>{{$item->payment_status}} <br />
                                          	@if($item->payment_status == "Pending" || $item->status_changed == 1)
                                          	<button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" href="/searchtransactionsInfo?txn_ref_id={{ $item->bbps_txnid }}" data-target="#getPendingView{{$item->bbps_txnid}}"><i class="fa fa-eye"></i></button>
                                          		{{-- time difference --}}
                                          		@php
                                          			$time_diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($item->created_at)) / 60;
                                          			//echo strtotime(date('Y-m-d H:i:s')).",".strtotime($item->created_at).",".$time_diff;
                                          		@endphp
                                          	@endif
                                       </td>
                                       <td>
                                          <?php if ($item->complaint_id == NULL): ?> 
                                            <p>Not Registered Yet!</p>
                                          <?php else: ?>
                                            {{$item->complaint_id}}
                                          <?php endif ?>
                                       </td>
                                       <td>
                                          <?php if ($item->complaint_status == NULL): ?> 
                                            <p>-</p>
                                          <?php else: ?>
                                            {{$item->complaint_status}}
                                          <?php endif ?>
                                       </td>
                                       <td>
                                        <button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" id="view_receipt" data-target="#getsuccessmodel{{$item->id}}">View</button>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- search transactions table-->
                     <div class="card-content" id="searchtxn">
                     </div>
                     <!-- search transactions table-->
                     @foreach($bbpstransaction as $item)
                     <div class="modal fade" id="getPendingView{{$item->bbps_txnid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 50%;">
                           <div class="modal-content">
                              <div class="modal-header" style="background: linear-gradient(to right, #7767F0, #cc2FF4);">
                                 <h4 class="modal-title" style=" color: white;" id="myModalLabel">Transaction Details</h4>
                              </div> 
                              <div class="modal-body print-container">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="transaction_result{{$item->bbps_txnid}}"></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="modal-footer" style="justify-content: center;">
                                 <h5>Thanks for your Business!</h5>
                                 <button type="button" class="btn btn-secondary" onclick="location.reload(true);" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                              </div>
                           </div>
                        </div>
                     </div>

                     
                     <div class="modal fade" id="getsuccessmodel{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 50%;">
                           <div class="modal-content">
                              <div class="modal-header" style="background: linear-gradient(to right, #7767F0, #cc2FF4);">
                                 <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                              </div> 
                              <div class="modal-body  print-container" id="getCode" >
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="content" style="text-align: center; margin-left: 0px;">
                                              <div id="invoice-template" class="card-body">
                                                  <div id="invoice-company-details" class="row">
                                                      <div class="col-sm-6 col-12 text-left pt-1">
                                                          <div class="media pt-1">
                                                              <img src="{{asset('images/Be Assured.png')}}" style="width: 120px;">
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-6 col-12 text-right">
                                                          <h1>Payment Details</h1>
                                                          <div class="invoice-details mt-2">
                                                              <h6>Biller Name</h6>
                                                              <p>{{$item->biller_name}}</p>
                                                              <h6>Biller ID</h6>
                                                              <p>{{$item->biller_id}}</p>
                                                              <h6>Bill Number</h6>
                                                              <?php if($item->bill_number == NULL): ?>
                                                                <p>-</p>
                                                              <?php else: ?>
                                                                <p>{{$item->bill_number}}</p>
                                                              <?php endif ?>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div id="invoice-customer-details" class="row pt-2 ">
                                                      <div class="col-sm-6 col-12 text-left">
                                                          <div class="recipient-info my-2">
                                                              <h6>Customer Name</h6>
                                                              <p>{{$item->cus_name}}</p>
                                                              <h6>Mobile Number</h6>
                                                              <p>{{$item->cust_mobile}}</p>
                                                              <h6>Txn Date & Time</h6>
                                                              <p><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span><h5>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</h5></p>
                                                              <h6>BBPS Txn ID</h6>
                                                              <p><h5>{{$item->bbps_txnid}}</h5></p>
                                                              <h6>Transaction Status</h6>
                                                              <p><h5>{{$item->payment_status}}</h5></p>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-6 col-12 text-right">
                                                          <div class="company-info my-2">
                                                              <h6>Bill Date</h6>
                                                              <?php if($item->bill_date == NULL): ?>
                                                                <p>-</p>
                                                              <?php else: ?>
                                                                <p>{{$item->bill_date}}</p>
                                                              <?php endif ?>
                                                              <h6>Due Date</h6>
                                                              <?php if($item->bill_due_date == NULL): ?>
                                                                <p>-</p>
                                                              <?php else: ?>
                                                                <p>{{$item->bill_due_date}}</p>
                                                              <?php endif ?>
                                                              <h6>Bill Period</h6>
                                                              <?php if($item->bill_period == NULL): ?>
                                                                <p>-</p>
                                                              <?php else: ?>
                                                                <p>{{$item->bill_period}}</p>
                                                              <?php endif ?>
                                                              <h6>Transaction Id</h6>
                                                              <p>{{$item->dms_txn_id}}</p>
                                                              <h6>Approval Number</h6>
                                                              <p>{{$item->approval_no}}</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                      <div class="row">
                                                          <div class="table-responsive col-12">
                                                              <table class="table table-borderless">
                                                                  <tbody>
                                                                      <tr>
                                                                          <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                          <td class="text-right"><h5>{{number_format($item->bill_amount , 3)}}</h5></td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                                          <td class="text-right" >{{number_format($item->ccf , 3)}}</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td class="text-left"><b>Payment Mode</b></td>
                                                                          <td class="text-right" id="Resppayment_mode">{{$item->payment_mode}}</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <th class="text-right">Total Amount (<i class="fa fa-inr"></i>)</th>
                                                                          <td class="text-right"><h5>{{number_format($item->bill_amount , 3)}}</h5></td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="modal-footer" style="justify-content: center;">
                                 <h5>Thanks for your Business!</h5>
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')

@foreach($bbpstransaction as $item)
<script type="text/javascript">
   // Fill modal with content from link href
   $("#getPendingView{{$item->bbps_txnid}}").on("show.bs.modal", function(e) {
       var link = $(e.relatedTarget);
       $(".transaction_result{{$item->bbps_txnid}}").load(link.attr("href"));
   });
</script>
@endforeach

<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
       var filter_date = $('#filter_date').val();
       var package_id = $('#package_id').val();
       var date_arr = filter_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filterbbpstxn", 
           data : {'start_date':start_date,'end_date':end_date,'package_id':package_id,},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                 var Result = data;
                  $("#alltxn").empty();
                  $("#searchtxn").empty();
                  $("#transactionresult").empty();

                  <?php if($user->role == "super admin"): ?>

                  $("#searchtxn").append(' <div class="card-body card-dashboard"><div class="table-responsive"><table id="example1" class="table table-striped table-bordered ">   <thead>      <tr>  <th>Name</th><th>Transaction Date</th><th>Transaction ID</th><th>Service</th><th>Operator Name</th><th>Mobile</th><th>Customer Name</th><th>Bill Amount</th><th>Commission</th><th>Admin Commission</th><th>Current Balance</th><th>Status</th><th>Complaint ID</th><th>Complaint Status</th> <th>View Receipt</th>       </tr>   </thead>   <tbody id="transactionresult">        </tbody></table></div></div>');
               <?php else: ?>
                  $("#searchtxn").append(' <div class="card-body card-dashboard"><div class="table-responsive"><table id="example1" class="table table-striped table-bordered ">   <thead>      <tr>  <th>Name</th><th>Transaction Date</th><th>Transaction ID</th><th>Service</th><th>Operator Name</th><th>Mobile</th><th>Customer Name</th><th>Bill Amount</th><th>Commission</th><th>Current Balance</th><th>Status</th><th>Check Status</th><th>Complaint ID</th><th>Complaint Status</th> <th>View Receipt</th>       </tr>   </thead>   <tbody id="transactionresult">        </tbody></table></div></div>');
               <?php endif; ?>
                 $.each(Result,function(key,value){
                  var user_role = '<?php echo Auth::user()->role; ?>';
                  var url = '{{ route("view-member", ":id") }}';
                  url = url.replace(':id', value.user_id);
                  // var maskedNumber = value.aadhar_number.slice(-4).padStart(value.aadhar_number.length, '*');
                     if (value.cus_name == "NA") {
                        var cust_name = "-";
                     }else{
                       var cust_name = value.cus_name;
                     }
                     if (value.commission == null) {
                       var commission = "-";
                     }else{
                       var commission = value.commission;
                     }
                     if (value.current_balance == null) {
                       var current_balance = "-";
                     }else{
                       var current_balance = value.current_balance;
                     }

                     if (value.complaint_id == null) {
                       var complaint_id = "Not Registered Yet!";
                     }else{
                       var complaint_id = value.complaint_id;
                     }

                     if (value.complaint_status == null) {
                       var complaint_status = "-";
                     }else{
                       var complaint_status = value.complaint_status;
                     }

                     var tr =''; 
                    if (user_role == 'super admin') {
                      tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+' <span> ('+value.umobile+')</span></a></td>';
                    }else{
                      tr += '<tr><td>'+value.first_name+' '+value.last_name+'<span> ('+value.umobile+')</span></td>';
                    }

                    <?php if($user->role == "super admin"): ?>

                    tr += '<td >'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td >'+value.dms_txn_id+'</td><td>'+value.biller_cat_name+'</td><td >'+value.biller_name+'</td><td >'+value.cust_mobile+'</td><td>'+cust_name+'</td><td> '+parseFloat(value.bill_amount).toFixed(3)+'</td><td > '+parseFloat(value.commission).toFixed(3)+'</td><td > '+parseFloat(value.admin_commission).toFixed(3)+'</td><td > '+parseFloat(value.current_balance).toFixed(3)+'</td><td>'+value.payment_status+'<br />';
                 <?php else: ?>
                  tr += '<td >'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td >'+value.dms_txn_id+'</td><td>'+value.biller_cat_name+'</td><td >'+value.biller_name+'</td><td >'+value.cust_mobile+'</td><td>'+cust_name+'</td><td> '+parseFloat(value.bill_amount).toFixed(3)+'</td><td > '+parseFloat(value.commission).toFixed(3)+'</td><td > '+parseFloat(value.current_balance).toFixed(3)+'</td><td>'+value.payment_status+'<br />';
                     <?php endif; ?>
                    if(value.payment_status == "Pending" || value.status_changed == 1)
                    {
                    	tr += '<button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal"                             href="/searchtransactionsInfo?txn_ref_id='+value.bbps_txnid+'" data-target="#getPendingView'+value.bbps_txnid+'"><i class="fa fa-eye"></i></button>';
                    }
                	tr += '</td><td >'+complaint_id+'</td><td >'+complaint_status+'</td><td ><button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" id="view_receipt" data-target="#getsuccessmodel'+value.id+'">View</button></td></tr>';
                      // alert(tr);
                  $('#transactionresult').append(tr);

                    // $("#transactionresult").append(' <tr><td >'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td >'+value.dms_txn_id+'</td><td >'+value.bbps_txnid+'</td><td >'+value.biller_cat_name+'</td><td >'+value.biller_name+'</td><td >'+value.cust_mobile+'</td><td >'+cust_name+'</td><td > '+parseFloat(value.bill_amount).toFixed(3)+'</td><td > '+parseFloat(value.commission).toFixed(3)+'</td><td > '+parseFloat(value.current_balance).toFixed(3)+'</td><td >'+value.payment_status+'</td><td >'+complaint_id+'</td><td >'+complaint_status+'</td><td ><button type="button" style="margin-left: auto;" class="btn btn-primary" data-backdrop="false" data-title="View Receipt" data-toggle="modal" id="view_receipt" data-target="#getsuccessmodel'+value.id+'">View</button></td></tr>');
                 });
                }
   
                $('#example1').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        }); 
        });
      });
</script>

@endsection