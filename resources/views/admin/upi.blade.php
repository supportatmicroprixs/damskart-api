@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   .my_qr_code_design{
      /*margin-top: 1px;*/
      border-radius: 10px;
      width: 220px;
      background: white;
      margin: auto;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">
   <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
         <div class="row breadcrumbs-top">
            <div class="col-12">
               <h2 class="content-header-title float-left mb-0">UPI Cash</h2>
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                     </li>
                     <li class="breadcrumb-item"><a href="#">Service</a>
                     </li>
                     <li class="breadcrumb-item active">UPI Cash
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="content-body">
      <div class="row">
           <div class="col-12">
               <p id="resp_message"></p>
           </div>
       </div>
      <!-- Nav Centered And Nav End Starts -->
      @if ($errors->any())
      <div  class="alert alert-danger alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button> 
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      @endif
      @if ($message = Session::get('info'))
      <div class="alert alert-primary alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button> 
         <strong>{{ $message }}</strong>
      </div>
      @endif
      @if ($message = Session::get('danger'))
      <div class="alert alert-danger alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button> 
         <strong>{{ $message }}</strong>
      </div>
      @endif
      <section id="nav-tabs-centered">
         <div class="row">
            <div class="col-sm-12">
               <div class="card overflow-hidden">
                  <div class="divider divider-center" style="margin:1rem 0 0 0;">
                     <div class="divider-text">
                        <img src="{{ asset('images/upi_icon.png') }}" width="40" alt="UPI">
                     </div>
                  </div>
                  <div class="card-content">
                     <div class="card-body">
                        <ul class="nav nav-tabs justify-content-center" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#home-center" aria-controls="home-center" role="tab" aria-selected="true">Generate QR</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#service-center" aria-controls="service-center" role="tab" aria-selected="false">SEND REQUEST</a>
                           </li>
                           <!--  <li class="nav-item">
                              <a class="nav-link disabled">
                                  Disabled
                              </a>
                              </li> -->
                           <li class="nav-item">
                              <a class="nav-link" id="#user_qr"  onclick="generateMYQR()"  data-toggle="tab" href="#account-center" aria-controls="account-center" role="tab" aria-selected="false">My QR</a>
                           </li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="home-center" aria-labelledby="home-tab-center" role="tabpanel">
                              <section id="multiple-column-form">
                                 <div class="row match-height">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="form-body">
                                                   <div class="row">
                                                      <div class="col-6">
                                                         <div class="form-group">
                                                            <label for="contact-info-icon">Mobile</label>
                                                            <div class="position-relative has-icon-left">
                                                               <input type="number" class="form-control" pattern="[6789][0-9]{9}" maxlength="10" name="mobile" id="generate_mobile" onkeyup="generatebuttonDetected()"  placeholder="Enter mobile number">
                                                               <div class="form-control-position">
                                                                  <i class="feather icon-smartphone"></i>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-6">
                                                         <div class="form-group">
                                                            <label for="contact-info-icon">Name</label>
                                                            <div class="position-relative has-icon-left">
                                                               <input type="text" class="form-control" name="name" onkeyup="generatebuttonDetected()" id="generate_name" placeholder="Enter name">
                                                               <div class="form-control-position">
                                                                  <i class="feather icon-user"></i>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-6">
                                                         <div class="form-group d-flex align-items-center pt-md-2">
                                                            <!-- <label class="mr-2">Payment Type :</label> -->
                                                            <div class="c-inputs-stacked">
                                                               <div class="d-inline-block mr-2">
                                                                  <div class="vs-checkbox-con vs-checkbox-primary">
                                                                     <input type="radio" name="check_paytype"  onchange="generatebuttonDetected()" id="check_generate_qr" value="check_generate_qr">
                                                                     <span class="vs-checkbox">
                                                                     <span class="vs-checkbox--check">
                                                                     <i class="vs-icon feather icon-check"></i>
                                                                     </span>
                                                                     </span>
                                                                     <span class="">Generate QR</span>
                                                                  </div>
                                                               </div>
                                                               <div class="d-inline-block mr-2" >
                                                                  <div class="vs-checkbox-con vs-checkbox-primary">
                                                                     <input type="radio" name="check_paytype"  onchange="generatebuttonDetected()" id="check_amount" value="check_amount">
                                                                     <span class="vs-checkbox">
                                                                     <span class="vs-checkbox--check">
                                                                     <i class="vs-icon feather icon-check"></i>
                                                                     </span>
                                                                     </span>
                                                                     <span class="">Amount</span>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-6">
                                                         <div class="form-group">
                                                            <label for="contact-info-icon">Amount</label>
                                                            <div class="position-relative has-icon-left">
                                                               <input type="number"  class="form-control" name="amount" id="generate_amount" min="1" max="100000" onkeyup="generatebuttonDetected()" placeholder="Enter amount" disabled>
                                                               <div class="form-control-position">
                                                                  <i class="fa fa-inr"></i>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-6">
                                                         <div class="form-group">
                                                            <div id="generate_response">
                                                            </div>
                                                         </div>
                                                         <div class="col-12">
                                                            <button  id="generate_button" class="btn btn-primary mr-1 mb-1" disabled>Submit</button>
                                                            <button type="reset" id="reset_button" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                         </div>
                                                         <div id="qrString"></div>
                                                         <br>
                                                         <p id="qr-result"></p>
                                                         <canvas id="qr-code"></canvas>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                              </section>
                              </div>
                              <div class="tab-pane " id="service-center" aria-labelledby="service-tab-center" role="tabpanel">
                                 <section id="multiple-column-form">
                                    <div class="row match-height">
                                       <div class="col-12">
                                          <div class="card">
                                             <div class="card-content">
                                                <div class="card-body">
                                                   <div class="form-body">
                                                      <div class="row">
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                               <label for="contact-info-icon">UPI Id</label>
                                                               <div class="position-relative has-icon-left">
                                                                  <input type="text" class="form-control" name="upi_id" onkeyup="requestbuttonDetected()" id="request_upi_id" placeholder="Enter UPI ID">
                                                                  <div class="form-control-position">
                                                                     <i class="feather icon-edit-2"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                               <label for="contact-info-icon">Name</label>
                                                               <div class="position-relative has-icon-left">
                                                                  <input type="text"  class="form-control" name="name" onkeyup="requestbuttonDetected()" id="request_name" placeholder="Enter name">
                                                                  <div class="form-control-position">
                                                                     <i class="feather icon-user"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                               <label for="contact-info-icon">Mobile</label>
                                                               <div class="position-relative has-icon-left">
                                                                  <input type="number" class="form-control" pattern="[6789][0-9]{9}" maxlength="10" name="mobile" onkeyup="requestbuttonDetected()" id="request_mobile" placeholder="Enter mobile number">
                                                                  <div class="form-control-position">
                                                                     <i class="feather icon-smartphone"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                               <label for="contact-info-icon">Amount</label>
                                                               <div class="position-relative has-icon-left">
                                                                  <input type="number"  class="form-control" name="amount" onkeyup="requestbuttonDetected()" id="request_amount" min="1" placeholder="Enter amount">
                                                                  <div class="form-control-position">
                                                                     <i class="feather icon-dollar-sign"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                               <div id="sendrequest_response">
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-12">
                                                            <button  id="request_button" class="btn btn-primary mr-1 mb-1" disabled>Submit</button>
                                                            <button type="reset" id="request_reset_button" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                              <div class="tab-pane" id="account-center" aria-labelledby="user_qr" role="tabpanel">
                                 <section id="basic-examples">
                                    <div class="row match-height">
                                       <div class="offset-4 col-xl-4 col-md-6 col-sm-12 profile-card-3">
                                          <div class="card" style="border: 3px solid #FF7600;">
                                             <div class="card-header" style="background:linear-gradient(to right, #e96c00ed, #df8a40e6);">
                                                <div style="margin: auto;">
                                                   <img class="img-fluid" src="{{asset('images/Damskart Logo.png')}}" alt="Profile">
                                                </div>
                                             </div>
                                             <div class="card-content">
                                                <div class="card-body text-center" style="color: white;background: linear-gradient(to right, #e96c00ed, #df8a40e6);font-size: large;">
                                                   <input id="myqr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr=MNO{{Auth::user()->mobile}}&am=&cu=INR&mc=5411" placeholder="Text to generate QR code"/>
                                                   <div id="my_qr"></div>
                                                   <b style="font-size: x-large;"><p id="myqr-title"></p></b>
                                                   <b><p id="myqr-description"></p></b>
                                                   <div id="myqrString"></div>
                                                   <div class="my_qr_code_design">
                                                      <canvas id="myqr-code" style="margin-top: 9px;"></canvas>
                                                   </div>
                                                   <br>
                                                </div>
                                             </div>
                                             <div class="card-footer">
                                                <hr style="border-top: 1px solid black;"><br>
                                                <hr style="border-top: 1px solid black;" class="pt-1">
                                                <b class="d-flex justify-content-center"><h3 id="myqr-logo-title" style="font-size: 2.35rem;font-style: italic;"></h3></b>
                                                <div class="d-flex justify-content-center  pt-2">
                                                   <div class="icon-dislike mr-2 mt-1">
                                                      <img class="img-fluid" src="{{asset('images/Upi_qr_icon.png')}}" height="40" width="100" alt="Profile">
                                                   </div>
                                                   <div class="icon-like mr-2" style="margin-top: 1.5rem !important;">
                                                      <img class="img-fluid" src="{{asset('images/bhim_upi_512x512.png')}}" height="40" width="100" alt="Profile">
                                                   </div>
                                                   <div class="icon-dislike mr-2" style="margin-top: 1.5rem !important;">
                                                      <img class="img-fluid" src="{{asset('images/amazon_pay_qr_icon.png')}}" height="40" width="100" alt="Profile">
                                                   </div>
                                                   <div class="icon-dislike " style="margin-top: 1.2rem !important;">
                                                      <img class="img-fluid" src="{{asset('images/paytm_pay.png')}}" height="40" width="100" alt="Profile">
                                                   </div>
                                                </div>
                                                <div class="d-flex justify-content-center pt-2">
                                                   <div class="icon-comment mr-2">
                                                      <img class="img-fluid" src="{{asset('images/Google-Pay-Logo-Icon.png')}}" height="40" width="60" alt="Profile">
                                                   </div>
                                                   <div class="icon-comment mr-2" style="margin-top: 1rem !important;">
                                                      <img class="img-fluid" src="{{asset('images/whatsapp_pay.png')}}" height="40" width="60" alt="Profile">
                                                   </div>
                                                   <div class="icon-comment mr-2 " style="margin-top: 1.5rem !important;">
                                                      <img class="img-fluid" src="{{asset('images/recharge_1.png')}}" height="40" width="100" alt="Profile">
                                                   </div>
                                                   <div class="icon-dislike">
                                                      <img class="img-fluid" src="{{asset('images/phone-pe.png')}}" height="40" width="60" alt="Profile">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </section>
      <div class="divWaiting" id="Dv_Loader" style="display: none;">
      <button class="btn btn-primary mb-1 round" type="button" >
      <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
      &nbsp;Loading... 
      </button>
      </div>
      <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>
<script>
   var qr;
   (function() {
         qr = new QRious({
         element: document.getElementById('qr-code'),
         size: 0,
         value: 'https://damskart.com'
     });
   })();
         
   function generateQRCode() {
       var qrtext = document.getElementById("qr-text").value;
       document.getElementById("qr-result").innerHTML = "Scan & Pay:";
       // alert(qrtext);
       qr.set({
           foreground: 'black',
           size: 200,
           value: qrtext
       });
   }
   
</script>
<script>
   var myqr;
   (function() {
         myqr = new QRious({
         element: document.getElementById('myqr-code'),
         size: 0,
         value: 'https://damskart.com'
     });
   })();
         
    function generateMYQR() {
       var myqrtext = document.getElementById("myqr-text").value;
       document.getElementById("myqr-description").innerHTML = "Pay using any UPI App";
       document.getElementById("myqr-title").innerHTML = "Damskart Business for Cash";
       document.getElementById("myqr-logo-title").innerHTML = "Accepted Here";
       // alert(myqrtext);
       myqr.set({
           foreground: 'black',
           size: 200,
           value: myqrtext
       });
   }
   
</script>
<script>
   $(document).ready(function() {
    $("#check_amount").click(function() {
        $("#generate_amount").attr("disabled", false);
     });
   
    $("#check_generate_qr").click(function() {
        $("#generate_amount").attr("disabled", true);
     });
   });
</script>
<script>
   document.getElementById('pay_type').onchange = function(){
       document.getElementById('price').disabled = (this.value === 'free');
       document.getElementById('display_price').disabled = (this.value === 'free');
   }
</script>
<script type="text/javascript">
   function generatebuttonDetected(){
     var gen_mobile = $('#generate_mobile').val();
     var gen_name = $('#generate_name').val();
     var gen_amount = $('#generate_amount').val();
     // var check_generate_qr = $('#check_generate_qr').val();
     // var check_amount = $('#check_amount').val();
         if (gen_mobile.length > 0 && gen_name.length > 0 ) {
           $('#generate_button').removeAttr('disabled');
         } else {
           $('#generate_button').attr('disabled', 'disabled');
         }
   }
</script>
<script type="text/javascript">
   function requestbuttonDetected(){
     var reqmobile = $('#request_mobile').val();
     var requpi_id = $('#request_upi_id').val();
     var reqname = $('#request_name').val();
     var reqamount = $('#request_amount').val();
         if (reqmobile.length > 0 && requpi_id.length > 0 && reqname.length > 0 && reqamount.length > 0) {
           $('#request_button').removeAttr('disabled');
         } else {
           $('#request_button').attr('disabled', 'disabled');
         }
   }
</script>
<script type="text/javascript"> 
   function myqr_button(){
       $("#my_qr").append('<input id="myqr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr=MNO9799594939&am=&cu=INR&mc=5411" placeholder="Text to generate QR code"/>'); 
    
   }
    
</script>
<script type="text/javascript">
   $(document).ready(function(){
       $('#generate_button').click(function(){
         var SITEURL = '{{URL::to('')}}';
          var generatemobile = $('#generate_mobile').val();
          var generatename = $('#generate_name').val();
          var generateamount = $('#generate_amount').val();
           $('#Dv_Loader').show();
           $.ajax({
              type:"get",
              url:"/upigenerateqr", 
              data : {'generatemobile':generatemobile,'generatename':generatename,'generateamount':generateamount},
              dataType: "json",
              success:function(data)
              {   
                 $('#Dv_Loader').hide();
                 if (data.permission_code == "PER111") {
                  $("#resp_message").empty();
                  $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                    
                    setTimeout(function () {
                      window.location.href = SITEURL + '/index';
                    }, 3000);
                 }else{
                    var Result = data.upigenerateQr;
                    var qramount = data.amount;
                    var generate_type = data.generate_type;
                    var dms_txn_id = data.dms_txn_id;
                    var message = Result.message;
                    if (Result.response == "0") {
                        var merchantTranId = Result.merchantTranId;
                        // window.location.href = 'upi://pay?pa=uatmer018@icici&pn=DamskartPay&tr='+Result.refId+'&am='+qramount+'&cu=INR&mc=5411';
                        $("#generate_button").hide();
                        $("#reset_button").hide();
                        // $("#qrString").empty();
                        // $("#qrString").empty();
                        // $("#qr-result").empty();
                        // $("#qr-code").empty();
                        // $("#qr-text").empty();
                        // $("#generate_response").append('<p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">TxnId : '+dms_txn_id+'</p><p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">Txn Status : '+message+'</p>');
                        if (generate_type == "generate_amount_qr") {
                           $("#qrString").append('<input id="qr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr='+Result.refId+'&am='+qramount+'&cu=INR&mc=5411" placeholder="Text to generate QR code"/><button class="qr-btn btn btn-success" onclick="generateQRCode()">Click To Generate QR</button>'); 
                        }else{
                           $("#qrString").append('<input id="qr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr='+Result.refId+'&am=&cu=INR&mc=5411" placeholder="Text to generate QR code"/><button class="qr-btn btn btn-success" onclick="generateQRCode()">Click To Generate QR</button>'); 
                        }
                    }else{
                        $("#generate_response").empty();
                        $("#generate_response").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+message+'</p>'); 
                    }
                  }
              }
           });
           });
       });
</script>
<script type="text/javascript">
   $(document).ready(function(){
       $('#request_button').click(function(){
         var SITEURL = '{{URL::to('')}}';
          var requestmobile = $('#request_mobile').val();
          var requestupi_id = $('#request_upi_id').val();
          var requestname = $('#request_name').val();
          var requestamount = $('#request_amount').val();
           $('#Dv_Loader').show();
           $.ajax({
              type:"get",
              url:"/upisendPayRequest", 
              data : {'requestmobile':requestmobile,'requestupi_id':requestupi_id,'requestname':requestname,'requestamount':requestamount},
              dataType: "json",
              success:function(data)
              {   
                 $('#Dv_Loader').hide();
                 if (data.permission_code == "PER111") {
                  $("#resp_message").empty();
                  $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                    
                    setTimeout(function () {
                      window.location.href = SITEURL + '/index';
                    }, 3000);
                 }else{
                    var Result = data.upisendpayrequest;
                    var reqamount = data.amount;
                    var rdms_txn_id = data.dms_txn_id;
                    var message = Result.message;
                    if (Result.response == "92") {
                        var merchantTranId = Result.merchantTranId;
                        $("#request_button").hide();
                        $("#request_reset_button").hide();
                        $("#sendrequest_response").empty();
                        $("#sendrequest_response").append('<p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">'+message+'</p>');
                    }else{ 
                        $("#sendrequest_response").empty();
                        $("#sendrequest_response").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+message+'</p>'); 
                    }
                 }
              }
           });
           });
       });
</script>
@endsection