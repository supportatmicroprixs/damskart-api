@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   #doc_img{
      width: 20%;border-radius: 3px;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Member Profile</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Member</a>
                        </li>
                        <li class="breadcrumb-item active"> Profile
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @if ($errors->any())
      <div  class="alert alert-danger alert-block">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      @endif
      @if ($message = Session::get('info'))
      <div class="alert alert-primary alert-block">
         <strong>{{ $message }}</strong>
      </div>
      @endif 
      @if ($message = Session::get('danger'))
      <div class="alert alert-danger alert-block">
         <strong>{{ $message }}</strong>
      </div>
      @endif
      <div class="content-body">
         <!-- account setting page start -->
         <section id="page-account-settings">
            <div class="row">
               <!-- left menu section -->
               <div class="col-md-12 mb-12 mb-md-0">
                  	<ul class="nav nav-tabs">
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75 active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                        <i class="feather icon-user mr-50 font-medium-3"></i>
                        General
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                        <i class="feather icon-file mr-50 font-medium-3"></i>
                        Documents
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-info" data-toggle="pill" href="#account-vertical-info" aria-expanded="false">
                        <i class="fa fa-users mr-50 font-medium-3"></i>
                        Downlines
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-social" data-toggle="pill" href="#account-vertical-social" aria-expanded="false">
                        <i class="fa fa-dropbox mr-50 font-medium-3"></i>
                        Packages
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-connections" data-toggle="pill" href="#account-vertical-connections" aria-expanded="false">
                        <i class="fa fa-th mr-50 font-medium-3"></i>
                        Services
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-notifications" data-toggle="pill" href="#account-vertical-notifications" aria-expanded="false">
                        <i class="fa fa-expand mr-50 font-medium-3"></i>
                        Transactions
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="account-pill-owner" data-toggle="pill" href="#account-vertical-owner" aria-expanded="false">
                        <i class="fa fa-street-view mr-50 font-medium-3"></i>
                        Owner
                        </a>
                     </li>
                  	</ul>

                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">
                         <div class="row">
                            <div class="col-6">
                               <div class="media">
                                  <a href="javascript: void(0);">
                                  <img style="margin: 10px;" src="{{asset('/').$user->profile_pic}}" class="rounded mr-75" alt="profile image" height="64" width="64">
                                  </a>
                                  <div class="media-body mt-75">
                                     <p>{{ucfirst($user->first_name).' '.ucfirst($user->last_name)}}
                                     <br>
                                     <small>
                                        <?php if($user->role == "super admin"): ?>
                                        <small>Super Admin</small>
                                     <?php endif ?>
                                     <?php if($user->role == "sdistributor"): ?>
                                        <small>Super Distributor</small>
                                     <?php endif ?>
                                     <?php if($user->role == "mdistributor"): ?>
                                        <small>Master Distributor</small>
                                     <?php endif ?>
                                     <?php if($user->role == "distributor"): ?>
                                        <small>Distributor</small>
                                     <?php endif ?>
                                     <?php if($user->role == "retailer"): ?>
                                        <small>Retailer</small>
                                     <?php endif ?>
                                     </small>
                                     </p>
                                  </div>
                               </div>
                            </div>
                            <div class="col-6" style="border-left: 1px inset;">
                                           
                               <div class="media" style="float: right;">
                                  @foreach($owner as $item)
                                     <?php if($user->created_by == $item->id): ?>
                                     <img style="margin: auto;" src="{{asset('/').$item->profile_pic}}" class="rounded mr-75" alt="Owner image" height="64" width="64">
                                        <p class="text-muted ml-75 mt-50">
                                        <?php if(Auth::user()->role == "super admin"): ?>
                                           <small><a href="{{route('view-member', $item->id)}}">{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</a></small>
                                        <?php else: ?>
                                           <small><a href="{{route('profile')}}">{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</a></small>
                                        <?php endif ?>
                                           <br>
                                        <small>{{ucfirst($item->mobile)}}</small>
                                        <br>
                                        <label class="btn btn-sm btn-success cursor-pointer" for="account-upload">Owner</label>
                                        </p>
                                     <?php endif ?>
                                  @endforeach
                               </div>
                            </div>
                         </div>
                         <hr>
                         <div class="row">
                            <div class="col-12" style="text-align: right;">
                               <?php $active = '#active'.$user->id; ?>
                             <?php $inactive = '#inactive'.$user->id; ?>
                             <?php if($user->status == 1): ?>
                               <button  type="button"   onclick="updateMemberStatus(0,<?= $user->id ;?>,'<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$user->id}}"  value="1" class="btn btn-success ">Active</button>
                               <button  type="button"   onclick="updateMemberStatus(1,<?= $user->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$user->id}}"  value="0" class="btn btn-danger " style="display: none;">Inactive</button>
                             <?php elseif($user->status == 0): ?>
                               <button  type="button" onclick="updateMemberStatus(1,<?= $user->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$user->id}}"  value="0" class="btn btn-danger ">Inactive</button>
                               <button  type="button" onclick="updateMemberStatus(0,<?= $user->id ;?>, '<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$user->id}}"  value="1" class="btn btn-success " style="display: none;">Active</button>
                             <?php endif ?>
                             </div>
                          </div>
                         <form  action="{{route('updatemember',$user->id)}}" method="POST" enctype="multipart/form-data" novalidate>
                            {{csrf_field()}}
                            <div class="row">
                               <div class="col-12">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-username">First Name</label>
                                        <input type="text" class="form-control" id="account-username" placeholder="First Name" value="{{ucfirst($user->first_name)}}" name="first_name" required data-validation-required-message="This username field is required">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-name">Last Name</label>
                                        <input type="text" class="form-control" id="account-name" placeholder="Last Name" value="{{ucfirst($user->last_name)}}" name="last_name" required data-validation-required-message="This name field is required">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-e-mail">E-mail</label>
                                        <input type="email" class="form-control" id="account-e-mail" name="email" placeholder="Email" value="{{$user->email}}" required data-validation-required-message="This email field is required">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <label for="account-company">Aadhar Card</label>
                                     <input type="text" value="{{$user->aadhar_number}}" name="aadhar_number" class="form-control" id="account-company" placeholder="Aadhar Card">
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <label for="account-company">Pan Card</label>
                                     <input type="text" value="{{$user->pan_number}}" name="pan_number" class="form-control" id="account-company" placeholder="Pan Card">
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <label for="account-company">Address</label>
                                     <textarea class="form-control" id="accountTextarea" rows="3" value="{{$user->address}}" name="address" placeholder="Company Address">{{$user->address}}</textarea>
                                  </div>
                               </div>
                               <div class="col-12">
                                  <hr>
                                  <div class="form-group">
                                     <label for="account-company">Company Name</label>
                                     <input type="text" class="form-control" id="account-company" name="company_name" value="{{$user->company_name}}" placeholder="Company name">
                                  </div>
                               </div>
                               <div class="col-12">
                                  <div class="form-group">
                                     <label for="account-company">Company Address</label>
                                     <textarea class="form-control" id="accountTextarea" rows="3" value="{{$user->company_address}}" name="company_address" placeholder="Company Address">{{$user->company_address}}</textarea>
                                  </div>
                               </div>
                               <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                  <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                  changes</button>
                                  <button type="reset" class="btn btn-outline-warning">Cancel</button>
                               </div>
                            </div>
                         </form>
                    		 </div>
                 			</div>
                 		</div>
                      </div>
                      <div class="tab-pane fade " id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">
                         <form action="{{route('updatedocument',$user->id)}}" method="POST" enctype="multipart/form-data" novalidate>
                            {{csrf_field()}}
                            <div class="row">
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-old-password">Profile Pic</label>
                                        <input type="file" class="form-control" id="account-company" name="profile_pic" alt="Profile Pic">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                   <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->profile_pic}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_Profile.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->profile_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile Image(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->profile_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile Image">
                                     <?php endif ?>
                                  </div>
                               </div>
                            </div>
                            <hr> 
                            <div class="row">
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-new-password">Aadhar card</label>
                                        <input type="file" class="form-control" id="account-company" name="aadhar_pic_front" alt="Aadhar Card">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->aadhar_pic_front}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_Aadharcard.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->aadhar_pic_front}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Aadhar card(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->aadhar_pic_front}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Aadhar card">
                                     <?php endif ?>
                                  </div>
                               </div>
                            </div>
                            <hr> 
                            <div class="row">
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-retype-new-password">Pan Card</label>
                                        <input type="file" class="form-control" id="account-company" name="pan_pic" id="Pan Card">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->pan_pic}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_Pancard.jpeg'}}">
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->pan_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pan Card(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->pan_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pan Card">
                                     <?php endif ?>
                                  </div>
                               </div>
                            </div>
                            <hr> 
                            <div class="row">
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-retype-new-password">Company Image</label>
                                        <input type="file" class="form-control" id="account-company" name="company_logo"  id="Company Image">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->company_logo}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_Company.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->company_logo}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Image(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->company_logo}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Image">
                                     <?php endif ?>
                                  </div>
                               </div>
                            </div>
                            <hr> 
                            <div class="row d-none">
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-retype-new-password">Account Passbook / Cancel Cheque </label>
                                        <input type="file" class="form-control" id="account-company" name="passbook_image" id="Pan Card">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->passbook_image}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_Cancelcheque.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->passbook_image}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Account Passbook/Cancel Cheque(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->passbook_image}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Account Passbook/Cancel Cheque">
                                     <?php endif ?>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-retype-new-password">10th Marksheet <small>(Optional)</small></label>
                                        <input type="file" class="form-control" id="account-company" name="marksheet_10"  id="Pan Card">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->marksheet_10}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_10Marksheet.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_10}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="10th Marksheet(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_10}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="10th Marksheet">
                                     <?php endif ?>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <div class="controls">
                                        <label for="account-retype-new-password">12th Marksheet</label>
                                        <input type="file" class="form-control" id="account-company" name="marksheet_12" value="{{$user->marksheet_12}}" id="Pan Card">
                                     </div>
                                  </div>
                               </div>
                               <div class="col-6">
                                  <div class="form-group">
                                     <?php if (Auth::user()->role == "super admin" || Auth::user()->role == "admin"):?>
                                        <a href="{{asset('/').$user->marksheet_12}}" download="{{ucfirst($user->first_name).'_'.ucfirst($user->last_name).'_12Marksheet.jpeg'}}"><img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_12}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="12th Marksheet(Click to download)"></a>
                                     <?php else: ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_12}}" alt="" data-toggle="tooltip" data-placement="top" title="" data-original-title="12th Marksheet">
                                     <?php endif ?>
                                  </div>
                               </div>
                            </div>
                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                               <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                               changes</button>
                               <button type="reset" class="btn btn-outline-warning">Cancel</button>
                            </div>
                         </form>
                     </div>
                 </div>
                 </div>
                      </div>
                      <div class="tab-pane fade" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
                      	<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">
                         <section id="basic-examples">
                            <div class="row match-height">
                               
                               @if($downlineCount > 0)
                                  <h2>Member List</h2>
                                  @foreach($downline as $item)
                                     <div class="col-xl-4 col-md-6 col-sm-12 profile-card-3">
                                         <div class="card">
                                             <div class="card-header mx-auto">
                                                 <div class="avatar avatar-xl">
                                                     <img class="img-fluid" src="{{asset('/').$item->profile_pic}}" alt="Profile">
                                                 </div>
                                             </div>
                                             <div class="card-content">
                                                 <div class="card-body text-center">
                                                     <h4>{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</h4>
                                                     <p class="">{{ucfirst($item->role)}}</p>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                  @endforeach
                               @endif
                            </div>
                            <div class="clearfix"></div>

                            <div class="row match-height">
                               
                               @if($downline1Count > 0)
                                  <h2>Referrals List</h2>
                                  @foreach($downline1 as $item)
                                     <div class="col-xl-4 col-md-6 col-sm-12 profile-card-3">
                                         <div class="card">
                                             <div class="card-header mx-auto">
                                                 <div class="avatar avatar-xl">
                                                     <img class="img-fluid" src="{{asset('/').$item->profile_pic}}" alt="Profile">
                                                 </div>
                                             </div>
                                             <div class="card-content">
                                                 <div class="card-body text-center">
                                                     <h4>{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</h4>
                                                     <p class="">{{ucfirst($item->role)}}</p>
                                                     @if($item->profile_status == 1)
                                                     Status : Success
                                                     @else
                                                     Status : E-KYC verification pending
                                                     @endif
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                  @endforeach
                               @endif
                            </div>
                         </section>
                     </div>
                 </div>
             </div>
                      </div>
                      <div class="tab-pane fade " id="account-vertical-social" role="tabpanel" aria-labelledby="account-pill-social" aria-expanded="false">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">
                         <form action="{{route('assignpackage',$user->id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-12">
                                     <div class="form-group">
                                         <label for="accountSelect">Assigned Package</label>
                                         <label class="btn btn-md btn-success ml-50 mb-50 mb-sm-0 cursor-pointer" for="account-upload">{{$user->package_name}}</label>
                                     </div>
                                 </div>
                                 <div class="col-12">
                                     <div class="form-group">
                                         <label for="accountSelect">Change Package</label>
                                         <select class="form-control" id="musicselect2" name="package_id">
                                             <option value="">Select Package</option>
                                              @foreach($package as $item)
                                              <option value="{{$item->id}}" <?php  if ($item->id ==$user->package_id) {echo ' selected';} ?>>{{$item->package_name}}</option>
                                              @endforeach
                                         </select>
                                     </div>
                                 </div>
                               <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                  <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                  changes</button>
                                  <button type="reset" class="btn btn-outline-warning">Cancel</button>
                               </div>
                            </div>
                         </form>
                     </div>
                 </div>
             </div>
                      </div>
                      <div class="tab-pane fade" id="account-vertical-connections" role="tabpanel" aria-labelledby="account-pill-connections" aria-expanded="false">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">
                           <div class="row">
                               <div class="col-12">
                                   <div class="card">
                                       <div class="card-header">
                                           <!-- <h4 class="card-title">Column selectors with Export and Print Options</h4> -->
                                       </div>
                                       <div class="card-content">
                                           <div class="card-body card-dashboard">
                                               <div class="table-responsive">
                                                   <table id="example2" class="table table-striped table-bordered">
                                                       <thead>
                                                           <tr>
                                                                 <th >Service Name</th>
                                                                <th >Action</th>
                                                           </tr>
                                                       </thead>
                                                       <tbody>
                                                             @foreach($service_list as $item)
                                                    <tr>
                                                       <td class="text-center">{{$item->service_name}}</td>
                                                       <td>
                                                        <?php $active = '#active'.$item->id; ?>
                                                        <?php $inactive = '#inactive'.$item->id; ?>
                                                        <?php if($item->status == 1): ?>
                                                          <button  type="button"   onclick="editMember(0,<?= $item->id ;?>,'<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success ">Active</button>
                                                          <button  type="button"   onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger " style="display: none;">Inactive</button>
                                                        <?php elseif($item->status == 0): ?>
                                                          <button  type="button" onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger ">Inactive</button>
                                                          <button  type="button" onclick="editMember(0,<?= $item->id ;?>, '<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success " style="display: none;">Active</button>
                                                        <?php endif ?>
                                                       </td>
                                                    </tr>
                                                    @endforeach
                                                       </tbody>
                                                   </table>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
                      </div>
                      <div class="tab-pane fade" id="account-vertical-notifications" role="tabpanel" aria-labelledby="account-pill-notifications" aria-expanded="false">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">

                         		<div class="row">
                               <div class="col-12">
                                   <div class="card">
                                       <div class="card-header">
                                           <h4 class="card-title">Main Wallet Transactions</h4>
                                       </div>
                                       <div class="card-content">
                                           <div class="card-body card-dashboard">
                                               <div class="table-responsive">
                                                   <table id="example" class="table table-striped table-bordered">
                                                       <thead>
                                                           <tr>
                                                                 <th >Date</th>
                                                                 <th >Transaction Id</th>
                                                                <th >Narration</th>
                                                                <th >Amount</th>
                                                                <th >Type</th>
                                                                <th >Balance</th>
                                                           </tr>
                                                       </thead>
                                                       <tbody>
                                                           @foreach ($wallet_transaction as $item)
                                                    <tr>
                                                       <td><span style="display: none;">{{date('YmdHis', strtotime($item->date))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->date))}}</td>
                                                       <td>{{$item->transaction_id}}</td>
                                                       <td>{{$item->narration}}</td>
                                                       @if($item->type == 'credit')
                                                       <td style="color: green;">&#x20B9; {{$item->amount}}</td>
                                                       @elseif($item->type == 'debit')
                                                       <td style="color: red;">&#x20B9; {{$item->amount}}</td>
                                                       @endif
                                                       <td>{{ucfirst($item->type)}}</td>
                                                       <td>&#x20B9; {{$item->current_balance}}</td>
                                                    </tr>
                                                    @endforeach
                                                       </tbody>
                                                   </table>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-12">
                                   <div class="card">
                                       <div class="card-header">
                                           <h4 class="card-title">AEPS Transactions</h4>
                                       </div>
                                       <div class="card-content">
                                           <div class="card-body card-dashboard">
                                               <div class="table-responsive">
                                                   <table id="example1" class="table table-striped table-bordered">
                                                       <thead>
                                                           <tr>
                                                                 <th >Date</th>
                                                                 <th >Transaction ID</th>
                                                                <th >Narration</th>
                                                                <th >Amount</th>
                                                                <th >Type</th>
                                                                <th >Balance</th>
                                                           </tr>
                                                       </thead>
                                                       <tbody>
                                                           @foreach ($aeps_transactions as $item)
                                                    <tr>
                                                                <td><span style="display: none;">{{date('YmdHis', strtotime($item->date))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->date))}}</td>
                                                       <td>{{$item->transaction_id}}</td>
                                                       <td>{{$item->narration}}</td>
                                                       @if($item->type == 'credit')
                                                       <td style="color: green;">&#x20B9; {{$item->amount}}</td>
                                                       @elseif($item->type == 'debit')
                                                       <td style="color: red;">&#x20B9; {{$item->amount}}</td>
                                                       @endif
                                                       <td>{{ucfirst($item->type)}}</td>
                                                       <td>&#x20B9; {{$item->current_balance}}</td>
                                                    </tr>
                                                    @endforeach
                                                       </tbody>
                                                   </table>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
                      </div>
                      <div class="tab-pane fade" id="account-vertical-owner" role="tabpanel" aria-labelledby="account-pill-owner" aria-expanded="false">
                      		<div class="card">
                      			<div class="card-content">
                      				<div class="card-body">

                         <form action="{{route('updateowner',$user->id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-12">
                                     <div class="form-group">
                                         <label for="accountSelect">Owner</label>
                                          @foreach($owner as $item)
                                           <?php if($user->created_by == $item->id): ?>
                                              <label class="btn btn-md btn-success ml-50 mb-50 mb-sm-0 cursor-pointer" for="account-upload">{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</label>
                                           <?php endif ?>
                                           @endforeach
                                     </div>
                                 </div>
                                 <div class="col-12">
                                     <div class="form-group">
                                         <label for="accountSelect">Change Owner</label>
                                         <select class="form-control" id="musicselect2" name="created_by">
                                             <option value="">Select Owner</option>
                                              @foreach($upgradeowner as $item)
                                              <option value="{{$item->id}}" <?php  if ($item->id ==$user->created_by ) {echo 'selected disabled';} ?>>{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</option>
                                              @endforeach
                                         </select>
                                     </div>
                                 </div>
                               <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                  <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                  changes</button>
                                  <button type="reset" class="btn btn-outline-warning">Cancel</button>
                               </div>
                            </div>
                         </form>
                     </div>
                 </div>
             </div>
                        </div>
                    </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- account setting page end -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
  function editMember(status, id, cid, sid) {
    if(id) {
        $.ajax({
            url: '/userservicestatus',
            type: 'get',
            data: {id : id,status : status},
            dataType: 'json',
            success:function(response) {
              console.log(response.success)
               
              if (response.status == 1) {
                 $(cid).hide();
                 $(sid).show();
              }else{
                 $(cid).hide();
                 $(sid).show();
              }

            }
        }); 
 
    } else {
        alert("Error : Refresh the page again");
    }
}
</script>
<script type="text/javascript">
  function updateMemberStatus(status, id, cid, sid) {
    if(id) {
        $.ajax({
            url: '/changeuser',
            type: 'get',
            data: {id : id,status : status},
            dataType: 'json',
            success:function(response) {
              console.log(response.success)
               
              if (response.status == 1) {
                 $(cid).hide();
                 $(sid).show();
              }else{
                 $(cid).hide();
                 $(sid).show();
              }

            }
        }); 
 
    } else {
        alert("Error : Refresh the page again");
    }
}
</script>

<script type="text/javascript">
   $('#example1').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
   $('#example2').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
</script>
@endsection