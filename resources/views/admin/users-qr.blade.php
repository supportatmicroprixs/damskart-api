@extends('admin.layouts.main')
@section('css')
<!-- <link rel="stylesheet" href="sweetalert2.min.css"> -->
<style type="text/css">
   .text-white{
   color: white;
   }
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   /*@media print{
   body *{
   visibility: hidden;
   }
   .print-container, .print-container *{
   visibility: visible;
   }
   .print-container, #cl_button *{
   visibility: hidden;
   }
   }*/
   .my_qr_code_design{
   width: 85%;
   margin: auto;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">User's QR Code</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">User's QR Code
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">User's QR</h4> -->
                        <!-- <a class="btn btn-primary" id="#user_qr"  onclick="generateMYQR()"  data-toggle="tab" href="#account-center" aria-controls="account-center" role="tab" aria-selected="false">Generate QR</a> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <div class="col-12">
                              <div class="table-responsive border rounded px-1 ">
                                 <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"> Generate QR</h6>
                                 <div class="row match-height">
                                    <div class="col-md-12">
                                       <div class="row">
                                          <div class="col-md-12">
                                             <div class="card" style="margin-bottom: 0rem;">
                                                <!-- <div class="card-header">
                                                   <h4 class="card-title">Filter</h4>
                                                   </div> -->
                                                <div class="card-content">
                                                   <div class="card-body">
                                                      <div class="row">
                                                         <div class="col-md-5 col-12 mb-1">
                                                            <fieldset>
                                                               <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                     <button class="btn btn-primary" type="button"><i class="fa fa-qrcode"></i></button>
                                                                  </div>
                                                                  <input type="text" onkeyup="numberQRDetected()" class="form-control" id="number_of_qr" placeholder="Total Number of QR" name="number_of_qr" />
                                                               </div>
                                                            </fieldset>
                                                         </div>
                                                         <div class="col-md-4 col-12 mb-1">
                                                            <div class="input-group-append">
                                                               <button type="button" class="btn btn-primary" id="user_qr" onclick="generateQRCode()"> Generate QR</button>
                                                               &nbsp;&nbsp;
                                                               {{--<a href="{{ route('download-zip')}}" class="btn btn-warning" id="download_pdf_qrcode" style="display:none"> Download as pdf</a>--}}
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <hr>
                           <div class="form-body">
                              <div class="row">
                                 <div class="col-12">
                                    <div class="form-group">
                                       <section id="basic-examples">
                                          <div class="row match-height all_qr_code" >
                                          </div>
                                       </section>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>

<script>
  function generateQRCode() {
    var formData = new FormData();
    $("#download_pdf_qrcode").hide();
    formData.append("_token", "{{ csrf_token() }}");
    formData.append('number_of_qr',$("#number_of_qr").val());

        $.ajax({
          type:'POST',
          url: '<?= route('generate-qr') ?>',
          data:formData,
          cache:false,
          contentType: false,
          processData: false,
          success:function(response){   
          if(response){

             $(".all_qr_code").html(response);
             $("#download_pdf_qrcode").show();
              
          }else{
              console.log("oke not");
          }                                            
              
          },
          error: function(response){
          }
      });
  }
   
</script>
<script type="text/javascript">
   function numberQRDetected(){
     var number_of_qr = $('#number_of_qr').val();
         if (number_of_qr.length > 0) {
           $('#user_qr').removeAttr('disabled');
         } else {
           $('#user_qr').attr('disabled', 'disabled');
         }
   }
</script>
@endsection