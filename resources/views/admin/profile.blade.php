@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .page-users-view .users-view-image {
   width: 175px;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif 
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <!-- page users view start -->
         <section class="page-users-view">
            <div class="row">
               <!-- account start -->
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-title">My Account</div>
                     </div>
                     
                     <div class="card-body">
                        <div class="row">
                           <div class="users-view-image">
                            <?php if($user->profile_pic == null ): ?>
                              <img src="{{asset('images/placeholder-profile.png')}}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                            <?php else: ?>
                              <img src="{{asset('/').$user->profile_pic}}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" height="160" alt="avatar">
                            <?php endif ?>

                           </div>
                           <div class="col-12 col-sm-9 col-md-6 col-lg-4">
                              <table>
                                 <tr>
                                    <td class="font-weight-bold">Username</td>
                                    <td>{{ucfirst($user->first_name).' '.ucfirst($user->last_name)}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Email</td>
                                    <td>{{$user->email}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Mobile</td>
                                    <td>{{$user->mobile}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Role</td>
                                    <?php if(Auth::user()->role == "super admin"): ?>
                                    <td>Super Admin</td>
                                    <?php endif ?>
                                    <?php if(Auth::user()->role == "sdistributor"): ?>
                                    <td>Super Distributor</td>
                                    <?php endif ?>
                                    <?php if(Auth::user()->role == "mdistributor"): ?>
                                    <td>Master Distributor</td>
                                    <?php endif ?>
                                    <?php if(Auth::user()->role == "distributor"): ?>
                                    <td>Distributor</td>
                                    <?php endif ?>
                                    <?php if(Auth::user()->role == "retailer"): ?>
                                    <td>Retailer</td>
                                    <?php endif ?>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Gender</td>
                                    <td>{{$user->gender}}</td>
                                 </tr>
                              </table>
                           </div>
                           <div class="col-12 col-md-12 col-lg-5">
                              <table class="ml-0 ml-sm-0 ml-lg-0">
                                 <tr>
                                    <td class="font-weight-bold">Aadhar Card</td>
                                    <td>{{$user->aadhar_number}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Pan Card</td>
                                    <td>{{$user->pan_number}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">AEPS Wallet</td>
                                    <td>&#8377; {{number_format($user->aeps_wallet,3)}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Main Wallet</td>
                                    <td>&#8377; {{number_format($user->ewallet,3)}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Login Pin</td>
                                    <td>{{$user->pin}}</td>
                                 </tr>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- account end -->
            </div>
         </section>
         <!-- page users view end -->
         <!-- page users view start -->
         <section class="page-users-view">
            <div class="row">
               <div class="col-md-6 col-12 ">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-title mb-2">Information</div>
                     </div>
                     <div class="card-body">
                        <div class="row">
                           <div class="col-12">
                              <table>
                                 <tr>
                                    <td class="font-weight-bold">Birth Date </td>
                                    <td>{{date('d-M-Y', strtotime($user->dob))}}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Status</td>
                                    <?php if(Auth::user()->status == 1): ?>
                                    <td>Active</td>
                                    <?php else: ?>
                                    <td>Inactive</td>
                                    <?php endif ?>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Address</td>
                                    <td>{{$user->address}}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">State</td>
                                    <td>{{$user->state}}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">District</td>
                                    <td>{{$user->district}}</td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Pin Code</td>
                                    <td>{{$user->pin_code}}
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 " style="display: grid;">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-title mb-2">Company Details </div>
                     </div>
                     <div class="card-body">
                        <div class="row">
                           <div class="col-12 col-sm-9  col-lg-6">
                              <table>
                                 <tr>
                                    <td class="font-weight-bold">Name</td>
                                    <td>{{$user->company_name}}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Address</td>
                                    <td>{{$user->company_address}}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="font-weight-bold">Pan/GST</td>
                                    <td>{{$user->company_pan}}
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- page users view end -->
         <!-- page users view start -->
         <section class="page-users-view">
            <div class="row">
               <!-- account start -->
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-title">My Documents</div>
                     </div>
                     <div class="card-body">
                        <form action="{{ route('updateProfile') }}" method="POST" enctype="multipart/form-data" novalidate>
                           {{csrf_field()}}
                           <div class="row">
                              <div class="col-12 col-sm-6">

                                 <div class="form-group">
                                    <div class="controls">
                                       <label>Profile Pic</label>
                                        <?php if($user->profile_pic == null): ?>
                                            <input type="file" class="form-control" name="profile_pic" placeholder="Profile Pic" required data-validation-required-message="This Profile pic is required">
                                             <img src="{{asset('images/placeholder-profile.png')}}" class="media-object img-radius" width="64" height="64"  alt="Profile Pic">
                                         <?php elseif(($user->profile_pic != null  && $user->kyc_status == 'REJECT')||($user->profile_pic == null && $user->outlet_id == null  && $user->kyc_status == null)): ?>
                                            <input type="file" class="form-control" name="profile_pic" placeholder="Profile Pic" required data-validation-required-message="This Profile pic is required">
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->profile_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Profile Image">
                                         <?php else: ?>
                                            <input type="file" class="form-control" name="profile_pic" placeholder="Profile Pic">
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->profile_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Profile Image">
                                         <?php endif ?>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="controls">
                                       <label>Company Photo</label>
                                        <?php if ($user->company_logo == null ): ?>
                                            <input type="file" class="form-control" name="company_logo"
                                                placeholder="Shop Photo" required data-validation-required-message="This Shop Photo is required">
                                            <br>
                                            <p>Not Uploaded Yet!</p>
                                        <?php elseif(($user->company_logo != null && $user->kyc_status == 'REJECT')||($user->company_logo == null && $user->outlet_id == null  && $user->kyc_status == null)) : ?>
                                            <input type="file" class="form-control" name="company_logo"
                                                placeholder="Shop Photo" value="<?php echo $user->company_logo;?>" required data-validation-required-message="This Shop Photo is required">
                                            <br>
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->company_logo}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Company Image">
                                        <?php else : ?>
                                            <input type="file" class="form-control" name="company_logo"
                                                placeholder="Shop Photo" value="<?php echo $user->company_logo;?>" disabled >
                                            <br>
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->company_logo}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Company Image">
                                        <?php endif ?>
                                    </div>
                                 </div>

                                 <div class="form-group d-none hide">
                                    <div class="controls">
                                       <label>Account Passbook/Cancel Cheque</label>

                                        <?php if ($user->passbook_image == null): ?>
                                            <input type="file" class="form-control" name="passbook_image" placeholder="Account Passbook/Cancel Cheque" value="">
                                            <br>
                                            <p>Not Uploaded Yet!</p>
                                        <?php else : ?>
                                            <input type="file" class="form-control" name="passbook_image" placeholder="Account Passbook/Cancel Cheque" value="<?php echo $user->passbook_image;?>" disabled>
                                            <br>
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->passbook_image}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Account Passbook/Cancel Cheque">
                                        <?php endif ?>
                                    </div>
                                 </div>
                                 <div class="form-group d-none hide">
                                    <div class="controls">
                                       <label>10th Marksheet (Optional)</label>
                                        <?php if ($user->marksheet_10 == null): ?>
                                            <input type="file" class="form-control" name="marksheet_10"placeholder="10th Marksheet (Optional)" >
                                            <br>
                                            <p>Not Uploaded Yet!</p>
                                       <?php else : ?>
                                            <input type="file" class="form-control" name="marksheet_10"placeholder="10th Marksheet (Optional)" value="<?php echo $user->marksheet_10;?>" disabled>
                                            <br>
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_10}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="10th Marksheet(Optional)">
                                       <?php endif ?>
                                    </div>
                                 </div>
                                 <div class="form-group d-none hide">
                                    <div class="controls">
                                       <label>12th Marksheet</label>
                                        <?php if ($user->marksheet_12 == null): ?>
                                            <input type="file" class="form-control" name="marksheet_12" placeholder="12th Marksheet" value="">
                                          <br>
                                          <p>Not Uploaded Yet!</p>
                                       <?php else : ?>
                                            <input type="file" class="form-control" value="<?php echo $user->marksheet_12;?>" name="marksheet_12" disabled placeholder="12th Marksheet" >
                                          <br>
                                            <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->marksheet_12}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="12th Marksheet">
                                       <?php endif ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-12 col-sm-6">
                                 <div class="form-group">
                                    <div class="controls">
                                       <label>Aadhar Card Photo</label>
                                        <?php if ($user->aadhar_pic_front == null): ?>
                                            <input type="file" class="form-control" name="aadhar_pic_front"placeholder="Name" value="" required data-validation-required-message="This Aadhar Card Photo field is required">
                                            <br>
                                            <p>No Image Avaliable!</p>
                                          <?php elseif(($user->aadhar_pic_front != null  && $user->kyc_status == 'REJECT')||($user->aadhar_pic_front == null &&  $user->outlet_id == null  && $user->kyc_status == null)) : ?>
                                            <input type="file" class="form-control" name="aadhar_pic_front"placeholder="Name" value="<?php echo $user->aadhar_pic_front;?>" required data-validation-required-message="This Aadhar Card Photo field is required">
                                       <?php else : ?>
                                            <input type="file" class="form-control" name="aadhar_pic_front"placeholder="Name" value="<?php echo $user->aadhar_pic_front;?>" disabled>
                                       <?php endif ?>
                                        <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->aadhar_pic_front}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Aadhar card">
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="controls">
                                       <label>Pan Card Photo</label>
                                       <?php if ($user->pan_pic == null): ?>
                                       <input type="file" class="form-control" name="pan_pic" placeholder="Pan Card Photo" value="" required data-validation-required-message="This Pan Card Photo is required">
                                       <br>
                                       <p>No Image Avaliable!</p>
                                       <?php elseif(($user->pan_pic != null  && $user->kyc_status == 'REJECT')||($user->pan_pic == null && $user->outlet_id == null  && $user->kyc_status == null)): ?>
                                           <input type="file" class="form-control" name="pan_pic" placeholder="Pan Card Photo" value="<?php echo $user->pan_pic;?>" required data-validation-required-message="This Pan Card Photo is required">
                                       <?php else : ?>
                                           <input type="file" class="form-control" name="pan_pic" placeholder="Pan Card Photo" value="<?php echo $user->pan_pic;?>" disabled>
                                       <?php endif ?>
                                    </div>
                                    <img id="doc_img" class="media-object img-radius" src="{{asset('/').$user->pan_pic}}" alt="" data-toggle="tooltip" data-placement="top" title="" width="64" height="64" data-original-title="Pan Card">
                                 </div>
                                 
                              </div>
                              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                 <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                                 Changes</button>
                                 <button type="reset" class="btn btn-outline-warning">Reset</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- account end -->
            </div>
         </section>
         <!-- page users view end -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection