@extends('admin.layouts.main')
@section('css')
<style type="text/css">
	.overlay {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9);
    overflow-x: hidden;
    transition: 0.5s;
}

   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   /*.table-responsive{overflow-x: hidden!important;}*/
   @media print{
   body *{
   visibility: hidden;
   }
   .print-container, .print-container *{
   visibility: visible;
   margin-top: 0px;
   }
   .print-container, #cl_button *{
   visibility: hidden;
   }
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
	<div class="content-wrapper">
		<div id="send_otp_message"></div>
		<div id="scan_message"></div>

		<div id="scan1_message"></div>
		<div id="scan2_message"></div>
		<div id="scan3_message"></div>
		<div id="scan4_message"></div>
		<div id="scan5_message"></div>
		<div id="scan6_message"></div>

		<div class="d-none hide">
			<select id="ddlAVDM" class="form-control">
	            <option></option>
	        </select>
			<input type="hidden" class="form-control" id="txtSSLDomName" placeholder="127.0.0.1">
			<textarea rows="5" id="txtDeviceInfo" class="form-control"></textarea>

			<textarea rows="5" id="txtPidData" class="form-control"></textarea>
		</div>

		<div id="myNav" class="overlay">
		    <div class="overlay-content">
		        <a href="#">Please wait while discovering port from 11100 to 11120.This will take some time.</a>
		    </div>
		</div>
	</div>
   
</div>

<script src="{{ asset('admin/aeps-js/jquery-1.8.2.js') }}" ></script>
<script src="{{ asset('admin/aeps-js/mfs100.min.js') }}" ></script>

<script type="text/javascript">
	var quality = 60; //(1 to 100) (recommanded minimum 55)
	var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

	function GetInfo() {
		url = "";

					
	    // $("#lblStatus").text("Discovering RD Service on Port : " + i.toString());
		//Dynamic URL

		finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();

		try {
			var protocol = window.location.href;
			if (protocol.indexOf("https") >= 0) {
				finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();
			}
		} catch (e)
		{ }

		//
		 var verb = "DEVICEINFO";
	      //alert(finalUrl);

	        var err = "";

			var res;
			$.support.cors = true;
			var httpStaus = false;
			var jsonstr="";
			;
				$.ajax({

				type: "DEVICEINFO",
				async: false,
				crossDomain: true,
				url: finalUrl+MethodInfo,
				contentType: "text/xml; charset=utf-8",
				processData: false,
				success: function (data) {
				//alert(data);
					httpStaus = true;
					res = { httpStaus: httpStaus, data: data };

					$('#txtDeviceInfo').val(data);

					//alert("Please scan data");

					CaptureAvdm();
				},
				error: function (jqXHR, ajaxOptions, thrownError) {
				alert(thrownError);
					res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
				},
			});

			return res;
	}

	function sendDataToThirdParty()
	{
		var pidData = $('#txtPidData').val();

		xmlDoc = $.parseXML( pidData );
  		$xml = $( xmlDoc );

  		//var errCode = errInfo = fCount = fType = fType = nmPoints = qScore = "";
  		//var dpId = rdsId = rdsVer = mi = mc = "";
  		var PidDatatype = Piddata = ci = dc = dpId = errInfo = hmac = mc = mi = nmPoints = qScore = rdsId = rdsVer = sessionKey = "";
  		errCode = fCount = fType = iCount = pCount = pType = 0;
  		var bank_id = 508534;

  		$xml.find("PidData").each(function(index,elem){
            PidDatatype = jQuery(elem).find("Data").attr("type");
            errInfo = jQuery(elem).find("Resp").attr("errInfo");
            errCode = jQuery(elem).find("Resp").attr("errCode");
            fCount = jQuery(elem).find("Resp").attr("fCount");
            fType = jQuery(elem).find("Resp").attr("fType");
            nmPoints = jQuery(elem).find("Resp").attr("nmPoints");
            qScore = jQuery(elem).find("Resp").attr("qScore");

            Piddata = jQuery(elem).find("Data").text();
            sessionKey = jQuery(elem).find("Skey").text();

            ci = jQuery(elem).find("Skey").attr('ci');
            dc = jQuery(elem).find("DeviceInfo").attr('dc');
            dpId = jQuery(elem).find("DeviceInfo").attr('dpId');

            hmac = jQuery(elem).find("Hmac").text();
            mc = jQuery(elem).find("DeviceInfo").attr('mc');
            mi = jQuery(elem).find("DeviceInfo").attr('mi');
            rdsId = jQuery(elem).find("DeviceInfo").attr('rdsId');
            rdsVer = jQuery(elem).find("DeviceInfo").attr('rdsVer');
		});

  		$('#scan1_message').append("PidDatatype= " + PidDatatype);
  		$('#scan1_message').append("<br />Piddata= " + Piddata);
  		$('#scan1_message').append("<br />CI= " + ci);
  		$('#scan1_message').append("<br />DC= " + dc);
  		$('#scan1_message').append("<br />dpId= " + dpId);
  		$('#scan1_message').append("<br />errInfo= " + errInfo);
  		$('#scan1_message').append("<br />errCode= " + errCode);
  		$('#scan1_message').append("<br />fCount= " + fCount);
  		$('#scan1_message').append("<br />fType= " + fType);
  		$('#scan1_message').append("<br />hmac= " + hmac);
  		$('#scan1_message').append("<br />iCount= " + iCount);
  		$('#scan1_message').append("<br />mc= " + mc);
  		$('#scan1_message').append("<br />mi= " + mi);
  		$('#scan1_message').append("<br />nmPoints= " + nmPoints);
  		$('#scan1_message').append("<br />qScore= " + qScore);
  		$('#scan1_message').append("<br />rdsId= " + rdsId);
  		$('#scan1_message').append("<br />rdsVer= " + rdsVer);
  		$('#scan1_message').append("<br />sessionKey= " + sessionKey);

  		ekyc_res_mobile = "9783335317";
    	ekyc_res_primaryKeyId = "51528";
    	ekyc_res_encodeFPTxnId = "EKYKF1604257040621223120127I";

    	$.ajax({
	        type:"get",
	        url:"/iciciEKYCcomplete", 
	        data : {
	        	'ekyc_res_mobile':ekyc_res_mobile,
	        	'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,
	        	'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,
	        	'bank_id' : bank_id,
	        	'PidDatatype' : PidDatatype,
	        	'Piddata' : Piddata,
	        	'ci' : ci,
	        	'dc' : dc,
	        	'dpId' : dpId,
	        	'errCode' : errCode,
	        	'errInfo' : errInfo,
	        	'fCount' : fCount,
	        	'fType' : fType,
	        	'hmac' : hmac,
	        	'iCount' : iCount,
	        	'mi' : mi,
	        	'nmPoints' : nmPoints,
	        	'pCount' : pCount,
	        	'pType' : pType,
	        	'qScore' : qScore,
	        	'rdsId' : rdsId,
	        	'rdsVer' : rdsVer,
	        	'sessionKey' : sessionKey
	        },
	        dataType: "json",
	        success:function(data)
	        {   
	            $('#Dv_Loader').hide();
	            if (data.permission_code == "PER111") {
	              $("#response_message").empty();
	              $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
	                
	                setTimeout(function () {
	                  window.location.href = SITEURL + '/index';
	                }, 3000);
	             }else{
	               if(data.status == true){
	                     $('#ekyc_button').hide();
	                     $('#ekyc_resendOTPbutton').hide();
	                     $('#ekyc_verifyOTPbutton').hide();
	                     
	                     $("#send_otp_message").empty();
	                     $('#send_otp_message').html(data.message).css('color','green');

	                     var timeleft = 3;
	                      var downloadTimer = setInterval(function(){
	                        if(timeleft <= 0){
	                          clearInterval(downloadTimer);
	                          $("#ekyc_countdown").empty();
	                          $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
	                        } else {
	                          $("#ekyc_countdown").empty();
	                          $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
	                        }
	                        timeleft -= 1;
	                      }, 875);
	                      setTimeout(function () {
	                         window.location.href = SITEURL + '/icici-aeps';
	                      }, 3000); 
	                 }else{
	                     $("#send_otp_message").empty();
	                     $('#send_otp_message').html(data.message).css('color','red');
	                 }
	             }
	         }
	    });
	}

	$(window).load(function() {
		discoverAvdm();
	});
</script>
@endsection