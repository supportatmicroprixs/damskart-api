@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   /*.table-responsive{overflow-x: hidden!important;}*/
   @media print{
   body *{
   visibility: hidden;
   }
   .print-container, .print-container *{
   visibility: visible;
   margin-top: 0px;
   }
   .print-container, #cl_button *{
   visibility: hidden;
   }
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">ICICI AEPS</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Services
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <h4 id="response_message"></h4>
            </div>
            <!-- <button type="button" class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#verifyekycOTPModal">verifyekycOTPModal</button> -->
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($validate == false || $user_ekyc_status == 0)
            @if ($validate == false)
            <section id="basic-examples">
               <div class="row match-height">
                  <div class="col-xl-4 col-md-6 col-sm-12">
                     <div class="card" style="border-bottom: 3px solid #FF7600;">
                        <div class="card-content">
                           <img class="card-img-top img-fluid" src="{{asset('/images/icici-bank-logo.png')}}" style="padding: 25px;" alt="Card image cap">
                           <div class="card-body">
                              <div class="card-btns d-flex justify-content-between ">
                                 <form  action="{{ route('icici-aeps') }}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <!-- <button type="submit" class="btn gradient-light-primary btn-block btn-lg" style="margin-top: -35px">Click To <b>"Onboard"</b> with ICICI Bank AEPS <i class="feather icon-arrow-right"></i></button> -->
                                    <a href="{{ route('icici-aeps') }}" class="btn gradient-light-primary btn-block btn-lg" style="margin-top: -35px">Click To <b>"Onboard"</b> with ICICI Bank AEPS <i class="feather icon-arrow-right"></i></a>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Profile Cards Ends -->
               </div>
            </section>
            @elseif ($user_ekyc_status == 0)
            <section id="basic-examples">
               <div class="row match-height">
                  <div class="col-xl-4 col-md-6 col-sm-12">
                     <div class="card" style="border-bottom: 3px solid #FF7600;">
                        <div class="card-content">
                           <img class="card-img-top img-fluid" src="{{asset('/images/icici-bank-logo.png')}}" style="padding: 25px;" alt="Card image cap">
                           <div class="card-body">
                                    
                              <div class="card-btns d-flex justify-content-between ">
                                 <form  action="{{ route('icici-aeps') }}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <button type="button" onclick="ekyc_send_otp()" class="btn gradient-light-danger btn-block btn-lg" style="margin-top: -35px">Click To <b>"E-KYC"</b> with ICICI Bank AEPS <i class="feather icon-arrow-right"></i></button>
                                    <input type="hidden" name="ekyc_mobile" id="ekyc_mobile" value="<?php echo Auth::user()->mobile; ?>">
                                    <input type="hidden" name="ekyc_aadhar_number" id="ekyc_aadhar_number" value="<?php echo Auth::user()->aadhar_number; ?>">
                                    <input type="hidden" name="ekyc_pan_number" id="ekyc_pan_number" value="<?php echo Auth::user()->pan_number; ?>">
                                 </form>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
                  <!-- Profile Cards Ends -->
               </div>
            </section>

            @endif
         @else
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                      <div class="divider divider-center">
                        <div class="divider-text">
                           <img src="{{ asset('images/icici-bank-logo.png') }}" width="100" alt="icici">
                        </div>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('icici-aeps') }}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                              {{csrf_field()}}

                              <div class="d-none hide">
                                 <select id="ddlAVDM" class="form-control">
                                       <option></option>
                                   </select>
                                 <input type="hidden" class="form-control" id="txtSSLDomName" placeholder="127.0.0.1">
                                 <textarea rows="5" id="txtDeviceInfoService" class="form-control"></textarea>

                                 <textarea rows="5" id="txtPidDataService" class="form-control"></textarea>
                              </div>

                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group d-flex align-items-center pt-md-2">
                                          <label class="mr-2">Select Service:</label>
                                          <div class="c-inputs-stacked">
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="method" id="withdrawal" value="withdrawal">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Withdrawal</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block mr-2" >
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" id="balance_enquiry" value="balance_enquiry" name="method">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Balance Enquiry</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" id="aadhar_pay" value="aadhar_pay" name="method">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Aadhar Pay</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" id="mini_statement" value="mini_statement" name="method">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Mini Statement</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group d-flex align-items-center pt-md-2">
                                          <label class="mr-2">Select Device:</label>
                                          <div class="c-inputs-stacked">
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" id="mantra" value="mantra" name="device">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Mantra</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block mr-2" >
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" id="morpho" value="morpho" name="device">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Morpho</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Mobile</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="number" class="form-control" size="10" maxlength="10" pattern="[6789][0-9]{9}" value="{{ old('mobile') }}" name="mobile" id="mobile" placeholder="Mobile Number" required>
                                             <div class="form-control-position">
                                                <i class="feather icon-smartphone"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Aadhar Number</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="text" class="form-control" pattern="^\d{12}$" maxlength="12" value="{{ old('aadhar_number') }}" id="aadhar_number" name="aadhar_number" placeholder="Aadhar Number " required>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Amount</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="number" class="form-control" min="50" value="{{ old('amount') }}" id="amount" name="amount" placeholder="Amount">
                                             <div class="form-control-position">
                                                <i class="fa fa-inr"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="email-id-icon">Select Bank</label>
                                          <div class="position-relative has-icon-left">
                                             <select class="form-control" id="bank_detail">
                                                <option value="">Select Bank</option>
                                                @foreach($bank_detail as $item)
                                                <option value="{{$item['id']}}">{{$item['bankName']}}</option>
                                                @endforeach
                                             </select>
                                             <div class="form-control-position">
                                                <i class="fa fa-university"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="col-md-12">
                                       <h4 id="send_otp_service_message"></h4>
                                    </div>

                                    <div class="col-md-12">
                                       <div class="form-group d-flex align-items-center pt-md-2">
                                          <div class="c-inputs-stacked">
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="" value="" checked>
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">I Agree <a href="">Terms & Conditions</a></span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    

                                    <div class="col-12">
                                       <button type="button" id="scan_button" onclick="scanaeps()" class="btn btn-primary mr-1 mb-1">Submit & Scan</button>
                                       <button type="reset" id="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                        <div id="demo-modal"></div>
                        <p id="test" style="display: none;"></p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      @endif
      
      <div class="divWaiting" id="Dv_Loader" style="display: none;">
         <button class="btn btn-primary mb-1 round" type="button" >
         <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
         &nbsp;Loading... 
         </button>
      </div>
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content " >
               <div class="print-container">
                  <div class="modal-header"  style="background: linear-gradient(to right, #7367F0, #968df4);">
                     <div class="row" style="width: 100%">
                        <div class="col-4" style="margin-left: 18px;">
                           <img  src="{{asset('/images/icici_response_img.png')}}" height="64" alt="logo">
                        </div>
                        <div class="col-7 text-right" style="margin-left: 18px;">
                           <img src="{{asset('images/Damskart Logo.png')}}" height="64" alt="logo.png">
                        </div>
                     </div>
                     <hr>
                     
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-6">
                           <b>Date:&nbsp;</b>
                           <p id="TxnDate"></p>
                        </div>
                        <div class="col-6">
                           <b>Order Id:&nbsp;</b>
                           <p id="TxnOrderId"></p>
                        </div>
                        <div class="col-6">
                           <b>Txn Amount(&#x20B9;):&nbsp;</b>
                           <p id="TxnAmount"></p>
                        </div>
                        <div class="col-6">
                           <b>Status:&nbsp;</b>
                           <p id="TxnStatus"></p>
                        </div>
                        <div class="col-6">
                           <b>Custmer Mobile No:&nbsp;</b>
                           <p id="TxnMobile"></p>
                        </div>
                        <div class="col-6">
                           <b>RRN:&nbsp;</b>
                           <p id="TxnRrn"></p>
                        </div>
                        <div class="col-6">
                           <b>Total Balance(&#x20B9;):&nbsp;</b>
                           <b>
                              <p style="font-size: large;" id="TxnTotalBalance"></p>
                           </b>
                        </div>
                        <div class="col-6">
                           <b>Remarks:&nbsp;</b>
                           <p id="TxnRemarks"></p>
                        </div>
                     </div>
                     <div class="col-12" id="mini_statement_response">
                       
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default"  onclick="ClearCtrlClose();" data-dismiss="modal" >Close</button>
                  <!-- <button type="button" class="btn btn-success" onclick="window.print()"  data-dismiss="modal" >Print</button> -->
                  <button class="btn btn-primary btn-print mb-1 mb-md-0"> <i class="feather icon-file-text"></i> Print</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="verifyekycOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
           <div class="modal-content">
              <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                 <h4 class="modal-title" style=" color: white;" id="myModalLabel">E-KYC OTP Verification</h4>
                 <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body" id="getCode" >
                 <div id="invoice-template" class="card-body">
                    <div id="invoice-company-details" class="row">
                       <div class="col-sm-4 col-12 text-left">
                          <div class="media">
                             <img src="{{asset('images/Damskart Logo.png')}}" style="width: 100px;">
                          </div>
                       </div>
                       <div class="col-sm-8 col-12 text-right">
                          <div class="invoice-details mt-1" >
                              <h4 id="send_otp_message"></h4>

                              <div class="d-none hide">
                                 <div id="scan_message"></div>

                                 <div id="scan1_message"></div>

                                 <select id="ddlAVDM" class="form-control">
                                       <option></option>
                                   </select>
                                 <input type="hidden" class="form-control" id="txtSSLDomName" placeholder="127.0.0.1">
                                 <textarea rows="5" id="txtDeviceInfo" class="form-control"></textarea>

                                 <textarea rows="5" id="txtPidData" class="form-control"></textarea>
                              </div>

                              <div id="myNav" class="overlay d-none">
                                  <div class="overlay-content">
                                      <a href="#">Please wait while discovering port from 11100 to 11120.This will take some time.</a>
                                  </div>
                              </div>
                          </div>
                       </div>
                    </div>
                    <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-1">
                       <div class="row">
                          <div class="table-responsive col-12">
                             <table class="table table-borderless" style="margin-top: 1rem;">
                                <tbody>
                                   <tr>
                                      <td class="text-left"><b>OTP</b></td>
                                      <td class="text-right">
                                         <input type="number" class="form-control" id="ekyc_otp" onkeyup="otpDetected()" placeholder="Enter OTP" required>
                                         <input type="hidden" class="form-control" value="" id="ekyc_res_mobile">
                                         <input type="hidden" class="form-control" value="" id="ekyc_res_primaryKeyId">
                                         <input type="hidden" class="form-control" value="" id="ekyc_res_encodeFPTxnId">
                                      </td>
                                   </tr>
                                   <tr id="ekyc_device_div1"></tr>
                                   <tr id="ekyc_device_div2"></tr>
                                </tbody>
                             </table>
                          </div>
                       </div>
                    </div>
                    <div id="verify_response_div">
                    </div>
                 </div>
              </div>
              <p id="ekyc_countdown" class="text-center"></p>
              <div class="modal-footer" style="justify-content: left;">
                 <button type="submit" id="ekyc_verifyOTPbutton" onclick="ekyc_verify_otp_button()" class="btn btn-primary" disabled>Verify OTP</button>
                 <div id="ekyc_complete_div"></div>
                 <button type="submit" id="ekyc_resendOTPbutton" onclick="ekyc_resend_otp_button()" class="btn btn-outline-primary" >Resend OTP</button>
              </div>
           </div>
        </div>
      </div>
      <!-- Column selectors with Export Options and print table -->
   </div>
</div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<!--<script src="{{ asset('admin/aeps-js/aeps-main-new.js') }}" ></script>-->
<script src="{{ asset('admin/aeps-js/mfs100.min.js') }}" ></script>

<script type="text/javascript">
   $(function () {
    $( "#amount" ).change(function() {
       // var max = parseInt($(this).attr('max'));
       var min = parseInt($(this).attr('min'));
       if ($(this).val() < min){
           $(this).val(min);
       }       
     }); 
   });
</script>
<script type="text/javascript">
    function otpDetected(){
        var otp = $("#ekyc_otp").val();
          if (otp.length == 7) {
            $('#ekyc_verifyOTPbutton').removeAttr('disabled'); 
          } else {
            $('#ekyc_verifyOTPbutton').attr('disabled', 'disabled');
          }
    }
</script>
<script>
   $(document).ready(function() {
    $("#withdrawal").click(function() {
        $("#amount").attr("disabled", false);
     });
   
    $("#balance_enquiry").click(function() {
        $("#amount").attr("disabled", true);
     });
   
    $("#aadhar_pay").click(function() {
          $("#amount").attr("disabled", false);
       });
    $("#mini_statement").click(function() {
          $("#amount").attr("disabled", true);
       });
   
   });
</script>
<!-- ekyc_send_otp -->
<script type="text/javascript">
	var quality = 60; //(1 to 100) (recommanded minimum 55)
	var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

	function GetInfoEnquiry() {
		url = "";
               
		// $("#lblStatus").text("Discovering RD Service on Port : " + i.toString());
		//Dynamic URL

		finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();

		try {
		 var protocol = window.location.href;
		 if (protocol.indexOf("https") >= 0) {
		    finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();
		 }
		} catch (e)
		{ }

      	var verb = "DEVICEINFO";
        var err = "";

        var res;
		$.support.cors = true;
		var httpStaus = false;
		var jsonstr="";

		$.ajax({
            type: "DEVICEINFO",
            async: false,
            crossDomain: true,
            url: finalUrl+MethodInfo,
            contentType: "text/xml; charset=utf-8",
            processData: false,
            success: function (data) {
            //alert(data);
               httpStaus = true;
               res = { httpStaus: httpStaus, data: data };

               $('#txtDeviceInfoService').val(data);

               //alert("Please scan data");

               CaptureAvdmEnquiry();
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
            alert(thrownError);
               res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
            },
         });

         return res;
   }

   function GetInfo() {
      url = "";

               
       // $("#lblStatus").text("Discovering RD Service on Port : " + i.toString());
      //Dynamic URL

      finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();

      try {
         var protocol = window.location.href;
         if (protocol.indexOf("https") >= 0) {
            finalUrl = "http://"+GetCustomDomName+":" + $("#ddlAVDM").val();
         }
      } catch (e)
      { }

      //
       var verb = "DEVICEINFO";
         //alert(finalUrl);

           var err = "";

         var res;
         $.support.cors = true;
         var httpStaus = false;
         var jsonstr="";
         ;
            $.ajax({

            type: "DEVICEINFO",
            async: false,
            crossDomain: true,
            url: finalUrl+MethodInfo,
            contentType: "text/xml; charset=utf-8",
            processData: false,
            success: function (data) {
            //alert(data);
               httpStaus = true;
               res = { httpStaus: httpStaus, data: data };

               $('#txtDeviceInfo').val(data);

               //alert("Please scan data");

               CaptureAvdm();
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
            alert(thrownError);
               res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
            },
         });

         return res;
   }

   function ekyc_send_otp() {
          var SITEURL = '{{URL::to('')}}';
          var ekyc_mobile = $('#ekyc_mobile').val();
          var ekyc_aadhar_number = $('#ekyc_aadhar_number').val();
          var ekyc_pan_number = $('#ekyc_pan_number').val();

          

          Swal.fire({
             title: 'Do you want to continue?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Continue',
             confirmButtonClass: 'btn btn-primary',
             cancelButtonClass: 'btn btn-danger ml-1',
             buttonsStyling: false,
           }).then((data) => {
            if (data.value) {
              $('#Dv_Loader').show();


   
              $.ajax({
                 type:"get",
                 url:"/iciciEKYCSendOtp", 
                 data : {'ekyc_mobile':ekyc_mobile,'ekyc_aadhar_number':ekyc_aadhar_number,'ekyc_pan_number':ekyc_pan_number,},
                 dataType: "json",
                 success:function(data)
                 {   
                    $('#Dv_Loader').hide();
                    if (data.permission_code == "PER111") {
                      $("#response_message").empty();
                      $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                        
                        setTimeout(function () {
                          window.location.href = SITEURL + '/index';
                        }, 3000);
                     }else{
                       if(data.status == true){
                           Result = data;                
                           var primaryKeyId= Result.data.primaryKeyId;       
                           var encodeFPTxnId= Result.data.encodeFPTxnId;                    
                          $("#response_message").empty();

                          $('#send_otp_message').html(data.message+". OTP Sent Successfully.").css('color','green');
                          $('#ekyc_res_mobile').val(ekyc_mobile);
                          $('#ekyc_res_primaryKeyId').val(primaryKeyId);
                          $('#ekyc_res_encodeFPTxnId').val(encodeFPTxnId);
                          
                          $('#verifyekycOTPModal').modal({backdrop: 'static', keyboard: false}); 
                       }else{
                          $("#response_message").empty();
                          $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>');
                       }
                    }
                 }
              });
           } else if (data.dismiss === Swal.DismissReason.cancel) {
               Swal.fire({
                title: 'Cancelled',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
              })
          }
        })
   }

   function scanaeps() {
      $('#Dv_Loader').show();
      var status = false; 

      var aadhar_number = $("#aadhar_number").val();
      var mobile = $("#mobile").val();
      var bank_id = $("#bank_detail").val();
      var amount = $("#amount").val();
      var type = $("input[name='method']:checked").val();
      var device = $("input[name='device']:checked").val();
      if (type == null) {
         alert("Select Method First");
         $('#Dv_Loader').hide();
         return false;
      } else if (device == null) { 
         alert("Select Device First");
         $('#Dv_Loader').hide();
         return false;
      }else if (mobile.length == 0) {
         alert("Enter Mobile Number");
         $('#Dv_Loader').hide();
         return false;
      }else if (aadhar_number.length == 0) {
         alert("Enter Aadhar Number");
         $('#Dv_Loader').hide();
         return false;
      }else if (bank_id == "") {
         alert("Select Bank First");
         $('#Dv_Loader').hide();
         return false;
      }

	    if (type == 'balance_enquiry' || type == 'mini_statement') {
	        if (aadhar_number !== '' && mobile !== '' && bank_id !== 'Select Option' && bank_id !== '') {
	            status = true;
	        }
	    } else if (type == 'withdrawal' || type == 'aadhar_pay') {
	        if (aadhar_number !== '' && mobile !== '' && bank_id !== 'Select Option' && bank_id !== '' && amount !== '') {
	            status = true;
	        }
	    }
	    if (status) {
	    	if(device == "mantra")
	    	{
            $('#Dv_Loader').show();

            setTimeout(function() {
               discoverAvdmEnquiry();
            }, 2000);
	    	}
	    	else if(device == "morpho")
	    	{
	    		RDServiceyEnquiryNormal(); //RDServiceyEnquiry
	    	}
	    }
   }

   function ekyc_verify_otp_button() {
         var SITEURL = '{{URL::to('')}}';
          var ekyc_otp = $('#ekyc_otp').val();
          var ekyc_res_mobile = $('#ekyc_res_mobile').val();
          var ekyc_res_primaryKeyId = $('#ekyc_res_primaryKeyId').val();
          var ekyc_res_encodeFPTxnId = $('#ekyc_res_encodeFPTxnId').val();
          Swal.fire({
             title: 'Do you want to continue?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Continue',
             confirmButtonClass: 'btn btn-primary',
             cancelButtonClass: 'btn btn-danger ml-1',
             buttonsStyling: false,
           }).then((data) => {
            if (data.value) {
               $('#Dv_Loader').show();
              
              

              $.ajax({
                 type:"get",
                 url:"/iciciEKYCVerifyOtp", 
                 data : {'ekyc_otp':ekyc_otp,'ekyc_res_mobile':ekyc_res_mobile,'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,},
                 dataType: "json",
                 success:function(data)
                 {   
                    $('#Dv_Loader').hide();
                    if (data.permission_code == "PER111") {
                      $("#response_message").empty();
                      $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                        
                        setTimeout(function () {
                          window.location.href = SITEURL + '/index';
                        }, 3000);
                     }else{
                       if(data.status == true){
                           Result = data;                
                           var primaryKeyId= Result.data.primaryKeyId;       
                           var encodeFPTxnId= Result.data.encodeFPTxnId;                    
                          $("#response_message").empty();
                          
                          $("#send_otp_message").empty();
                          $('#send_otp_message').html("OTP Verify Successfully.").css('color','green');
                          $('#ekyc_res_mobile').val(ekyc_res_mobile);
                          $('#ekyc_res_primaryKeyId').val(primaryKeyId);
                          $('#ekyc_res_encodeFPTxnId').val(encodeFPTxnId);

                          $("#ekyc_verifyOTPbutton").hide();
                          $('#ekyc_otp').attr('disabled', 'disabled');
                          $('#ekyc_resendOTPbutton').hide();

                          $("#ekyc_device_div1").empty();
                          $("#ekyc_device_div2").empty();
                          
                          $("#ekyc_device_div1").append(' <td class="text-left"><b>Select Device:</b></td>   <td>             <div class="c-inputs-stacked">          <div class="d-inline-block mr-2">             <div class="vs-checkbox-con vs-checkbox-primary">                <input type="radio" id="ekyc_mantra" value="mantra" name="ekyc_device">                <span class="vs-checkbox">                <span class="vs-checkbox--check">                <i class="vs-icon feather icon-check"></i>                </span>                </span>                <span class="">Mantra</span>             </div>          </div>          <div class="d-inline-block mr-2" >             <div class="vs-checkbox-con vs-checkbox-primary">                <input type="radio" id="ekyc_morpho" value="morpho" name="ekyc_device">                <span class="vs-checkbox">                <span class="vs-checkbox--check">                <i class="vs-icon feather icon-check"></i>                </span>                </span>                <span class="">Morpho</span>             </div>          </div>       </div>   </td>');
                          
                          $("#ekyc_device_div2").append(' <td class="text-left"><b>Select Bank:</b></td>   <td class="text-right">      <select class="form-control" id="ekyc_bank_detail">         <option>Select Option</option>         @foreach($bank_detail as $item)          <option value="{{$item["id"]}}">{{$item["bankName"]}}</option>          @endforeach      </select>   </td>');

                          $("#ekyc_complete_div").empty();
                          $("#ekyc_complete_div").append('<button type="button" id="ekyc_button" onclick="ekyc_complete_button()" class="btn gradient-light-danger" >Complete E-KYC</button>');
                          // $("#ekyc_complete_div").append('<button type="button" id="ekyc_button" onclick="ekyc_complete_button_demo()" class="btn gradient-light-danger" >Complete E-KYC</button>');
                          // $('#verifyekycOTPModal').modal({backdrop: 'static', keyboard: false}); 
                       }else{
                          // $('#verifyekycOTPModal').modal('hide'); 
                          $("#send_otp_message").empty();
                          $("#send_otp_message").html(data.message).css('color','red');
                       }
                     }
                 }
              });
           } else if (data.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
                title: 'Cancelled',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
              })
          }
        })
   }


   
</script>
<!-- ekyc_resend_otp_button -->
<script type="text/javascript">
   function ekyc_resend_otp_button() {
          var SITEURL = '{{URL::to('')}}';
          var ekyc_res_mobile = $('#ekyc_res_mobile').val();
          var ekyc_res_primaryKeyId = $('#ekyc_res_primaryKeyId').val();
          var ekyc_res_encodeFPTxnId = $('#ekyc_res_encodeFPTxnId').val();
          Swal.fire({
             title: 'Do you want to continue?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Continue',
             confirmButtonClass: 'btn btn-primary',
             cancelButtonClass: 'btn btn-danger ml-1',
             buttonsStyling: false,
           }).then((data) => {
            if (data.value) {
              $('#Dv_Loader').show();
              $.ajax({
                 type:"get",
                 url:"/iciciEKYCResendOtp", 
                 data : {'ekyc_res_mobile':ekyc_res_mobile,'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,},
                 dataType: "json",
                 success:function(data)
                 {   
                    $('#Dv_Loader').hide();
                    if (data.permission_code == "PER111") {
                      $("#response_message").empty();
                      $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                        
                        setTimeout(function () {
                          window.location.href = SITEURL + '/index';
                        }, 3000);
                     }else{
                       if(data.status == true){
                           Result = data;                
                           var primaryKeyId= Result.data.primaryKeyId;       
                           var encodeFPTxnId= Result.data.encodeFPTxnId;                    
                          $("#response_message").empty();

                          $("#send_otp_message").empty();
                          $('#send_otp_message').html("OTP Sent Successfully.").css('color','green');
                          $('#ekyc_res_mobile').val(ekyc_res_mobile);
                          $('#ekyc_res_primaryKeyId').val(primaryKeyId);
                          $('#ekyc_res_encodeFPTxnId').val(encodeFPTxnId);
                          
                          // $('#verifyekycOTPModal').modal({backdrop: 'static', keyboard: false}); 
                       }else{
                          $("#send_otp_message").empty();
                          $("#send_otp_message").html(data.message).css('color','red');
                       }
                     }
                 }
              });
           } else if (data.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
                title: 'Cancelled',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
              })
          }
        })
   }
</script>
<!-- ekyc_resend_otp_button -->
<script type="text/javascript">
   // Send Enquiry
	function sendEnquiry() {
		var SITEURL = '{{URL::to('')}}';

		var pidData = $('#txtPidDataService').val();

		//console.log(pidData);

        xmlDoc = $.parseXML( pidData );
        $xml = $( xmlDoc );

        console.log($xml);

        //
        var aadhar_number = $("#aadhar_number").val();
	    var mobile = $("#mobile").val();
	    var bank_id = $("#bank_detail").val();
	    var amount = $("#amount").val();
	    var type = $("input[name='method']:checked").val();
	    var device = $("input[name='device']:checked").val();

        //var errCode = errInfo = fCount = fType = fType = nmPoints = qScore = "";
        //var dpId = rdsId = rdsVer = mi = mc = "";
        var PidDatatype = Piddata = ci = dc = dpId = errInfo = hmac = mc = wadh = mi = nmPoints = qScore = rdsId = rdsVer = sessionKey = pgCount = deviceSRNO = "";
        errCode = fCount = fType = iCount = pCount = pType = 0;
        //var bank_id = $('#ekyc_bank_detail').val();

        $xml.find("PidData").each(function(index,elem){
              PidDatatype = jQuery(elem).find("Data").attr("type");
              errInfo = jQuery(elem).find("Resp").attr("errInfo");
              errCode = jQuery(elem).find("Resp").attr("errCode");
              fCount = jQuery(elem).find("Resp").attr("fCount");
              fType = jQuery(elem).find("Resp").attr("fType");
              nmPoints = jQuery(elem).find("Resp").attr("nmPoints");
              qScore = jQuery(elem).find("Resp").attr("qScore");
              pgCount = jQuery(elem).find('Resp').attr("pgCount");

              Piddata = jQuery(elem).find("Data").text();
              sessionKey = jQuery(elem).find("Skey").text();

              ci = jQuery(elem).find("Skey").attr('ci');
              dc = jQuery(elem).find("DeviceInfo").attr('dc');
              dpId = jQuery(elem).find("DeviceInfo").attr('dpId');

              hmac = jQuery(elem).find("Hmac").text();
              mc = jQuery(elem).find("DeviceInfo").attr('mc');
              mi = jQuery(elem).find("DeviceInfo").attr('mi');
              rdsId = jQuery(elem).find("DeviceInfo").attr('rdsId');
              rdsVer = jQuery(elem).find("DeviceInfo").attr('rdsVer');

              deviceSRNO = jQuery(elem).find("Param").attr("value");
        });


         if(hmac != "")
         {
            jQuery.ajax({
	            type: "POST",
	            url: "/balanceinquiry",
	            data: {
                     'method': type,
                     'errCode': errCode,
                     'errInfo': errInfo,
                     'fCount': fCount,
                     'fType': fType,
                     'iCount': 0,
                     'pCount': 0,
                     'pType': 0,
                     'pgCount': pgCount,
                     'nmPoints': nmPoints,
                     'qScore': qScore,
                     'dpId': dpId,
                     'rdsId': rdsId,
                     'rdsVer': rdsVer,
                     'dc': dc,
                     'mi': mi,
                     'mc': mc,
                     'ci': ci,
                     'hmac': hmac,
                     'PidDatatype': PidDatatype,
                     'Piddata': Piddata,
                     'sessionKey': sessionKey,
                     'deviceSRNO': deviceSRNO,
                     'aadhar_number': aadhar_number,
                     'mobile': mobile,
                     'bank_id': bank_id,
                     'amount': amount,
                     "_token": "{{ csrf_token() }}",
	            },
	            dataType: "json",
	            success: function(msg) {
                     $('#Dv_Loader').hide();
	                if (msg.permission_code == "PER111") {
	                    var SITEURL = '{{URL::to('')}}';
	                  $("#response_message").empty();
	                  $("#response_message").html('<button class="btn btn-danger">'+msg.message+'</button>').css('color','red');
	                    
	                    setTimeout(function () {
	                      //location.reload();
	                    }, 3000);
	                }else{
	                    //$('#scan_button').hide();
	                    //$('#reset').hide();
	                    // $('#view_receipt').show();
	                    var Result = msg.icici_data;
	                    // alert(Result);
	                    var status = Result.status;
	                    if (status == false) {
	                        var Msg; 
	                        Msg = Result.message;
	                        if (Msg == null) {
	                            Msg = "Transactions Failed!";
	                            $('#send_otp_service_message').html('<div class="alert alert-danger">'+Msg+'</div>');
	                        }else{
	                        	$('#send_otp_service_message').html('<div class="alert alert-danger">'+Msg+'</div>');
	                        }

	                        $('html, body').animate({ scrollTop: 0 }, 'slow');

	                        setTimeout(function () {
		                    	 //location.reload();
		                    }, 7000);
	                        //

	                    }else{
	                        var cus_mobile = msg.mobile;
	                        
	                        var TxnDate = Result.request_transaction_time;                                
	                        var TxnId = Result.dms_txn_id;                                
	                        var Rrn = Result.bankRRN;                                
	                        var Mobile = cus_mobile;                                
	                        var TxnAmount = Result.transaction_amount;                                
	                        var Status = Result.transaction_status;                                
	                        var TotalBal = Result.balance_amount;                                
	                        var Message = Result.message;                                
	                        
	                        $('#TxnDate').html(TxnDate);
	                        $('#TxnOrderId').html(TxnId);
	                        $('#TxnMobile').html(Mobile);
	                        $('#TxnRrn').html(Rrn);

	                        $('#TxnAmount').html(TxnAmount);
	                        $('#TxnTotalBalance').html(TotalBal);
	                        $('#TxnStatus').html(Status);
	                        $('#TxnRemarks').html(Message);
	                        // alert(Result.transaction_type);
	                        if (Result.transaction_type == "MS") {

	                            $("#mini_statement_response").empty();
	                            $("#mini_statement_response").append('<div class="row">   <div class="col-md-12">      <div class="card" style="margin-bottom: 0rem;">         <div class="card-header">            <h4 class="card-title">Transactions List</h4>         </div>         <div class="card-content">            <div class="card-body">               <div class="table-responsive">                  <table class="table table-striped">                     <thead><tr><th>Date</th><th>Txn Type</th><th>Amount</th><th>Narration</th></tr></thead><tbody id="transactionresult"></tbody></table>               </div>            </div>         </div>      </div>   </div></div>');


	                            var ms_res = msg.ms_structure_model;
	                            var isArray = Array.isArray(ms_res);
	                             if (isArray) {
	                                $("#transactionresult").empty();
	                                $.each(msg.ms_structure_model,function(key,value){
	                                    $("#transactionresult").append(' <tr>   <td id="agentid">'+value.date+'</td>  <td id="amount">'+value.txnType+'</td> <td id="biller_name">&#x20b9; '+value.amount+'</td>  <td id="transaction_date">'+value.narration+'</td>  </tr> ');
	                                });
	                             }else{
	                                $("#transactionresult").empty();
	                                $("#transactionresult").append(' <tr>   <td id="agentid">'+ms_res.date+'</td>  <td id="amount">'+ms_res.txnType+'</td> <td id="biller_name">&#x20b9; '+ms_res.amount+'</td>  <td id="transaction_date">'+ms_res.narration+'</td>  </tr> ');
	                             }
	                        }
	                        $('#myModal').modal('show');
	                    }
	                }
	            },
	            error: function(d) {
	                alert("Something went wrong. Please contact administrator.");
	            }
	        });
		}
		else
		{
			//alert("Something went wrong");
		}
    }

	function ekyc_complete_button_send() {
         var SITEURL = '{{URL::to('')}}';
         var ekyc_res_mobile = $('#ekyc_res_mobile').val();
         var ekyc_res_primaryKeyId = $('#ekyc_res_primaryKeyId').val();
         var ekyc_res_encodeFPTxnId = $('#ekyc_res_encodeFPTxnId').val();

         var pidData = $('#txtPidData').val();

            xmlDoc = $.parseXML( pidData );
            $xml = $( xmlDoc );

            //var errCode = errInfo = fCount = fType = fType = nmPoints = qScore = "";
            //var dpId = rdsId = rdsVer = mi = mc = "";
            var PidDatatype = Piddata = ci = dc = dpId = errInfo = hmac = mc = wadh = mi = nmPoints = qScore = rdsId = rdsVer = sessionKey = "";
            errCode = fCount = fType = iCount = pCount = pType = 0;
            var bank_id = $('#ekyc_bank_detail').val();

            $xml.find("PidData").each(function(index,elem){
                  PidDatatype = jQuery(elem).find("Data").attr("type");
                  errInfo = jQuery(elem).find("Resp").attr("errInfo");
                  errCode = jQuery(elem).find("Resp").attr("errCode");
                  fCount = jQuery(elem).find("Resp").attr("fCount");
                  fType = jQuery(elem).find("Resp").attr("fType");
                  nmPoints = jQuery(elem).find("Resp").attr("nmPoints");
                  qScore = jQuery(elem).find("Resp").attr("qScore");

                  Piddata = jQuery(elem).find("Data").text();
                  sessionKey = jQuery(elem).find("Skey").text();

                  ci = jQuery(elem).find("Skey").attr('ci');
                  dc = jQuery(elem).find("DeviceInfo").attr('dc');
                  dpId = jQuery(elem).find("DeviceInfo").attr('dpId');

                  hmac = jQuery(elem).find("Hmac").text();
                  mc = jQuery(elem).find("DeviceInfo").attr('mc');
                  mi = jQuery(elem).find("DeviceInfo").attr('mi');
                  rdsId = jQuery(elem).find("DeviceInfo").attr('rdsId');
                  rdsVer = jQuery(elem).find("DeviceInfo").attr('rdsVer');
            });

            Swal.fire({
               title: 'Do you want to continue?',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes, Continue',
               confirmButtonClass: 'btn btn-primary',
               cancelButtonClass: 'btn btn-danger ml-1',
               buttonsStyling: false,
            }).then((data) => {
               if (data.value) {
                  $('#Dv_Loader').show();
                  $.ajax({
                    type:"post",
                    url:"/iciciEKYCcomplete", 
                    data : {
                        'ekyc_res_mobile':ekyc_res_mobile,
                        'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,
                        'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,
                        'bank_id' : bank_id,
                        'PidDatatype' : PidDatatype,
                        'Piddata' : Piddata,
                        'ci' : ci,
                        'dc' : dc,
                        'dpId' : dpId,
                        'errCode' : errCode,
                        'errInfo' : errInfo,
                        'fCount' : fCount,
                        'fType' : fType,
                        'hmac' : hmac,
                        'mc' : mc,
                        'iCount' : iCount,
                        'mi' : mi,
                        'nmPoints' : nmPoints,
                        'pCount' : pCount,
                        'pType' : pType,
                        'qScore' : qScore,
                        'rdsId' : rdsId,
                        'rdsVer' : rdsVer,
                        'sessionKey' : sessionKey,
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: "json",
                    success:function(data)
                    {   
                       $('#Dv_Loader').hide();
                       if (data.permission_code == "PER111") {
                         $("#response_message").empty();
                         $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                           
                           setTimeout(function () {
                             window.location.href = SITEURL + '/index';
                           }, 3000);
                        }else{
                          if(data.status == true){
                                $('#ekyc_button').hide();
                                $('#ekyc_resendOTPbutton').hide();
                                $('#ekyc_verifyOTPbutton').hide();
                                
                                $("#send_otp_message").empty();
                                $('#send_otp_message').html(data.message).css('color','green');

                                var timeleft = 3;
                                 var downloadTimer = setInterval(function(){
                                   if(timeleft <= 0){
                                     clearInterval(downloadTimer);
                                     $("#ekyc_countdown").empty();
                                     $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                   } else {
                                     $("#ekyc_countdown").empty();
                                     $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                   }
                                   timeleft -= 1;
                                 }, 875);
                                 setTimeout(function () {
                                    window.location.href = SITEURL + '/icici-aeps';
                                 }, 3000); 
                            }else{
                                $("#send_otp_message").empty();
                                $('#send_otp_message').html(data.message).css('color','red');
                            }
                        }
                    }
                 });
              } else if (data.dismiss === Swal.DismissReason.cancel) {
               Swal.fire({
                   title: 'Cancelled',
                   type: 'error',
                   confirmButtonClass: 'btn btn-success',
                 })
             }
           })
      }

      function ekyc_complete_button() {
         // check if mantra or morpho
         var ekyc_device = $('input[name="ekyc_device"]:checked').val();
         if(ekyc_device == "mantra")
         {
            discoverAvdm();
         }
         else
         {
            $('#Dv_Loader').show();
            RDService();
         }
         
         /*var SITEURL = '{{URL::to('')}}';
         var ekyc_res_mobile = $('#ekyc_res_mobile').val();
         var ekyc_res_primaryKeyId = $('#ekyc_res_primaryKeyId').val();
         var ekyc_res_encodeFPTxnId = $('#ekyc_res_encodeFPTxnId').val();

         if(ekyc_device == "mantra")
         {
            var pidData = $('#txtPidData').val();

            xmlDoc = $.parseXML( pidData );
            $xml = $( xmlDoc );

            //var errCode = errInfo = fCount = fType = fType = nmPoints = qScore = "";
            //var dpId = rdsId = rdsVer = mi = mc = "";
            var PidDatatype = Piddata = ci = dc = dpId = errInfo = hmac = mc = mi = nmPoints = qScore = rdsId = rdsVer = sessionKey = "";
            errCode = fCount = fType = iCount = pCount = pType = 0;
            var bank_id = $('#ekyc_bank_detail').val();

            $xml.find("PidData").each(function(index,elem){
                  PidDatatype = jQuery(elem).find("Data").attr("type");
                  errInfo = jQuery(elem).find("Resp").attr("errInfo");
                  errCode = jQuery(elem).find("Resp").attr("errCode");
                  fCount = jQuery(elem).find("Resp").attr("fCount");
                  fType = jQuery(elem).find("Resp").attr("fType");
                  nmPoints = jQuery(elem).find("Resp").attr("nmPoints");
                  qScore = jQuery(elem).find("Resp").attr("qScore");

                  Piddata = jQuery(elem).find("Data").text();
                  sessionKey = jQuery(elem).find("Skey").text();

                  ci = jQuery(elem).find("Skey").attr('ci');
                  dc = jQuery(elem).find("DeviceInfo").attr('dc');
                  dpId = jQuery(elem).find("DeviceInfo").attr('dpId');

                  hmac = jQuery(elem).find("Hmac").text();
                  mc = jQuery(elem).find("DeviceInfo").attr('mc');
                  mi = jQuery(elem).find("DeviceInfo").attr('mi');
                  rdsId = jQuery(elem).find("DeviceInfo").attr('rdsId');
                  rdsVer = jQuery(elem).find("DeviceInfo").attr('rdsVer');
            });

            Swal.fire({
               title: 'Do you want to continue?',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes, Continue',
               confirmButtonClass: 'btn btn-primary',
               cancelButtonClass: 'btn btn-danger ml-1',
               buttonsStyling: false,
            }).then((data) => {
               if (data.value) {
                  $('#Dv_Loader').show();
                  $.ajax({
                    type:"get",
                    url:"/iciciEKYCcomplete", 
                    data : {
                        'ekyc_res_mobile':ekyc_res_mobile,
                        'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,
                        'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,
                        'bank_id' : bank_id,
                        'PidDatatype' : PidDatatype,
                        'Piddata' : Piddata,
                        'ci' : ci,
                        'dc' : dc,
                        'dpId' : dpId,
                        'errCode' : errCode,
                        'errInfo' : errInfo,
                        'fCount' : fCount,
                        'fType' : fType,
                        'hmac' : hmac,
                        'iCount' : iCount,
                        'mi' : mi,
                        'mc' : mc,
                        'nmPoints' : nmPoints,
                        'pCount' : pCount,
                        'pType' : pType,
                        'qScore' : qScore,
                        'rdsId' : rdsId,
                        'rdsVer' : rdsVer,
                        'sessionKey' : sessionKey
                    },
                    dataType: "json",
                    success:function(data)
                    {   
                       $('#Dv_Loader').hide();
                       if (data.permission_code == "PER111") {
                         $("#response_message").empty();
                         $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                           
                           setTimeout(function () {
                             window.location.href = SITEURL + '/index';
                           }, 3000);
                        }else{
                          if(data.status == true){
                                $('#ekyc_button').hide();
                                $('#ekyc_resendOTPbutton').hide();
                                $('#ekyc_verifyOTPbutton').hide();
                                
                                $("#send_otp_message").empty();
                                $('#send_otp_message').html(data.message).css('color','green');

                                var timeleft = 3;
                                 var downloadTimer = setInterval(function(){
                                   if(timeleft <= 0){
                                     clearInterval(downloadTimer);
                                     $("#ekyc_countdown").empty();
                                     $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                   } else {
                                     $("#ekyc_countdown").empty();
                                     $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                   }
                                   timeleft -= 1;
                                 }, 875);
                                 setTimeout(function () {
                                    window.location.href = SITEURL + '/icici-aeps';
                                 }, 3000); 
                            }else{
                                $("#send_otp_message").empty();
                                $('#send_otp_message').html(data.message).css('color','red');
                            }
                        }
                    }
                 });
              } else if (data.dismiss === Swal.DismissReason.cancel) {
               Swal.fire({
                   title: 'Cancelled',
                   type: 'error',
                   confirmButtonClass: 'btn btn-success',
                 })
             }
           })
         }*/
   }

   function ekyc_complete_button_demo() {
          var SITEURL = '{{URL::to('')}}';
          var ekyc_res_mobile = $('#ekyc_res_mobile').val();
          var ekyc_res_primaryKeyId = $('#ekyc_res_primaryKeyId').val();
          var ekyc_res_encodeFPTxnId = $('#ekyc_res_encodeFPTxnId').val();
          
          Swal.fire({
             title: 'Do you want to continue?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Continue',
             confirmButtonClass: 'btn btn-primary',
             cancelButtonClass: 'btn btn-danger ml-1',
             buttonsStyling: false,
           }).then((data) => {
            if (data.value) {
              $('#Dv_Loader').show();
              $.ajax({
                 type:"post",
                 url:"/iciciEKYCcomplete", 
                 data : {'ekyc_res_mobile':ekyc_res_mobile,'ekyc_res_primaryKeyId':ekyc_res_primaryKeyId,'ekyc_res_encodeFPTxnId':ekyc_res_encodeFPTxnId,},
                 dataType: "json",
                 success:function(data)
                 {   
                    $('#Dv_Loader').hide();
                    if (data.permission_code == "PER111") {
                      $("#response_message").empty();
                      $("#response_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                        
                        setTimeout(function () {
                          window.location.href = SITEURL + '/index';
                        }, 3000);
                     }else{
                       if(data.status == true){
                             $('#ekyc_button').hide();
                             $('#ekyc_resendOTPbutton').hide();
                             $('#ekyc_verifyOTPbutton').hide();
                             
                             $("#send_otp_message").empty();
                             $('#send_otp_message').html(data.message).css('color','green');

                             var timeleft = 3;
                              var downloadTimer = setInterval(function(){
                                if(timeleft <= 0){
                                  clearInterval(downloadTimer);
                                  $("#ekyc_countdown").empty();
                                  $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                } else {
                                  $("#ekyc_countdown").empty();
                                  $("#ekyc_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                                }
                                timeleft -= 1;
                              }, 875);
                              setTimeout(function () {
                                 window.location.href = SITEURL + '/icici-aeps';
                              }, 3000); 
                         }else{
                             $("#send_otp_message").empty();
                             $('#send_otp_message').html(data.message).css('color','red');
                         }
                     }
                 }
              });
           } else if (data.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
                title: 'Cancelled',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
              })
          }
        })
   }
</script>

@endsection