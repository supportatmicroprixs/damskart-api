@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   .border {
   border: 2px solid #EDEDED !important;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">UPI</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Report
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#getreceiptModal">Receipt</button> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('upi-report-filter') }}" method="post" name="fund_transfer_form" class="form-horizontal">
                              @csrf

                              <div class="col-12">
                                 <div class="table-responsive border rounded px-1 ">
                                    <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i class="fa fa-filter mr-50 "></i>Filter</h6>
                                    <div class="row match-height">
                                       <div class="col-md-12">
                                          <div class="row">
                                             <div class="col-md-12">
                                                <div class="card" style="margin-bottom: 0rem;">
                                                   <!-- <div class="card-header">
                                                      <h4 class="card-title">Filter</h4>
                                                      </div> -->
                                                   <div class="card-content">
                                                      <div class="card-body">
                                                         <div class="row">
                                                            <div class="col-md-5 col-12 mb-1">
                                                               <fieldset>
                                                                  <div class="input-group">
                                                                     <div class="input-group-prepend">
                                                                        <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                                                     </div>
                                                                     <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                                                  </div>
                                                               </fieldset>
                                                            </div>
                                                            <?php if($user->role == "super admin"): ?>
                                                            <div class="col-md-5 col-12 mb-1">
                                                               <fieldset>
                                                                  <div class="input-group">
                                                                     <div class="input-group-prepend">
                                                                        <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                                                     </div>
                                                                     <select name="package_id" id="package_id" class="form-control">
                                                                        <option value="" selected="">Select Package</option>
                                                                        @foreach($package as $item)
                                                                        <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                                        @endforeach
                                                                     </select>
                                                                  </div>
                                                               </fieldset>
                                                            </div>
                                                            <?php endif ?>
                                                            <div class="col-md-2 col-12 mb-1">
                                                               <div class="input-group-append">
                                                                  <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>

                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                           <ul class="nav nav-tabs justify-content-center" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#Generate-QR" aria-controls="home-center" role="tab" aria-selected="true">Generate QR</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#SEND-REQUEST" aria-controls="service-center" role="tab" aria-selected="false">SEND REQUEST</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#My-QR" aria-controls="service-center" role="tab" aria-selected="false">My QR</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="Generate-QR" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="table-responsive" id="alltxn">
                                    <table id="example" class="table table-striped table-bordered">
                                       <thead>
                                          <tr>
                                             <th >Txn ID</th>
                                             <th >Txn Date</th>
                                             <th >Txn Type</th>
                                             <th >Mobile No.</th>
                                             <th >Original Txn ID</th>
                                             <th >Amount</th>
                                             <th >Status</th>
                                             <th >Action</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          @foreach($upiqrtxn as $item)
                                          <tr>
                                             <td >XX{{ substr($item->dms_txn_id, 9) }}</td>
                                             <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                             <td>{{ str_replace("_", " ", ucfirst($item->txn_type)) }}</td>
                                             <td>{{$item->name}} <br /> {{$item->mobile}}</td>
                                             <td>{{$item->bankRRN}}</td>
                                             <td> {{number_format($item->payer_amount , 3)}}</td>
                                             <td>{{$item->status}}</td>
                                             <td>
                                                &nbsp; <a href="/upitxnStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> 
                                                <!-- <button type="button"  id="upitxnStatusCheck" data-mrid="{{$item->merchantTranId}}" onclick="upitxnStatusCheck()" class="btn btn-primary mr-1 mb-1 check_status">Txn Status</button> -->
                                                &nbsp; <a href="/upicallbackStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a> 
                                                <!-- @if($item->status == "SUCCESS")
                                                   &nbsp; <a href="/upirefundRequest/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Refund Request"><i style="color: red;" class="feather icon-corner-down-left"></i></a>
                                                   @endif -->
                                             </td>
                                          </tr>
                                          @endforeach 
                                       </tbody>
                                    </table>
                                 </div>
                                 <!-- search transactions table-->
                                 <div class="table-responsive" id="searchtxn">
                                 </div>
                                 <!-- search transactions table-->
                              </div>
                              <div class="tab-pane " id="SEND-REQUEST" aria-labelledby="service-tab-center" role="tabpanel">
                                 <div class="table-responsive" id="allRequesttxn">
                                    <table id="example4" class="table table-striped table-bordered">
                                       <thead>
                                          <tr>
                                             <th >Txn ID</th>
                                             <th >Txn Date</th>
                                             <th >Txn Type</th>
                                             <th >Payer Detail</th>
                                             <th >Original Txn ID</th>
                                             <th >Amount</th>
                                             <th >Status</th>
                                             <th >Action</th>
                                          </tr>
                                       </thead>
                                       <tbody id="databox">
                                          @foreach($upisendreqtxn as $item)
                                          <tr>
                                             <td >XX{{ substr($item->dms_txn_id, 9) }}</td>
                                             <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                             <td>{{ str_replace("_", " ", ucfirst($item->txn_type)) }}</td>
                                             <td>{{ucfirst($item->name)}} <br /> {{$item->mobile}} <br />{{$item->upi_id}}</td>
                                             <td>{{$item->bankRRN}}</td>
                                             <td> {{number_format($item->payer_amount , 3)}}</td>
                                             <td>{{$item->status}}</td>
                                             <td>
                                                &nbsp; <a href="/upitxnStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> 
                                                &nbsp; <a href="/upicallbackStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Callback Txn Status" ><i style="color: blue;" class="feather icon-check-circle"></i></a>
                                                <!-- @if($item->status == "SUCCESS")
                                                   &nbsp; <a href="/upirefundRequest/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Refund Request" ><i style="color: red;" class="feather icon-corner-down-left"></i></a>
                                                   @endif -->
                                             </td>
                                          </tr>
                                          @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                                 <!-- search transactions table-->
                                 <div class="table-responsive" id="searchRequesttxn">
                                 </div>
                                 <!-- search transactions table-->
                              </div>
                              <div class="tab-pane " id="My-QR" aria-labelledby="service-tab-center" role="tabpanel">
                                 <div class="table-responsive" id="allmyqrtxn">
                                    <table id="example5" class="table table-striped table-bordered">
                                       <thead>
                                          <tr>
                                             <th >Txn ID</th>
                                             <th >Txn Date</th>
                                             <th >Txn Type</th>
                                             <th >Payer Detail</th>
                                             <th >Original Txn ID</th>
                                             <th >Amount</th>
                                             <th >Status</th>
                                             <th >Action</th>
                                          </tr>
                                       </thead>
                                       <tbody id="databox">
                                          @foreach($upimyqrtxn as $item)
                                          <tr>
                                             <td >XX{{ substr($item->dms_txn_id, 9) }}</td>
                                             <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                             <td>{{ str_replace("_", " ", ucfirst($item->txn_type)) }}</td>
                                             <td>{{$item->name}} <br />{{$item->mobile}}</td>
                                             <td>{{$item->bankRRN}}</td>
                                             <td> {{number_format($item->payer_amount , 3)}}</td>
                                             <td>{{$item->status}}</td>
                                             <td>
                                                &nbsp; <a href="/upitxnStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> 
                                                &nbsp; <a href="/upicallbackStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a> 
                                                <!-- @if($item->status == "SUCCESS")
                                                   &nbsp; <a href="/upirefundRequest/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Refund Request"><i style="color: red;" class="feather icon-corner-down-left"></i></a>
                                                   @endif -->
                                             </td>
                                          </tr>
                                          @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                                 <!-- search transactions table-->
                                 <div class="table-responsive" id="searchmyqrtxn">
                                 </div>
                                 <!-- search transactions table-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal fade" id="getreceiptModal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="Resp_request_type">Generate QR</h4>
                          </div> 
                          <div class="modal-body  print-container" id="getCode" >
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                          <div id="invoice-template" class="card-body">
                                              <div id="invoice-company-details" class="row">
                                                  <div class="col-sm-6 col-12 text-left pt-1">
                                                      <div class="media pt-1">
                                                          <img src="{{asset('images/upi_icon.png')}}" style="width: 120px;">
                                                      </div>
                                                  </div> 
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <h1>Transaction Details</h1>
                                                      <div class="invoice-details mt-2">
                                                          <h6>UPI Transaction ID</h6>
                                                          <p id="Resp_upi_txnid"></p>
                                                          <h6>Txn Date & Time</h6>
                                                          <p><h5 id="Resp_txn_dateTime"></h5></p>
                                                          <h6>Transaction Status</h6>
                                                          <p id="Resp_txn_status" style="font-size: xx-large;color: green;"></p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-customer-details" class="row pt-2 ">
                                                  <div class="col-sm-6 col-12 text-left">
                                                      <div class="recipient-info my-2">
                                                          <h6>Sender Name</h6>
                                                          <p id="Resp_sender_name"></p>
                                                          <h6>Sender Mobile</h6>
                                                          <p id="Resp_sender_mobile"></p>
                                                          <h6>Request Amount</h6>
                                                          <p id="Resp_request_amount"></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <div class="company-info my-2">
                                                          <h6>Payer Name</h6>
                                                          <p id="Resp_payer_name"></p>
                                                          <h6>Payer UPI Id</h6>
                                                          <p id="Resp_payer_upi_id"></p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                  <div class="row">
                                                      <div class="table-responsive col-12">
                                                          <table class="table table-borderless">
                                                              <tbody>
                                                                  <tr>
                                                                      <td class="text-left"><b>Transaction Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right"><h5 id="Resp_txn_amount"></h5></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Commission (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right" id="Resp_commission"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Wallet Balance (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right" id="Resp_wallet_bal"></td>
                                                                  </tr>
                                                                 
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <p id="countdown" class="text-center"></p>
                          <div class="modal-footer" >
                             <!-- <h5>Thanks for your Business!</h5> -->
                             <button type="button" class="btn btn-secondary"   data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                          </div>
                       </div>
                    </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
</script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $('#example4').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
   $('#example5').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
</script>
<script type="text/javascript">
// function fundtransferButton() {
   $('.check_status').click(function(){
    var SITEURL = '{{URL::to('')}}';
     var merchantTranId = $(this).data('mrid');
    Swal.fire({
        title: 'Do you want to continue?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Continue',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
       }).then((data) => {
        if (data.value) {
          $('#Dv_Loader').show();
            $.ajax({
              type:"POST",
              url:"/upitxnStatusCheck", 
              data : {'merchantTranId':merchantTranId,},
              dataType: "json",
              success:function(data)
              {   
                $('#Dv_Loader').hide();
                if (data.permission_code == "PER111") {
                  $("#resp_message").empty();
                  $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                    
                    setTimeout(function () {
                      window.location.href = SITEURL + '/index';
                    }, 3000);
                 }else{
                    // if (data.code == "000" ) {
                        // alert(data.code);
                        $("#errormessage").empty();

                        var Res_request_type = data.txn_type;
                        var Res_upi_txnid = data.merchantTranId;
                        var Res_txn_dateTime = moment(data.created_at).format('DD-MM-YYYY hh:mm:ss A');
                        var Res_txn_status = data.status;
                        var Res_sender_name = data.name;
                        var Res_sender_mobile = data.mobile;
                        var Res_request_amount = data.amount;
                        var Res_payer_name = data.payer_name;
                        var Res_payer_upi_id = data.payerVA;
                        var Res_txn_amount = data.payer_amount;
                        var Res_commission = data.commission;
                        var Res_wallet_bal = data.current_balance;
                      
                      $('#Resp_request_type').html(Res_request_type);
                      $('#Resp_upi_txnid').html(Res_upi_txnid);
                      $('#Resp_txn_dateTime').html(Res_txn_dateTime);
                      $('#Resp_txn_status').html(Res_txn_status);
                      $('#Resp_sender_name').html(Res_sender_name);
                      $('#Resp_sender_mobile').html(Res_sender_mobile);
                      $('#Resp_request_amount').html(Res_request_amount);
                      $('#Resp_payer_amount').html(Res_payer_name);
                      $('#Resp_payer_upi_id').html(Res_payer_upi_id);
                      $('#Resp_txn_amount').html(Res_txn_amount);
                      $('#Resp_commission').html(Res_commission);
                      $('#Resp_wallet_bal').html(Res_wallet_bal);

                      $('#getreceiptModal').modal({backdrop: 'static', keyboard: false});

                    // }else{
                    //   var error = data.message;
                    //     $("#errormessage").empty();
                    //     $('#errormessage').html(error).css('color', 'red');
                    // }
                  }
              }
           });
        } else if (data.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
              title: 'Cancelled',
              type: 'error',
              confirmButtonClass: 'btn btn-success',
            })
        }
    })
});
// }
</script>


@endsection