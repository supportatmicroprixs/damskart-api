@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Fund Transfer Surcharge</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">Setting
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Center</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                           <ul class="nav nav-tabs justify-content-center" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#IMPS" aria-controls="home-center" role="tab" aria-selected="true">IMPS</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#NEFT " aria-controls="service-center" role="tab" aria-selected="false">NEFT </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" id="account-tab-center" data-toggle="tab" href="#RTGS" aria-controls="account-center" role="tab" aria-selected="false">RTGS</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="IMPS" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">IMPS</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updateadminfundtransferIMPS',$fundtransfer_IMPS->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="payout" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Surcharge</th>
                                                               <th scope="col">Surcharge Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                                $commission = json_decode($fundtransfer_IMPS->commission);
                                                                 if ($fundtransfer_IMPS->commission == 'null') {
                                                                  echo "
                                                                  
                                                                        <tr>
                                                               
                                                                           <td>
                                                                              <input type='text' name='store[0][start_price]' placeholder='Start Price'  value='' id='start_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][end_price]' placeholder='End Price'  id='end_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][commission]' placeholder='Surcharge' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                             
                                                                              <select class='form-control round' name='store[0][commission_type]' required>
                                                                                 <option>Select Type</option>
                                                                                 <option value='flat'>Flat</option>
                                                                                 <option value='percentage'>Percentage</option>
                                                                              </select>
                                                                           </td>
                                                                           <td>
                                                                              <button type='button' name='add' id='addpayout' class='btn btn-primary round'>Add More</button>
                                                                           </td>
                                                                        </tr>
                                                                     ";
                                                                }else{
                                                                 foreach ($commission as $key => $value) {
                                                                    $abc = $key;
                                                                  echo "<tr>
                                                                  <td><input type='text' name='store[".$key."][start_price]' placeholder='Start Price'  id='start_price'  value='$value->start_price' class='form-control round'  required /></td>
                                                                  <td><input type='text' name='store[".$key."][end_price]' placeholder='End Price' value='$value->end_price' id='end_price' class='form-control round' required /></td>  
                                                                  <td><input type='text' name='store[".$key."][commission]' placeholder='Surcharge' value='$value->commission' class='form-control round' required /></td>  
                                                                  <td>
                                                                      
                                                                     <select class='form-control round' name='store[".$key."][commission_type]' required >";
                                                                       echo '<option value="flat"';
                                                                        if ($value->commission_type =='flat') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Flat</option>';
                                                                        echo '<option value="percentage"';
                                                                        if ($value->commission_type =='percentage') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Percentage</option>';
                                                                      
                                                                  echo"  </select>
                                                                  </td>
                                                                  ";
                                                                  ?>
                                                            <td>
                                                               <?php if ($key == 0): ?>
                                                               <button type='button' name='add' id='addpayout' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="amount(<?php echo $i; ?>)">Remove</button>
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue' value=".$abc.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane " id="NEFT" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">NEFT</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updateadminfundtransferNEFT',$fundtransfer_NEFT->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="payout_NEFT" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Surcharge</th>
                                                               <th scope="col">Surcharge Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                                $commission2 = json_decode($fundtransfer_NEFT->commission);
                                                                 if ($fundtransfer_NEFT->commission == 'null') {
                                                                  echo "
                                                                  
                                                                        <tr>
                                                               
                                                                           <td>
                                                                              <input type='text' name='store[0][start_price]' placeholder='Start Price'  value='' id='start_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][end_price]' placeholder='End Price'  id='end_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][commission]' placeholder='Surcharge' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                             
                                                                              <select class='form-control round' name='store[0][commission_type]' required>
                                                                                 <option>Select Type</option>
                                                                                 <option value='flat'>Flat</option>
                                                                                 <option value='percentage'>Percentage</option>
                                                                              </select>
                                                                           </td>
                                                                           <td>
                                                                              <button type='button' name='addpayout_NEFT' id='addpayout_NEFT' class='btn btn-primary round'>Add More</button>
                                                                           </td>
                                                                        </tr>
                                                                     ";
                                                                }else{
                                                                 foreach ($commission2 as $key_NEFT => $value) {
                                                                    $abc2 = $key_NEFT;
                                                                  echo "<tr>
                                                                  <td><input type='text' name='store[".$key_NEFT."][start_price]' placeholder='Start Price'  id='start_price'  value='$value->start_price' class='form-control round'  required /></td>
                                                                  <td><input type='text' name='store[".$key_NEFT."][end_price]' placeholder='End Price' value='$value->end_price' id='end_price' class='form-control round' required /></td>  
                                                                  <td><input type='text' name='store[".$key_NEFT."][commission]' placeholder='Surcharge' value='$value->commission' class='form-control round' required /></td>  
                                                                  <td>
                                                                      
                                                                     <select class='form-control round' name='store[".$key_NEFT."][commission_type]' required >";
                                                                       echo '<option value="flat"';
                                                                        if ($value->commission_type =='flat') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Flat</option>';
                                                                        echo '<option value="percentage"';
                                                                        if ($value->commission_type =='percentage') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Percentage</option>';
                                                                      
                                                                  echo"  </select>
                                                                  </td>
                                                                  ";
                                                                  ?>
                                                            <td>
                                                               <?php if ($key_NEFT == 0): ?>
                                                               <button type='button' name='addpayout_NEFT' id='addpayout_NEFT' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="amount(<?php echo $i; ?>)">Remove</button>
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue_NEFT' value=".$abc2.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane " id="RTGS" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">RTGS</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updateadminfundtransferRTGS',$fundtransfer_RTGS->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="payout_RTGS" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Surcharge</th>
                                                               <th scope="col">Surcharge Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                                $commission3 = json_decode($fundtransfer_RTGS->commission);
                                                                 if ($fundtransfer_RTGS->commission == 'null') {
                                                                  echo "
                                                                  
                                                                        <tr>
                                                               
                                                                           <td>
                                                                              <input type='text' name='store[0][start_price]' placeholder='Start Price'  value='' id='start_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][end_price]' placeholder='End Price'  id='end_price' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                              <input type='text' name='store[0][commission]' placeholder='Surcharge' class='form-control round' required/>
                                                                           </td>
                                                                           <td>
                                                                             
                                                                              <select class='form-control round' name='store[0][commission_type]' required>
                                                                                 <option>Select Type</option>
                                                                                 <option value='flat'>Flat</option>
                                                                                 <option value='percentage'>Percentage</option>
                                                                              </select>
                                                                           </td>
                                                                           <td>
                                                                              <button type='button' name='addpayout_RTGS' id='addpayout_RTGS' class='btn btn-primary round'>Add More</button>
                                                                           </td>
                                                                        </tr>
                                                                     ";
                                                                }else{
                                                                 foreach ($commission3 as $key_RTGS => $value) {
                                                                    $abc3 = $key_RTGS;
                                                                  echo "<tr>
                                                                  <td><input type='text' name='store[".$key_RTGS."][start_price]' placeholder='Start Price'  id='start_price'  value='$value->start_price' class='form-control round'  required /></td>
                                                                  <td><input type='text' name='store[".$key_RTGS."][end_price]' placeholder='End Price' value='$value->end_price' id='end_price' class='form-control round' required /></td>  
                                                                  <td><input type='text' name='store[".$key_RTGS."][commission]' placeholder='Surcharge' value='$value->commission' class='form-control round' required /></td>  
                                                                  <td>
                                                                      
                                                                     <select class='form-control round' name='store[".$key_RTGS."][commission_type]' required >";
                                                                       echo '<option value="flat"';
                                                                        if ($value->commission_type =='flat') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Flat</option>';
                                                                        echo '<option value="percentage"';
                                                                        if ($value->commission_type =='percentage') {
                                                                           echo ' selected';
                                                                        }
                                                                        echo '>Percentage</option>';
                                                                      
                                                                  echo"  </select>
                                                                  </td>
                                                                  ";
                                                                  ?>
                                                            <td>
                                                               <?php if ($key_RTGS == 0): ?>
                                                               <button type='button' name='addpayout_RTGS' id='addpayout_RTGS' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="amount(<?php echo $i; ?>)">Remove</button>
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue_RTGS' value=".$abc3.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection