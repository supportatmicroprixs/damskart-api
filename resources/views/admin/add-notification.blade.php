@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Add Notification</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Notification
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Transfer Fund</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form  action="{{ route('add-notification') }}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                              {{csrf_field()}}
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Role</label>
                                          <div class="position-relative has-icon-left">
                                             <select class="form-control" name="role">
                                                <option value="" disabled="" selected="">Select Role</option>
                                                <option value="all">All</option>
                                                <option value="manager">Manager</option>
                                                <option value="sdistributor">Super Distributor</option>
                                                <option value="mdistributor">Master Distributor</option>
                                                <option value="distributor">Distributor</option>
                                                <option value="retailer">Retailer</option>
                                             </select>
                                             <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Title</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="text" value="{{ old('title') }}" id="title" name="title"  class="form-control" placeholder="Notification Title" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-edit-2"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Date</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="date" value="{{ old('date') }}" id="date" name="date" class="form-control" placeholder="Date" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-calendar"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Time</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="time" value="{{ old('notification_time') }}" id="time" name="notification_time" class="form-control" placeholder="Time" required="">
                                             <div class="form-control-position">
                                                <i class="feather icon-calendar"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group">
                                          <label for="password-icon">Message</label>
                                          <div class="position-relative has-icon-left">
                                             <textarea class="form-control" cols="3" rows="3" placeholder="Message" name="message" value="{{ old('message') }}">{{ old('message') }}</textarea>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection