@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .text-white{
   color: white;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">AEPS Wallet</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Wallet
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <!-- Column selectors with Export Options and print table -->
         @if(Auth::user()->role == 'retailer' || Auth::user()->role == 'mdistributor'|| Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'distributor')
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-4">
                  <div class="card" style="background: linear-gradient(to right, #7367F0, #968df4);">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Merchant ID : 1234567890</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <div class="card-block text-center" style="font-family: inherit;">
                              <h6 class="text-white">Marchent ID : {{Auth::user()->mobile}}</h6>
                              <br>
                              <div class="m-b-25">
                                 <img src="{{asset('images/aeps (1).png')}}" style="width: 100px;" class="img-radius" alt="Aeps-walllet">
                              </div>
                              <br>
                              <h6 class="text-white">Total AEPS Balance : &#x20B9; {{number_format(Auth::user()->aeps_wallet,3)}}</h6>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Transfer Fund To Main Wallet</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('wallet-transaction') }}" method="POST" enctype="multipart/form-data" class="form">
                             {{csrf_field()}}
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-label-group position-relative has-icon-left">
                                          <input type="number" min="0" id="amount" value="{{ old('amount') }}" class="form-control" name="amount" placeholder="Amount" required="">
                                          <div class="form-control-position">
                                             <i class="fa fa-inr"></i>
                                          </div>
                                          <label for="amount">Amount</label>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <div class=" form-label-group position-relative has-icon-left">
                                             <textarea class="form-control" cols="3" rows="3" placeholder="Enter Remarks" name="naration" required></textarea>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          <label for="password-icon">Narration</label>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">AEPS Transactions</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                         <th>Name</th>
                                         <th>Transaction Date</th>
                                         <th>Transaction ID</th>
                                         <th>Narration</th>
                                         <th>Amount</th>
                                         <th>Type</th>
                                         <th>Balance</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                      @foreach ($wallet_transaction as $key)
                                        <tr>
                                          @if(Auth::user()->role == 'super admin') 
                                            <td><a href="{{route('view-member', $key->id)}}">{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span>({{$key->mobile}})</span></a></td>
                                          @else 
                                            <td>{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span>({{$key->mobile}})</span></td>
                                          @endif
                                          <td><span style="display: none;">{{date('YmdHis', strtotime($key->date))}}</span>{{date('d-m-Y h:i:s A', strtotime($key->date))}}</td>
                                          <td>{{$key->transaction_id}}</td>
                                          <td>{{$key->narration}}</td>
                                          @if($key->type == 'credit')
                                            <td style="color: green;">&#x20B9; {{number_format($key->amount,3)}}</td>
                                          @elseif($key->type == 'debit')
                                            <td style="color: red;">&#x20B9; {{number_format($key->amount,3)}}</td>
                                          @endif
                                          <td>{{ucfirst($key->type)}}</td>
                                          <td>&#x20B9; {{number_format($key->current_balance,3)}}</td>
                                        </tr>
                                      @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection