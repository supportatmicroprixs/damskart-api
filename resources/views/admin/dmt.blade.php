@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   .input_style{
   background-color: #ffffff !important;
   padding: 0.0rem 0.0em;
   border: 0px solid #D9D9D9;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">DMT</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Service
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-content">
                        <!-- <button type="button" class="btn btn-success" data-backdrop="false" data-toggle="modal" data-target="#verifyOTPModal">verifyOTPModal</button> -->
                        <!-- <button type="button" class="btn btn-outline-primary float-right" data-backdrop="false" data-toggle="modal" data-target="#addrecipientModal">Add Recipient</button> -->
                        <!-- <button type="button" class="btn btn-outline-primary float-right" data-backdrop="false" data-toggle="modal" data-target="#addfundtransfer">addfundtransfer</button> -->
                        <div class="divider divider-center">
                           <div class="divider-text">
                              <img src="{{ asset('images/dmt_icon.png') }}" width="70" alt="icici">
                           </div>
                        </div>
                        <div class="card-body">
                           <div class="form-body">
                              <div class="form-group" id="errormessage"></div>
                              <div class="form-group" id="successmessage"></div>

                              <div class="row">
                                 <div class="col-md-6" id="loginForm">
                                    <div class="form-group d-none hide">
                                       <label for="contact-info-icon">Name</label>
                                       <div class="position-relative has-icon-left">
                                          <input type="hidden" class="" name="customer_name"  id="customer_name" onkeyup="sendOtpDetected()" placeholder="Enter Name">
                                          <div class="form-control-position">
                                             <i class="feather icon-user"></i>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="contact-info-icon">Mobile</label>
                                       <div class="position-relative has-icon-left">
                                          <input type="number" class="form-control" minlength="10" maxlength="10" name="customer_mobile"  id="customer_mobile" onkeyup="sendOtpDetected()" placeholder="Enter Mobile Number">
                                          <div class="form-control-position">
                                             <i class="feather icon-smartphone"></i>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <!-- <label for="contact-info-icon">Pin</label> -->
                                       <div class="position-relative has-icon-left">
                                          <input type="hidden" class="form-control" minlength="6" maxlength="6" name="customer_pin" id="customer_pin" value="123456" placeholder="Enter Mobile Number">
                                          <!-- <div class="form-control-position">
                                             <i class="feather icon-hash"></i>
                                          </div> -->
                                       </div>
                                    </div>
                                    
                                 </div>
                                  
                                 <div class="col-12">
                                    <div id="payment_button"> 
                                       <button class="btn btn-primary" id="getotpbutton" onclick="checkRegistered()" type="button" disabled=""> Send OTP</button>
                                    </div>
                                 </div>


                                 	<div class="col-12" id="registerForm" style="display:none;">
                                 		<div class="form-group">
	                                       <label for="contact-info-icon">Name</label>
	                                       <div class="position-relative has-icon-left">
	                                          <input type="text" required="" class="form-control" name="user_customer_name"  id="user_customer_name" placeholder="Enter Name">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-user"></i>
	                                          </div>
	                                       </div>
	                                    </div>
	                                    <div class="form-group">
	                                       <label for="contact-info-icon">Mobile</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<input type="number" required="" class="form-control" minlength="10" maxlength="10" name="user_customer_mobile" id="user_customer_mobile"   placeholder="Enter Mobile Number">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-smartphone"></i>
	                                          </div>
	                                       </div>
	                                    </div>

	                                    <div class="form-group">
	                                       <label for="contact-info-icon">Email</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<input type="email" class="form-control" name="user_customer_email" id="user_customer_email"  placeholder="Enter Email">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-mail"></i>
	                                          </div>
	                                       </div>
	                                    </div>

	                                    <div class="form-group">
	                                       <label for="contact-info-icon">Aadhar Number</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<input type="text" required="" class="form-control" minlength="12" maxlength="12" name="user_customer_aadhar" id="user_customer_aadhar"   placeholder="Enter Aadhar Number">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-smartphone"></i>
	                                          </div>
	                                       </div>
	                                    </div>

	                                    <div class="form-group">
	                                       <label for="contact-info-icon">PAN Number</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<input type="text" required="" class="form-control" minlength="10" maxlength="10" name="user_customer_pan" id="user_customer_pan"   placeholder="Enter PAN Number" pattern="^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-smartphone"></i>
	                                          </div>
	                                       </div>
	                                    </div>

	                                    <div class="form-group">
	                                       <label for="contact-info-icon">Address</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<textarea class="form-control" required="" name="user_customer_address" id="user_customer_address"  placeholder="Enter Address"></textarea>
	                                          <div class="form-control-position">
	                                             <i class="feather icon-smartphone"></i>
	                                          </div>
	                                       </div>
	                                    </div>


	                                    <div class="form-group">
	                                       <label for="contact-info-icon">PIN Code</label>
	                                       <div class="position-relative has-icon-left">
	                                          	<input type="number" required="" class="form-control" minlength="6" maxlength="6" name="user_customer_pincode" id="user_customer_pincode"  placeholder="Enter PIN Code">
	                                          <div class="form-control-position">
	                                             <i class="feather icon-map-pin"></i>
	                                          </div>
	                                       </div>
	                                    </div>

	                                    <div class="form-group">
	                                       <div class="position-relative has-icon-left">
	                                          <input type="text" readonly="" class="form-control" minlength="6" maxlength="6" name="user_customer_pin" id="user_customer_pin" value="{{ $rand_value }}" placeholder="Enter Mobile Number">
	                                          <!-- <div class="form-control-position">
	                                             <i class="feather icon-hash"></i>
	                                          </div> -->
	                                       </div>
	                                    </div>
                                	</div>

                                	<div class="col-12">
	                                    <div id="signup_button" style="display:none;"> 
	                                       <button class="btn btn-primary" id="getregisterbutton" onclick="sendOTP()" type="button"> Register</button>
	                                    </div>
	                                </div>
                                 <div class="col-12" id="receipentDataList">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal fade" id="verifyOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                        <div class="modal-content">
                           <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                              <h4 class="modal-title" style=" color: white;" id="myModalLabel">DMT</h4>
                               <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           </div>
                           <div class="modal-body" id="getCode" >
                              <div id="invoice-template" class="card-body">
                                 <div id="invoice-company-details" class="row">
                                    <div class="col-sm-6 col-12 text-left pt-1">
                                       <div class="media">
                                          <img src="{{asset('images/dmt_icon.png')}}" style="width: 100px;">
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                       <h1>Verify Otp</h1>
                                       <div class="invoice-details mt-2">
                                          <h5 id="respDesc"></h5>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-1">
                                    <div class="row">
                                       <div class="table-responsive col-12">
                                          <table class="table table-borderless" style="margin-top: 1rem;">
                                             <tbody>
                                                <tr>
                                                   <td><b>OTP</b></td>
                                                   <td>
                                                      <input type="text" class="form-control" id="otp" onkeyup="otpDetected()" placeholder="Enter OTP" required>
                                                      <input type="hidden" class="form-control" id="additionalRegData">
                                                      <input type="hidden" class="form-control" id="senderMobileNumber">
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer" style="justify-content: left;">
                              <button type="submit" id="verifyOTPbutton" onclick="verifyOTPbutton()" class="btn btn-primary" disabled="">Verify</button>
                              <button type="submit" id="resendOTPbutton" onclick="resendOTPbutton()" class="btn btn-outline-primary" >Resend OTP</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal fade" id="addrecipientModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                        <div class="modal-content">
                           <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                              <h4 class="modal-title" style=" color: white;" id="myModalLabel">DMT</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           </div>
                           <div class="modal-body" id="getCode" >
                              <div id="invoice-template" class="card-body">
                                 <div id="invoice-company-details" class="row">
                                    <div class="col-sm-6 col-12 text-left">
                                       <div class="media">
                                          <img src="{{asset('images/dmt_icon.png')}}" style="width: 100px;">
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                       <!-- <h1>Add Recipient</h1> -->
                                       <div class="invoice-details mt-2">
                                          <h1 id="respDesc">Add Recipient</h1>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-1">
                                    <div class="row">
                                       <div class="table-responsive col-12">
                                          <table class="table table-borderless" style="margin-top: 1rem;">
                                             <tbody>
                                                <tr>
                                                   <td><b>Name</b></td>
                                                   <td>
                                                      <input type="text" class="form-control" id="recipient_name" onkeyup="recipient_Detected()" min="0" placeholder="Enter Name" required>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td><b>Mobile</b></td>
                                                   <td>
                                                      <input type="number" class="form-control" id="recipient_mobile" onkeyup="recipient_Detected()" minlength="10" maxlength="10" placeholder="Enter Mobile" required>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td><b>Bank</b></td>
                                                   <td>
                                                      <!-- <input type="text" class="form-control" id="recipient_bankcode" onkeyup="recipient_Detected()" min="0" placeholder="Enter Bank Code" required> -->
                                                      <select id="recipient_bankcode" class="form-control" onchange="recipient_Detected()">
                                                         <option value="">Select Bank</option>
                                                         @foreach($dmt_bankList as $item)
                                                         <option value="{{$item->bankCode}}">{{$item->bankName}}</option>
                                                         @endforeach
                                                      </select>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td><b>Account Number</b></td>
                                                   <td>
                                                      <input type="number" class="form-control" id="recipient_account_number" onkeyup="recipient_Detected()" min="0" placeholder="Enter Account Number" required>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td><b>IFSC Code</b></td>
                                                   <td>
                                                      <input type="text" class="form-control" id="recipient_ifsc" onkeyup="recipient_Detected()" min="0" placeholder="Enter IFSC Code" required>
                                                   </td>
                                                </tr>
                                                
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer" >
                              <button type="submit" id="addRecipientbutton" onclick="addRecipientbutton()" class="btn btn-primary" disabled="">Add</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal fade" id="recipientDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                        <div class="modal-content">
                           <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                              <h4 class="modal-title" style=" color: white;" id="myModalLabel">DMT</h4>
                              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                           </div>
                           <div class="modal-body" id="getCode" >
                              <div id="invoice-template" class="card-body">
                                 <div id="invoice-company-details" class="row mb-1">
                                    <div class="col-sm-6 col-12 text-left">
                                       <div class="media">
                                          <img src="{{asset('images/dmt_icon.png')}}" style="width: 100px;">
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                       <!-- <h1>Add Recipient</h1> -->
                                       <div class="invoice-details mt-2">
                                          <h1 id="respDesc">Recipient Details</h1>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="invoice-customer-details" style="border: 3px solid lightgray;border-radius: 8px;" class="row pt-1">
                                    <div class="col-sm-6 col-12 text-left">
                                       <div class="recipient-info my-2">
                                          <h6>Recipient Id</h6>
                                          <p><h5 id="DMTrecipientId"></h5></p>
                                          <h6>Recipient Name</h6>
                                          <p id="DMTrecipientName"></p>
                                          <h6>Account Number</h6>
                                          <p id="DMTbankAccountNumber"></p>
                                          <h6>Status</h6>
                                          <p><h5 id="DMTresponseReason"></h5></p>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                       <div class="company-info my-2">
                                          <h6>Bank Name</h6>
                                          <p id="DMTbankName"></p>
                                          <h6>Bank Code</h6>
                                          <p id="DMTbankCode"></p>
                                          <h6>IFSC</h6>
                                          <p id="DMTifsc"></p>
                                          <h6>Message</h6>
                                          <p id="DMTrespDesc"></p>
                                       </div>
                                    </div>
                                 </div>
                                 <input type="hidden" name="senderMobileNumber" id="senderMobileNumber">
                              </div>
                           </div>
                           <div class="modal-footer" >
                              <button type="button" id="dmtAllRecipientData" onclick="dmtAllRecipientData()" class="btn btn-secondary"  data-dismiss="modal">Close</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal fade fund" id="addfundtransfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                        <div class="modal-content">
                           <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                              <h4 class="modal-title" style=" color: white;" id="myModalLabel">DMT</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           </div>
                           <div class="modal-body" id="getCode" >
                              <div id="invoice-template" class="card-body">
                                 <div id="invoice-company-details" class="row">
                                    <div class="col-sm-6 col-12 text-left">
                                       <div class="media">
                                          <img src="{{asset('images/dmt_icon.png')}}" style="width: 100px;">
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                       <!-- <h1>Add Recipient</h1> -->
                                       <div class="invoice-details mt-2">
                                          <h1 id="respDesc">Fund Transfer</h1>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="invoice-customer-details" class="row pt-2 ">
                                   <div class="col-sm-6 col-12 text-left">
                                       <div class="recipient-info my-2">
                                           <h6>Recipient ID</h6>
                                           <p id="txndetail_recipientId"></p>
                                           <h6>Customer Name</h6>
                                           <p id="txndetail_recipientName"></p>
                                       </div>
                                   </div>
                                   <div class="col-sm-6 col-12 text-right">
                                       <div class="company-info my-2">
                                           <h6>Bank</h6>
                                           <p id="txndetail_bankName"></p>
                                           <h6>Account Number</h6>
                                           <p id="txndetail_bankCode"></p>
                                           <h6>IFSC Code</h6>
                                           <p id="txndetail_ifsc"></p>
                                       </div>
                                   </div>
                                 </div>
                                 <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-1">
                                    <div class="row">
                                       <div class="table-responsive col-12">
                                          <table class="table table-borderless" style="margin-top: 1rem;">
                                            <tbody>
                                                <tr>
                                                    <td class="text-left"><b>Transaction Amount (<i class="fa fa-inr"></i>)</b></td>
                                                    <td class="text-right"><input type="text" class="form-control" id="transfer_amount"  min="0" placeholder="Enter Transaction Amount" required></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                    <td class="text-right"  id="respCustConvFee"><input type="text" class="form-control" id="transfer_custConvFee" min="0" placeholder="Customer Convienence Fee" required>
                                                      <input type="hidden" name="" id="senderMobileNumber">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer" >
                              <button type="submit" id="addRecipientbutton" onclick="addRecipientbutton()" class="btn btn-primary" >Transfer</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
   </div>
   </section>
   <div class="divWaiting" id="Dv_Loader" style="display: none;">
      <button class="btn btn-primary mb-1 round" type="button" >
      <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
      &nbsp;Loading... 
      </button>
   </div>
   <!-- Column selectors with Export Options and print table -->
</div>
</div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<!-- sendOtpDetected -->
<script type="text/javascript">
  function sendOtpDetected(){
      //var customer_name = $("#customer_name").val();
      var customer_mobile = $("#customer_mobile").val();
        if (customer_mobile.length > 0 && customer_mobile.length == 10) {
          $('#getotpbutton').removeAttr('disabled'); 
        } else {
          $('#getotpbutton').attr('disabled', 'disabled');
        }
  }
</script>
<!-- otpDetected -->
<script type="text/javascript">
  function otpDetected(){
      var otp = $("#otp").val();
        if (otp.length > 0) {
          $('#verifyOTPbutton').removeAttr('disabled'); 
        } else {
          $('#verifyOTPbutton').attr('disabled', 'disabled');
        }
  }
</script>
<!-- recipient_Detected -->
<script type="text/javascript">
  function recipient_Detected(){
      var recipient_name = $("#recipient_name").val();
      var recipient_mobile = $("#recipient_mobile").val();
      var recipient_bankcode = $("#recipient_bankcode").val();
      var recipient_account_number = $("#recipient_account_number").val();
      var recipient_ifsc = $("#recipient_ifsc").val();
        if (recipient_name.length > 0 && recipient_mobile.length > 0 && recipient_mobile.length == 10 && recipient_bankcode.length > 0 && recipient_account_number.length > 0 && recipient_ifsc.length > 0 ) {
          $('#addRecipientbutton').removeAttr('disabled'); 
        } else {
          $('#addRecipientbutton').attr('disabled', 'disabled');
        }
  }
</script>
<!-- getotpbutton -->
<script type="text/javascript">
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });

   
   
   function sendOTP(){

		var user_customer_name = $('#user_customer_name').val();
       	var user_customer_mobile = $('#user_customer_mobile').val();
       	var user_customer_email = $('#user_customer_email').val();
       	var user_customer_aadhar = $('#user_customer_aadhar').val();
       	var user_customer_pan = $('#user_customer_pan').val();
       	var user_customer_address = $('#user_customer_address').val();
       	var user_customer_pincode = $('#user_customer_pincode').val();
       	var user_customer_pin = $('#user_customer_pin').val();
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtSenderRegister", 
           data : {'user_customer_name':user_customer_name, 'user_customer_mobile':user_customer_mobile, 'user_customer_email':user_customer_email, 'user_customer_aadhar':user_customer_aadhar, 'user_customer_pan':user_customer_pan, 'user_customer_address':user_customer_address, 'user_customer_pincode':user_customer_pincode, 'user_customer_pin': user_customer_pin},
           dataType: "json",
           success:function(data)
           {
				$('#Dv_Loader').hide();

				if(data.error)
				{
					$("#errormessage").html(data.error);
				}
				else
				{
					// show OTP
					getotpbutton();
				}


           }
        });
        // });
      // });
   }

   function checkRegistered(){
       var customer_mobile = $('#customer_mobile').val();
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtSenderCheck", 
           data : {'customer_mobile':customer_mobile},
           dataType: "json",
           success:function(data)
           {   
				$('#Dv_Loader').hide();
				$("#errormessage").empty();

				if(data.success == 0)
				{
					$('#customer_mobile').attr('disabled', 'disabled');

					$('#loginForm').hide();
					$('#registerForm').show();

					$('#user_customer_mobile').val(customer_mobile);

					$('#payment_button').hide();
					$('#signup_button').show();
				}

				
           }
        });
        // });
      // });
   }

   function getotpbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){

    	var customer_name = $('#user_customer_name').val();
       	var customer_mobile = $('#user_customer_mobile').val();
       	var customer_pin = $('#user_customer_pin').val();
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtSenderOtp", 
           data : {'customer_name':customer_name,'customer_mobile':customer_mobile,'customer_pin':customer_pin,},
           dataType: "json",
           success:function(data)
           {   
				$('#Dv_Loader').hide();
				$("#errormessage").empty();
				//$('#customer_name').attr('disabled', 'disabled');
				//$('#customer_mobile').attr('disabled', 'disabled');

				$("#getotpbutton").hide();

				//$('#additionalRegData').val(additionalRegData);
				//$('#respDesc').html(respDesc);
				//$('#senderMobileNumber').val(senderMobileNumber);

				$('#verifyOTPModal').modal('show');
           	}
        });
        // });
      // });
   }
</script>
<!-- resendOTPbutton -->
<script type="text/javascript">
   function resendOTPbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var senderMobileNumber = $('#senderMobileNumber').val();
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtResendOtp", 
           data : {'senderMobileNumber':senderMobileNumber,},
           dataType: "json",
           success:function(data)
           {   
                  $('#Dv_Loader').hide();
                  $("#errormessage").empty();
   
               if (data.responseCode == "000") {
                  var DMTresendOtpResp = data; 
                  var additionalRegData = DMTresendOtpResp.additionalRegData;
                  var respDesc = DMTresendOtpResp.respDesc;
                 
                  $('#additionalRegData').val(additionalRegData);
                  $('#respDesc').html(respDesc);
                  $('#verifyOTPModal').modal('show');
               }else{
                  var error = data.errorInfo.error;
                   var isArray = Array.isArray(error);
                   if (isArray) {
                      $("#errormessage").empty();
                      $.each(error,function(key,value){
                       $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                      });
                   }else{
                      var error =data.errorInfo.error.errorMessage;
                      $("#errormessage").empty();
                       $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                   }
               }
               
           }
        });
        // });
      // });
   }
</script>
<!-- verifyOTPbutton -->
<script type="text/javascript">
   function verifyOTPbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var verify_otp = $('#otp').val();
       var verify_additionalRegData = $('#additionalRegData').val();
       var verify_senderMobileNumber = $('#senderMobileNumber').val();
   
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtVerifyOtp", 
           data : {'verify_otp':verify_otp,'verify_additionalRegData':verify_additionalRegData,'verify_senderMobileNumber':verify_senderMobileNumber,},
           dataType: "json",
           success:function(data)
           {   
                  $('#Dv_Loader').hide();
                  $("#errormessage").empty();
                  $('#verifyOTPModal').modal('hide');
   
               if (data.responseCode == "000") {
                  var DMTverifyOTPResp = data; 
                  
                  $("#errormessage").empty();
                  $("#successmessage").empty();
                  $("#successmessage").append('<p class="btn btn-success" >'+DMTverifyOTPResp.respDesc+'</p>');

                    var customer_mobile = $('#senderMobileNumber').val();
       
                    $('#Dv_Loader').show();
                    $.ajax({
                       type:"POST",
                       url:"/dmtAllRecipientData", 
                       data : {'customer_mobile':customer_mobile,},
                       dataType: "json",
                       success:function(data)
                       {   
                              $('#Dv_Loader').hide();
                              $("#errormessage").empty();
                              // $("#successmessage").empty();
                              // $('#verifyOTPModal').modal('hide');
               
                           if (data.responseCode == "000") {
                              var DMTallRecipientResp = data; 
                              
                              var allrecipientData = DMTallRecipientResp.recipientList.dmtRecipientList; 
                              $("#receipentDataList").empty();
                                $("#receipentDataList").append('<hr><br><button type="button" class="btn btn-primary float-right" data-backdrop="false" data-toggle="modal" data-target="#addrecipientModal">Add Recipient</button><br><div class="table-responsive mt-3">   <table class="table table-striped table-bordered">      <thead>         <tr>              <th>Recipient Name</th><th>Account No.</th>              <th>Bank Code</th>              <th>Bank Name</th>              <th>IFSC Code</th>              <th>Recipient</th>              <th>Action</th>         </tr>      </thead>      <tbody id="recipientData">                   </tbody>   </table></div>');
                                  

                                  var isArray = Array.isArray(allrecipientData);
                                  if (isArray) {
                                    $("#recipientData").empty();
                                    $.each(allrecipientData,function(key,value){
                                    $("#recipientData").append(' <tr>  <td id="transfer_recipientName">'+value.recipientName+'</td><td id="transfer_bankAccountNumber">'+value.bankAccountNumber+'</td><td id="transfer_bankCode">'+value.bankCode+'</td><td id="transfer_bankName">'+value.bankName+'</td><td id="transfer_ifsc">'+value.ifsc+'</td><td id="transfer_recipientId">'+value.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+value.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" data-toggle="modal" data-target="#'+value.recipientId+'"  value="'+value.recipientId+'" onClick="dmtTransfer()" style="margin-left: 11px;">Transfer</button></td>        </tr> ');
                                    });
                                  }else{
                                    $("#recipientData").empty();
                                    $("#recipientData").append(' <tr> <td id="transfer_recipientName">'+allrecipientData.recipientName+'</td><td id="transfer_bankAccountNumber">'+allrecipientData.bankAccountNumber+'</td><td id="transfer_bankCode">'+allrecipientData.bankCode+'</td><td id="transfer_bankName">'+allrecipientData.bankName+'</td><td id="transfer_ifsc">'+allrecipientData.ifsc+'</td><td id="transfer_recipientId">'+allrecipientData.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+allrecipientData.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" value="'+allrecipientData.recipientId+'" onClick="dmtTransfer()" style="margin-left: 11px;">Transfer</button></td>         </tr> ');
                                   
                                  }
                           }else{
                              $("#successmessage").empty();
                              var error = data.errorInfo.error;
                               var isArray = Array.isArray(error);
                               if (isArray) {
                                  $("#errormessage").empty();
                                  $.each(error,function(key,value){
                                   $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                                  });
                               }else{
                                  var error =data.errorInfo.error.errorMessage;
                                  $("#errormessage").empty();
                                  if (data.errorInfo.error.errorCode == "DMT042") {
                                    $("#successmessage").append('<p class="btn btn-primary" >'+error+'</p>');

                                  }else{
                                    $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                                  }
                               }
                           }
                           
                       }
                    }); 
                  
               }else{
                  var error = data.errorInfo.error;
                   var isArray = Array.isArray(error);
                   if (isArray) {
                      $("#errormessage").empty();
                      $.each(error,function(key,value){
                       $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                      });
                   }else{
                      var error =data.errorInfo.error.errorMessage;
                      $("#errormessage").empty();
                       $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                   }
               }
               
           }
        });
        // });
      // });
   }
</script>
<!-- addRecipientbutton -->
<script type="text/javascript">
   function addRecipientbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var recipient_name = $('#recipient_name').val();
       var recipient_mobile = $('#recipient_mobile').val();
       var recipient_bankcode = $('#recipient_bankcode').val();
       var recipient_account_number = $('#recipient_account_number').val();
       var recipient_ifsc = $('#recipient_ifsc').val();
       var senderMobileNumber = $('#customer_mobile').val();
   
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtAddrecipient", 
           data : {'recipient_name':recipient_name,'recipient_mobile':recipient_mobile,'recipient_bankcode':recipient_bankcode,'recipient_account_number':recipient_account_number,'recipient_ifsc':recipient_ifsc,'senderMobileNumber':senderMobileNumber,},
           dataType: "json",
           success:function(data)
           {   
                  $('#Dv_Loader').hide();
                  $("#errormessage").empty();
                  $("#successmessage").empty();
                  $('#addrecipientModal').modal('hide');
   
               if (data.responseCode == "000") {
                  var respDesc = data.respDesc;
                  // var DMTaddRecipientResp = data.recipientList.dmtRecipient; 
                  
                  // var bankAccountNumber = DMTaddRecipientResp.bankAccountNumber;
                  // var bankCode = DMTaddRecipientResp.bankCode;
                  // var bankName = DMTaddRecipientResp.bankName;
                  // var ifsc = DMTaddRecipientResp.ifsc;
                  // var recipientId = DMTaddRecipientResp.recipientId;
                  // var recipientName = DMTaddRecipientResp.recipientName;
                  // var responseReason = data.responseReason;
                  // var senderMobileNumber = data.senderMobileNumber;

                 
                  // $('#DMTbankAccountNumber').html(bankAccountNumber);
                  // $('#DMTbankCode').html(bankCode);
                  // $('#DMTbankName').html(bankName);
                  // $('#DMTifsc').html(ifsc);
                  // $('#DMTrecipientId').html(recipientId);
                  // $('#DMTrecipientName').html(recipientName);
                  // $('#DMTrespDesc').html(respDesc);
                  // $('#DMTresponseReason').html(responseReason);
                  // $('#senderMobileNumber').val(senderMobileNumber);

                  // $('#recipientDetailsModal').modal('show');

                  var customer_mobile = $('#senderMobileNumber').val();
       
                    $('#Dv_Loader').show();
                    $.ajax({
                       type:"POST",
                       url:"/dmtAllRecipientData", 
                       data : {'customer_mobile':customer_mobile,},
                       dataType: "json",
                       success:function(data)
                       {   
                              $('#Dv_Loader').hide();
                              $("#errormessage").empty();
                              $("#successmessage").empty();
                              $("#successmessage").append('<p class="btn btn-success" >'+respDesc+'</p>');
                              // $('#verifyOTPModal').modal('hide');
               
                           if (data.responseCode == "000") {
                              var DMTallRecipientResp = data; 
                              
                              var allrecipientData = DMTallRecipientResp.recipientList.dmtRecipientList; 
                              $("#receipentDataList").empty();
                                $("#receipentDataList").append('<hr><br><button type="button" class="btn btn-outline-primary float-right" data-backdrop="false" data-toggle="modal" data-target="#addrecipientModal">Add Recipient</button><br><div class="table-responsive mt-3">   <table class="table table-striped table-bordered">      <thead>         <tr>              <th>Recipient Name</th><th>Account No.</th>              <th>Bank Code</th>              <th>Bank Name</th>              <th>IFSC Code</th>              <th>Recipient</th>              <th>Action</th>         </tr>      </thead>      <tbody id="recipientData">                   </tbody>   </table></div>');
                                  

                                  var isArray = Array.isArray(allrecipientData);
                                  if (isArray) {
                                    $("#recipientData").empty();
                                    $.each(allrecipientData,function(key,value){
                                    $("#recipientData").append(' <tr>  <td id="transfer_recipientName">'+value.recipientName+'</td><td id="transfer_bankAccountNumber">'+value.bankAccountNumber+'</td><td id="transfer_bankCode">'+value.bankCode+'</td><td id="transfer_bankName">'+value.bankName+'</td><td id="transfer_ifsc">'+value.ifsc+'</td><td id="transfer_recipientId">'+value.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+value.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" value="'+value.recipientId+'" onClick="" style="margin-left: 11px;">Transfer</button></td>        </tr> ');
                                    });
                                  }else{
                                    $("#recipientData").empty();
                                    $("#recipientData").append(' <tr> <td id="transfer_recipientName">'+allrecipientData.recipientName+'</td><td id="transfer_bankAccountNumber">'+allrecipientData.bankAccountNumber+'</td><td id="transfer_bankCode">'+allrecipientData.bankCode+'</td><td id="transfer_bankName">'+allrecipientData.bankName+'</td><td id="transfer_ifsc">'+allrecipientData.ifsc+'</td><td id="transfer_recipientId">'+allrecipientData.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+allrecipientData.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" value="'+allrecipientData.recipientId+'" onClick="" style="margin-left: 11px;">Transfer</button></td>         </tr> ');
                                   
                                  } 
                           }else{
                              $("#successmessage").empty();
                              var error = data.errorInfo.error;
                               var isArray = Array.isArray(error);
                               if (isArray) {
                                  $("#errormessage").empty();
                                  $.each(error,function(key,value){
                                   $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                                  });
                               }else{
                                  var error =data.errorInfo.error.errorMessage;
                                  $("#errormessage").empty();
                                   $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                               }
                           }
                           
                       }
                    });
                  
               }else{ 
                  $("#successmessage").empty();
                  var error = data.errorInfo.error;
                   var isArray = Array.isArray(error);
                   if (isArray) {
                      $("#errormessage").empty();
                      $.each(error,function(key,value){
                       $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                      });
                   }else{
                      var error =data.errorInfo.error.errorMessage;
                      $("#errormessage").empty();
                       $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                   }
               }
               
           }
        });
        // });
      // });
   }
</script>
<!-- deleteRecipientbutton -->
<script type="text/javascript">
   function deleteRecipientbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var recipient_id = $('#deleteRecipientbutton').val();
       var senderMobileNumber = $('#senderMobileNumber').val();
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/dmtdeleterecipient", 
           data : {'recipient_id':recipient_id,'senderMobileNumber':senderMobileNumber,},
           dataType: "json",
           success:function(data)
           {   
                  $('#Dv_Loader').hide();
                  $("#errormessage").empty();
   
               if (data.responseCode == "000") {
                  var respDesc = data.respDesc;
                  $("#successmessage").empty();
                  $("#successmessage").append('<p class="btn btn-success" >'+respDesc+'</p>');
                  
               }else{ 
                  $("#successmessage").empty();
                  var error = data.errorInfo.error;
                   var isArray = Array.isArray(error);
                   if (isArray) {
                      $("#errormessage").empty();
                      $.each(error,function(key,value){
                       $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                      });
                   }else{
                      var error =data.errorInfo.error.errorMessage;
                      $("#errormessage").empty();
                       $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                   }
               }
               
           }
        });
        // });
      // });
   }
</script>
<!-- dmtfundtransfer -->
<script type="text/javascript">
   function dmtTransferData(that)
   {
      console.log($(that).attr('data-recipientName'));
      return false;

      var senderMobileNumber = $('#customer_mobile').val();
      $('#txndetail_recipientName').html($(that).attr('data-recipientName'));
      $('#txndetail_bankAccountNumber').html(transfer_bankAccountNumber);
      $('#txndetail_bankCode').html(transfer_bankCode);
      $('#txndetail_bankName').html(transfer_bankName);
      $('#txndetail_ifsc').html(transfer_ifsc);
      $('#txndetail_recipientId').html(transfer_recipientId);
      $('#senderMobileNumber').val(senderMobileNumber);
      $('#addfundtransfer').modal('show');
   }

   function dmtTransfer(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
        // $('#Dv_Loader').show();
      var senderMobileNumber = $('#senderMobileNumber').val();
      var transfer_recipientName = document.getElementById("transfer_recipientName").innerText;
      var transfer_bankAccountNumber = document.getElementById("transfer_bankAccountNumber").innerText;
      var transfer_bankCode = document.getElementById("transfer_bankCode").innerText;
      var transfer_bankName = document.getElementById("transfer_bankName").innerText;
      var transfer_ifsc = document.getElementById("transfer_ifsc").innerText;
      var transfer_recipientId = document.getElementById("transfer_recipientId").innerText;
      alert(transfer_recipientId);
      // document.getElementById("addfundtransfer").innerHTML = transfer_recipientId;
      // var cust_convfee = $('#transfer_amount').val();
      // var amount = $('#transfer_custConvFee').val();

        
      $('#txndetail_recipientName').html(transfer_recipientName);
      $('#txndetail_bankAccountNumber').html(transfer_bankAccountNumber);
      $('#txndetail_bankCode').html(transfer_bankCode);
      $('#txndetail_bankName').html(transfer_bankName);
      $('#txndetail_ifsc').html(transfer_ifsc);
      $('#txndetail_recipientId').html(transfer_recipientId);
      $('#senderMobileNumber').val(senderMobileNumber);
      // $('#addfundtransfer').modal('show');
      // $('.fund').each(function(){
      //      if(this.id){
      //        this.id = transfer_recipientId;
      //      }
      //    });
   }
</script>
<!-- dmtAllRecipientData -->
<script type="text/javascript">
   function dmtAllRecipientData() {
     var customer_mobile = $('#customer_mobile').val();
     $('#Dv_Loader').show();
     $.ajax({
        type:"POST",
        url:"/dmtAllRecipientData", 
        data : {'customer_mobile':customer_mobile,},
        dataType: "json",
        success:function(data)
        {   
           $('#Dv_Loader').hide();
           $("#successmessage").empty();
           $("#errormessage").empty();
           $('#recipientDetailsModal').modal('hide');

            if (data.responseCode == "000") {
               var allrecipientData = data.recipientList.dmtRecipient; 
               //console.log(allrecipientData);


               $("#receipentDataList").empty();
                 $("#receipentDataList").append('<hr><br><button type="button" class="btn btn-outline-primary float-right" data-backdrop="false" data-toggle="modal" data-target="#addrecipientModal">Add Recipient</button><br><div class="table-responsive mt-3">   <table class="table table-striped table-bordered">      <thead>         <tr>              <th>Account No.</th>              <th>Bank Code</th>              <th>Bank Name</th>              <th>IFSC Code</th>              <th>Recipient</th>              <th>Action</th>         </tr>      </thead>      <tbody id="recipientData">                   </tbody>   </table></div>');
                   

                   var isArray = Array.isArray(allrecipientData);
                   if (isArray) {
                     $("#recipientData").empty();
                     $.each(allrecipientData,function(key,value){
                     $("#recipientData").append(' <tr>  <td>'+value.bankAccountNumber+'</td><td>'+value.bankCode+'</td><td>'+value.bankName+'</td><td>'+value.ifsc+'</td><td>'+value.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+value.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" value="'+value.recipientId+'" data-recipientName="'+value.recipientName+'" onClick=dmtTransferData(this);"  style="margin-left: 11px;">Transfer</button></td>        </tr> ');

                     //

                     //senderMobileNumber, transfer_recipientName, transfer_bankAccountNumber, transfer_bankCode, transfer_bankName, transfer_ifsc, transfer_recipientId
                     });
                   }else{
                     $("#recipientData").empty();
                     $("#recipientData").append(' <tr> <td>'+allrecipientData.bankAccountNumber+'</td><td>'+allrecipientData.bankCode+'</td><td>'+allrecipientData.bankName+'</td><td>'+allrecipientData.ifsc+'</td><td>'+allrecipientData.recipientId+'</td><td><button class="btn btn-outline-danger round" id="deleteRecipientbutton" value="'+allrecipientData.recipientId+'" onClick="deleteRecipientbutton()"><i class="fa fa-trash"></i></button><button class="btn btn-outline-primary round" id="transfer_button" value="'+allrecipientData.recipientId+'"  onClick="dmtTransferSingle()" style="margin-left: 11px;">Transfer</button></td>         </tr> ');
                    
                   }
            }else{
               var error = data.errorInfo.error;
                var isArray = Array.isArray(error);
                if (isArray) {
                   $("#errormessage").empty();
                   $.each(error,function(key,value){
                    $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                   });
                }else{
                   var error =data.errorInfo.error.errorMessage;
                   $("#errormessage").empty();
                    $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
                }
            }
            
        }
     }); 
   }
   
</script>
@endsection