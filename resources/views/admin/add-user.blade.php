@extends('admin.layouts.main')
@section('css')
 <style type="text/css">
         .divWaiting {
         position: fixed;
         background-color: #FAFAFA;
         z-index: 2147483647 !important;
         opacity: 0.8;
         overflow: hidden;
         text-align: center;
         top: 0;
         left: 0;
         height: 100%;
         width: 100%;
         padding-top: 20%;
         }
      </style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Add Member</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Member</a>
                        </li>
                        <li class="breadcrumb-item active">Add Member
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Form wizard with step validation section start -->
         <section id="validation">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Add Details</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           @if ($errors->any())
                           <div  class="alert alert-danger alert-block">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                           @endif
                           @if ($message = Session::get('info'))
                           <div class="alert alert-primary alert-block">
                              <strong>{{ $message }}</strong>
                           </div>
                           @endif 
                           @if ($message = Session::get('danger'))
                           <div class="alert alert-danger alert-block">
                              <strong>{{ $message }}</strong>
                           </div>
                           @endif 
                           <form action="{{ route('add-user') }}" method="POST" enctype="multipart/form-data" class="wizard-circle">
                               @csrf
                              <!-- Step 1 -->
                              <h4><i class="step-icon feather icon-user"></i> User's Details</h4>
                              <fieldset>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="role">
                                          Role
                                          </label>
                                          <select class="custom-select form-control" id="role" name="role" required="">
                                             <option value="">Select Role</option>
                                             <?php if (Auth::user()->role == 'super admin'): ?>
                                             <!--<option  value="admin">Admin</option>-->
                                             <?php endif ?> 
                                             <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'manager'): ?>
                                             <option  value="manager">Manager</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'manager' ||Auth::user()->role == 'admin'): ?>
                                             <option  value="sdistributor">Super Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'manager'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'): ?>
                                             <option  value="mdistributor">Master Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'manager'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor'): ?>
                                             <option  value="distributor">Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin' || Auth::user()->role == 'manager'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor' || Auth::user()->role == 'distributor'): ?>
                                             <option  value="retailer">Retailer</option>
                                             <?php endif ?>
                                          </select>

                                          <input type="text"  id="domain"   class="form-control" value="{{ URL::to('/') }}" name="domain" placeholder="Enter Domain Name" required="" disabled="" hidden="">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group" id="package_col">
                                          <label for="package">
                                          Package
                                          </label>
                                           <select class="custom-select form-control" id="package_id" name="package_id"></select>
                                       </div>
                                       <div class="form-group" id="position_col" style="display: none;">
                                          <label for="position">
                                          Position
                                          </label>
                                           <select class="custom-select form-control" id="position" name="position"></select>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="row">
                                    <div class="col-md-12 text-danger" id="token_availibity"></div>
                                    <input type="hidden" name="token_id" id="token_id">
                                    <input type="hidden" name="availability" id="availability">
                                 </div>

                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="firstName3">
                                          First Name
                                          </label>
                                          <input type="text" class="form-control required" id="first_name" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}" required="">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="lastName3">
                                          Last Name
                                          </label>
                                          <input type="text" class="form-control required" id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Namename" required="">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="emailAddress5">
                                          Email
                                          </label>
                                          <input type="email" class="form-control required" id="email" name="email" value="{{ old('email') }}" placeholder="Enter E-mail" required="" autocomplete="off" autocompletenew="off">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="mobile">
                                          Mobile
                                          </label>
                                          <input type="number" class="form-control required" id="mobile" name="mobile" placeholder="Enter Mobile" pattern="[6789][0-9]{9}" maxlength="10" minlength="10" required="">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row d-none hide">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="loginpin">
                                          Login Pin
                                          </label>
                                          <input type="number" class="form-control required" id="loginpin" onKeyPress="if(this.value.length==4) return false;" onKeyDown="if(this.value.length==4 && event.keyCode!=8) return false;" min="1000" max="9999" value="{{ old('pin') }}" name="pin" placeholder="Enter Login PIN">
                                       </div>
                                    </div>
                                 </div>

                                 {{--<div class="row hide d-none">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="emailAddress5">
                                          Password
                                          </label>
                                          <input type="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*?[#?!@$%^&*-]).{8,}" name="password" class="form-control" onkeyup="passwordDetected()" placeholder="Your Password"  >
                                          <span>(The password must contain - Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.)</span>
                                       </div>
                                    </div>
                                    <div class="col-md-6"> 
                                       <div class="form-group">
                                          <label for="emailAddress5">
                                          Confirm Password
                                          </label>
                                          <input type="password" id="password-confirm" name="password_confirmation" class="form-control" onkeyup="passwordDetected()" placeholder="Confirm Password" >
                                       </div>
                                       <span id='password_response'></span>
                                    </div>
                                 </div>--}}
                              </fieldset>
                              <!-- Step 2 -->
                              <h4><i class="step-icon feather icon-map-pin"></i> Address & DOB</h4>
                              <fieldset>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="state">
                                          State
                                          </label>
                                          <select class="custom-select form-control" id="state" name="state_id" required="">
                                             <option value="">Select State</option>
                                             @foreach ($states as $state)
                                             <option value="{{$state->id}}">
                                                {{$state->state_name}}
                                             </option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="district">
                                          District
                                          </label>
                                          <select class="custom-select form-control"  id="city" name="district_id" ></select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="pincode">
                                          Pin Code
                                          </label>
                                          <input type="text" class="form-control required" id="pincode" name="pin_code" placeholder="Pin Code">
                                       </div>
                                       <div class="form-group">
                                          <label for="DOB">
                                          Date of Birth
                                          </label>
                                          <input type="date" class="form-control required" id="dob" name="dob"> 
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="address">Address</label>
                                          <textarea name="address" id="address" rows="4" class="form-control required" required></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group d-flex align-items-center pt-md-2">
                                          <label class="mr-2">Gender :</label>
                                          <div class="c-inputs-stacked">
                                             <div class="d-inline-block mr-2">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="gender" value="male">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Male</span>
                                                </div>
                                             </div>
                                             <div class="d-inline-block">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                   <input type="radio" name="gender" value="female">
                                                   <span class="vs-checkbox">
                                                   <span class="vs-checkbox--check">
                                                   <i class="vs-icon feather icon-check"></i>
                                                   </span>
                                                   </span>
                                                   <span class="">Female</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="status">
                                          Status
                                          </label>
                                          <select class="custom-select form-control" id="status" name="status" required="">
                                             <option value="">Select Status</option>
                                             <option value="1">Active</option>
                                             <option value="0">Inactive</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </fieldset>
                              <!-- Step 3 -->
                              <h4><i class="step-icon feather icon-file"></i> Documents</h4>
                              <fieldset>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="aadharcard">
                                          Aadhar Card
                                          </label>
                                          <input type="text" class="form-control required" id="aadhar_number" name="aadhar_number" pattern="^\d{12}$" maxlength="12" value="{{ old('aadhar_number') }}" placeholder="Enter Aadhar Card" required="">
                                       </div> 
                                       <div class="form-group">
                                          <label for="pancard">
                                          Pan Card
                                          </label>
                                          <input type="text" class="form-control required" id="pan_number" name="pan_number" maxlength="10" value="{{ old('pan_number') }}"  onchange="digioPanVerify()"  pattern="^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$" placeholder="Enter Pan Card" required="">
                                          <span id="pancard_response"></span>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="ProfilePic">
                                          Profile Image
                                          </label>
                                          <input type="file" class="form-control required" id="ProfilePic" name="profile_pic" required="">
                                       </div>
                                       <div class="form-group">
                                          <label for="aadharcard_image">
                                          Aadhar Card Image
                                          </label>
                                          <input type="file" class="form-control required" id="aadharcard_image" name="aadhar_pic_front" required="">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="pan_card_image">
                                          Pan Card Image
                                          </label>
                                          <input type="file" class="form-control required" id="pan_card_image" name="pan_pic" required="">
                                       </div>
                                       <div class="form-group d-none hide">
                                          <label for="passbook_image">
                                          Account Passbook/Cancel Cheque
                                          </label>
                                          <input type="file" class="form-control" id="passbook_image" name="passbook_image">
                                       </div>
                                       <div class="form-group d-none hide">
                                          <label for="marksheet_10">
                                          10th Marksheet (Optional)
                                          </label>
                                          <input type="file" class="form-control required" id="marksheet_10" name="marksheet_10">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       
                                       <div class="form-group d-none hide">
                                          <label for="marksheet_12">
                                          12th Marksheet
                                          </label>
                                          <input type="file" class="form-control" id="marksheet_12" name="marksheet_12">
                                       </div>
                                    </div>
                                 </div>
                              </fieldset>
                              <!-- Step 4 -->
                              <h4><i class="step-icon feather icon-shopping-cart"></i> Shop Details</h4>
                              <fieldset>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="company_name">
                                          Shop/Office Name
                                          </label>
                                          <input type="text" class="form-control required" id="company_name" name="company_name">
                                       </div>
                                       <div class="form-group">
                                          <label for="company_address">
                                          Shop/Office Address
                                          </label>
                                          <input type="text" class="form-control required" id="company_address" name="company_address">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="company_pan">
                                          Shop/Office Pan/GST (If Available)
                                          </label>
                                          <input type="text" class="form-control required" id="company_pan" name="company_pan">
                                       </div>
                                       <div class="form-group">
                                          <label for="shop_image">
                                          Shop Picture
                                          </label>
                                          <input type="file" class="form-control required" id="shop_image" name="company_logo">
                                       </div>
                                    </div>
                                 </div>
                              </fieldset>
                              <button class="btn btn-primary float-right btn-inline waves-effect waves-light mb-2" type="submit" id="register_button" disabled="">Register</button>
                              <br>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Form wizard with step validation section end -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script>
    document.getElementById('role').onchange = function(){
        document.getElementById('domain').disabled = (this.value === '' || this.value === 'manager' || this.value === 'sdistributor'||this.value === 'mdistributor'||this.value === 'distributor'||this.value === 'retailer');
        document.getElementById('domain').hidden = (this.value === '' || this.value === 'manager' ||this.value === 'sdistributor'||this.value === 'mdistributor'||this.value === 'distributor'||this.value === 'retailer')
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#state').change(function(){
            var sid = $(this).val();
           
            if(sid){
            $.ajax({
               type:"get",
               url:"/getCities/"+sid, //Please see the note at the end of the post**
               success:function(res)
               {
                    if(res)
                    {
                        $("#city").empty();
                        $("#city").append('<option value="">Select District</option>');
                        $.each(res,function(key,value){
                            $("#city").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
               }
            });
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#role').change(function(){
            var role = $(this).val();

            if(role != 'manager'){
               $('#position_col').hide();
               $('#package_col').show();
               $.ajax({
                  type:"get",
                  url:"/getPackage/"+role, //Please see the note at the end of the post**
                  success:function(res)
                  {
                       if(res)
                       {
                           $("#package_id").empty();
                           $("#token_availibity").empty();
                           $('#availability').val('');

                           $("#package_id").append('<option value="">Select Package</option>');
                           $.each(res,function(key,value){
                               $("#package_id").append('<option value="'+key+'">'+value+'</option>');
                           });
                       }
                  }
               });
            }else{
               $('#package_col').hide();
               $('#position_col').show();
               $.ajax({
                  type:"get",
                  url:"/getPosition/"+role, //Please see the note at the end of the post**
                  success:function(res)
                  {
                       if(res)
                       {
                           $("#position").empty();
                           $("#position").append('<option value="">Select Position</option>');
                           $.each(res,function(key,value){
                               $("#position").append('<option value="'+key+'">'+value+'</option>');
                           });
                       }
                  }
               });
            }
        });
        $("#package_id").change(function(){
            var package_id = $(this).val();
           
            if(package_id){
               $.ajax({
                  type:"get",
                  url:"/getTokenAvailibity/"+package_id, //Please see the note at the end of the post**
                  success:function(res)
                  {
                       if(res)
                       {
                           $("#token_id").val(res.token_id);

                           $('#availability').val(res.availability);
                           $("#token_availibity").empty();
                           $("#token_availibity").html('<div class="form-group">'+res.message+'</div>');
                           if(res.is_allow==1){
                              $('#register_button').removeAttr('disabled');
                              $('#token_availibity').removeClass('text-danger');
                              $('#token_availibity').addClass('text-success');
                              digioPanVerify();
                           }else{
                              $('#register_button').attr('disabled', 'disabled');
                              $('#token_availibity').removeClass('text-success');
                              $('#token_availibity').addClass('text-danger');
                           }
                          
                       }
                  }
               });
            }
        });
    });
</script>
<script type="text/javascript">
function digioPanVerify() {
   // $(document).ready(function(){
   //  $('#pan_number').change(function(){
       var pan_number = $('#pan_number').val();
       var dob = $('#dob').val();
       var first_name = $('#first_name').val();
       var last_name = $('#last_name').val();
       
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/digioPanVerify", 
           data : {'pan_number':pan_number,'dob':dob,'first_name':first_name,'last_name':last_name,},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if(data.is_pan_dob_valid == true && data.name_matched == true){
                  $("#pancard_response").empty();
                  $("#pancard_response").append('<p style="color:green;">Successfully Verified.</p>');
                  $('#register_button').removeAttr('disabled');
                  // $('#pan_number').attr('readonly', 'readonly');   
                  // $('#dob').attr('readonly', 'readonly');   
                  // $('#first_name').attr('readonly', 'readonly');   
                  // $('#last_name').attr('readonly', 'readonly');   
               }else if(data.is_pan_dob_valid == true && data.name_matched == false){
                  $("#pancard_response").empty();
                  $("#pancard_response").append('<p style="color:red;">Invalid Name.</p>');
                  $('#register_button').attr('disabled', 'disabled');   
               }else if(data.is_pan_dob_valid == false && data.name_matched == true){
                  $("#pancard_response").empty();
                  $("#pancard_response").append('<p style="color:red;">Invalid Date of Birth.</p>');
                  $('#register_button').attr('disabled', 'disabled');   
               }else if(data.error_message){
                  $("#pancard_response").empty();
                  $("#pancard_response").append('<p style="color:red;">'+data.error_message+'</p>');
                  $('#register_button').attr('disabled', 'disabled');   
               }else{
                  $("#pancard_response").empty();
                  $("#pancard_response").append('<p style="color:red;">'+data.message+'</p>');
                  $('#register_button').attr('disabled', 'disabled');   
               }
           }
        });
      //   });
      // });
}
</script>
<script type="text/javascript">
function passwordDetected(){
 password = $('#password').val()
 passwordconfirm = $('#password-confirm').val()
 if ((password.length > 0) && (passwordconfirm.length > 0)) {
      if ($('#password').val() == $('#password-confirm').val()) {
         $('#password_response').html('Password Matched.').css('color', 'green');
         $('#register_button').removeAttr('disabled');
      }else{
         $('#password_response').html('Password does not match!' ).css('color', 'red');
         $('#register_button').attr('disabled', 'disabled');   
      } 
 }else{
      $('#register_button').attr('disabled', 'disabled');
 }
}
</script>
@endsection