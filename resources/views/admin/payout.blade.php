@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
    
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Payout</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Service
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                    <div class="divider divider-center">
                        <div class="divider-text">
                           <img src="{{ asset('images/payout_icon (1).png') }}" width="50" alt="UPI">
                        </div>
                     </div>
                     <div class="card-header">
                        <h4 class="card-title">One time registration for payout
                          <br><small>Please register your any two bank account for payout.</small></h4>
                        <?php if(Auth::user()->bank_name < 2): ?>
                        <button type="button" style="margin-left: auto;" class="btn btn-outline-success" data-backdrop="false" data-toggle="modal" data-target="#exampleModalScrollable">
                        Add Bank Account
                        </button>
                         <?php endif ?>
                     </div>

                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered ">
                                 <thead>
                                    <tr>
                                        <th>Name</th>
                                         <th>Bank Name</th>
                                         <th>Account No.</th>
                                         <th>IFSC Code</th>
                                         <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($payout as $item)
                                    <tr>
                                       <td>{{ucfirst($item->benificiary_name)}}</td>
                                       <td>{{$item->bank_name}}</td>
                                       <td>{{$item->account}}</td>
                                       <td>{{$item->ifsc_code}}</td>
                                       <td>
                                          <a href="/pay/{{$item->id}}"  title="Transfer" class="btn btn-primary">Transfer</a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- Modal -->
                     <?php if(Auth::user()->bank_name < 2): ?>
                     <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                           <div class="modal-content">
                              <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4); ">
                                 <h5 class="modal-title"  style=" color: white;" id="exampleModalScrollableTitle">Add Beneficiary Details</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                                 <div class="modal-body">
                                 	<div id="payment_message"></div>
                                    <form action="{{ route('payout') }}" method="POST" class="payout_form">
                                       {{csrf_field()}}
                                       <label>Benificiary Name: </label>
                                       <div class="form-group">
                                          <input type="text" class="form-control" value="{{ ucfirst(Auth::user()->first_name ).' '.ucfirst(Auth::user()->last_name ) }}" name="benificiary_name" placeholder="Name"  readonly="" >
                                       </div>
                                       <div class="row">
                                           <div class="col-12">
                                               <label>Account No.: </label>
                                               <div class="form-group">
                                                  <input type="text" class="form-control" value="{{ old('account') }}" id="account" name="account" placeholder=" Account Number" required>
                                               </div>
                                           </div>
                                           <div class="col-4 d-none">
                                               <button type="button" style="margin-top: 18px;" class="btn btn-success" data-dismiss="modal">Verify</button>
                                           </div>
                                       </div>
                                       <label>Confirm Account No.: </label>
                                       <div class="form-group confirm_account_container">
                                           <input type="text" class="form-control" value="{{ old('confirm_account') }}" name="confirm_account" id="confirm_account" placeholder="Re-enter Account Number" required>
                                           <span id='message'></span>
                                       </div>
                                       <label>IFSC Code: </label>
                                       <div class="form-group">
                                          <input type="text" class="form-control" value="{{ old('ifsc_code') }}" name="ifsc_code" placeholder="IFSC Code" required>
                                       </div>
                                       <label>Bank Name: </label>
                                       <div class="form-group">
                                       <select class="form-control" name="bank_name" id="bank_name" required>
                                           <option value="">Select Bank</option>
                                           <option value="Airtel Payment Bank">Airtel Payment Bank</option>
                                           <option value="Allahabad Bank">Allahabad Bank</option>
                                           <option value="Andhra Pradesh Grameena Vikash Bank">Andhra Pradesh Grameena Vikash Bank</option>
                                           <option value="Andhra Pragathi Grameena Bank">Andhra Pragathi Grameena Bank</option>
                                           <option value="AP Mahesh Coop Urban Bank Ltd">AP Mahesh Coop Urban Bank Ltd</option>
                                           <option value="Aryavart Bank erstwhile Gramin Bank of Aryavart">Aryavart Bank erstwhile Gramin Bank of Aryavart</option>
                                           <option value="Assam Gramin Vikash Bank">Assam Gramin Vikash Bank</option>
                                           <option value="AU Small Finance Bank">AU Small Finance Bank</option>
                                           <option value="Axis Bank">Axis Bank</option>
                                           <option value="Bangiya Gramin Vikash Bank">Bangiya Gramin Vikash Bank</option>
                                           <option value="Bank Of Baroda ertswhile Vijaya Bank erstwhile Dena Bank">Bank Of Baroda ertswhile Vijaya Bank erstwhile Dena Bank</option>
                                           <option value="Bank of India">Bank of India</option>
                                           <option value="Bank of Maharashtra">Bank of Maharashtra</option>
                                           <option value="Bank of Maharashtra">Baroda Gujarat Gramin Bank</option>
                                           <option value="Baroda Rajasthan Kshetriya Gramin Bank">Baroda Rajasthan Kshetriya Gramin Bank</option>
                                           <option value="Baroda Uttar Pradesh Gramin Bank">Baroda Uttar Pradesh Gramin Bank </option>
                                           <option value="Canara Bank erstwhile Syndicate Bank">Canara Bank erstwhile Syndicate Bank</option>
                                           <option value="Catholic Syrian Bank">Catholic Syrian Bank</option>
                                           <option value="Central Bank of India">Central Bank of India</option>
                                           <option value="Chaitanya Godavari Gramin Bank">Chaitanya Godavari Gramin Bank</option>
                                           <option value="Chhattisgarh Rajya Gramin Bank">Chhattisgarh Rajya Gramin Bank</option>
                                           <option value="City Union Bank">City Union Bank </option>
                                           <option value="Dharmapuri District Central Co op Bank Ltd">Dharmapuri District Central Co op Bank Ltd</option>
                                           <option value="Ellaquai Dehati Bank">Ellaquai Dehati Bank</option>
                                           <option value="Equitas Small Finance Bank">Equitas Small Finance Bank</option>
                                           <option value="Erode District Central Co-operative Bank">Erode District Central Co-operative Bank</option>
                                           <option value="ESAF Small Finance Bank">ESAF Small Finance Bank</option>
                                           <option value="Federal Bank">Federal Bank</option>
                                           <option value="Fincare Small Finance Bank">Fincare Small Finance Bank</option>
                                           <option value="Fino Payments Bank">Fino Payments Bank</option>
                                           <option value="HDFC Bank">HDFC Bank</option>
                                           <option value="Himachal Pradesh Gramin Bank">Himachal Pradesh Gramin Bank</option>
                                           <option value="ICICI Bank">ICICI Bank</option>
                                           <option value="IDBI Bank">IDBI Bank</option>
                                           <option value="IDFC First Bank">IDFC First Bank</option>
                                           <option value="India Post Payment Bank">India Post Payment Bank</option>
                                           <option value="Indian Bank">Indian Bank</option>
                                           <option value="Indian Overseas Bank">Indian Overseas Bank</option>
                                           <option value="IndusInd Bank">IndusInd Bank</option>
                                           <option value="J & K Grameen Bank">J & K Grameen Bank</option>
                                           <option value="Jammu & Kashmir Bank">Jammu & Kashmir Bank </option>
                                           <option value="Jharkhand Rajya Gramin Bank erstwhile Vananchal Gramin Bank">Jharkhand Rajya Gramin Bank erstwhile Vananchal Gramin Bank</option>
                                           <option value="Karnataka Bank">Karnataka Bank</option>
                                           <option value="Karnataka Gramin Bank erstwhile Pragathi Krishna Gramin Bank">Karnataka Gramin Bank erstwhile Pragathi Krishna Gramin Bank</option>
                                           <option value="Karnataka Vikas Grameena Bank">Karnataka Vikas Grameena Bank </option>
                                           <option value="Karur Vysya Bank">Karur Vysya Bank</option>
                                           <option value="Kashi Gomati Samyut Gramin Bank">Kashi Gomati Samyut Gramin Bank</option>
                                           <option value="Kerala Gramin Bank">Kerala Gramin Bank</option>
                                           <option value="Kotak Mahindra Bank">Kotak Mahindra Bank</option>
                                           <option value="Lakshmi Vilas Bank">Lakshmi Vilas Bank</option>
                                           <option value="Madhya Bihar Gramin Bank erstwhile Dakshin Bihar Gramin Bank">Madhya Bihar Gramin Bank erstwhile Dakshin Bihar Gramin Bank</option>
                                           <option value="Madhya Pradesh Gramin Bank erstwhile Narmada Jhabua Gramin Bank">Madhya Pradesh Gramin Bank erstwhile Narmada Jhabua Gramin Bank</option>
                                           <option value="Madhyanchal Gramin Bank">Madhyanchal Gramin Bank</option>
                                           <option value="Maharashtra Gramin Bank">Maharashtra Gramin Bank </option>
                                           <option value="Manipur Rural Bank">Manipur Rural Bank </option>
                                           <option value="Meghalaya Rural Bank">Meghalaya Rural Bank</option>
                                           <option value="Mizoram Rural Bank">Mizoram Rural Bank</option>
                                           <option value="Odisha Gramya Bank">Odisha Gramya Bank </option>
                                           <option value="Paschim Banga Gramin Bank">Paschim Banga Gramin Bank</option>
                                           <option value="Paytm Payments Bank">Paytm Payments Bank</option>
                                           <option value="Prathma UP Gramin Bank erstwhile Sarva UP Gramin Bank">Prathma UP Gramin Bank erstwhile Sarva UP Gramin Bank </option>
                                           <option value="Puduvai Bharathiar Grama Bank">Puduvai Bharathiar Grama Bank</option>
                                           <option value="Punjab & Sind Bank">Punjab & Sind Bank</option>
                                           <option value="Punjab Gramin Bank">Punjab Gramin Bank</option>
                                           <option value="Punjab National Bank erstwhile Oriental Bank of Commerce">Punjab National Bank erstwhile Oriental Bank of Commerce</option>
                                           <option value="Purvanchal Gramin Bank">Purvanchal Gramin Bank</option>
                                           <option value="Rajasthan Marudhara Gramin Bank">Rajasthan Marudhara Gramin Bank</option>
                                           <option value="Ratnakar Bank">Ratnakar Bank</option>
                                           <option value="Saptagiri Grameena Bank">Saptagiri Grameena Bank</option>
                                           <option value="Sarva Haryana Gramin Bank">Sarva Haryana Gramin Bank</option>
                                           <option value="Saurashtra Gramin Bank">Saurashtra Gramin Bank</option>
                                           <option value="SBM Bank (INDIA) Limited">SBM Bank (INDIA) Limited</option>
                                           <option value="Shivalik Mercantile Cooperative Bank">Shivalik Mercantile Cooperative Bank </option>
                                           <option value="South Indian Bank">South Indian Bank</option>
                                           <option value="State Bank of India">State Bank of India</option>
                                           <option value="Suryoday Small Fianance Bank">Suryoday Small Fianance Bank</option>
                                           <option value="Tamil Nadu State Apex Co-operative Bank Ltd">Tamil Nadu State Apex Co-operative Bank Ltd</option>
                                           <option value="Tamilnad Mercantile Bank">Tamilnad Mercantile Bank</option>
                                           <option value="TamilNadu Grama Bank erstwhile Pallavan Grama Bank">TamilNadu Grama Bank erstwhile Pallavan Grama Bank</option>
                                           <option value="Telangana Grameena Bank">Telangana Grameena Bank</option>
                                           <option value="The Coimbatore District Central Co-op Bank Limited">The Coimbatore District Central Co-op Bank Limited</option>
                                           <option value="THE CUDDALORE DISTRICT CENTRAL COOPERATIVE BANK">THE CUDDALORE DISTRICT CENTRAL COOPERATIVE BANK</option>
                                           <option value="The Kanyakumari District Central Cooperative Bank">The Kanyakumari District Central Cooperative Bank</option>
                                           <option value="The Kumbakonam Central Co-operative Bank Ltd">The Kumbakonam Central Co-operative Bank Ltd</option>
                                           <option value="The Saraswat Co-operative Bank Ltd">The Saraswat Co-operative Bank Ltd</option>
                                           <option value="The Thoothukudi District Central Coop Bank Ltd">The Thoothukudi District Central Coop Bank Ltd</option>
                                           <option value="The Tirunelveli District Central Co-op Bank Ltd">The Tirunelveli District Central Co-op Bank Ltd</option>
                                           <option value="THE VELLORE DISTRICT CENTRAL CO-OP BANK LTD.">THE VELLORE DISTRICT CENTRAL CO-OP BANK LTD.</option>
                                           <option value="THE VILLUPURAM DISTRICT CENTRAL CO-OP BANK LTD">THE VILLUPURAM DISTRICT CENTRAL CO-OP BANK LTD</option>
                                           <option value="Tripura Gramin Bank">Tripura Gramin Bank</option>
                                           <option value="UCO Bank">UCO Bank</option>
                                           <option value="Ujjivan Small Finance Bank Limited">Ujjivan Small Finance Bank Limited</option>
                                           <option value="Union Bank of India erstwhile Corporation Bank erstwhile Andhra Bank">Union Bank of India erstwhile Corporation Bank erstwhile Andhra Bank</option>
                                           <option value="Utkal Gramin Bank">Utkal Gramin Bank</option>
                                           <option value="Uttar Banga Kshetriya Gramin Bank">Uttar Banga Kshetriya Gramin Bank </option>
                                           <option value="Uttar Bihar Grameen Bank">Uttar Bihar Grameen Bank </option>
                                           <option value="Uttarakhand Gramin Bank">Uttarakhand Gramin Bank</option>
                                           <option value="Vidarbha Konkan Gramin Bank">Vidarbha Konkan Gramin Bank</option>
                                           <option value="YES Bank">YES Bank</option>
                                       </select>
                                    </div>
                                    <label>Mobile: </label>
                                    <div class="form-group">
                                       <input type="text" id="mobile" class="form-control" pattern="[789][0-9]{9}" value="{{ Auth::user()->mobile  }}" name="mobile" maxlength="10" placeholder=" Mobile No." >
                                    </div>
                                    <label>Email:</label>
                                    <div class="form-group">
                                       <input type="email" class="form-control"  value="{{ Auth::user()->email  }}" name="email" placeholder="Email" readonly="">
                                    </div>
                                    
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="addbeneficiary" >Add Beneficiary</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                       <?php endif ?>
                  </div>
               </div>
            </div>
         </section>

         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
	 $('#account, #confirm_account').on('keyup', function () {
	  if ($('#account').val() == $('#confirm_account').val()) {
	  	$('#message').html('Account number Confirmed.').css('color', 'green');
	  } else 
	  	$('.payout_form').addClass('was-validated');

	  	var hasClassValid = $('.confirm_account_container').hasClass('validate');
	  	if(hasClassValid)
	  	{
	  		if($('#account').val() != $('#confirm_account').val())
	  		{
	  			$('#message').html('Account number does not match!' ).css('color', 'red');	
	  		}
	  	}
	  	else
	  	{
	    	$('#message').html('Account number does not match!' ).css('color', 'red');
	    }
	});

    function verifyAccount() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
     // alert("hello");
        $.ajax( {
            url:'/verifyAccount',
            type:'post',
            data: {'account': $('#account').val(),'ifsc_code': $('#ifsc_code').val(),'mobile': $('#mobile').val(),},
            success:function(data) {
               if(data.amtmessage){
                   $('#error_message').html(data.amtmessage);
                }else{
                  if(data.verifyStatus != "VERIFIED"){
                     
                   $('#error_message').html('Account does not exist.');

                  }else{
                   $('#error_message').html(data.AccountName);
                  }
                }

            },
            error:function () {
                console.log('error');
            }
        });
    }

    $(document).ready(function(){
    	$('#addbeneficiary').click(function(){
			var SITEURL = '{{URL::to('')}}';
	        $('#Dv_Loader').show();

	        $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

	        $.ajax({
				type:"post",
				url: "/payoutSubmit", 
				data : $('.payout_form').serialize(),
				dataType: "json",
				success:function(data)
				{
					$('#Dv_Loader').hide();   
					if(data.success)
					{
						$('.payout_form').removeClass('was-validated');
						$('#payment_message').html('<div class="alert alert-primary">'+data.message+'</div>');

						setTimeout(function() {
							location.reload(true);
						}, 1000);
					}
					else
					{
						$('.payout_form').addClass('was-validated');
						$('#payment_message').html('<div class="alert alert-danger">'+data.message+'</div>');
					}
				},
				error:function(data)
				{ 
					$('#Dv_Loader').hide();
					if(!data.success)
					{
						$('#payment_message').html(data.message);
					}
				}
			});
        });
    });
</script>
@endsection