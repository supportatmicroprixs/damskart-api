@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Permission</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Settings
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title"></h4> -->
                        <button type="button" style="margin-left: auto;" class="btn btn-relief-primary mr-1 mb-1" data-backdrop="false" data-toggle="modal" data-target="#exampleModalScrollable">
                        <i class="feather icon-plus"></i> Add Permission
                        </button>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Permission Name</th>
                                       <th>Guard</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($permission as $item)
                                    <tr>
                                       <td>{{$item->name }}</td>
                                       <td>{{$item->guard_name }}</td>
                                       <td>
                                          <!-- <a href="/editpackage/{{$item->id}}" style="color: #3c8dbc;" title="Edit" class="feather icon-edit"></a> -->
                                          <!-- &nbsp; <a href="/deletepackage/{{$item->id}}" style="color:#FF0000;" title="Delete" class=" feather icon-trash"></a> -->
                                                &nbsp; <a href="/viewpermission/{{$item->id}}"  title="Edit" class=" btn btn-primary">Edit</a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- Modal -->
                     <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                           <div class="modal-content">
                              <div class="modal-header"  style="background: linear-gradient(to right, #7367F0, #968df4);">
                                 <h5 class="modal-title" style=" color: white;" id="exampleModalScrollableTitle">Add Permission</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <form  action="{{route('permission-list')}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <label>Permission Name </label>
                                    <div class="form-group">
                                       <input type="text" placeholder="Permission Name" class="form-control" required name="name" required="">
                                    </div>
                              </div>
                              <div class="modal-footer">
                              <button type="submit" class="btn btn-primary"><i class="feather icon-plus"></i> Add </button>
                              </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   
</script>
@endsection