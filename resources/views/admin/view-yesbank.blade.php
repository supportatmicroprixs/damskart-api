@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Yes Bank Commissions</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">Setting
                        </li>
                        <li class="breadcrumb-item">Package
                        </li>
                        <li class="breadcrumb-item active">Package Service's
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Center</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                           <ul class="nav nav-tabs justify-content-center" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#Withdraw" aria-controls="home-center" role="tab" aria-selected="true">Withdraw</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#Mini-Statement" aria-controls="service-center" role="tab" aria-selected="false">Mini Statement</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" id="account-tab-center" data-toggle="tab" href="#Balance-Enquiry" aria-controls="account-center" role="tab" aria-selected="false">Balance Enquiry</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="Withdraw" aria-labelledby="home-tab-center" role="tabpanel">
                                 <div class="row" id="table-hover-animation">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-header">
                                             <h4 class="card-title">Withdraw Commission</h4>
                                          </div>
                                          <div class="card-content">
                                             <div class="card-body">
                                                <div class="table-responsive">
                                                   <form class="form form-vertical" action="{{route('updateyesbankcw',$yesbankcw->id)}}" method="POST" enctype="multipart/form-data">
                                                      {{csrf_field()}}
                                                      <table id="dynamicTable" class="table table-hover-animation mb-0">
                                                         <thead>
                                                            <tr>
                                                               <th scope="col">Start Price</th>
                                                               <th scope="col">End Price</th>
                                                               <th scope="col">Commission</th>
                                                               <th scope="col">Commission Type</th>
                                                               <th scope="col">Action</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $i = 1;
                                                               $commission = json_decode($yesbankcw->commission);
                                                               if ($yesbankcw->commission == 'null') {
                                                                // alert('hello');
                                                                 echo "
                                                                 
                                                                       <tr>
                                                               
                                                                          <td>
                                                                             <input type='text' name='store[0][start_price]' placeholder='Start Price' class=' form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                             <input type='text' name='store[0][end_price]' placeholder='End Price' class=' form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                             <input type='text' name='store[0][commission]' placeholder='Commission' class=' form-control round' required />
                                                                          </td>
                                                                          <td>
                                                                            
                                                                             <select class=' form-control round' name='store[0][commission_type]' required>
                                                                                <option>Select Type</option>
                                                                                <option value='flat'>Flat</option>
                                                                                <option value='percentage'>Percentage</option>
                                                                             </select>
                                                                          </td>
                                                                          <td>
                                                                             <button type='button' name='add' id='add' class='btn btn-primary round'>Add More</button>
                                                                          </td>
                                                                       </tr>
                                                                   ";
                                                               }else{
                                                               
                                                               
                                                                  foreach ($commission as $key => $value) {
                                                                      $abc = $key;
                                                                   echo "<tr>
                                                                   <td><input type='text' id='start_price' value='$value->start_price' name='store[".$key."][start_price]' placeholder='Start Price' value='$value->start_price' class=' form-control round'  required /></td>
                                                                   <td><input type='text' name='store[".$key."][end_price]' placeholder='End Price' value='$value->end_price' class=' form-control round' required /></td>  
                                                                   <td><input type='text' name='store[".$key."][commission]' placeholder='Commission' value='$value->commission' class=' form-control round' required /></td>  
                                                                   <td>
                                                                       
                                                                      <select class=' form-control round' name='store[".$key."][commission_type]' required>";
                                                                        echo '<option value="flat"';
                                                                         if ($value->commission_type =='flat') {
                                                                            echo ' selected';
                                                                         }
                                                                         echo '>Flat</option>';
                                                                         echo '<option value="percentage"';
                                                                         if ($value->commission_type =='percentage') {
                                                                            echo ' selected';
                                                                         }
                                                                         echo '>Percentage</option>';
                                                                       
                                                                   echo"  </select>
                                                                   </td>
                                                                   ";
                                                                   ?>
                                                            <td>
                                                               <?php if ($key == 0): ?>
                                                               <button type='button' name='add' id='add' class='btn btn-primary round'>Add More</button>
                                                               <?php else: ?>
                                                               <button type="button" class="btn btn-danger round remove-tr" onclick="addmore(<?php echo $i; ?>)">Remove</button>
                                                               <!-- id="price<?php echo $i; ?>" -->
                                                               <?php endif ?>
                                                            </td>
                                                            </tr>  
                                                            <?php
                                                               $i++;
                                                               }
                                                               echo "<input type='hidden' id='keyvalue' value=".$abc.">";
                                                               }
                                                               ?>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <hr>
                                                      <div class=" col-12">
                                                         <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                         <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane " id="Mini-Statement" aria-labelledby="service-tab-center" role="tabpanel">
                                 <section id="multiple-column-form">
                                    <div class="row match-height">
                                       <div class="col-12">
                                          <div class="card">
                                             <div class="card-header">
                                                <h4 class="card-title">Mini Statement Commission</h4>
                                             </div>
                                             <div class="card-content">
                                                <div class="card-body">
                                                   <form action="{{route('updateyesbankms',$yesbankms->id)}}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                                                      {{csrf_field()}}
                                                      <div class="form-body">
                                                         <div class="row">
                                                            <div class="col-6">
                                                               <div class="form-group">
                                                                  <label for="contact-info-icon">Commission Price (₹)</label>
                                                                  <div class="position-relative has-icon-left">
                                                                     <input type="number" id="contact-info-icon" value="{{$yesbankms->commission}}" class="form-control round" name="commission" placeholder="Commission Price" required="">
                                                                     <div class="form-control-position">
                                                                        <i class="fa fa-inr"></i>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-12">
                                                               <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                               <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                              <div class="tab-pane" id="Balance-Enquiry" aria-labelledby="account-tab-center" role="tabpanel">
                                 <section id="multiple-column-form">
                                    <div class="row match-height">
                                       <div class="col-12">
                                          <div class="card">
                                             <div class="card-header">
                                                <h4 class="card-title">Balance Enquiry Commission</h4>
                                             </div>
                                             <div class="card-content">
                                                <div class="card-body">
                                                   <form action="{{route('updateyesbankbe',$yesbankbe->id)}}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                                                      {{csrf_field()}}
                                                      <div class="form-body">
                                                         <div class="row">
                                                            <div class="col-6">
                                                               <div class="form-group">
                                                                  <label for="contact-info-icon">Commission Price (₹)</label>
                                                                  <div class="position-relative has-icon-left">
                                                                     <input type="number" id="contact-info-icon"  value="{{$yesbankbe->commission}}"  class="form-control round" name="commission" placeholder="Commission Price" required="">
                                                                     <div class="form-control-position">
                                                                        <i class="fa fa-inr"></i>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-12">
                                                               <button type="submit" class="btn btn-primary mr-1 mb-1">Update</button>
                                                               <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection