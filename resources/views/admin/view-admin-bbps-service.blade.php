@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
    @media print{
         body *{
         visibility: hidden;
         }
         .print-container, .print-container *{
         visibility: visible;
         }
         .print-container, #cl_button *{
         visibility: hidden;
         }
         }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">BBPS Service's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item ">Setting
                        </li> 
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table  id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Service</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($bbps_biller_cat as $item)
                                    <tr>
                                       <td>{{$item->biller_cat_name}}</td>
                                       <td>
                                          <?php $active = '#active'.$item->id; ?>
                                        <?php $inactive = '#inactive'.$item->id; ?>
                                        <?php if($item->status == 1): ?>
                                          <button  type="button"   onclick="editMember(0,<?= $item->id ;?>,'<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success ">Active</button>
                                          <button  type="button"   onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger " style="display: none;">Inactive</button>
                                        <?php elseif($item->status == 0): ?>
                                          <button  type="button" onclick="editMember(1,<?= $item->id ;?>, '<?php echo $inactive; ?>','<?php echo $active; ?>');" id="inactive{{$item->id}}"  value="0" class="btn btn-danger ">Inactive</button>
                                          <button  type="button" onclick="editMember(0,<?= $item->id ;?>, '<?php echo $active; ?>','<?php echo $inactive; ?>');" id="active{{$item->id}}"  value="1" class="btn btn-success " style="display: none;">Active</button>
                                        <?php endif ?>
                                       </td>
                                       <td>
                                          <a href="/viewadminbbps/{{$item->biller_cat_name}}" title="View" class="btn btn-primary">View</a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
  function editMember(status, id, cid, sid) {
    if(id) {
        $.ajax({
            url: '/changeBbpsserviceSatus',
            type: 'get',
            data: {id : id,status : status},
            dataType: 'json',
            success:function(response) {
              console.log(response.success)
               
              if (response.status == 1) {
                 $(cid).hide();
                 $(sid).show();
              }else{
                 $(cid).hide();
                 $(sid).show();
              }

            }
        }); 
 
    } else {
        alert("Error : Refresh the page again");
    }
}
</script>
@endsection