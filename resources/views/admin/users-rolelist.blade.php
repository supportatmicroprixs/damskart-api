@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">User's Role List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Settings
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">User's</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Mobile</th>
                                       <th>Current Role</th>
                                       <th>Upgrade Role</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($usersrole as $item)
                                    <tr>
                                       <td>{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}</td>
                                       <td>{{$item->mobile}}</td>
                                       <td>
                                          @if($item->role == "sdistributor")
                                          Super Distributor
                                          @endif
                                          @if($item->role == "mdistributor")
                                          Master Distributor
                                          @endif
                                          @if($item->role == "distributor")
                                          Distributor
                                          @endif
                                          @if($item->role == "retailer")
                                          Retailer
                                          @endif
                                       </td>
                                       <td>
                                          <div class="btn-group dropdown mr-1 mb-1">
                                             <button type="button" class="btn  btn-primary">
                                                @if($item->role == "sdistributor")
                                                Super Distributor
                                                @endif
                                                @if($item->role == "mdistributor")
                                                Master Distributor
                                                @endif
                                                @if($item->role == "distributor")
                                                Distributor
                                                @endif
                                                @if($item->role == "retailer")
                                                Retailer
                                                @endif
                                             </button>
                                             <button type="button" class="dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             </button>
                                             <div class="dropdown-menu">
                                                @if($item->role != "sdistributor")
                                                <a class="dropdown-item" href="/upgrade/{{$item->id}}/sdistributor">Super Distributor</a>
                                                @endif
                                                @if($item->role != "mdistributor")
                                                <a class="dropdown-item" href="/upgrade/{{$item->id}}/mdistributor">Master Distributor</a>
                                                @endif
                                                @if($item->role != "distributor")
                                                <a class="dropdown-item" href="/upgrade/{{$item->id}}/distributor">Distributor</a>
                                                @endif
                                                @if($item->role != "retailer")
                                                <a class="dropdown-item" href="/upgrade/{{$item->id}}/retailer">Retailer</a>
                                                @endif
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection