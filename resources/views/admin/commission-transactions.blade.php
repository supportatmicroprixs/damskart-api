@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   .border {
   border: 2px solid #EDEDED !important;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Admin Commission</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Admin Commission Report
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#getreceiptModal">Receipt</button> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                              <div class="col-12">
                              <div class="table-responsive border rounded px-1 ">
                                 <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i class="fa fa-filter mr-50 "></i>Filter</h6>
                                 <div class="row match-height">
                                    <div class="col-md-12">
                                       <div class="row">
                                          <div class="col-md-12">
                                             <div class="card" style="margin-bottom: 0rem;">
                                                <!-- <div class="card-header">
                                                   <h4 class="card-title">Filter</h4>
                                                   </div> -->
                                                <div class="card-content">
                                                   <div class="card-body">
                                                      <div class="row">
                                                   		<div class="col-md-4 col-12 mb-1">
                                                            <fieldset>
                                                               <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                     <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </div>
                                                                  <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                                               </div>
                                                            </fieldset>
                                                         </div>
                                                         <div class="col-md-3 col-12 mb-1">
                                                            <fieldset>
                                                               <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                     <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                                                  </div>
                                                                  <select name="package_id" id="package_id" class="form-control">
                                                                     <option value="" selected="">Select Package</option>
                                                                     @foreach($package as $item)
                                                                     <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                                     @endforeach
                                                                  </select>
                                                               </div>
                                                            </fieldset>
                                                         </div>
                                                         <div class="col-md-3 col-12 mb-1">
                                                            <fieldset>
                                                               <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                     <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                                                  </div>
                                                                  <select name="service_id" id="service_id" class="form-control">
                                                                     <option value="" selected="">Select Service</option>
                                                                     <option value="AEPS">AEPS</option>
                                                                     <option value="BBPS">BBPS</option>
                                                                     <option value="Payout">Payout</option>
                                                                     <option value="Fund Transfer">Fund Transfer</option>
                                                                     <option value="Express Payout">Express Payout</option>
                                                                     <option value="MICROATM">MicroATM</option>
                                                                     <option value="UPI">UPI</option>
                                                                     
                                                                  </select>
                                                               </div>
                                                            </fieldset>
                                                         </div>
                                                         <div class="col-md-2 col-12 mb-1">
                                                            <div class="input-group-append">
                                                               <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <hr>
                           <div class="table-responsive" id="alltxn">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Transaction Date</th>
                                       <th>Transaction ID</th>
                                       <th>Package</th>
                                       <th>Customer Name</th>
                                       <th>Aadhar / Card / Account No. / Mobile No.</th>
                                       <th>Service Name</th>
                                       <th>Transaction Type / Service Type</th>
                                       <th>RRN/UTR/Biller Txn</th>
                                       <th>Bill / Txn Amount</th>
                                       <th>Commission / Surcharge</th>
                                       <th>Admin Commission</th>
                                       <th>Current Balance</th>
                                       <th>Total Admin Commission</th>
                                       <th>Check Status</th>
                                       <th>Message / Remarks</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($admin_commissions as $item)
                                    <tr>
                                       <td><a href="{{route('view-member', $item->user_id)}}">{{ucfirst($item->first_name).' '.ucfirst($item->last_name)}}<span> ({{$item->mobile}})</span></a></td>
                                       <td ><span style="display: none;">{{date('YmdHis', strtotime($item->request_transaction_time))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->request_transaction_time))}}</td>
                                       <td>{{$item->dms_txn_id}}</td>
                                       <td>{{$item->package_name}}</td>
                                       <td>{{$item->customer_name}}</td>
                                       <td>{{$item->aadhar_number}}</td>
                                       <td>{{$item->transaction_category}}</td>
                                       <td>{{$item->transaction_type}}</td>
                                       <td>{{$item->original_transaction_id}}</td>
                                       <td> {{number_format($item->transaction_amount , 3)}}</td>
                                       <td> {{number_format($item->commission , 3)}}</td>
                                       @if($item->commission_type == 'credit')
                                       <td style="color: green;"> {{number_format($item->admin_commission , 3)}}</td>
                                       @else
                                       <td style="color: red;"> {{number_format($item->admin_commission , 3)}}</td>
                                       @endif
                                       <td> {{number_format($item->current_balance , 3)}}</td>
                                       <td> {{number_format($item->admin_balance , 3)}}</td>
                                       <td>{{$item->transaction_status}}</td>
                                       <td>{{$item->message}}</td>
                                    </tr>
                                    @endforeach 
                                 </tbody>
                              </table>
                           </div>
                           <!-- search transactions table-->
                           <div class="table-responsive" id="searchtxn">
                           </div>
                           <!-- search transactions table-->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
</script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
       var filter_date = $('#filter_date').val();
       var package_id = $('#package_id').val();
       var service_id = $('#service_id').val();
       var date_arr = filter_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filterAdminCommissiontxn", 
           data : {'start_date':start_date,'end_date':end_date,'package_id':package_id, 'service_id':service_id},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
					var Result = data;
					$("#alltxn").empty();
					$("#searchtxn").empty();
					$("#searchtxnbypackage").empty();
					$("#txnresultbypackage").empty();
					$("#transactionresult").empty();
					$("#searchtxn").append(' <table id="example1" class="table table-striped table-bordered "> <thead>  <tr><th>Name</th> <th>Transaction Date</th> <th>Transaction ID</th> <th>Package</th> <th>Customer Name</th> <th>Aadhar / Card / Account No. / Mobile No.</th> <th>Service Name</th> <th>Transaction Type / Service Type</th> <th>RRN/UTR/Biller Txn</th> <th>Bill / Txn Amount</th> <th>Commission / Surcharge</th> <th>Admin Commission</th> <th>Current Balance</th> <th>Total Admin Commission</th> <th>Check Status</th> <th>Message / Remarks</th> </tr></thead>   <tbody id="transactionresult"></tbody></table>');
					$.each(Result,function(key,value){
						var tr ='';

						var url = '{{ route("view-member", ":id") }}';
                   	url = url.replace(':id', value.user_id);
                     var v_type = value.commission_type;

                   	tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+' <span> ('+value.mobile+')</span></a></td>';

                     tr += '<td><span style="display: none;">'+moment(value.request_transaction_time).format('YYYYMMDDHHmmss')+'</span>'+moment(value.request_transaction_time).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.dms_txn_id+'</td><td>'+value.package_name+'</td><td>'+value.customer_name+'</td><td>'+value.aadhar_number+'</td><td>'+value.transaction_category+'</td><td>'+value.transaction_type+'</td> <td>'+value.original_transaction_id+'</td> <td> '+parseFloat(value.transaction_amount).toFixed(3)+'</td><td> '+parseFloat(value.commission).toFixed(3)+'</td>';

                     if (v_type == "credit") {
                        tr += '<td style="color: green;"> '+parseFloat(value.admin_commission).toFixed(3)+'</td>';
                     }else{
                        tr += '<td style="color: red;"> '+parseFloat(value.admin_commission).toFixed(3)+'</td>';
                     }

                     tr += '<td> '+parseFloat(value.current_balance).toFixed(3)+'</td> <td> '+parseFloat(value.admin_balance).toFixed(3)+'</td> <td>'+value.transaction_status+'</td><td>'+value.message+'</td></tr>';

                   		$('#transactionresult').append(tr);
                   	});
				}
   
                 $('#example1').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchRequestbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
       var filter_Request_date = $('#filter_Request_date').val();
       var package_Request_id = $('#package_Request_id').val();
       var date_arr = filter_Request_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filterUpiSendReqTxn", 
           data : {'start_date':start_date,'end_date':end_date,'package_id':package_Request_id,},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                  var Result = data;
                   $("#allRequesttxn").empty();
                   $("#searchRequesttxn").empty();
                   $("#searchtxnbypackage").empty();
                   $("#txnresultbypackage").empty();
                   $("#txnRequestresult").empty();
                   $("#searchRequesttxn").append(' <table id="example2" class="table table-striped table-bordered ">   <thead>      <tr>  <th>Name</th><th>Date</th><th>Transaction ID</th><th>UPI ID</th><th>Request Name</th><th>Mobile No</th><th>UPI Txn ID</th><th>Request Amount</th><th>Pay Amount</th><th>Commission</th><th>Current Balance</th><th>Message</th><th>Status</th><th>Action</th>       </tr>   </thead>   <tbody id="txnRequestresult">        </tbody></table>');
                  $.each(Result,function(key,value){
                     var user_role = '<?php echo Auth::user()->role; ?>';
                      var url = '{{ route("view-member", ":id") }}';
                      url = url.replace(':id', value.user_id);
                     if (value.amount == null) {
                       var r_amount = '0.000';
                     }else{
                       var r_amount = value.amount;
                     }
      
                     if (value.payer_amount == null) {
                       var r_payer_amount = '0.000';
                     }else{
                       var r_payer_amount = value.payer_amount;
                     }
                     if (value.commission == null) {
                       var r_commission = '0.000';
                     }else{
                       var r_commission = value.commission;
                     }
                     if (value.current_balance == null) {
                       var r_current_balance = '0.000';
                     }else{
                       var r_current_balance = value.current_balance;
                     } 

                     var tr =''; 
                     if (user_role == 'super admin') {
                       tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+' <span> ('+value.mobile+')</span></a></td>';
                     }else{
                       tr += '<tr><td>'+value.first_name+' '+value.last_name+'<span> ('+value.mobile+')</span></td>';
                     }
                     tr += '<td>'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.dms_txn_id+'</td><td>'+value.upi_id+'</td><td>'+value.name+'</td><td>'+value.mobile+'</td><td>'+value.merchantTranId+'</td><td> '+parseFloat(r_amount).toFixed(3)+'</td><td> '+parseFloat(r_payer_amount).toFixed(3)+'</td><td> '+parseFloat(r_commission).toFixed(3)+'</td><td> '+parseFloat(r_current_balance).toFixed(3)+'</td><td>'+value.message+'</td><td>'+value.status+'</td><td class=""> &nbsp; <a href="/upitxnStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> &nbsp; <a href="/upicallbackStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a></td></tr>';
                       // alert(tr);
                   $('#txnRequestresult').append(tr);
                      
                     // $("#txnRequestresult").append(' <tr><td>'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.dms_txn_id+'</td><td>'+value.upi_id+'</td><td>'+value.name+'</td><td>'+value.mobile+'</td><td> '+parseFloat(r_amount).toFixed(3)+'</td><td> '+parseFloat(r_payer_amount).toFixed(3)+'</td><td>'+value.merchantTranId+'</td><td>'+value.message+'</td><td>'+value.status+'</td><td class=""> &nbsp; <a href="/upitxnStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> &nbsp; <a href="/upicallbackStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a></td></tr>');
                  });
               }
   
                 $('#example2').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchmyqrbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
       var filter_myqr_date = $('#filter_myqr_date').val();
       var package_myqr_id = $('#package_myqr_id').val();
       var date_arr = filter_myqr_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filterUpiMyqrTxn", 
           data : {'start_date':start_date,'end_date':end_date,'package_id':package_myqr_id,},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                  var Result = data;
                   $("#allmyqrtxn").empty();
                   $("#searchmyqrtxn").empty();
                   $("#searchtxnbypackage").empty();
                   $("#txnresultbypackage").empty();
                   $("#txnmyqrresult").empty();
                   $("#searchmyqrtxn").append(' <table id="example3" class="table table-striped table-bordered ">   <thead>      <tr>  <th>Name</th><th>Date</th><th>Transaction ID</th><th>Request Name</th><th>Mobile No</th><th>UPI Txn ID</th><th>Pay Amount</th><th>Commission</th><th>Current Balance</th><th>Status</th><th>Action</th>       </tr>   </thead>   <tbody id="txnmyqrresult">        </tbody></table>');
                  $.each(Result,function(key,value){
                     var user_role = '<?php echo Auth::user()->role; ?>';
                      var url = '{{ route("view-member", ":id") }}';
                      url = url.replace(':id', value.user_id);
                     if (value.payer_amount == null) {
                       var r_payer_amount = '0.000';
                     }else{
                       var r_payer_amount = value.payer_amount;
                     }

                     if (value.commission == null) {
                       var r_commission = '0.000';
                     }else{
                       var r_commission = value.commission;
                     }
                     if (value.current_balance == null) {
                       var r_current_balance = '0.000';
                     }else{
                       var r_current_balance = value.current_balance;
                     } 
                     
                     var tr =''; 
                     if (user_role == 'super admin') {
                       tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+' <span> ('+value.mobile+')</span></a></td>';
                     }else{
                       tr += '<tr><td>'+value.first_name+' '+value.last_name+'<span> ('+value.mobile+')</span></td>';
                     }
                     tr += '<td>'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.dms_txn_id+'</td><td>'+value.name+'</td><td>'+value.mobile+'</td><td>'+value.merchantTranId+'</td><td> '+parseFloat(r_payer_amount).toFixed(3)+'</td><td> '+parseFloat(r_commission).toFixed(3)+'</td><td> '+parseFloat(r_current_balance).toFixed(3)+'</td><td>'+value.status+'</td><td class=""> &nbsp; <a href="/upitxnStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> &nbsp; <a href="/upicallbackStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a></td></tr>';
                       // alert(tr);
                   $('#txnmyqrresult').append(tr);

                     // $("#txnmyqrresult").append(' <tr><td>'+moment(value.created_at).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.dms_txn_id+'</td><td>'+value.name+'</td><td>'+value.mobile+'</td><td> '+parseFloat(r_payer_amount).toFixed(3)+'</td><td>'+value.merchantTranId+'</td><td>'+value.status+'</td><td class=""> &nbsp; <a href="/upitxnStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> &nbsp; <a href="/upicallbackStatusCheck/'+value.merchantTranId+'" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a></td></tr>');
                  });
               }
   
               $('#example3').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>
<script type="text/javascript">
   $('#example4').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
   $('#example5').DataTable( {
        dom: 'Bfrtip',
        "pageLength": 20,
         buttons: [
           'excel', 'pdf', 'print', 
         ]
    } );
</script>
<script type="text/javascript">
// function fundtransferButton() {
   $('.check_status').click(function(){
    var SITEURL = '{{URL::to('')}}';
     var merchantTranId = $(this).data('mrid');
    Swal.fire({
        title: 'Do you want to continue?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Continue',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
       }).then((data) => {
        if (data.value) {
          $('#Dv_Loader').show();
            $.ajax({
              type:"POST",
              url:"/upitxnStatusCheck", 
              data : {'merchantTranId':merchantTranId,},
              dataType: "json",
              success:function(data)
              {   
                $('#Dv_Loader').hide();
                if (data.permission_code == "PER111") {
                  $("#resp_message").empty();
                  $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                    
                    setTimeout(function () {
                      window.location.href = SITEURL + '/index';
                    }, 3000);
                 }else{
                    // if (data.code == "000" ) {
                        // alert(data.code);
                        $("#errormessage").empty();

                        var Res_request_type = data.txn_type;
                        var Res_upi_txnid = data.merchantTranId;
                        var Res_txn_dateTime = moment(data.created_at).format('DD-MM-YYYY hh:mm:ss A');
                        var Res_txn_status = data.status;
                        var Res_sender_name = data.name;
                        var Res_sender_mobile = data.mobile;
                        var Res_request_amount = data.amount;
                        var Res_payer_name = data.payer_name;
                        var Res_payer_upi_id = data.payerVA;
                        var Res_txn_amount = data.payer_amount;
                        var Res_commission = data.commission;
                        var Res_wallet_bal = data.current_balance;
                      
                      $('#Resp_request_type').html(Res_request_type);
                      $('#Resp_upi_txnid').html(Res_upi_txnid);
                      $('#Resp_txn_dateTime').html(Res_txn_dateTime);
                      $('#Resp_txn_status').html(Res_txn_status);
                      $('#Resp_sender_name').html(Res_sender_name);
                      $('#Resp_sender_mobile').html(Res_sender_mobile);
                      $('#Resp_request_amount').html(Res_request_amount);
                      $('#Resp_payer_amount').html(Res_payer_name);
                      $('#Resp_payer_upi_id').html(Res_payer_upi_id);
                      $('#Resp_txn_amount').html(Res_txn_amount);
                      $('#Resp_commission').html(Res_commission);
                      $('#Resp_wallet_bal').html(Res_wallet_bal);

                      $('#getreceiptModal').modal({backdrop: 'static', keyboard: false});

                    // }else{
                    //   var error = data.message;
                    //     $("#errormessage").empty();
                    //     $('#errormessage').html(error).css('color', 'red');
                    // }
                  }
              }
           });
        } else if (data.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
              title: 'Cancelled',
              type: 'error',
              confirmButtonClass: 'btn btn-success',
            })
        }
    })
});
// }
</script>


@endsection