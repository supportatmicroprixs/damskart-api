@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">AEPS Transaction's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Reports
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <form action="{{ route('aeps-report') }}" method="post" name="fund_transfer_form" class="form-horizontal">
                           @csrf
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php if($user->role == "super admin"): ?>
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                             </div>
                                             <select name="package_id" id="package_id" class="form-control">
                                                <option value="" selected="">Select Package</option>
                                                @foreach($package as $item)
                                                <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th >Txn ID</th>
                                       <th >Txn Date</th>
                                       <th >Txn Type</th>
                                       <th >Aadhar No.</th>
                                       <th >RRN</th>
                                       <th >Amount</th>
                                       <th >Status</th>
                                       <th >Message</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($aepstransactions as $item)
                                    <tr>
                                    	<td >XX{{ substr($item->dms_txn_id, 9) }}</td>
                                       <td ><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                       <td >{{$item->transaction_type}}</td>
                                       <td> XXX{{ substr($item->aadhar_number, -4)}} <br />
                                       </td>
                                       <td >
                                           {{$item->bankRRN}}
                                       </td>
                                       <td >&#x20B9; {{number_format($item->transaction_amount,3)}}</td>
                                       <td >{{$item->transaction_status}}</td>
                                       <td data-placement="left" data-html="true" rel="tooltip" title="{{$item->message}}" data-toggle="tooltip"> {{ substr($item->message, 0, 20) }} <br /></td>
                                       
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- search transactions table-->
                     <div class="card-content" id="searchtxn">
                     </div>
                     <!-- search transactions table-->
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script>
   $(function() {
      $("[rel='tooltip']").tooltip();
      
      $('#example').on('draw.dt', function() {
         $("[rel='tooltip']").tooltip();
         //$('[data-toggle="tooltip"]').tooltip(); // Or your function for tooltips
      });

      $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>

@endsection