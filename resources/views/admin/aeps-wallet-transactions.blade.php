@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">AEPS Wallet Transaction's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Reports
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div> 
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php if($user->role == "super admin"): ?>
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-dropbox"></i></button>
                                             </div>
                                             <select name="package_id" id="package_id" class="form-control">
                                                <option value="" selected="">Select Package</option>
                                                @foreach($package as $item)
                                                <option value="{{$item->id}}">{{$item->package_name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </fieldset>
                                    </div>
                                    <?php endif ?>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Push Notifications</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Date</th>
                                       <th>Transaction ID</th>
                                       <th>Narration</th>
                                       <th>Amount</th>
                                       <th>Type</th>
                                       <th>Balance</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($wallet_transaction as $key)
                                    <tr>
                                       @if(Auth::user()->role == 'super admin') 
                                       <td><a href="{{route('view-member', $key->id)}}">{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span> ({{$key->mobile}})</span></a></td>
                                       @else 
                                       <td>{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span> ({{$key->mobile}})</span></td>
                                       @endif
                                       <td><span style="display: none;">{{date('YmdHis', strtotime($key->date))}}</span>{{date('d-m-Y h:i:s A', strtotime($key->date))}}</td>
                                       <td>{{$key->transaction_id}}</td>
                                       <td>{{$key->narration}}</td>
                                       @if($key->type == 'credit')
                                       <td style="color: green;"> {{number_format($key->amount,3)}}</td>
                                       @elseif($key->type == 'debit')
                                       <td style="color: red;"> {{number_format($key->amount,3)}}</td>
                                       @endif
                                       <td>{{ucfirst($key->type)}}</td>
                                       <td> {{number_format($key->current_balance,3)}}</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <!-- search transactions table-->
                     <div class="card-content" id="searchtxn">
                     </div>
                     <!-- search transactions table-->
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')

<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchbutton').click(function(){
       var filter_date = $('#filter_date').val();
       var package_id = $('#package_id').val();
       // if (filter_date == '') {
       //     alert("Select Date Range");
       //     return false;
       // }
       var SITEURL = '{{URL::to('')}}';
       var date_arr = filter_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filteraepswallettxn", 
           data : {'start_date':start_date,'end_date':end_date,'package_id':package_id,},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                 var Result = data.wallet_transaction;
                  $("#alltxn").empty();
                  $("#searchtxn").empty();
                  $("#searchtxnbypackage").empty();
                  $("#txnresultbypackage").empty();
                  $("#transactionresult").empty();
                  $("#searchtxn").append(' <div class="card-body card-dashboard"><div class="table-responsive"><table id="example1" class="table table-striped table-bordered ">   <thead>      <tr>   <th>Name</th><th>Date</th><th>Transaction ID</th><th>Narration</th><th>Amount</th><th>Type</th><th>Balance</th>       </tr>   </thead>   <tbody id="transactionresult">        </tbody></table></div></div>');
                 $.each(Result,function(key,value){
                 var user_role = data.user_role;
                  var v_type = value.type;
                  var v_user_id = value.user_id;
                  var url = '{{ route("view-member", ":id") }}';
                  url = url.replace(':id', value.user_id);
     
                    var tr =''; 
                    if (data.user_role == 'super admin') {
                      tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+'<span> ('+value.mobile+')</span></a></td>';
                    }else{
                      tr += '<tr><td>'+value.first_name+' '+value.last_name+'<span> ('+value.mobile+')</span></td>';
                    }
                      tr += '<td>'+moment(value.date).format('DD-MM-YYYY hh:mm:ss A')+'</td><td>'+value.transaction_id+'</td><td>'+value.narration+'</td>';
                    if (v_type == "credit") {
                      tr += '<td style="color: green;"> '+parseFloat(value.amount).toFixed(3)+'</td>';
                    }else{
                      tr += '<td style="color: red;"> '+parseFloat(value.amount).toFixed(3)+'</td>';
                    }
                      tr += '<td>'+v_type+'</td><td> '+parseFloat(value.current_balance).toFixed(3)+'</td></tr>';
                        // alert(tr);
                    $('#transactionresult').append(tr);
                 });
               }
   
                $('#example1').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>

@endsection