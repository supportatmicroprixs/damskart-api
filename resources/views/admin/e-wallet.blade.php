@extends('admin.layouts.main')
@section('css')
<!-- <link rel="stylesheet" href="sweetalert2.min.css"> -->
<style type="text/css">
   .text-white{
   color: white;
   }
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
   /*@media print{
   body *{
   visibility: hidden;
   }
   .print-container, .print-container *{
   visibility: visible;
   }
   .print-container, #cl_button *{
   visibility: hidden;
   }
   }*/
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Main Wallet</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Wallet
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <!-- Column selectors with Export Options and print table -->
         
         <section id="floating-label-layouts">
            @if(Auth::user()->role == 'retailer' || Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor'|| Auth::user()->role == 'distributor')
            <div class="row match-height">
               <div class="col-md-4">
                  <div class="card card-deck">
                     <div class="card-content">
                        <div class="card-body">
                           <div class="card-block text-center" style="font-family: inherit;">
                              <h6 class="text-dark">Marchent ID : {{Auth::user()->mobile}}</h6>
                              <hr>
                              <div style="padding: 0px;" class="card-block text-white">
                                 <h6 class="text-dark">Bank Name : ICICI Bank</h6>
                                 <h6 class="text-dark">IFSC Code : ICIC0000106</h6>
                                 <h6 class="text-dark">Account Number : DMSPAY{{Auth::user()->mobile}}</h6>
                              </div>
                              <div class="m-b-25 d-none hide">
                                 <img src="{{asset('images/wallet.png')}}"  class="img-radius" width="80" alt="e-walllet">
                              </div>
                              <hr>
                              <h6 class="text-dark">Total Balance : &#x20B9; {{number_format(Auth::user()->ewallet,3)}}</h6>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Add Wallet</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-label-group position-relative has-icon-left">
                                          <input type="number" id="amount1" class="form-control" name="amount1" value="" placeholder="Amount"  min="1" required="">
                                          <div class="form-control-position">
                                             <i class="fa fa-inr"></i>
                                          </div>
                                          <label for="first-name-floating-icon">Add Wallet Balance</label>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <a href="javascript:void(0)" class="img-radius mr-1 mb-1 buy_now"><button type="button" class="btn btn-outline-success ">Add Fund</button></a>
                                       <button type="reset" class="btn btn-outline-warning ">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>


                        </div>
                     </div>
                  </div>
               </div>
            </div>

            @endif

            @if(Auth::user()->role == 'super admin'|| Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor'|| Auth::user()->role == 'distributor')
            <div class="row match-height11">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Transfer Fund</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form action="{{ route('credit-fund') }}" method="POST" enctype="multipart/form-data" class="form form-vertical">
                            {{csrf_field()}}
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="first-name-icon">Select Type</label>
                                          <div class="position-relative has-icon-left">
                                             <select class="form-control" id="type" name="type" required="">
                                                <option value="">Select Type</option>
                                                <option value="debit">Debit</option>
                                                <option value="credit">Credit</option>
                                             </select>
                                             <div class="form-control-position">
                                                <i class="feather icon-credit-card"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="email-id-icon">Select Member</label>
                                          <div class="position-relative has-icon-left">
                                             <select class="form-control" name="user_id" id="bank_detail" required>
                                                <option value="">Select Member</option>
                                                 @foreach($member as $key)
                                                 <option value="{{$key['id']}}"  balance="{{$key['ewallet']}}"> {{ucfirst($key['first_name']).' - '.$key['mobile'].' (Balance : '.number_format($key['ewallet'],3).')'}}</option>
                                                 @endforeach
                                             </select>
                                             <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="contact-info-icon">Amount</label>
                                          <div class="position-relative has-icon-left">
                                             <input type="number" class="form-control" max="" value="{{ old('amount') }}" id="amount" name="amount" placeholder="Enter Amount">
                                             <div class="form-control-position">
                                                <i class="fa fa-inr"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group">
                                          <label for="password-icon">Narration</label>
                                          <div class="position-relative has-icon-left">
                                             <textarea class="form-control" cols="3" rows="3" placeholder="Narration" name="naration" required=""></textarea>
                                             <div class="form-control-position">
                                                <i class="feather icon-edit"></i>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endif
         </section>
         
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Main Wallet Transactions</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th style="width: 150px;">Transaction Date</th>
                                        <th>Transaction ID</th>
                                        <th>Narration</th>
                                        <th>Amount</th>
                                        <th>Type</th>
                                        <th>Balance</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                      @foreach ($wallet_transaction as $key)
                                      <tr>
                                         @if(Auth::user()->role == 'super admin') 
                                         <td>
                                          <a href="{{route('view-member', $key->user_id)}}">{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span> ({{$key->mobile}})</span></a>
                                          
                                         </td>
                                         @else 
                                         <td>{{ucfirst($key->first_name).' '.ucfirst($key->last_name)}}<span> ({{$key->mobile}})</span></td>
                                         @endif
                                         <td><span style="display: none;">{{date('YmdHis', strtotime($key->date))}}</span>{{date('d-m-Y h:i:s A', strtotime($key->date))}}</td>
                                         <td>{{$key->transaction_id}}</td>
                                         <td>{{$key->narration}}</td>
                                         @if($key->type == 'credit')
                                         <td style="color: green;">&#x20B9; {{number_format($key->amount,3)}}</td>
                                         @elseif($key->type == 'debit')
                                         <td style="color: red;">&#x20B9; {{number_format($key->amount,3)}}</td>
                                         @endif
                                         <td>{{ucfirst($key->type)}}</td>
                                         <td>&#x20B9; {{number_format($key->current_balance,3)}}</td>
                                      </tr>
                                      @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                <div class="modal fade" id="messageDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Payment Receipt</h4>
                             <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                          </div> 
                          <div class="modal-body " id="getCode" >
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                          <div id="invoice-template" class="card-body">
                                              <div id="invoice-company-details" class="row">
                                                  <div class="col-sm-6 col-12 text-left pt-1">
                                                      <div class="media pt-0">
                                                          <img src="{{asset('images/Damskart Logo.png')}}" style="width: 120px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <h1>Payment Details</h1>
                                                      <div class="invoice-details mt-2">
                                                          <h6>Status</h6>
                                                          <p><b style="font-size: x-large;" id="getstatus"></b></p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-customer-details" class="row pt-2 ">
                                                  <div class="col-sm-6 col-12 text-left">
                                                      <div class="recipient-info my-2">
                                                          <h6>Transaction Id</h6>
                                                          <p><b id="transaction_id"></b></p>
                                                          <h6>Message</h6>
                                                          <p id="getmsg"></p>
                                                      </div> 
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <div class="company-info my-2">
                                                          <h6>Amount(&#x20B9;)</h6>
                                                          <p id="getamount"></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-12">
                                                      <div class="company-info my-0">
                                                          <p style="color: red;margin-bottom: 0px;"><b id="countdown"></b></p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer" >
                             <button type="button" class="btn btn-secondary text-right" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                             <!-- <button type="button" class="btn btn-primary btn-print text-right" ><i class="feather icon-file-text"></i> Print</button> -->
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="paymentDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" id="myModalLabel" style=" color: white;">Payment Info</h4>
                          </div>
                          <div class="modal-body" id="result" >
                             <h3 id="rozerpaytax"></h3> 
                          </div>
                          <div class="modal-footer" >
                             <button type="button" class="btn btn-secondary text-right" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                          </div>
                       </div>
                    </div>
                 </div>
                <div class="divWaiting" id="Dv_Loader" style="display: none;">
                  <button class="btn btn-primary mb-1 round" type="button" >
                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
                  &nbsp;Loading... 
                  </button>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script> -->
<script type="text/javascript">
   function paytmtax(){
     var amount = document.getElementById("amount1").value;
     if (amount == null || amount == "") {
       alert("Amount Should Be Complusory");
       return false;
     }
     alert("Nominal Charge of 2.4% is applicable on adding money.");
   }
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<script>
   var SITEURL = '{{URL::to('')}}';
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   }); 
   $('body').on('click', '.buy_now', function(e){
     var requestamount = parseFloat($("#amount1").val());
     var taxmount =  parseFloat(requestamount * 2.5 /100);
     var totalAmount = (+(requestamount+taxmount).toFixed(2));
     // alert(totalAmount);
   if (totalAmount == null || Number.isNaN(totalAmount) || totalAmount == "") {
      Swal.fire({
        title: 'Amount should not be empty',
        type: 'warning',
        showConfirmButton: false,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'OK',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
      });
      return false;
    }
     // alert("Nominal Charge of 2.5% is applicable on adding money.");
     $('#Dv_Loader').show();
    	$.ajax({
           type:"get",
           url:"/getorderid", 
           data : {'totalAmount':totalAmount,},
           dataType: "json",
           success:function(data)
           {   
            $('#Dv_Loader').hide();
              var order_id = data.orderId;
              var ramount = data.amount;
              // alert(order_id);
              Swal.fire({
                title: 'Nominal Charge of 2.5% is applicable on adding money. Do you want to continue?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                buttonsStyling: false,
              }).then((data) => {
                if (data.value) {
                  var options = {
                     "key": "{{ env('RAZOR_KEY') }}",
                     "amount": ramount, // 2000 paise = INR 20
                     "name": "DAMSKART",
                     "description": "Payment",
                     "image": "https://damskart.com/public//images/Damskart Logo.png",
                      "prefill": {
                         "contact": '<?php echo $user->mobile;?>',
                         "email":   '<?php echo $user->email;?>',
                     },
                     "theme": {
                         "color": "#528FF0"
                     },
                     "order_id": order_id,
                     "handler": function (response){
                           $.ajax({
                             url: SITEURL + '/rozerpayment',
                             type: 'post',
                             dataType: 'json',
                             data: {
                              razorpay_payment_id: response.razorpay_payment_id , 
                               totalAmount : totalAmount, 
                               transaction_type : 'Add fund by ICICI Bank' 
                             }, 
                             success: function (msg) {
                                   var Result = msg;
                                   var transaction_id = Result.transaction_id;
                                   var getmsg = Result.msg;
                                   var getamount = Result.amount;
                                   var getstatus = Result.status;
                                    var timeleft = 10;
                                    var downloadTimer = setInterval(function(){
                                      if(timeleft <= 0){
                                        clearInterval(downloadTimer);
                                        document.getElementById("countdown").innerHTML = "Finished";
                                      } else {
                                        document.getElementById("countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";
                                      }
                                      timeleft -= 1;
                                    }, 1000);
                                        $('#transaction_id').html(transaction_id);
                                        $('#getmsg').html(getmsg);
                                        $('#getamount').html(getamount);
                                        $('#getstatus').html(getstatus);
                                        $('#messageDialog').modal('show');
                                 setTimeout(location.reload.bind(location), 10000);
                                 // window.location.href = SITEURL + '/e-wallet';
                             }
                         });
                       
                     },
                   
                   };
                   var rzp1 = new Razorpay(options);
                   rzp1.open();
                   e.preventDefault();
                } else if (data.dismiss === Swal.DismissReason.cancel) {
                   Swal.fire({
                      title: 'Cancelled',
                      type: 'error',
                      confirmButtonClass: 'btn btn-success',
                    })
                }
              })

           }
        });
     
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
    $("#type").change(function() {
      var type = document.getElementById('type').value; 
        if (type == 'credit') {
            <?php if($user->role == "super admin"): ?> 
              $('#amount').attr('max',null);  
            <?php else: ?>
              var i = '<?php echo $user->ewallet; ?>';
              $('#amount').attr('max',i);  
            <?php endif ?>
          }else{
            var element = $('#bank_detail').find('option:selected'); 
            var myTag = element.attr("balance"); 
            $('#amount').attr('max',myTag); 
            
          }
     });
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
      $('#bank_detail').change(function(){
          var type = document.getElementById('type').value; 
          if (type == 'credit') {
            <?php if($user->role == "super admin"): ?> 
              $('#amount').attr('max',null);  
            <?php else: ?>
              var i = '<?php echo $user->ewallet; ?>';
              $('#amount').attr('max',i);  
            <?php endif ?>
          }else{
            var element = $(this).find('option:selected'); 
            var myTag = element.attr("balance"); 
            $('#amount').attr('max',myTag); 
   
          }
      });
   });
</script>
@endsection