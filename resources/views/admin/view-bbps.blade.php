@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
    @media print{
         body *{
         visibility: hidden;
         }
         .print-container, .print-container *{
         visibility: visible;
         }
         .print-container, #cl_button *{
         visibility: hidden;
         }
         }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">BBPS Biller's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item ">Setting
                        </li> 
                        <li class="breadcrumb-item ">Package
                        </li> 
                        <li class="breadcrumb-item active">Package Service's
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Fund Transaction's List</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="col-12">
                          <div class="table-responsive border rounded px-1 ">
                             <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2">Apply All</h6>
                             <div class="row match-height">
                                <div class="col-md-12">
                                   <div class="row">
                                      <div class="col-md-12">
                                         <div class="card" style="margin-bottom: 0rem;">
                                            <div class="card-content">
                                               <div class="card-body">
                                                  <div class="row">
                                                     <div class="col-md-5 col-12 mb-1">
                                                        <fieldset>
                                                           <div class="input-group">
                                                              <div class="input-group-prepend">
                                                                 <button class="btn btn-primary" type="button"><i class="fa fa-inr"></i></button>
                                                              </div>
                                                              <input type="text" class="form-control apply_commission"  placeholder="Commission" name="apply_commission" value="" />
                                                           </div>
                                                        </fieldset>
                                                     </div>
                                                     <div class="col-md-5 col-12 mb-1">
                                                        <fieldset>
                                                           <div class="input-group">
                                                              <div class="input-group-prepend">
                                                                 <button class="btn btn-primary" type="button"><i class="fa fa-tag"></i></button>
                                                              </div>
                                                              <select class="form-control apply_commission_type"  name="apply_commission_type">
                                                                <option value="flat">Flat</option>
                                                                <option value="percentage">Percentage</option>
                                                              </select>
                                                           </div>
                                                        </fieldset>
                                                     </div>
                                                     <div class="col-md-2 col-12 mb-1">
                                                        <div class="input-group-append">
                                                           <button  type="submit"  class="btn btn-primary change" onclick="changedetected()"><i class="fa fa-check"></i> Apply</button>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                        </div>
                       <hr>
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                            <form  action="/updatebbps/{{$billers->id}}/{{$cat_name}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Operator Name</th>
                                       <th>Biller Id</th>
                                       <th>Commission</th>
                                       <th>Commission Type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($commission as $item)
                                    <tr>
                                       <td>{{$item->biller_name}}</td>
                                       <td>{{$item->biller_id}}</td>
                                       <td>
                                          <input type="hidden" name="id[]" value="{{$item->id}}">
                                          <input type="number" name="commission[]" step="any" id="commission" placeholder="commission" class="form-control round apply_all" value="{{$item->commission}}" ></td>
                                       <td>
                                          <select class='form-control round apply_all_type' name='commission_type[]' id='commission_type'>
                                             <option value="flat" <?php if ($item->commission_type =='flat'){echo ' selected';} ?>>Flat</option>
                                             <option value="percentage"<?php if ($item->commission_type =='percentage') {echo ' selected';}?> >Percentage</option>';
                                              
                                          </select>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                               <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   function changedetected(){
       var apply_commission = $("input[name=apply_commission]").val();
       var apply_commission_type = $("select[name=apply_commission_type]").val();
       $('.apply_all').val(apply_commission);
       $('.apply_all_type').val(apply_commission_type);
     }
</script>
@endsection