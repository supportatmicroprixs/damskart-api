   @extends('admin.layouts.main')
@section('css')
 <style type="text/css">
         .divWaiting {
         position: fixed;
         background-color: #FAFAFA;
         z-index: 2147483647 !important;
         opacity: 0.8;
         overflow: hidden;
         text-align: center;
         top: 0;
         left: 0;
         height: 100%;
         width: 100%;
         padding-top: 20%;
         }
      </style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Buy Token</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Member</a>
                        </li>
                        <li class="breadcrumb-item active">Buy Token
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Form wizard with step validation section start -->
         <section id="validation">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Buy Details</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           @if ($errors->any())
                           <div  class="alert alert-danger alert-block">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                           @endif
                           @if ($message = Session::get('info'))
                           <div class="alert alert-primary alert-block">
                              <strong>{{ $message }}</strong>
                           </div>
                           @endif 
                           @if ($message = Session::get('danger'))
                           <div class="alert alert-danger alert-block">
                              <strong>{{ $message }}</strong>
                           </div>
                           @endif 
                           <form action="{{ route('add-tokenbuy') }}" method="POST" enctype="multipart/form-data" class="wizard-circle">
                               @csrf
                              <!-- Step 1 -->
                              <h4><i class="step-icon feather icon-user"></i> Token's Details</h4>
                              <fieldset>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="role">
                                          Role
                                          </label>
                                          <select class="custom-select form-control" id="role" name="role" required="">
                                             <option value="">Select Role</option>
                                             <?php if (Auth::user()->role == 'super admin'): ?>
                                             <option  value="admin">Admin</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin'||Auth::user()->role == 'admin'): ?>
                                             <option  value="sdistributor">Super Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'): ?>
                                             <option  value="mdistributor">Master Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor'): ?>
                                             <option  value="distributor">Distributor</option>
                                             <?php endif ?>
                                             <?php if (Auth::user()->role == 'super admin'||Auth::user()->role == 'admin' || Auth::user()->role == 'sdistributor'|| Auth::user()->role == 'mdistributor' || Auth::user()->role == 'distributor'): ?>
                                             <option  value="retailer">Retailer</option>
                                             <?php endif ?>
                                          </select>
                                       </div>
                                          
                                    </div>
                                    
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="package">
                                          Package
                                          </label>
                                           <select class="custom-select form-control" id="package_id" name="package_id" required></select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="firstName3">
                                          Token Allow
                                          </label>
                                          <input type="text" class="form-control required" id="token" name="token" placeholder="Token" value="{{ old('token') }}" required="" onInput="get_price(this.value)">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label for="lastName3">
                                          Price
                                          </label>
                                          <input type="text" class="form-control required" id="price" name="price" value="{{ old('price') }}" placeholder="Price" required="" readonly=readonly>
                                       </div>
                                    </div>
                                 </div>
                                
                              </fieldset>
                              <a class="btn btn-warning btn-inline waves-effect waves-light mb-2" href="{{ route('token-list') }}">Cancel</a>
                              <button class="btn btn-primary btn-inline waves-effect waves-light mb-2" type="submit" id="register_button" >Submit</button>
                              
                              <br>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Form wizard with step validation section end -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script>

function get_price(value){
    
    var package_id =  $('#package_id').val();
    if(value==''){
        token_value = 0;
    }else{
        token_value = value;
    }
    
    if(package_id){
        $.ajax({
           type:"get",
           url:"/getPackageAmount/"+package_id, //Please see the note at the end of the post**
           success:function(response)
           {
              price = parseInt(token_value)*parseInt(response.amount);
              $("#price").val(price);
           }
        });
     }
    
    
    
}

function getUsers(getRole){
    $.ajax({
               type:"get",
               url:"/getUsers/"+getRole, //Please see the note at the end of the post**
               success:function(res)
               {
                    if(res)
                    {
                        $("#user_id").empty();
                        $("#user_id").append('<option value="">Select Member</option>');
                        $.each(res,function(key,value){
                            $("#user_id").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
               }
            });
}
    document.getElementById('role').onchange = function(){
        document.getElementById('domain').disabled = (this.value === ''|| this.value === 'sdistributor'||this.value === 'mdistributor'||this.value === 'distributor'||this.value === 'retailer');
        document.getElementById('domain').hidden = (this.value === ''||this.value === 'sdistributor'||this.value === 'mdistributor'||this.value === 'distributor'||this.value === 'retailer')
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#package_id').change(function(){
            var token = $("#token").val();
            get_price(token);
        });
        $('#role').change(function(){
            var role = $(this).val();
           
            if(role){
            getUsers(role);
            $.ajax({
               type:"get",
               url:"/getPackage/"+role, //Please see the note at the end of the post**
               success:function(res)
               {
                    if(res)
                    {
                        $("#package_id").empty();
                        $("#package_id").append('<option value="">Select Package</option>');
                        $.each(res,function(key,value){
                            $("#package_id").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
               }
            });
            }
        });
    });
</script>
@endsection