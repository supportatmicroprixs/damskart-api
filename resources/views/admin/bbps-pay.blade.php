@extends('admin.layouts.main')
@section('css')
<style type="text/css">
    .divWaiting {
        position: fixed;
        background-color: #FAFAFA;
        z-index: 2147483647 !important;
        opacity: 0.8;
        overflow: hidden;
        text-align: center;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        padding-top: 20%;
    }
    .input_style{
        background-color: #ffffff !important;
        padding: 0.0rem 0.0em;
        border: 0px solid #D9D9D9;
    }

    @media print{
        body *{
            visibility: hidden;
        }
        .print-container, .print-container *{
            visibility: visible;
        }
        .print-container, #cl_button *{
            visibility: hidden;
        }
    }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">BBPS</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Service
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="multiple-column-form">
            <div class="row match-height">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header" id="bbps_logo">
                        <h4 class="card-title">Bharat Bill Payment System (BBPS) Billers</h4>
                        <!-- <button type="button" class="btn btn-success" data-backdrop="false" data-toggle="modal" data-target="#prepaidpaySuccessmodel">prepaidpaySuccessmodel</button> -->
                        <!-- <button type="button" class="btn btn-success" data-backdrop="false" data-toggle="modal" data-target="#prepaidpayPendingmodel">prepaidpayPendingmodel</button> -->
                        <!-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#rechargePlanDetail">Plan</button> -->

                        <img src="{{asset('images/bbps_logo_hor_colored (1).png')}}" style="width: 9%;margin-left: auto;">
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group" id="errormessage"></div>
                                       <div class="form-group">
                                          <label for="contact-info-icon">Biller Category *</label>
                                          <div class="position-relative has-icon-left">
                                             <select class="select2 form-control" name="biller_cat_name" id="biller_cat_name">
                                                <option value="">Select Biller</option>
                                                @foreach($bbps_biller_cat as $item)
                                                <option value="{{$item->service_name}}">{{$item->service_name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       <div class="form-group" id="operator_list"></div>
                                       <div class="form-group" id="customer_mobile_no"></div>
                                       <div id="circle_list"></div>
                                       <div id="param"></div>
                                       <div id="q_amount"></div>
                                 
                                    </div>
                                        
                                 </div>
                              </div>
                              <div class="col-12">
                                <div id="payment_button"></div>
                              </div>
                        </div>
                     </div>
                    </div>

                    <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                            <div class="modal-content">
                                <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                     <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                                </div>

                                <div class="modal-body" id="getCode" >
                                    <div class="row">
                                        <div class="col-md-12">
                                           <div class="content" style="text-align: center; margin-left: 0px;">
                                                <div id="invoice-template" class="card-body">
                                                    <h1>Bill Details</h1>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-12 text-left">
                                                            <div class="invoice-details">
                                                                <h6>Biller Name</h6>
                                                                <p id="bbpsbillername"></p>
                                                                <h6>Bill Number</h6>
                                                                <p id="bbpsbillNumber"></p>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6 col-12 text-right">
                                                            <div class="text-right">
                                                                <img src="{{asset('images/Bharat.png')}}" style="width: 60px;">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 >Customer Details</h4>
                                                    <div id="invoice-customer-details" class="row">
                                                        <div class="col-sm-6 col-12 text-left">
                                                            <div class="recipient-info">
                                                                <h6>Customer Name</h6>
                                                                <p id="bbpscustomerName"></p>
                                                                <h6>Mobile Number</h6>
                                                                <p id="bbpscustomermobile"></p>
                                                            </div>
                                                            <div class="recipient-contact pb-2">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-12 text-right">
                                                            <div class="company-info">
                                                                <h6>Bill Date</h6>
                                                                <p id="bbpsbillDate"></p>
                                                                <h6>Due Date</h6>
                                                                <p id="bbpsdueDate"></p>
                                                                <h6>Bill Period</h6>
                                                                <p id="bbpsbillPeriod"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="invoice-customer-details" class="row pt-0">
                                                        <div class="col-sm-12 col-12 text-left">
                                                            <div class="recipient-info my-2 row" id="inputparamdisplay">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider divider-center">
                                                        <div class="divider-text">
                                                           <div class="badge badge-pill badge-md badge-secondary">Payment Details</div>
                                                        </div>
                                                     </div>
                                                    <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                        <div class="row">
                                                            <div class="table-responsive col-12">
                                                                <table class="table table-borderless">
                                                                    <tbody id="bbpspaymentInformation">
                                                                        <tr>
                                                                            <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                            <td class="text-right"><h5 id="bbpsbillAmount"></h5>
                                                                              <input type="hidden" id="bbpscustConvFee" min="0" placeholder="Customer Convienence Fee" value="0" >
                                                                              <input type="hidden" id="RequestId">
                                                                              <input type="hidden" id="payment_mode"  value="Cash" name="payment_mode">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="invoice-customer-details" class="row pt-0">
                                                        <div class="col-sm-6 col-12 text-left">
                                                            <div class="recipient-info my-2" id="amountOptionsdisplay">
                                                            </div>
                                                            <div class="recipient-info my-2" id="additionalInfodisplay">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="justify-content: left;">
                                     <button type="submit" id="paybutton" class="btn btn-primary" >Pay</button>
                                     <button type="button" class="btn btn-secondary" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="getsuccessmodel" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                          </div> 
                          <div class="modal-body  print-container" id="getCode" >
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                          <div id="invoice-template" class="card-body">
                                              <div id="invoice-company-details" class="row">
                                                  <div class="col-sm-6 col-12 text-left pt-1">
                                                      <div class="media pt-1">
                                                          <img src="{{asset('images/Be Assured.png')}}" style="width: 120px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <h1>Payment Details</h1>
                                                      <div class="invoice-details mt-2">
                                                          <h6>Biller Name</h6>
                                                          <p id="Respbillername"></p>
                                                          <h6>Biller ID</h6>
                                                          <p id="Respbillerid"></p>
                                                          <h6>Bill Number</h6>
                                                          <p id="RespbillNumber"></p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-customer-details" class="row pt-2 ">
                                                  <div class="col-sm-6 col-12 text-left">
                                                      <div class="recipient-info my-2">
                                                          <h6>Customer Name</h6>
                                                          <p id="RespcustomerName"></p>
                                                          <h6>Mobile Number</h6>
                                                          <p id="Respcustomermobile"></p>
                                                          <h6>Txn Date & Time</h6>
                                                          <p><h5 id="Respdate_time"></h5></p>
                                                          <h6>Transaction ID</h6>
                                                          <p><h5 id="resptxnRefId"></h5></p>
                                                          <h6>Transaction Status</h6>
                                                          <p><h5 id="respresponseReason"></h5></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <div class="company-info my-2">
                                                          <h6>Bill Date</h6>
                                                          <p id="RespbillDate"></p>
                                                          <h6>Due Date</h6>
                                                          <p id="RespdueDate"></p>
                                                          <h6>Bill Period</h6>
                                                          <p id="RespbillPeriod"></p>
                                                          <h6>Init Channel</h6>
                                                          <p id="init_Channel"></p>
                                                          <h6>Approval Number</h6>
                                                          <p id="respapprovalRefNumber"></p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-items-details" class=" card bg-transparent shadow-1">
                                                  <div class="row">
                                                      <div class="table-responsive col-12">
                                                          <table class="table table-borderless">
                                                              <tbody>
                                                                  <tr>
                                                                      <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right"><h5 id="RespbillAmount"></h5></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right"  id="respCustConvFee"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Payment Mode</b></td>
                                                                      <td class="text-right" id="Resppayment_mode"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <th class="text-right">Total Amount (<i class="fa fa-inr"></i>)</th>
                                                                      <td class="text-right"><h5 id="RespbilltotalAmount"></h5></td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer" style="justify-content: center;">
                             <h5>Thanks for your Business!</h5>
                             <button type="button" class="btn btn-secondary" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="quickpaymentmodel" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                          </div> 
                          <div class="modal-body print-container" id="getCode">
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                          <div id="invoice-template" class="card-body">
                                              <div id="invoice-company-details" class="row">
                                                  <div class="col-sm-6 col-12 text-left pt-1">
                                                      <div class="media pt-1">
                                                          <img src="{{asset('images/Be Assured.png')}}" style="width: 120px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <h1>Payment Details</h1>
                                                        <div class="invoice-details mt-2">
                                                          <h6>Biller Name</h6>
                                                          <p id="QpRespbillername"></p>
                                                          <h6>Biller ID</h6>
                                                          <p id="QpRespbillerid"></p>
                                                        </div>

                                                        <div class="d-none hide">
                                                            <h6>Init Channel</h6>
                                                              <p id="Qpresp_init_Channel"></p>
                                                              <h6>Approval Number</h6>
                                                              <p id="QprespapprovalRefNumber"></p>
                                                        </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-customer-details" class="row pt-2 ">
                                                  <div class="col-sm-6 col-12 text-left">
                                                      <div class="recipient-info my-2">
                                                          <h6>Customer Name</h6>
                                                          <p id="QpRespcustomerName"></p>
                                                          <h6>Mobile Number</h6>
                                                          <p id="QpRespcustomermobile"></p>
                                                          
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <div class="company-info my-2">
                                                          <h6>Transaction ID</h6>
                                                          <h5 id="QpresptxnRefId"></h5>
                                                          <h6>Txn Date & Time</h6>
                                                          <h5 id="QpRespdate_time"></h5>
                                                          <h6>Transaction Status</h6>
                                                          <h5 id="QprespresponseReason"></h5>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="invoice-items-details" class="card bg-transparent shadow-1">
                                                  <div class="row">
                                                      <div class="table-responsive col-12">
                                                          <table class="table table-borderless">
                                                              <tbody>
                                                                  <tr>
                                                                      <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right"><h5 id="QpRespbillAmount"></h5></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right" id="QprespCustConvFee"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Payment Mode</b></td>
                                                                      <td class="text-right" id="QpResppayment_mode"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <th class="text-right">Total Amount (<i class="fa fa-inr"></i>)</th>
                                                                      <td class="text-right"><h5 id="QpRespbilltotalAmount"></h5></td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer" style="justify-content: center;">
                             <h5>Thanks for your Business!</h5>
                             <button type="button" class="btn btn-secondary" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="rechargePlanDetail" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document" style="max-width: 65%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                             </button>
                          </div>
                          <div class="modal-body" id="getCode" >
                            <div id="invoice-template" class="card-body">
                                <div id="invoice-company-details" class="row">
                                    <div class="col-sm-6 col-12 text-left">
                                        <div class="media">
                                            <img src="{{asset('images/Bharat.png')}}" style="width: 60px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12 text-right">
                                        <h1>Plan Details</h1>
                                        <div class="invoice-details mt-2">
                                        </div>
                                    </div>
                                </div>
                                <div id="invoice-items-details" class=" card bg-transparent shadow-1 mt-2">
                                  <div class="todo-task-list list-group" >
                                     <div class="todo-task-list-wrapper media-list">
                                        <div id="recharge_plan_response" style="margin-left: 10px;"></div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="prepaidpaySuccessmodel" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                          </div> 
                          <div class="modal-body print-container" id="getCode">
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                      <div id="invoice-template" class="card-body card-body-container">
                                      		<div class="d-none">
												<h6>Biller ID</h6>
												<p id="PRESSQpRespbillerid"></p>
												<h6>Mobile Number</h6>
												<p id="PRESSQpRespcustomermobile"></p>

												<h6>Customer Name</h6>
												<p id="PRESSQpRespcustomerName"></p>
												<h6>Init Channel</h6>
												<p id="PRESSQpresp_init_Channel"></p>
												<h6>Approval Number</h6>
												<p id="PRESSQprespapprovalRefNumber"></p>
                                            </div>

                                          <div id="invoice-customer-details" class="row d-none hide">
                                              <div class="col-sm-6 col-12 text-left">
                                                  <div class="recipient-info my-2">
                                                      <h6>Status</h6>
                                                      <p><b style="font-size: x-large;" id="getstatus"></b></p>
                                                      <h6>Transaction Id</h6>
                                                      <p><b id="transaction_id"></b></p>
                                                      <h6>Message</h6>
                                                      <p id="getmsg"></p>
                                                  </div> 
                                              </div>
                                              <div class="col-sm-6 col-12 text-right">
                                                  <div class="company-info my-2">
                                                      <h6>Amount(&#x20B9;)</h6>
                                                      <p id="getamount"></p>
                                                  </div>
                                              </div>
                                              <div class="col-12">
                                                  <div class="company-info my-0">
                                                      <p style="color: red;margin-bottom: 0px;"><b id="countdown"></b></p>
                                                  </div>
                                              </div>
                                            </div>

                                          	<div id="invoice-company-details" class="row">
                                          		<div class="col-sm-6 col-12 text-center">
		                                            <div class="text-center">
		                                                <img src="{{asset('images/Damskart-Logo.jpg')}}" style="width: 120px;">
		                                            </div>
		                                        </div>

		                                        <div class="col-sm-6 col-12 text-center">
		                                            <div class="text-center">
		                                                <img src="{{asset('images/Be Assured.png')}}" style="width: 120px;">
		                                            </div>
		                                        </div>

		                                        <div class="col-sm-12 col-12 text-center">
		                                            <div class="text-center">
		                                        		<h1>Payment Details</h1>
		                                        	</div>
		                                        </div>

                                          		<div class="col-sm-6 col-12 text-left">
                                                    <div class="recipient-info" id="PRESuccessinputparamdisplay"></div>
                                                </div>

                                                <div class="col-sm-6 col-12 text-right">
													<h6>Biller Name</h6>
													<p id="PRESSQpRespbillername"></p>
													<h6>Recharge Circle</h6>
                                                    <p id="rechargeCircle"></p>
												</div>
                                          	</div>

                                          	<div id="invoice-customer-details" class="row">
                                              	<div class="col-sm-6 col-12 text-left">
                                                  	<div class="company-info">
                                                      <h6>Transaction ID</h6>
                                                      <h5 id="PRESSQpresptxnRefId"></h5>
                                                  	</div>
                                              	</div>

                                              	<div class="col-sm-6 col-12 text-right">
                                                  	<div class="company-info">
                                                      <h6>Txn Date & Time</h6>
                                                      <h5 id="PRESSQpRespdate_time"></h5>
                                                  	</div>
                                              	</div>
                                          	</div>

                                          	<div id="invoice-items-details" class="card bg-transparent shadow-1">
                                              	<div class="row">
                                                  	<div class="table-responsive col-12">
                                                    	<table class="table table-borderless custom-table">
                                                          <tbody>
                                                              <tr>
                                                                  <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                  <td class="text-right"><h5 id="PRESSQpRespbillAmount"></h5></td>
                                                              </tr>
                                                              <tr class="d-none">
                                                                  <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                                  <td class="text-right" id="PRESSQprespCustConvFee"></td>
                                                              </tr>
                                                              <tr>
                                                                  <td class="text-left"><b>Payment Mode</b></td>
                                                                  <td class="text-right" id="PRESSQpResppayment_mode"></td>
                                                              </tr>
                                                              <tr>
                                                                  <th class="text-right">Total Amount (<i class="fa fa-inr"></i>)</th>
                                                                  <td class="text-right"><h5 id="PRESSQpRespbilltotalAmount"></h5></td>
                                                              </tr>
                                                          </tbody>
                                                      	</table>
                                                  	</div>
                                              	</div>
                                          	</div>

                                          	<div id="invoice-customer-details" class="row">
                                              	<div class="col-sm-12 col-12 text-center">
                                                  	<div class="company-info">
                                                  		<h6>Transaction Status</h6>
                                                        <h5 class="prepaid-success" style="color:green" id="PRESSQprespresponseReason"></h5>
                                                  	</div>
                                              	</div>
                                          	</div>
                                      	</div>
                                   	</div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer" style="justify-content: center;">
                             <h5>Thanks for your Business!</h5>
                             <button type="button" class="btn btn-secondary" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="prepaidpayPendingmodel" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                          </div> 
                          <div class="modal-body print-container" id="getCode">
                             <div class="row">
                                <div class="col-md-12">
                                   <div class="content" style="text-align: center; margin-left: 0px;">
                                        <div id="invoice-template" class="card-body card-body-container">
                                          <div id="invoice-company-details" class="row">
                                          		<div class="d-none">
													<h6>Biller Name</h6>
													<p id="PRERespbillername"></p>
													<h6>Biller ID</h6>
													<p id="PREQpRespbillerid"></p>
													<h6>Init Channel</h6>
													<p id="PREQpresp_init_Channel"></p>
	                                            </div>

                                                <div id="invoice-customer-details" class="row d-none hide">
                                                  <div class="col-sm-6 col-12 text-left">
                                                      <div class="recipient-info my-2">
                                                         <h6>Status</h6>
                                                              <p><b style="font-size: x-large;" id="getstatus"></b></p>

                                                          <h6>Transaction Id</h6>
                                                          <p><b id="transaction_id"></b></p>
                                                          <h6>Message</h6>
                                                          <p id="getmsg"></p>
                                                      </div> 
                                                  </div>
                                                  <div class="col-sm-6 col-12 text-right">
                                                      <div class="company-info my-2">
                                                          <h6>Amount(&#x20B9;)</h6>
                                                          <p id="getamount"></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-12">
                                                      <div class="company-info my-0">
                                                          <p style="color: red;margin-bottom: 0px;"><b id="countdown"></b></p>
                                                      </div>
                                                  </div>
                                                </div>

	                                          	<div id="invoice-company-details" class="row">
	                                          		<div class="col-sm-12 col-12 text-center">
			                                            <div class="text-center">
			                                                <img src="{{asset('images/Be Assured.png')}}" style="width: 120px;">
			                                                <h1>Payment Details</h1>
			                                            </div>
			                                        </div>

                                              		<div class="col-sm-6 col-12 text-left">
	                                                      <div class="recipient-info my-2" id="PREinputparamdisplay">
	                                                      </div>
	                                                </div>

	                                                <div class="col-sm-6 col-12 text-left">
														<h6>Biller Name</h6>
														<p id="PRESSQpRespbillername"></p>
													</div>
	                                          	</div>

	                                          	<div id="invoice-customer-details" class="row pt-2 ">
	                                              	<div class="col-sm-6 col-12 text-left">
	                                                  	<div class="company-info my-2">
	                                                      <h6>Transaction ID</h6>
	                                                      <h5 id="PREQpresptxnRefId"></h5>
	                                                      <h6>Recharge Circle</h6>
	                                                      <h5 id="rechargeCircle"></h5>
	                                                  	</div>
	                                              	</div>

	                                              	<div class="col-sm-6 col-12 text-right">
	                                                  	<div class="company-info my-2">
	                                                      <h6>Txn Date & Time</h6>
	                                                      <h5 id="PREQpRespdate_time"></h5>
	                                                  	</div>
	                                              	</div>
	                                          	</div>

	                                          	<div id="invoice-items-details" class="card bg-transparent shadow-1">
	                                              	<div class="row">
	                                                  	<div class="table-responsive col-12">
	                                                    	<table class="table table-borderless custom-table">
	                                                        	<tbody>
                                                                  <tr>
                                                                      <td class="text-left"><b>Bill Amount (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right"><h5 id="PREQpRespbillAmount"></h5></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Customer Convienence Fee (<i class="fa fa-inr"></i>)</b></td>
                                                                      <td class="text-right" id="PRErespCustConvFee"></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td class="text-left"><b>Payment Mode</b></td>
                                                                      <td class="text-right" id="PREResppayment_mode"></td>
                                                                  </tr>
                                                              </tbody>
	                                                      	</table>
	                                                  	</div>
	                                              	</div>
	                                          	</div>

	                                          	<div id="invoice-customer-details" class="row">
	                                              	<div class="col-sm-12 col-12 text-center">
	                                                  	<div class="company-info">
	                                                  		<h6>Transaction Status</h6>
	                                                  		<h5 style="color: red;" id="PREQprespresponseReason"></h5>
	                                                  	</div>
	                                              	</div>
	                                          	</div>
                                            </div>
                                        </div>
                                   	</div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer" style="justify-content: center;">
                             <h5>Thanks for your Business!</h5>
                             <button type="button" class="btn btn-secondary" onClick="window.location.reload();" data-dismiss="modal">Close</button>
                             <button type="button" class="btn btn-primary btn-print" ><i class="feather icon-file-text"></i> Print</button>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="modal fade" id="errormessageModal" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                       <div class="modal-content">
                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                             <h4 class="modal-title" id="myModalLabel">Bharat Bill Payment System (BBPS)</h4>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                             </button>
                          </div>
                          <div class="modal-body"  >
                             <h3 id="amount_error"></h3> 
                          </div>
                       </div>
                    </div>
                 </div>
               </div>
            </div>
      </div>
   </div>
   </section>
   <div class="divWaiting" id="Dv_Loader" style="display: none;">
      <button class="btn btn-primary mb-1 round" type="button" >
      <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
      &nbsp;Loading... 
      </button>
   </div>
   <!-- Column selectors with Export Options and print table -->
</div> 
</div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
</script>
<!-- biller_cat_name -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#biller_cat_name').change(function(){
            var sid = $(this).val();
           
               $('#Dv_Loader').show();
            if(sid){
            $.ajax({
               type:"POST",
               url:"/getBiller/"+sid, //Please see the note at the end of the post**
               success:function(res)
               {  
                  $('#Dv_Loader').hide();
                    if(res)
                    {
                        $("#billerId").empty();
                        $("#operator_list").empty();
                        $("#circle_list").empty();
                        $("#param").empty();
                        $("#q_amount").empty();
                        $("#errormessage").empty();
                        $("#payment_button").empty();
                        $("#customer_mobile_no").empty();
                        $("#operator_list").append('<label>Operator List <span>*</span></label><select class="form-control select2" id="billerId" name="billerId" onchange="billerDetected();" required></select>'); 
                        $("#customer_mobile_no").append(' <input type="hidden" name="customer_mobile" class="form-control" placeholder="Customer Mobile Number" id="customer_mobile" value="<?php echo Auth::user()->mobile; ?>" onkeyup="billDetected()" required>');
                        $("#billerId").append('<option value="">Select Operator</option>');
                        $.each(res,function(key,value){
                            $("#billerId").append('<option value="'+key+'">'+value+'</option>');
                        }); 
                    }
                   $(document).ready(function() {
                     $('.select2').select2();
                     });
               }
            });
            }
        });
    });
</script>
<!-- billDetected -->
<script type="text/javascript">
  function billDetected(){
    var customer_mobile = $('#customer_mobile').val();
    $("input[data-id]").each(function(){
      var inputdata = $(this).data('id');
      var inputparamid = inputdata.replace(/ /g,"_");
      var paramvalue = $("#"+inputparamid).val();
        if (paramvalue.length > 0 && customer_mobile.length > 0) {
          $('#billbutton').removeAttr('disabled');
        } else {
          $('#billbutton').attr('disabled', 'disabled');
        }
    });
  }
</script>
<!-- quickpayDetected -->
<script type="text/javascript">
  function quickpayDetected(){
    var customer_mobile = $('#customer_mobile').val();
    var quickpay_amount = $('#amount').val();
    var q_payment_mode = $("#quick_payment_mode").val();
    var recharge_circle = $("#recharge_circle").val();
    var getbillerId = $("#billerId").val();
    var biller_cat_name = $("#biller_cat_name").val();
    
    $("input[data-id]").each(function(){
      var inputdata = $(this).data('id');
      var inputparamid = inputdata.replace(/ /g,"_");
      var paramvalue = $("#"+inputparamid).val();
        // if ((getbillerId.length > 0 && recharge_circle.length > 0)) {
        // } else {
        // }
        if (paramvalue.length > 0 && customer_mobile.length > 0 && quickpay_amount.length > 0 && q_payment_mode.length > 0) {
          $('#quickpaybutton').removeAttr('disabled');

          if (recharge_circle.length > 0 && getbillerId.length > 0 && recharge_circle.length > 0) {
            $('#prepaidPaybutton').removeAttr('disabled');
            $('#viewPrepaidPlan').removeAttr('disabled');
          }else{
            $('#prepaidPaybutton').attr('disabled', 'disabled');
            $('#viewPrepaidPlan').attr('disabled', 'disabled');
          }
        } else {
          $('#quickpaybutton').attr('disabled', 'disabled');
          $('#prepaidPaybutton').attr('disabled', 'disabled');
        }
    });
  }
</script>
<!-- payDetected -->
<script type="text/javascript">
  function payDetected(){
      var payment_mode = $("#payment_mode").val();
      var bbpscustConvFee = $("#bbpscustConvFee").val();
        if ((payment_mode.length > 0 && bbpscustConvFee.length > 0)) {
          $('#paybutton').removeAttr('disabled');
        } else {
          $('#paybutton').attr('disabled', 'disabled');
        }
  }
</script>
<!-- bbpscustConvFee -->
<script type="text/javascript">
   $(function () {
    $( "#bbpscustConvFee" ).change(function() {
       // var max = parseInt($(this).attr('max'));
       var min = parseInt($(this).attr('min'));
       if ($(this).val() < min || $(this).val() == ''){
           $(this).val(min);
       }       
     }); 
   });
</script>
<!-- totalAmount -->
<script type="text/javascript">
  function totalAmount(){
    var bbpsbillAmount = document.getElementById("bbpsbillAmount").innerText;
    var bbpscustConvFee = $('#bbpscustConvFee').val();
    if (bbpsbillAmount.length < 1 || bbpscustConvFee.length < 1 || bbpscustConvFee == '') {
      var f_amt = bbpsbillAmount;
    }else{
      var f_amt = parseInt(bbpsbillAmount) + parseInt(bbpscustConvFee);
    }
    document.getElementById("bbpsbilltotalAmount").innerHTML = f_amt;
  }
</script>
<!-- billerDetected -->
<script type="text/javascript">
   function billerDetected(){
    var billerId = $('#billerId').val();
     $('#Dv_Loader').show();
     $.ajax({
        type:"POST",
        url:"/getbillerinfo", 
        data : {'billerId':billerId,},
        dataType: "json",
        success:function(data)
        {   
          if(data)
            {
               $('#Dv_Loader').hide();
               $('#errormessage').empty();

               var billername = data.biller.billerName;
               var biller_FetchRequiremet = data.biller.billerFetchRequiremet;
               var biller_SupportBillValidation = data.biller.billerSupportBillValidation;
               var billerpaymentModes = data.biller.billerPaymentModes;
               var Result = data.biller.billerInputParams.paramInfo;
               if (data.responseCode == "000") {
                  var isArray = Array.isArray(Result);
                  if (isArray) {
                     $("#param").empty();
                     $.each(Result,function(key,value){
                        var paramid = value.paramName.replace(/ /g,"_");   
                        if(value.paramName == "Location")
                        {
                            $("#param").append('<div class="form-group d-none"><label>'+value.paramName+'</label><input type="'+value.dataType+'" maxlength="'+value.maxLength+'" minlength="'+value.minLength+'" name="'+value.paramName+'" class="form-control" onkeyup="billDetected()" value="INDIA" data-id="'+value.paramName+'" id="'+paramid+'" placeholder="'+value.paramName+'" required></div>');
                        }
                        else
                        {
                            $("#param").append('<div class="form-group"><label>'+value.paramName+'</label><input type="'+value.dataType+'" maxlength="'+value.maxLength+'" minlength="'+value.minLength+'" name="'+value.paramName+'" class="form-control" onkeyup="billDetected()" data-id="'+value.paramName+'" id="'+paramid+'" placeholder="'+value.paramName+'" required></div>');
                        }
                     });
                  }else{
                     var paramid = Result.paramName.replace(/ /g,"_");   
                     $("#param").empty();
                     $("#param").append('<div class="form-group"><label>'+Result.paramName+'</label><input type="'+Result.dataType+'" maxlength="'+Result.maxLength+'" minlength="'+Result.minLength+'" name="'+Result.paramName+'" class="form-control" onkeyup="billDetected()" data-id="'+Result.paramName+'" id="'+paramid+'" placeholder="'+Result.paramName+'" required></div>');
                  }  

                  $("#param").append('<input type="hidden" class="form-control" id="billername" value="'+billername+'"><input type="hidden" class="form-control" id="billerpaymentModes" value="'+billerpaymentModes+'">');

                  if (biller_FetchRequiremet == "MANDATORY") {
                     $("#amount").empty();
                     $("#circle_list").empty();
                     $("#quick_payment_mode").empty();
                     $("#q_amount").empty();
                     $("#payment_button").empty();
                     $("#payment_button").append('<button type="submit" id="billbutton" onclick="billbutton()"  class="btn btn-primary mr-1 mb-1" disabled="">Fetch Bill</button>');
                  }else if((biller_SupportBillValidation == "MANDATORY" && biller_FetchRequiremet == "NOT_SUPPORTED") || (biller_SupportBillValidation == "OPTIONAL" && biller_FetchRequiremet == "NOT_SUPPORTED")){
                     $("#payment_button").empty();
                     $("#circle_list").empty();
                     $("#quick_payment_mode").empty();

                     var billerCoverage = data.biller.billerCoverage;
                     if(billerCoverage == "IND")
                     {
                        $("#circle_list").append('<div class="row">   <div class="col-md-8"><div class="form-group"><label>Circle List <span>*</span></label><select class="form-control select2" id="recharge_circle" name="recharge_circle" onchange="quickpayDetected()" required><option value="">Select Circle</option> @foreach($bbps_circle as $item) <option value="{{$item->circle_name}}">{{$item->circle_name}}</option> @endforeach</select></div></div><div class="col-md-4"><button type="button" id="viewPrepaidPlan" onclick="viewPrepaidPlan()" class="btn btn-success" style="margin-top: 1.3rem !important;" >View Plans</button></div></div>');
                     }
                     else
                     {
                        $("#circle_list").append('<div class="row">   <div class="col-md-8"><div class="form-group"><label>Circle List <span>*</span></label><select class="form-control select2" id="recharge_circle" name="recharge_circle" onchange="quickpayDetected()" required><option value="">Select Circle</option> <option value="'+billerCoverage+'">'+billerCoverage+'</option> </select></div></div><div class="col-md-4"><button type="button" id="viewPrepaidPlan" onclick="viewPrepaidPlan()" class="btn btn-success" style="margin-top: 1.3rem !important;" >View Plans</button></div></div>');
                     }

                     $("#q_amount").empty(); 
                     $("#q_amount").append('<div class="form-group"> <label>Amount</label> <input type="text" name="amount" class="form-control" placeholder="Amount" id="amount" onkeyup="quickpayDetected()" required=""> </div> <div class="form-group"> <label>Paymet Method</label> <select name="payment_method" class="form-control" id="payment_method" required=""><option value="Payment Gateway">Payment Gateway</option><option value="Cash">Cash</option></select></div><div class="form-group">   <input type="hidden" name="quickpay_ccfamount" class="form-control" placeholder="Customer Convienence Fee" id="quickpay_ccfamount" value="0"></div> <div class="form-group"><input type="hidden" name="quick_payment_mode" value="Cash"  class="form-control"  id="quick_payment_mode" value="0"></div>');

                     // $("#q_amount").append('<div class="form-group"> <label>Amount</label> <input type="text" name="amount" class="form-control" placeholder="Amount" id="amount" onkeyup="quickpayDetected()" required=""></div><div class="form-group">  <input type="hidden" name="quickpay_ccfamount" class="form-control" placeholder="Customer Convienence Fee" id="quickpay_ccfamount" value="0"></div> <div class="form-group"> <label>Select Payment Mode</label>  <select value="" name="quick_payment_mode" onchange="quickpayDetected()" id="quick_payment_mode" class="form-control" required=""></div>');
                     
                     // $("#quick_payment_mode").append('<option value="">Select Payment Mode</option>');
                     // var billerpaymentModes = $('#billerpaymentModes').val().split(",");
                     // $.each(billerpaymentModes,function(i){
                     //      $("#quick_payment_mode").append('<option value="'+billerpaymentModes[i]+'">'+billerpaymentModes[i]+'</option>');
                     // });

                     $("#payment_button").append('<button type="submit" id="quickpaybutton" onclick="quickpaybutton()" class="btn btn-primary mr-1 mb-1" disabled>Pay</button>');

                  }else if(biller_SupportBillValidation == "NOT_SUPPORTED" && biller_FetchRequiremet == "NOT_SUPPORTED"){
                     $("#payment_button").empty();
                     $("#quick_payment_mode").empty();
                     $("#circle_list").empty();
                     // $("#circle_list").append('<div class="form-group"><label>Circle List <span>*</span></label><select class="form-control select2" id="recharge_circle" name="recharge_circle" required><option value="">Select Circle</option> @foreach($bbps_circle as $item) <option value="{{$item->circle_name}}">{{$item->circle_name}}</option> @endforeach</select></div>');

                     var billerCoverage = data.biller.billerCoverage;
                     if(billerCoverage == "IND")
                     {
                     	$("#circle_list").append('<div class="row">   <div class="col-md-8"><div class="form-group"><label>Circle List <span>*</span></label><select class="form-control select2" id="recharge_circle" name="recharge_circle" onchange="quickpayDetected()" required><option value="">Select Circle</option> @foreach($bbps_circle as $item) <option value="{{$item->circle_name}}">{{$item->circle_name}}</option> @endforeach</select></div></div><div class="col-md-4"><button type="button" id="viewPrepaidPlan" onclick="viewPrepaidPlan()" class="btn btn-success" style="margin-top: 1.3rem !important;" >View Plans</button></div></div>');
                     }
                     else
                     {
                     	$("#circle_list").append('<div class="row">   <div class="col-md-8"><div class="form-group"><label>Circle List <span>*</span></label><select class="form-control select2" id="recharge_circle" name="recharge_circle" onchange="quickpayDetected()" required><option value="">Select Circle</option> <option value="'+billerCoverage+'">'+billerCoverage+'</option> </select></div></div><div class="col-md-4"><button type="button" id="viewPrepaidPlan" onclick="viewPrepaidPlan()" class="btn btn-success" style="margin-top: 1.3rem !important;" >View Plans</button></div></div>');
                     }
                     
                     $("#q_amount").empty();
                     $("#q_amount").append('<div class="form-group"> <label>Amount</label> <input type="text" name="amount" class="form-control" placeholder="Amount" id="amount" onkeyup="quickpayDetected()" required=""> </div> <div class="form-group"> <label>Paymet Method</label> <select name="payment_method" class="form-control" id="payment_method" required=""><option value="Payment Gateway">Payment Gateway</option><option value="Cash">Cash</option></select></div> <div class="form-group">  <input type="hidden" name="quickpay_ccfamount" class="form-control" placeholder="Customer Convienence Fee" id="quickpay_ccfamount" value="0"></div> <div class="form-group"><input type="hidden" name="quick_payment_mode" value="Cash"  class="form-control"  id="quick_payment_mode" value="0"></div>');

                     // $("#q_amount").append('<div class="form-group"> <label>Amount</label> <input type="text" name="amount" class="form-control" placeholder="Amount" id="amount" onkeyup="quickpayDetected()" required=""></div><div class="form-group">  <input type="hidden" name="quickpay_ccfamount" class="form-control" placeholder="Customer Convienence Fee" id="quickpay_ccfamount" value="0"></div> <div class="form-group"> <label>Select Payment Mode</label>  <select value="" name="quick_payment_mode" onchange="quickpayDetected()" id="quick_payment_mode" class="form-control" required=""></div>');
                      
                     // $("#quick_payment_mode").append('<option value="">Select Payment Mode</option>');
                     // var billerpaymentModes = $('#billerpaymentModes').val().split(",");
                     // $.each(billerpaymentModes,function(i){
                     //      $("#quick_payment_mode").append('<option value="'+billerpaymentModes[i]+'">'+billerpaymentModes[i]+'</option>');
                     // });

                     $("#payment_button").append('<button type="submit" id="prepaidPaybutton" onclick="prepaidPaybutton()" class="btn btn-primary mr-1 mb-1" disabled>Pay</button>');

                  }else{
                     $("#amount").empty();
                     $("#quick_payment_mode").empty();
                     $("#q_amount").empty();
                     $("#circle_list").empty();
                     $("#payment_button").empty();
                     $("#errormessage").empty();
                     $("#errormessage").append('<p class="btn btn-danger">Something went wrong...</p>');
                  }
               }else{
                  var error = data.errorInfo.error.errorMessage;
                   $("#param").empty();
                  // $("#payment_button").empty();
                  // $("#q_amount").empty();
                  $("#errormessage").empty();
                  $("#errormessage").append('<p class="btn btn-danger">'+error+'</p>');
               }
               
            }
            $(document).ready(function() {
              $('.select2').select2();
            });
        }
     });
   }
</script>
<!-- billbutton -->
<script type="text/javascript">
   function billbutton(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var billerId = $('#billerId').val();
       var billername = $('#billername').val();
       var customer_mobile = $('#customer_mobile').val();
       var biller_cat_name = $('#biller_cat_name').val();
       var billerpaymentModes = $('#billerpaymentModes').val().split(",");

       var paramarr = [];
       $("input[data-id]").each(function(){

             var inputdata = $(this).data('id');
             var inputparamid = inputdata.replace(/ /g,"_");
             var paramvalue = $("#"+inputparamid).val();
            paramarr.push({inputdata : inputdata, paramvalue : paramvalue});
       });
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/getBBPSBillFetch", 
           data : {'biller_cat_name': biller_cat_name, 'billerId':billerId,'paramarr':paramarr,'customer_mobile':customer_mobile,},
           dataType: "json",
           success:function(data)
           {   
                  $('#Dv_Loader').hide();
                  $("#errormessage").empty();

               if (data.bbps_billfetch.responseCode == "000") {
                  $('#bbps_logo').hide();
                  $("#inputparamdisplay").empty();
                  $("#amountOptionsdisplay").empty();
                  $("#additionalInfodisplay").empty();
                  // $("#billerdetaildisplay").empty();

                  if (data.bbps_billfetch.inputParams) {
                     var Result = data.bbps_billfetch.inputParams.input;
                     var isArray = Array.isArray(Result);
                     if (isArray) {
                        $.each(Result,function(key,value){
                           var paramidinput = value.paramName.replace(/ /g,"_");
                           $("#inputparamdisplay").append('<div class="col-sm-3"><h6 >'+value.paramName+'</h6><input style="" type="text" class="form-control input_style" data-pid="'+value.paramName+'" value="'+value.paramValue+'"  id="'+paramidinput+'" placeholder="'+value.paramName+'" disabled></div>');
                        });
                     }else{
                           var paramidinput = Result.paramName.replace(/ /g,"_");
                           $("#inputparamdisplay").append('<div class="col-sm-3"><h6 >'+Result.paramName+'</h6><input type="text" class="form-control input_style" data-pid="'+Result.paramName+'" value="'+Result.paramValue+'"  id="'+paramidinput+'" placeholder="'+Result.paramName+'" disabled></div>');
                     }  
                  }else{
                        $("#inputparamdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style"   placeholder="No Data Available" disabled></div>');
                  }

                  // alert(data.bbps_billfetch.billerResponse.amountOptions);
                  if (data.bbps_billfetch.billerResponse.amountOptions) {
                  var amountOptions = data.bbps_billfetch.billerResponse.amountOptions.option;
                     var isArrayamountOptions = Array.isArray(amountOptions);
                     if (isArrayamountOptions) {
                        $.each(amountOptions,function(key,value){
                           var amountOpt = value.amountName.replace(/ /g,"_");
                           $("#amountOptionsdisplay").append('<div class="form-group "><input type="hidden" class="form-control input_style" data-amount="'+value.amountName+'" value="'+value.amountValue+'"  id="'+amountOpt+'"  placeholder="'+value.amountName+'" disabled></div>');
                        });
                       
                     }else{
                           var amountOpt = value.amountName.replace(/ /g,"_");
                           $("#amountOptionsdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style" data-amount="'+amountOptions.amountName+'" value="'+amountOptions.amountValue+'"  id="'+amountOpt+'"  placeholder="'+amountOptions.amountName+'" disabled></div>');
                     }  
                  }else{
                           $("#amountOptionsdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style"   placeholder="No Data Available" disabled></div>');
                  }

                  if (data.bbps_billfetch.additionalInfo) {
                  var additionalInfo = data.bbps_billfetch.additionalInfo.info;
                     var isArrayadditionalInfo = Array.isArray(additionalInfo);
                     if (isArrayadditionalInfo) {
                        $.each(additionalInfo,function(key,value){
                           var paraminfoName = value.infoName.replace(/ /g,"_");
                           $("#additionalInfodisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style" data-additionalinfo="'+value.infoName+'" value="'+value.infoValue+'"  id="'+paraminfoName+'" placeholder="'+value.infoName+'" disabled></div>');
                        });
                       
                     }else{
                            var paraminfoName = value.infoName.replace(/ /g,"_");
                           $("#additionalInfodisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style" data-additionalinfo="'+additionalInfo.infoName+'" value="'+additionalInfo.infoValue+'"  id="'+paraminfoName+'" placeholder="'+additionalInfo.infoName+'" disabled></div>');
                     }  
                  }else{
                           $("#additionalInfodisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style"   placeholder="No Data Available" disabled></div>');
                  }

                  if (data.bbps_billfetch.billerResponse.amountOptions) {
                  var amountOptions = data.bbps_billfetch.billerResponse.amountOptions.option;
                     var isArrayamountOptions = Array.isArray(amountOptions);
                     if (isArrayamountOptions) {
                        $.each(amountOptions,function(key,value){
                            var amountOpt = value.amountName.replace(/ /g,"_");
                            $("#bbpspaymentInformation").append('<tr><td class="text-left">'+value.amountName+' (<i class="fa fa-inr"></i>)</td><td class="text-right">'+value.amountValue+'</td></tr>');
                        });
                       
                     }else{
                        var amountOpt = value.amountName.replace(/ /g,"_");
                        $("#bbpspaymentInformation").append('</tr><tr><td>'+amountOptions.amountName+' (<i class="fa fa-inr"></i>)</td><td>'+amountOptions.amountValue+'</td>');
                     }  

                    $("#bbpspaymentInformation").append('<tr><td class="text-left"><b>Total Amount (<i class="fa fa-inr"></i>)</b></td><td class="text-right"><h5 id="bbpsbilltotalAmount"></h5></td></tr>');
                  }else{
                    $("#bbpspaymentInformation").append('<tr><td class="text-left"><b>Total Amount (<i class="fa fa-inr"></i>)</b></td><td class="text-right"><h5 id="bbpsbilltotalAmount"></h5></td></tr>');
                    //$("#bbpspaymentInformation").append('');
                  }


                  

                  // var billerdetail = data.billerResponse;
                  // $.each(billerdetail,function(key,value){
                  //    var billerdetailparamid = key.replace(/ /g,"_");
                  //    $("#billerdetaildisplay").append('<div class="form-group"><label >'+key+'</label><input type="text" class="form-control" data-billerde="'+key+'" value="'+value+'"  id="'+billerdetailparamid+'" placeholder="'+key+'" disabled></div>');
                  // });
                  var BillerResponse = data.bbps_billfetch.billerResponse; 
                  var billAmount = BillerResponse.billAmount;
                  // var billDate = moment(BillerResponse.billDate).format('DD-MM-YYYY');
                  var billDate = BillerResponse.billDate;
                  var billNumber = BillerResponse.billNumber;
                  var billPeriod = BillerResponse.billPeriod;
                  var customerName = BillerResponse.customerName;
                  var dueDate = BillerResponse.dueDate;
                  // var dueDate = moment(BillerResponse.dueDate).format('DD-MM-YYYY');
                  // alert(dueDate);
                  var requestId = data.requestId;

                  $('#RequestId').val(requestId);
                  $('#bbpsbillAmount').html(billAmount/100);
                  $('#bbpsbilltotalAmount').html(billAmount/100);
                  $('#bbpsbillDate').html(billDate);
                  $('#bbpsbillNumber').html(billNumber);
                  $('#bbpsbillPeriod').html(billPeriod);
                  $('#bbpscustomerName').html(customerName);
                  $('#bbpsdueDate').html(dueDate);
                  $('#bbpsbillername').html(billername);
                  $('#bbpscustomermobile').html(customer_mobile);

                   // $("#payment_mode").empty();
                   // $("#payment_mode").append('<option value="">Select Payment Mode</option>');
                   // $.each(billerpaymentModes,function(i){
                   //    var str = billerpaymentModes[i];
                   //    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                   //      return letter.toUpperCase();
                   //    });
                   //    $("#payment_mode").append('<option value="'+str+'">'+billerpaymentModes[i]+'</option>');
                   // });
                  $('#getCodeModal').modal({backdrop: 'static', keyboard: false});
               }else{
                  var error = data.bbps_billfetch.errorInfo.error;
                   //$("#param").empty();
                  //$("#payment_button").empty();
                  //$("#q_amount").empty();
                  // $("#errormessage").empty();

                   var isArray = Array.isArray(error);
                   if (isArray) {
                      $("#errormessage").empty();
                      $.each(error,function(key,value){
                       $("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
                      });
                   }else{
                      var error =data.bbps_billfetch.errorInfo.error.errorMessage;
                      $("#errormessage").empty();
                       $("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');

                       if(error == "Incorrect / invalid customer account")
                       {
                            billbutton();
                       }
                   }
               }
               
           }
        });
        // });
      // });
   }
</script>
<!-- quickpaybutton -->
<script type="text/javascript">
   function quickpaybutton() {
         var billerId = $('#billerId').val();
         var customer_mobile = $('#customer_mobile').val();
         var customer_mobile_recharge = $('#Mobile_Number').val();
         var quick_payment_mode = $('#quick_payment_mode').val();
         var quickpay_ccfamount = $('#quickpay_ccfamount').val();
         var billerpaymentModes = $('#payment_mode').val();
         var billername = $('#billername').val();
         var amount = $('#amount').val();
         var biller_cat_name = $('#biller_cat_name').val();
        var paramarr = [];
        $("input[data-id]").each(function(){
        var inputdata = $(this).data('id');
        var inputparamid = inputdata.replace(/ /g,"_");
        var paramvalue = $("#"+inputparamid).val();
        paramarr.push({inputdata : inputdata, paramvalue : paramvalue});
        });

        if(biller_cat_name == "Mobile Prepaid" || biller_cat_name == "DTH")
        {
        }
        else
        {
                $('#Dv_Loader').show();
                if(amount != "" && amount < 50)
                {
                    Swal.fire({
                        title: 'Amount should not be less than Rs. 50 for mobile Postpaid',
                        type: 'warning',
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'OK',
                        cancelButtonClass: 'btn btn-danger ml-1',
                        buttonsStyling: false,
                    });
                    $('#Dv_Loader').hide();
                    return false;
                }
                else
                {
                    $('#Dv_Loader').hide();
                }
            }

           Swal.fire({
                title: 'Do you want to continue?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                buttonsStyling: false,
              }).then((data) => {
                if (data.value) {
                  $('#Dv_Loader').show();
                           $('#plan').modal('hide');
                     $.ajax({
                      type:"POST",
                      url:"/bbpsquickPay", 
                      data : {'billername':billername,'billerId':billerId,'quick_payment_mode':quick_payment_mode,'quickpay_ccfamount':quickpay_ccfamount,'amount':amount,'customer_mobile':customer_mobile,'paramarr':paramarr, 'biller_cat_name' : biller_cat_name},
                      dataType: "json",
                      success:function(data)
                      {   
                         $('#Dv_Loader').hide();
                               var Result = data.bbps_quickbillpay;
                          if ((Result.resText)) {
                            // alert(Result.resText);
                            // var error = Result.resText; 
                            $('#amount_error').html(Result.resText);
                            $('#errormessageModal').modal({backdrop: 'static', keyboard: false});
                          }else{
                            if (data.error_type != "validate_err") {
                                var txn_date_time = data.txn_date_time;
                                var resp_init_Channel = data.resp_init_Channel;
                                var respresponseCode = Result.responseCode;
                               if (respresponseCode == "000" ) {

                                  $('#bbps_logo').hide();
                                  var respbillername = billername;
                                  var respbillerId = billerId;
                                  var resptxnRefId = Result.txnRefId;
                                  var respcustomerName = Result.RespCustomerName;
                                  var respcustomermobile = customer_mobile_recharge;//customer_mobile;
                                  var respbillAmount = Result.RespAmount;
                                  var respCustConvFee = Result.CustConvFee;
                                  var respbilltotalAmount = Result.RespAmount;
                                  var respdate_time = txn_date_time;
                                  var respinit_Channel = resp_init_Channel;
                                  var resppayment_mode = quick_payment_mode;
                                  var respresponseReason = Result.responseReason;
                                  var respapprovalRefNumber = Result.approvalRefNumber;

                                  // var respbillDate = Result.RespBillDate;
                                  // var respbillPeriod = Result.RespBillPeriod;
                                  // var respbillNumber = Result.RespBillNumber;
                                  // var respdueDate = Result.RespDueDate;
                                  // $('#RespbillDate').html(respbillDate);
                                  // $('#RespbillPeriod').html(respbillPeriod);
                                  // $('#RespbillNumber').html(respbillNumber);
                                  // $('#RespdueDate').html(respdueDate);

                                  $('#QpRespbillername').html(respbillername);
                                  $('#QpRespbillerid').html(respbillerId);
                                  $('#QpRespcustomerName').html(respcustomerName);
                                  $('#QpresptxnRefId').html(resptxnRefId);
                                  $('#QpRespcustomermobile').html(respcustomermobile);
                                  $('#QpResppayment_mode').html(resppayment_mode);
                                  $('#QprespapprovalRefNumber').html(respapprovalRefNumber);
                                  $('#QprespresponseReason').html(respresponseReason);
                                  $('#QpRespbillAmount').html(respbillAmount / 100);
                                  $('#QprespCustConvFee').html(respCustConvFee);
                                  $('#QpRespdate_time').html(respdate_time);
                                  $('#Qpresp_init_Channel').html(respinit_Channel);
                                  var a = (respbillAmount / 100);
                                  var b = respCustConvFee;
                                  var billtotalAmount = parseInt(a, 10) + parseInt(b, 10);
                                  $('#QpRespbilltotalAmount').html(billtotalAmount);
                                   
                                  $('#quickpaymentmodel').modal({backdrop: 'static', keyboard: false});
                               }else{
                                  var error = Result.errorInfo.error;
                                  // alert("1");
                                  var isArray = Array.isArray(error);
                                  if (isArray) {
                                    $("#errormessage").empty();
                                    $.each(error,function(key,value){
                                     $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+value.errorMessage+'</p>');
                                    });
                                  }else{
                                    var error =Result.errorInfo.error.errorMessage;
                                    $("#errormessage").empty();
                                     $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+error+'</p>');
                                  }
                               }
                            }else{
                               var error =data.bbps_quickbillpay.complianceReason;
                               $("#errormessage").empty();
                               $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+error+'</p>');
                            }
                        }
                         
                      }
                   });
                } else if (data.dismiss === Swal.DismissReason.cancel) {
                  Swal.fire({
                      title: 'Cancelled',
                      type: 'error',
                      confirmButtonClass: 'btn btn-success',
                    })
                }
              })
    }
</script>
<script type="text/javascript">
  function appendRechargeAmount(){
    var val_amt = $("input[type='radio'][name='recharge_amount']:checked").val();
    $('#amount').val(val_amt);
    $('#rechargePlanDetail').modal('hide');
  }
</script>
<!-- viewPrepaidPlan -->
<script type="text/javascript">
	function onlyUnique(value, index, self) {
  		return self.indexOf(value) === index;
	}

   function viewPrepaidPlan(){
   // $(document).ready(function(){
    // $('#billbutton').click(function(){
       var billerId = $('#billerId').val();
       var circle = $('#recharge_circle').val();
        $('#Dv_Loader').show();
        $.ajax({
           type:"POST",
           url:"/bbpsfetchplan", 
           data : {'billerId':billerId,'circle':circle,},
           dataType: "json",
           success:function(data)
           {   
				$('#Dv_Loader').hide();
				$("#errormessage").empty();

				if (data.responseCode == "000") {
					$('#bbps_logo').hide();

					var RechargePlanResponse = data; 

					var plan_response = data.rechargePlan.rechargePlansDetails;
					$("#recharge_plan_response").empty();
					var j = 1;

					var plan_tab = '<ul class="nav nav-tabs" id="myTab" role="tablist">';

					if(billerId == "BILAVAIRTEL001" || billerId == "BILAVVI0000001") //AIRTEL
					{
						plan_tab+='<li class="nav-item"><a class="nav-link active" id="topup-tab" data-toggle="tab" href="#topup-plan" role="tab" aria-controls="topup-plan" aria-selected="false">Topup Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="unlimited-tab" data-toggle="tab" href="#unlimited-plan" role="tab" aria-controls="unlimited-plan" aria-selected="false">Unlimited Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="international-tab" data-toggle="tab" href="#international-plan" role="tab" aria-controls="international-plan" aria-selected="false">International Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="other-tab" data-toggle="tab" href="#other-plan" role="tab" aria-controls="other-plan" aria-selected="false">Other Plan</a></li>';
					}
					else if(billerId == "BILAVBSNL00001" || billerId == "BILAVMTNL00001" || billerId == "BILAVMTNL00002") // BSNL
					{
						plan_tab+='<li class="nav-item"><a class="nav-link active" id="topup-tab" data-toggle="tab" href="#topup-plan" role="tab" aria-controls="topup-plan" aria-selected="false">Topup Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="unlimited-tab" data-toggle="tab" href="#unlimited-plan" role="tab" aria-controls="unlimited-plan" aria-selected="false">Unlimited Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="international-tab" data-toggle="tab" href="#international-plan" role="tab" aria-controls="international-plan" aria-selected="false">International Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="full-talktime-tab" data-toggle="tab" href="#full-talktime-plan" role="tab" aria-controls="full-talktime-plan" aria-selected="false">Full Talktime Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="special-recharge-tab" data-toggle="tab" href="#special-recharge-plan" role="tab" aria-controls="special-recharge-plan" aria-selected="false">Special Recharge Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="other-tab" data-toggle="tab" href="#other-plan" role="tab" aria-controls="other-plan" aria-selected="false">Other Plan</a></li>';

					}
					else if(billerId == "BILAVJIO000001") // JIO
					{
						plan_tab+='<li class="nav-item"><a class="nav-link active" id="topup-tab" data-toggle="tab" href="#topup-plan" role="tab" aria-controls="topup-plan" aria-selected="false">Topup Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="prime-tab" data-toggle="tab" href="#prime-plan" role="tab" aria-controls="prime-plan" aria-selected="false">Prime Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="non-prime-tab" data-toggle="tab" href="#non-prime-plan" role="tab" aria-controls="non-prime-plan" aria-selected="false">Non Prime Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="jio-tab" data-toggle="tab" href="#jio-phone" role="tab" aria-controls="jio-phone" aria-selected="false">Jio Phone all in one</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="international-tab" data-toggle="tab" href="#international-plan" role="tab" aria-controls="international-plan" aria-selected="false">International Plan</a></li>';
						plan_tab+='<li class="nav-item"><a class="nav-link" id="other-tab" data-toggle="tab" href="#other-plan" role="tab" aria-controls="other-plan" aria-selected="false">Other Plan</a></li>';
					}

					plan_tab+='</ul><div class="tab-content" id="myTabContent">';
					

					if(billerId == "BILAVAIRTEL001" || billerId == "BILAVVI0000001") //AIRTEL
                    {
                    	plan_tab += '<div class="tab-pane fade show active" id="topup-plan" role="tabpanel" aria-labelledby="topup-tab">topup</div><div class="tab-pane fade" id="unlimited-plan" role="tabpanel" aria-labelledby="unlimited-tab">unlimited</div><div class="tab-pane fade" id="international-plan" role="tabpanel" aria-labelledby="international-tab">international</div><div class="tab-pane fade" id="other-plan" role="tabpanel" aria-labelledby="other-tab">other</div>';
                    }
                    else if(billerId == "BILAVBSNL00001" || billerId == "BILAVMTNL00001" || billerId == "BILAVMTNL00002") //AIRTEL
                    {
                    	plan_tab += '<div class="tab-pane fade show active" id="topup-plan" role="tabpanel" aria-labelledby="topup-tab">topup</div><div class="tab-pane fade" id="unlimited-plan" role="tabpanel" aria-labelledby="unlimited-tab">unlimited</div><div class="tab-pane fade" id="international-plan" role="tabpanel" aria-labelledby="international-tab">international</div><div class="tab-pane fade" id="full-talktime-plan" role="tabpanel" aria-labelledby="full-talktime-tab">full-talktime</div><div class="tab-pane fade" id="special-recharge-plan" role="tabpanel" aria-labelledby="special-recharge-tab">special-recharge</div><div class="tab-pane fade" id="other-plan" role="tabpanel" aria-labelledby="other-tab">other</div>';
                    }
                    else if(billerId == "BILAVJIO000001") // JIO
                    {
                    	plan_tab += '<div class="tab-pane fade show active" id="topup-plan" role="tabpanel" aria-labelledby="topup-tab">topup</div><div class="tab-pane fade" id="prime-plan" role="tabpanel" aria-labelledby="prime-tab">prime</div><div class="tab-pane fade" id="non-prime-plan" role="tabpanel" aria-labelledby="non-prime-tab">non-prime</div><div class="tab-pane fade" id="jio-phone" role="tabpanel" aria-labelledby="jio-tab">jiophone</div><div class="tab-pane fade" id="international-plan" role="tabpanel" aria-labelledby="international-tab">international</div><div class="tab-pane fade" id="other-plan" role="tabpanel" aria-labelledby="other-tab">other</div>';
                    }

                    plan_tab += '</div>';
                    $("#recharge_plan_response").append(plan_tab);

                    if(billerId == "BILAVAIRTEL001" || billerId == "BILAVVI0000001") //AIRTEL
                    {
                    	var topup_response_html = "<ul class='list-unstyled'>";
                    	var unlimited_response_html = "<ul class='list-unstyled'>";
                    	var international_plan_response_html = "<ul class='list-unstyled'>";
                    	var other_plan_response_html = "<ul class='list-unstyled'>";

                    	$.each(plan_response,function(key,value){
                    		if(value.planName == "Combo Pack" || value.planName == "First Recharge" || value.planName == "Internet Pack"  || value.planName == "SMS Pack")
                    		{
	                        	other_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "International Roaming" || value.planName == "ISD Pack")
                    		{
	                        	international_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Unlimited Packs" || value.planName == "Unlimited Pack")
                    		{
	                        	unlimited_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else
	                        {
	                        	topup_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }

	                        j++;
	                    });
	                    topup_response_html += "</ul>";
	                    unlimited_response_html += "</ul>";
	                    international_plan_response_html += "</ul>";
	                    other_plan_response_html += "</ul>";

	                    $("#topup-plan").html(topup_response_html);
	                    $("#unlimited-plan").html(unlimited_response_html);
	                    $("#international-plan").html(international_plan_response_html);
	                    $("#other-plan").html(other_plan_response_html);
                    }
                    else if(billerId == "BILAVBSNL00001" || billerId == "BILAVMTNL00001" || billerId == "BILAVMTNL00002") //AIRTEL
                    {
                    	var topup_response_html = "<ul class='list-unstyled'>";
                    	var unlimited_response_html = "<ul class='list-unstyled'>";
                    	var international_plan_response_html = "<ul class='list-unstyled'>";
                    	var full_talktime_plan_response_html = "<ul class='list-unstyled'>";
                    	var special_recharge_plan_response_html = "<ul class='list-unstyled'>";
                    	var other_plan_response_html = "<ul class='list-unstyled'>";

                    	$.each(plan_response,function(key,value){
                    		if(value.planName == "Combo Pack" || value.planName == "First Recharge" || value.planName == "Internet Pack"  || value.planName == "SMS Pack" || value.planName == "Validity Extention" || value.planName == "Data Pack" || value.planName == "Night Pack" || value.planName == "STD Pack" || value.planName == "Validity Extension")
                    		{
	                        	other_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Full Talktime")
                    		{
	                        	full_talktime_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Special Recharge")
                    		{
	                        	special_recharge_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Unlimited Pack")
                    		{
	                        	unlimited_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        

	                        else if(value.planName == "International Roaming" || value.planName == "ISD Plan")
                    		{
	                        	international_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else
	                        {
	                        	topup_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }

	                        j++;
	                    });
	                    topup_response_html += "</ul>";
	                    unlimited_response_html += "</ul>";
	                    international_plan_response_html += "</ul>";
	                    full_talktime_plan_response_html += "</ul>";
	                    special_recharge_plan_response_html += "</ul>";
	                    other_plan_response_html += "</ul>";

	                    $("#topup-plan").html(topup_response_html);
	                    $("#unlimited-plan").html(unlimited_response_html);
	                    $("#international-plan").html(international_plan_response_html);
	                    $("#full-talktime-plan").html(full_talktime_plan_response_html);
	                    $("#special-recharge-plan").html(special_recharge_plan_response_html);
	                    $("#other-plan").html(other_plan_response_html);
                    }
                    else if(billerId == "BILAVJIO000001") // JIO
                    {
                    	var topup_response_html = "<ul class='list-unstyled'>";
                    	var prime_response_html = "<ul class='list-unstyled'>";
                    	var non_prime_response_html = "<ul class='list-unstyled'>";
                    	var jio_phone_response_html = "<ul class='list-unstyled'>";
                    	var international_response_html = "<ul class='list-unstyled'>";
                    	var other_plan_response_html = "<ul class='list-unstyled'>";

                    	$.each(plan_response,function(key,value){
                    		if(value.planName == "Topup Plan")
                    		{
	                        	topup_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "International Roaming")
                    		{
	                        	international_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Jio Phone All in One Plan")
                    		{
	                        	jio_phone_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Non Prime 1 GB plan" || value.planName == "Non Prime 1.5 GB Plan" || value.planName == "Non Prime 2 GB Plan" || value.planName == "Non Prime 3 GB Plan" || value.planName == "Non Prime Disney plus plans")
                    		{
	                        	non_prime_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else if(value.planName == "Prime 1 GB Plan" || value.planName == "Prime 1.5 GB Plan" || value.planName == "Prime 2 GB Plan" || value.planName == "Prime 3 GB Plan" || value.planName == "Prime Disney plus plans" || value.planName == "Prime ISD Plan" || value.planName == "Prime Long term Plans")
                    		{
	                        	prime_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }
	                        else
	                        {
	                        	other_plan_response_html += '<li class="todo-item pt-1 mr-1" style="border-bottom: 1px solid #7367F0;" data-toggle="modal" data-target="#editTaskModal"><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="title-wrapper d-flex mr-1">  <h5 class="todo-title mt-50 mx-50">'+value.planName+'</h5></div>      </div><div class="float-right todo-item-action d-flex"><div class="vs-radio-con"><input type="radio" class="mr-1" style="margin: auto;" id="recharge_amount'+j+'" name="recharge_amount" onclick="appendRechargeAmount()" value="'+value.amount+'"><span class="vs-radio"><span class="vs-radio--border"></span><span class="vs-radio--circle"></span></span></div><button type="button" class="btn btn-warning" style="padding:11px; border-radius:20px;"><i class="fa fa-inr"></i>&nbsp; '+value.amount+'</button></div></div><div class="todo-title-wrapper d-flex justify-content-between mb-50"><div class="todo-title-area d-flex align-items-center"><div class="chip-wrapper mr-1"><div class="chip mb-0"><div class="chip-body"><span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-success bullet-xs"></span>&nbsp; Validity:<b>'+value.validity+'</b></span> </div> </div> </div> <div class="chip-wrapper"> <div class="chip mb-0"> <div class="chip-body"> <span class="chip-text" style="font-size: 12px;" data-value="Frontend"><span class=""><span class="bullet bullet-primary bullet-xs"></span>&nbsp; Talktime: <b>'+value.talktime+'</b></span></div></div></div></div></div><p class="todo-desc mb-1" style="text-align: -webkit-auto;">'+value.description+'</p></li>';
	                        }

	                        j++;
	                    });

	                    topup_response_html += "</ul>";
	                    prime_response_html += "</ul>";
	                    non_prime_response_html += "</ul>";
	                    jio_phone_response_html += "</ul>";
	                    international_response_html += "</ul>";
	                    other_plan_response_html += "</ul>";

	                    $("#topup-plan").html(topup_response_html);
	                    $("#prime-plan").html(prime_response_html);
	                    $("#non-prime-plan").html(non_prime_response_html);
	                    $("#jio-phone").html(jio_phone_response_html);
	                    $("#international-plan").html(international_response_html);
	                    $("#other-plan").html(other_plan_response_html);
                    }


	                $('#rechargePlanDetail').modal({backdrop: 'static', keyboard: false});
               	}else{
					var error = data.errorInfo.error;
					// $("#param").empty();
					// $("#payment_button").empty();
					// $("#rechargePlanDetail").hide();
					// $("#q_amount").empty();
					// $("#prepaidPaybutton").empty();
					// $("#errormessage").empty();

					var isArray = Array.isArray(error);
					if (isArray) {
						$("#errormessage").empty();
						$.each(error,function(key,value){
						$("#errormessage").append('<p class="btn btn-danger" >'+value.errorMessage+'</p>');
						});
					}else{
						var error =data.errorInfo.error.errorMessage;
						$("#errormessage").empty();
						$("#errormessage").append('<p class="btn btn-danger" >'+error+'</p>');
					}
				}
			}
        });
        // });
      // });
   }
</script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<!-- prepaidPaybutton -->
<script type="text/javascript">
   function prepaidPaybutton() {
        var SITEURL = '{{URL::to('')}}';

        var billerId = $('#billerId').val();
        var customer_mobile = $('#customer_mobile').val();
        var quick_payment_mode = $('#quick_payment_mode').val();
        var quickpay_ccfamount = $('#quickpay_ccfamount').val();
        var billerpaymentModes = $('#payment_mode').val();
        var billername = $('#billername').val();
        var amount = $('#amount').val();
        var recharge_circle = $("#recharge_circle").val();
        var payment_method = $("#payment_method").val();
        var paramarr = [];
        $("input[data-id]").each(function(){
             var inputdata = $(this).data('id');
             var inputparamid = inputdata.replace(/ /g,"_");
             var paramvalue = $("#"+inputparamid).val();
             paramarr.push({inputdata : inputdata, paramvalue : paramvalue});
        });
        Swal.fire({
            title: 'Do you want to continue?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Continue',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then((data) => {
            if (data.value) {
                $('#Dv_Loader').show();

                if (amount >= 5000) {
                    Swal.fire({
                        title: 'Amount should not be equal to or more than five thousand',
                        type: 'warning',
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'OK',
                        cancelButtonClass: 'btn btn-danger ml-1',
                        buttonsStyling: false,
                    });
                    $('#Dv_Loader').hide();
                    return false;
                }

                if(payment_method == "Cash")
                {
                    $('#Dv_Loader').show();
                    $('#plan').modal('hide');
                    $.ajax({
                        type:"POST",
                        url:"/bbpsprepaidPay", 
                        data : {'billername':billername,'billerId':billerId,'quick_payment_mode':quick_payment_mode,'quickpay_ccfamount':quickpay_ccfamount,'amount':amount,'customer_mobile':customer_mobile,'paramarr':paramarr, 'recharge_circle': recharge_circle, 'payment_method': payment_method},
                        dataType: "json",
                        success:function(data)
                        {
                            $('#Dv_Loader').hide();
                            var Result = data.bbps_prepaidpay;
                            if ((Result.resText)) {
                                // alert(Result.resText);
                                // var error = Result.resText; 
                                $('#amount_error').html(Result.resText);
                                $('#errormessageModal').modal({backdrop: 'static', keyboard: false}); 
                            }else{
                                var txn_date_time = data.txn_date_time;
                                var resp_init_Channel = data.resp_init_Channel;
                                var respresponseCode = Result.responseCode;

                                if (respresponseCode == "000" ) {

                                    $('#bbps_logo').hide();
                                    $("#PRESuccessinputparamdisplay").empty();
                                    $("#errormessage").empty();

                                    if (data.bbps_prepaidpay.inputParams) {
                                        var InputResult = data.bbps_prepaidpay.inputParams.input;
                                        var isArray = Array.isArray(InputResult);
                                        if (isArray) {
                                            $.each(InputResult,function(key,value){
                                                var paramidinput = value.inputdata.replace(/ /g,"_");
                                                var inputDataHeading = value.inputdata;
                                                if(value.inputdata == "Mobile Number")
                                                {
                                                    inputDataHeading = "Customer Mobile Number";
                                                }

                                                $("#PRESuccessinputparamdisplay").append('<h6 >'+inputDataHeading+'</h6><input style="" type="text" class="form-control input_style" data-pid="'+value.inputdata+'" value="'+value.paramvalue+'"  id="'+paramidinput+'" placeholder="'+value.inputdata+'" disabled>');
                                            });
                                        }else{
                                            var paramidinput = InputResult.inputdata.replace(/ /g,"_");
                                            $("#PRESuccessinputparamdisplay").append('<h6 >'+InputResult.inputdata+'</h6><input type="text" class="form-control input_style" data-pid="'+InputResult.inputdata+'" value="'+InputResult.paramvalue+'"  id="'+paramidinput+'" placeholder="'+InputResult.inputdata+'" disabled>');
                                        }  
                                    } else {
                                        $("#PRESuccessinputparamdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style" placeholder="No Data Available" disabled></div>');
                                    } // if inputparams

                                    // Show response in respective div accordingly
                                    var respbillername = billername;
                                    var respbillerId = billerId;
                                    var resptxnRefId = Result.txnRefId;
                                    var respcustomerName = Result.RespCustomerName;
                                    var respcustomermobile = customer_mobile;
                                    var respbillAmount = Result.RespAmount;
                                    // alert(respbillAmount);
                                    var respCustConvFee = Result.CustConvFee;
                                    // alert(respCustConvFee);
                                    var respbilltotalAmount = Result.RespAmount;
                                    var respdate_time = txn_date_time;
                                    var respinit_Channel = resp_init_Channel;
                                    var resppayment_mode = quick_payment_mode;
                                    var respresponseReason = Result.responseReason;
                                    var respapprovalRefNumber = Result.approvalRefNumber;
                              
                                    $('#PRESSQpRespbillername').html(respbillername);
                                    $('#PRESSQpRespbillerid').html(respbillerId);
                                    $('#PRESSQpRespcustomerName').html(respcustomerName);
                                    $('#PRESSQpresptxnRefId').html(resptxnRefId);
                                    $('#PRESSQpRespcustomermobile').html(respcustomermobile);
                                    $('#PRESSQpResppayment_mode').html(resppayment_mode);
                                    $('#PRESSQprespapprovalRefNumber').html(respapprovalRefNumber);
                                    $('#PRESSQprespresponseReason').html(respresponseReason);
                                    $('#PRESSQpRespbillAmount').html(respbillAmount / 100);
                                    $('#PRESSQprespCustConvFee').html(respCustConvFee);
                                    $('#PRESSQpRespdate_time').html(respdate_time);
                                    $('#PRESSQpresp_init_Channel').html(respinit_Channel);

                                    $('#rechargeCircle').html(data.recharge_circle);

                                    var a = (respbillAmount / 100);
                                    var b = respCustConvFee;
                                    var billtotalAmount = parseInt(a, 10) + parseInt(b, 10);
                                    $('#PRESSQpRespbilltotalAmount').html(billtotalAmount);
                                    $('#prepaidpaySuccessmodel').modal({backdrop: 'static', keyboard: false});
                                }
                                else if(respresponseCode == "900"){
                                    $('#bbps_logo').hide();
                                    $("#PREinputparamdisplay").empty();
                                    $("#errormessage").empty();

                                    var respbillername = data.billername;
                                    var respbillerId = billerId;
                                    var resptxnRefId = Result.txnRefId;
                                    var respbillAmount = data.amount;
                                    var respCustConvFee = data.quickpay_ccfamount;
                                    var respdate_time = txn_date_time;
                                    var respinit_Channel = resp_init_Channel;
                                    var resppayment_mode = data.quick_payment_mode;
                                    var respresponseReason = Result.responseReason;

                                    $('#PRERespbillername').html(respbillername);
                                    $('#PREQpRespbillerid').html(respbillerId);
                                    $('#PREQpresptxnRefId').html(resptxnRefId);
                                    $('#PREQprespresponseReason').html(respresponseReason);
                                    $('#PREQpRespbillAmount').html(respbillAmount);
                                    $('#PREQpRespdate_time').html(respdate_time);
                                    $('#PREQpresp_init_Channel').html(respinit_Channel);
                                    $('#PRErespCustConvFee').html(respCustConvFee);
                                    $('#PREResppayment_mode').html(resppayment_mode);
                             
                                    if (data.bbps_prepaidpay.inputParams) {
                                        var InputResult = data.bbps_prepaidpay.inputParams.input;
                                        var isArray = Array.isArray(InputResult);
                                        if (isArray) {
                                        $.each(InputResult,function(key,value){
                                        // alert(value.inputdata);
                                        var paramidinput = value.inputdata.replace(/ /g,"_");
                                        $("#PREinputparamdisplay").append('<h6 >'+value.inputdata+'</h6><input style="" type="text" class="form-control input_style" data-pid="'+value.inputdata+'" value="'+value.paramvalue+'"  id="'+paramidinput+'" placeholder="'+value.inputdata+'" disabled>');
                                        });
                                        }else{
                                        var paramidinput = InputResult.inputdata.replace(/ /g,"_");
                                        $("#PREinputparamdisplay").append('<h6 >'+InputResult.inputdata+'</h6><input type="text" class="form-control input_style" data-pid="'+InputResult.inputdata+'" value="'+InputResult.paramvalue+'"  id="'+paramidinput+'" placeholder="'+InputResult.inputdata+'" disabled>');
                                        }
                                    } else {
                                        $("#PREinputparamdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style"   placeholder="No Data Available" disabled></div>');
                                    }
                                    $('#prepaidpayPendingmodel').modal({backdrop: 'static', keyboard: false});
                                } else {
                                    var error = Result.errorInfo.error;
                                    // alert("1");
                                    var isArray = Array.isArray(error);
                                    if (isArray) {
                                    $("#errormessage").empty();
                                    $.each(error,function(key,value){
                                    $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+value.errorMessage+'</p>');
                                    });
                                    }else{
                                    var error =Result.errorInfo.error.errorMessage;
                                    $("#errormessage").empty();
                                    $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+error+'</p>');
                                    }
                                }
                            }
                        }
                   });
                } // If cash
                else
                {
                    $.ajax({
                       type:"get",
                       url:"/getorderidPrepaid", 
                       data : {'totalAmount':amount},
                       dataType: "json",
                       success:function(data)
                       {  
                            $('#Dv_Loader').hide();
                            var order_id = data.orderId;
                            var ramount = data.amount;

                            var options = {
                                "key": "{{ env('RAZOR_KEY') }}",
                                "amount": ramount, // 2000 paise = INR 20
                                "name": "DAMSKART",
                                "description": "Prepaid Recharge",
                                "image": "https://damskart.com/public//images/Damskart Logo.png",
                                "prefill": {
                                    "contact": '<?php echo $user->mobile;?>',
                                    "email":   '<?php echo $user->email;?>',
                                },
                                "theme": {
                                    "color": "#528FF0"
                                },
                                "order_id": order_id,
                                "handler": function (response){
                                    $('#Dv_Loader').show();
                                $.ajax({
                                     url: SITEURL + '/rozerpaymentPrepaid',
                                     type: 'post',
                                     dataType: 'json',
                                     data: {
                                       razorpay_payment_id: response.razorpay_payment_id , 
                                       totalAmount : amount,
                                       transaction_type : 'prepaid' 
                                     }, 
                                     success: function (msg) {
                                        var Result = msg;
                                        var transaction_id = Result.transaction_id;
                                        var getmsg = Result.msg;
                                        var getamount = Result.amount;
                                        var getstatus = Result.status;
                                        var timeleft = 10;
                                        var downloadTimer = setInterval(function(){
                                            if(timeleft <= 0){
                                                clearInterval(downloadTimer);
                                                document.getElementById("countdown").innerHTML = "Finished";
                                            } else {
                                                document.getElementById("countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";
                                            }

                                            timeleft -= 1;
                                        }, 1000);

                                        $('#transaction_id').html(transaction_id);
                                        $('#getmsg').html(getmsg);
                                        $('#getamount').html(getamount);
                                        $('#getstatus').html(getstatus);
                                        $('#messageDialog').modal('show');

                                        $('#plan').modal('hide');
                                        $('#Dv_Loader').hide();
                                        $.ajax({
                                            type:"POST",
                                            url:"/bbpsprepaidPay", 
                                            data : {'billername':billername,'billerId':billerId,'quick_payment_mode':quick_payment_mode,'quickpay_ccfamount':quickpay_ccfamount,'amount':amount,'customer_mobile':customer_mobile,'paramarr':paramarr, 'recharge_circle': recharge_circle, 'payment_method': payment_method},
                                            dataType: "json",
                                            success:function(data)
                                            {
                                                $('#Dv_Loader').hide();
                                                var Result = data.bbps_prepaidpay;
                                                if ((Result.resText)) {
                                                    // alert(Result.resText);
                                                    // var error = Result.resText; 
                                                    $('#amount_error').html(Result.resText);
                                                    $('#errormessageModal').modal({backdrop: 'static', keyboard: false}); 
                                                }else{
                                                    var txn_date_time = data.txn_date_time;
                                                    var resp_init_Channel = data.resp_init_Channel;
                                                    var respresponseCode = Result.responseCode;

                                                    if (respresponseCode == "000" ) {

                                                        $('#bbps_logo').hide();
                                                        $("#PRESuccessinputparamdisplay").empty();
                                                        $("#errormessage").empty();

                                                        if (data.bbps_prepaidpay.inputParams) {
                                                            var InputResult = data.bbps_prepaidpay.inputParams.input;
                                                            var isArray = Array.isArray(InputResult);
                                                            if (isArray) {
                                                                $.each(InputResult,function(key,value){
                                                                    var paramidinput = value.inputdata.replace(/ /g,"_");
                                                                    var inputDataHeading = value.inputdata;
                                                                    if(value.inputdata == "Mobile Number")
                                                                    {
                                                                        inputDataHeading = "Customer Mobile Number";
                                                                    }

                                                                    $("#PRESuccessinputparamdisplay").append('<h6 >'+inputDataHeading+'</h6><input style="" type="text" class="form-control input_style" data-pid="'+value.inputdata+'" value="'+value.paramvalue+'"  id="'+paramidinput+'" placeholder="'+value.inputdata+'" disabled>');
                                                                });
                                                            }else{
                                                                var paramidinput = InputResult.inputdata.replace(/ /g,"_");
                                                                $("#PRESuccessinputparamdisplay").append('<h6 >'+InputResult.inputdata+'</h6><input type="text" class="form-control input_style" data-pid="'+InputResult.inputdata+'" value="'+InputResult.paramvalue+'"  id="'+paramidinput+'" placeholder="'+InputResult.inputdata+'" disabled>');
                                                            }  
                                                        } else {
                                                            $("#PRESuccessinputparamdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style" placeholder="No Data Available" disabled></div>');
                                                        } // if inputparams

                                                        // Show response in respective div accordingly
                                                        var respbillername = billername;
                                                        var respbillerId = billerId;
                                                        var resptxnRefId = Result.txnRefId;
                                                        var respcustomerName = Result.RespCustomerName;
                                                        var respcustomermobile = customer_mobile;
                                                        var respbillAmount = Result.RespAmount;
                                                        // alert(respbillAmount);
                                                        var respCustConvFee = Result.CustConvFee;
                                                        // alert(respCustConvFee);
                                                        var respbilltotalAmount = Result.RespAmount;
                                                        var respdate_time = txn_date_time;
                                                        var respinit_Channel = resp_init_Channel;
                                                        var resppayment_mode = quick_payment_mode;
                                                        var respresponseReason = Result.responseReason;
                                                        var respapprovalRefNumber = Result.approvalRefNumber;
                                                  
                                                        $('#PRESSQpRespbillername').html(respbillername);
                                                        $('#PRESSQpRespbillerid').html(respbillerId);
                                                        $('#PRESSQpRespcustomerName').html(respcustomerName);
                                                        $('#PRESSQpresptxnRefId').html(resptxnRefId);
                                                        $('#PRESSQpRespcustomermobile').html(respcustomermobile);
                                                        $('#PRESSQpResppayment_mode').html(resppayment_mode);
                                                        $('#PRESSQprespapprovalRefNumber').html(respapprovalRefNumber);
                                                        $('#PRESSQprespresponseReason').html(respresponseReason);
                                                        $('#PRESSQpRespbillAmount').html(respbillAmount / 100);
                                                        $('#PRESSQprespCustConvFee').html(respCustConvFee);
                                                        $('#PRESSQpRespdate_time').html(respdate_time);
                                                        $('#PRESSQpresp_init_Channel').html(respinit_Channel);

                                                        $('#rechargeCircle').html(data.recharge_circle);

                                                        var a = (respbillAmount / 100);
                                                        var b = respCustConvFee;
                                                        var billtotalAmount = parseInt(a, 10) + parseInt(b, 10);
                                                        $('#PRESSQpRespbilltotalAmount').html(billtotalAmount);
                                                        $('#prepaidpaySuccessmodel').modal({backdrop: 'static', keyboard: false});
                                                    }
                                                    else if(respresponseCode == "900"){
                                                        $('#bbps_logo').hide();
                                                        $("#PREinputparamdisplay").empty();
                                                        $("#errormessage").empty();

                                                        var respbillername = data.billername;
                                                        var respbillerId = billerId;
                                                        var resptxnRefId = Result.txnRefId;
                                                        var respbillAmount = data.amount;
                                                        var respCustConvFee = data.quickpay_ccfamount;
                                                        var respdate_time = txn_date_time;
                                                        var respinit_Channel = resp_init_Channel;
                                                        var resppayment_mode = data.quick_payment_mode;
                                                        var respresponseReason = Result.responseReason;

                                                        $('#PRERespbillername').html(respbillername);
                                                        $('#PREQpRespbillerid').html(respbillerId);
                                                        $('#PREQpresptxnRefId').html(resptxnRefId);
                                                        $('#PREQprespresponseReason').html(respresponseReason);
                                                        $('#PREQpRespbillAmount').html(respbillAmount);
                                                        $('#PREQpRespdate_time').html(respdate_time);
                                                        $('#PREQpresp_init_Channel').html(respinit_Channel);
                                                        $('#PRErespCustConvFee').html(respCustConvFee);
                                                        $('#PREResppayment_mode').html(resppayment_mode);
                                                 
                                                        if (data.bbps_prepaidpay.inputParams) {
                                                            var InputResult = data.bbps_prepaidpay.inputParams.input;
                                                            var isArray = Array.isArray(InputResult);
                                                            if (isArray) {
                                                            $.each(InputResult,function(key,value){
                                                            // alert(value.inputdata);
                                                            var paramidinput = value.inputdata.replace(/ /g,"_");
                                                            $("#PREinputparamdisplay").append('<h6 >'+value.inputdata+'</h6><input style="" type="text" class="form-control input_style" data-pid="'+value.inputdata+'" value="'+value.paramvalue+'"  id="'+paramidinput+'" placeholder="'+value.inputdata+'" disabled>');
                                                            });
                                                            }else{
                                                            var paramidinput = InputResult.inputdata.replace(/ /g,"_");
                                                            $("#PREinputparamdisplay").append('<h6 >'+InputResult.inputdata+'</h6><input type="text" class="form-control input_style" data-pid="'+InputResult.inputdata+'" value="'+InputResult.paramvalue+'"  id="'+paramidinput+'" placeholder="'+InputResult.inputdata+'" disabled>');
                                                            }
                                                        } else {
                                                            $("#PREinputparamdisplay").append('<div class="form-group"><input type="hidden" class="form-control input_style"   placeholder="No Data Available" disabled></div>');
                                                        }
                                                        $('#prepaidpayPendingmodel').modal({backdrop: 'static', keyboard: false});
                                                    } else {
                                                        var error = Result.errorInfo.error;
                                                        // alert("1");
                                                        var isArray = Array.isArray(error);
                                                        if (isArray) {
                                                        $("#errormessage").empty();
                                                        $.each(error,function(key,value){
                                                        $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+value.errorMessage+'</p>');
                                                        });
                                                        }else{
                                                        var error =Result.errorInfo.error.errorMessage;
                                                        $("#errormessage").empty();
                                                        $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+error+'</p>');
                                                        }
                                                    }
                                                }
                                            }
                                       });
                                        //setTimeout(location.reload.bind(location), 10000);
                                     }
                                });
                               
                                },
                            };
                            var rzp1 = new Razorpay(options);
                            rzp1.open();
                            //e.preventDefault();
                        }
                    });
                }
            } else if (data.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    type: 'error',
                    confirmButtonClass: 'btn btn-success',
                })
            }
          })
    }
</script>
<!-- paybutton -->
<script type="text/javascript">
   $(document).ready(function(){
    $('#paybutton').click(function(){
      var billerId = $('#billerId').val();
      var RequestId = $('#RequestId').val();
      var rechargeamount = $('#rechargeamount').val();
      var bbpscustConvFee = $('#bbpscustConvFee').val();
      var billerpaymentModes = $('#payment_mode').val();
      var customer_mobile = document.getElementById("bbpscustomermobile").innerText;
      var bbpsbillAmount = document.getElementById("bbpsbillAmount").innerText;
      var bbpsbillDate = document.getElementById("bbpsbillDate").innerText;
      var bbpsbillNumber = document.getElementById("bbpsbillNumber").innerText;
      var bbpsbillPeriod = document.getElementById("bbpsbillPeriod").innerText;
      var bbpscustomerName = document.getElementById("bbpscustomerName").innerText;
      var bbpsdueDate = document.getElementById("bbpsdueDate").innerText;
      var billername = document.getElementById("bbpsbillername").innerText;
      var bbpsbilltotalAmount = document.getElementById("bbpsbilltotalAmount").innerText;

      if(bbpsbilltotalAmount == null)
      {
        alert('Please choose amount and try again');
        return false;
      }
       
      var inparamarr = []; 
      $("input[data-pid]").each(function(){
         var inputdatapar = $(this).data('pid');
         var inputparamid = inputdatapar.replace(/ /g,"_");
         var inparamvalue = $("#"+inputparamid).val();
         inparamarr.push({inputdatapar : inputdatapar, inparamvalue : inparamvalue});
      }); 

      var amountoptionarr = [];
      $("input[data-amount]").each(function(){
         var amountoptioinputdata = $(this).data('amount');
         var amountoptioinputparamid = amountoptioinputdata.replace(/ /g,"_");
         var amountoptioparamvalue = $("#"+amountoptioinputparamid).val();
         amountoptionarr.push({amountoptioinputdata : amountoptioinputdata, amountoptioparamvalue : amountoptioparamvalue});
      });

      var additionalInfoarr = [];
      $("input[data-additionalinfo]").each(function(){

         var additionalInfoinputdata = $(this).data('additionalinfo');
         var additionalInfoinputparamid = additionalInfoinputdata.replace(/ /g,"_");
         var additionalInfoparamvalue = $("#"+additionalInfoinputparamid).val();
            additionalInfoarr.push({additionalInfoinputdata : additionalInfoinputdata, additionalInfoparamvalue : additionalInfoparamvalue});
      });

      $('#getCodeModal').modal('hide');
      Swal.fire({
          title: 'Do you want to continue?',
          showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Continue',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then((data) => {
          if (data.value) {
            $('#plan').modal('hide');
            $('#Dv_Loader').show();
            $.ajax({
             type:"POST",
             url:"/getBBPSBillPay", 
             data : {'billerId':billerId,'RequestId':RequestId,'rechargeamount':rechargeamount,'billerpaymentModes':billerpaymentModes,'customer_mobile':customer_mobile,'inparamarr':inparamarr,'bbpscustConvFee':bbpscustConvFee,'amountoptionarr':amountoptionarr,'additionalInfoarr':additionalInfoarr,'bbpsbillAmount':bbpsbillAmount,'bbpsbillDate':bbpsbillDate,'bbpsbillNumber':bbpsbillNumber,'bbpsbillPeriod':bbpsbillPeriod,'bbpscustomerName':bbpscustomerName,'bbpsdueDate':bbpsdueDate,'billername':billername,'bbpsbilltotalAmount':bbpsbilltotalAmount,},
             dataType: "json",
             success:function(data)
             {   
               $('#Dv_Loader').hide();
                var Result = data.bbps_billpayrequest;
                if ((Result.resText)) {
                  // alert(Result.resText);
                  // var error = Result.resText; 
                  $('#amount_error').html(Result.resText);
                  $('#errormessageModal').modal({backdrop: 'static', keyboard: false});
                }else{
                  var txn_date_time = data.txn_date_time;
                  var initChannel = data.initChannel;
                  var respresponseCode = Result.responseCode;
                  if (respresponseCode == "000") {
                    $('#bbps_logo').hide();

                    var respbillername = billername;
                    var respcustomerName = Result.RespCustomerName;
                    var respbillerId = billerId;
                    var respbillDate = Result.RespBillDate;
                    var respbillPeriod = Result.RespBillPeriod;
                    var respbillNumber = Result.RespBillNumber;
                    var respdueDate = Result.RespDueDate;
                    var respcustomermobile = customer_mobile;
                    var resppayment_mode = billerpaymentModes;
                    var respbillAmount = Result.RespAmount;
                    var respbilltotalAmount = Result.RespAmount;
                    var resptxnRefId = Result.txnRefId;
                    var respapprovalRefNumber = Result.approvalRefNumber;
                    var respresponseReason = Result.responseReason;
                    var respCustConvFee = Result.CustConvFee;
                    var respdate_time = txn_date_time;
                    var init_Channel = initChannel;

                    $('#Respbillerid').html(respbillerId);
                    $('#Respbillername').html(respbillername);
                    $('#RespcustomerName').html(respcustomerName);
                    $('#RespbillDate').html(respbillDate);
                    $('#RespbillPeriod').html(respbillPeriod);
                    $('#RespbillNumber').html(respbillNumber);
                    $('#RespdueDate').html(respdueDate);
                    $('#Respcustomermobile').html(respcustomermobile);
                    $('#Resppayment_mode').html(resppayment_mode);
                    $('#resptxnRefId').html(resptxnRefId);
                    $('#respapprovalRefNumber').html(respapprovalRefNumber);
                    $('#respresponseReason').html(respresponseReason);
                    $('#RespbillAmount').html(respbillAmount / 100);
                    $('#respCustConvFee').html(respCustConvFee);
                    $('#Respdate_time').html(respdate_time);
                    $('#init_Channel').html(init_Channel);
                    var a = (respbillAmount / 100);
                    var b = respCustConvFee;
                    var billtotalAmount = parseInt(a, 10) + parseInt(b, 10);
                    $('#RespbilltotalAmount').html(billtotalAmount);
                     
                    $('#getsuccessmodel').modal({backdrop: 'static', keyboard: false});
                  }else{
                    $('#bbps_logo').show();
                       var error = Result.errorInfo.error;
                       var isArray = Array.isArray(error);
                       // alert(isArray);
                       if (isArray) {
                          $("#errormessage").empty();
                          $.each(error,function(key,value){
                           $("#errormessage").append('<p class="btn btn-danger">'+value.errorMessage+'</p>');
                          // $('#errormessageModal').modal('show');
                          });
                       }else{
                          var error =Result.errorInfo.error.errorMessage;
                          $("#errormessage").empty();
                           $("#errormessage").append('<p class="btn btn-danger">'+error+'</p>');
                          // $('#errormessage').html(error);
                          // $('#errormessageModal').modal('show');
                       }
                  }
                }
             }
          });
       }else if (data.dismiss === Swal.DismissReason.cancel) {
         Swal.fire({
          title: 'Cancelled',
          type: 'error',
          confirmButtonClass: 'btn btn-success',
        })
       }
     })
    });
   });
</script>
<!-- RazorPay -->
<!-- <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
   var SITEURL = '{{URL::to('')}}';
   $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   }); 
   $('body').on('click', '.pay_now', function(e){
     var totalAmount = document.getElementById("bbpsbilltotalAmount").innerText;
      var billerId = $('#billerId').val();
      var RequestId = $('#RequestId').val();
      var rechargeamount = $('#rechargeamount').val();
      var bbpscustConvFee = $('#bbpscustConvFee').val();
      var billerpaymentModes = $('#payment_mode').val();
      var customer_mobile = document.getElementById("bbpscustomermobile").innerText;
      var bbpsbillAmount = document.getElementById("bbpsbillAmount").innerText;
      var bbpsbillDate = document.getElementById("bbpsbillDate").innerText;
      var bbpsbillNumber = document.getElementById("bbpsbillNumber").innerText;
      var bbpsbillPeriod = document.getElementById("bbpsbillPeriod").innerText;
      var bbpscustomerName = document.getElementById("bbpscustomerName").innerText;
      var bbpsdueDate = document.getElementById("bbpsdueDate").innerText;
      var billername = document.getElementById("bbpsbillername").innerText;
      var bbpsbilltotalAmount = document.getElementById("bbpsbilltotalAmount").innerText;
       
      var inparamarr = []; 
      $("input[data-pid]").each(function(){
         var inputdatapar = $(this).data('pid');
         var inputparamid = inputdatapar.replace(/ /g,"_");
         var inparamvalue = $("#"+inputparamid).val();
         inparamarr.push({inputdatapar : inputdatapar, inparamvalue : inparamvalue});
      }); 

      var amountoptionarr = [];
      $("input[data-amount]").each(function(){
         var amountoptioinputdata = $(this).data('amount');
         var amountoptioinputparamid = amountoptioinputdata.replace(/ /g,"_");
         var amountoptioparamvalue = $("#"+amountoptioinputparamid).val();
         amountoptionarr.push({amountoptioinputdata : amountoptioinputdata, amountoptioparamvalue : amountoptioparamvalue});
      });

      var additionalInfoarr = [];
      $("input[data-additionalinfo]").each(function(){

         var additionalInfoinputdata = $(this).data('additionalinfo');
         var additionalInfoinputparamid = additionalInfoinputdata.replace(/ /g,"_");
         var additionalInfoparamvalue = $("#"+additionalInfoinputparamid).val();
            additionalInfoarr.push({additionalInfoinputdata : additionalInfoinputdata, additionalInfoparamvalue : additionalInfoparamvalue});
      });

   if (totalAmount == null || totalAmount == "") {
      alert("Amount Should Be Complusory");
      return false;
    }
                $('#Dv_Loader').show();
    $.ajax({
           type:"POST",
           url:"/getCardRazorpay", 
           data : {'totalAmount':totalAmount,},
           dataType: "json",
           success:function(data)
           {   
                $('#Dv_Loader').hide();
              var order_id = data.orderId;
              var ramount = data.amount;
              // alert(order_id);
              Swal.fire({
                title: 'Do you want to continue?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                buttonsStyling: false,
              }).then((data) => {
                if (data.value) {
                  var options = {
                     "key": "rzp_test_UfbGEvka08iFR0",
                     "amount": ramount, // 2000 paise = INR 20
                     "name": "DAMSKART",
                     "description": "Payment",
                     "image": "https://damskart.com/public//files/assets/images/Damskart%20Logo.png",
                      "prefill": {
                         "contact": '<?php echo Auth::user()->mobile;?>',
                         "email":   '<?php echo Auth::user()->email;?>',
                     },
                     "theme": {
                         "color": "#7367F0"
                     },
                     "order_id": order_id,
                     "handler": function (response){
                      $('#getCodeModal').modal('hide');
                        $('#plan').modal('hide');
                        $('#Dv_Loader').show();
                           $.ajax({
                             url: SITEURL + '/getBBPSBillPay',
                             type: 'get',
                             dataType: 'json',
                             data: {
                              razorpay_payment_id: response.razorpay_payment_id , totalAmount : totalAmount,'billerId':billerId,'RequestId':RequestId,'rechargeamount':rechargeamount,'billerpaymentModes':billerpaymentModes,'customer_mobile':customer_mobile,'inparamarr':inparamarr,'bbpscustConvFee':bbpscustConvFee,'amountoptionarr':amountoptionarr,'additionalInfoarr':additionalInfoarr,'bbpsbillAmount':bbpsbillAmount,'bbpsbillDate':bbpsbillDate,'bbpsbillNumber':bbpsbillNumber,'bbpsbillPeriod':bbpsbillPeriod,'bbpscustomerName':bbpscustomerName,'bbpsdueDate':bbpsdueDate,'billername':billername,'bbpsbilltotalAmount':bbpsbilltotalAmount, 
                             }, 
                             success: function (res) {
                                 $('#Dv_Loader').hide();
                                  var Result = res.bbps_billpayrequest;
                                  var txn_date_time = res.txn_date_time;
                                  var initChannel = res.initChannel;
                                  var respresponseCode = Result.responseCode;
                                  if (respresponseCode == "000") {
                                    $('#bbps_logo').hide();

                                    var respbillername = billername;
                                    var respcustomerName = Result.RespCustomerName;
                                    var respbillerId = billerId;
                                    var respbillDate = Result.RespBillDate;
                                    var respbillPeriod = Result.RespBillPeriod;
                                    var respbillNumber = Result.RespBillNumber;
                                    var respdueDate = Result.RespDueDate;
                                    var respcustomermobile = customer_mobile;
                                    var resppayment_mode = billerpaymentModes;
                                    var respbillAmount = Result.RespAmount;
                                    var respbilltotalAmount = Result.RespAmount;
                                    var resptxnRefId = Result.txnRefId;
                                    var respapprovalRefNumber = Result.approvalRefNumber;
                                    var respresponseReason = Result.responseReason;
                                    var respCustConvFee = Result.CustConvFee;
                                    var respdate_time = txn_date_time;
                                    var init_Channel = initChannel;

                                    $('#Respbillerid').html(respbillerId);
                                    $('#Respbillername').html(respbillername);
                                    $('#RespcustomerName').html(respcustomerName);
                                    $('#RespbillDate').html(respbillDate);
                                    $('#RespbillPeriod').html(respbillPeriod);
                                    $('#RespbillNumber').html(respbillNumber);
                                    $('#RespdueDate').html(respdueDate);
                                    $('#Respcustomermobile').html(respcustomermobile);
                                    $('#Resppayment_mode').html(resppayment_mode);
                                    $('#resptxnRefId').html(resptxnRefId);
                                    $('#respapprovalRefNumber').html(respapprovalRefNumber);
                                    $('#respresponseReason').html(respresponseReason);
                                    $('#RespbillAmount').html(respbillAmount / 100);
                                    $('#respCustConvFee').html(respCustConvFee);
                                    $('#Respdate_time').html(respdate_time);
                                    $('#init_Channel').html(init_Channel);
                                    var a = (respbillAmount / 100);
                                    var b = respCustConvFee;
                                    var billtotalAmount = parseInt(a, 10) + parseInt(b, 10);
                                    $('#RespbilltotalAmount').html(billtotalAmount);
                                     
                                    $('#getsuccessmodel').modal('show');
                                  }else{
                                    $('#bbps_logo').show();
                                       var error = Result.errorInfo.error;
                                       var isArray = Array.isArray(error);
                                       // alert(isArray);
                                       if (isArray) {
                                          // $('#errormessage').html(error);
                                          $("#errormessage").empty();
                                          $.each(error,function(key,value){
                                           $("#errormessage").append('<p class="btn btn-danger">'+value.errorMessage+'</p>');
                                          // $('#errormessageModal').modal('show');
                                          });
                                       }else{
                                          var error =Result.errorInfo.error.errorMessage;
                                          $("#errormessage").empty();
                                           $("#errormessage").append('<p class="btn btn-danger">'+error+'</p>');
                                          // $('#errormessage').html(error);
                                          // $('#errormessageModal').modal('show');
                                       }
                                  }
                             }
                         });
                       
                     },
                   
                   };
                   var rzp1 = new Razorpay(options);
                   rzp1.open();
                   e.preventDefault();
                } else if (data.dismiss === Swal.DismissReason.cancel) {
                   Swal.fire({
                      title: 'Cancelled',
                      type: 'error',
                      confirmButtonClass: 'btn btn-success',
                    })
                }
              })

           }
        });
     
   });
</script> -->
<!-- validate_quickpaybutton -->
<script type="text/javascript">
   function validate_quickpaybutton() {
      var billerId = $('#billerId').val();
      var amount = $('#amount').val();
       var paramarr = [];
       $("input[data-id]").each(function(){
          var inputdata = $(this).data('id');
          var inputparamid = inputdata.replace(/ /g,"_");
          var paramvalue = $("#"+inputparamid).val();
          paramarr.push({inputdata : inputdata, paramvalue : paramvalue});
       });
        Swal.fire({
             title: 'Do you want to continue?',
             showDenyButton: true,
             // showCancelButton: true,
             confirmButtonText: `Pay`,
             denyButtonText: `Cancle`,
           }).then((data) => {
             if (data.isConfirmed) {
               $('#Dv_Loader').show();
                        $('#plan').modal('hide');
                  $.ajax({
                   type:"POST",
                   url:"/bbpsbillValidation", 
                   data : {'billerId':billerId, 'paramarr':paramarr},
                   dataType: "json",
                   success:function(data)
                   {   
                      $('#Dv_Loader').hide();
                      var Result = data;
                      var respresponseCode = Result.responseCode;
                      if (respresponseCode == "000") {
                        $("#validate_quickpaybutton").hide();
                        $("#payment_button").empty();
                        
                        
                        $("#payment_button").append('<p>User Verfication Successful </p><button type="submit" style="margin-left: 20px;" id="quickpaybutton" onclick="quickpaybutton()" class="btn btn-success waves-effect m-r-20 f-w-600 d-inline-block save_btn">Pay</button>');   
                        
                      }else{
                        var error = Result.errorInfo.error;
                        var isArray = Array.isArray(error);
                        if (isArray) {
                           $("#errormessage").empty();
                           $.each(error,function(key,value){
                            $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+value.errorMessage+'</p>');
                           });
                        }else{
                           var error =Result.errorInfo.error.errorMessage;
                           $("#errormessage").empty();
                            $("#errormessage").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+error+'</p>');
                        }
                      }
                   }
                });
             } else if (data.isDenied) {
               Swal.fire('Cancle Payment', '', 'info')
             }
           })
    }
</script>
@endsection