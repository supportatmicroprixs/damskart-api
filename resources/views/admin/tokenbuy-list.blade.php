@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Tokens</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Settings
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title"></h4> -->
                        <a style="margin-left: auto;" class="btn btn-relief-primary mr-1 mb-1" href="{{route('add-tokenbuy')}}">
                        <i class="feather icon-plus"></i> Buy Token
                        </a>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Role</th>
                                       <th>Member</th>
                                       <th>Package Name</th>
                                       <th>Token</th>
                                       <th>Amount</th>
                                       <th>Date</th>
                                       <th>Payment</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($token as $item)
                                    <tr>
                                   <td>{{ucfirst($item->role) }}</td>
                                   <td><a href="{{route('view-member', $item->user_id)}}">{{$item->first_name }} {{$item->last_name}}</a> <small>({{$item->mobile}})</small></td>
                                   <td>{{$item->package_name }}</td>
                                   <td>{{$item->token }}</td>
                                   <td>{{number_format($item->price,2)}}</td>
                                   <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                    @if($item->receive == "Received")
                                       <td><a href="#!" class="btn btn-success">Done</a></td>
                                       @elseif($item->receive == "Pending")
                                       <td><a data-id="{{$item->id}}" href="#" class="btn btn-primary" onclick="active_status({{$item->id}})">{{$item->receive}}</a></td>
                                       @else
                                       <td><a href="#!" class="btn btn-danger">{{$item->receive}}</a></td>
                                       @endif
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script type="text/javascript">
   
    var token = {
        
        'status':function(){
          var status_id = $("#status_id").val();
          $.ajax({
            type:'POST',
            url: '<?= route('tokenbuy-status') ?>',
            data:{'id':status_id,"_token": "{{ csrf_token() }}"},
            success:function(response){  

              if(response.success==1){
                var payload = {
                    'title':'Token Buy Successfully',
                    'message':response.message,
                  }
                  damskart.alertModal(payload);
              }else{
                var payload = {
                    'title':'Token Buy Warning',
                    'message':response.message,
                  }
                  damskart.alertModal(payload);
              }                                       
              setTimeout(function(){
                 location.reload();
              }, 2000);

            },
            error: function(response){
               var payload = {
                    'title':'Token Buy Warning',
                    'message':response.message,
                  }
                  damskart.alertModal(payload);
            }
          });
        }
    }

 function active_status(id){
  var id = id;
  var payload = {
    'url':'<?= route('status_confirm') ?>',
    'title':'Token Payment',
    'button_name':'Yes',
    'message':'Are you sure you want to make payment? Before payment please make sure your wallet have sufficient balance.',
    "_token": "{{ csrf_token() }}",
    'id':id,
    "modal_id": "delete-modal",
  }
  damskart.loadModal(payload,'token.status()');
 }              


 
</script>
@endsection
