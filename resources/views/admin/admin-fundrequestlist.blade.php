@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Fund Request List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Wallet
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Member's Fund Requests</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Member ID</th>
                                       <th>Amount</th>
                                       <th>Payment Type</th>
                                       <th>Date</th>
                                       <th>Approve Date</th>
                                       <th>Receive Date</th>
                                       <th>Approve Status</th>
                                       <th>Receive</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($fundrequest as $item)
                                    <tr>
                                       <td class="text-center">{{$item->mobile}}</td>
                                       <td class="text-center">&#x20B9; {{number_format($item->amount,3)}}</td>
                                       <td class="text-center">{{$item->payment_type}}</td>
                                       <td class="text-center"><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                       <td class="text-center">
                                          @if($item->approved_date == NULL)
                                          <p>Not Approved Yet!</p>
                                          @else
                                          {{date('d-M-Y', strtotime($item->approved_date))}}
                                          @endif
                                       </td>
                                       <td class="text-center">
                                          @if($item->received_date == NULL)
                                          <p>Not Received Yet!</p>
                                          @else
                                          {{date('d-M-Y', strtotime($item->received_date))}}
                                          @endif
                                       </td>
                                       @if($item->status == "Pending")
                                       <td class="text-center">
                                          <div class="btn-group dropdown mr-1 mb-1">
                                             <button type="button" class="btn  btn-primary">{{$item->status}}</button>
                                             <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <span class="sr-only">Toggle Dropdown</span>
                                             </button>
                                             <div class="dropdown-menu">
                                                <a class="dropdown-item active" href="/approve/{{$item->id}}">Approve</a>
                                                <a class="dropdown-item" href="/reject/{{$item->id}}">Reject</a>
                                             </div>
                                          </div>
                                       </td>
                                       @else
                                       @if($item->status == "Rejected")
                                       <td class="text-center"><a href="#!" class="btn btn-danger">{{$item->status}}</a></td>
                                       @else
                                       <td class="text-center"><a href="#!" class="btn btn-success">{{$item->status}}</a></td>
                                       @endif
                                       @endif
                                       @if($item->receive == "Pending")
                                       <td class="text-center">
                                          <div class="btn-group dropdown mr-1 mb-1">
                                             <button type="button" class="btn  btn-primary">{{$item->receive}}</button>
                                             <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <span class="sr-only">Toggle Dropdown</span>
                                             </button>
                                             <div class="dropdown-menu">
                                                <a class="dropdown-item" href="/receive/{{$item->id}}">Receive</a>
                                             </div>
                                          </div>
                                       </td>
                                       @else
                                       @if($item->receive == "Rejected")
                                       <td class="text-center"><a href="#!" class="btn btn-danger">{{$item->receive}}</a></td>
                                       @else
                                       <td class="text-center"><a href="#!" class="btn btn-success">{{$item->receive}}</a></td>
                                       @endif
                                       @endif
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection