@extends('admin.layouts.main')
@section('css')
<style type="text/css">
      .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }

</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">User Login List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Report
                        </li>
                        <li class="breadcrumb-item active" style="margin-top: -4px;"><a href="/viewmember/{{$loginuser_name->id}}" title="Click to View Profile" data-toggle="tooltip" class="badge badge-pill badge-success badge-lg"> {{ucfirst($loginuser_name->first_name).' '.ucfirst($loginuser_name->last_name)}}</a>
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <p id="resp_message"></p>
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="floating-label-layouts">
            <div class="row match-height">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Filter</h4>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="row">
                                    <div class="col-md-5 col-12 mb-1">
                                       <fieldset>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                             </div>
                                             <input type="text" class="form-control" id="filter_date" placeholder="Select Date Range" name="daterange" value="" />
                                             <input type="hidden" class="form-control" id="user_id" placeholder="User" name="user_id" value="{{$loginuser_name->id}}" />
                                          </div>
                                       </fieldset>
                                    </div>

                                    <div class="col-md-5 col-12 mb-1">
                                       
                                    </div>
                                    <div class="col-md-2 col-12 mb-1">
                                       <div class="input-group-append">
                                          <button  type="submit"  class="btn btn-primary" id="searchbutton" ><i class="feather icon-search"></i> Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <!-- <h4 class="card-title">User's</h4> -->
                     </div>
                     <div class="card-content" id="alltxn">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example11" class="table table-striped table-bordered ">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th>Package</th>
                                       <th>Owner</th>
                                       <th>Login From</th>
                                       <th>Login To</th>
                                       <th>Duration</th>
                                    </tr>
                                 </thead>

                                 <tbody>
                                    @foreach($userData as $item)
                                    <tr>
                                       <td><a href="{{route('view-member', $item['user_id'])}}">{{ucfirst($item['first_name']).' '.ucfirst($item['last_name'])}} <span>({{$item['mobile'] }})</span></a></td>
                                       <td>{{ $item['package_name'] }}</td>
                                       <td>{{ $item['login_from']}}</td>
                                       <td>{{ $item['start_time']}}</td>
                                       <td>{{ $item['end_time']}}</td>
                                       <td>{{ $item['duration']}}</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="card-content" id="searchtxn">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">
            <button class="btn btn-primary mb-1 round" type="button" >
            <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
            &nbsp;Loading... 
            </button>
         </div>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script>
   $(function() {
     $('input[name="daterange"]').daterangepicker({
       opens: 'center',
        locale: {
          cancelLabel: 'Clear'
      }
     }, function(start, end, label) {
       console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
     });
     $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
   });
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
    $('#searchbutton').click(function(){
      var SITEURL = '{{URL::to('')}}';
       var filter_date = $('#filter_date').val();
       var user_id = $('#user_id').val();
       var date_arr = filter_date.split('- ');
       var start_date = date_arr[0];
       var end_date = date_arr[1];
        $('#Dv_Loader').show();
        $.ajax({
           type:"get",
           url:"/filteruserlogin", 
           data : {'user_id':user_id,'start_date':start_date,'end_date':end_date},
           dataType: "json",
           success:function(data)
           {   
               $('#Dv_Loader').hide();
               if (data.permission_code == "PER111") {
                $("#resp_message").empty();
                $("#resp_message").html('<button class="btn btn-danger">'+data.message+'</button>').css('color','red');
                  
                  setTimeout(function () {
                    window.location.href = SITEURL + '/index';
                  }, 3000);
               }else{
                 var Result = data;
                  $("#alltxn").empty();
                  $("#searchtxn").empty();
                  $("#transactionresult").empty();
                  $("#searchtxn").append(' <div class="card-body card-dashboard"><div class="table-responsive"><table id="example1" class="table table-striped table-bordered ">   <thead> <tr> <th>Name</th> <th>Package</th> <th>Owner</th> <th>Login From</th> <th>Login To</th> <th>Duration</th>  </tr> </thead>   <tbody id="transactionresult">        </tbody></table></div></div>');
                 $.each(Result,function(key,value){
                     var url = '{{ route("view-member", ":id") }}';

                     url = url.replace(':id', value.user_id);

                     var tr ='';

                     tr += '<tr><td><a href="'+url+'">'+value.first_name+' '+value.last_name+' <span> ('+value.mobile+')</span></a></td>';

                     var end_time = value.end_time;
                     if(end_time == null)
                     {
                        end_time = "";
                     }

                     var duration = value.duration;
                     if(duration == null)
                     {
                        duration = "";
                     }

                     tr += '<td>'+value.package_name+'</td><td>'+value.login_from+'</td><td>'+value.start_time+'</td><td>'+end_time+'</td><td>'+duration+'</td>';

                     tr += '</tr>';

                     $('#transactionresult').append(tr);
                                       

                 });
                }
                $('#example1').DataTable( {
                    dom: 'Bfrtip',
                    "pageLength": 20,
                     buttons: [
                       'excel', 'pdf', 'print', 
                     ]
                } );
           }
        });
        });
      });
</script>
@endsection