@extends('admin.layouts.main')
@section('css')
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">Fund Request List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Wallet
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <div class="row">
            <div class="col-12">
               <!-- <p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
            </div>
         </div>
         <!-- Column selectors with Export Options and print table -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="column-selectors">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Fund Requests</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body card-dashboard">
                           <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Member ID</th>
                                       <th>Amount</th>
                                       <th>Payment Type</th>
                                       <th>Date</th>
                                       <th>Approve Status</th>
                                       <th>Receive</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($fundrequest as $item)
                                    <tr>
                                       <td>{{$item->mobile}}</td>
                                       <td>&#x20B9; {{number_format($item->amount,3)}}</td>
                                       <td>{{$item->payment_type}}</td>
                                       <td><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                       @if($item->status == "Approved")
                                       <td><a href="#!" class="btn btn-success">{{$item->status}}</a></td>
                                       @elseif($item->status == "Pending")
                                       <td><a href="#!" class="btn btn-primary">{{$item->status}}</a></td>
                                       @else
                                       <td><a href="#!" class="btn btn-danger">{{$item->status}}</a></td>
                                       @endif
                                       @if($item->receive == "Received")
                                       <td><a href="#!" class="btn btn-success">{{$item->receive}}</a></td>
                                       @elseif($item->receive == "Pending")
                                       <td><a href="#!" class="btn btn-primary">{{$item->receive}}</a></td>
                                       @else
                                       <td><a href="#!" class="btn btn-danger">{{$item->receive}}</a></td>
                                       @endif
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Column selectors with Export Options and print table -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
@endsection