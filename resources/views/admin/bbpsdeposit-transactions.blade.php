@extends('admin.layouts.main')
@section('css')
<style type="text/css">
   .divWaiting {
   position: fixed;
   background-color: #FAFAFA;
   z-index: 2147483647 !important;
   opacity: 0.8;
   overflow: hidden;
   text-align: center;
   top: 0;
   left: 0;
   height: 100%;
   width: 100%;
   padding-top: 20%;
   }
</style>
@endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
               <div class="col-12">
                  <h2 class="content-header-title float-left mb-0">BBPS Deposit Transaction's List</h2>
                  <div class="breadcrumb-wrapper col-12">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Report</a>
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Nav Centered And Nav End Starts -->
         @if ($errors->any())
         <div  class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if ($message = Session::get('info'))
         <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         @if ($message = Session::get('danger'))
         <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <section id="nav-tabs-centered">
            <div class="row">
               <div class="col-sm-12">
                  <div class="card overflow-hidden">
                     <div class="card-header">
                        <!-- <h4 class="card-title">Center</h4> -->
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <!-- <p>Use class <code>.justify-content-center</code> to align your menu to center</p> -->
                           <ul class="nav nav-tabs justify-content-center" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="home-tab-center" data-toggle="tab" href="#DR" aria-controls="home-center" role="tab" aria-selected="true">BBPS Transaction's (DR)</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " id="service-tab-center" data-toggle="tab" href="#CR" aria-controls="service-center" role="tab" aria-selected="false">BBPS Transaction's (CR)</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="DR" aria-labelledby="home-tab-center" role="tabpanel">
                                 <table class="table table-striped dataex-html5-selectors">
                                 <thead>
                                    <tr>
                                      <th class="text-center">Date</th>
                                          <th class="text-center">Transaction ID</th>
                                          <th class="text-center">Name</th>
                                          <th class="text-center">Mobile No</th>
                                          <th class="text-center">Amount</th>
                                          <th class="text-center">UPI Txn ID</th>
                                          <th class="text-center">Message</th>
                                          <th class="text-center">Status</th>
                                          <th class="text-center">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($upiqrtxn as $item)
                                       <tr>
                                          <td class="text-center"><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                          <td class="text-center">{{$item->dms_txn_id}}</td>
                                          <td class="text-center">{{$item->name}}</td>
                                          <td class="text-center">{{$item->mobile}}</td>
                                          <td class="text-center">&#x20B9; {{$item->amount}}</td>
                                          <td class="text-center">{{$item->merchantTranId}}</td>
                                          <td class="text-center">{{$item->message}}</td>
                                          <td class="text-center">{{$item->status}}</td>
                                          <td class="text-center">
                                             &nbsp; <a href="/upitxnStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> 
                                             &nbsp; <a href="/upicallbackStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Callback Txn Status"><i style="color: blue;" class="feather icon-check-circle"></i></a> 
                                             <!-- @if($item->status == "SUCCESS")
                                             &nbsp; <a href="/upirefundRequest/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Refund Request"><i style="color: red;" class="feather icon-corner-down-left"></i></a>
                                             @endif -->
                                          </td>
                                       </tr>
                                       @endforeach 
                                 </tbody>
                              </table>                                                                
                              </div>
                              <div class="tab-pane " id="CR" aria-labelledby="service-tab-center" role="tabpanel">
                                 <table id="dom-jqry1" class="table table-striped table-bordered zero-configuration">
                                    <!-- <table id="colum-rendr" class="table table-striped table-bordered "> -->
                                    <thead>
                                       <tr>
                                          <th class="text-center">Date</th>
                                          <th class="text-center">Transaction ID</th>
                                          <th class="text-center">UPI ID</th>
                                          <th class="text-center">Name</th>
                                          <th class="text-center">Mobile No</th>
                                          <th class="text-center">Amount</th>
                                          <th class="text-center">UPI Txn ID</th>
                                          <th class="text-center">Message</th>
                                          <th class="text-center">Status</th>
                                          <th class="text-center">Action</th>
                                       </tr>
                                    </thead>
                                    <tbody id="databox">
                                       @foreach($upisendreqtxn as $item)
                                       <tr>
                                          <td class="text-center"><span style="display: none;">{{date('YmdHis', strtotime($item->created_at))}}</span>{{date('d-m-Y h:i:s A', strtotime($item->created_at))}}</td>
                                          <td class="text-center">{{$item->dms_txn_id}}</td>
                                          <td class="text-center">{{$item->upi_id}}</td>
                                          <td class="text-center">{{ucfirst($item->name)}}</td>
                                          <td class="text-center">{{$item->mobile}}</td>
                                          <td class="text-center">&#x20B9; {{$item->amount}}</td>
                                          <td class="text-center">{{$item->merchantTranId}}</td>
                                          <td class="text-center">{{$item->message}}</td>
                                          <td class="text-center">{{$item->status}}</td>
                                          <td class="text-center">
                                             &nbsp; <a href="/upitxnStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Txn Status" ><i style="color: green;" class="feather icon-eye"></i></a> 
                                             &nbsp; <a href="/upicallbackStatusCheck/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Check Callback Txn Status" ><i style="color: blue;" class="feather icon-check-circle"></i></a>
                                             @if($item->status == "SUCCESS")
                                              &nbsp; <a href="/upirefundRequest/{{$item->merchantTranId}}" style="border-radius: 20px;" title="Refund Request" ><i style="color: red;" class="feather icon-corner-down-left"></i></a>
                                             @endif
                                          </td>
                                       </tr>
                                       @endforeach
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="divWaiting" id="Dv_Loader" style="display: none;">

         <button class="btn btn-primary mb-1 round" type="button" >
         <span class="spinner-grow spinner-grow-sm text-danger" role="status" aria-hidden="true"></span>
         <span class="spinner-grow spinner-grow-sm text-dark" role="status" aria-hidden="true"></span>
         <span class="spinner-grow spinner-grow-sm text-secondary" role="status" aria-hidden="true"></span>
         <span class="spinner-grow spinner-grow-sm text-success" role="status" aria-hidden="true"></span>
         <span class="spinner-grow spinner-grow-sm text-warning" role="status" aria-hidden="true"></span>
         &nbsp;Loading...
         </button>
      </div>
         <!-- Nav Centered And Nav End Ends -->
      </div>
   </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>
<script>
  var qr;
  (function() {
        qr = new QRious({
        element: document.getElementById('qr-code'),
        size: 0,
        value: 'https://damskart.com'
    });
  })();
        
  function generateQRCode() {
      var qrtext = document.getElementById("qr-text").value;
      document.getElementById("qr-result").innerHTML = "Scan & Pay:";
      // alert(qrtext);
      qr.set({
          foreground: 'black',
          size: 200,
          value: qrtext
      });
  }
 
</script>
<script>
  var myqr;
  (function() {
        myqr = new QRious({
        element: document.getElementById('myqr-code'),
        size: 0,
        value: 'https://damskart.com'
    });
  })();
        
   function generateMYQR() {
      var myqrtext = document.getElementById("myqr-text").value;
      // document.getElementById("myqr-result").innerHTML = "My QR Code:";
      // alert(myqrtext);
      myqr.set({
          foreground: 'black',
          size: 200,
          value: myqrtext
      });
  }
 
</script>
<script>
   $(document).ready(function() {
    $("#check_amount").click(function() {
        $("#generate_amount").attr("disabled", false);
     });

    $("#check_generate_qr").click(function() {
        $("#generate_amount").attr("disabled", true);
     });
});
</script>
<script>
    document.getElementById('pay_type').onchange = function(){
        document.getElementById('price').disabled = (this.value === 'free');
        document.getElementById('display_price').disabled = (this.value === 'free');
    }
</script>
<script type="text/javascript">
  function generatebuttonDetected(){
    var gen_mobile = $('#generate_mobile').val();
    var gen_name = $('#generate_name').val();
    var gen_amount = $('#generate_amount').val();
    // var check_generate_qr = $('#check_generate_qr').val();
    // var check_amount = $('#check_amount').val();
        if (gen_mobile.length > 0 && gen_name.length > 0 ) {
          $('#generate_button').removeAttr('disabled');
        } else {
          $('#generate_button').attr('disabled', 'disabled');
        }
  }
</script>
<script type="text/javascript">
  function requestbuttonDetected(){
    var reqmobile = $('#request_mobile').val();
    var requpi_id = $('#request_upi_id').val();
    var reqname = $('#request_name').val();
    var reqamount = $('#request_amount').val();
        if (reqmobile.length > 0 && requpi_id.length > 0 && reqname.length > 0 && reqamount.length > 0) {
          $('#request_button').removeAttr('disabled');
        } else {
          $('#request_button').attr('disabled', 'disabled');
        }
  }
</script>
<script type="text/javascript"> 
  function myqr_button(){
      $("#my_qr").append('<input id="myqr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr=MNO9799594939&am=&cu=INR&mc=5411" placeholder="Text to generate QR code"/>'); 
   
  }
   
</script>

<script type="text/javascript">
   $(document).ready(function(){
       $('#generate_button').click(function(){
          var generatemobile = $('#generate_mobile').val();
          var generatename = $('#generate_name').val();
          var generateamount = $('#generate_amount').val();
           $('#Dv_Loader').show();
           $.ajax({
              type:"get",
              url:"/upigenerateqr", 
              data : {'generatemobile':generatemobile,'generatename':generatename,'generateamount':generateamount},
              dataType: "json",
              success:function(data)
              {   
                 $('#Dv_Loader').hide();
                 var Result = data.upigenerateQr;
                 var qramount = data.amount;
                 var generate_type = data.generate_type;
                 var dms_txn_id = data.dms_txn_id;
                 var message = Result.message;
                 if (Result.response == "0") {
                     var merchantTranId = Result.merchantTranId;
                     // window.location.href = 'upi://pay?pa=uatmer018@icici&pn=DamskartPay&tr='+Result.refId+'&am='+qramount+'&cu=INR&mc=5411';
                     $("#generate_button").hide();
                     // $("#qrString").empty();
                     // $("#qrString").empty();
                     // $("#qr-result").empty();
                     // $("#qr-code").empty();
                     // $("#qr-text").empty();
                     // $("#generate_response").append('<p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">TxnId : '+dms_txn_id+'</p><p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">Txn Status : '+message+'</p>');
                     if (generate_type == "generate_amount_qr") {
                        $("#qrString").append('<input id="qr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr='+Result.refId+'&am='+qramount+'&cu=INR&mc=5411" placeholder="Text to generate QR code"/><button class="qr-btn btn btn-success" onclick="generateQRCode()">Click To Generate QR</button>'); 
                     }else{
                        $("#qrString").append('<input id="qr-text" type="hidden" value="upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr='+Result.refId+'&am=&cu=INR&mc=5411" placeholder="Text to generate QR code"/><button class="qr-btn btn btn-success" onclick="generateQRCode()">Click To Generate QR</button>'); 
                     }
                 }else{
                     $("#generate_response").empty();
                     $("#generate_response").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+message+'</p>'); 
                 }
              }
           });
           });
       });
</script>

<script type="text/javascript">
   $(document).ready(function(){
       $('#request_button').click(function(){
          var requestmobile = $('#request_mobile').val();
          var requestupi_id = $('#request_upi_id').val();
          var requestname = $('#request_name').val();
          var requestamount = $('#request_amount').val();
           $('#Dv_Loader').show();
           $.ajax({
              type:"get",
              url:"/upisendPayRequest", 
              data : {'requestmobile':requestmobile,'requestupi_id':requestupi_id,'requestname':requestname,'requestamount':requestamount},
              dataType: "json",
              success:function(data)
              {   
                 $('#Dv_Loader').hide();
                 var Result = data.upisendpayrequest;
                 var reqamount = data.amount;
                 var rdms_txn_id = data.dms_txn_id;
                 var message = Result.message;
                 if (Result.response == "92") {
                     var merchantTranId = Result.merchantTranId;
                     $("#request_button").hide();
                     $("#sendrequest_response").empty();
                     $("#sendrequest_response").append('<p class="text-white" style="background-color:green; padding: 8px 12px; border-radius: 5px;">'+message+'</p>');
                 }else{ 
                     $("#sendrequest_response").empty();
                     $("#sendrequest_response").append('<p class="text-white" style="background-color:#d13625; padding: 8px 12px; border-radius: 5px;">'+message+'</p>'); 
                 }
              }
           });
           });
       });
</script>
@endsection