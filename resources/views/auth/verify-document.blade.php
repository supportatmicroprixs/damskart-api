<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

   <!-- BEGIN: Head-->

   <head>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

      <title>Verify Document - Damskart Business</title>

      <link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">

      <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">

      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

      <!-- BEGIN: Vendor CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">

      <!-- END: Vendor CSS-->

      <!-- BEGIN: Theme CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">

      <!-- BEGIN: Page CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/authentication.css')}}">

      <!-- END: Page CSS-->

      <!-- BEGIN: Custom CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">

      <!-- END: Custom CSS-->

      <style type="text/css">

         .divWaiting {

         position: fixed;

         background-color: #FAFAFA;

         z-index: 2147483647 !important;

         opacity: 0.8;

         overflow: hidden;

         text-align: center;

         top: 0;

         left: 0;

         height: 100%;

         width: 100%;

         padding-top: 20%;

         }

      </style>

   </head>

   <!-- END: Head-->

   <!-- BEGIN: Body-->

   <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

      <!-- BEGIN: Content-->

      <div class="app-content content">

         <div class="content-overlay"></div>

         <div class="header-navbar-shadow"></div>

         <div class="content-wrapper">

            <div class="content-header row">

            </div>

            <div class="content-body">

               <section class="row flexbox-container">

                  <div class="col-xl-10 col-10 d-flex justify-content-center mt-2 pb-2">

                     <div class="card bg-authentication rounded-0 mb-0 w-100">

                        <div class="row m-0">

                           <div class="col-lg-5 d-lg-block d-none text-center align-self-center p-0">

                              <img src="{{ asset('admin/app-assets/images/pages/forgot-password.png')}}" alt="branding logo">

                           </div>

                           <div class="col-lg-7 col-12 p-0">

                              <div class="card rounded-0 mb-0 px-2">

                                 <div class="card-header pb-1">

                                    <div class="card-title">

                                       <h3 class="mb-0 mt-1">Document Verification</h3>

                                    </div>

                                       <a class="btn btn-outline-primary" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form1').submit();">Back to Login</a>

                                 </div>

                                 <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">

                                          @csrf

                                       </form>

                                 @if ($errors->any())

                                 <div  class="alert alert-danger alert-block">

                                    <button type="button" class="close" data-dismiss="alert">×</button> 

                                    <ul>

                                       @foreach ($errors->all() as $error)

                                       <li>{{ $error }}</li>

                                       @endforeach

                                    </ul>

                                 </div>

                                 @endif

                                 @if ($message = Session::get('info'))

                                 <div class="alert alert-primary alert-block">

                                    <button type="button" class="close" data-dismiss="alert">×</button> 

                                    <strong>{{ $message }}</strong>

                                 </div>

                                 @endif

                                 @if ($message = Session::get('danger'))

                                 <div class="alert alert-danger alert-block">

                                    <button type="button" class="close" data-dismiss="alert">×</button> 

                                    <strong>{{ $message }}</strong>

                                 </div>

                                 @endif

                                 

                                 <b><p class="px-2" style="font-size: 18px;" id="document_verify_response"></p></b>



                                 <!-- <button type="button" class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#verifyOTPModal">Upload Document</button> -->

                                 <div class="card-content">

                                    <div class="card-body">

                                       <ul class="nav nav-pills nav-active-bordered-pill">



                                          <li class="nav-item dropdown">

                                             <a class="nav-link active"  data-toggle="pill" href="#uploadDocument" role="button" aria-haspopup="true" aria-expanded="false">

                                             Upload Document  

                                             </a>

                                          </li>

                                          <li class="nav-item dropdown">

                                             <a class="nav-link" data-toggle="pill" href="#aadharverify" role="button" aria-expanded="false">

                                             Aadhar Card 

                                             </a>

                                          </li>

                                          <li class="nav-item mr-1">

                                             <a class="nav-link"  data-toggle="pill" href="#other" aria-expanded="false">Other Document</a>

                                          </li>

                                          <li class="nav-item">

                                             <div id="reload_icon"></div>

                                          </li>

                                       </ul>

                                       <hr>

                                       <div class="tab-content">

                                          <div class="tab-pane active" id="uploadDocument" role="tabpanel" aria-labelledby="uploadDocument-tab" aria-expanded="false">

                                             <form  method="POST"  action="{{route('verify-document')}}" enctype="multipart/form-data">

                                                {{csrf_field()}}

                                                <div class="form-body">

                                                   <div class="row">

                                                      <div class="col-12">

                                                         <div class="form-group">

                                                            <?php if($retailer->profile_status == 1): ?>

                                                               <h4 style="color: green;">Document Uploaded Successfully.</h4> 

                                                               <p>Please, Verify your Aadhar Card if it is linked with Mobile click <b>"Aadhar Card"</b>. If not, click <b>"Other Document"</b>.</p>

                                                            <?php elseif($retailer->profile_status == 0): ?>

                                                               <h4 style="color: green;">Please Upload Documents.</h4> 

                                                            <?php endif ?>

                                                         </div>

                                                      </div>

                                                      <div class="row">

                                                         <div class="col-md-6">

                                                            <div class="form-group">

                                                               <label for="ProfilePic">

                                                               Profile Image

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="file" class="form-control required" id="profile_pic" name="profile_pic">

                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="profile_pic" name="profile_pic" disabled="">

                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="aadharcard_image">

                                                               Aadhar Card Image

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="file" class="form-control required" id="aadhar_pic_front" name="aadhar_pic_front" >

                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="aadhar_pic_front" name="aadhar_pic_front" disabled="">

                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="pan_card_image">

                                                               Pan Card Image

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="file" class="form-control required" id="pan_pic" name="pan_pic" >

                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="pan_pic" name="pan_pic" disabled="">

                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="shop_image">

                                                               Shop Picture

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>



                                                               <input type="file" class="form-control required" id="shop_image" name="company_logo">

                                                               <?php else: ?>



                                                               <input type="file" class="form-control required" id="shop_image" name="company_logo" disabled="">

                                                               <?php endif ?>

                                                            </div>

                                                         </div>

                                                         <div class="d-none hide col-md-6">

                                                            <div class="form-group">

                                                               <label for="passbook_image">

                                                               Account Passbook/Cancel Cheque

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="file" class="form-control required" id="passbook_image" name="passbook_image" > 



                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="passbook_image" name="passbook_image" disabled=""> 



                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="marksheet_10">

                                                               10th Marksheet (Optional)

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>



                                                               <input type="file" class="form-control required" id="marksheet_10" name="marksheet_10">

                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="marksheet_10" name="marksheet_10" disabled="">



                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="marksheet_12">

                                                               12th Marksheet

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="file" class="form-control required" id="marksheet_12" name="marksheet_12" >



                                                               <?php else: ?>

                                                               <input type="file" class="form-control required" id="marksheet_12" name="marksheet_12" disabled="">



                                                               <?php endif ?>

                                                            </div>

                                                         </div>

                                                         <div class="col-md-6">

                                                            <div class="form-group">

                                                               <label for="company_name">

                                                               Shop/Office Name

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>



                                                               <input type="text" value="{{ $retailer->company_name }}" class="form-control required" id="company_name" name="company_name" >

                                                               <?php else: ?>

                                                               <input type="text" value="{{ $retailer->company_name }}" class="form-control required" id="company_name" name="company_name" disabled="">



                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="company_address">

                                                               Shop/Office Address

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="text" class="form-control required" id="company_address" name="company_address" >



                                                               <?php else: ?>

                                                               <input type="text" class="form-control required" id="company_address" name="company_address" disabled="">



                                                               <?php endif ?>

                                                            </div>

                                                            <div class="form-group">

                                                               <label for="company_pan">

                                                               Shop/Office Pan/GST (If Available)

                                                               </label>

                                                               <?php if($retailer->profile_status == 0): ?>

                                                               <input type="text" class="form-control required" id="company_pan" name="company_pan">



                                                               <?php else: ?>

                                                               <input type="text" class="form-control required" id="company_pan" name="company_pan" disabled="">



                                                               <?php endif ?>

                                                            </div>

                                                            

                                                            <input type="hidden" name="mobile" class="form-control" value="{{$retailer->mobile}}" id="mobile" >

                                                         </div>

                                                      </div>

                                                      <div class="col-12">

                                                         <?php if($retailer->profile_status == 0): ?>

                                                         <button type="submit"  class="btn btn-primary mr-1 mb-1">Upload</button>

                                                         <?php endif ?>

                                                      </div>

                                                   </div>

                                                </div>

                                             </form>

                                          </div>

                                          <div class="tab-pane " id="aadharverify" role="tabpanel" aria-labelledby="aadharverify-tab" aria-expanded="false">

                                             <form  method="POST"  action="{{route('verify-document')}}" enctype="multipart/form-data">

                                                {{csrf_field()}}

                                                <div class="form-body">

                                                   <div class="row">

                                                      <div class="col-12">

                                                         <div class="form-group">

                                                            <p>Verify your Aadhar Card if it is linked with Mobile.</p>

                                                            <p> If not, click <b>"Other Document"</b>.</p>

                                                         </div>

                                                      </div>

                                                      <input type="hidden" name="first_name" class="form-control" value="{{$retailer->first_name}}" id="first_name" >

                                                      <input type="hidden" name="last_name" class="form-control" value="{{$retailer->last_name}}" id="last_name" >

                                                      <input type="hidden" name="mobile" class="form-control" value="{{$retailer->mobile}}" id="mobile" >

                                                      <input type="hidden" name="user_id" class="form-control" value="{{$retailer->id}}" id="user_id" >

                                                      <div class="col-12">

                                                         <button type="button" id="verify_Aadhar_button" onclick="digioKYCregister()"  class="btn btn-primary mr-1 mb-1">Verify</button>

                                                      </div>

                                                   </div>

                                                </div>

                                             </form>

                                          </div>

                                          

                                          <div class="tab-pane" id="other" role="tabpanel" aria-labelledby="other-tab" aria-expanded="false">

                                             <form  method="POST"  action="{{route('verify-document')}}" enctype="multipart/form-data">

                                                {{csrf_field()}}

                                                <div class="form-body">

                                                   <div class="row">

                                                      <div class="col-12">

                                                         <div class="form-group">

                                                            <label for="contact-info-icon">Select Document</label>

                                                            <div class="position-relative has-icon-left">

                                                               <select name="id_card_type" class="form-control" id="id_card_type" onchange="idCardDetected()">

                                                                  <option value="">Select Option</option>

                                                                  <option value="DRIVING_LICENSE">Driving Licence</option>

                                                                  <option value="VOTER_ID">Voter ID</option>

                                                                  <option value="VEHICLE_RC">Registration Certificate (Vehicle RC)</option>

                                                               </select>

                                                               <div class="form-control-position">

                                                                  <i class="feather icon-file"></i>

                                                               </div>

                                                            </div>

                                                         </div>

                                                      </div>

                                                      <div class="col-12">

                                                         <div class="form-group">

                                                            <label for="contact-info-icon">Document ID</label>

                                                            <div class="position-relative has-icon-left">

                                                               <input type="text" name="id_card_no" id="id_card_no" class="form-control" placeholder="Enter Document ID" onkeyup="idCardDetected()">

                                                               <div class="form-control-position">

                                                                  <i class="feather icon-hash"></i>

                                                               </div>

                                                            </div>

                                                         </div>

                                                         <input type="hidden" class="form-control" name="dob" id="dob" value="{{$retailer->dob}}">

                                                      </div>

                                                      <div class="col-12">

                                                         <button type="button" id="verify_other_button" onclick="verifyByOther()" class="btn btn-primary mr-1 mb-1" disabled>Verify</button>

                                                      </div>

                                                   </div>

                                                </div>

                                             </form>

                                          </div>

                                       </div>

                                    </div>

                                 </div>

                                 <div class="modal fade" id="verifyOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">

                                       <div class="modal-content">

                                          <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">

                                             <h4 class="modal-title" style=" color: white;" id="myModalLabel">OTP Verification</h4>

                                             <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->

                                             <!-- <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload();" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->

                                          </div>

                                          <div class="modal-body" id="getCode" >

                                             <div id="invoice-template" class="card-body">

                                                <div id="invoice-company-details" class="row">

                                                   <div class="col-sm-4 col-12 text-left">

                                                      <div class="media">

                                                         <img src="{{asset('images/Damskart Logo.png')}}" style="width: 100px;">

                                                      </div>

                                                   </div>

                                                   <div class="col-sm-8 col-12 text-right">

                                                      <div class="invoice-details mt-0" id="send_otp_message">

                                                      </div>

                                                   </div>

                                                </div>

                                                <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-0">

                                                   <div class="row">

                                                      <div class="table-responsive col-12">

                                                         <table class="table table-borderless" style="margin-top: 1rem;">

                                                            <tbody>

                                                               <tr>

                                                                  <td><b>OTP</b></td>

                                                                  <td>

                                                                     <input type="number" class="form-control" id="otp" onkeyup="otpDetected()" placeholder="Enter OTP" required>

                                                                     <input type="hidden" class="form-control" value="{{$retailer->mobile}}" id="mobile">

                                                                  </td>

                                                               </tr>

                                                            </tbody>

                                                         </table>

                                                      </div>

                                                   </div>

                                                </div>

                                                <div id="verify_response_div">

                                                </div>

                                             </div>

                                          </div>

                                          <p id="countdown" class="text-center"></p>

                                          <div class="modal-footer" style="justify-content: left;">

                                             <button type="button" id="verifyOTPbutton" onclick="verifyOTPbutton()" class="btn btn-primary" disabled="">Verify OTP</button>

                                             <button type="button" id="resendOTPbutton" onclick="resendOTPbutton()" class="btn btn-outline-primary" >Resend OTP</button>

                                          </div>

                                       </div>

                                    </div>

                                 </div>

                              </div>

                           </div>

                        </div>

                     </div>

                  </div>

               </section>

               <div class="divWaiting" id="Dv_Loader" style="display: none;">

                  <button class="btn btn-primary mb-1 round" type="button" >

                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>

                  &nbsp;Loading... 

                  </button>

               </div>

            </div>

         </div>

      </div>

      <!-- END: Content-->

      <!-- BEGIN: Vendor JS-->

      <script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>

      <script src="{{ asset('admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>

      <script src="https://app.digio.in/sdk/v9/digio.js"></script>

      <!-- END: Page JS-->

      <script type="text/javascript">

         <?php if($retailer->profile_status == 1 && $retailer->digio_kyc_status == 1 && $retailer->verify == 0): ?>

            $('#verifyOTPModal').modal({backdrop: 'static', keyboard: false});

         <?php endif ?>

      </script>

      <script type="text/javascript">

         $.ajaxSetup({

           headers: {

               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

           }

         });

      </script>

      <!-- idCardDetected -->

      <script type="text/javascript">

         function idCardDetected(){

           var id_card_type = $('#id_card_type').val();

           var id_card_no = $('#id_card_no').val();

               if (id_card_type.length > 0 && id_card_no.length > 0) {

                 $('#verify_other_button').removeAttr('disabled');

               } else {

                 $('#verify_other_button').attr('disabled', 'disabled');

               }

         }

      </script>

      <!-- otpDetected -->

      <script type="text/javascript">

         function otpDetected(){

             var otp = $("#otp").val();

               if (otp.length == 6) {

                 $('#verifyOTPbutton').removeAttr('disabled'); 

               } else {

                 $('#verifyOTPbutton').attr('disabled', 'disabled');

               }

         }

      </script>

      <!-- digioKYCregister -->

      <script type="text/javascript">

         function digioKYCregister() {

            // $(document).ready(function(){

            //  $('#pan_number').change(function(){

               var SITEURL = '{{URL::to('')}}';

                var mobile = $('#mobile').val();

                var user_id = $('#user_id').val();

                var first_name = $('#first_name').val();

                var last_name = $('#last_name').val();

                Swal.fire({

                   title: 'Do you want to continue?',

                   type: 'warning',

                   showCancelButton: true,

                   confirmButtonColor: '#3085d6',

                   cancelButtonColor: '#d33',

                   confirmButtonText: 'Yes, Continue',

                   confirmButtonClass: 'btn btn-primary',

                   cancelButtonClass: 'btn btn-danger ml-1',

                   buttonsStyling: false,

                 }).then((data) => {

                  if (data.value) {

                    $('#Dv_Loader').show();

         

                    $.ajax({

                       type:"get",

                       url:"/digioKYCRegister", 

                       data : {'mobile':mobile,'user_id':user_id,'first_name':first_name,'last_name':last_name,},

                       dataType: "json",

                       success:function(data)

                       {   

                           $('#Dv_Loader').hide();

                           // alert(data.id);

                           // alert(data.customer_identifier);

                           // alert(data.access_token.id);

                           var request_id = data.id;

                           var identifier = data.customer_identifier;

                           var token_id = data.access_token.id;

                           var options = {

                              environment : "production",

                              callback : function (response){

                                 // if(response.hasOwnProperty("error_code"){

                                 //    return console.log("error occurred in process");

                                 // })

                                 console.log(response);

                                    if(!response.error_code){

                                       $('#Dv_Loader').show();

                                       $.ajax({

                                         type:"get",

                                         url:"/digioAadharStatusUpdate", 

                                         data : {'mobile':mobile,'user_id':user_id,'kyc_response':response,'mobile':mobile,},

                                         dataType: "json",

                                         success:function(res)

                                         {   

                                            $('#Dv_Loader').hide();

                                            $("#document_verify_response").empty();

                                            $("#document_verify_response").html('Successfully Verified.').css('color', 'green');

                                            $("#verify_other_button").hide();

                                            $("#verify_Aadhar_button").hide();

                                         }

                                       });



                                      if (data.verify == 0) {

                                          $("#send_otp_message").empty();

                                          $("#send_otp_message").append('<h5>A OTP(One Time Passcode) has been sent to '+mobile+'</h5><p>Please enter the OTP below to verify your Mobile.</p>');

                                          $('#verifyOTPModal').modal({backdrop: 'static', keyboard: false});

                                      }else{

                                            var timeleft = 5;

                                            var downloadTimer = setInterval(function(){

                                              if(timeleft <= 0){

                                                clearInterval(downloadTimer);

                                                $("#reload_icon").empty();

                                                $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">             <i class="spinner-border spinner-border-sm text-secondary"></i>         </div>         <span class="chip-text spinner-border spinner-border-sm text-secondary"></span>     </div> </div>');



                                              } else {

                                                $("#reload_icon").empty();

                                                $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">               <span class="chip-text spinner-border spinner-border-sm text-danger"></span>         </div>         <span class="chip-text">'+ timeleft +'</span>     </div> </div>');

                                              }

                                              timeleft -= 1;

                                            }, 900);

                                            setTimeout(function () {

                                               window.location.href = SITEURL + '/index';

                                            }, 5000); 

                                      }

                                    }else{

                                      $("#document_verify_response").empty();

                                      $("#document_verify_response").html(response.message).css('color', 'red');

                                    }

                              },

                              "logo" : "https://damskart.com/public//images/Damskart Logo.png",

                              // "redirect_url" : "https://damskart.com/",

                              "theme" : {

                              "primaryColor" : "#FF7600",

                              "secondaryColor" : "#000000"

                              }

                           }

         

                           var digio = new Digio(options);

                           digio.init();

                           digio.submit(request_id, identifier, token_id);

                           // digio.cancel();

                       }

                    });

                 } else if (data.dismiss === Swal.DismissReason.cancel) {

                  Swal.fire({

                      title: 'Cancelled',

                      type: 'error',

                      confirmButtonClass: 'btn btn-success',

                    })

                }

              })

               //   });

               // });

         }

      </script>

      <!-- verifyByOther -->

      <script type="text/javascript">

         function verifyByOther() {

                var SITEURL = '{{URL::to('')}}';

                var id_card_type = $('#id_card_type').val();

                var id_card_no = $('#id_card_no').val();

                var mobile = $('#mobile').val();

                var user_id = $('#user_id').val();

                var dob = $('#dob').val();

                Swal.fire({

                   title: 'Do you want to continue?',

                   type: 'warning',

                   showCancelButton: true,

                   confirmButtonColor: '#3085d6',

                   cancelButtonColor: '#d33',

                   confirmButtonText: 'Yes, Continue',

                   confirmButtonClass: 'btn btn-primary',

                   cancelButtonClass: 'btn btn-danger ml-1',

                   buttonsStyling: false,

                 }).then((data) => {

                  if (data.value) {

                    $('#Dv_Loader').show();

         

                    $.ajax({

                       type:"get",

                       url:"/digioDOCverify", 

                       data : {'id_card_type':id_card_type,'id_card_no':id_card_no,'dob':dob,'mobile':mobile,'user_id':user_id,},

                       dataType: "json",

                       success:function(data)

                       {   

                          $('#Dv_Loader').hide();

                          if(!data.code){

                             $("#document_verify_response").empty();

                             $("#document_verify_response").html('Successfully Verified.').css('color', 'green');

                             $("#verify_other_button").hide();

                             $("#verify_Aadhar_button").hide();

         

                             if (data.verify == '0') {

                                 $("#send_otp_message").empty();

                                 $("#send_otp_message").append('<h5>A OTP(One Time Passcode) has been sent to '+mobile+'</h5><p>Please enter the OTP below to verify your Mobile.</p>');

                                 $('#verifyOTPModal').modal({backdrop: 'static', keyboard: false});

                             }else{

                                   var timeleft = 5;

                                   var downloadTimer = setInterval(function(){

                                     if(timeleft <= 0){

                                       clearInterval(downloadTimer);

                                       $("#reload_icon").empty();

                                       $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">             <i class="spinner-border spinner-border-sm text-secondary"></i>         </div>         <span class="chip-text spinner-border spinner-border-sm text-secondary"></span>     </div> </div>');



                                     } else {

                                       $("#reload_icon").empty();

                                       $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">               <span class="chip-text spinner-border spinner-border-sm text-danger"></span>         </div>         <span class="chip-text">'+ timeleft +'</span>     </div> </div>');

                                     }

                                     timeleft -= 1;

                                   }, 900);

                                   setTimeout(function () {

                                      window.location.href = SITEURL + '/index';

                                   }, 5000); 

                             }

                          }else{

                             $("#document_verify_response").empty();

                             $("#document_verify_response").html(data.message).css('color', 'red');

                          }

                       }

                    });

                 } else if (data.dismiss === Swal.DismissReason.cancel) {

                  Swal.fire({

                      title: 'Cancelled',

                      type: 'error',

                      confirmButtonClass: 'btn btn-success',

                    })

                }

              })

         }

      </script>

      <!-- verifyOTPbutton -->

      <script type="text/javascript">

         function verifyOTPbutton() {

                var SITEURL = '{{URL::to('')}}';

                var otp = $('#otp').val();

                var mobile = $('#mobile').val();

                Swal.fire({

                   title: 'Do you want to continue?',

                   type: 'warning',

                   showCancelButton: true,

                   confirmButtonColor: '#3085d6',

                   cancelButtonColor: '#d33',

                   confirmButtonText: 'Yes, Continue',

                   confirmButtonClass: 'btn btn-primary',

                   cancelButtonClass: 'btn btn-danger ml-1',

                   buttonsStyling: false,

                 }).then((data) => {

                  if (data.value) {

                    $('#Dv_Loader').show();

         

                    $.ajax({

                       type:"get",

                       url:"/verifyOTP", 

                       data : {'otp':otp,'mobile':mobile,},

                       dataType: "json",

                       success:function(data)

                       {   

                          $('#Dv_Loader').hide();

                          if(data.code == '000'){

         

                             // $("#verifyOTPbutton").hide();

                             $('#resendOTPbutton').attr('disabled', 'disabled');

                             $('#verifyOTPbutton').attr('disabled', 'disabled');

                             $('#otp').attr('disabled', 'disabled');

         

                             $("#send_otp_message").empty();

                             $("#verify_response_div").empty();

         

                             $("#send_otp_message").html('<h5 style="color:green;font-size: x-large;">Successfully Verified.</h5>');

                             $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');

                             $("#verify_message").empty();

                             $("#verify_message").html(data.message);

         

                              var timeleft = 3;

                              var downloadTimer = setInterval(function(){

                                if(timeleft <= 0){

                                  clearInterval(downloadTimer);

                                  // document.getElementById("countdown").innerHTML = "Finished";

                                  $("#countdown").empty();

                                  $("#countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');



                                } else {

                                  // document.getElementById("countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";

                                  $("#countdown").empty();

                                  $("#countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');

                                }

                                timeleft -= 1;

                              }, 875);

                              setTimeout(function () {

                                 window.location.href = SITEURL + '/index';

                              }, 3000); 

                          }else{

                             $("#verify_response_div").empty();

                             $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');

                             $("#verify_message").empty();

                             $("#verify_message").html(data.message).css('color', 'red');

                          }

         

                          

         

                       }

                    });

                 } else if (data.dismiss === Swal.DismissReason.cancel) {

                  Swal.fire({

                      title: 'Cancelled',

                      type: 'error',

                      confirmButtonClass: 'btn btn-success',

                    })

                }

              })

         }

      </script>

      <!-- resendOTPbutton -->

      <script type="text/javascript">

         function resendOTPbutton(){

         // $(document).ready(function(){

          // $('#billbutton').click(function(){

             var mobile = $('#mobile').val();

              $('#Dv_Loader').show();

              $.ajax({

                 type:"get",

                 url:"/resendOTP", 

                 data : {'mobile':mobile,},

                 dataType: "json",

                 success:function(data)

                 {   

                    $('#Dv_Loader').hide();

                    if(data.code == '000'){

         

                       $("#verify_response_div").empty();

                       $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');

                       $("#verify_message").empty();

                       $("#verify_message").html(data.message).css('color', 'green');

         

                    }else{

                       $("#verify_response_div").empty();

                       $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');

                       $("#verify_message").empty();

                       $("#verify_message").html(data.message).css('color', 'red');

                    }

         

                 }

              });

              // });

            // });

         }

      </script>

      

   </body>

</html>