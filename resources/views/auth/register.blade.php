<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <!-- BEGIN: Head-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <title>Sign Up - Damskart Business</title>
      <link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">
      <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
      <!-- BEGIN: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">
      <!-- END: Vendor CSS-->
      <!-- BEGIN: Theme CSS-->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">
      <!-- BEGIN: Page CSS-->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/authentication.css')}}">
      <!-- END: Page CSS-->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/wizard.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">
      <style type="text/css">
         .divWaiting {
         position: fixed;
         background-color: #FAFAFA;
         z-index: 2147483647 !important;
         opacity: 0.8;
         overflow: hidden;
         text-align: center;
         top: 0;
         left: 0;
         height: 100%;
         width: 100%;
         padding-top: 20%;
         }
      </style>
   </head>
   <!-- END: Head-->
   <!-- BEGIN: Body-->
   <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
      <!-- BEGIN: Content-->
      <div class="app-content content">
         <div class="content-overlay"></div>
         <div class="header-navbar-shadow"></div>
         <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
               <section class="row flexbox-container">
                  <div class="col-xl-8 col-11 d-flex11 justify-content-center pt-2" style="padding-bottom: 1.5rem !important;">
                     <div class="card bg-authentication rounded-0 mb-0 pt-md-2">
                        <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                           <img src="{{ asset('admin/app-assets/images/pages/register.jpg')}}" alt="branding logo">
                        </div>
                        <div class="row m-0">
                           <div class="col-lg-12 col-12 p-0">
                              <div class="card rounded-1 mb-0 px-1 pt-1">
                                 <div class="card-header pb-1">
                                    <div class="card-title">
                                       <h4 class="mb-0">Register</h4>
                                    </div>
                                 </div>
                                 <p class="px-2">Welcome, please Register to your account.</p>
                                 @if ($errors->any())
                                 <div  class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <ul>
                                       @foreach ($errors->all() as $error)
                                       <li>{{ $error }}</li>
                                       @endforeach
                                    </ul>
                                 </div>
                                 @endif
                                 @if ($message = Session::get('info'))
                                 <div class="alert alert-primary alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <strong>{{ $message }}</strong>
                                 </div>
                                 @endif
                                 @if ($message = Session::get('danger'))
                                 <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <strong>{{ $message }}</strong>
                                 </div>
                                 @endif 
                                 <div class="card-content">
                                    <div class="card-body pt-1">
                                       <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                          @csrf
                                          <!-- Step 1 -->
                                          <h6><i class="step-icon feather icon-user"></i> User's Details</h6>
                                          <fieldset>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="firstName3">
                                                      First Name
                                                      </label>
                                                      <input type="text" class="form-control required" id="first_name" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}" required="">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="lastName3">
                                                      Last Name
                                                      </label>
                                                      <input type="text" class="form-control required" id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Name" required="">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="emailAddress5">
                                                      Email
                                                      </label>
                                                      <input type="email" class="form-control required" id="email" name="email"  value="{{ old('email') }}" placeholder="Enter E-mail" required="">
                                                   </div>


                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="mobile">
                                                      Mobile
                                                      </label>
                                                      <input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Enter Mobile"  pattern="[6789][0-9]{9}" required="">
                                                   </div>
                                                </div>
                                             </div>

                                             {{--<div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label class="d-none " for="password">
                                                      Password
                                                      </label>
                                                      <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*?[#?!@$%^&*-]).{8,}" name="password" class="d-none form-control" placeholder="Your Password" id="password" onkeyup="passwordDetected()" required data-validation-required-message="The password field is required" >
                                                      <span style="visibility: hidden;">(The password must contain - Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.)</span>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label class="d-none" for="passwordconfirm">
                                                      Confirm Password
                                                      </label>
                                                      <input type="password" name="password_confirmation" class="d-none form-control" placeholder="Confirm Password" id="password-confirm" onkeyup="passwordDetected()" required data-validation-match-match="password" data-validation-required-message="The Confirm password field is required" >
                                                   </div>
                                                   <span id='password_response'></span>
                                                </div>
                                             </div>--}}

                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="DOB">
                                                      Date of Birth
                                                      </label>
                                                      <input type="date" class="form-control required" id="dob" name="dob"> 
                                                   </div>
                                                   <div class="d-none hide form-group">
                                                      <label for="loginpin">
                                                      Login Pin
                                                      </label>
                                                      <input type="number" class="form-control" id="loginpin" onKeyPress="if(this.value.length==4) return false;" onKeyDown="if(this.value.length==4 && event.keyCode!=8) return false;" min="1000" max="9999" value="{{ old('pin') }}" name="pin" placeholder="Enter Login PIN">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group d-flex align-items-center pt-md-2">
                                                      <label class="mr-2">Gender :</label>
                                                      <div class="c-inputs-stacked">
                                                         <div class="d-inline-block mr-2">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                               <input type="radio" name="gender" value="male">
                                                               <span class="vs-checkbox">
                                                               <span class="vs-checkbox--check">
                                                               <i class="vs-icon feather icon-check"></i>
                                                               </span>
                                                               </span>
                                                               <span class="">Male</span>
                                                            </div>
                                                         </div>
                                                         <div class="d-inline-block">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                               <input type="radio" name="gender" value="female">
                                                               <span class="vs-checkbox">
                                                               <span class="vs-checkbox--check">
                                                               <i class="vs-icon feather icon-check"></i>
                                                               </span>
                                                               </span>
                                                               <span class="">Female</span>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </fieldset>
                                          <!-- Step 3 -->
                                          <h6><i class="step-icon feather icon-file"></i> Documents</h6>
                                          <fieldset>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="aadharcard">
                                                      Aadhar Card
                                                      </label>
                                                      <input type="text" class="form-control required" id="aadhar_number" name="aadhar_number" pattern="^\d{12}$" maxlength="12" size="12" value="{{ old('aadhar_number') }}" placeholder="Enter Aadhar Card Number" required="">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="pancard">
                                                      Pan Card
                                                      </label>
                                                      <input type="text" class="form-control required" id="pan_number" onchange="digioPanVerify()" name="pan_number" maxlength="10" value="{{ old('pan_number') }}" pattern="^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$" placeholder="Enter Pan Card Number" required="">
                                                      <span id="pancard_response"></span>
                                                   </div>
                                                </div>
                                                <!-- <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="ProfilePic">
                                                      Profile Image
                                                      </label>
                                                      <input type="file" class="form-control required" id="profile_pic" name="profile_pic">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="passbook_image">
                                                      Account Passbook/Cancel Cheque
                                                      </label>
                                                      <input type="file" class="form-control required" id="passbook_image" name="passbook_image" required=""> 
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="aadharcard_image">
                                                      Aadhar Card Image
                                                      </label>
                                                      <input type="file" class="form-control required" id="aadhar_pic_front" name="aadhar_pic_front" required="">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="pan_card_image">
                                                      Pan Card Image
                                                      </label>
                                                      <input type="file" class="form-control required" id="pan_pic" name="pan_pic" required="">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="marksheet_10">
                                                      10th Marksheet (Optional)
                                                      </label>
                                                      <input type="file" class="form-control required" id="marksheet_10" name="marksheet_10">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="marksheet_12">
                                                      12th Marksheet
                                                      </label>
                                                      <input type="file" class="form-control required" id="marksheet_12" name="marksheet_12" required="">
                                                   </div>
                                                </div> -->
                                             </div>
                                          </fieldset>

                                          <h6><i class="step-icon feather icon-file"></i> Refer</h6>
                                          <fieldset>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="aadharcard">
                                                      Refer Mobile
                                                      </label>
                                                      <input type="text" class="form-control" id="refer_phone" onchange="getUserName()" name="refer_phone" maxlength="10"
                                                       size="10" value="{{ old('refer_phone') }}" placeholder="Enter Refer Phone">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="aadharcard">
                                                      Refer Name
                                                      </label>
                                                      <input type="text" class="form-control" readonly="" id="refer_name" name="refer_name" value="{{ old('refer_name') }}" placeholder="Enter Refer Name" />
                                                       <input type="hidden" class="form-control" id="refer_userid" name="refer_userid" value="{{ old('refer_userid') }}" placeholder="Enter Refer ID" />
                                                   </div>
                                                </div>
                                             </div>
                                          </fieldset>

                                          <!-- Step 2 -->
                                          <h6><i class="step-icon feather icon-map-pin"></i> Address & DOB</h6>
                                          <fieldset>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="state">
                                                      State
                                                      </label>
                                                      <select class="custom-select form-control" id="state" name="state_id" required="">
                                                         <option value="">Select State</option>
                                                         @foreach ($states as $state)
                                                         <option value="{{$state->id}}">
                                                            {{$state->state_name}}
                                                         </option>
                                                         @endforeach
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="district">
                                                      District
                                                      </label>
                                                      <select class="custom-select form-control"  id="city" name="district_id" ></select>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="pincode">
                                                      Pin Code
                                                      </label>
                                                      <input type="text" class="form-control required" pattern="^\d{6}$" maxlength="6" size="6" id="pin_code" name="pin_code" placeholder="Pin Code">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="company_name">
                                                      Shop/Office/Business Name
                                                      </label>
                                                      <input type="text" class="form-control required" id="company_name" name="company_name" required="">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="address">Address</label>
                                                      <textarea name="address" id="address" rows="4" class="form-control required" required></textarea>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                          </fieldset>
                                          
                                          <!-- Step 4 -->
                                          <!-- <h6><i class="step-icon feather icon-shopping-cart"></i> Shop Details</h6>
                                          <fieldset>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="company_name">
                                                      Shop/Office Name
                                                      </label>
                                                      <input type="text" class="form-control required" id="company_name" name="company_name" required="">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="company_address">
                                                      Shop/Office Address
                                                      </label>
                                                      <input type="text" class="form-control required" id="company_address" name="company_address" required="">
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <label for="company_pan">
                                                      Shop/Office Pan/GST (If Available)
                                                      </label>
                                                      <input type="text" class="form-control required" id="company_pan" name="company_pan">
                                                   </div>
                                                   <div class="form-group">
                                                      <label for="shop_image">
                                                      Shop Picture
                                                      </label>
                                                      <input type="file" class="form-control required" id="shop_image" name="company_logo">
                                                   </div>
                                                </div>
                                             </div>
                                          </fieldset> -->
                                          <button class="btn btn-primary float-right btn-inline waves-effect waves-light" id="register_button" type="submit" disabled>Register</button>
                                       </form>
                                       <a href="{{route('login')}}" class="btn btn-outline-primary float-left btn-inline">Back To Login</a>
                                    </div>
                                 </div>
                                <!--  <div class="login-footer">
                                    <div class="divider">
                                       <div class="divider-text">OR</div>
                                    </div>
                                    <div class="footer-btn d-inline">
                                       <a href="#" class="btn btn-facebook"><span class="fa fa-facebook"></span></a>
                                       <a href="#" class="btn btn-twitter white"><span class="fa fa-twitter"></span></a>
                                       <a href="#" class="btn btn-google"><span class="fa fa-google"></span></a>
                                       <a href="#" class="btn btn-github"><span class="fa fa-github-alt"></span></a>
                                    </div>
                                 </div> -->
                                 <br> 
                                 <br> 

                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <div class="divWaiting" id="Dv_Loader" style="display: none;">
                  <button class="btn btn-primary mb-1 round" type="button" >
                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
                  &nbsp;Loading... 
                  </button>
               </div>
            </div>
         </div>
      </div>
      <!-- END: Content-->
      <!-- BEGIN: Vendor JS-->
      <script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>
      <!-- BEGIN Vendor JS-->
      <!-- BEGIN: Page Vendor JS-->
      <!-- END: Page Vendor JS-->
      <!-- BEGIN: Theme JS-->
      <script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>
      <script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>
      <script src="{{ asset('admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
      <script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>
      <script src="{{ asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
      <script src="{{ asset('admin/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
      <!-- <script src="{{ asset('admin/app-assets/js/scripts/forms/wizard-steps.js')}}"></script> -->
      <!-- END: Theme JS-->
      <!-- BEGIN: Page JS-->
      <!-- END: Page JS-->
      <script type="text/javascript">
         $(document).ready(function(){
          $('#state').change(function(){
            var sid = $(this).val();
            // alert(sid);
            if(sid){
              $.ajax({
                 type:"get",
                 url:"/getCities/"+sid, //Please see the note at the end of the post**
                 success:function(res)
                 {
                      if(res)
                      {
                          $("#city").empty();
                          $("#city").append('<option class="form-label" value="">Select District</option>');
                          $.each(res,function(key,value){
                              $("#city").append('<option value="'+key+'">'+value+'</option>');
                          });
                      }
                 }
              });
            }
          });

          var dtToday = new Date();
    
        var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
        var day = dtToday.getDate();
        var year = dtToday.getFullYear() - 18;
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
    	var minDate = year + '-' + month + '-' + day;
        var maxDate = year + '-' + month + '-' + day;
    	$('#dob').attr('max', maxDate);

         });
      </script>
      <script type="text/javascript">
      	 function getUserName()
         {
            var refer_phone = $('#refer_phone').val();

            $.ajax({
				type:"get",
				url:"/getUserInfo", 
				data : {'refer_phone':refer_phone},
				dataType: "json",
				success:function(data)
				{
					if(data.full_name)
					{
						$("#refer_name").val(data.full_name);
						$("#refer_userid").val(data.user_id);	
					}
					
				}
            });
         }

         function digioPanVerify() {
            // $(document).ready(function(){
            //  $('#pan_number').change(function(){
                var pan_number = $('#pan_number').val();
                var dob = $('#dob').val();
                var first_name = $('#first_name').val();
                var last_name = $('#last_name').val();
                
                 $('#Dv_Loader').show();
                 $.ajax({
                    type:"get",
                    url:"/digioPanVerify", 
                    data : {'pan_number':pan_number,'dob':dob,'first_name':first_name,'last_name':last_name,},
                    dataType: "json",
                    success:function(data)
                    {   
                        $('#Dv_Loader').hide();
                        if(data.is_pan_dob_valid == true && data.name_matched == true){
                           $("#pancard_response").empty();
                           $("#pancard_response").append('<p style="color:green;">Successfully Verified.</p>');
                           $('#register_button').removeAttr('disabled');
                           // $('#pan_number').attr('readonly', 'readonly');   
                           $('#dob').attr('readonly', 'readonly');   
                           $('#first_name').attr('readonly', 'readonly');   
                           $('#last_name').attr('readonly', 'readonly');   
                        }else if(data.is_pan_dob_valid == true && data.name_matched == false){
                           $("#pancard_response").empty();
                           $("#pancard_response").append('<p style="color:red;">Invalid Name.</p>');
                           $('#register_button').attr('disabled', 'disabled');  

                           $('#dob').removeAttr('readonly');   
                           $('#first_name').removeAttr('readonly');   
                           $('#last_name').removeAttr('readonly');   
                        }else if(data.is_pan_dob_valid == false && data.name_matched == true){
                           $("#pancard_response").empty();
                           $("#pancard_response").append('<p style="color:red;">Invalid Date of Birth.</p>');
                           $('#register_button').attr('disabled', 'disabled');   
                           $('#dob').removeAttr('readonly');   
                           $('#first_name').removeAttr('readonly');   
                           $('#last_name').removeAttr('readonly');   
                        }else if(data.error_message){
                           $("#pancard_response").empty();
                           $('#dob').removeAttr('readonly');   
                           $('#first_name').removeAttr('readonly');   
                           $('#last_name').removeAttr('readonly');   
                           $("#pancard_response").append('<p style="color:red;">'+data.error_message+'</p>');
                           $('#register_button').attr('disabled', 'disabled');   
                        }else{
                           $("#pancard_response").empty();
                           $('#dob').removeAttr('readonly');   
                           $('#first_name').removeAttr('readonly');   
                           $('#last_name').removeAttr('readonly');   
                           $("#pancard_response").append('<p style="color:red;">'+data.message+'</p>');
                           $('#register_button').attr('disabled', 'disabled');   
                        }
                    }
                 });
               //   });
               // });
         }
      </script>
      <script type="text/javascript">
        function passwordDetected(){
          password = $('#password').val()
          passwordconfirm = $('#password-confirm').val()
          if ((password.length > 0) && (passwordconfirm.length > 0)) {
               if ($('#password').val() == $('#password-confirm').val()) {
                  $('#password_response').html('Password Matched.').css('color', 'green');
                  $('#register_button').removeAttr('disabled');
               }else{
                  $('#password_response').html('Password does not match!' ).css('color', 'red');
                  $('#register_button').attr('disabled', 'disabled');   
               } 
          }else{
               $('#register_button').attr('disabled', 'disabled');
          }
        }
      </script>
   </body>
   <!-- END: Body-->
</html>