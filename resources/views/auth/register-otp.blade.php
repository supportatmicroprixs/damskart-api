<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

   <!-- BEGIN: Head-->

   <head>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

      <title>OTP - Damskart Business</title>

      <link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">

      <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">

      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

      <!-- BEGIN: Vendor CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">

      <!-- END: Vendor CSS-->

      <!-- BEGIN: Theme CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">

      <!-- BEGIN: Page CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/authentication.css')}}">

      <!-- END: Page CSS-->

      <!-- BEGIN: Custom CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">

      <!-- END: Custom CSS-->

   </head>

   <!-- END: Head-->

   <!-- BEGIN: Body-->

   <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

      <!-- BEGIN: Content-->

      <div class="app-content content">

         <div class="content-overlay"></div>

         <div class="header-navbar-shadow"></div>

         <div class="content-wrapper">

            <div class="content-header row">

            </div>

            <div class="content-body">

               @if ($errors->any())

               <div  class="alert alert-danger alert-block">

                  <button type="button" class="close" data-dismiss="alert">×</button> 

                  <ul>

                     @foreach ($errors->all() as $error)

                     <li>{{ $error }}</li>

                     @endforeach

                  </ul>

               </div>

               @endif

               @if ($message = Session::get('info'))

               <div class="alert alert-primary alert-block">

                  <button type="button" class="close" data-dismiss="alert">×</button> 

                  <strong>{{ $message }}</strong>

               </div>

               @endif

               @if ($message = Session::get('danger'))

               <div class="alert alert-danger alert-block">

                  <button type="button" class="close" data-dismiss="alert">×</button> 

                  <strong>{{ $message }}</strong>

               </div>

               @endif

               <section class="row flexbox-container">

                  <div class="col-xl-7 col-md-9 col-10 d-flex justify-content-center px-0">

                     <div class="card bg-authentication rounded-0 mb-0">

                        <div class="row m-0">

                           <div class="col-lg-6 d-lg-block d-none text-center align-self-center">

                              <img src="{{ asset('admin/app-assets/images/pages/forgot-password.png')}}" alt="branding logo">

                           </div>

                           <div class="col-lg-6 col-12 p-0">

                              <div class="card rounded-0 mb-0 px-2 py-1">

                                 <div class="card-header pb-1">

                                    <div class="card-title">

                                       <h4 class="mb-0">Verify Your OTP</h4>

                                    </div>

                                 </div>

                                 <p class="px-2 mb-0">Please enter OTP.</p>

                                 <div class="card-content">

                                    <div class="card-body">

                                       <form  method="POST"  action="{{route('rotp')}}" enctype="multipart/form-data">

                                          @csrf

                                          <div class="form-label-group">

                                             <input type="text" id="inputOTP" class="form-control" placeholder="Enter OTP" name="otp">

                                             <label for="inputEmail">OTP</label>

                                          </div>

                                          <input id="" type="hidden" class="form-control" value="{{$retailer->mobile}}" name="mobile" >

                                          <div class="d-block mb-1">

                                             <button class="btn btn-primary btn-block px-75">Verify OTP</button>

                                          </div>

                                          <div class="d-block mb-1">

                                             <a class="btn btn-outline-primary btn-block px-75" href="{{ route('logout') }}"

                                                onclick="event.preventDefault();

                                                document.getElementById('logout-form1').submit();">

                                             Back to Login</a>

                                          </div>

                                       </form>

                                       <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">

                                          @csrf

                                       </form>

                                    </div>

                                 </div>

                              </div>

                           </div>

                        </div>

                     </div>

                  </div>

            </div>

            </section>

         </div>

      </div>

      </div>

      <!-- END: Content-->

      <!-- BEGIN: Vendor JS-->

      <script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>

      <!-- BEGIN Vendor JS-->

      <!-- BEGIN: Page Vendor JS-->

      <!-- END: Page Vendor JS-->

      <!-- BEGIN: Theme JS-->

      <script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>

      <!-- END: Theme JS-->

      <!-- BEGIN: Page JS-->

      <!-- END: Page JS-->

   </body>

   <!-- END: Body-->

</html>