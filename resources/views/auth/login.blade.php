<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login - Damskart Business</title>
    <link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/authentication.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/plugins/forms/wizard.css')}}">
    <style type="text/css">
         .divWaiting {
         position: fixed;
         background-color: #FAFAFA;
         z-index: 2147483647 !important;
         opacity: 0.8;
         overflow: hidden;
         text-align: center;
         top: 0;
         left: 0;
         height: 100%;
         width: 100%;
         padding-top: 20%;
         }
      </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-11 d-flex justify-content-center">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                    <img src="{{ asset('admin/app-assets/images/pages/login.png')}}" alt="branding logo">
                                </div>
                                <div class="col-lg-6 col-12 p-0">
                                    <div class="card rounded-0 mb-0 px-2">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
                                                <h4 class="mb-0">Login</h4>
                                                <!-- <button type="button" class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#changePasswordModal">changePasswordModal</button> -->
                                            </div>
                                            <div id="reload_icon"></div>
                                            
                                        </div>
                                        <p class="px-2">Welcome back, please login to your account.</p>
                                        <p class="px-2" id="error_message"></p>
                                          @if ($errors->any())
                                           <div  class="alert alert-danger alert-block">
                                              <ul>
                                                 @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                                 @endforeach
                                              </ul>
                                           </div>
                                           @endif
                                           @if ($message = Session::get('info'))
                                           <div class="alert alert-login-info alert-block">
                                              <strong>{{ $message }}</strong>
                                           </div>
                                           @endif 
                                           @if ($message = Session::get('danger'))
                                           <div class="alert alert-danger alert-block">
                                              <strong>{{ $message }}</strong>
                                           </div>
                                           @endif 
                                        <div class="card-content">
                                           
                                            <div class="card-body pt-1">
                                                <form method="POST" action="{{ route('login') }}">
                                                    @csrf
                                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                                        <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile Number" required autocomplete="mobile" autofocus>
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user"></i>
                                                        </div>
                                                        <label for="user-name">Mobile Number</label>
                                                    </fieldset>

                                                    <fieldset class="form-label-group position-relative has-icon-left">
                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        <div class="form-control-position">
                                                            <i class="feather icon-lock"></i>
                                                        </div>
                                                        <label for="user-password">Password</label>
                                                    </fieldset>
                                                    <div class="form-group d-flex justify-content-between align-items-center">
                                                        <div class="text-right">
                                                          <!-- <a href="{{ route('forgot-password') }}" class="card-link">Forgot Password?</a> -->
                                                          <a href="#!" onclick="forgotPasswordButton()" class="card-link">Forgot Password?</a>
                                                        </div>  
                                                    </div>
                                                    <a href="{{ route('register') }}" class="btn btn-outline-primary float-left btn-inline">Register</a>
                                                    <button type="button" id="login_button" onclick="loginButton()"  class="btn btn-primary float-right btn-inline">Login</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="verifyOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                                               <div class="modal-content">
                                                  <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                                     <h4 class="modal-title" style=" color: white;" id="myModalLabel">OTP Verification</h4>
                                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  </div>
                                                  <div class="modal-body" id="getCode" >
                                                     <div id="invoice-template" class="card-body">
                                                        <div id="invoice-company-details" class="row">
                                                           <div class="col-sm-4 col-12 text-left">
                                                              <div class="media">
                                                                 <img src="{{asset('images/Damskart Logo.png')}}" style="width: 100px;">
                                                              </div>
                                                           </div>
                                                           <div class="col-sm-8 col-12 text-right">
                                                              <div class="invoice-details mt-0" id="send_otp_message">
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-0">
                                                           <div class="row">
                                                              <div class="table-responsive col-12">
                                                                 <table class="table table-borderless" style="margin-top: 1rem;">
                                                                    <tbody>
                                                                       <tr>
                                                                          <td><b>OTP</b></td>
                                                                          <td>
                                                                             <input type="number" class="form-control" id="otp" onkeyup="otpDetected()" placeholder="Enter OTP" required>
                                                                             <input type="hidden" class="form-control" value="" id="customer_mobile">
                                                                          </td>
                                                                       </tr>
                                                                    </tbody>
                                                                 </table>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="verify_response_div">
                                                        </div>
                                                     </div>
                                                  </div>
                                                  <p id="countdown" class="text-center"></p>
                                                  <div class="modal-footer" style="justify-content: left;">
                                                     <button type="submit" id="verifyOTPbutton" onclick="verifyOTPbutton()" class="btn btn-primary" disabled="">Verify OTP</button>
                                                     <button type="submit" id="resendOTPbutton" onclick="resendOTPbutton()" class="btn btn-outline-primary" >Resend OTP</button>
                                                  </div>
                                               </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="sendForgotOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                                               <div class="modal-content">
                                                  <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                                     <h4 class="modal-title" style=" color: white;" id="myModalLabel">Forgot Password</h4>
                                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  </div>
                                                  <div class="modal-body" id="getCode" >
                                                     <div id="invoice-template" class="card-body">
                                                        <div id="invoice-company-details" class="row">
                                                           <div class="col-sm-4 col-12 text-left">
                                                              <div class="media">
                                                                 <img src="{{asset('images/Damskart Logo.png')}}" style="width: 100px;">
                                                              </div>
                                                           </div>
                                                           <div class="col-sm-8 col-12 text-right">
                                                              <div class="invoice-details mt-0" id="send_forgot_otp_message">
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-0">
                                                           <div class="row">
                                                              <div class="table-responsive col-12">
                                                                 <table class="table table-borderless" style="margin-top: 1rem;">
                                                                    <tbody>
                                                                       <tr>
                                                                          <td><b>Mobile</b></td>
                                                                          <td>
                                                                             <input type="number" class="form-control" id="forgot_mobile" onkeyup="otpDetected()" placeholder="Enter Registered Mobile" required>
                                                                          </td>
                                                                       </tr>
                                                                       <tr id="forgot_input_div">
                                                                         
                                                                       </tr>
                                                                    </tbody>
                                                                 </table>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="forgot_verify_response_div">
                                                        </div>
                                                     </div>
                                                  </div>
                                                  <p id="countdown" class="text-center"></p>
                                                  <div class="modal-footer" style="justify-content: left;">
                                                     <button type="button" id="forgot_GetOTPbutton" onclick="forgotGetOTPbutton()" class="btn btn-primary" >Get OTP</button>
                                                     <div id="forgot_otp_verify_button"></div>
                                                     <button type="button" id="forgotresendOTPbutton" onclick="forgotResendOTPbutton()" class="btn btn-outline-primary" >Resend OTP</button>
                                                  </div>
                                               </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 35%;">
                                               <div class="modal-content">
                                                  <div class="modal-header" style="background: linear-gradient(to right, #7367F0, #968df4);">
                                                     <h4 class="modal-title" style=" color: white;" id="myModalLabel">Change Password</h4>
                                                     <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                                  </div>
                                                  <div class="modal-body" id="getCode" >
                                                     <div id="invoice-template" class="card-body">
                                                        <div id="invoice-company-details" class="row">
                                                           <div class="col-sm-4 col-12 text-left">
                                                              <div class="media">
                                                                 <img src="{{asset('images/Damskart Logo.png')}}" style="width: 100px;">
                                                              </div>
                                                           </div>
                                                           <div class="col-sm-8 col-12 text-right">
                                                              <div class="invoice-details mt-0" id="change_pass_message">
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="invoice-items-details" style="margin-bottom: 0rem;" class=" card bg-transparent shadow-1 mt-0">
                                                           <div class="row">
                                                              <div class="table-responsive col-12">
                                                                 <table class="table table-borderless" style="margin-top: 1rem;">
                                                                    <tbody>
                                                                       <tr>
                                                                          <td><b>Password</b></td>
                                                                          <td>
                                                                             <input type="text" class="form-control" id="user_password" name="password" placeholder="Enter New Password" required>
                                                                             <input type="hidden" name="user_id" id="change_user_id">
                                                                             <input type="hidden" name="user_mobile" id="change_user_mobile">
                                                                          </td>
                                                                       </tr>
                                                                       <tr>
                                                                          <td><b>Confirm Password</b></td>
                                                                          <td>
                                                                             <input type="text" class="form-control" id="confirm_password" name="confirm_password" placeholder="Re-enter Password" required>
                                                                          </td>
                                                                       </tr>
                                                                    </tbody>
                                                                 </table>
                                                              </div>
                                                           </div>
                                                        </div>
                                                        <div id="change_pass_response_div">
                                                        </div>
                                                     </div>
                                                  </div>
                                                  <p id="change_countdown" class="text-center"></p>
                                                  <div class="modal-footer" style="justify-content: left;">
                                                     <button type="button" id="changePassbutton" onclick="changePasswordButton()" class="btn btn-primary" >Update</button>
                                                     <a href="{{route('login')}}" class="btn btn-outline-warning">Back to Login</a>
                                                  </div>
                                               </div>
                                            </div>
                                        </div>
                                        
                                        <div class="login-footer">
                                            <!-- <div class="divider">
                                                <div class="divider-text">OR</div>
                                            </div>
                                            <div class="footer-btn d-inline">
                                                <a href="#" class="btn btn-facebook"><span class="fa fa-facebook"></span></a>
                                                <a href="#" class="btn btn-twitter white"><span class="fa fa-twitter"></span></a>
                                                <a href="#" class="btn btn-google"><span class="fa fa-google"></span></a>
                                                <a href="#" class="btn btn-github"><span class="fa fa-github-alt"></span></a>
                                            </div> -->
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="divWaiting" id="Dv_Loader" style="display: none;">
                  <button class="btn btn-primary mb-1 round" type="button" >
                  <span class="spinner-border spinner-border-sm text-secondary" role="status" aria-hidden="true"></span>
                  &nbsp;Loading... 
                  </button>
               </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->
    <script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>
    <script src="{{ asset('admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script type="text/javascript">
    </script>
    <!-- END: Theme JS-->
    <!-- otpDetected -->
    <script type="text/javascript">
       function otpDetected(){
           var otp = $("#otp").val();
             if (otp.length == 6) {
               $('#verifyOTPbutton').removeAttr('disabled'); 
             } else {
               $('#verifyOTPbutton').attr('disabled', 'disabled');
             }
       }

       $('#password').bind('keypress', function(e) {
            var code = e.keyCode || e.which;
            if(code == 13) {
                loginButton();
            }
       });

     function loginButton(){
      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
     // $(document).ready(function(){
      // $('#billbutton').click(function(){
          var SITEURL = '{{URL::to('')}}';
         var mobile = $('#mobile').val();
         var password = $('#password').val();
          $('#Dv_Loader').show();
          $.ajax({
             type:"POST",
             url:"/login", 
             data : {'mobile':mobile,'password':password,},
             dataType: "json",
             success:function(data)
             {   
                  $('#Dv_Loader').hide();
                  if(data.code == '111'){
                      var u_mobile = data.mobile;
                      $('#customer_mobile').val(u_mobile);
                      $("#send_otp_message").empty();
                      $("#send_otp_message").append('<h5>'+data.message+'</h5><p>Please enter the OTP below to verify your Mobile.</p>');
                      $('#verifyOTPModal').modal({backdrop: 'static', keyboard: false}); 
                  }else{

                      $("#error_message").empty();
                      if (data.code == '000') {
                        $("#error_message").append(data.message).css('color', 'green');
                        window.location.href = SITEURL + '/index';
                      }else if (data.code == '333') {
                        $("#error_message").append(data.message).css('color', 'red');
                        window.location.href = SITEURL + '/index';
                      }else{
                        $("#error_message").append(data.message).css('color', 'red');
                        var timeleft = 5;
                        var downloadTimer = setInterval(function(){
                          if(timeleft <= 0){
                            clearInterval(downloadTimer);
                            $("#reload_icon").empty();
                            $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">             <i class="spinner-border spinner-border-sm text-secondary"></i>         </div>         <span class="chip-text spinner-border spinner-border-sm text-secondary"></span>     </div> </div>');

                          } else {
                            $("#reload_icon").empty();
                            $("#reload_icon").append('<div class="chip chip-danger mr-1">     <div class="chip-body">         <div class="avatar">               <span class="chip-text spinner-border spinner-border-sm text-danger"></span>         </div>         <span class="chip-text">'+ timeleft +'</span>     </div> </div>');
                          }
                          timeleft -= 1;
                        }, 900);
                        setTimeout(function () {
                           window.location.href = SITEURL + '/index';
                        }, 5000); 
                      }
                      

                  }
             }
          });
          // });
        // });
     }
  </script>
  <!-- verifyOTPbutton -->
  <script type="text/javascript">
     function verifyOTPbutton() {
            var SITEURL = '{{URL::to('')}}';
            var otp = $('#otp').val();
            var mobile = $('#mobile').val();
            Swal.fire({
               title: 'Do you want to continue?',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes, Continue',
               confirmButtonClass: 'btn btn-primary',
               cancelButtonClass: 'btn btn-danger ml-1',
               buttonsStyling: false,
             }).then((data) => {
              if (data.value) {
                $('#Dv_Loader').show();
     
                $.ajax({
                   type:"get",
                   url:"/verifyOTP", 
                   data : {'otp':otp,'mobile':mobile,},
                   dataType: "json",
                   success:function(data)
                   {   
                      $('#Dv_Loader').hide();
                      if(data.code == '000'){
     
                         // $("#verifyOTPbutton").hide();
                         $('#resendOTPbutton').attr('disabled', 'disabled');
                         $('#verifyOTPbutton').attr('disabled', 'disabled');
                         $('#otp').attr('disabled', 'disabled');
     
                         $("#send_otp_message").empty();
                         $("#verify_response_div").empty();
     
                         $("#send_otp_message").html('<h5 style="color:green;font-size: x-large;">Successfully Verified.</h5>');
                         $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');
                         $("#verify_message").empty();
                         $("#verify_message").html(data.message);
     
                          var timeleft = 3;
                          var downloadTimer = setInterval(function(){
                            if(timeleft <= 0){
                              clearInterval(downloadTimer);
                              document.getElementById("countdown").innerHTML = "Finished";
                            } else {
                              document.getElementById("countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";
                            }
                            timeleft -= 1;
                          }, 875);
                          setTimeout(function () {
                             window.location.href = SITEURL + '/index';
                          }, 3000); 
                      }else{
                         $("#verify_response_div").empty();
                         $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');
                         $("#verify_message").empty();
                         $("#verify_message").html(data.message).css('color', 'red');
                      }
     
                      
     
                   }
                });
             } else if (data.dismiss === Swal.DismissReason.cancel) {
              Swal.fire({
                  title: 'Cancelled',
                  type: 'error',
                  confirmButtonClass: 'btn btn-success',
                })
            }
          })
     }
  </script>
  <!-- resendOTPbutton -->
  <script type="text/javascript">
     function resendOTPbutton(){
     // $(document).ready(function(){
      // $('#billbutton').click(function(){
         var mobile = $('#mobile').val();
          $('#Dv_Loader').show();
          $.ajax({
             type:"get",
             url:"/resendOTP", 
             data : {'mobile':mobile,},
             dataType: "json",
             success:function(data)
             {   
                $('#Dv_Loader').hide();
                if(data.code == '000'){
     
                   $("#verify_response_div").empty();
                   $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');
                   $("#verify_message").empty();
                   $("#verify_message").html(data.message).css('color', 'green');
     
                }else{
                   $("#verify_response_div").empty();
                   $("#verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="verify_message"></h5></div>       </div></div>');
                   $("#verify_message").empty();
                   $("#verify_message").html(data.message).css('color', 'red');
                }
     
             }
          });
          // });
        // });
     }
  </script>
  <!-- forgotPasswordButton -->
  <script type="text/javascript">
     function forgotPasswordButton(){
          $("#send_forgot_otp_message").empty();
          $("#send_forgot_otp_message").append('<h4>Please enter the Registered Mobile Number below to get OTP.</h4>');
          $('#sendForgotOTPModal').modal({backdrop: 'static', keyboard: false}); 
     }
  </script>
  <!-- forgotGetOTPbutton -->
  <script type="text/javascript">
   function forgotGetOTPbutton() {
          var SITEURL = '{{URL::to('')}}';
          var mobile = $('#forgot_mobile').val();
          Swal.fire({
             title: 'Do you want to continue?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Continue',
             confirmButtonClass: 'btn btn-primary',
             cancelButtonClass: 'btn btn-danger ml-1',
             buttonsStyling: false,
           }).then((data) => {
            if (data.value) {
              $('#Dv_Loader').show();
   
              $.ajax({
                 type:"get",
                 url:"/getForgotOTP", 
                 data : {'mobile':mobile,},
                 dataType: "json",
                 success:function(data)
                 {   
                    $('#Dv_Loader').hide();
                    if(data.code == '000'){
                       $('#mobile').attr('disabled', 'disabled');
            
                       $("#forgot_GetOTPbutton").hide();
                       $("#send_forgot_otp_message").empty();
                       $("#forgot_verify_response_div").empty();
                       $("#forgot_input_div").empty();
   
                       $("#send_forgot_otp_message").html('<h5>A OTP(One Time Passcode) has been sent to '+mobile+'</h5><p>Please enter the OTP below to verify your Mobile.</p>');
                       $("#forgot_input_div").append('<td><b>OTP</b></td> <td>    <input type="number" class="form-control" id="forgot_otp"  placeholder="Enter OTP" required></td>');

                       $("#forgot_otp_verify_button").empty();
                       $("#forgot_otp_verify_button").append('<button type="button" onclick="verifyForgotOTPbutton()" class="btn btn-primary" >Verify OTP</button>');
                    }else{
                       $("#forgot_verify_response_div").empty();
                       $("#forgot_verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="forgot_verify_message"></h5></div>       </div></div>');
                       $("#forgot_verify_message").empty();
                       $("#forgot_verify_message").html(data.message).css('color', 'red');
                    }
                 }
              });
           } else if (data.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
                title: 'Cancelled',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
              })
          }
        })
   }
  </script>
  <!-- forgotResendOTPbutton -->
  <script type="text/javascript">
     function forgotResendOTPbutton(){
     // $(document).ready(function(){
      // $('#billbutton').click(function(){
         var mobile = $('#forgot_mobile').val();
          $('#Dv_Loader').show();
          $.ajax({
             type:"get",
             url:"/forgotResendOTP", 
             data : {'mobile':mobile,},
             dataType: "json",
             success:function(data)
             {   
                $('#Dv_Loader').hide();
                if(data.code == '000'){
     
                   $("#forgot_verify_response_div").empty();
                   $("#forgot_verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="forgot_verify_message"></h5></div>       </div></div>');
                   $("#forgot_verify_message").empty();
                   $("#forgot_verify_message").html(data.message).css('color', 'green');
     
                }else{
                   $("#forgot_verify_response_div").empty();
                   $("#forgot_verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="forgot_verify_message"></h5></div>       </div></div>');
                   $("#forgot_verify_message").empty();
                   $("#forgot_verify_message").html(data.message).css('color', 'red');
                }
     
             }
          });
          // });
        // });
     }
  </script>
  <!-- verifyForgotOTPbutton -->
  <script type="text/javascript">
     function verifyForgotOTPbutton() {
            var SITEURL = '{{URL::to('')}}';
            var otp = $('#forgot_otp').val();
            var mobile = $('#forgot_mobile').val();
            Swal.fire({
               title: 'Do you want to continue?',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes, Continue',
               confirmButtonClass: 'btn btn-primary',
               cancelButtonClass: 'btn btn-danger ml-1',
               buttonsStyling: false,
             }).then((data) => {
              if (data.value) {
                $('#Dv_Loader').show();
     
                $.ajax({
                   type:"get",
                   url:"/verifyForgotOTP", 
                   data : {'otp':otp,'mobile':mobile,},
                   dataType: "json",
                   success:function(data)
                   {   
                      $('#Dv_Loader').hide();
                      if(data.code == '000'){
                        var user_id = data.id;
                        var user_mobile = data.mobile;

                         $('#sendForgotOTPModal').modal('hide');
                         $("#change_pass_message").empty();
                         $("#forgot_verify_response_div").empty();
                         $("#change_pass_message").append('<h3 style="color: green;">OTP Verify Successfully. <p style="line-height: 2.5rem;">Reset Your Password.</p></h3>');
                         $('#change_user_id').val(user_id);
                         $('#change_user_mobile').val(user_mobile);
                         $('#changePasswordModal').modal({backdrop: 'static', keyboard: false}); 
                      }else{
                         $("#forgot_verify_response_div").empty();
                         $("#forgot_verify_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="forgot_verify_message"></h5></div>       </div></div>');
                         $("#forgot_verify_message").empty();
                         $("#forgot_verify_message").html(data.message).css('color', 'red');
                      }
     
                   }
                });
             } else if (data.dismiss === Swal.DismissReason.cancel) {
              Swal.fire({
                  title: 'Cancelled',
                  type: 'error',
                  confirmButtonClass: 'btn btn-success',
                })
            }
          })
     }
  </script>
  <!-- changePasswordButton -->
  <script type="text/javascript">
     function changePasswordButton() {
            var SITEURL = '{{URL::to('')}}';
            var id = $('#change_user_id').val();
            var mobile = $('#change_user_mobile').val();
            var password = $('#user_password').val();
            var confirm_password = $('#confirm_password').val();



            Swal.fire({
               title: 'Do you want to continue?',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes, Continue',
               confirmButtonClass: 'btn btn-primary',
               cancelButtonClass: 'btn btn-danger ml-1',
               buttonsStyling: false,
             }).then((data) => {
              if (data.value) {
                $('#Dv_Loader').show();
     
                $.ajax({
                   type:"get",
                   url:"/changePassword", 
                   data : {'id':id,'mobile':mobile,'password':password,},
                   dataType: "json",
                   success:function(data)
                   {   
                      $('#Dv_Loader').hide();
                      if(data.code == '000'){
                         $("#change_pass_message").empty();
                         $("#change_pass_response_div").empty();
                         $("#change_pass_message").append('<h3 style="color: green;">Password Reset Successfully.</h3>');

                          var timeleft = 5;
                          var downloadTimer = setInterval(function(){
                            if(timeleft <= 0){
                              clearInterval(downloadTimer);
                              // document.getElementById("countdown").innerHTML = "Finished";
                              $("#change_countdown").empty();
                              $("#change_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');

                            } else {
                              // document.getElementById("change_countdown").innerHTML ="This Page is reload after " + timeleft + " seconds.";
                              $("#change_countdown").empty();
                              $("#change_countdown").append('<span class="chip-text spinner-border spinner-border-lg text-primary"></span>');
                            }
                            timeleft -= 1;
                          }, 970);
                          setTimeout(function () {
                             window.location.href = SITEURL + '/login';
                          }, 5000);
                      }else{
                         $("#change_pass_response_div").empty();
                         $("#change_pass_response_div").append('<div id="invoice-company-details" class="row"> <div class="col-sm-12 col-12 text-center rounded" style="margin-bottom: -1rem;border: 2px solid darkgray;"> <div class="invoice-details" style="margin-top: 0.5rem !important;"><h5 id="changepass_message"></h5></div>       </div></div>');
                         $("#changepass_message").empty();
                         $("#changepass_message").html(data.message).css('color', 'red');
                      }
     
                   }
                });
             } else if (data.dismiss === Swal.DismissReason.cancel) {
              Swal.fire({
                  title: 'Cancelled',
                  type: 'error',
                  confirmButtonClass: 'btn btn-success',
                })
            }
          })
     }
  </script>
  
</body>
<!-- END: Body-->

</html>