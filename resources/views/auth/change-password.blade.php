<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

   <!-- BEGIN: Head-->

   <head>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

      <title>Forgot Password - Damskart Business</title>

      <link rel="apple-touch-icon" href="{{ asset('admin/app-assets/images/ico/apple-icon-120.png')}}">

      <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/Damskart Logo.png')}}">

      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

      <!-- BEGIN: Vendor CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/vendors/css/vendors.min.css')}}">

      <!-- END: Vendor CSS-->

      <!-- BEGIN: Theme CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/bootstrap-extended.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/colors.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/components.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/dark-layout.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/themes/semi-dark-layout.css')}}">

      <!-- BEGIN: Page CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/core/colors/palette-gradient.css')}}">

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/app-assets/css/pages/authentication.css')}}">

      <!-- END: Page CSS-->

      <!-- BEGIN: Custom CSS-->

      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css')}}">

      <!-- END: Custom CSS-->

   </head>

   <!-- END: Head-->

   <!-- BEGIN: Body-->

   <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

      <!-- BEGIN: Content-->

      <div class="app-content content">

         <div class="content-overlay"></div>

         <div class="header-navbar-shadow"></div>

         <div class="content-wrapper">

            <div class="content-header row">

            </div>

            <div class="content-body">

               <section class="row flexbox-container">

                  <div class="col-xl-7 col-md-9 col-10 d-flex justify-content-center px-0">

                     <div class="card bg-authentication rounded-0 mb-0">

                        <div class="row m-0">

                           <div class="col-lg-6 d-lg-block d-none text-center align-self-center">

                              <img src="{{ asset('admin/app-assets/images/pages/forgot-password.png')}}" alt="branding logo">

                           </div>

                           <div class="col-lg-6 col-12 p-0">

                              <div class="card rounded-0 mb-0 px-2 py-1">

                                 <div class="card-header pb-1">

                                    <div class="card-title">

                                       <h4 class="mb-0">Change Password</h4>

                                    </div>

                                 </div>

                                 <p class="px-2 mb-0">Enter New Password</p>

                                 <div class="card-content">

                                    <div class="card-body">

                                       <form  method="POST"  action="{{route('changepassword')}}" enctype="multipart/form-data">

                                          @csrf

                                          <div class="form-label-group">

                                             <input id="password" type="password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" onkeyup="passwordDetected()" placeholder="Enter Password" required >



                                             <label for="inputEmail">New Password</label>

                                          </div> 

                                          <div class="form-label-group">

                                             <input id="password-confirm" type="password" class="form-control"  value="{{ old('password_confirmation') }}" name="password_confirmation" placeholder="Confirm Password " onkeyup="passwordDetected()" required>



                                             <label for="inputEmail">Confirm Password</label>

                                             <span id='message'></span>

                                          </div>

                                          <input type="hidden" name="mobile" value="{{$user->mobile}}">

                                          <input type="hidden" name="id" value="{{$user->id}}">

                                          <div class="d-block mb-1">

                                             <button class="btn btn-primary btn-block px-75">Submit</button>

                                          </div>

                                       </form>

                                    </div>

                                 </div>

                              </div>

                           </div>

                        </div>

                     </div>

                  </div>

            </div>

            </section>

         </div>

      </div>

      </div>

      <!-- END: Content-->

      <!-- BEGIN: Vendor JS-->

      <script src="{{ asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>

      <!-- BEGIN Vendor JS-->

      <!-- BEGIN: Page Vendor JS-->

      <!-- END: Page Vendor JS-->

      <!-- BEGIN: Theme JS-->

      <script src="{{ asset('admin/app-assets/js/core/app-menu.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/core/app.js')}}"></script>

      <script src="{{ asset('admin/app-assets/js/scripts/components.js')}}"></script>

      <!-- END: Theme JS-->

      <!-- BEGIN: Page JS-->

      <!-- END: Page JS-->

      <script type="text/javascript">

        function passwordDetected(){

          password = $('#password').val()

          passwordconfirm = $('#password-confirm').val()

          if ((password.length > 0) && (passwordconfirm.length > 0)) {

               if ($('#password').val() == $('#password-confirm').val()) {

                  $('#message').html('Password matched.').css('color', 'green');

                  $('#loginbutton').removeAttr('disabled');

               }else{

                  $('#message').html('Password does not match!' ).css('color', 'red');

                  $('#loginbutton').attr('disabled', 'disabled');   

               } 

          }else{

               $('#loginbutton').attr('disabled', 'disabled');

          }

        }

      </script>

   </body>

   <!-- END: Body-->

</html>