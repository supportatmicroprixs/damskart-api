<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Damkart Business Merchant API</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.print.css") }}" media="print">
    <script src="{{ asset("vendor/scribe/js/theme-default-3.8.0.js") }}"></script>

    <link rel="stylesheet"
          href="//unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/obsidian.min.css">
    <script src="//unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"></script>
    <script>hljs.highlightAll();</script>

    <script src="//cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
    <script>
        var baseUrl = "http://192.168.0.4/damskart-manage/";
    </script>
    <script src="{{ asset("vendor/scribe/js/tryitout-3.8.0.js") }}"></script>

</head>

<body data-languages="[&quot;php&quot;,&quot;dotnet&quot;,&quot;javascript&quot;,&quot;bash&quot;]">
<a href="#" id="nav-button">
      <span>
        MENU
        <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="navbar-image" />
      </span>
</a>
<div class="tocify-wrapper">
        <img src="images/damskart-logo.jpg" alt="logo" class="logo" style="padding-top: 10px; max-width: 100%; padding-left: 35%" width="56px"/>
                <div class="lang-selector">
                            <a href="#" data-language-name="php">php</a>
                            <a href="#" data-language-name="dotnet">dotnet</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                            <a href="#" data-language-name="bash">bash</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                            <li><a href="#">Documentation powered by Damskart</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: August 14 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>Damkart API description</p>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>You'll see code examples for working with the API in different programming languages .</aside>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">https://manage.damskart.com</code></pre>

        <h1>Authenticating requests</h1>
<p>This API is not authenticated.</p>

        <h1 id="icici-aeps">ICICI AEPS</h1>

    

            <h2 id="icici-aeps-POSTmerchantapi-withdrawICICI">Withdraw Money</h2>

<p>
</p>



<span id="example-requests-POSTmerchantapi-withdrawICICI">
<blockquote>Example request:</blockquote>


<pre><code class="language-php">$url = "https://manage.damskart.com/merchantapi/withdrawICICI";
$data = [
            'merchant_key' =&gt; 'ut',
            'merchant_secret' =&gt; 'quia',
            'service_name' =&gt; 'accusantium',
            'device_name' =&gt; 'magni',
            'mobile_number' =&gt; 'amet',
            'aadhar_number' =&gt; 'est',
            'amount' =&gt; 'vitae',
            'bank' =&gt; 'doloribus',
        ];

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);</code></pre>

<pre><code class="language-dotnet">Dictionary&lt;string, object&gt; body = new Dictionary&lt;string, object&gt;();
Dictionary&lt;string, string&gt; head = new Dictionary&lt;string, string&gt;();
Dictionary&lt;string, object &gt; requestBody = new Dictionary&lt;string, object &gt;();

string post_data = JsonConvert.SerializeObject("[
            'merchant_key' =&gt; 'ut',
            'merchant_secret' =&gt; 'quia',
            'service_name' =&gt; 'accusantium',
            'device_name' =&gt; 'magni',
            'mobile_number' =&gt; 'amet',
            'aadhar_number' =&gt; 'est',
            'amount' =&gt; 'vitae',
            'bank' =&gt; 'doloribus',
        ]", "1");

string url = "https://manage.damskart.com/merchantapi/withdrawICICI";

HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

webRequest.Method = "POST";
webRequest.ContentType = "application/json";
webRequest.ContentLength = post_data.Length;

using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
{
    requestWriter.Write(post_data);
}

string responseData = string.Empty;

using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
{
    responseData = responseReader.ReadToEnd();
    Console.WriteLine(responseData);
}
</code></pre>

<pre><code class="language-javascript">const url = new URL(
    "https://manage.damskart.com/merchantapi/withdrawICICI"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "merchant_key": "ut",
    "merchant_secret": "quia",
    "service_name": "accusantium",
    "device_name": "magni",
    "mobile_number": "amet",
    "aadhar_number": "est",
    "amount": "vitae",
    "bank": "doloribus"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>

<pre><code class="language-bash">curl --request POST \
    "https://manage.damskart.com/merchantapi/withdrawICICI" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"merchant_key\": \"ut\",
    \"merchant_secret\": \"quia\",
    \"service_name\": \"accusantium\",
    \"device_name\": \"magni\",
    \"mobile_number\": \"amet\",
    \"aadhar_number\": \"est\",
    \"amount\": \"vitae\",
    \"bank\": \"doloribus\"
}"
</code></pre>
</span>

<span id="example-responses-POSTmerchantapi-withdrawICICI">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;success&quot;: &quot;Form is successfully submitted&quot;,
    &quot;txn_id&quot;: &quot;DMS20210812203700&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (402):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;error&quot;: &quot;Service name is not correct&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Oops! We are in lost space. Please be patient while we are working on it&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTmerchantapi-withdrawICICI" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTmerchantapi-withdrawICICI"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTmerchantapi-withdrawICICI"></code></pre>
</span>
<span id="execution-error-POSTmerchantapi-withdrawICICI" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTmerchantapi-withdrawICICI"></code></pre>
</span>
<form id="form-POSTmerchantapi-withdrawICICI" data-method="POST"
      data-path="merchantapi/withdrawICICI"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTmerchantapi-withdrawICICI', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTmerchantapi-withdrawICICI"
                    onclick="tryItOut('POSTmerchantapi-withdrawICICI');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTmerchantapi-withdrawICICI"
                    onclick="cancelTryOut('POSTmerchantapi-withdrawICICI');" hidden>Cancel
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTmerchantapi-withdrawICICI" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>merchantapi/withdrawICICI</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>merchant_key</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_key"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant key</p>        </p>
                <p>
            <b><code>merchant_secret</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_secret"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant secret</p>        </p>
                <p>
            <b><code>service_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="service_name"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>withdraw</p>        </p>
                <p>
            <b><code>device_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="device_name"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>mantra / morpho</p>        </p>
                <p>
            <b><code>mobile_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="mobile_number"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>Mobile number</p>        </p>
                <p>
            <b><code>aadhar_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="aadhar_number"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>Aadhar number</p>        </p>
                <p>
            <b><code>amount</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="amount"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>Withdraw Amount</p>        </p>
                <p>
            <b><code>bank</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="bank"
               data-endpoint="POSTmerchantapi-withdrawICICI"
               data-component="body" required  hidden>
    <br>
<p>Bank name</p>        </p>
    
    </form>

            <h2 id="icici-aeps-POSTmerchantapi-balanceEnquiryICICI">Balance Enquiry</h2>

<p>
</p>



<span id="example-requests-POSTmerchantapi-balanceEnquiryICICI">
<blockquote>Example request:</blockquote>


<pre><code class="language-php">$url = "https://manage.damskart.com/merchantapi/balanceEnquiryICICI";
$data = [
            'merchant_key' =&gt; 'molestiae',
            'merchant_secret' =&gt; 'eum',
            'service_name' =&gt; 'repellendus',
            'device_name' =&gt; 'officiis',
            'mobile_number' =&gt; 'facilis',
            'aadhar_number' =&gt; 'qui',
            'bank' =&gt; 'mollitia',
        ];

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);</code></pre>

<pre><code class="language-dotnet">Dictionary&lt;string, object&gt; body = new Dictionary&lt;string, object&gt;();
Dictionary&lt;string, string&gt; head = new Dictionary&lt;string, string&gt;();
Dictionary&lt;string, object &gt; requestBody = new Dictionary&lt;string, object &gt;();

string post_data = JsonConvert.SerializeObject("[
            'merchant_key' =&gt; 'molestiae',
            'merchant_secret' =&gt; 'eum',
            'service_name' =&gt; 'repellendus',
            'device_name' =&gt; 'officiis',
            'mobile_number' =&gt; 'facilis',
            'aadhar_number' =&gt; 'qui',
            'bank' =&gt; 'mollitia',
        ]", "1");

string url = "https://manage.damskart.com/merchantapi/balanceEnquiryICICI";

HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

webRequest.Method = "POST";
webRequest.ContentType = "application/json";
webRequest.ContentLength = post_data.Length;

using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
{
    requestWriter.Write(post_data);
}

string responseData = string.Empty;

using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
{
    responseData = responseReader.ReadToEnd();
    Console.WriteLine(responseData);
}
</code></pre>

<pre><code class="language-javascript">const url = new URL(
    "https://manage.damskart.com/merchantapi/balanceEnquiryICICI"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "merchant_key": "molestiae",
    "merchant_secret": "eum",
    "service_name": "repellendus",
    "device_name": "officiis",
    "mobile_number": "facilis",
    "aadhar_number": "qui",
    "bank": "mollitia"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>

<pre><code class="language-bash">curl --request POST \
    "https://manage.damskart.com/merchantapi/balanceEnquiryICICI" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"merchant_key\": \"molestiae\",
    \"merchant_secret\": \"eum\",
    \"service_name\": \"repellendus\",
    \"device_name\": \"officiis\",
    \"mobile_number\": \"facilis\",
    \"aadhar_number\": \"qui\",
    \"bank\": \"mollitia\"
}"
</code></pre>
</span>

<span id="example-responses-POSTmerchantapi-balanceEnquiryICICI">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;success&quot;: &quot;Form is successfully submitted&quot;,
    &quot;txn_id&quot;: &quot;DMS20210812203700&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (402):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;error&quot;: &quot;Service name is not correct&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Oops! We are in lost space. Please be patient while we are working on it&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTmerchantapi-balanceEnquiryICICI" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTmerchantapi-balanceEnquiryICICI"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTmerchantapi-balanceEnquiryICICI"></code></pre>
</span>
<span id="execution-error-POSTmerchantapi-balanceEnquiryICICI" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTmerchantapi-balanceEnquiryICICI"></code></pre>
</span>
<form id="form-POSTmerchantapi-balanceEnquiryICICI" data-method="POST"
      data-path="merchantapi/balanceEnquiryICICI"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTmerchantapi-balanceEnquiryICICI', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTmerchantapi-balanceEnquiryICICI"
                    onclick="tryItOut('POSTmerchantapi-balanceEnquiryICICI');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTmerchantapi-balanceEnquiryICICI"
                    onclick="cancelTryOut('POSTmerchantapi-balanceEnquiryICICI');" hidden>Cancel
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTmerchantapi-balanceEnquiryICICI" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>merchantapi/balanceEnquiryICICI</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>merchant_key</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_key"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant key</p>        </p>
                <p>
            <b><code>merchant_secret</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_secret"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant secret</p>        </p>
                <p>
            <b><code>service_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="service_name"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>withdraw</p>        </p>
                <p>
            <b><code>device_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="device_name"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>mantra / morpho</p>        </p>
                <p>
            <b><code>mobile_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="mobile_number"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>Mobile number</p>        </p>
                <p>
            <b><code>aadhar_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="aadhar_number"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>Aadhar number</p>        </p>
                <p>
            <b><code>bank</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="bank"
               data-endpoint="POSTmerchantapi-balanceEnquiryICICI"
               data-component="body" required  hidden>
    <br>
<p>Bank name</p>        </p>
    
    </form>

            <h2 id="icici-aeps-POSTmerchantapi-aadharPayICICI">Aadhar Pay Money</h2>

<p>
</p>



<span id="example-requests-POSTmerchantapi-aadharPayICICI">
<blockquote>Example request:</blockquote>


<pre><code class="language-php">$url = "https://manage.damskart.com/merchantapi/aadharPayICICI";
$data = [
            'merchant_key' =&gt; 'nihil',
            'merchant_secret' =&gt; 'molestiae',
            'service_name' =&gt; 'provident',
            'device_name' =&gt; 'similique',
            'mobile_number' =&gt; 'aut',
            'aadhar_number' =&gt; 'explicabo',
            'amount' =&gt; 'molestiae',
            'bank' =&gt; 'ea',
        ];

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);</code></pre>

<pre><code class="language-dotnet">Dictionary&lt;string, object&gt; body = new Dictionary&lt;string, object&gt;();
Dictionary&lt;string, string&gt; head = new Dictionary&lt;string, string&gt;();
Dictionary&lt;string, object &gt; requestBody = new Dictionary&lt;string, object &gt;();

string post_data = JsonConvert.SerializeObject("[
            'merchant_key' =&gt; 'nihil',
            'merchant_secret' =&gt; 'molestiae',
            'service_name' =&gt; 'provident',
            'device_name' =&gt; 'similique',
            'mobile_number' =&gt; 'aut',
            'aadhar_number' =&gt; 'explicabo',
            'amount' =&gt; 'molestiae',
            'bank' =&gt; 'ea',
        ]", "1");

string url = "https://manage.damskart.com/merchantapi/aadharPayICICI";

HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

webRequest.Method = "POST";
webRequest.ContentType = "application/json";
webRequest.ContentLength = post_data.Length;

using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
{
    requestWriter.Write(post_data);
}

string responseData = string.Empty;

using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
{
    responseData = responseReader.ReadToEnd();
    Console.WriteLine(responseData);
}
</code></pre>

<pre><code class="language-javascript">const url = new URL(
    "https://manage.damskart.com/merchantapi/aadharPayICICI"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "merchant_key": "nihil",
    "merchant_secret": "molestiae",
    "service_name": "provident",
    "device_name": "similique",
    "mobile_number": "aut",
    "aadhar_number": "explicabo",
    "amount": "molestiae",
    "bank": "ea"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>

<pre><code class="language-bash">curl --request POST \
    "https://manage.damskart.com/merchantapi/aadharPayICICI" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"merchant_key\": \"nihil\",
    \"merchant_secret\": \"molestiae\",
    \"service_name\": \"provident\",
    \"device_name\": \"similique\",
    \"mobile_number\": \"aut\",
    \"aadhar_number\": \"explicabo\",
    \"amount\": \"molestiae\",
    \"bank\": \"ea\"
}"
</code></pre>
</span>

<span id="example-responses-POSTmerchantapi-aadharPayICICI">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;success&quot;: &quot;Form is successfully submitted&quot;,
    &quot;txn_id&quot;: &quot;DMS20210812203700&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (402):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;error&quot;: &quot;Service name is not correct&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Oops! We are in lost space. Please be patient while we are working on it&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTmerchantapi-aadharPayICICI" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTmerchantapi-aadharPayICICI"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTmerchantapi-aadharPayICICI"></code></pre>
</span>
<span id="execution-error-POSTmerchantapi-aadharPayICICI" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTmerchantapi-aadharPayICICI"></code></pre>
</span>
<form id="form-POSTmerchantapi-aadharPayICICI" data-method="POST"
      data-path="merchantapi/aadharPayICICI"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTmerchantapi-aadharPayICICI', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTmerchantapi-aadharPayICICI"
                    onclick="tryItOut('POSTmerchantapi-aadharPayICICI');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTmerchantapi-aadharPayICICI"
                    onclick="cancelTryOut('POSTmerchantapi-aadharPayICICI');" hidden>Cancel
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTmerchantapi-aadharPayICICI" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>merchantapi/aadharPayICICI</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>merchant_key</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_key"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant key</p>        </p>
                <p>
            <b><code>merchant_secret</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_secret"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant secret</p>        </p>
                <p>
            <b><code>service_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="service_name"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>withdraw</p>        </p>
                <p>
            <b><code>device_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="device_name"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>mantra / morpho</p>        </p>
                <p>
            <b><code>mobile_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="mobile_number"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>Mobile number</p>        </p>
                <p>
            <b><code>aadhar_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="aadhar_number"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>Aadhar number</p>        </p>
                <p>
            <b><code>amount</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="amount"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>Withdraw Amount</p>        </p>
                <p>
            <b><code>bank</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="bank"
               data-endpoint="POSTmerchantapi-aadharPayICICI"
               data-component="body" required  hidden>
    <br>
<p>Bank name</p>        </p>
    
    </form>

            <h2 id="icici-aeps-POSTmerchantapi-miniStatementICICI">Mini Statement</h2>

<p>
</p>



<span id="example-requests-POSTmerchantapi-miniStatementICICI">
<blockquote>Example request:</blockquote>


<pre><code class="language-php">$url = "https://manage.damskart.com/merchantapi/miniStatementICICI";
$data = [
            'merchant_key' =&gt; 'porro',
            'merchant_secret' =&gt; 'delectus',
            'service_name' =&gt; 'et',
            'device_name' =&gt; 'ipsa',
            'mobile_number' =&gt; 'soluta',
            'aadhar_number' =&gt; 'et',
            'bank' =&gt; 'numquam',
        ];

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);</code></pre>

<pre><code class="language-dotnet">Dictionary&lt;string, object&gt; body = new Dictionary&lt;string, object&gt;();
Dictionary&lt;string, string&gt; head = new Dictionary&lt;string, string&gt;();
Dictionary&lt;string, object &gt; requestBody = new Dictionary&lt;string, object &gt;();

string post_data = JsonConvert.SerializeObject("[
            'merchant_key' =&gt; 'porro',
            'merchant_secret' =&gt; 'delectus',
            'service_name' =&gt; 'et',
            'device_name' =&gt; 'ipsa',
            'mobile_number' =&gt; 'soluta',
            'aadhar_number' =&gt; 'et',
            'bank' =&gt; 'numquam',
        ]", "1");

string url = "https://manage.damskart.com/merchantapi/miniStatementICICI";

HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

webRequest.Method = "POST";
webRequest.ContentType = "application/json";
webRequest.ContentLength = post_data.Length;

using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
{
    requestWriter.Write(post_data);
}

string responseData = string.Empty;

using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
{
    responseData = responseReader.ReadToEnd();
    Console.WriteLine(responseData);
}
</code></pre>

<pre><code class="language-javascript">const url = new URL(
    "https://manage.damskart.com/merchantapi/miniStatementICICI"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "merchant_key": "porro",
    "merchant_secret": "delectus",
    "service_name": "et",
    "device_name": "ipsa",
    "mobile_number": "soluta",
    "aadhar_number": "et",
    "bank": "numquam"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>

<pre><code class="language-bash">curl --request POST \
    "https://manage.damskart.com/merchantapi/miniStatementICICI" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"merchant_key\": \"porro\",
    \"merchant_secret\": \"delectus\",
    \"service_name\": \"et\",
    \"device_name\": \"ipsa\",
    \"mobile_number\": \"soluta\",
    \"aadhar_number\": \"et\",
    \"bank\": \"numquam\"
}"
</code></pre>
</span>

<span id="example-responses-POSTmerchantapi-miniStatementICICI">
            <blockquote>
            <p>Example response (200, success):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;success&quot;: &quot;Form is successfully submitted&quot;,
    &quot;txn_id&quot;: &quot;DMS20210812203700&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (402):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;error&quot;: &quot;Service name is not correct&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Oops! We are in lost space. Please be patient while we are working on it&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTmerchantapi-miniStatementICICI" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTmerchantapi-miniStatementICICI"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTmerchantapi-miniStatementICICI"></code></pre>
</span>
<span id="execution-error-POSTmerchantapi-miniStatementICICI" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTmerchantapi-miniStatementICICI"></code></pre>
</span>
<form id="form-POSTmerchantapi-miniStatementICICI" data-method="POST"
      data-path="merchantapi/miniStatementICICI"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTmerchantapi-miniStatementICICI', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTmerchantapi-miniStatementICICI"
                    onclick="tryItOut('POSTmerchantapi-miniStatementICICI');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTmerchantapi-miniStatementICICI"
                    onclick="cancelTryOut('POSTmerchantapi-miniStatementICICI');" hidden>Cancel
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTmerchantapi-miniStatementICICI" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>merchantapi/miniStatementICICI</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>merchant_key</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_key"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant key</p>        </p>
                <p>
            <b><code>merchant_secret</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="merchant_secret"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>merchant secret</p>        </p>
                <p>
            <b><code>service_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="service_name"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>withdraw</p>        </p>
                <p>
            <b><code>device_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="device_name"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>mantra / morpho</p>        </p>
                <p>
            <b><code>mobile_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="mobile_number"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>Mobile number</p>        </p>
                <p>
            <b><code>aadhar_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="aadhar_number"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>Aadhar number</p>        </p>
                <p>
            <b><code>bank</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="bank"
               data-endpoint="POSTmerchantapi-miniStatementICICI"
               data-component="body" required  hidden>
    <br>
<p>Bank name</p>        </p>
    
    </form>

    

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="php">php</a>
                                    <a href="#" data-language-name="dotnet">dotnet</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                                    <a href="#" data-language-name="bash">bash</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var exampleLanguages = ["php","dotnet","javascript","bash"];
        setupLanguages(exampleLanguages);
    });
</script>
</body>
</html>