<div class="commission_container_{{ $count }}">
	<div class="row">
		<div  class="form-group col-sm-2 ">
			<label>{!! $label1 !!}</label>
			<input
					type="number"
					name="{{ $name }}_start_price[]"
					value=""
					min="0"
					step="1"
					required
					onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');"
					style="width: 100%"
					onchange="checkAmountStartPrice({{ $count }})"
					style="width: 100%"
					class="form-control start_price_info{{ $count }}"
				>
		</div>

		<div class="form-group col-sm-3 ">
			<label>{!! $label2 !!}</label>
			<input
					type="number"
					name="{{ $name }}_end_price[]"
					value=""
					min="0"
					step="1"
					required
					onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');"
					style="width: 100%"
					onchange="checkAmountEndPrice({{ $count }})"
					style="width: 100%"
					class="form-control end_price_info{{ $count }}"
				>
		</div>

		<div  class="form-group col-sm-3 ">
			<label>{!! $label3 !!}</label>
			<input
					type="number"
					name="{{ $name }}_commission[]"
					value=""
					step="any"
					required
					style="width: 100%"
					@include('crud::inc.field_attributes')
				>
		</div>

		<div class="form-group col-sm-3">
			<label>{!! $label4 !!} </label>
			<select
					name="{{ $name }}_commission_type[]"
					style="width: 100%"
					required
					@include('crud::inc.field_attributes')
				>
				<option value="flat">Flat</option>
				<option value="percentage">Percentage</option>
			</select>
		</div>

		<div class="form-group col-sm-1">
			<label>&nbsp;</label><br /><i class="la la-times btn btn-danger pull-right" onclick="removeCommissionContainer({{ $count }});"></i>
		</div>
	</div>

	<script type="text/javascript">
		$('.next-commission').attr('onclick', "addNextCommission({{ $count }});");
	</script>
</div>
