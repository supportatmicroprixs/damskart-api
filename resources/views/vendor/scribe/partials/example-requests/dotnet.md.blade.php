@php
    use Knuckles\Scribe\Tools\WritingUtils as u;
    /** @var  Knuckles\Camel\Output\OutputEndpointData $endpoint */
@endphp
```dotnet
Dictionary<string, object> body = new Dictionary<string, object>();
Dictionary<string, string> head = new Dictionary<string, string>();
Dictionary<string, object > requestBody = new Dictionary<string, object >();

string post_data = JsonConvert.SerializeObject("{!! u::printPhpValue($endpoint->cleanBodyParameters, 8) !!}", "1");

string url = "{{ rtrim($baseUrl, '/') . '/' . ltrim($endpoint->boundUri, '/') }}";

HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

webRequest.Method = "POST";
webRequest.ContentType = "application/json";
webRequest.ContentLength = post_data.Length;

using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
{
    requestWriter.Write(post_data);
}

string responseData = string.Empty;

using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
{
    responseData = responseReader.ReadToEnd();
    Console.WriteLine(responseData);
}

```