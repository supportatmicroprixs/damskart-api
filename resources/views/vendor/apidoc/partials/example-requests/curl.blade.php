```curl

$url = "{{ rtrim($baseUrl, '/') . '/' . ltrim($route['boundUri'], '/') }}";
$data = {!! \Mpociot\ApiDoc\Tools\Utils::printPhpValue($route['cleanBodyParameters'], 8) !!};

$postdata = json_encode($data);

$ch = curl_init($url); 
@if(strtolower($route['methods'][0]) == "post")
curl_setopt($ch, CURLOPT_POST, 1);
@endif
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);
```
