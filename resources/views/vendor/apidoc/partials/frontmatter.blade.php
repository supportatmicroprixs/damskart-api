title: Damskart API Reference

language_tabs:
@foreach($settings['languages'] as $language)
- {{ $language }}
@endforeach

includes:

search: true

toc_footers:
- <a href='#'>Powered by Damskart</a>