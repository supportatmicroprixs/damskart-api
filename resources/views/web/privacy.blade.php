@extends('web.layouts.app')
@section('title')
Damskart Business - Privacy & Policy
@endsection
@section('css')
@endsection

@section('content')
  <header>
  <section class="exp-banner-head primary-overlay" id="exp-inner-banner">
    <div class="container">
      <div class="section-title-center exp-banner-title">
        <h1 class="text-white">Privacy & Policy</h1>
        <p class="text-white">Home<span class="exp-primary-color">Privacy & Policy</span></p>
      </div>
    </div>
  </section>
</header>
<!--=============Header==================-->

<section class="exp-contact-1 section-paddingg">
    <div id="page" class="hfeed site">
      <div class="welcome inside-page text-center"></div>
      <div class="container">
         <div id="content" class="content-lift site-content">
            <div class="row">
               <div id="primary" class="col-sm-12">
                  <main id="blog" class="site-main blog-main" role="blog">
                     <article id="post-44364" class="post-44364 page type-page status-publish hentry">
                        <div class="content-area">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                              <h3><span style="font-weight: 400;">Privacy Policy</span></h3>
                              <p><span style="font-weight: 400;">We, Damskart India Private Limited, are registered at D-308, Gali No. 7, MukundVihar, Karawal Nagar, North East Delhi, 110094 and hereinafter referred to as Damskart. At Damskart, we value your trust & respect your privacy. This Privacy Policy provides you with details about the manner in which your data is collected, stored & used by us. You are advised to read this Privacy Policy carefully. By downloading and using the Damskart application/ website/WAP site you expressly give us consent to use & disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access Damskart.</span></p>

                              <p><span style="font-weight: 400;"><strong>Note: Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to Damskart applications, desktop website & mobile WAP site.</strong></span>

                              
                              <h3><span style="font-weight: 400;">General</span></h3>


                              <p><span style="font-weight: 400;">We will not sell, share or rent your personal information to any 3rd party or use your email address/mobile number for unsolicited emails and/or SMS. Any emails and/or SMS sent by Damskart will only be in connection with the provision of agreed services & products and this Privacy Policy.
                              Periodically, we may reveal general statistical information about Damskart& its users, such as number of visitors, number and type of goods and services purchased, etc.
                              We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure.
                              </span></p>



                              <h3><span style="font-weight: 400;">Personal Information</span></h3>
                              <p><span style="font-weight: 400;">Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on Damskart.
                              </span></p>
                              <p><span style="font-weight: 400;">When you browse through Damskart, we may collect information regarding the mobile/ tab device details, domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.
                              </span></p>




                              <h3><span style="font-weight: 400;">Use of Personal Information</span></h3>
                              <p><span style="font-weight: 400;">We use personal information to provide you with services & products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect & protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc.
                              We also use your contact information to send you offers based on your previous orders and interests.
                              We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at Damskart, providing you with content that we think you might be interested in and to display content according to your preferences.
                              </span></p>

                              <h3><span style="font-weight: 400;">Cookies</span></h3>

                              <p><span style="font-weight: 400;">A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. Damskart uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.
                              </span></p>



                              
                              <h3><span style="font-weight: 400;">Links to Other Sites</span></h3>
                              
                              <p><span style="font-weight: 400;">Our site links to other websites that may collect personally identifiable information about you. Damskart is not responsible for the privacy practices or the content of those linked websites.
                              </span></p>








                              <h3><span style="font-weight: 400;">Security</span></h3>
                              <p><span style="font-weight: 400;">Damskart has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</span></p>




                              <h3><span style="font-weight: 400;">Consent</span></h3>
                              <p><span style="font-weight: 400;">By using Damskart and/or by providing your information, you consent to the collection and use of the information you disclose on Damskart in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</span></p>
                              <p><span style="font-weight: 400;">We are required to make an automated IVR or a manual verification call to the customer for every cash on delivery (COD) order placed. You will receive this verification call even if your number is registered for the Do Not Disturb (DND) option.</span></p>
                              <p><span style="font-weight: 400;">For merchants accepting payments through Damskart for their websites / app/stores/shops</span></p>



                              <p><span style="font-weight: 400;">Online queries in Damskart merchants dashboard (https://dashboard.Damskart.com/)</span></p>
                              <p><span style="font-weight: 400;">First response time - 2 working days</span></p>
                              <p><span style="font-weight: 400;">Final resolution time - 4 working days</span></p>
                              <p><span style="font-weight: 400;">Helpline - +91-9783335317</span></p>
                              <p><span style="font-weight: 400;">Final resolution time - 4 working days</span></p>
                              <p><span style="font-weight: 400;">If Damskart needs additional time for any case, same will be informed to customer along with reason of delay and expected resolution timelines</span></p>
                              <p><span style="font-weight: 400;"><strong>Level 2 (Complaints)</strong></span></p>

                              <p><span style="font-weight: 400;">If customer's issue is not resolved even after contacting various complaint resolution channels, he/she can reach out to our grievance officer at:</span></p>


                              <p><span style="font-weight: 400;">Grievance Officer
                              Damskart India Private Limited<br>
                              501-A, D-111, 6th Floor,<br>
                              Radhey Villa Apartment,<br>
                              Power House Road, Banipark<br>
                              Jaipur – Rajasthan - 302016<br>
                              </span></p>
                              <p><span style="font-weight: 400;">Email: admin@damskart.com</span></p>
                              

                              
                           </div>
                           <!-- .entry-content -->
                        </div>
                     </article>
                     <!-- #post-## -->  
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
         </div>
      </div>
</section>
@endsection

@section('scripts')
@endsection