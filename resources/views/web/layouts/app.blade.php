<!DOCTYPE html>
<html lang="en">
   <head>
      <title>@yield('title')</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- bootstrap-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/bootstrap.min.css')}}">
      <!--Font awesome-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/fontawesome.min.css')}}">
      <!--animate.css-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/animate.css')}}">
      <!-- flaticon-->
      <link rel="stylesheet" type="text/css" href="{{ asset('web/assets/fonts/flaticon.css')}}">
      <!--main css-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/style.css')}}">
      <!-- slick slider css-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/slick.css')}}">
      <!--slick theme css-->
      <link rel="stylesheet" href="{{ asset('web/assets/css/slick-theme.css')}}">
      <!-- magnific popup -->
      <link rel="stylesheet" href="{{ asset('web/assets/css/magnific-popup.css')}}">
      <!-- google fonts-->
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;display=swap" rel="stylesheet">
      <!-- google fonts-->
      <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700,900&amp;display=swap" rel="stylesheet">
      @yield('css')
   </head>
   <body>
      <!-- Navigation -->
      <div class="navigation-wrap  start-header start-style">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <nav class="navbar navbar-expand-md navbar-light">
                     <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('web/assets/img/logo.png')}}" alt=""></a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto py-4 py-md-0">
                           <li class="nav-item pl-4 pl-md-0 ml-0">
                              <a class="nav-link {{ (request()->is('/')) ? 'active' : '' }}" href="{{url('/')}}" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                           </li>
                           <li class="nav-item pl-4 pl-md-0 ml-0">
                              <a class="nav-link {{ (request()->is('about-us')) ? 'active' : '' }}" href="{{route('about-us')}}" role="button" aria-haspopup="true" aria-expanded="false">About Us</a>
                           </li>
                           <li class="nav-item pl-4 pl-md-0 ml-0">
                              <a class="nav-link {{ (request()->is('services')) ? 'active' : '' }}" href="{{route('services')}}" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
                           </li>
                           <li class="nav-item pl-4 pl-md-0 ml-0">
                              <a class="nav-link {{ (request()->is('contact')) ? 'active' : '' }}" href="{{route('contact')}}" role="button" aria-haspopup="true" aria-expanded="false">Contact Us</a>
                           </li>
                           <li class="nav-item pl-4 pl-md-0 ml-0">
                              <a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}" href="{{route('login')}}" role="button" aria-haspopup="true" aria-expanded="false">Log-In</a>
                           </li>
                           <li class="nav-item pl-4 pl-md-0 ml-0 mega-menu-link">
                              <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}" href="{{route('register')}}">Sign-Up</a>
                           </li>
                        </ul>
                        <div class="mega-menu-link" id="searchLink">
                           <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i></a>
                           <div class="dropdown-menu mega-menu search-form">
                              <div class="row">
                                 <div class="col-12">
                                    <form role="search" method="get" id="searchform">
                                       <div class="input-group">
                                          <input type="text" id="searchbox" class="form-control" name="search-box">
                                          <div class="input-group-btn">
                                             <button class="btn btn-default"  id="searchsubmit"  type="submit">
                                             Search
                                             </button>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </div>
            <div class="seperator"></div>
         </div>
      </div>
      <!--==END Navigation==-->
      @yield('content')
      <!--==Footer==-->
      <footer class="exp-footer-style-2" id="exp-home3-footer">
         <div class="exp-footer-wrapper">
            <div class="container">
               <div class="row">
                  <div class="col-xl-4 col-lg-6">
                     <div class="exp-wrapper">
                        <div class="exp-footer-item-left">
                           <h6>Contact Us</h6>
                          <!--  <div class="exp-footer-item-inner">
                              <p>Lorem Ipsum is not simply random text It has roots in a piece of classical Latin literature Latin.</p>
                           </div> -->
                           <div class="exp-wrapper exp-footer-contact">
                              <div class="exp-footer-item-left">
                                 <ul>
                                    <li><span><i class="fas fa-phone-volume"></i></span><span><a href="tel:+91-9783335317">+91-9783335317</a></span></li>
                                 </ul>
                                 <ul>
                                    <li><span><i class="far fa-envelope"></i></span><a href="mailto:info@Damskart.com">info@Damskart.com</a></li>
                                 </ul>
                                 <ul>
                                    <li><span><i class="fas fa-map-marker-alt"></i></span><span>Grievance Officer
                                      Damskart India Pvt Ltd.
                                       501-A, D111, 6th Floor, Radhey Villa Apartment, Banipark
                                       Jaipur, Rajasthan - 302016
                                       </span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-6">
                     <div class="exp-wrapper">
                        <div class="exp-footer-item-center">
                           <h6>Quick Links</h6>
                           <ul>
                              <li><a href="{{route('about-us')}}">About Us</a></li>
                              <li><a href="{{route('services')}}">Services</a></li>
                              <li><a href="{{route('contact')}}">Contact us</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-6 padding-right-0">
                     <div class="exp-wrapper">
                        <div class="exp-footer-item-center-2">
                           <h6>Other Links</h6>
                           <ul>
                              <li><a href="{{route('terms-conditions')}}">Terms and Conditions</a></li>
                              <li><a href="{{route('privacy')}}">Privacy &amp; Policy</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-2 col-lg-6">
                     <div class="exp-wrapper">
                        <div class="exp-footer-item-center">
                           <h6>Services</h6>
                           <ul>
                              <li><a href="{{route('services')}}">ICICI AEPS</a></li>
                              <li><a href="{{route('services')}}">Fund Transfer</a></li>
                              <li><a href="{{route('services')}}">Payout</a></li>
                              <li><a href="{{route('services')}}">Recharge</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid exp-dark-bg exp-footer-copyright" id="exp-home3-copyright">
            <div class="container">
               <div class="row">
                  <div class="col-xl-6 col-md-4">
                     <div class="exp-copyright-left">
                        <p class="text-white">Copyright © <a href="#!" class="text-white">Damskart</a> - 2020</p>
                     </div>
                  </div>
                  <div class="col-xl-6 col-md-8">
                     <div class="exp-copyright-right">
                        <ul class="inline-block">
                           <li><a href="{{route('terms-conditions')}}" class="text-white">Terms and Conditions</a></li>
                           <li><a href="{{route('privacy')}}" class="text-white">Privacy &amp; Policy</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <script src="{{ asset('web/assets/js/jquery-3.4.1.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/popper.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/bootstrap.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/slick.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/jquery.magnific-popup.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/jquery.waypoints.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/jquery.counterup.js')}}"></script>
      <script src="{{ asset('web/assets/js/masonry.pkgd.min.js')}}"></script>
      <script src="{{ asset('web/assets/js/exvox.js')}}"></script>
      @yield('scripts')

      <!--Start of Tawk.to Script-->
       <script type="text/javascript">
       var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
       (function(){
       var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
       s1.async=true;
       s1.src='https://embed.tawk.to/60d1d54165b7290ac6374642/1f8pqi2gn';
       s1.charset='UTF-8';
       s1.setAttribute('crossorigin','*');
       s0.parentNode.insertBefore(s1,s0);
       })();
       </script>
       <!--End of Tawk.to Script-->
   </body>
</html>