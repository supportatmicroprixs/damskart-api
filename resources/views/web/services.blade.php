@extends('web.layouts.app')
@section('title')
Damskart Business - Our Services
@endsection
@section('css')
@endsection

@section('content')
<header>
  <section class="exp-banner-head primary-overlay" id="exp-inner-banner">
    <div class="container">
      <div class="section-title-center exp-banner-title">
        <h1 class="text-white">Our Services</h1>
        <p class="text-white">Home<span class="exp-primary-color">Services</span></p>
      </div>
    </div>
  </section>
</header>

<div class="exp-services-2 section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="blue-gradient"><i class="flaticon-shout"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>BBPS</h6>
            
<p>Utility service providers would be able to get payments instantly,
BBPS platform would have fraud monitoring and risk mitigation systems,There would be retail points for bill payments across the country who would be able to accept all kinds of bills payments</p>

          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="orange-gradient"><i class="flaticon-computer"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>AEPS</h6>
            <p>Transactions that can be done through AEPS registration are:Cash withdrawal,Balance enquiry,Mini Statement.
            Major requirements to carry out an AEPS transactions:Aadhaar Number,Fingerprint,Name or Bank IIN(Issuer Identification Number)</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="purple-gradient"><i class="flaticon-target"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>Loans</h6>
            <p>Home loans are a secured mode of finance, that give you the funds to buy or build the home of your choice. Note that while buying a new property/home, the lender requires you make a down payment of at least 10-20% of the property’s value. </p>
          

          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="purple-gradient"><i class="flaticon-idea-1"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>Money Transfer</h6>
            <p>Domestic money transfers will now be made easy with Cash to Account. Carry in cash and walk-in to any Customer Service Provider, fill in few details and have the money transferred to your loved ones, anywhere in the country.</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
     <!--  <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="green-gradient"><i class="flaticon-supermarket"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>Your title goes here...</h6>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="purple-gradient"><i class="flaticon-download"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>Your title goes here...</h6>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection