@extends('web.layouts.app')
@section('title')
Damskart Business
@endsection
@section('css')
@endsection

@section('content') 

<header>
  <section class="exp-home1-header exp-dark-bg exp-home3-header">
    <div class="exp-home1-slider">
      <div class="container-fluid">

        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-content-center">
              <div class="exp-header-item">
                <h1 class="text-white">Simple & Secure</h1>
                <p class="light-color">Damskart is India's leading B2B financial services company, offering complete financial solutions, full-stack payments facilities to Distributors/Retailers.</p>
                <a href="{{route('contact')}}" class="btn btn-primary btn-top-70">Contact Us</a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="exp-img-wrapper">
                 <img src="{{ asset('web/assets/img/homepage-1/banner.png')}}" alt="image">

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-content-center">
              <div class="exp-header-item">
                <h1 class="text-white">Aadhar Enabled Payment Systems</h1>
                <p class="light-color">Damskart AEPS services enable you hassle-free and secure cash withdraw, balance inquiry and mini-statements from your aadhaar linked bank accounts.</p>
                <a href="{{route('contact')}}" class="btn btn-primary btn-top-70">Contact Us</a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="exp-img-wrapper">
                <img src="{{ asset('web/assets/img/homepage-3/banner.png')}}" alt="image">
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-content-center">
              <div class="exp-header-item">
                <h1 class="text-white">Get Pre-Approved Loan Offers</h1>
                <p class="light-color">Damskart loan service in answer to all your financial requirements. Get instant approval and minimal paperwork required.</p>
                <a href="{{route('contact')}}" class="btn btn-primary btn-top-70">Contact Us</a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="exp-img-wrapper">
                <img src="{{ asset('web/assets/img/homepage-3/banner.png')}}" alt="image">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon fill="white" points="0,100 100,0 100,100"/>
    </svg>
  </section>
</header>

<section class="exp-services-2 section-padding">
  <div class="container">
    <div class="section-title-center">
      <h3>Our Services</h3>
      <p>Here are our services to make your life a little hustle free in terms of the financial transaction.</p>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="green-gradient"><i class="flaticon-shout"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>BBPS</h6>
            <p>Utility service providers would be able to get payments instantly. BBPS platform would have fraud monitoring and risk mitigation systems. There would be retail points for bill payments across the country that would accept all kinds of bill payments.</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="orange-gradient"><i class="flaticon-computer"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>AEPS</h6>
            <p>Transactions that can be done through AEPS registration are Cash withdrawal, Balance inquiry, Mini Statement. Significant requirements to carry out AEPS transactions: Aadhaar Number, Fingerprint, Name, or Bank IIN(Issuer Identification Number).</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="exp-services-wrapper box-shadow">
          <div class="exp-icon-block">
           <span class="purple-gradient"><i class="flaticon-idea-1"></i></span>
          </div>
          <div class="exp-services-content">
            <h6>Money Transfers</h6>
            <p>Domestic money transfers will now be made easy with Cash to Account. Carry in cash and walk-in to any Customer Service Provider, fill in few details, and have the money transferred to your loved ones, everywhere to anywhere in the country.</p>
          </div>
          <div class="exp-arrow">
            <a><i class="fas fa-angle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-padding exp-product-section exp-about-product2 exp-secondary-bg exp-home3-product2">
  <div class="container-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-content-center">
          <div class="exp-product-content">
            <div class="exp-product-title">
              <h3>Why Choose Damskart</h3>
            </div>
            <div class="exp-points d-flex">
              <div class="exp-num-block">
                <p class="purple-gradient"><i class="flaticon-feedback"></i></p>
              </div>
              <div class="exp-points-txt">

                <p><b>Best in Industry Support</b></p>
                <p>Our 24*7 support, available through emails, phone, and chat-based to help you at every step of problems.</p>
              </div>
            </div>
             <div class="exp-points d-flex margin-l-30">
              <div class="exp-num-block">
                <p class="orange-gradient"><i class="flaticon-pie-chart"></i></p>
              </div>
              <div class="exp-points-txt">
                <p><b>Highly Secure</b></p>
                <p>Our highly secure portals make sure your every transaction is safe and smooth.</p>
              </div>
            </div>
             <div class="exp-points d-flex margin-l-50">
              <div class="exp-num-block">
                <p class="green-gradient"><i class="flaticon-target"></i></p>
              </div>
              <div class="exp-points-txt">
                <p><b>Exciting Offers</b></p>
                <p>Get exciting offers and cashback for every single transaction done on our portal.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-img-wrapper right-side height-50">
            <img src="{{ asset('web/assets/img/homepage-3/business.webp')}}" alt="image">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="section-padding primary-overlay exp-home3-video">
  <div class="container">
    <div class="
    row">
      <div class="col-lg-9">
        <div class="video-title">
          <h3>Your title goes here...</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="video-points">
              <div>
                <span>1</span>
              </div>
              <div class="content">
                <h6>Your title goes here...</h6>
                <p>There are many variations of passages of Lorem Ipsum available but the major.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="video-points">
              <div>
                <span>2</span>
              </div>
              <div class="content">
                <h6>Your title goes here...</h6>
                <p>There are many variations of passages of Lorem Ipsum available but the major.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="video-points">
              <div>
                <span>3</span>
              </div>
              <div class="content">
                <h6>Your title goes here...</h6>
                <p>There are many variations of passages of Lorem Ipsum available but the major.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="video-points">
              <div>
                <span>4</span>
              </div>
              <div class="content">
                <h6>Your title goes here...</h6>
                <p>Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="video-btn-wrapper">
          <div class="circle"><a class="video-btn popup-video" href=""><i class="fas fa-play"></i></a></div>
        </div>
      </div>
    </div>
  </div>
</section> -->

<!-- <section class="section-padding exp-home3-testimonial exp-secondary-bg">
  <div class="container">
    <div class="section-title-center">
      <h3>Your title goes here...</h3>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="home3-testimonial">
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-2.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
              <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-3.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
             <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-4.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
              <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-5.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
              <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-2.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
              <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
          <div class="testimonial-item">
            <div class="img-cover">
              <img src="{{ asset('web/assets/img/homepage-3/client-1.jpg')}}" alt="image" class="d-block mx-auto img-fluid">
              <a href="#"><i class="fab fa-twitter"></i></a>
            </div>
            <p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</i></p>
            <ul class="inline-block">
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
              <li><i class="flaticon-rate-star-button exp-gold"></i></li>
            </ul>
            <h6>Your title goes here...</h6>
            <p>UX Designer</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->

@endsection

@section('scripts')
@endsection