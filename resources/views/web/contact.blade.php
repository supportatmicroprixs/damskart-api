@extends('web.layouts.app')
@section('title')
Damskart Business - Contact Us
@endsection
@section('css') 
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<header>
  <section class="exp-banner-head exp-banner-contact-2 primary-overlay">
    <div class="container">
      <div class="section-title-center exp-banner-title">
        <h1 class="text-white">Contact Us</h1>
        <p class="text-white">Home<span class="exp-primary-color">Contact Us</span></p>
      </div>
    </div>
  </section>
</header>
<section class="exp-contact-1 section-padding exp-about-workflow exp-home4-contact">
    <div class="container-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="exp-contact-section">
              <div class="exp-heading-right">
                <h3>Damskart India</h3>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod-->
                <!--tempor incididunt ut labore et dolore magna aliqua. </p>-->
              </div>
              <div class="exp-contact-form">
                <form  id="contact-form">
                 
                  <div class="form-group">
                    <input type="text" name="user_name" class="form-control" id="user_name" placeholder="Your Name">
                    <span class="text-danger" id="name-error"></span>
                  </div>
                  <div class="form-group">
                    <input type="text" maxlength="10" name="mobile" class="form-control" id="mobile" placeholder="Your Mobile">
                    <span class="text-danger" id="mobile-number-error"></span>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                    <span class="text-danger" id="email-error"></span>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control contact" name="query" id="query" rows="3" placeholder="Your Message"></textarea>
                    <span class="text-danger" id="message-error"></span>
                  </div>
                  <button id="submit" class="send btn btn-primary margin-top-30 text-center">Send Data</button>
                  
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-6 contact-img">
            <div class="exp-img-wrapper right-side video-img">
              <div class="img-wrap">
              <img src="{{asset('web/assets/img/contact/product-2.jpg')}}" alt="image">
              </div>
              <!-- <div class="video-btn-wrapper">
                <div class="circle"><a class="video-btn popup-video" href="https://www.youtube.com/watch?v=nfMTdwhQbdE"><i class="fas fa-play"></i></a></div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection

@section('scripts')
 <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#contact-form').on('submit', function(event){
        event.preventDefault();
        $('#name-error').text('');
        $('#email-error').text('');
        $('#mobile-number-error').text('');
        $('#message-error').text('');

        name = $('#name').val();
        email = $('#email').val();
        mobile = $('#mobile').val();
        query = $('#query').val();

        $.ajax({
          url: "/contactform",
          type: "POST",
          data:{
              name:name,
              email:email,
              mobile:mobile,
              query:query,
          },
          success:function(response){
            console.log(response);
            if (response) {
              $('#success-message').text(response.success);
              $("#contact-form")[0].reset();
            }
          },
          error: function(response) {
              $('#name-error').text(response.responseJSON.errors.name);
              $('#email-error').text(response.responseJSON.errors.email);
              $('#mobile-number-error').text(response.responseJSON.errors.mobile);
              $('#message-error').text(response.responseJSON.errors.query);
          }
         });
        });
      </script>
@endsection