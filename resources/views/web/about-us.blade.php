@extends('web.layouts.app')
@section('title')
Damskart Business - About Us
@endsection
@section('css')
@endsection

@section('content') 

<header>

  <section class="exp-banner-head primary-overlay" id="exp-inner-banner">

    <div class="container">

      <div class="section-title-center exp-banner-title">

        <h1 class="text-white">About Us</h1>

        <p class="text-white">Home<span class="exp-primary-color">About Us</span></p>

      </div>

    </div>

  </section>

</header>



<section class="section-padding exp-product-section exp-about-product1">

  <div class="container-wrapper">

    <div class="container">

      <div class="row">

        <div class="col-lg-6 col-content-center">

          <div class="exp-product-content">

            <h3>About Damskart</h3>

            <p>Damskart is B2B Portal offers its Distributor/Retailer the facility of Online Recharge, SMS Recharge, and Mobile App (Android), AEPS, BBPS, Domestic Money Transfer, Travel Booking,Pan Card .</p>
            <p>We have a special Recharge and Bill Payment option for the Distributor/Retailer where they can do Recharge Mobile, DTH, Data card or bill payments of their customers for all Postpaid / Prepaid Mobile, DTH, Data-card, and Bill Payment monthly For All Major Operators</p>
            <p>Thus began the journey of Damskart.com, Veerendra Sharma believed in the power of taking banking services to the vast majority in real time, so much that he set in motion an exit from ^d and developed solutions. What followed was a string of inventions, patented all over the world, partnerships with the largest banks in India, and setting up of a vast chain of merchant outlets to work as Business Correspondents to provide banking services to customers, specially from weaker economic sections of the society.</p>

            <button type="button" class="btn btn-primary btn-top-50">Learn More</button>

          </div>

        </div>

        <div class="col-lg-6">

          <div class="exp-img-wrapper right-side">

            <img src="{{ asset('web/assets/img/about-us-1/about.jpg')}}" alt="image">

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!--Product b-->

<!-- <section class="section-padding exp-values blue-top">

  <div class="container">

    <div class="section-title-center">

      <h3 class="text-white">Your title goes here...</h3>

      <p class="text-white">orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>

    </div>

    <div class="row">

      <div class="col-lg-3 col-md-6 col-sm-6">

        <div class="exp-values-item">

          <div class="icon"><i class="flaticon-analytics"></i></div>

          <div class="exp-values-content">

          <h6>Your title</h6>

          <p>Apparent was a dead black had ceased twinkle.</p>

          </div>

        </div>

      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">

        <div class="exp-values-item">

          <div class="icon"><i class="flaticon-sale"></i></div>

         <div class="exp-values-content">

          <h6>Your title</h6>

          <p>Apparent was a dead black had ceased twinkle.</p>

          </div>

        </div>

      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">

        <div class="exp-values-item">

          <div class="icon"><i class="flaticon-support"></i></div>

         <div class="exp-values-content">

          <h6>Your title</h6>

          <p>Apparent was a dead black had ceased twinkle.</p>

          </div>

        </div>

      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">

        <div class="exp-values-item">

          <div class="icon"><i class="flaticon-review-1"></i></div>

          <div class="exp-values-content">

          <h6>Testimonials</h6>

          <p>Apparent was a dead black had ceased twinkle.</p>

          </div>

        </div>

      </div>

    </div>

  </div>

</section> -->

@endsection

@section('scripts')
@endsection