---
title: Damskart API Reference

language_tabs:
- curl
- javascript
- bash
- python

includes:

search: true

toc_footers:
- <a href='#'>Powered by Damskart</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

<!-- END_INFO -->

#ICICI AEPS


<!-- START_e8eab00031514b11ace93dddde4ebde2 -->
## ICICI AEPS Withdraw Money

> Example request:

```curl

$url = "https://manage.damskart.com/apidetails/withdrawICICI";
$data = [
            'service_name' => 'fugit',
            'device_name' => 'qui',
            'mobile_number' => 'illo',
            'aadhar_number' => 'voluptas',
            'amount' => 'libero',
            'bank' => 'illum',
        ];

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);
```

```javascript
const url = new URL(
    "https://manage.damskart.com/apidetails/withdrawICICI"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "service_name": "fugit",
    "device_name": "qui",
    "mobile_number": "illo",
    "aadhar_number": "voluptas",
    "amount": "libero",
    "bank": "illum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```bash
curl -X POST \
    "https://manage.damskart.com/apidetails/withdrawICICI" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"service_name":"fugit","device_name":"qui","mobile_number":"illo","aadhar_number":"voluptas","amount":"libero","bank":"illum"}'

```

```python
import requests
import json

url = 'https://manage.damskart.com/apidetails/withdrawICICI'
payload = {
    "service_name": "fugit",
    "device_name": "qui",
    "mobile_number": "illo",
    "aadhar_number": "voluptas",
    "amount": "libero",
    "bank": "illum"
}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}
response = requests.request('POST', url, headers=headers, json=payload)
response.json()
```


> Example response (200):

```json
{
    "success": "Form is successfully submitted!"
}
```
> Example response (422):

```json
{
    "error": "Service name is not correct"
}
```

### HTTP Request
`POST apidetails/withdrawICICI`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `service_name` | string |  required  | Service name to be chosen
        `device_name` | string |  optional  | Mantra or Morpho
        `mobile_number` | string |  optional  | Your Mobile number
        `aadhar_number` | string |  optional  | Your Aadhar number
        `amount` | string |  optional  | Aamount to be withdrawn
        `bank` | string |  optional  | Bank name
    
<!-- END_e8eab00031514b11ace93dddde4ebde2 -->


