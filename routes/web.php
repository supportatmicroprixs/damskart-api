<?php

// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| Here is where you forgotResendOTP register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'fw-block-blacklisted'], function () 
{
	//Route::get('/', 'HomeController@index');
});

/* Doc API name */
Route::post('/merchantapi/withdrawICICI', 'UserAPIController@withdrawICICI')->name('api-withdrawICICI');
Route::post('/merchantapi/balanceEnquiryICICI', 'UserAPIController@balanceEnquiryICICI')->name('api-balanceEnquiryICICI');
Route::post('/merchantapi/aadharPayICICI', 'UserAPIController@aadharPayICICI')->name('api-aadharPayICICI');
Route::post('/merchantapi/miniStatementICICI', 'UserAPIController@miniStatementICICI')->name('api-miniStatementICICI');