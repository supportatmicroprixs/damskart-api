<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Venturecraft\\Revisionable' => array($vendorDir . '/venturecraft/revisionable/src'),
    'Snowplow\\RefererParser' => array($vendorDir . '/snowplow/referer-parser/php/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'Mpociot' => array($vendorDir . '/mpociot/reflection-docblock/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Detection' => array($vendorDir . '/mobiledetect/mobiledetectlib/namespaced'),
    'Bllim\\Datatables' => array($vendorDir . '/pragmarx/datatables/src'),
);
