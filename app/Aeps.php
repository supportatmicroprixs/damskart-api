<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aeps extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
  

    protected $fillable = [
        'user_id',
        'status'
    ];
}
