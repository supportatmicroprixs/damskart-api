<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund_transactions extends Model
{
  
   protected $fillable = [
   	'user_id',	'created_by',	'transfer_type',	'BID','orderId','transferMode',	'amount',	'status', 'statusMessage','account','ifsc_code','upi','purpose','surcharge','txn_id','reference_no','current_balance','utrnumber','reqid','pay_type',
    ];
}
