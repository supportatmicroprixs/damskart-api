<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use \Venturecraft\Revisionable\RevisionableTrait;

use Backpack\CRUD\app\Models\Traits\CrudTrait; // <------------------------------- this one

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, RevisionableTrait;
    protected $revisionEnabled = true;

    use CrudTrait; // <----- this

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id', 'district_id', 'package_id', 'position', 'created_by', 'refer_userid', 'refer_phone', 'refer_name', 'first_name', 'last_name', 'email', 'mobile', 'password', 'gender', 'dob', 'address', 'district', 'state', 'pin_code', 'profile_pic', 'aadhar_number', 'pan_number', 'company_name', 'company_logo', 'company_address', 'company_pan', 'ewallet', 'allow_add_member', 'aeps_wallet', 'aeps_pin', 'login_pin', 'otp', 'aadhar_pic_front', 'pan_pic', 'passbook_image',  'digio_document', 'digio_kyc_status', 'digio_e_sign_status', 'digio_response', 'fcm_token', 'kyc_update', 'bank_name', 'package', 'domain', 'profile_status', 'onboarding', 'aeps_ekyc_status', 'fundtransfer', 'yesbank', 
        'kyc_status', 'ewallet_status', 'aeps_status', 'fcm_status', 'online', 'refer_by', 'last_login', 'role', 'verify',
        'status', 'notification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userRoles()
    {
        return $this->belongsTo('App\Models\Role', 'role');
    }    

    public function states()
    {
        return $this->belongsTo('App\Models\State', 'state');
    }

    public function districts()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }

    public function packages()
    {
        return $this->belongsTo('App\Models\Package', 'package_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function referUser()
    {
        return $this->belongsTo('App\User', 'refer_userid');
    }

    public function referBy()
    {
        return $this->belongsTo('App\User', 'refer_by');
    }

    public function getFullNameAttribute() {
        $fullName = "<a href='".backpack_url('user/'.$this->id.'/edit')."'>".$this->first_name ." " . $this->last_name." <br /> (".$this->mobile.")</a>";
        return $fullName;
    }

    public function getFullUserNameAttribute() {
        $fullName = $this->first_name ." " . $this->last_name." (".$this->mobile.")";
        return $fullName;
    }
}
