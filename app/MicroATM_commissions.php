<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MicroATM_commissions extends Model
{
    protected $table = "microatm_commissions";

   protected $fillable = [
       'services',
        'package_id',
        'commission',
        'status',
        'created_at',
        'updated_at',
    ];
}
