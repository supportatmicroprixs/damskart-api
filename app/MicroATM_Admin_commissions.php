<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MicroATM_Admin_commissions extends Model
{
    protected $table = "microatm_admin_commissions";

   protected $fillable = [
       'services',
        'commission',
        'status',
        'created_at',
        'updated_at',
    ];
}
