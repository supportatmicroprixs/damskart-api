<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yesbank_transactions extends Model
{
  

   protected $fillable = [
      'user_id','commission','current_balance',	'created_by',	'dms_txn_id',	'bank_user_id',	'outletid',	'transaction_id',	'mobile_no',	'amount',	'aadhaar_no',	'txntype',	'status',	'txn_status',	'device_id',	'message',	'bcid',	'agent_trid',	'rrn',	'transaction_date',	
    ];
}
