<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bbps_transactions extends Model
{
  

   protected $fillable = [
   		'user_id','created_by','biller_name','biller_id','cust_mobile','cus_name','bbps_txnid','dms_txn_id','txn_date_time','init_channel','approval_no','bill_amount','ccf','payment_mode','biller_cat_name','pay_type','commission','current_balance','bill_date','bill_period','bill_number','bill_due_date','payment_status','message','bbps_response','complaint_id','complaint_assigned','complaint_status','complaint_disposition','complaint_description',
    ];
}