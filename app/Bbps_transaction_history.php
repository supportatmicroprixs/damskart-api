<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bbps_transaction_history extends Model
{
  
   protected $table = 'bbps_transaction_history';
   protected $fillable = [
   		'bbps_transaction_id','payment_status','remarks'
    ];
}