<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ewallet_requests extends Model
{
  

   protected $fillable = [
      'user_id','created_by',	'amount',	'payment_type',	'remarks',	'receive',	'status','request_date','approved_date','received_date','dms_txn_id','proof_image','proof_txnid',
    ];
}
