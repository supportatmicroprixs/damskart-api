<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aeps_Admin_commissions extends Model
{
  
    protected $table = "aeps_admin_commissions";  

    protected $fillable = [
       'services',
'commission',
'status',
'created_at',
'updated_at',
    ];
}
