<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund_requests extends Model
{
	protected $fillable = [
		'user_id', 'customer_code', 'virtual_code', 'mode', 'utr', 'customer_account_number', 'payee_name', 'payee_account_number', 'payee_bank_ifsc', 'amount', 'remarks', 'payment_date', 'bank_transaction_number', 'transaction_message', 'status'
    ];
}
