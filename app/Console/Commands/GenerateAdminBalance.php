<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class GenerateAdminBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:admin_balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Admin Balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    

    public function handle()
    {
        $user_transactions = \DB::table('user_transactions')->whereIn('status',  ['ACCEPTED', 'Pending', 'Complete', 'successful', 'COMPLETED', 'SUCCESS', 'APPROVED'])->get();
        $admin_balance = 0;
        foreach($user_transactions as $row)
        {
            if($row->label == 'ewallet')
            {
                if($row->type == "debit")
                {
                    if($row->user_id == 1 && $row->created_by == 1)
                    {
                    }
                    else
                    {
                        $admin_balance -= $row->amount;
                    }
                }
                else if($row->type == "credit")
                {
                    if($row->user_id == 1 && $row->created_by == 1) // && ($row->transaction_type == "ICICI AEPS CW Commission" || $row->transaction_type == "Fund Transfer Commission"))
                    {

                    }
                    else if(($row->user_id > 1) && ($row->transaction_type == "AEPS FW"))
                    {

                    }
                    else
                    {
                        $admin_balance += $row->amount;
                    }
                }
            }
            else if($row->label == 'debit_fund')
            {
                if($row->user_id == 1 && $row->created_by == 1)
                {
                    //$admin_balance += $row->amount;
                }
                else
                {
                    $admin_balance -= $row->amount;
                }
            }
            else if($row->label == 'credit_fund')
            {
                if($row->user_id == 1 && $row->created_by == 1)
                {
                    //$admin_balance -= $row->amount;
                }
                else
                {
                    $admin_balance += $row->amount;
                }
            }
            else if($row->label == 'fund_transfer')
            {
                if($row->user_id == 1 && $row->created_by == 1)
                {
                }
                else
                {
                    $admin_balance -= $row->amount;
                }
            }
            else if($row->label == 'aeps_commision')
            {
                $admin_balance += $row->amount;
            }
            else if($row->label == 'aeps_request')
            {
                if($row->type == "debit")
                {
                    $admin_balance += $row->amount;
                }
                else if($row->type == "credit")
                {
                    //$admin_balance -= $row->amount;
                }
            }
            

            \DB::table('user_transactions')->where('id',$row->id)->update(['admin_balance' => $admin_balance]);
        }
        echo "admin balance generated successfully";
        

    }
}
