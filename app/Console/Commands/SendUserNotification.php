<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Notifications;
use App\User;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendUserNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:user_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send User Notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = Notifications::where('notification_date', date('Y-m-d'))->where('notification_time', date('H:i:00'))->where('is_sent', '0')->get();

        if($notifications)
        {
            foreach($notifications as $notification)
            {
                $role = $notification->role;

                $users = User::where('role', $role)->get();

                if($users)
                {
                    foreach($users as $user)
                    {
                        $optionBuilder = new OptionsBuilder();
                        $optionBuilder->setTimeToLive(60*20);

                        $image = "https://dashboard.damskart.com/public//images/Damskart%20Logo.png";
                                
                        $title = $notification->title;
                        $message = $notification->message;

                        $notificationBuilder = new PayloadNotificationBuilder($title);
                        $notificationBuilder->setBody($message)->setIcon("xxxhdpi")->setImage($image)->setSound('default');
                        
                        $dataBuilder = new PayloadDataBuilder();
                        $dataBuilder->addData(['title' => $title, 'content' => $message, 'notify_type' => 'message']);
                        
                        $option = $optionBuilder->build();

                        $notificationBuild = $notificationBuilder->build();
                        $data = $dataBuilder->build();

                        $tokenData = array($user->fcm_token);
                                            
                        $downstreamResponse = FCM::sendTo($tokenData, $option, $notificationBuild, $data);
                    }
                }

                Notifications::where('id',$notification->id)->update(['is_sent' => '1', 'updated_at' => date('Y-m-d H:i:s')]);
            }
        }
	
	echo "notification send successfully";

    }
}
