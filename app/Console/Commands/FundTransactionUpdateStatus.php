<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Fund_transactions;
use App\User;

class FundTransactionUpdateStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fundtransaction:update_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fund Transfer Update Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    //*********** Encryption Function *********************
    public function encrypt($plainText, $key) {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
        $encryptedText = bin2hex($openMode);
        return $encryptedText;
    }
    //*********** Decryption Function *********************
    public function decrypt($encryptedText, $key) {
        $key = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
        return $decryptedText;
    }
    //*********** Padding Function *********************
    public function pkcs5_pad($plainText, $blockSize) {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }
    //********** Hexadecimal to Binary function for php 4.0 version ********
    public function hextobin($hexString) {
        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length) {
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString .= $packedString;
            }
            $count += 2;
        }
        return $binString;
    }
    
    //********** To generate ramdom String ********
    public function generateRandomString($length = 35) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $transactions = Fund_transactions::where('status',"PENDING")->get(); 

        if($transactions)
        {
            foreach($transactions as $transaction)
            {
                $dms_txn_id = $transaction->dms_txn_id;

                $apiname = 'TransactionInquiry';
                $guid = 'GUID'.date('YmdHis');

                $params = [
                    "CORPID"    =>  "574327555",
                    "USERID"    =>  "VEERENDR",
                    "AGGRID"    =>  "OTOE0077",
                    "URN"       =>  "SR194499369",
                    // "UNIQUEID" => date('YmdHis'),
                    "UNIQUEID" => $dms_txn_id
                ];

                $source = json_encode($params);

                $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                $pub_key_string=fread($fp,8192);
                fclose($fp);
                openssl_get_publickey($pub_key_string);
                openssl_public_encrypt($source,$crypttext,$pub_key_string);
                
                $request = json_encode(base64_encode($crypttext));

                $header = [
                    'apikey:1032da18afb74ccf8247f971aaae24ac',
                    'Content-type:text/plain'
                ];

                $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/TransactionInquiry';


                $file = public_path().'/logs/'.$apiname.'.txt';
                
                $log = "\n\n".'GUID - '.$guid."================================================================\n";
                $log .= 'URL - '.$httpUrl."\n\n";
                $log .= 'HEADER - '.json_encode($header)."\n\n";
                $log .= 'REQUEST - '.json_encode($params)."\n\n";
                $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                
                file_put_contents($file, $log, FILE_APPEND | LOCK_EX);


                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_PORT => "443",
                    CURLOPT_URL => $httpUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 60,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $request,
                    CURLOPT_HTTPHEADER => $header
                ));

                $response = curl_exec($curl);
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $err = curl_error($curl);
                curl_close($curl);

                $fp= fopen(public_path()."/keys/damskart.final.key","r");
                $priv_key=fread($fp,8192);
                fclose($fp);
                $res = openssl_get_privatekey($priv_key, "");

                openssl_private_decrypt(base64_decode($response), $newsource, $res);

                $transaction = json_decode($newsource,TRUE);
                //dd($transaction);

                $html = '';

                if($transaction['STATUS'] == "SUCCESS")
                {
                    Fund_transactions::where('dms_txn_id', $dms_txn_id)->update(['status' => 'ACCEPTED', 'statusMessage' => 'SUCCESS']);
                }
                else
                {
                    if(isset($transaction['MESSAGE']))
                    {
                        $msg = strstr($transaction['MESSAGE'], "Invalid Bank/Branch Identifier or network");
                        
                        if($msg != "")
                        {
                            Fund_transactions::where('dms_txn_id', $dms_txn_id)->update(['status' => 'FAILURE', 'statusMessage' => 'FAILURE']);
                        }
                        else
                        {
                            Fund_transactions::where('dms_txn_id', $dms_txn_id)->update(['status' => $transaction['STATUS'], 'statusMessage' => $transaction['STATUS']]);
                        }
                    }
                    else
                    {
                        Fund_transactions::where('dms_txn_id', $dms_txn_id)->update(['status' => $transaction['STATUS'], 'statusMessage' => $transaction['STATUS']]);
                    }
                }
            }
        }
	
        echo "fund transaction status updated successfully";

    }
}
