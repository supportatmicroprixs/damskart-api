<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class GenerateCibReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:cib_report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate CIB Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    

    public function handle()
    {
        $apiname = 'AccountStatement';
        $guid = 'GUID'.date('YmdHis');

        $params = [
          "CORPID"    =>  "574327555",
          "USERID"    =>  "VEERENDR",
          "AGGRID"    =>  "OTOE0077",
          "URN"       =>  "SR194499369",
          "ACCOUNTNO" => "678405600727",
          "FROMDATE" => date("d-m-Y", strtotime('-7 days')),
          "TODATE" => date("d-m-Y")
        ];

        //dd($params);

        $source = json_encode($params);

        $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
        $pub_key_string=fread($fp,8192);
        fclose($fp);
        openssl_get_publickey($pub_key_string);
        openssl_public_encrypt($source,$crypttext,$pub_key_string);

        $request = json_encode(base64_encode($crypttext));

        $header = [
          'apikey:1032da18afb74ccf8247f971aaae24ac',
          'Content-type:text/plain'
        ];

        $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/AccountStatement';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_PORT => "443",
          CURLOPT_URL => $httpUrl,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 60,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $request,
          CURLOPT_HTTPHEADER => $header
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        curl_close($curl);

        $fp= fopen(public_path()."/keys/damskart.final.key","r");
        $priv_key=fread($fp,8192);
        fclose($fp);
        $res = openssl_get_privatekey($priv_key, "");
        $data = json_decode($response, true);

        $encryptedKey = base64_decode($data['encryptedKey']);
        $encryptedData = base64_decode($data['encryptedData']);
        openssl_private_decrypt($encryptedKey, $key, $priv_key);
        $encData = openssl_decrypt($encryptedData,"aes-128-cbc",$key,OPENSSL_PKCS1_PADDING);
        $newsource = substr($encData, 16); 

        $data = json_decode($newsource, TRUE);

        $cibTransactions = $data['Record'];

        if($cibTransactions)
        {
            foreach($cibTransactions as $cibtransaction)
            {
                $transaction_id = $cibtransaction['TRANSACTIONID'];
                $remarks = $cibtransaction['REMARKS'];

                $isExists = \DB::table('cib_transactions')->where('transaction_id', '=', $transaction_id)->where('remarks', '=', $remarks)->count();

                if($isExists == 0)
                {
                    $cibReport = new CibReport();
                    $cibReport->cheque_number = $cibtransaction['CHEQUENO'];
                    $cibReport->txn_date = date('Y-m-d H:i:s', strtotime($cibtransaction['TXNDATE']));
                    $cibReport->remarks = $cibtransaction['REMARKS'];
                    $cibReport->amount = str_replace(",", "", $cibtransaction['AMOUNT']);
                    $cibReport->balance = str_replace(",", "", $cibtransaction['BALANCE']);
                    $cibReport->value_date = date('Y-m-d H:i:s', strtotime($cibtransaction['VALUEDATE']));
                    $cibReport->type = $cibtransaction['TYPE'];
                    $cibReport->transaction_id = $cibtransaction['TRANSACTIONID'];
                    $cibReport->save();
                }
            }
        }

        echo "admin balance generated successfully";
        

    }
}
