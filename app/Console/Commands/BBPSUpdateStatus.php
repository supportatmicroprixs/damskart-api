<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Bbps_transactions;
use App\Bbps_transaction_history;
use App\User;

class BBPSUpdateStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbps:update_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BBPS Update Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    //*********** Encryption Function *********************
    public function encrypt($plainText, $key) {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
        $encryptedText = bin2hex($openMode);
        return $encryptedText;
    }
    //*********** Decryption Function *********************
    public function decrypt($encryptedText, $key) {
        $key = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
        return $decryptedText;
    }
    //*********** Padding Function *********************
    public function pkcs5_pad($plainText, $blockSize) {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }
    //********** Hexadecimal to Binary function for php 4.0 version ********
    public function hextobin($hexString) {
        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length) {
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString .= $packedString;
            }
            $count += 2;
        }
        return $binString;
    }
    
    //********** To generate ramdom String ********
    public function generateRandomString($length = 35) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $transactions = Bbps_transactions::where('payment_status',"Pending")->get(); 

        if($transactions)
        {
            foreach($transactions as $transaction)
            {
                $bbps_txnid = $transaction->bbps_txnid;
                

                $searchtype = "TRANS_REF_ID";
                $plainText = '<transactionStatusReq>
                       <trackType>TRANS_REF_ID</trackType>
                       <trackValue>'.$bbps_txnid.'</trackValue>
                      </transactionStatusReq>';

                $key = "A549042391A0A97623646364DD5D7150";
                $encrypt_xml_data = $this->encrypt($plainText, $key);
                $data['accessCode'] = "AVAF78KX80WT10HOUD";
                $data['requestId'] = $this->generateRandomString();
                $data['encRequest'] = $encrypt_xml_data;
                $data['ver'] = "1.0";
                $data['instituteId'] = "BA44";
                $parameters = http_build_query($data);
                $url = "https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                $result = curl_exec($ch);

                $responsedata = $this->decrypt($result, $key);
                $getdata = htmlentities($responsedata);

                $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
                $getjson = json_encode($xml);
                $bbps_TransactionStatus = json_decode($getjson,TRUE);
                //dd($bbps_TransactionStatus);

                if($bbps_TransactionStatus['responseCode'] == "000" && $searchtype == "TRANS_REF_ID")
                {
                    $txndate = substr($bbps_TransactionStatus['txnList']['txnDate'], 0, -6);

                    $txnamount = $bbps_TransactionStatus['txnList']['amount'] / 100;
                    $billerId = $bbps_TransactionStatus['txnList']['billerId'];
                    $txnDate = date('d-M-Y h:i:s a', strtotime($txndate));
                    $txnDate1 = date('Y-m-d H:i:s', strtotime($txndate));
                    $txnReferenceId = $bbps_TransactionStatus['txnList']['txnReferenceId'];
                    $txnStatus = $bbps_TransactionStatus['txnList']['txnStatus'];

                    // Get BBPS transaction response
                    $bbpsTrans = Bbps_transactions::where('bbps_txnid', $bbps_txnid)->first();
                    if($bbpsTrans)
                    {
                        $txnDate = date('d-M-Y h:i:s a', strtotime($bbpsTrans->txn_date_time));

                        if($bbpsTrans->payment_status == "Pending")
                        {
                            if($txnStatus == "SUCCESS")
                            {
                                $bbps_billpay_history = new Bbps_transaction_history();
                    
                                $bbps_billpay_history->bbps_transaction_id = $bbpsTrans->id;
                                $bbps_billpay_history->payment_status = 'Successful';
                                $bbps_billpay_history->remarks = 'Successful';
                                $bbps_billpay_history->created_at = $txnDate1;
                                $bbps_billpay_history->updated_at = $txnDate1;

                                $bbps_billpay_history->save();

                                Bbps_transactions::where('bbps_txnid', $bbps_txnid)->update(['payment_status' => 'Successful', 'status_changed' => '1', 'message' => 'Successful']);

                                $current_balance = $user->ewallet - $bbpsTrans->bill_amount;
                            }
                            else if($txnStatus == "FAILURE")
                            {
                                $bbps_billpay_history = new Bbps_transaction_history();
                    
                                $bbps_billpay_history->bbps_transaction_id = $bbpsTrans->id;
                                $bbps_billpay_history->payment_status = 'Failure';
                                $bbps_billpay_history->remarks = 'Failure';
                                $bbps_billpay_history->created_at = $txnDate1;
                                $bbps_billpay_history->updated_at = $txnDate1;

                                $bbps_billpay_history->save();

                                Bbps_transactions::where('bbps_txnid', $bbps_txnid)->update(['payment_status' => 'Failure', 'status_changed' => '1', 'message' => 'Failure']);
                            }
                        }
                    }
                }
            }
        }

        $transactions = Bbps_transactions::whereNull('bbps_transaction_date')->get(); 

        if($transactions)
        {
            foreach($transactions as $transaction)
            {
                $bbps_txnid = $transaction->bbps_txnid;
                

                $searchtype = "TRANS_REF_ID";
                $plainText = '<transactionStatusReq>
                       <trackType>TRANS_REF_ID</trackType>
                       <trackValue>'.$bbps_txnid.'</trackValue>
                      </transactionStatusReq>';

                $key = "A549042391A0A97623646364DD5D7150";
                $encrypt_xml_data = $this->encrypt($plainText, $key);
                $data['accessCode'] = "AVAF78KX80WT10HOUD";
                $data['requestId'] = $this->generateRandomString();
                $data['encRequest'] = $encrypt_xml_data;
                $data['ver'] = "1.0";
                $data['instituteId'] = "BA44";
                $parameters = http_build_query($data);
                $url = "https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                $result = curl_exec($ch);

                $responsedata = $this->decrypt($result, $key);
                $getdata = htmlentities($responsedata);

                $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
                $getjson = json_encode($xml);
                $bbps_TransactionStatus = json_decode($getjson,TRUE);

                //dd($bbps_TransactionStatus);
                /*"agentId" => "CC01BA44AGTU00000001"
                "amount" => "14900"
                "billerId" => "BILAVAIRTEL001"
                "txnDate" => "2021-06-30T18:19:14+06:19"
                "txnReferenceId" => "CC011181BAAA02488401"
                "txnStatus" => "SUCCESS"
                "mobile" => "9799619106"*/

                $status = $bbps_TransactionStatus['txnList']['txnStatus'];
                $txndate = substr($bbps_TransactionStatus['txnList']['txnDate'], 0, -6);
                $txndate = str_replace("T", " ", $txndate);
                
                $txnDate = date('Y-m-d H:i:s', strtotime($txndate));

                \DB::table('bbps_transactions')->where('id', $transaction->id)->update(['bbps_transaction_date' => $txnDate, 'bbps_transaction_status' => $status]);
            }
        }
	
        echo "BBPS transaction status updated successfully";

    }
}
