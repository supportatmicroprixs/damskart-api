<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Aeps_commissions;
use App\Aeps_Admin_commissions;

use App\Bbps_commissions;
use App\Bbps_Admin_commissions;

use App\MicroATM_commissions;
use App\MicroATM_Admin_commissions;

class GenerateAdminCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:admin_commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Commission Balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function userInfo($userData, $user_id)
    {
        $userInfo = \DB::table('users')->find($user_id);
        //echo '<pre>'; print_r($userInfo); exit;
        if($userInfo->created_by == 1)
        {
            $userData[] = array('user_id' => $userInfo->id, 'created_by' => $userInfo->created_by, 'package_id' => $userInfo->package_id, 'first_name' => $userInfo->first_name, 'user_role' => $userInfo->role);

            $userInfo1 = \DB::table('users')->find(1);

            $userData[] = array('user_id' => $userInfo1->id, 'created_by' => $userInfo1->created_by, 'package_id' => $userInfo1->package_id, 'first_name' => $userInfo1->first_name, 'user_role' => $userInfo1->role);
            return $userData;
        }
        else
        {
            $userData[] = array('user_id' => $userInfo->id, 'created_by' => $userInfo->created_by, 'package_id' => $userInfo->package_id, 'first_name' => $userInfo->first_name, 'user_role' => $userInfo->role);
            return $this->userInfo($userData, $userInfo->created_by);
        }
    }

    public function handle()
    {

        // AEPS Commission
        $userComm = \DB::table('user_transactions')->where('label', 'LIKE', 'aeps_request')->where('status', 'successful')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get(); //->where('transaction_type', 'LIKE', '%ICICI AEPS%')
        //dd($userComm);

        $userData = array();
        if($userComm)
        {
            foreach($userComm as $row)
            {
                if($row->created_by != 1 && $row->user_id != 1)
                {
                    $user_id = $row->user_id;
                    $dataArr = $this->userInfo($userData, $user_id);

                    // full hierarchy
                    $transAmount = \DB::table('aeps_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_amount;
                    $servicetype = \DB::table('aeps_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_type;
                    if($servicetype == "BE")
                    {
                        $servicetype = 2; // Flat
                    }
                    else if($servicetype == "CW")
                    {
                        $servicetype = 16; // range
                    }
                    else if($servicetype == "MS")
                    {
                        $servicetype = 9; // flat
                    }
                    else if($servicetype == "M")
                    {
                        $servicetype = 1; // range
                    }

                    //echo $servicetype; exit;

                    //echo $transAmount.'<pre>'; print_r($dataArr); 

                    $totalCommission = array();
                    $commissionAmount = 0;
                    foreach($dataArr as $k => $userRow)
                    {
                        if($userRow['user_id'] > 1)
                        {
                            $isExists = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%ICICI AEPS%')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();

                            if($isExists == 0)
                            {
                                $usercommission = Aeps_commissions::where('package_id',$userRow['package_id'])->where('service_id',$servicetype)->first();

                                $commissionAmount = $commissionAmountBelow = 0;
                                if($servicetype == 16 || $servicetype == 1)
                                {
                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmount = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmount = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ($usercommission->commission_type == "flat") {
                                        $commissionAmount = $usercommission->commission;
                                    } elseif ($usercommission->commission_type == "percentage") {
                                        $commissionAmount = ($transAmount * $usercommission->commission)/100;
                                    }
                                }

                                $usercommission = Aeps_commissions::where('package_id',$dataArr[$k - 1]['package_id'])->where('service_id',$servicetype)->first();

                                if($servicetype == 16 || $servicetype == 1)
                                {
                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmountBelow = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ($usercommission->commission_type == "flat") {
                                        $commissionAmountBelow = $usercommission->commission;
                                    } elseif ($usercommission->commission_type == "percentage") {
                                        $commissionAmountBelow = ($transAmount * $usercommission->commission)/100;
                                    }
                                }

                                if($servicetype == 1)
                                {
                                    $commissionAmt = $commissionAmountBelow - $commissionAmount;
                                    $type = "credit";
                                }
                                else
                                {
                                    $commissionAmt = $commissionAmount - $commissionAmountBelow;
                                    $type = "credit";

                                    if($commissionAmt < 0)
                                    {
                                        $type = "debit";
                                    }
                                }

                                $narration = str_replace(" Success", " Commission Success", $row->narration);

                                $user = \DB::table('users')->find($userRow['user_id']);
                                if($type == "credit")
                                {
                                    $current_balance = $user->ewallet + $commissionAmt;
                                }
                                else
                                {
                                    $current_balance = $user->ewallet - $commissionAmt;   
                                }

                                \DB::table('user_transactions')->insert(['user_id' => $userRow['user_id'], 'created_by' => $userRow['created_by'], 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => $current_balance, 'admin_balance' => '', 'narration' => $narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'successful', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                                    //echo $userRow['user_id'].",".$commissionAmount."<br />";
                                //echo $userRow['user_id'].",".$userRow['package_id'].",".$dataArr[$k - 1]['package_id'].",".$commissionAmt."<br />";
                            }

                            //echo $isExists.",".$userRow['user_id'].'<br />';
                        }
                    }
                }
            }
        }

        $userComm = \DB::table('user_transactions')->where('label', 'LIKE', 'aeps_request')->where('status', 'successful')->where('user_id', '!=','1')->orderBy('transaction_id', 'DESC')->get();
        //echo '<pre>'; print_r($userComm); exit;

        if($userComm)
        {
            foreach($userComm as $row)
            {
                // full hierarchy
                $transAmount = \DB::table('aeps_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_amount;
                $servicetype = \DB::table('aeps_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_type;
                if($servicetype == "BE")
                {
                    $servicetype = 2; // Flat
                }
                else if($servicetype == "CW")
                {
                    $servicetype = 16; // range
                }
                else if($servicetype == "MS")
                {
                    $servicetype = 9; // flat
                }
                else if($servicetype == "M")
                {
                    $servicetype = 1; // range
                }

                $isExists1 = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%ICICI AEPS%')->where('transaction_type', '!=', 'AEPS FW')->where('status', 'successful')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', 1)->first();

                $isExists = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%ICICI AEPS%')->where('status', 'successful')->where('transaction_id', $row->transaction_id)->where('user_id', '1')->count();

                if($isExists == 0)
                {
                    $usercommission = Aeps_Admin_commissions::where('service_id',$servicetype)->first();

                    if($servicetype == 16 || $servicetype == 1)
                    {
                        $getcommission = json_decode($usercommission->commission);
                        $commissionAmount = $commissionAmountBelow = 0;
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmount = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmount = ($transAmount * $value->commission)/100;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ($usercommission->commission_type == "flat") {
                            $commissionAmount = $usercommission->commission;
                        } elseif ($usercommission->commission_type == "percentage") {
                            $commissionAmount = ($transAmount * $usercommission->commission)/100;
                        }
                    }

                    //echo $row->transaction_id."<br />";
                    $userInfo = \DB::table('users')->find($isExists1->user_id);
                    
                    $usercommission = Aeps_commissions::where('package_id', $userInfo->package_id)->where('service_id',$servicetype)->first();

                    if($servicetype == 16 || $servicetype == 1)
                    {
                        $getcommission = json_decode($usercommission->commission);
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmountBelow = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ($usercommission->commission_type == "flat") {
                            $commissionAmount = $usercommission->commission;
                        } elseif ($usercommission->commission_type == "percentage") {
                            $commissionAmount = ($transAmount * $usercommission->commission)/100;
                        }
                    }

                    if($servicetype == 1)
                    {
                        $commissionAmt = $commissionAmountBelow - $commissionAmount;
                        $type = "credit";
                    }
                    else
                    {
                        $commissionAmt = $commissionAmount - $commissionAmountBelow;
                        $type = "credit";

                        if($commissionAmt < 0)
                        {
                            $type = "debit";
                        }
                    }

                    $narration = str_replace(" Success", " Commission Success", $row->narration);
                    
                    \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => $narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'successful', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                        //echo $userRow['user_id'].",".$commissionAmount."<br />";
                }
            }
        }

        // Fund Transfer Commission
        $userComm = \DB::table('user_transactions')->whereIn('transaction_type', ['Fund Transfer', 'Express Payout', 'Payout'])->where('status', 'ACCEPTED')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get();

        $userData = array();
        if($userComm)
        {
            foreach($userComm as $row)
            {
                if($row->created_by != 1 && $row->user_id != 1)
                {
                    $user_id = $row->user_id;
                    $dataArr = $this->userInfo($userData, $user_id);

                    // full hierarchy
                    $transAmount = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first()->amount;
                    $servicetype = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first()->purpose;
                    if($servicetype == "payout")
                    {
                        $servicetype = 10; // Flat
                    }
                    else if($servicetype == "fundtransfer")
                    {
                        $servicetype = 6; // range
                    }
                    else if($servicetype == "Express Payout")
                    {
                        $servicetype = 3; // flat
                    }

                    if($servicetype == 10)
                    {

                    }
                    else
                    {
                        $totalCommission = array();
                        $commissionAmount = 0;

                        //echo '<pre>'; print_r($dataArr); exit;
                        foreach($dataArr as $k => $userRow)
                        {
                            if($k > 0 && $userRow['user_id'] > 1)
                            {
                                if($servicetype == 3)
                                {
                                    $isExists = \DB::table('user_transactions')->where('transaction_type', 'Express Payout Commission')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();
                                }
                                else if($servicetype == 6)
                                {
                                    $isExists = \DB::table('user_transactions')->where('transaction_type', 'Fund Transfer Commission')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();
                                }

                                if($isExists == 0)
                                {
                                    $usercommission = Aeps_commissions::where('package_id',$userRow['package_id'])->where('service_id',$servicetype)->first();

                                    $commissionAmount = $commissionAmountBelow = 0;
                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmount = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmount = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }

                                    $usercommission = Aeps_commissions::where('package_id',$dataArr[$k - 1]['package_id'])->where('service_id',$servicetype)->first();

                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmountBelow = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }

                                    $commissionAmt = $commissionAmountBelow - $commissionAmount;
                                    $type = "credit";

                                    if($commissionAmt < 0)
                                    {
                                        $type = "debit";
                                    }

                                    $user = \DB::table('users')->find($userRow['user_id']);
                                    if($type == "credit")
                                    {
                                        $current_balance = $user->ewallet + $commissionAmt;
                                    }
                                    else
                                    {
                                        $current_balance = $user->ewallet - $commissionAmt;   
                                    }

                                    \DB::table('user_transactions')->insert(['user_id' => $userRow['user_id'], 'created_by' => $userRow['created_by'], 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => $current_balance, 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'ACCEPTED', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                                        //echo $userRow['user_id'].",".$commissionAmount."<br />";
                                    //echo $userRow['user_id'].",".$userRow['package_id'].",".$dataArr[$k - 1]['package_id'].",".$commissionAmt."<br />";
                                }

                                //echo $isExists.",".$userRow['user_id'].'<br />';
                            }
                        }
                    }
                }
            }
        }
        
        $userComm = \DB::table('user_transactions')->whereIn('transaction_type', ['Fund Transfer', 'Express Payout', 'Payout'])->where('status', 'ACCEPTED')->where('user_id', '!=','1')->orderBy('transaction_id', 'DESC')->get();
        //echo '<pre>'; print_r($userComm); exit;

        if($userComm)
        {
            foreach($userComm as $row)
            {
                // full hierarchy
                $transAmount = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first()->amount;
                $servicetype = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first()->purpose;
                if($servicetype == "payout")
                {
                    $servicetype = 10; // Flat
                }
                else if($servicetype == "fundtransfer")
                {
                    $servicetype = 6; // range
                }
                else if($servicetype == "Express Payout")
                {
                    $servicetype = 3; // flat
                }
                
                //echo $row->transaction_id.",".$servicetype."<br />";
                if($servicetype == 10)
                {
                    $isExists1 = \DB::table('user_transactions')->where('narration', 'LIKE', '%Payout Success%')->where('status', 'ACCEPTED')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->first();

                    $isExists = \DB::table('user_transactions')->where('transaction_type', 'Payout Commission')->where('status', 'ACCEPTED')->where('transaction_id', $row->transaction_id)->where('user_id', '1')->count();

                    if($isExists == 0)
                    {
                        $usercommission = Aeps_Admin_commissions::where('service_id',$servicetype)->first();

                        $getcommission = json_decode($usercommission->commission);
                        $commissionAmount = $commissionAmountBelow = 0;
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmount = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmount = ($transAmount * $value->commission)/100;
                                }
                            }
                        }

                        $userInfo = \DB::table('users')->find($isExists1->user_id);
                        
                    
                        $usercommission = Aeps_commissions::where('package_id', $userInfo->package_id)->where('service_id',$servicetype)->first();

                        $getcommission = json_decode($usercommission->commission);
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmountBelow = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                }
                            }
                        }

                        $commissionAmt = $commissionAmountBelow - $commissionAmount;

                        if($commissionAmt < 0)
                        {
                            $commissionAmt = -$commissionAmt;
                            $type = "debit";
                        }
                        else
                        {
                            $type = "credit";
                        }
                        echo $row->transaction_id.">".$commissionAmount.",".$commissionAmountBelow.",".$commissionAmt.'<br />';

                            \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => str_replace("Payout", "Payout Commission", $row->narration), 'label' => 'ewallet', 'type' => $type, 'status' => 'ACCEPTED', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                            //echo $userRow['user_id'].",".$commissionAmount."<br />";
                    }
                }
                else
                {
                    if($servicetype == 3)
                    {
                        $isExists = \DB::table('user_transactions')->where('transaction_type', 'Express Payout Commission')->where('transaction_id', $row->transaction_id)->where('user_id', 1)->count();
                        $isExists1 = \DB::table('user_transactions')->where('narration', 'LIKE', '%Express Payout Success%')->where('status', 'ACCEPTED')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', '1')->first();
                    }
                    else if($servicetype == 6)
                    {
                        $isExists = \DB::table('user_transactions')->where('transaction_type', 'Fund Transfer Commission')->where('transaction_id', $row->transaction_id)->where('user_id', 1)->count();
                        $isExists1 = \DB::table('user_transactions')->where('narration', 'LIKE', '%Fund Transfer Success%')->where('status', 'ACCEPTED')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', '1')->first();
                    }

                    if($isExists == 0)
                    {
                        $usercommission = Aeps_Admin_commissions::where('service_id',$servicetype)->first();

                        $getcommission = json_decode($usercommission->commission);
                        $commissionAmount = $commissionAmountBelow = 0;
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmount = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmount = ($transAmount * $value->commission)/100;
                                }
                            }
                        }

                        $userInfo = \DB::table('users')->find($isExists1->user_id);
                        
                    
                        $usercommission = Aeps_commissions::where('package_id', $userInfo->package_id)->where('service_id',$servicetype)->first();

                        $getcommission = json_decode($usercommission->commission);
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmountBelow = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                }
                            }
                        }

                        $commissionAmt = $commissionAmountBelow - $commissionAmount;
                        $type = "credit";
                        //echo $row->transaction_id.">".$commissionAmount.",".$commissionAmountBelow.",".$commissionAmt.'<br />';

                        if($commissionAmt < 0)
                        {
                            $type = "debit";
                        }

                        \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'ACCEPTED', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                            //echo $userRow['user_id'].",".$commissionAmount."<br />";
                    }
                }
            }
        }

        // MICRO ATM
        $userComm = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%MICROATM%')->where('status', 'successful')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get();
        
        $userData = array();
        if($userComm)
        {
            foreach($userComm as $row)
            {
                if($row->created_by != 1 && $row->user_id != 1)
                {
                    $user_id = $row->user_id;
                    $dataArr = $this->userInfo($userData, $user_id);

                    // full hierarchy
                    $transAmount = \DB::table('microatm_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_amount;
                    $servicetype = \DB::table('microatm_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_type;
                    if($servicetype == "BAL")
                    {
                        $servicetype = 2; // Flat
                    }
                    else if($servicetype == "WDLS")
                    {
                        $servicetype = 16; // range
                    }
                    else if($servicetype == "MS")
                    {
                        $servicetype = 9; // flat
                    }
                    else if($servicetype == "M")
                    {
                        $servicetype = 1; // range
                    }

                    //echo $servicetype; exit;

                    //echo '<pre>'; print_r($dataArr); exit;

                    //echo $transAmount.'<pre>'; print_r($dataArr); 

                    $totalCommission = array();
                    $commissionAmount = 0;
                    foreach($dataArr as $k => $userRow)
                    {
                        if($userRow['user_id'] > 1)
                        {
                            $isExists = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%MICROATM%')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();

                            if($isExists == 0)
                            {
                                //echo $transAmount.'<pre>'; print_r($dataArr);


                                $usercommission = MicroATM_commissions::where('package_id',$userRow['package_id'])->where('service_id',$servicetype)->first();

                                $commissionAmount = $commissionAmountBelow = 0;
                                if($servicetype == 16 || $servicetype == 1)
                                {
                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmount = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmount = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ($usercommission->commission_type == "flat") {
                                        $commissionAmount = $usercommission->commission;
                                    } elseif ($usercommission->commission_type == "percentage") {
                                        $commissionAmount = ($transAmount * $usercommission->commission)/100;
                                    }
                                }

                                $usercommission = MicroATM_commissions::where('package_id',$dataArr[$k - 1]['package_id'])->where('service_id',$servicetype)->first();

                                if($servicetype == 16 || $servicetype == 1)
                                {
                                    $getcommission = json_decode($usercommission->commission);
                                    foreach ($getcommission as $key => $value) {
                                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                            // Get commission
                                            if ($value->commission_type == "flat") {
                                                $commissionAmountBelow = $value->commission;
                                            } elseif ($value->commission_type == "percentage") {
                                                $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ($usercommission->commission_type == "flat") {
                                        $commissionAmountBelow = $usercommission->commission;
                                    } elseif ($usercommission->commission_type == "percentage") {
                                        $commissionAmountBelow = ($transAmount * $usercommission->commission)/100;
                                    }
                                }

                                if($servicetype == 1)
                                {
                                    $commissionAmt = $commissionAmountBelow - $commissionAmount;
                                    $type = "credit";
                                }
                                else
                                {
                                    $commissionAmt = $commissionAmount - $commissionAmountBelow;
                                    $type = "credit";

                                    if($commissionAmt < 0)
                                    {
                                        $type = "debit";
                                    }
                                }
                                //secho $commissionAmt; exit;

                                $user = \DB::table('users')->find($userRow['user_id']);
                                if($type == "credit")
                                {
                                    $current_balance = $user->ewallet + $commissionAmt;
                                }
                                else
                                {
                                    $current_balance = $user->ewallet - $commissionAmt;   
                                }

                                \DB::table('user_transactions')->insert(['user_id' => $userRow['user_id'], 'created_by' => $userRow['created_by'], 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => $current_balance, 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'successful', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                                    //echo $userRow['user_id'].",".$commissionAmount."<br />";
                                //echo $userRow['user_id'].",".$userRow['package_id'].",".$dataArr[$k - 1]['package_id'].",".$commissionAmt."<br />";
                            }

                            //echo $isExists.",".$userRow['user_id'].'<br />';
                        }
                    }
                }
            }
        }

        $userComm = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%MICROATM%')->where('status', 'successful')->where('user_id', '!=','1')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get();
        //echo '<pre>'; print_r($userComm); exit;

        if($userComm)
        {
            foreach($userComm as $row)
            {
                // full hierarchy
                $transAmount = \DB::table('microatm_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_amount;
                $servicetype = \DB::table('microatm_txn_news')->where('dms_txn_id', $row->transaction_id)->first()->transaction_type;
                if($servicetype == "BAL")
                {
                    $servicetype = 2; // Flat
                }
                else if($servicetype == "WDLS")
                {
                    $servicetype = 16; // range
                }
                else if($servicetype == "MS")
                {
                    $servicetype = 9; // flat
                }
                else if($servicetype == "M")
                {
                    $servicetype = 1; // range
                }

                $isExists1 = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%MICROATM%')->where('status', 'successful')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', 1)->first();

                $isExists = \DB::table('user_transactions')->where('transaction_type', 'LIKE', '%MICROATM%')->where('status', 'successful')->where('transaction_id', $row->transaction_id)->where('user_id', '1')->count();

                if($isExists == 0)
                {
                    $usercommission = MicroATM_Admin_commissions::where('service_id',$servicetype)->first();

                    $commissionAmount = $commissionAmountBelow = 0;
                    if($servicetype == 16 || $servicetype == 1)
                    {
                        $getcommission = json_decode($usercommission->commission);
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmount = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmount = ($transAmount * $value->commission)/100;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ($usercommission->commission_type == "flat") {
                            $commissionAmount = $usercommission->commission;
                        } elseif ($usercommission->commission_type == "percentage") {
                            $commissionAmount = ($transAmount * $usercommission->commission)/100;
                        }
                    }

                    //echo $row->transaction_id.",".$isExists1->user_id."<br />";
                    $userInfo = \DB::table('users')->find($isExists1->user_id);
                    
                    $usercommission = MicroATM_commissions::where('package_id', $userInfo->package_id)->where('service_id',$servicetype)->first();

                    if($servicetype == 16 || $servicetype == 1)
                    {
                        $getcommission = json_decode($usercommission->commission);
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                // Get commission
                                if ($value->commission_type == "flat") {
                                    $commissionAmountBelow = $value->commission;
                                } elseif ($value->commission_type == "percentage") {
                                    $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ($usercommission->commission_type == "flat") {
                            $commissionAmount = $usercommission->commission;
                        } elseif ($usercommission->commission_type == "percentage") {
                            $commissionAmount = ($transAmount * $usercommission->commission)/100;
                        }
                    }

                    if($servicetype == 1)
                    {
                        $commissionAmt = $commissionAmountBelow - $commissionAmount;
                        $type = "credit";
                    }
                    else
                    {
                        $commissionAmt = $commissionAmount - $commissionAmountBelow;

                        $type = "credit";

                        if($commissionAmt < 0)
                        {
                            $type = "debit";
                        }
                    }

                    \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'successful', 'transaction_type' => $row->transaction_type. " Commission", 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                        //echo $userRow['user_id'].",".$commissionAmount."<br />";
                }
            }
        }


        // UPI
        $userComm = \DB::table('user_transactions')->where('narration', 'LIKE', '%UPI Payment Comm. Info:%')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get();

        $userData = array();
        if($userComm)
        {
            foreach($userComm as $row)
            {
                if($row->created_by != 1 && $row->user_id != 1)
                {
                    $user_id = $row->user_id;
                    $dataArr = $this->userInfo($userData, $user_id);

                    // full hierarchy
                    $transAmount = \DB::table('upi_qr_txns')->where('dms_txn_id', $row->transaction_id)->first()->payer_amount;
                    $servicetype = \DB::table('upi_qr_txns')->where('dms_txn_id', $row->transaction_id)->first()->txn_type;
                    if($servicetype == "my_qr")
                    {
                        $servicetype = 14;
                    }
                    else if($servicetype == "send_request")
                    {
                        $servicetype = 15;
                    }
                    else
                    {
                        $servicetype = 13;
                    }

                    //echo $transAmount.'<pre>'; print_r($dataArr); 

                    $totalCommission = array();
                    $commissionAmount = 0;
                    foreach($dataArr as $k => $userRow)
                    {
                        if($userRow['user_id'] > 1)
                        {
                            $isExists = \DB::table('user_transactions')->where('narration', 'LIKE', '%UPI Payment Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();

                            if($isExists == 0)
                            {
                                $usercommission = Aeps_commissions::where('package_id',$userRow['package_id'])->where('service_id',$servicetype)->first();

                                $getcommission = json_decode($usercommission->commission);
                                $commissionAmount = $commissionAmountBelow = 0;
                                foreach ($getcommission as $key => $value) {
                                    if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                        // Get commission
                                        if ($value->commission_type == "flat") {
                                            $commissionAmount = $value->commission;
                                        } elseif ($value->commission_type == "percentage") {
                                            $commissionAmount = ($transAmount * $value->commission)/100;
                                        }
                                    }
                                }

                                $usercommission = Aeps_commissions::where('package_id',$dataArr[$k - 1]['package_id'])->where('service_id',$servicetype)->first();

                                $getcommission = json_decode($usercommission->commission);
                                foreach ($getcommission as $key => $value) {
                                    if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                                        // Get commission
                                        if ($value->commission_type == "flat") {
                                            $commissionAmountBelow = $value->commission;
                                        } elseif ($value->commission_type == "percentage") {
                                            $commissionAmountBelow = ($transAmount * $value->commission)/100;
                                        }
                                    }
                                }

                                $commissionAmt = $commissionAmount - $commissionAmountBelow;

                                $type = 'credit';
                                if($commissionAmt < 0)
                                {
                                    $type = "debit";
                                }

                                $user = \DB::table('users')->find($userRow['user_id']);
                                if($type == "credit")
                                {
                                    $current_balance = $user->ewallet + $commissionAmt;
                                }
                                else
                                {
                                    $current_balance = $user->ewallet - $commissionAmt;   
                                }

                                \DB::table('user_transactions')->insert(['user_id' => $userRow['user_id'], 'created_by' => $userRow['created_by'], 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => $current_balance, 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'SUCCESS', 'transaction_type' => 'UPI Payment Commission', 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                                    //echo $userRow['user_id'].",".$commissionAmount."<br />";
                                //echo $userRow['user_id'].",".$userRow['package_id'].",".$dataArr[$k - 1]['package_id'].",".$commissionAmt."<br />";
                            }

                            //echo $isExists.",".$userRow['user_id'].'<br />';
                        }
                    }
                }
            }
        }

        $userComm = \DB::table('user_transactions')->where('narration', 'LIKE', '%UPI Payment Comm. Info:%')->where('user_id', '!=','1')->orderBy('transaction_id', 'DESC')->get();
        //dd($userComm);

        if($userComm)
        {
            foreach($userComm as $row)
            {
                // full hierarchy
                $transAmount = \DB::table('upi_qr_txns')->where('dms_txn_id', $row->transaction_id)->first()->payer_amount;
                $servicetype = \DB::table('upi_qr_txns')->where('dms_txn_id', $row->transaction_id)->first()->txn_type;
                if($servicetype == "my_qr")
                {
                    $servicetype = 14;
                }
                else if($servicetype == "send_request")
                {
                    $servicetype = 15;
                }
                else
                {
                    $servicetype = 13;
                }

                $isExists1 = \DB::table('user_transactions')->where('narration', 'LIKE', '%UPI Payment Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', 1)->first();

                $isExists = \DB::table('user_transactions')->where('narration', 'LIKE', '%UPI Payment Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', '1')->count();

                if($isExists == 0)
                {
                    $usercommission = Aeps_Admin_commissions::where('service_id',$servicetype)->first();

                    $getcommission = json_decode($usercommission->commission);
                    $commissionAmount = $commissionAmountBelow = 0;
                    foreach ($getcommission as $key => $value) {
                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                            // Get commission
                            if ($value->commission_type == "flat") {
                                $commissionAmount = $value->commission;
                            } elseif ($value->commission_type == "percentage") {
                                $commissionAmount = ($transAmount * $value->commission)/100;
                            }
                        }
                    }

                    $userInfo = \DB::table('users')->find($isExists1->user_id);
                    
                    $usercommission = Aeps_commissions::where('package_id', $userInfo->package_id)->where('service_id',$servicetype)->first();

                    $getcommission = json_decode($usercommission->commission);
                    foreach ($getcommission as $key => $value) {
                        if ($value->start_price <= $transAmount && $value->end_price >= $transAmount) {
                            // Get commission
                            if ($value->commission_type == "flat") {
                                $commissionAmountBelow = $value->commission;
                            } elseif ($value->commission_type == "percentage") {
                                $commissionAmountBelow = ($transAmount * $value->commission)/100;
                            }
                        }
                    }

                    $commissionAmt = $commissionAmount - $commissionAmountBelow;
                    
                    $type = 'credit';
                    if($commissionAmt < 0)
                    {
                        $type = "debit";
                    }

                    \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'SUCCESS', 'transaction_type' => 'UPI Payment Commission', 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                        //echo $userRow['user_id'].",".$commissionAmount."<br />";
                }
            }
        }

        // Biller
        $userComm = \DB::table('user_transactions')->where('narration', 'LIKE', '%Bill Payment - Mobile Prepaid Comm. Info%')->groupBy('transaction_id')->orderBy('transaction_id', 'DESC')->get();
        
        $userData = array();
        if($userComm)
        {
            foreach($userComm as $row)
            {
                if($row->created_by != 1 && $row->user_id != 1)
                {
                    $user_id = $row->user_id;
                    $dataArr = $this->userInfo($userData, $user_id);

                    // full hierarchy
                    $transAmount = \DB::table('bbps_transactions')->where('dms_txn_id', $row->transaction_id)->first()->bill_amount;
                    $biller_id = \DB::table('bbps_transactions')->where('dms_txn_id', $row->transaction_id)->first()->biller_id;
                    //dd($transAmount);

                    //echo $biller_id;
                    echo '<pre>'; print_r($dataArr);
                    
                    $totalCommission = array();
                    $commissionAmount = 0;
                    foreach($dataArr as $k => $userRow)
                    {
                        if($userRow['user_id'] > 1)
                        {
                            $isExists = \DB::table('user_transactions')->where('narration', 'LIKE', '%Bill Payment - Mobile Prepaid Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', $userRow['user_id'])->count();
                            
                            if($isExists == 0)
                            {
                                $usercommission = Bbps_commissions::where('package_id',$userRow['package_id'])->where('bbps_biller_id',$biller_id)->first();

                                $commissionAmount = $commissionAmountBelow = 0;
                                if ($usercommission->commission_type == "flat") {
                                    $commissionAmount = $usercommission->commission;
                                } elseif ($usercommission->commission_type == "percentage") {
                                    $commissionAmount = ($transAmount * $usercommission->commission)/100;
                                }

                                $usercommission = Bbps_commissions::where('package_id',$dataArr[$k - 1]['package_id'])->where('bbps_biller_id',$biller_id)->first();

                                // Get commission
                                if ($usercommission->commission_type == "flat") {
                                    $commissionAmountBelow = $usercommission->commission;
                                } elseif ($usercommission->commission_type == "percentage") {
                                    $commissionAmountBelow = ($transAmount * $usercommission->commission)/100;
                                }

                                $commissionAmt = $commissionAmount - $commissionAmountBelow;

                                //echo $commissionAmt."==";

                                $type = 'credit';
                                if($commissionAmt < 0)
                                {
                                    $type = "debit";
                                }

                                $user = \DB::table('users')->find($userRow['user_id']);
                                if($type == "credit")
                                {
                                    $current_balance = $user->ewallet + $commissionAmt;
                                }
                                else
                                {
                                    $current_balance = $user->ewallet - $commissionAmt;   
                                }

                                \DB::table('user_transactions')->insert(['user_id' => $userRow['user_id'], 'created_by' => $userRow['created_by'], 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => $current_balance, 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'Successful', 'transaction_type' => $row->transaction_type, 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                                    //echo $userRow['user_id'].",".$commissionAmount."<br />";
                                //echo $userRow['user_id'].",".$userRow['package_id'].",".$dataArr[$k - 1]['package_id'].",".$commissionAmt."<br />";
                            }

                            //echo $isExists.",".$userRow['user_id'].'<br />';
                        }
                    }
                }
            }
        }

        $userComm = \DB::table('user_transactions')->where('narration', 'LIKE', '%Bill Payment - Mobile Prepaid Comm. Info%')->where('user_id', '!=','1')->orderBy('transaction_id', 'DESC')->get();
        //dd($userComm);

        if($userComm)
        {
            foreach($userComm as $row)
            {
                // full hierarchy
                $transAmount = \DB::table('bbps_transactions')->where('dms_txn_id', $row->transaction_id)->first()->bill_amount;
                $biller_id = \DB::table('bbps_transactions')->where('dms_txn_id', $row->transaction_id)->first()->biller_id;

                $isExists1 = \DB::table('user_transactions')->where('narration', 'LIKE', '%Bill Payment - Mobile Prepaid Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', '!=', 1)->where('created_by', 1)->first();
                $isExists = \DB::table('user_transactions')->where('narration', 'LIKE', '%Bill Payment - Mobile Prepaid Comm. Info%')->where('transaction_id', $row->transaction_id)->where('user_id', '1')->count();

                if($isExists == 0)
                {
                    $usercommission = Bbps_Admin_commissions::where('bbps_biller_id',$biller_id)->first();

                    $commissionAmount = $commissionAmountBelow = 0;
                    if ($usercommission->commission_type == "flat") {
                        $commissionAmount = $usercommission->commission;
                    } elseif ($usercommission->commission_type == "percentage") {
                        $commissionAmount = ($transAmount * $usercommission->commission)/100;
                    }

                    $userInfo = \DB::table('users')->find($isExists1->user_id);
                    
                    $usercommission = Bbps_commissions::where('package_id', $userInfo->package_id)->where('bbps_biller_id',$biller_id)->first();

                    if ($usercommission->commission_type == "flat") {
                        $commissionAmountBelow = $usercommission->commission;
                    } elseif ($usercommission->commission_type == "percentage") {
                        $commissionAmountBelow = ($transAmount * $usercommission->commission)/100;
                    }

                    $commissionAmt = $commissionAmount - $commissionAmountBelow;
                    $type = "credit";
                    if($commissionAmt < 0)
                    {
                        $type = "debit";
                    }

                    \DB::table('user_transactions')->insert(['user_id' => 1, 'created_by' => 1, 'transaction_id' => $row->transaction_id, 'amount' => $commissionAmt, 'current_balance' => '', 'admin_balance' => '', 'narration' => $row->narration, 'label' => 'ewallet', 'type' => $type, 'status' => 'SUCCESS', 'transaction_type' => $row->transaction_type, 'date' => $row->date, 'created_at' => date('Y-m-d H:i:s')]);
                        //echo $userRow['user_id'].",".$commissionAmount."<br />";
                }
            }
        }

        // Admin commission
        $userComm = \DB::table('user_transactions')->where(['user_id' => 1,'created_by' => 1, 'label' => 'ewallet'])->get();
        
        $admin_balance = 0;
        if($userComm)
        {
            foreach($userComm as $row)
            {
                // Check for AEPS ICICI
                if(substr($row->transaction_type, 0, 10) == "ICICI AEPS")
                {
                    $transactionInfo = \DB::table('aeps_txn_news')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    // Check if exists

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }
                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by, 'package_id' => $userInfo->package_id,
                            'table_name' => 'aeps_txn_news', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->request_transaction_time1,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => '', 'aadhar_number' => $transactionInfo->aadhar_number,
                            'transaction_category' => 'AEPS', 'transaction_type' => $transactionInfo->transaction_type,
                            'original_transaction_id' => $transactionInfo->bankRRN, 'transaction_amount' => $transactionInfo->transaction_amount,
                            'commission' => $transactionInfo->commission, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->transaction_status,
                            'message' => $transactionInfo->message, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "ICICI Records <br />";
                }
                else if(substr($row->transaction_type, 0, 8) == "MICROATM")
                {
                    $transactionInfo = \DB::table('microatm_txn_news')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }

                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by, 'package_id' => $userInfo->package_id,
                            'table_name' => 'microatm_txn_news', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->request_transaction_time1,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => '', 'aadhar_number' => $transactionInfo->aadhar_number,
                            'transaction_category' => 'MICROATM', 'transaction_type' => $transactionInfo->transaction_type,
                            'original_transaction_id' => $transactionInfo->bankRRN, 'transaction_amount' => $transactionInfo->transaction_amount,
                            'commission' => $transactionInfo->commission, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->transaction_status,
                            'message' => $transactionInfo->message, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    
                    echo "MIcroATM Records <br />";
                }
                else if($row->transaction_type == "Express Payout Commission")
                {
                    $transactionInfo = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $customerInfo = \DB::table('customer_accounts')->where('BID', $transactionInfo->BID)->first();

                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }

                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by,  'package_id' => $userInfo->package_id,
                            'table_name' => 'fund_transactions', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->created_at,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => $customerInfo->benificiary_name, 'aadhar_number' => $customerInfo->account,
                            'transaction_category' => 'Fund Transaction', 'transaction_type' => 'Express Payout',
                            'original_transaction_id' => $transactionInfo->utrnumber, 'transaction_amount' => $transactionInfo->amount,
                            'commission' => $transactionInfo->surcharge, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->status,
                            'message' => $transactionInfo->statusMessage, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "Express Payout Records <br />";
                }
                else if($row->transaction_type == "Bill Payment")
                {
                    $transactionInfo = \DB::table('bbps_transactions')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $bbps_response = json_decode($transactionInfo->bbps_response);
                        $phone_number = "";
                        if($transactionInfo->biller_cat_name == "Mobile Prepaid")
                        {
                            $phone_number = $bbps_response->inputParams->input[0]->paramValue;
                            //echo '<pre>'; print_r($phone_number); exit;
                        }

                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }

                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by, 'package_id' => $userInfo->package_id,
                            'table_name' => 'bbps_transactions', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->created_at,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => '', 'aadhar_number' => $phone_number,
                            'transaction_category' => 'BBPS', 'transaction_type' => $transactionInfo->biller_cat_name,
                            'original_transaction_id' => $transactionInfo->bbps_txnid, 'transaction_amount' => $transactionInfo->bill_amount,
                            'commission' => $transactionInfo->commission, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->payment_status,
                            'message' => $transactionInfo->message, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "Bill Payment Records <br />";
                }
                else if($row->transaction_type == "Fund Transfer Commission")
                {
                    $transactionInfo = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $customerInfo = \DB::table('customer_accounts')->where('BID', $transactionInfo->BID)->first();

                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }

                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by,  'package_id' => $userInfo->package_id,
                            'table_name' => 'fund_transactions', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->created_at,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => $customerInfo->benificiary_name, 'aadhar_number' => $customerInfo->account,
                            'transaction_category' => 'Fund Transaction', 'transaction_type' => 'Fund Transfer',
                            'original_transaction_id' => $transactionInfo->utrnumber, 'transaction_amount' => $transactionInfo->amount,
                            'commission' => $transactionInfo->surcharge, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->status,
                            'message' => $transactionInfo->statusMessage, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "Fund Transfer Records <br />";
                }
                else if($row->transaction_type == "Payout Commission")
                {
                    $transactionInfo = \DB::table('fund_transactions')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $customerInfo = \DB::table('user_accounts')->where('BID', $transactionInfo->BID)->first();

                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }

                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by, 'package_id' => $userInfo->package_id,
                            'table_name' => 'fund_transactions', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->created_at,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => $customerInfo->benificiary_name, 'aadhar_number' => $customerInfo->account,
                            'transaction_category' => 'Fund Transaction', 'transaction_type' => 'Payout',
                            'original_transaction_id' => $transactionInfo->utrnumber, 'transaction_amount' => $transactionInfo->amount,
                            'commission' => $transactionInfo->surcharge, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->status,
                            'message' => $transactionInfo->statusMessage, 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "Payout Records <br />";
                }
                else if($row->transaction_type == "UPI Payment Commission")
                {
                    $transactionInfo = \DB::table('upi_qr_txns')->where('dms_txn_id', $row->transaction_id)->first();
                    $userInfo = \DB::table('users')->where('id', $transactionInfo->user_id)->first();

                    $isExists = \DB::table('admin_transactions')->where('dms_txn_id', $row->transaction_id)->count();
                    if($isExists == 0)
                    {
                        $commission_type = "credit";
                        if($row->amount < 0)
                        {
                            $commission_type = 'debit';
                        }
                        
                        $admin_balance+=$row->amount;
                        \DB::table('admin_transactions')->insert([
                            'user_id' => $transactionInfo->user_id, 'created_by' => $row->created_by, 'package_id' => $userInfo->package_id,
                            'table_name' => 'upi_qr_txns', 'transaction_id' => $transactionInfo->id, 'request_transaction_time' => $transactionInfo->created_at,
                            'dms_txn_id' => $row->transaction_id, 'customer_name' => $transactionInfo->payer_name, 'aadhar_number' => $transactionInfo->payerVA,
                            'transaction_category' => 'UPI', 'transaction_type' => $transactionInfo->txn_type,
                            'original_transaction_id' => $transactionInfo->bankRRN, 'transaction_amount' => $transactionInfo->payer_amount,
                            'commission' => $transactionInfo->commission, 'admin_commission' => $row->amount, 'commission_type' => $commission_type,
                            'balance_amount' => '', 'current_balance' => $transactionInfo->current_balance,
                            'admin_balance' => $admin_balance, 'transaction_status' => $transactionInfo->status,
                            'message' => '', 'created_at' => $transactionInfo->created_at,
                            'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    echo "UPI Payment Records <br />";
                }

            }
        }

        $userComm = \DB::table('admin_transactions')->get();
        
        $admin_balance = 0;
        if($userComm)
        {
            $amount = 0;
            foreach($userComm as $row)
            {
                $amount+=$row->admin_commission;

                \DB::table('admin_transactions')->where('id', $row->id)->update(['admin_balance' => $amount]);
            }
        }
    }
}
