<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Aeps extends Model
{
    use CrudTrait;
  

    protected $fillable = [
        'user_id',
        'status'
    ];
}
