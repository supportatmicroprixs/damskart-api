<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class UserReport extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'users';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function users()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function owners()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function packages()
    {
        return $this->belongsTo('App\Models\Package', 'package_id');
    }

    public function getFullNameAttribute() {
        $fullName = "<a href='".backpack_url('user/'.$this->id.'/show')."'>".$this->first_name ." " . $this->last_name." <br /> (".$this->mobile.")</a>";
        return $fullName;
    }

    public function getFullUserNameAttribute() {
        $fullName = $this->first_name ." " . $this->last_name." (".$this->mobile.")";
        return $fullName;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
