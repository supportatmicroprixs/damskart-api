<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class CibReport extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'cib_transactions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['cheque_number', 'txn_date', 'remarks', 'amount', 'balance', 'value_date', 'type', 'transaction_id'];

    /*
    CHEQUENO" => ""
    "TXNDATE" => "01-01-2021 17:01:57"
    "REMARKS" => "NEFT-AXISCN0065248921-RAZORPAY SOFTWARE PRIVATE LIMITED-CUST RAZORPAY SOFTWARE "
    "AMOUNT" => "98.22"
    "BALANCE" => "98.22"
    "VALUEDATE" => "01-01-2021"
    "TYPE" => "CR"
    "TRANSACTIONID*/
    
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
