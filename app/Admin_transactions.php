<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;

Class Admin_transactions extends Model
{
	use RevisionableTrait;
	protected $revisionEnabled = true;
}
