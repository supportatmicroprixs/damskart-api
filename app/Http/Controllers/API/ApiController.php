<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator,DateTime;
use DB;
use File;
use App\Customer_accounts;
use App\Aeps_commissions;
use App\User_transactions;
use App\Models\Catalog;
use App\Services;
use App\User_services;
use App\User_accounts;
use App\Bbps_categories;
use App\Bbps_gaslists;
use App\Bbps_dthlists;
use App\Bbps_electricities;
use App\Dth_commissions;
use App\Gas_commissions;
use App\Electricity_commissions;
use App\Models\BankDetail;
use App\Service_packages;
use App\Models\Package;
use App\Fund_transactions;
use paytm\checksum\PaytmChecksumLibrary;
use App\Http\Controllers\PaytmChecksum;
use App\Aeps;
use App\Ewallet_requests;
use App\Operators;
use App\Models\Circle;
use App\Mobile_recharges;
use App\Electricity_transactions;
use App\Mobile_commissions;
use App\Models\UserQuery;
use App\Notifications;
use App\Paytm_transactions;
use App\Rozerpay_transactions;
use App\Users_messages;
use App\Bbps_billers;
use App\Bbps_billers_cats;
use App\Upi_qr_txns;
use App\Bbps_transactions;
use App\MicroATM_txn_news;
use App\Aeps_txn_news;
use App\Models\Token;
use Razorpay\Api\Api;
use CURLFile;

use App\Models\UserLastLogin;



class ApiController extends Controller{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password'), 'package' => request('package'), 'status' => '1'])){
            // $response['status'] = 'error';
            // $response['message'] = "Something went wrong";
            // return response()->json($response, $this->successStatus);
            $user = Auth::user();
            if ($user->role == 'super admin' || $user->role == 'manager' || $user->role == 'admin') {
                $response['status'] = 'error';
                $response['message'] = "You are not eligible to use this application";
                return response()->json($response, $this->successStatus);
            }
            if($user->verify == 0){
                $otp = rand(100000, 999999);
                $user->otp = $otp;
                $user->save();

                $api_key = '25F6EF10FE18AF';
                $contacts = $user->mobile;
                $from = 'DMSKRT';
                // $sms_text = urlencode('Damskart never calls for OTP. Do not share it with anyone. Login OTP: '.$otp.'. Not You? Report https://damskart.com');
                $sms_text = urlencode('Dear '.$user->first_name.', 

Your OTP to access login transaction is '.$otp.'. Do not disclose OTP to anyone,

Regards
Damskart Business.');
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207162371995454317&msg=".$sms_text);
                $output = curl_exec($ch);
                
                // $message = "Damskart never calls for OTP. Do not share it with anyone. Login OTP: ".$otp.". Not You? Report https://damskart.com";
        
                // //Preparing post parameters
                // $postData = array(
                //     'uname' => 'damskart',
                //     'password' => 'D@123456',
                //     'sender' => 'DMSKRT',
                //     'receiver' => $user->mobile,
                //     'route' => 'TA',
                //     'msgtype' => 1,
                //     'sms' => $message
                // );
             
                // $url = "http://sms.lrgroup.in/index.php/smsapi/httpapi/";
             
                // $ch = curl_init();
                // curl_setopt_array($ch, array(
                //     CURLOPT_URL => $url,
                //     CURLOPT_RETURNTRANSFER => true,
                //     CURLOPT_POST => true,
                //     CURLOPT_POSTFIELDS => $postData
                // ));
             
             
                // //Ignore SSL certificate verification
                // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
             
             
                // //get response
                // $output = curl_exec($ch);
                
                $response['status'] = 'success';
                $response['message'] = "Verify Your Mobile Number";
                $response['type'] = 'register';
                $response['description'] = $user;
            }else{
                if(!empty(request('fcm_token'))){
                    $user->fcm_token = request('fcm_token');
                    $user->last_login = date('Y-m-d H:i:s');
                    //last login save start
                    $lastlogin = new UserLastLogin();
                    $lastlogin->user_id = $user->id;
                    $lastlogin->mobile = request('mobile');
                    $lastlogin->email = $user->email;
                    $lastlogin->login_from = "application";
                    $lastlogin->last_login = date('Y-m-d H:i:s');
                    $lastlogin->start_time = date('H:i:s');
                    $lastlogin->date = date('Y-m-d');
                    $lastlogin->save(); 
                    //last login save end 
                    $user->save();
                }
                $user['token'] =  $user->createToken('token')->accessToken;
                $response['status'] = 'success';
                $response['message'] = "Login Successfully!";
                $response['description'] = $user;
            }
        }
        else{
            $response['status'] = 'error';
            $response['message'] = "Mobile & Password not match";
        }
        return response()->json($response, $this->successStatus);
    }

    public function check_login(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric',
            'user_type' => 'required|alpha',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        
        if($user->role != request('user_type') || $user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $status = false;

        $user = User::where('id', '=', $user->id)->get();
        if ($user[0]) {
            $response['status'] = 'success';
            $response['message'] = 'User Details';
            $response['description']['data'] = $user[0];
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You are not login';
        }
        return response()->json($response, $this->successStatus);
    }
 
    public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users', 
            'type' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        
        $otp = rand(100000, 999999);
        $user = User::where('mobile', request('mobile'))->first();
        // $sms_text = urlencode('Damskart never calls for OTP. Do not share it with anyone. Login OTP: '.$otp.'. Not You? Report https://damskart.com');
        $sms_text = urlencode('Dear '.$user->first_name.', 

Your OTP to access registration is '.$otp.' and Password is '.$password.' Do not disclose OTP to anyone,

Regards
Damskart Business.');

        if (!empty($user)) {
            
            $api_key = '25F6EF10FE18AF';
            $contacts = $user->mobile;
            $from = 'DMSKRT';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207162371995454317&msg=".$sms_text);
            $output = curl_exec($ch);
          
            // $sms = Settings::where('title', 'sms')->first();
            // $sms->value = $sms->value + 1;
            // $sms->save();

            $user->otp = $otp;
            if ($user->save()) {
                $response['status'] = 'success';
                $response['message'] = 'OTP Sent Successfully!!!';
                $response['type'] = $request->type;
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Something went wrong';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Not Registered';
        }
        return response()->json($response, $this->successStatus);
    }

    public function verifyOtp(Request $request){
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            // 'password' => 'required',
            'otp' => 'required',
            // 'pin' => 'required|numeric',
            'type' => 'required',
            // 'package' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if ($request->type == 'forgot') {
            $user = User::where('mobile',$request->mobile)->where('otp',$request->otp)->first();
            if (!empty($user)) {
                $user['type'] = request('type');
                $response['status'] = 'success';
                $response['message'] = "Otp Verify successfully!";
            }else{
                 $response['status'] = 'error';
                $response['message'] = "Otp doesn't match...";
            }

        }elseif ($request->type == 'register') {
            if(Auth::attempt(['mobile' => request('mobile'),'password' => request('password'), 'otp' => request('otp'),'package' => request('package')])){
                $user = Auth::user();
                $user->aeps_pin = rand(111111, 999999); // DO NOT CHANGE
                $user->login_pin = request('pin');
                $user['verify'] = 1;
                $user->otp = NULL;
                $user->save();

                $api_key = '25F6EF10FE18AF';
                $contacts = $user->mobile;
                $from = 'DMSKRT';
                // $sms_text = urlencode('Dear '.strtoupper($user->first_name).' '.strtoupper($user->last_name).', You are successfully registered in https://damskart.com/login/ Your Login ID - '.$user->mobile.', and AEPS Pin - '.$user->pin);
                $sms_text =  urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Congratulations! You have successfully registered with https://damskart.com We have created an account for you with below information. User ID: '.$user->mobile.', Transaction PIN: '.$user->login_pin.', Thanks.');
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207161738049301567&msg=".$sms_text);
                $output = curl_exec($ch);


                $user['token'] =  $user->createToken('token')->accessToken;
                $response['status'] = 'success';
                $response['message'] = "Login Successfully!";
                $response['description'] = $user;
            }
            else{
                $response['status'] = 'error';
                $response['message'] = "Otp doesn't match...";
            }
        }elseif ($request->type == 'change_password') {
            $user = User::where('mobile',$request->mobile)->where('otp',$request->otp)->first();
            if (!empty($user)) {
                $user['type'] = request('type');
                $response['status'] = 'success';
                $response['message'] = "Otp Verify successfully!";
            }else{
                 $response['status'] = 'error';
                $response['message'] = "Otp doesn't match...";
            }
        }elseif ($request->type == 'update_pin') {
            $user = User::where('mobile',$request->mobile)->where('otp',$request->otp)->first();
            if (!empty($user)) {
                $user['type'] = request('type');
                $response['status'] = 'success';
                $response['message'] = "Otp Verify successfully!";
            }else{
                 $response['status'] = 'error';
                $response['message'] = "Otp doesn't match...";
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = "type invalid...";
        }
        
        return response()->json($response, $this->successStatus);
    }

    public function verifyPin(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'pin' => 'required|numeric',
            'package' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        if ($user->mobile == request('mobile') && $user->login_pin == request('pin') && $user->package == request('package')) {
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = "Login Successfully!";
            $response['description'] = $user;
        }
        else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = "Pin doesn't match...";
        }
        return response()->json($response, $this->successStatus);
    }

    public function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!?~@#-_+<>[]{}=';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function getReferName(Request $request){
        $validator = Validator::make($request->all(), [
            'refer_phone' => 'required|exists:users|max:10|min:10|starts_with:6,7,8,9',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $refer_phone = request('refer_phone');

        $userData = DB::table('users')->where('mobile', $refer_phone)->first();

        if(!empty($userData)){
            $response['status'] = 'success';
            $response['message'] = 'User Found';
            $response['description'] = array('full_name' => $userData->first_name." ".$userData->last_name, 'user_id' => $userData->id);
        }else{
            $response['status'] = 'error';
            $response['message'] = 'No User Found';
        }
        return response()->json($response, $this->successStatus);
    }

    public function getMobile(Request $request){
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|exists:users|max:10|min:10|starts_with:6,7,8,9',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'success';
            $response['message'] = 'Mobile Number Not Registered';
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Mobile Number Already Registered';
        }
        return response()->json($response, $this->successStatus);
    }

    public function register(Request $request)
    {
        // $response['status'] = 'error';
        // $response['message'] = "Something went wrong";
        // return response()->json($response, $this->successStatus);
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required|unique:users|max:10|min:10|starts_with:6,7,8,9',
            'email' => 'required|email|unique:users',
            'gender' => 'required|alpha',
            'address' => 'required',
            'state_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'state' => 'required',
            'district' => 'required',
            'pin_code' => 'required|numeric',
            'package' => 'required',
            'aadhar_number' => 'required',
            'pan_number' => 'required',
            'company_name' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $input = $request->all();
       

        $admin = User::where('package', $input['package'])->where('role', 'super admin')->orWhere('role', 'admin')->first();

        $password = $this->randomPassword();
        $cpassword = $password;

        // var_dump($admin);
        // exit;
        // $input['state_id'] = $request->state_id;
        // $input['district_id'] = $request->district_id;
        // $input['state'] = $request->state;
        // $input['district'] = $request->district;
        $input['password'] = bcrypt($password);
        $input['refer_phone'] = $request->refer_phone;
        $input['dob'] = $request->dob;
        $input['refer_name'] = $request->refer_name;
        $input['refer_userid'] = $request->refer_userid;
        $input['created_by'] = $admin->id;
        $input['refer_by'] = $admin->id;
        $input['domain'] = $admin->domain;
        $input['package'] = $admin->package;
        $input['fcm_token'] = $request->fcm_token;
        $input['aadhar_number'] = $request->aadhar_number;
        $input['pan_number'] = $request->pan_number;
        $input['aeps_pin'] = rand(111111, 999999);
        $input['ewallet'] = 0;
        $input['aeps_wallet'] = 0;
        $input['ewallet_status'] = 1;
        $input['aeps_status'] = NULL;
        $input['fcm_status'] = 1;
        $input['online'] = 0;
        $input['profile_status'] = 0;
        $input['onboarding'] = 0;
        $input['fundtransfer'] = 0;
        $input['yesbank'] = 0;
        $input['verify'] = 0;
        $input['status'] = 1;    
        $input['allow_add_member'] = 0;    
        $input['digio_kyc_status'] = 0;    
        $input['digio_e_sign_status'] = 0;    
        $input['aeps_ekyc_status'] = 0;    
        $input['kyc_update'] = 0;   
        $input['notification'] = 0;
        
        $input['role'] = 'retailer';
        // $input['password'] = bcrypt($input['password']);
        $otp = rand(100000, 999999);
        $input['otp'] = $otp;

        // $destinationPath = 'public/images/'; // upload path
        // $passbook_image = $request->file('passbook_image');
        // $marksheet1 = $request->file('marksheet_10');
        // $marksheet2 = $request->file('marksheet_12');

        // $passbookImage = time() . "passbook_cheque." . $passbook_image->getClientOriginalExtension();
        // if($passbook_image->move($destinationPath, $passbookImage)){
        //     $input['passbook_image'] = 'images/'.$passbookImage;
        // }
        
        // $marksheetImage10 = time() . "10th_marksheet." . $marksheet1->getClientOriginalExtension();
        // if($marksheet1->move($destinationPath, $marksheetImage10)){
        //     $input['marksheet_10'] = 'images/'.$marksheetImage10;
        // }
        
        // $marksheetImage12 = time() . "12th_marksheet." . $marksheet2->getClientOriginalExtension();
        // if($marksheet2->move($destinationPath, $marksheetImage12)){
        //     $input['marksheet_12'] = 'images/'.$marksheetImage12;
        // }

        $retailerpackage = Package::where('role','=','all')->first();
        $input['package_id'] = $retailerpackage->id; 
        
        
        $user = User::create($input);
        $user->assignRole($user->role);
        // User::where('mobile',$request->mobile)->update(['otp'=>$otp]);
         //assign package start
        $memberid = $user->id;
        $package_id = $user->package_id;
        // DB::table('user_services')->where('user_id', $memberid)->delete(); 
        $userservice = Service_packages::all()->where('package_id','=',$package_id);
        $data = [];
            foreach($userservice as  $value) {
                $data['servicepack_id'] = $value['id'];
                $data['user_id'] = $memberid; 
                $data['status'] = $value['status']; 
            User_services::create($data); 
        }
        //assign package end
        // $sms_text = urlencode('Thank You For Registration With Damskart. Your Verification Code is : '.$otp);

//         $api_key = '25F6EF10FE18AF';
//         $contacts = $user->mobile;
//         $from = 'DMSKRT';
//         $sms_text = urlencode('Dear '.$user->first_name.', 

// Your OTP to access registration is '.$otp.' and Password is '.$password.' Do not disclose OTP to anyone,

// Regards
// Damskart Business.');
//         $ch = curl_init();
//         curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207162371995454317&msg=".$sms_text);
//         $output = curl_exec($ch);
       

        $response['status'] = 'success';
        $response['message'] = 'Registration Successfully!!! Verify your account';
        $response['type'] = 'register';
        $response['password'] = $cpassword;
        $response['description'] = $user;
        return response()->json($response, $this->successStatus);
    }

    public function digioKYCRegister(Request $request){

        // $user = Auth::user();
        $requestdata = request()->all();
        $user_id = $requestdata['user_id']; 
        $validator = Validator::make($request->all(), [ 
            // 'user_id' => 'required', 
        ]);
        
        
        
        // print_r($name.'---'.$mobile);
        // $postData = array(
        //     "customer_identifier" => $mobile,  //name@abc.com
        //     "customer_name" => $name,          //name
        //     "reference_id" => "DMS".date("YmdHis"), 
        //     "template_name" => "KYC", 
        //     "notify_customer" => true, 
        //     "request_details" => array(
        //         "customer_name" => $name, 
        //         "customer_identifier" => $mobile, 
        //     ), 
        //     "transaction_id" => "DMS".$mobile,
        //     "generate_access_token" => true
        // );
        // $data = json_encode($postData);
        // //Test Authorization QUk1OVBFMUM1RlY5VFBWOFU4Mjg0MVVUTDEzR0RBUTI6VEc0TUVDWUU2MUpVNUxMSTVITkRVVlgzTTVCWDhLTFI=
        // $header = [
        //     'Authorization: Basic QUk2WFJWOFlYTEVKM1lLUEhVRTMyT0FWOUI2VlVKM006M1ZYRkFaN0I1WTk0WFdDTkg2TFBXWVkzQTJZODM1UEY=', 
        //     'Content-Type: application/json',
        // ];
        // $httpUrl = 'https://api.digio.in/client/kyc/v2/request/with_template';
        // $ch = curl_init();
        // curl_setopt($ch,CURLOPT_URL, $httpUrl);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // // curl_setopt($ch,CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $header);  
        // $result = curl_exec($ch);
        // print_r($result);
        // $result = array(
        //     "id" => "KID210408184120413VDY8X2IT2C4F8U",
        //     "created_at" => "2021-04-08 18:41:20",
        //     "status" => "requested",
        //     "customer_identifier" => "9783335317",
        //     "reference_id" => "CRN122306114425315NN",
        //     "request_details" => array(
        //         "customer_name" => "Damskart",
        //         "customer_identifier" => "9783335317"
        //     ),
        //     "transaction_id" => "DMS9783335317",
        //     "customer_name" => "Damskart",
        //     "expire_in_days" => 10,
        //     "reminder_registered" => false,
        //     "access_token" => array(
        //         "created_at" => "2021-04-08 18:41:20",
        //         "id" => "GWT210408184120416WNBW1RK8GNHQ9Q",
        //         "entity_id" => "KID210408184120413VDY8X2IT2C4F8U",
        //         "valid_till" => "2021-04-09 18:41:20"
        //     )
        // );

        // $kyc_register_response = json_decode(json_encode($result));
        // $kyc_register_response = json_decode($result);
        $id_type = "Aadhar";
        $aadhar_verification_response = $requestdata;
        $user = User::where('id',$user_id)->first();  
        $user->digio_document = $id_type;
        $user->digio_kyc_status = 1;
        $user->digio_response = json_encode($aadhar_verification_response);
        $user->save();
        if ($user->save()) {
            $response['status'] = 'success';
            $response['message'] = 'Verify Successfully';
            // $response['description'] = $kyc_register_response;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
        }
        
        return response()->json($response, $this->successStatus);
    }
    
    public function digioDocVerify(Request $request){

        // $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'idcard_type' => 'required', 
            'idcard_value' => 'required', 
            'mobile' => 'required', 
            'dob' => 'required', 
        ]);
        
        
       
        $id_type = request('idcard_type');
        $id_no = request('idcard_value');
        $mobile = request('mobile');
        $user_id = request('user_id');
       
        if ($id_type == "DRIVING_LICENSE") {
            $dob = date('d/m/Y', strtotime(request('dob')));
            $postData = array(
                "id_no" => $id_no, //RJ14C20160028502
                "dob" => $dob, // 14/04/1996
            );
        }elseif ($id_type == "VOTER_ID") {
            $postData = array(
                "id_no" => $id_no, //AFQ2053295
                "dob" => '',
            );
        }elseif ($id_type == "VEHICLE_RC") {
            $postData = array(
                "id_no" => $id_no, //RJ34SM5433
                "dob" => '',
            );
        }

        $data = json_encode($postData);
        $header = [
            'Authorization: Basic QUk2WFJWOFlYTEVKM1lLUEhVRTMyT0FWOUI2VlVKM006M1ZYRkFaN0I1WTk0WFdDTkg2TFBXWVkzQTJZODM1UEY=', 
            'Content-Type: application/json',
        ];

        $httpUrl = 'https://api.digio.in/v3/client/kyc/fetch_id_data/'.$id_type;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $httpUrl,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_RETURNTRANSFER => true
        ));

        $raw_response = curl_exec($curl);
        curl_close($curl);

        //DL Response
        // $raw_response = array(
        //     "Source Of Data" => "SARATHI",
        //     "Last Completed Transaction" => "ISSUE OF DRIVING LICENCE",
        //     "NT" => "LMV",
        //     "Initial Issuing Office" => "DISTRICT TRANSPORT OFFICE, VIDHYADHARNAGAR, JAIPUR",
        //     "Last Endorsed Office" => "DISTRICT TRANSPORT OFFICE, VIDHYADHARNAGAR, JAIPUR",
        //     "Old \/ New DL No." => "NA",
        //     "Holder's Name" => "SHOBHIT KHANDELWAL",
        //     "Initial Issue Date" => "02-Sep-2016",
        //     "Last Endorsed Date" => "02-Sep-2016",
        //     "Hazardous Valid Till" => "NA",
        //     "Non-Transport" => "From: 02-Sep-2016",
        //     "Transport" => "From: NA",
        //     "Current Status" => "ACTIVE"
        // );
        
        //Voter ID Response
        // $raw_response = array(
        //     "pc_name" => "NORTH WEST DELHI              ",
        //     "st_code" => "U05",
        //     "ps_lat_long_1_coordinate" => 0,
        //     "gender" => "M",
        //     "rln_name_v2" => "",
        //     "rln_name_v1" => "\u0938\u0924\u0940\u0936  \u0915\u0941\u092e\u093e\u0930 ",
        //     "rln_name_v3" => "",
        //     "name_v1" => "\u0905\u0930\u0936 \u0927\u094d\u0930\u0941\u0935  \u092c\u0918\u0947\u0932 ",
        //     "epic_no" => "AFQ2053295       ",
        //     "ac_name" => "ROHINI",
        //     "name_v2" => "",
        //     "name_v3" => "",
        //     "ps_lat_long" => "0.0,0.0",
        //     "pc_no" => "5",
        //     "last_update" => "Sun Jan 24 08:30:16 IST 2021",
        //     "id" => "U050130022050828",
        //     "dist_no" => "8",
        //     "ps_no" => "22",
        //     "ps_name" => "NORTH DMC PRY. SCHOOL NO-2 SEC 18 ROHINI (NEAR DR. LAL PATH LAB)",
        //     "ps_name_v1" => "\u0928\u0949\u0930\u094d\u0925 \u0921\u0940 \u090f\u092e \u0938\u0940 \u092a\u094d\u0930\u093e\u0907\u092e\u0930\u0940 \u0938\u094d\u0915\u0942\u0932\t \u0928\u0902 -2 \u0938\u0947\u0915\u094d\u091f\u0930-18 \u0930\u094b\u0939\u093f\u0923\u0940 (\u0928\u093f\u0915\u091f \u0921\u0949\u0915\u094d\u091f\u0930  \u0932\u093e\u0932 \u092a\u093e\u0925 \u0932\u0948\u092c\u0964) \u0926\u093f\u0932\u094d\u0932\u0940",
        //     "st_name" => "NCT OF Delhi",
        //     "dist_name" => "NORTH                         ",
        //     "rln_type" => "F",
        //     "pc_name_v1" => "\u0909\u0924\u094d\u0924\u0930 \u092a\u0936\u094d\u091a\u093f\u092e\u0940 \u0926\u093f\u0932\u094d\u0932\u0940",
        //     "part_name_v1" => "\u0930\u094b\u0939\u0923\u0940 \u0938\u0948\u0915\u094d\u091f\u0930 18",
        //     "ac_name_v1" => "\u0930\u094b\u0939\u093f\u0923\u0940",
        //     "part_no" => "22",
        //     "dist_name_v1" => "\u0909\u0924\u094d\u0924\u0930",
        //     "ps_lat_long_0_coordinate" => 0,
        //     "_version_" => 1689750744814583808,
        //     "name" => "ARSH DHRUV  BAGHEL ",
        //     "section_no" => "5",
        //     "ac_no" => "13",
        //     "slno_inpart" => "828",
        //     "rln_name" => "SATISH  KUMAR ",
        //     "age" => 25,
        //     "part_name" => "SECTOR-18 ROHINI"
        // );

        // Error Response
        // $raw_response = array(
        //     "details" => "EX210409172845956HDW",
        //     "code" => "ENTITY_NOT_FOUND",
        //     "message" => "Entity not found for this id "
        // );

        // $doc_verification_response = json_decode(json_encode($raw_response));
        $doc_verification_response = json_decode($raw_response);
        if ($doc_verification_response) {
            $user = User::where('mobile',$mobile)->first();    
            $user->digio_document = $id_type.' ('.$id_no.')';
            $user->digio_kyc_status = 1;
            $user->digio_response = json_encode($doc_verification_response);
            $user->save();

            $response['status'] = 'success';
            $response['message'] = 'Registered Successfully';
            $response['description'] = $doc_verification_response;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
        }
        
        return response()->json($response, $this->successStatus);
    }
    

    public function resendOtp(Request $request){
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $mobile = request('mobile');

        $retailer = User::where('mobile',$mobile)->first();
        
        $otp = rand(100000, 999999);
        // $sms_text = urlencode('Thank You For Registration With Damskart. Your Verification Code is : '.$otp);
        $sms_text = urlencode('Dear '.$retailer->first_name.', 

Your OTP to access login transaction is '.$otp.'. Do not disclose OTP to anyone,

Regards
Damskart Business.');

        $user = User::where('mobile', '=', request('mobile'))->where('role', '!=', 'super_admin')->where('role', '!=', 'admin')->where('role', '!=', 'manager')->first();

        if (!empty($user)) {
            $api_key = '25F6EF10FE18AF';
            $contacts = $user->mobile;
            $from = 'DMSKRT';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207162371995454317&msg=".$sms_text);
            $output = curl_exec($ch);

            $user->otp = $otp;
            if ($user->save()) {
                $response['status'] = 'success';
                $response['message'] = 'OTP Send Successfully!!!';
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Something went wrong';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Not Registered';
        }
        
        return response()->json($response, $this->successStatus);
    }
    
    public function logoutApi(Request $request){ 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric', 
            'fcm_token' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->id == request('user_id')) {
                $user->online = 0;
                $data = array('fcm_token' => null, 'fcm_status' => 0, 'updated_at' => date('Y-m-d H:i:s'));
                // $end_date_data = array('end_time' => date('H:i:s'));
                DB::table('users')->where('id', $user->id)->where('fcm_token', request('fcm_token'))->update($data);
                // DB::table('users_last_logins')->where('user_id', $user->id)->where('login_from', "application")->latest()->update($end_date_data);
                // $logouttime = UserLastLogin::where('user_id', $user->id)->where('login_from', "application")->latest()->first();
                // $logouttime->end_time = date('H:i:s');

                // $start_time = $logouttime->start_time;
                // $end = $logouttime->end_time;
                // $dteStart = new DateTime($start_time);
                // $dteEnd   = new DateTime($end);
                // $dteDiff  = $dteStart->diff($dteEnd);

                // $logouttime->duration = $dteDiff->format("%H:%I:%S");
                // $logouttime->save();
                $user->save();
                $accessToken = $user->token();
                DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $accessToken->id)
                    ->update([
                    'revoked' => true
                ]);


                $accessToken->revoke();
                $response['status'] = 'success';
                $response['message'] = 'You are logout successfully!!!';
            }else{
                $response['status'] = 'error';
                $response['message'] = 'You are not valid user';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You are already logout';
        }
        return response()->json($response, $this->successStatus);
    }

    public function checkProfile(Request $request){
        if (Auth::check()) {
            $status = true;
            $user = Auth::user();
            $name = $user->first_name.' '.$user->last_name;
            if ($user->profile_status) {
                 if (!$user->yesbank) {
                    $postData = array(
                        "api_access_key" => "c838c08f2245376823272c2b227801a6",
                        "mobile" => Auth::user()->mobile,
                        "email" => Auth::user()->email,
                        "pan_no" => Auth::user()->pan_number,
                        "first_name" => Auth::user()->first_name,
                        "middle_name" => "",
                        "last_name" => Auth::user()->last_name
                    );
                    // $data = json_encode($postData);
                    //$data = str_replace("/", "", $data);
                    // var_dump($data);
                    // exit;
                    //     echo "<pre>";
                    // print_r($_SERVER); die();
                    $url = "https://netpaisa.com/nps/api/aeps/yesbank_aeps_registartion.php";
                    $ch = curl_init();
                    curl_setopt_array($ch, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $postData
                    ));
                    //Ignore SSL certificate verification
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    //get response
                    $output = curl_exec($ch);
                    $data = json_decode($output);
                    // var_dump($data);
                    // exit;
                    
                    if ($data->ERR_STATE == 0) {
                        $user->outlet_id = $data->outlet_id;
                    }else if(Auth::user()->kyc_update == 0){
                      $pan_pic = new \CURLFile('@/'.asset('/' . Auth::user()->pan_pic));
                      $aadhar_pic_front = new \CURLFile('@/'.asset('/' . Auth::user()->aadhar_pic_front));
                      $company_logo = new \CURLFile('@/'.asset('/' . Auth::user()->company_logo));
                      $profile_pic = new \CURLFile('@/'.asset('/' . Auth::user()->profile_pic));
                        $postData = array(
                            "api_access_key" => "c838c08f2245376823272c2b227801a6",
                            "mobile" => Auth::user()->mobile,
                            "pan_no" => Auth::user()->pan_number,
                            "outlet_id" => Auth::user()->outlet_id,
                            "email" => Auth::user()->email,
                            "first_name" => Auth::user()->first_name,
                            "middle_name" => "",
                            "last_name" => Auth::user()->last_name,
                            "company" => Auth::user()->company_name,
                            "aadhaar_no" => Auth::user()->aadhar_number,
                            "dob" => date('d-m-Y', strtotime(Auth::user()->dob)),
                            "address" => Auth::user()->address,
                            "pincode" => Auth::user()->pin_code,
                            "district" => Auth::user()->district,
                            "state" => Auth::user()->state,
                            "city" => Auth::user()->district,
                            "shop_name" => Auth::user()->company_name,
                            "shop_in_code" => Auth::user()->pin_code,
                            "shop_district" => Auth::user()->district,
                            "shop_state" => Auth::user()->state,
                            "shop_city" => Auth::user()->district,
                            "shop_area" => Auth::user()->district,
                            "alter_mobile" => Auth::user()->mobile,
                            "shop_address" => Auth::user()->address,
                            "local_address" => Auth::user()->address,
                            "local_area" => Auth::user()->district,
                            "pan_img" => $pan_pic,
                            "aadhaar_img" => $aadhar_pic_front,
                            "shop_img" => $company_logo,
                            "profile_img" => $profile_pic,
                        );
                        $url = "https://netpaisa.com/nps/api/aeps/yesbank_aeps_kyc_update.php";
                        $ch = curl_init();
                            curl_setopt_array($ch, array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $postData
                        ));
                        //Ignore SSL certificate verification
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        //get response
                        $output = curl_exec($ch);
                        $yesbank_data = json_decode($output);
                         

                        $user->kyc_update = 1;
                    }
                    if($user->kyc_update == 1){
                      $Data = array(
                          "api_access_key" => "c838c08f2245376823272c2b227801a6",
                          "mobile" => Auth::user()->mobile,
                          "pan_no" => Auth::user()->pan_number,
                      );
                      $url1 = "https://www.netpaisa.com/nps/api/aeps/yesbank_outlet_details.php";
                      $ch = curl_init();
                          curl_setopt_array($ch, array(
                          CURLOPT_URL => $url1,
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_POST => true,
                          CURLOPT_POSTFIELDS => $Data
                      ));
                      //Ignore SSL certificate verification
                      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                      //get response
                      $output1 = curl_exec($ch);
                      $yesbank_data1 = json_decode($output1);

                      if($yesbank_data1->ERR_STATE == 0){
                        $user->kyc_status = $yesbank_data1->DATA->kyc_status;
                        $user->outlet_status = $yesbank_data1->DATA->outlet_status;
                        if ($yesbank_data1->DATA->kyc_status == 'APPROVED') {
                            $user->yesbank = 1;
                        }
                      }
                    }
                }
                $status = $user->save();
                if ($status) {
                    $user->ewallet = number_format($user->ewallet,2, '.', '');
                    $user->aeps_wallet = number_format($user->aeps_wallet,2, '.', '');
                    $response['status'] = 'success';
                    $response['message'] = 'Profile is Completed';
                    $response['user_status'] = $user->status;
                    $response['description'] = $user;
                }else{
                    $response['status'] = 'error';
                    $response['user_status'] = $user->status;
                    $response['message'] = 'Something went wrong';
                }
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Complete Your Profile First';
                $response['user_status'] = $user->status;
                $response['description']['profile_status'] = 0;
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'You are not login';
        }
        return response()->json($response, $this->successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function userDetails()
    {
        $user = Auth::user();
        $user_accounts = User_accounts::where('user_id',$user->id)->get();       

        if($user->role != 'super admin' && $user->role != 'admin' && $user->role != 'manager'){
             $user->ewallet = number_format($user->ewallet,2, '.', '');
             $user->aeps_wallet = number_format($user->aeps_wallet,2, '.', '');
            $user->bank_accounts = $user_accounts;
            $response['status'] = 'success';
            $response['message'] = 'User Details';
            $response['user_status'] = $user->status;
            $response['description'] = $user;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You are invalid user';
            $response['user_status'] = $user->status;
            $response['description']['login'] = 0;
        }
        return response()->json($response, $this->successStatus);
    }

    public function updateProfile(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            // 'aadhar_number' => 'required|numeric', 
            // 'pan_number' => 'required', 
            // 'company_name' => 'required', 
            // 'company_address' => 'required', 
            // 'aadhar_pic_front' => 'required|image|mimes:jpeg,jpg,png', 
            // 'aadhar_pic_back' => 'required|image|mimes:jpeg,jpg,png', 
            // 'pan_pic' => 'required|image|mimes:jpeg,jpg,png', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $destinationPath = 'public/images/'; // upload path
        $aadhar_pic_front = $request->file('aadhar_pic_front');
        $aadhar_pic_back = $request->file('aadhar_pic_back');
        $pan_pic = $request->file('pan_pic');
        $passbook_image = $request->file('passbook_image');
        $marksheet_10 = $request->file('marksheet_10');
        $marksheet_12 = $request->file('marksheet_12');

        if (!empty($aadhar_pic_front)) {
            $frontImage = time() . "_front." . $aadhar_pic_front->getClientOriginalExtension();
            if($aadhar_pic_front->move($destinationPath, $frontImage)){
                $user->aadhar_pic_front = 'images/'.$frontImage;
            }
        }

        if (!empty($aadhar_pic_back)) {
            $backImage = time() . "_back." . $aadhar_pic_back->getClientOriginalExtension();
            if($aadhar_pic_back->move($destinationPath, $backImage)){
                $user->aadhar_pic_back = 'images/'.$backImage;
            }
        }

        if (!empty($pan_pic)) {
            $panImage = time() . "_pan." . $pan_pic->getClientOriginalExtension();
            if($pan_pic->move($destinationPath, $panImage)){
                $user->pan_pic = 'images/'.$panImage;
            }
        }

        if (!empty($passbook_image)) {
            $passImage = time() . "_pan." . $passbook_image->getClientOriginalExtension();
            if($passbook_image->move($destinationPath, $passImage)){
                $user->passbook_image = 'images/'.$passImage;
            }
        }

        if (!empty($marksheet_10)) {
            $marksheet10Image = time() . "_pan." . $marksheet_10->getClientOriginalExtension();
            if($marksheet_10->move($destinationPath, $marksheet10Image)){
                $user->marksheet_10 = 'images/'.$marksheet10Image;
            }
        }

        if (!empty($marksheet_12)) {
            $marksheet12Image = time() . "_pan." . $marksheet_12->getClientOriginalExtension();
            if($marksheet_12->move($destinationPath, $marksheet12Image)){
                $user->marksheet_12 = 'images/'.$marksheet12Image;
            }
        }
        
        if (request('profile_pic')) {
           if ($files = $request->file('profile_pic')) {
                $destinationPath = 'public/images/'; // upload path
                $profileImage = time() . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $user->profile_pic = 'images/'.$profileImage;
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Image format is not valid';
                return response()->json($response, $this->successStatus);
            }
        }

        if(!empty(request('company_pan'))){
            $user->company_pan = request('company_pan');
        }

        $user->aadhar_number = request('aadhar_number');
        $user->pan_number = request('pan_number');
        $user->company_name = request('company_name');
        $user->company_address = request('company_address');

        $user->profile_status = 1;
        if($user->save()){
            $user->save();
            $response['status'] = 'success';
            $response['message'] = 'Profile Updated Successfully!!!';
            $response['user_status'] = $user->status;
            $response['description'] = $user;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Something went wrong';
        }
        return response()->json($response, $this->successStatus);
    }

    public function updateProfilePic(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'profile_pic' => 'image|mimes:jpeg,jpg,png', 
            
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if (!empty(request('profile_pic'))) {
           if ($image = $request->file('profile_pic')) {
                $image_name = 'profile'.time() . '.'.$image->getClientOriginalExtension();
                $destinationPath = public_path().'/images/';
                 if ($image->move($destinationPath, $image_name)) {
                        if ($user->profile_pic != null) {
                            $userimage = public_path("/{$user->profile_pic}");
                            if (File::exists($userimage)) { 
                                unlink($userimage);
                            }
                            
                        }
                        $user->profile_pic= 'images/'.$image_name;
                    }
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Image format is not valid';
                $response['user_status'] = $user->status;
                return response()->json($response, $this->successStatus);
            }
        }
   
        if($user->save()){
            $response['status'] = 'success';
            $response['message'] = 'Profile Updated Successfully!!!';
            $response['user_status'] = $user->status;
            $response['description'] = $user;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function services(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if ($user->package_id != null) {
            $catelogs = Catalog::where('status', 1)->get();

            if (!empty($catelogs[0])) {
                foreach ($catelogs as $key => $value) {
                    $service_list = Services::leftJoin('catelogs', 'catelogs.id', '=', 'services.catelog_id')->leftJoin('service_packages', 'service_packages.service_id', '=', 'services.id')->leftJoin('user_services', 'user_services.servicepack_id', '=', 'service_packages.id')->where('services.catelog_id', $value->id)->where('services.status', 1)->where('services.parent_id', null)->where('service_packages.status', 1)->where('user_services.status', 1)->where('user_services.user_id', $user->id)->where('service_packages.package_id', $user->package_id)->get(['services.id', 'services.service_name', 'services.icon', 'services.url']);
                    if (!empty($service_list[0])) {
                        foreach ($service_list as $key1 => $value1) {
                            if ($value1->url != 'bbps') {
                                $service_child = Services::where('parent_id', $value1->id)->where('status', 1)->get(['id', 'service_name', 'icon', 'url']);
                                if (!empty($service_child[0])) {
                                    $value1['child_data'] = $service_child;
                                }else{
                                    $value1['child_data'] = null;
                                }
                            }
                        }
                        $data[$key] = array('category_name' => $value->catelog_name, 'service_list' => $service_list);
                    }
                }
                if (!empty($data[0])) {
                    $response['status'] = 'success';
                    $response['message'] = "Service Available";
                    $response['user_status'] = $user->status;
                    $response['description']['service_list'] = $data;
                }else{
                    $response['status'] = 'error';
                    $response['user_status'] = $user->status;
                    $response['message'] = "No Service Available In This Package... Please Contact to Administrator";
                }
            }else{
                $response['status'] = 'error';
                    $response['user_status'] = $user->status;
                $response['message'] = "No Service Available In This Package... Please Contact to Administrator";
            }

        }else{
            $response['status'] = 'error';
                    $response['user_status'] = $user->status;
            $response['message'] = "You Don't Have Any Service Active Yet.... Please Buy Package For Using Our Services";
        }
        return response()->json($response, $this->successStatus); 
    }
    
    public function fetch_area(Request $request){ 
        $state = DB::select('select * from states');
        if(!empty(request('state_id'))){
            $district = DB::select("select * from districts where state_id = '".request('state_id')."'");
            $response['description']['district_data'] = $district;
        }
        if (!empty($state)) {
            $response['status'] = 'success';
            $response['message'] = 'All states and districts';
            $response['description']['state_data'] = $state;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
        }
        return response()->json($response, $this->successStatus); 
    }
    
    public function set_area(Request $request) 
    {
        $validator = Validator::make($request->all(), [ 
            'state_id' => 'required|numeric',
            'district_id' => 'required|numeric', 
            'state' => 'required', 
            'district' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $user = Auth::user();
        $user->state_id = request('state_id');
        $user->district_id = request('district_id');
        $user->district = request('district');
        $user->state = request('state');
        if ($user->save()) {
            $response['status'] = 'success';
            $response['message'] = 'Thanks for Login';
            $response['description']['data'] = $user;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
        }
        return response()->json($response, $this->successStatus); 
    }

    public function ewallet(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $fund_transfer = User_transactions::where('user_id', $user->id)->where('label', 'fund_transfer')->where('type', 'credit')->where('status','=','ACCEPTED')->count('amount');
        $withdrawl = User_transactions::where('user_id', $user->id)->where('label', 'fund_transfer')->where('type', 'debit')->where('status','=','ACCEPTED')->count('amount');
        $transaction = User_transactions::where('user_id', $user->id)->where('label', 'ewallet')->orderBy('created_at','desc')->get()->take('5');
        $user->ewallet = number_format($user->ewallet,3, '.', '');
        foreach ($transaction as $key => $value) {
            $value->amount = number_format($value->amount,3, '.', '');
            $value->current_balance = number_format($value->current_balance,3, '.', '');
        }
        $response['status'] = "success";
        $response['message'] = "Successfully";
        $response['user_status'] = $user->status;
        $response['description']['ewallet_data'] = array('ewallet' => $user->ewallet, 'fund_transfer' => $fund_transfer, 'withdrawl' => $withdrawl, 'transaction' => $transaction);
        
        return response()->json($response, $this->successStatus);            
    }
    
    public function ewalletTransactions(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword');
        $date = request('date');

        if(request('date') == 'all'){
            $ewallet_transactions = User_transactions::where('user_id', $user->id)->where('label', 'ewallet')->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            // var_dump($ewallet_transactions);
            // exit;
        }elseif(!empty($keyword) && !empty($date)){
          
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $ewallet_transactions =  DB::table('user_transactions')
            ->where('user_id', $user->id)
            ->where('label', 'ewallet')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('date', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('date', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->where('type', 'like', "%{$keyword}%")
            ->orWhere('amount', 'like', "%{$keyword}%")
           
            ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
        }elseif (!empty($keyword)) {
            $ewallet_transactions = User_transactions::where('user_id', request('user_id'))->where('label', 'ewallet')->where('type', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
        }elseif(!empty($date)){
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $ewallet_transactions =  DB::table('user_transactions')
            ->where('user_id', $user->id)
            ->where('label', 'ewallet')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('date', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('date', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            
        }else{
            $ewallet_transactions = User_transactions::where('user_id', $user->id)->where('label', 'ewallet')->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
        } 
            foreach ($ewallet_transactions as $key => $value) {
                $value->amount = number_format($value->amount,3, '.', '');
                $value->current_balance = number_format($value->current_balance,3, '.', '');
            }


        if (!empty($ewallet_transactions[0])) {
            $response['status'] = "success";
            $response['message'] = "Successfully";
            $response['user_status'] = $user->status;
            $response['description']['ewallet_transactions'] = $ewallet_transactions;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'No transaction made yet.';
        }
        
        return response()->json($response, $this->successStatus);            
    }

    public function aepsTransactions(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
            'type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword'); 
        $date = request('date');
        if (request('type') == 'aeps_transactions') {
            if(request('date') == 'all'){
                 $aeps_transactions = Aeps_txn_news::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $aeps_transactions =  DB::table('aeps')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where('amount', 'like', "%{$keyword}%")
                ->orWhere('mobile', 'like', "%{$keyword}%")
                ->orWhere('status', 'like', "%{$keyword}%") 
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $aeps_transactions = Aeps_txn_news::where('user_id', request('user_id'))->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $aeps_transactions =  DB::table('aeps')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $aeps_transactions = Aeps_txn_news::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif(request('type') == 'aepstransfer_request'){
            if(request('date') == 'all'){
                $aeps_transactions = User_transactions::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $aeps_transactions =  DB::table('user_transactions')
                ->where('user_id', $user->id)
                ->where('label', 'aeps_request')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('date', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('date', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where('type', 'like', "%{$keyword}%")
                ->orWhere('amount', 'like', "%{$keyword}%")
                
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $aeps_transactions = User_transactions::where('user_id', request('user_id'))->where('label', 'aeps_request')->where('type', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));

            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $aeps_transactions =  DB::table('user_transactions')
                ->where('user_id', $user->id)
                ->where('label', 'aeps_request')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('date', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('date', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $aeps_transactions = User_transactions::where('user_id', request('user_id'))->where('label', 'aeps_request')->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Type not valid!';
        }
            
            foreach ($aeps_transactions as $key => $value) {
                $value->amount = number_format($value->amount,3, '.', '');
                $value->current_balance = number_format($value->current_balance,3, '.', '');
            }

        if (!empty($aeps_transactions[0])) {
            $response['status'] = "success";
            $response['message'] = "Successfully";
            $response['user_status'] = $user->status;
            $response['description']['aeps_transactions'] = $aeps_transactions;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'No transaction made yet.';
        }
        
        return response()->json($response, $this->successStatus);            
    }
    
    public function transferAepsMain(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'amount' => 'required|numeric|min:5000',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $user = User::where('id',request('user_id'))->first();
        $user->aeps_wallet = $user->aeps_wallet - request('amount');
        $user->ewallet = $user->ewallet + request('amount');

        $unique_id = "DMS".date("Ymd").time();

        if ($user->save()) {

              $data1 = new User_transactions();
              $data1->user_id = $user->id;
              $data1->created_by = $user->created_by;
              $data1->transaction_id = $unique_id;
              $data1->amount = request('amount');
              $data1->current_balance = $user->aeps_wallet;
              $data1->narration = "AEPS FW Success TXN: DMS".date("Ymd").time();
              $data1->type = "debit";
              $data1->transaction_type = "AEPS FW";
              $data1->label = 'aeps_request';
              $data1->status = "COMPLETED";
              $data1->date = date('Y-m-d H:i:s');

              $datacr = new User_transactions();
              $datacr->user_id = $user->id;
              $datacr->created_by = $user->created_by;
              $datacr->transaction_id = $unique_id;
              $datacr->amount = request('amount');
              $datacr->current_balance = $user->ewallet;
              $datacr->narration = "AEPS FW Success TXN: DMS".date("Ymd").time();
              $datacr->type = "credit";
              $datacr->transaction_type = "AEPS FW";
              $datacr->label = 'ewallet';
              $datacr->status = "COMPLETED";
              $datacr->date = date('Y-m-d H:i:s');
              
              $data1->save();
              $datacr->save();
              
            $response['status'] = "success";
            $response['message'] = "Aeps transaction successful.";
            $response['user_status'] = $user->status;
            $response['aeps_wallet'] = $user->aeps_wallet;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error!';
            $response['user_status'] = $user->status;
        }
        
        return response()->json($response, $this->successStatus);            
    }

    public function razorpayOrderId(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'amount' => 'required',
            'merchant_id' => 'required',
            'secret_key' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $amount = request('amount') * 100;
        $receiptno = "DMS".date("YmdHis");
        $api = new Api(request('merchant_id'), request('secret_key'));
        
        $order  = $api->order->create([
          'receipt'         => $receiptno,
          'amount'          => $amount, 
          'currency'        => 'INR',
        ]);
        $order_id = $order['id'];
        $order  = $api->order->fetch($order_id);

        if (!empty($order_id)) {
            $response['status'] = "success";
            $response['message'] = "Success!!!";
            $response['user_status'] = $user->status;
            $response['razorpay_orderid'] = $order_id;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Payment initialization Failed!';
        }
        
        return response()->json($response, $this->successStatus);            
    }
    
    public function addMoney(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'amount' => 'required|numeric',
            'status' => 'required',
            'payment_through' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $unique_id = "DMS".date("Ymd").time();

        if (request('payment_through') == "icici") {
          $datacr = new User_transactions();
          $datacr->user_id = $user->id;
          $datacr->created_by = $user->created_by;
          $datacr->transaction_id = $unique_id;
          $datacr->amount = request('amount');
          $datacr->type = "credit";
          $datacr->label = 'ewallet';
          $datacr->status = request('status');
          $datacr->date = date('Y-m-d H:i:s');

          $rozerpaytxn = new Rozerpay_transactions();
          $rozerpaytxn->user_id = $user->id;
          $rozerpaytxn->created_by = $user->created_by;
          $rozerpaytxn->rozerpay_id = request('razorpay_id');
          $rozerpaytxn->dms_txn_id = $unique_id;
          $rozerpaytxn->amount = request('amount');
          $rozerpaytxn->date = date('Y-m-d H:i:s');
        }else if(request('payment_through') == "paytm"){
          $datacr = new User_transactions();
          $datacr->user_id = $user->id;
          $datacr->created_by = $user->created_by;
          $datacr->transaction_id = request('txn_id');
          $datacr->amount = request('amount');
          $datacr->narration ="Fund Credited Success by Net Banking/Credit Card. Info: ".$datacr->transaction_id;
          // $datacr->narration = "Fund Added (".number_format(request('amount'),2, '.', '').") by (".request('payment_mode').")";
          $datacr->type = "credit";
          $datacr->label = 'ewallet';
          $datacr->status = request('status');
          $datacr->date = date('Y-m-d H:i:s');

          $paytmtxn = new Paytm_transactions();
          $paytmtxn->user_id = $user->id;
          $paytmtxn->created_by = $user->created_by;
          $paytmtxn->bank_name = request('bank_name');
          $paytmtxn->bank_txnid = request('bank_txnid');
          $paytmtxn->gateway_name = request('gateway_name');
          $paytmtxn->order_id = request('order_id');
          $paytmtxn->payment_mode = request('payment_mode');
          $paytmtxn->response_msg = request('response_msg');
          $paytmtxn->status = request('status');
          $paytmtxn->txn_amount = request('amount');
          $paytmtxn->txn_date = request('txn_date');
          $paytmtxn->txn_id = request('txn_id');
          $paytmtxn->dms_txn_id = $unique_id;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Please choose valid payment method!';
            $response['user_status'] = $user->status;
            return response()->json($response, $this->successStatus);            
        }

        if (request('status') == "Complete") {
            if (request('payment_through') == "icici") {
                $datacr->narration = "Fund Credited Success by icici bank. Info: ".$datacr->transaction_id;
                $datacr->current_balance = $user->ewallet + request('amount');
                  $rozerpaytxn->message = "Fund Credited Success by icici bank. Info: ".$datacr->transaction_id;
                  $rozerpaytxn->status = "Complete";
                  $rozerpaytxn->current_balance = $user->ewallet + request('amount');
                $datacr->save();
                $rozerpaytxn->save();
            }else{
                $datacr->current_balance = $user->ewallet + request('amount');
                $datacr->save();
                $paytmtxn->save();
            }

            $user = User::where('id',request('user_id'))->first();
            $user->ewallet = $user->ewallet + request('amount');
            $user->save();             
              
            $response['status'] = "success";
            $response['message'] = "Add Money successful.";
            $response['user_status'] = $user->status;
            $response['description'] = $datacr;
        }else{
          if (request('payment_through') == "icici") {
            $datacr->narration = "Fund Credited Failed by icici bank. Info: ".$datacr->transaction_id;
            $datacr->current_balance = $user->ewallet;
              $rozerpaytxn->message = "Fund Credited Failed by icici bank. Info: ".$datacr->transaction_id;
              $rozerpaytxn->status = "Failed";
              $rozerpaytxn->current_balance = $user->ewallet;
            $datacr->save();
            $rozerpaytxn->save();
          }else{
            $datacr->current_balance = $user->ewallet;
            $datacr->save();
            $paytmtxn->save();
          }
            $response['status'] = 'error';
            $response['message'] = 'Payment Failed!';
            $response['user_status'] = $user->status;
            $response['amount'] = number_format(request('amount'),2, '.', '');
          
        }
        return response()->json($response, $this->successStatus);            
    }

    public function fundTransactions(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword');
        $date = request('date');
        
        
        if(request('date') == 'all'){
            $fund_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id','=',request('user_id'))
                ->where('fund_transactions.transfer_type','Fund Transfer')
                ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
                ->orderBy('fund_transactions.created_at','desc')
                ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                    ->splice(request('skip'))->take(request('limit'));
           
        }elseif(!empty($keyword) && !empty($date)){
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $fund_transactions =  DB::table('fund_transactions')
            ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
            ->where('fund_transactions.user_id', request('user_id'))
            ->where('fund_transactions.transfer_type','Fund Transfer')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
            ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
            // ->Where('fund_transactions.created_at', 'like', "%{$startdate}%")
            // ->orWhere(function ($query) use ($enddate) {
            //     $query->where('created_at', 'like', "%{$enddate}%");
            // })
            ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
        }elseif (!empty($keyword)) {
            $fund_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                        ->where('fund_transactions.user_id','=',request('user_id'))
                        ->where('fund_transactions.transfer_type','Fund Transfer')
                        ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                        ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
                        ->orderBy('fund_transactions.created_at','desc')
                        ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                            ->splice(request('skip'))->take(request('limit'));
            
        }elseif(!empty($date)){
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $fund_transactions =  DB::table('fund_transactions')
            ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
            ->where('fund_transactions.user_id', request('user_id'))
            ->where('fund_transactions.transfer_type','Fund Transfer')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
        }else{
            $fund_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id','=',request('user_id'))
                ->where('fund_transactions.transfer_type','Fund Transfer')
                ->orderBy('fund_transactions.created_at','desc')
                ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                ->splice(request('skip'))->take(request('limit'));
        }

        if (!empty($fund_transactions[0])) {
            $response['status'] = "success";
            $response['message'] = "Successfully";
            $response['user_status'] = $user->status;
            $response['description']['fund_transactions'] = $fund_transactions;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'No transaction made yet.';
        }
        
        return response()->json($response, $this->successStatus);            
    }

    public function payoutTransactions(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword');
        $date = request('date');

        if(request('date') == 'all'){
            $payout_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id','=',request('user_id'))
                ->where('fund_transactions.transfer_type','Payout')
                ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
                ->orderBy('fund_transactions.created_at','desc')
                ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                    ->splice(request('skip'))->take(request('limit'));
        }elseif(!empty($keyword) && !empty($date)){
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $payout_transactions =  DB::table('fund_transactions')
            ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
            ->where('fund_transactions.user_id', request('user_id'))
            ->where('fund_transactions.transfer_type','Payout')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
            ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
            ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
            ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
        }elseif (!empty($keyword)) {
            $payout_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                        ->where('fund_transactions.user_id','=',request('user_id'))
                        ->where('fund_transactions.transfer_type','Payout')
                        ->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                        ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%")
                        ->orderBy('fund_transactions.created_at','desc')
                        ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                            ->splice(request('skip'))->take(request('limit'));
        }elseif(!empty($date)){
            $splitdate = explode(':', $date);
            $startdate = $splitdate[0];
            $enddate = $splitdate[1];
            $payout_transactions =  DB::table('fund_transactions')
            ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
            ->where('fund_transactions.user_id', request('user_id'))
            ->where('fund_transactions.transfer_type','Payout')
            ->where(function($q) use ($startdate, $enddate){
                $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                  ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
            })
            ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
        }else{
            $payout_transactions = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id','=',request('user_id'))
                ->where('fund_transactions.transfer_type','Payout')
                ->orderBy('fund_transactions.created_at','desc')
                ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                ->splice(request('skip'))->take(request('limit'));
        }

        if (!empty($payout_transactions[0])) {
            $response['status'] = "success";
            $response['message'] = "Successfully";
            $response['user_status'] = $user->status;
            $response['description']['payout_transactions'] = $payout_transactions;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'No transaction made yet.';
            $response['user_status'] = $user->status;
        }
        
        return response()->json($response, $this->successStatus);            
    }

    public function aepsWallet(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $transfer = User_transactions::where('user_id', $user->id)->where('label', 'aeps_request')->where('type', 'debit')->where('status', 0)->count('amount');
        $transaction = Aeps_txn_news::where('user_id', $user->id)->get()->take('5');
         $user->aeps_wallet = number_format($user->aeps_wallet,3, '.', '');
         foreach ($transaction as $key => $value) {
                $value->amount = number_format($value->amount,3, '.', '');
                $value->current_balance = number_format($value->current_balance,3, '.', '');
            }
        $response['status'] = "success";
        $response['message'] = "Successfully";
        $response['user_status'] = $user->status;
        $response['description']['aeps_data'] = array('aeps_wallet' => $user->aeps_wallet, 'transfer' => $transfer, 'transaction' => $transaction);
        
        return response()->json($response, $this->successStatus);            
    }

    public function fundRequest(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
    }

    public function addFundRequest(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'amount' => 'required|numeric',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        
        $unique_id = "DMS".date("Ymd").time();

          $ewallet_requests = new Ewallet_requests();
          $ewallet_requests->user_id = request('user_id');
          $ewallet_requests->created_by = $user->created_by;
          $ewallet_requests->amount = request('amount');
          $ewallet_requests->proof_txnid = request('proof_txnid');
          $ewallet_requests->dms_txn_id = $unique_id;
          $ewallet_requests->request_date = date('Y-m-d H:i:s');
          $ewallet_requests->payment_type = request('payment_type');
          $ewallet_requests->remarks = request('remarks');
          $ewallet_requests->receive = "Pending";
          $ewallet_requests->status = "Pending";
          // $ewallet_requests->proof_image = request('proof_image');
        if (request('proof_image')) {
           if ($files = $request->file('proof_image')) {
                $destinationPath = 'public/images/'; // upload path
                $proofImage = time() . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $proofImage);
                $ewallet_requests->proof_image = 'images/'.$proofImage;
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Format is not valid';
                $response['user_status'] = $user->status;
                return response()->json($response, $this->successStatus);
            }
        }

        if ($ewallet_requests->save()) {
            $response['status'] = "success";
            $response['message'] = "Request sent successful.";
            $response['user_status'] = $user->status;
            $response['description'] = $ewallet_requests;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error!';
            $response['user_status'] = $user->status;
        }
        
        return response()->json($response, $this->successStatus);            
    }

    public function fundRequestList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'request_type' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword');
        $date = request('date');
        
        if (request('request_type') == "my_request") {
            if(request('date') == 'all'){
                $fundrequest_list = Ewallet_requests::where('user_id',request('user_id'))->where('amount', 'like', "%{$keyword}%")->orWhere('payment_type', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $fundrequest_list =  DB::table('ewallet_requests')
                ->where('user_id', $user->id)
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where('amount', 'like', "%{$keyword}%")
                ->orWhere('payment_type', 'like', "%{$keyword}%")
               
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $fundrequest_list = Ewallet_requests::where('user_id',request('user_id'))->where('amount', 'like', "%{$keyword}%")->orWhere('payment_type', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $fundrequest_list =  DB::table('ewallet_requests')
                ->where('user_id', $user->id)
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $fundrequest_list = Ewallet_requests::where('user_id',request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif (request('request_type') == "team_request") {
            if(request('date') == 'all'){
                $fundrequest_list = Ewallet_requests::where('created_by',request('user_id'))->where('amount', 'like', "%{$keyword}%")->orWhere('payment_type', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $fundrequest_list =  DB::table('ewallet_requests')
                ->where('created_by', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where('amount', 'like', "%{$keyword}%")
                ->orWhere('payment_type', 'like', "%{$keyword}%")
               
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $fundrequest_list = Ewallet_requests::where('created_by',request('user_id'))->where('amount', 'like', "%{$keyword}%")->orWhere('payment_type', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $fundrequest_list =  DB::table('ewallet_requests')
                ->where('created_by', $user->id)
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $fundrequest_list = Ewallet_requests::where('created_by',request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }
       
        
        if (!empty($fundrequest_list[0])) {
            $response['status'] = "success";
            $response['message'] = "Fund Request list Available.";
            $response['user_status'] = $user->status;
            $response['description']['fundrequest_list'] = $fundrequest_list;
        }else{
            $response['status'] = 'error';  
            $response['user_status'] = $user->status;
            $response['message'] = 'Fund Request list Not Available.';
        }
        
        return response()->json($response, $this->successStatus);            
    }
    public function bankList(Request $request){
        $user = Auth::user();

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://fingpayap.tapits.in/fpaepsservice/api/bankdata/bank/details/',         
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,         ));
        $result = curl_exec($curl);
        $output = json_decode($result, true);
        $bank_data = $output['data'];
        $i = 0;
        foreach ($bank_data as $key => $value) {
            if ($key != 0) {
                $bank_detail[$i] = array('id' => $value['iinno'],'bankName' => $value['bankName'] );
                $i++;
            }
        }

        // $bank_detail = BankDetail::all();

        $response['status'] = 'success';
        $response['message'] = 'Bank list getting successfully!';
        $response['user_status'] = $user->status;
        $response['description']['bank_list'] = $bank_detail;
        return response()->json($response, $this->successStatus);
    }

    public function changePassword(Request $request) 
    {
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'otp' => 'required', 
            'type' => 'required', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $user = User::where('mobile',$request->mobile)
                        ->where('otp',$request->otp)->first();
        if($user->otp == $request->otp){
            // $user = Auth::user();
            $user->password = bcrypt(request('password'));

            $user->otp = NULL;

            if ($user->save()) {
                $response['status'] = 'success';
                $response['message'] = 'Password Change Successfully!';
                $response['type'] = $request->type;
                $response['description']['data'] = $user;
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Something went wrong';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = "Otp doesn't match...";
        }
        
        return response()->json($response, $this->successStatus); 
    }

    public function aepsOld(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            "errCode" => 'required',
            "fCount" => 'required',
            "fType" => 'required',
            "nmPoints" => 'required',
            "qScore" => 'required',
            "dpId" => 'required',
            "rdsId" => 'required',
            "rdsVer" => 'required',
            "dc" => 'required',
            "mi" => 'required',
            "mc" => 'required',
            "ci" => 'required',
            "sessionKey" => 'required',
            "hmac" => 'required',
            "PidDatatype" => 'required',
            "Piddata" => 'required',
            "aadhar_number" => 'required',
            "bank_id" => 'required',
            "mobile" => 'required',
            "deviceSRNO" => 'required',
            "amount" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "method" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $number = strval(rand(100000000000000, 999999999999999));
        $deviceTransaction = strval(rand(100000000000000, 999999999999999));
        $postData = array(
            "errCode" => $request->errCode,
            "errInfo" => "",
            "fCount" => $request->fCount,
            "fType" => $request->fType,
            "iCount" => "0",
            "iType" => "0",
            "pCount" => "0",
            "pType" => "0",
            "nmPoints" => $request->nmPoints,
            "qScore" => $request->qScore,
            "dpID" => $request->dpId,
            "rdsID" => $request->rdsId,
            "rdsVer" => $request->rdsVer,
            "dc" => $request->dc,
            "mi" => $request->mi,
            "mc" => $request->mc,
            "ci" => $request->ci,
            "sessionKey" => $request->sessionKey,
            "hmac" => $request->hmac,
            "PidDatatype" => $request->PidDatatype,
            "Piddata" => $request->Piddata,
            "aadhaarNumber" => $request->aadhar_number,
            "nationalBankIdentificationNumber" => $request->bank_id,
            "mobileNumber" => $request->mobile,
            "deviceSRNO" => $request->deviceSRNO,
            "transactionType" => "BE",
            "merchantTransactionId" => $number,
            "deviceTransactionId" => $deviceTransaction,
            "merchantUserName" => "8058995714",
            "merchantPin" => "2565",
            "transactionAmount" => $request->amount,
            "balanceAmount" => 0.0,
            "latitude" => $request->latitude,
            "longitude" => $request->longitude
        );


        $data = json_encode($postData);
        //$data = str_replace("/", "", $data);
        // var_dump($data);
        // exit;
        if ($request->method == 'withdrawal') {
            $url = "https://apiworld.co.in/api/AAEPS/AEPSExpressCashWithdrawal?tokenkey=API772473&password=0404&retailrId=8058995714";
        }elseif ($request->method == 'balance_enquiry') {
            $url = "https://apiworld.co.in/api/AAEPS/AEPSExpressBalanceEnquiry?tokenkey=API772473&password=0404";
        }elseif ($request->method == 'mini_statement') {
            $url = "https://apiworld.co.in/api/AAEPS/MiniStatement?tokenkey=API772473&password=0404";
        }else{
            return false;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
     
     
        //get response
        $output = curl_exec($ch);
        $getData = json_decode(json_decode($output));
 
        $response['status'] = 'success';
        $response['message'] = 'Transaction Successfully';
        $response['method'] = $request->method;
        $response['description']['aeps_data'] = array('status' => $getData->details->status, 'message' => $getData->details->message, 'detail' => $getData->details->data);

        return response()->json($response, $this->successStatus);
    }

    public function aeps(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            "errCode" => 'required',
            "fCount" => 'required',
            "fType" => 'required',
            "nmPoints" => 'required',
            "qScore" => 'required',
            "dpId" => 'required',
            "rdsId" => 'required',
            "rdsVer" => 'required',
            "dc" => 'required',
            "mi" => 'required',
            "mc" => 'required',
            "ci" => 'required',
            "sessionKey" => 'required',
            "hmac" => 'required',
            "PidDatatype" => 'required',
            "Piddata" => 'required',
            "aadhar_number" => 'required',
            "bank_id" => 'required',
            "mobile" => 'required',
            "deviceSRNO" => 'required',
            "amount" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "method" => 'required',
            "iCount" => 'required',
            "pCount" => 'required',
            "pType" => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }


        
            $aadhar = $request->aadhar_number;
            $req_mobile = $request->mobile;
            $login_id = $user->mobile;
            $pin = $user->aeps_pin;
            $number = strval(rand(100000000000000, 999999999999999)); 
            $deviceTransaction = strval(rand(100000000000000, 999999999999999));
            $indicatorforUID = "0";
            if ($request->method == "withdrawal") {
                $txn_method = "CW";
            }elseif ($request->method == "balance_enquiry") {
                $txn_method = "BE";
            }elseif ($request->method == "mini_statement") {
                $txn_method = "MS";
            }elseif ($request->method == "aadhar_pay") {
                $txn_method = "M";
            }else{
                return false;
            }

            $unique_id = "DMS".date("Ymd").time();

            if ($request->method == "aadhar_pay") {
                $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id','1')->first();
                    
                $getcommission = json_decode($usercommission->commission);
                

                foreach ($getcommission as $key => $value) {
                    if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                        if ($value->commission_type == "flat") {
                            $surcharge = $value->commission; 
                            $percentagecom = request('amount') + $value->commission; 
                            if ((request('amount') + $value->commission) <= $user->ewallet) {
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }elseif ($value->commission_type == "percentage") {
                            $surcharge = (request('amount')*$value->commission)/100;
                            $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            if($percentagecom <= $user->ewallet){
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }

                    }else{
                         $surcharge = 0;
                        if (request('amount') <= $user->ewallet) {
                            $response['status'] = 'success';
                        }else{
                            $response['status'] = 'error';
                            $response['user_status'] = $user->status;
                            $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                            return response()->json($response, $this->successStatus);
                        }
                    } 
                }
            }

            
            $data = array('captureResponse' => array('PidDatatype' => $request->PidDatatype,
                                                    'Piddata' => $request->Piddata, 
                                                    'ci' => $request->ci, 
                                                    'dc' => $request->dc, 
                                                    'dpID' => $request->dpId, 
                                                    'errCode' => $request->errCode, 
                                                    'errInfo' => $request->errInfo, 
                                                    'fCount' => $request->fCount, 
                                                    'fType' => $request->fType, 
                                                    'hmac' => $request->hmac, 
                                                    'iCount' => $request->iCount, 
                                                    'mc' => $request->mc, 
                                                    'mi' => $request->mi, 
                                                    'nmPoints' => $request->nmPoints, 
                                                    'pCount' => $request->pCount, 
                                                    'pType' => $request->pType, 
                                                    'qScore' => $request->qScore, 
                                                    'rdsID' => $request->rdsId, 
                                                    'rdsVer' => $request->rdsVer, 
                                                    'sessionKey' => $request->sessionKey, 
                                                  ), 
                        'cardnumberORUID' => array('adhaarNumber' => $request->aadhar_number,
                                                  'indicatorforUID' => $indicatorforUID,
                                                  'nationalBankIdentificationNumber' => $request->bank_id,),
                        'languageCode'=> "en",
                        'latitude'=> $request->latitude,
                        'longitude'=> $request->longitude,
                        'mobileNumber'=> $request->mobile,
                        'paymentType'=> "B",
                        'requestRemarks'=> "DMSPAY",
                        'deviceTransactionId'=> $deviceTransaction,
                        'timestamp'=> date("d/m/Y H:i:s"),
                        'transactionType'=> $txn_method,
                        'merchantUserName'=> $login_id,
                        'merchantPin'=> md5($pin), //796c3ee556ac31f3754a38cfd15b8044
                        'subMerchantId'=> "Damskartm",
                        'superMerchantId'=> "968",
                        );
            if($txn_method == "CW" || $txn_method == "M") {
               $data['transactionAmount'] = $request->amount;
            }
            if($txn_method == "BE") {
               $data['merchantTransactionId'] = $number;
            }else{
               $data['merchantTranId'] = $number;
            }
            
            $post = json_decode(json_encode($data)); // Request String
            $method = "POST"; // Method
            $dc = "352801082418919";
            /* Rendom kye for encryption */
            $key = '';
            $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            foreach ($mt_rand as $chr) {
                $key .= chr($chr);
            }

            $iv =   '06f2f04cc530364f';
            $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
            $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
           
            // $fp = fopen("fingpay_public_production.cer","r");
            $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
            $pub_key_string = fread($fp,8192);
            fclose($fp);
            openssl_public_encrypt($key,$crypttext,$pub_key_string);

            $header = [
            'Content-Type: text/xml',    
            'trnTimestamp:'.date('d/m/Y H:i:s'),
            'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
            'deviceIMEI:'.$dc,
            'eskey:'.base64_encode($crypttext)
            ];

            if ($txn_method == "CW") {
                $url = "https://fingpayap.tapits.in/fpaepsservice/api/cashWithdrawal/merchant/php/withdrawal";
            }elseif ($txn_method == "BE") {
                $url = "https://fingpayap.tapits.in/fpaepsservice/api/balanceInquiry/merchant/php/getBalance";
            }elseif ($txn_method == "MS") {
                $url = "https://fingpayap.tapits.in/fpaepsservice/api/miniStatement/merchant/php/statement";
            }elseif ($txn_method == "M") {
                $url = "https://fingpayap.tapits.in/fpaepsservice/api/aadhaarPay/merchant/php/pay";
            }else{
                return false;
            }
          
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            // CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_HTTPHEADER => $header
            ));
            $result = curl_exec($curl);
           
            //---------- Create Log File Start----------
            $date = date('Y-m-d H:i:s');
            $cryptedText = file_get_contents('php://input');
            $file = public_path().'/logs/ICICI-AEPS-Log.txt';

            $log = "\n\n".'Date - '.$date."================================================================ \n\n";
            $log .= 'Server IP- '.$_SERVER['SERVER_ADDR']."\n\n";
            $log .= 'Server Name- '.$_SERVER['SERVER_NAME']."\n\n";
            $log .= 'Header Data- '.json_encode($header)."\n\n";
            $log .= 'Request Encode Data- '.json_encode($data)."\n\n";
            $log .= 'Response Encode Data- '.$result."\n\n";
            file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
            // ---------- Create Log File End------------
            
             // $result = array(
             //          "status"=> true,
             //          "message"=> "Request Completed",
             //          "data"=> array(
             //            "terminalId"=>"FA212700",
             //            "requestTransactionTime"=>"03/03/2021 15:37:32",
             //            "transactionAmount"=>0.0,
             //            "transactionStatus"=>"successful",
             //            "balanceAmount"=>4674.37,
             //            "strMiniStatementBalance"=>null,
             //            "bankRRN"=>"106215619488",
             //            "transactionType"=>"BE",
             //            "fpTransactionId"=>"BEBS1568303030321153732243I",
             //            "merchantTxnId"=>null,
             //            "errorCode"=>null,
             //            "errorMessage"=>null,
             //            "merchantTransactionId"=>"795144329984046",
             //            "miniStatementStructureModel"=> null,
             //          ),
             //          "statusCode"=> 10000
             //        ); 

            // $array = json_decode(json_encode($result), true);
            $array = json_decode($result, true);
           
            if ($array['status'] == false) {
                $response['status'] = 'error';
                if ($array['message'] == null) {
                    $response['message'] = "Transaction Failed!";
                }else{
                    $response['message'] = $array['message'];
                }
                $response['user_status'] = $user->status;
                return response()->json($response, $this->successStatus);
            }elseif($array['status'] == true && $array['data']['responseCode'] == "00"){
              $icici_data = new Aeps_txn_news();
              $icici_data->user_id = $user->id;
              $icici_data->created_by = $user->created_by;
              $icici_data->dms_txn_id = $unique_id;
              $icici_data->message = $array['message'];
              $icici_data->mobile = $req_mobile;
              $icici_data->aadhar_number = $aadhar;
              $icici_data->terminal_id = $array['data']['terminalId'];
              $icici_data->request_transaction_time = $array['data']['requestTransactionTime'];

              $request_transaction_time = $array['data']['requestTransactionTime'];
              $request_transaction_time = str_replace('/', '-', $request_transaction_time);

              $icici_data->request_transaction_time1 = date('Y-m-d H:i:s', strtotime($request_transaction_time));
              $icici_data->transaction_amount = $array['data']['transactionAmount'];
              $icici_data->transaction_status = $array['data']['transactionStatus'];
              $icici_data->str_ms_bal = $array['data']['strMiniStatementBalance'];
              $icici_data->balance_amount = $array['data']['balanceAmount'];
              $icici_data->bankRRN = $array['data']['bankRRN'];
              $icici_data->transaction_type = $array['data']['transactionType'];
              $icici_data->merchant_txn_id_ms = $array['data']['merchantTxnId'];
              $icici_data->fp_transaction_id = $array['data']['fpTransactionId'];
              $icici_data->merchant_transaction_id = $array['data']['merchantTransactionId'];
              if ($array['data']['miniStatementStructureModel'] == null) {
                $icici_data->ms_structure_model = NULL;  
              }else{
                $icici_data->ms_structure_model = json_encode($array['data']['miniStatementStructureModel']);
              }
              // $icici_data->ms_structure_model = $array['data']['miniStatementStructureModel'];
              $icici_data->error_message = $array['data']['errorMessage'];
              if ($icici_data->save()) {
                  if ($icici_data->transaction_status == "successful") {
                    if ($icici_data->transaction_type == "CW") {
                        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id','16')->first();

                        $getcommission = json_decode($usercommission->commission);

                        $is_commission = 0;
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $icici_data->transaction_amount && $value->end_price >= $icici_data->transaction_amount) {
                                $data = new User_transactions();
                                $data->user_id = $user->id;
                                $data->created_by = $user->created_by;
                                $data->transaction_id = $unique_id;
                                $data->amount = $icici_data->transaction_amount;
                                $data->current_balance = $user->aeps_wallet + $icici_data->transaction_amount;
                                $data->narration = "ICICI AEPS CW Success TXN:".$icici_data->dms_txn_id;
                                $data->type = "credit";
                                $data->label = 'aeps_request';
                                $data->transaction_type = "ICICI AEPS CW";
                                $data->status = $icici_data->transaction_status;
                                $data->date = date('Y-m-d H:i:s');

                                $datacr = new User_transactions();
                                $datacr->user_id = $user->id;
                                $datacr->created_by = $user->created_by;
                                $datacr->transaction_id = $unique_id;
                                $datacr->type = "credit";
                                $datacr->label = 'ewallet';
                                $datacr->status = $icici_data->transaction_status;
                                $datacr->date = date('Y-m-d H:i:s');
                                $datacr->narration = "ICICI AEPS CW Commission Success TXN:".$icici_data->dms_txn_id;
                                $datacr->transaction_type = "ICICI AEPS CW";

                                if ($value->commission_type == "flat") {
                                    $comamount = $value->commission;
                                    $datacr->amount = $value->commission;
                                    $datacr->current_balance = $user->ewallet + $value->commission;
                                    $user->aeps_wallet = $data->current_balance;
                                    $user->ewallet = $datacr->current_balance;
                                    $user->save();
                                    $data->save();
                                    $datacr->save();
                                }elseif ($value->commission_type == "percentage") {
                                    $comamount = ($icici_data->transaction_amount*$value->commission)/100;
                                    $datacr->amount = ($icici_data->transaction_amount*$value->commission)/100;
                                    $datacr->current_balance = $user->ewallet + (($icici_data->transaction_amount*$value->commission)/100);
                                    $user->aeps_wallet = $data->current_balance;
                                    $user->ewallet = $datacr->current_balance;
                                    $user->save();
                                    $data->save(); 
                                    $datacr->save();
                                }

                                $icici_datacom = Aeps_txn_news::where('fp_transaction_id',$icici_data->fp_transaction_id)->first();
                                $icici_datacom->commission = $comamount;
                                $icici_datacom->current_balance = $user->aeps_wallet;
                                $icici_datacom->save();

                                $mobile = $user->mobile;
                                $amt = $data->amount;
                                $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your AEPS Wallet credited with INR '.number_format($amt,2, '.', '').' on '.$data->date.'. Info: (ICICI AEPS CW Successful. Txn Id:'.$data->transaction_id.', Principle Amt:'.$data->amount.', Commission Amt:'.$datacr->amount.'). Avbl AEPS Bal:INR '.number_format($user->aeps_wallet,2, '.', '').' Avbl Main Bal:INR '.number_format($user->ewallet,2, '.', ''));
                                $api_key = '25F6EF10FE18AF';
                                $contacts = $mobile;
                                $from = 'DMSKRT';
                                $ch = curl_init();
                                curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                                $output = curl_exec($ch);

                                $is_commission = 1;
                                // $icici_data1 = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);  
                                // return response()
                                //     ->json($icici_data1, 200);
                                $response['status'] = 'success';
                                $response['message'] = 'Transaction Successfully';
                                $response['user_status'] = $user->status;
                                // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);
                                $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                            }
                        }

                        if($is_commission == 0) {
                            $data = new User_transactions();
                            $data->user_id = $user->id;
                            $data->created_by = $user->created_by;
                            $data->transaction_id = $unique_id;
                            $data->amount = $icici_data->transaction_amount;
                            $data->current_balance = $user->aeps_wallet + $icici_data->transaction_amount;
                            $data->narration = "ICICI AEPS CW Failed TXN:".$icici_data->dms_txn_id;
                            $data->transaction_type = "ICICI AEPS CW";
                            $data->type = "credit";
                            $data->label = 'aeps_request';
                            $data->status = 'Failed';
                            $data->date = date('Y-m-d H:i:s');
                            $user->aeps_wallet = $data->current_balance;
                            $user->save();
                            $data->save();
                        }
                      // $icici_data1 = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);  
                      // return response()
                      //       ->json($icici_data1, 200);
                        $response['status'] = 'success';
                        $response['message'] = 'Transaction Successfully';
                        $response['user_status'] = $user->status;
                        $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                        // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);
                    }elseif ($icici_data->transaction_type == "M") {
                        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id','1')->first();

                        $getcommission = json_decode($usercommission->commission);
                        $is_commission = 0;
                        foreach ($getcommission as $key => $value) {
                            if ($value->start_price <= $icici_data->transaction_amount && $value->end_price >= $icici_data->transaction_amount) {
                                $data = new User_transactions();
                                $data->user_id = $user->id;
                                $data->created_by = $user->created_by;
                                $data->transaction_id = $unique_id;
                                $data->amount = $icici_data->transaction_amount;
                                $data->current_balance = $user->aeps_wallet + $icici_data->transaction_amount;
                                $data->narration = "ICICI AEPS AadharPay Success TXN:".$icici_data->dms_txn_id;
                                $data->type = "credit";
                                $data->label = 'aeps_request';
                                $data->status = $icici_data->transaction_status;
                                $data->transaction_type = "ICICI AEPS AAdharPay";
                                $data->date = date('Y-m-d H:i:s');

                                $datacr = new User_transactions();
                                $datacr->user_id = $user->id;
                                $datacr->created_by = $user->created_by;
                                $datacr->transaction_id = $unique_id;
                                $datacr->type = "debit";
                                $datacr->label = 'ewallet';
                                $datacr->status = $icici_data->transaction_status;
                                $datacr->date = date('Y-m-d H:i:s');
                                $datacr->narration = "ICICI AEPS AadharPay Surcharge Success TXN:".$icici_data->dms_txn_id;
                                $datacr->transaction_type = "ICICI AEPS AAdharPay";

                                if ($value->commission_type == "flat") {
                                  $comamount = $value->commission;
                                  $datacr->amount = $value->commission;
                                  $datacr->current_balance = $user->ewallet - $value->commission;
                                    $user->aeps_wallet = $data->current_balance;
                                    $user->ewallet = $datacr->current_balance;
                                    $user->save();
                                  $data->save();
                                  $datacr->save();
                                }elseif ($value->commission_type == "percentage") {
                                  $comamount = ($icici_data->transaction_amount*$value->commission)/100;
                                  $datacr->amount = ($icici_data->transaction_amount*$value->commission)/100;
                                  $datacr->current_balance = $user->ewallet - (($icici_data->transaction_amount*$value->commission)/100);
                                      $user->aeps_wallet = $data->current_balance;
                                      $user->ewallet = $datacr->current_balance;
                                      $user->save();
                                  $data->save(); 
                                  $datacr->save();
                                }

                                $icici_datacom = Aeps_txn_news::where('fp_transaction_id',$icici_data->fp_transaction_id)->first();
                                $icici_datacom->commission = $comamount;
                                $icici_datacom->current_balance = $user->aeps_wallet;
                                $icici_datacom->save();

                                $mobile = $user->mobile;
                                $amt = $data->amount;
                                $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your AEPS Wallet credited with INR '.number_format($amt,2, '.', '').' on '.$data->date.'. Info: (ICICI AEPS AadharPay Successful. Txn Id:'.$data->transaction_id.', Principle Amt:'.$data->amount.', Surcharge Amt:'.$datacr->amount.'). Avbl AEPS Bal:INR '.number_format($user->aeps_wallet,2, '.', '').' Avbl Main Bal:INR '.number_format($user->ewallet,2, '.', ''));
                                $api_key = '25F6EF10FE18AF';
                                $contacts = $mobile;
                                $from = 'DMSKRT';
                                $ch = curl_init();
                                curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                                $output = curl_exec($ch);
                                // $icici_data1 = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);  
                                // return response()
                                //     ->json($icici_data1, 200);

                                $is_commission = 1;
                                $response['status'] = 'success';
                                $response['message'] = 'Transaction Successfully';
                                $response['user_status'] = $user->status;
                                // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);
                                $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                            }
                        }

                        if($is_commission == 0)
                        {
                            $data = new User_transactions();
                            $data->user_id = $user->id;
                            $data->created_by = $user->created_by;
                            $data->transaction_id = $unique_id;
                            $data->amount = $icici_data->transaction_amount;
                            $data->current_balance = $user->aeps_wallet + $icici_data->transaction_amount;
                            $data->narration = "ICICI AEPS AadharPay Failed TXN:".$icici_data->dms_txn_id;
                            $data->transaction_type = "ICICI AEPS AAdharPay";
                            $data->type = "credit";
                            $data->label = 'aeps_request';
                            $data->status = 'Failed';
                            $data->date = date('Y-m-d H:i:s');
                            $user->aeps_wallet = $data->current_balance;
                            $user->save();
                            $data->save();

                            $icici_datacom = Aeps_txn_news::where('fp_transaction_id',$icici_data->fp_transaction_id)->first();
                            $icici_datacom->commission = "0";
                            $icici_datacom->current_balance = $user->aeps_wallet;
                            $icici_datacom->save();

                            $mobile = $user->mobile;
                            $amt = $data->amount;
                            $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your AEPS Wallet credited with INR '.number_format($amt,2, '.', '').' on '.$data->date.'. Info: (ICICI AEPS AadharPay Successful. Txn Id:'.$data->transaction_id.'). Avbl AEPS Bal:INR '.number_format($user->aeps_wallet,2, '.', '').' Avbl Main Bal:INR '.number_format($user->ewallet,2, '.', ''));
                            $api_key = '25F6EF10FE18AF';
                            $contacts = $mobile;
                            $from = 'DMSKRT';
                            $ch = curl_init();
                            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                            $output = curl_exec($ch);
                        }

                        // $icici_data1 = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);  
                        // return response()
                        //       ->json($icici_data1, 200);
                        $response['status'] = 'success';
                        $response['message'] = 'Transaction Successfully';
                        $response['user_status'] = $user->status;
                        // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);
                        $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                    } elseif ($icici_data->transaction_type == "BE") {
                        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id','2')->first();
                        $comamount = $usercommission->commission;
                        $datacr = new User_transactions();
                        $datacr->user_id = $user->id;
                        $datacr->created_by = $user->created_by;
                        $datacr->transaction_id = $unique_id;
                        $datacr->amount = $comamount;
                        $datacr->current_balance = $user->ewallet + $comamount;
                        $datacr->narration = "ICICI AEPS BE Success TXN:".$icici_data->dms_txn_id;
                        $datacr->type = "credit";
                        $datacr->label = 'ewallet';
                        $datacr->status = $icici_data->transaction_status;
                        $datacr->date = date('Y-m-d H:i:s');
                        $datacr->transaction_type = "ICICI AEPS BE";
                      
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                        $datacr->save(); 

                        $icici_datacom = Aeps_txn_news::where('fp_transaction_id',$icici_data->fp_transaction_id)->first();
                        $icici_datacom->commission = $comamount;
                        $icici_datacom->current_balance = $user->aeps_wallet;
                        $icici_datacom->save();

                        $mobile = $user->mobile;
                        $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your Main Wallet credited with INR '.number_format($datacr->amount,2, '.', '').' on '.$datacr->date.'. Info: (ICICI AEPS BE Successful. Txn Id:'.$datacr->transaction_id.'). Avbl Main Bal:INR '.number_format($user->ewallet,2, '.', ''));
                        $api_key = '25F6EF10FE18AF';
                        $contacts = $mobile;
                        $from = 'DMSKRT';
                        $ch = curl_init();
                        curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                        $output = curl_exec($ch);

                        // $response = "ICICI BE Success";
                        // $icici_data1 = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);  
                        // return response()
                        //     ->json($icici_data1, 200);
                        $response['status'] = 'success';
                        $response['message'] = 'Transaction Successfully';
                        $response['user_status'] = $user->status;
                        // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'icici_data' => $icici_data);
                        $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                    }elseif ($icici_data->transaction_type == "MS") {
                      $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id','9')->first();
                      $comamount = $usercommission->commission; 
                      $datacr = new User_transactions();
                      $datacr->user_id = $user->id;
                      $datacr->created_by = $user->created_by;
                      $datacr->transaction_id = $unique_id;
                      $datacr->amount = $comamount;
                      $datacr->current_balance = $user->ewallet + $comamount;
                      $datacr->narration = "ICICI AEPS MS Success TXN:".$icici_data->dms_txn_id;
                      $datacr->type = "credit";
                      $datacr->label = 'ewallet';
                      $datacr->status = $icici_data->transaction_status;
                      $datacr->date = date('Y-m-d H:i:s');
                      $datacr->transaction_type = "ICICI AEPS MS";
                    
                          $user->ewallet = $datacr->current_balance;
                          $user->save();
                      $datacr->save(); 

                      $icici_datacom = Aeps_txn_news::where('fp_transaction_id',$icici_data->fp_transaction_id)->first();
                      $icici_datacom->commission = $comamount;
                      $icici_datacom->current_balance = $user->aeps_wallet;
                      $icici_datacom->save();

                      $mobile = $user->mobile;
                      $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your Main Wallet credited with INR '.number_format($datacr->amount,2, '.', '').' on '.$datacr->date.'. Info: (ICICI AEPS MS Successful. Txn Id:'.$datacr->transaction_id.'). Avbl Main Bal:INR '.number_format($user->ewallet,2, '.', ''));
                      $api_key = '25F6EF10FE18AF';
                      $contacts = $mobile;
                      $from = 'DMSKRT';
                      $ch = curl_init();
                      curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                      $output = curl_exec($ch);

                      // $response = "MS Success";
                      // return response()->json($response, $this->successStatus);

                      // $icici_data1 = array('mobile' => $icici_data->mobile,'ms_structure_model' => json_decode($icici_data->ms_structure_model),'icici_data' => $icici_data);  
                      // return response()
                      //       ->json($icici_data1, 200);
                        $response['status'] = 'success';
                        $response['message'] = 'Transaction Successfully';
                        $response['user_status'] = $user->status;
                        $icici_data->ms_structure_model = $array['data']['miniStatementStructureModel'];
                        
                        // $response['description']['aeps_data'] = array('mobile' => $icici_data->mobile,'ms_structure_model' => json_decode($icici_data->ms_structure_model),'icici_data' => $icici_data);
                        $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                    }else {
                        // $response = "Invalid Type";
                        $response['status'] = 'error';
                        $response['message'] = 'Invalid Type';
                        $response['user_status'] = $user->status;
                    }
                    // $response = "Aeps Success";
                    $response['status'] = 'success';
                    $response['message'] = 'Aeps Success';
                    $response['user_status'] = $user->status;
                    $response['description']['aeps_data'] = array('status' => $icici_data->transaction_status, 'message' => $icici_data->message, 'detail' => $icici_data);
                  }else{
                    // $response = "Failed";
                    $response['status'] = 'error';
                    $response['message'] = 'Failed';
                    $response['user_status'] = $user->status;
                  }  
              }else{
                $response['status'] = 'error';
                $response['message'] = 'Aeps Error';
                $response['user_status'] = $user->status;
              }
              return response()->json($response, $this->successStatus);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'FAILURE';
                $response['user_status'] = $user->status;
            }
        
        return response()->json($response, $this->successStatus);
    }

    public function iciciaepsEKYCSendOTP(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            "user_id" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $ekyc_mobile = $user->mobile;
        $ekyc_aadhar_number = $user->aadhar_number;
        $ekyc_pan_number = $user->pan_number;

        $user_ekyc_status = $user->aeps_ekyc_status;
        $data = array(
                  "superMerchantId" => 968,
                  "merchantLoginId" => $ekyc_mobile,
                  "transactionType" => "EKY",
                  "mobileNumber" => $ekyc_mobile,
                  "aadharNumber" => $ekyc_aadhar_number,
                  "panNumber" => $ekyc_pan_number,
                  "matmSerialNumber" => "",
                  "latitude" => 26.912434,
                  "longitude" => 75.787270
                );

        // print_r($data);
        // exit;
        $post = json_decode(json_encode($data)); // Request String
        $method = "POST"; // Method
        $dc = "352801082418919";
        /* Rendom kye for encryption */
        $key = '';
        $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        foreach ($mt_rand as $chr) {
            $key .= chr($chr);
        }

        $iv =   '06f2f04cc530364f';
        $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
        $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
       
        // $fp = fopen("fingpay_public_production.cer","r");
        $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
        $pub_key_string = fread($fp,8192);
        fclose($fp);
        openssl_public_encrypt($key,$crypttext,$pub_key_string);

        $header = [
        'Content-Type: text/xml',    
        'trnTimestamp:'.date('d/m/Y H:i:s'),
        'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
        'deviceIMEI:'.$dc,
        'eskey:'.base64_encode($crypttext)
        ];

        $url = "https://fpekyc.tapits.in/fpekyc/api/ekyc/merchant/php/sendotp";
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        // CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $request,
        CURLOPT_HTTPHEADER => $header
        ));
        $result = curl_exec($curl);
        $kyc_validateotp_response = json_decode(json_encode($result));

        $response['status'] = 'success';
        $response['message'] = 'KYC Data';
        $response['description']['kyc_data'] = json_decode($result);


        return response()->json($response, $this->successStatus);
    }

    public function iciciaepsEKYCResendOTP(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            "user_id" => 'required',
            "ekyc_primaryKeyId" => 'required',
            "ekyc_encodeFPTxnId" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $ekyc_res_mobile = $user->mobile;
        $ekyc_res_primaryKeyId = request('ekyc_primaryKeyId');
        $ekyc_res_encodeFPTxnId = request('ekyc_encodeFPTxnId');

        $data = array(
                  "superMerchantId" => 968, 
                  "merchantLoginId" => $ekyc_res_mobile , 
                  "primaryKeyId" => $ekyc_res_primaryKeyId,
                  "encodeFPTxnId" => $ekyc_res_encodeFPTxnId,
                );

        // print_r($data);
        // exit;
        $post = json_decode(json_encode($data)); // Request String
        $method = "POST"; // Method
        $dc = "352801082418919";
        /* Rendom kye for encryption */
        $key = '';
        $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        foreach ($mt_rand as $chr) {
            $key .= chr($chr);
        }

        $iv =   '06f2f04cc530364f';
        $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
        $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
       
        // $fp = fopen("fingpay_public_production.cer","r");
        $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
        $pub_key_string = fread($fp,8192);
        fclose($fp);
        openssl_public_encrypt($key,$crypttext,$pub_key_string);

        $header = [
        'Content-Type: text/xml',    
        'trnTimestamp:'.date('d/m/Y H:i:s'),
        'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
        'deviceIMEI:'.$dc,
        'eskey:'.base64_encode($crypttext)
        ];

        $url = "https://fpekyc.tapits.in/fpekyc/api/ekyc/merchant/php/resendotp";
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        // CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $request,
        CURLOPT_HTTPHEADER => $header
        ));
        $result = curl_exec($curl);

        //---------- Create Log File Start----------
        $date = date('Y-m-d H:i:s');
        $cryptedText = file_get_contents('php://input');
        $file = public_path().'/logs/ICICI-AEPS-EKYC-Log.txt';

        $log = "\n\n".'Date - '.$date."================================================================ \n";
        $log .= 'Server IP- '.$_SERVER['SERVER_ADDR']."\n\n";
        $log .= 'Request Header Data- '.json_encode($header)."\n\n";
        $log .= 'Request Encode Data- '.json_encode($data)."\n\n";
        $log .= 'Response Encode Data- '.json_encode(json_decode($result))."\n\n";
        file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
        // ---------- Create Log File End------------

        $response['status'] = "success";
        $response['message'] = "OTP Send Successfully";
        $response['description']['kyc_data'] = json_decode($result);
        return response()->json($response, $this->successStatus);
    }

    public function iciciaepsEKYCVerifyOTP(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            "user_id" => 'required',
            "ekyc_otp" => 'required',
            "ekyc_primaryKeyId" => 'required',
            "ekyc_encodeFPTxnId" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $ekyc_otp = request('ekyc_otp');
        $ekyc_res_mobile = $user->mobile;
        $ekyc_res_primaryKeyId = request('ekyc_primaryKeyId');
        $ekyc_res_encodeFPTxnId = request('ekyc_encodeFPTxnId');

        $data = array(
                  "superMerchantId" => 968, 
                  "merchantLoginId" => $ekyc_res_mobile , 
                  "otp" => $ekyc_otp,
                  "primaryKeyId" => $ekyc_res_primaryKeyId,
                  "encodeFPTxnId" => $ekyc_res_encodeFPTxnId,
                );

        // print_r($data);
        // exit;
        $post = json_decode(json_encode($data)); // Request String
        $method = "POST"; // Method
        $dc = "352801082418919";
        /* Rendom kye for encryption */
        $key = '';
        $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        foreach ($mt_rand as $chr) {
            $key .= chr($chr);
        }

        $iv =   '06f2f04cc530364f';
        $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
        $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
       
        // $fp = fopen("fingpay_public_production.cer","r");
        $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
        $pub_key_string = fread($fp,8192);
        fclose($fp);
        openssl_public_encrypt($key,$crypttext,$pub_key_string);

        $header = [
            'Content-Type: text/xml',    
            'trnTimestamp:'.date('d/m/Y H:i:s'),
            'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
            'deviceIMEI:'.$dc,
            'eskey:'.base64_encode($crypttext)
        ];

        $url = "https://fpekyc.tapits.in/fpekyc/api/ekyc/merchant/php/validateotp";
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        // CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $request,
        CURLOPT_HTTPHEADER => $header
        ));
        $result = curl_exec($curl);

        // $kyc_validateotp_response = json_decode(json_encode($result));
        $response['status'] = "success";
        $response['message'] = "OTP Verified Successfully";
        $response['description']['kyc_data'] = json_decode($result);
        return response()->json($response, $this->successStatus);
    }

    public function iciciaepsEKYCComplete(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            "errCode" => 'required',
            "fCount" => 'required',
            "fType" => 'required',
            "nmPoints" => 'required',
            "qScore" => 'required',
            "dpId" => 'required',
            "rdsId" => 'required',
            "rdsVer" => 'required',
            "dc" => 'required',
            "mi" => 'required',
            "mc" => 'required',
            "ci" => 'required',
            "sessionKey" => 'required',
            "hmac" => 'required',
            "PidDatatype" => 'required',
            "Piddata" => 'required',
            "bank_id" => 'required',
            "deviceSRNO" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "iCount" => 'required',
            "pCount" => 'required',
            "pType" => 'required',
            "ekyc_primaryKeyId" => 'required',
            "ekyc_encodeFPTxnId" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $ekyc_res_mobile = $user->mobile;
        $ekyc_res_primaryKeyId = request('ekyc_primaryKeyId');
        $ekyc_res_encodeFPTxnId = request('ekyc_encodeFPTxnId');
        $indicatorforUID = "0";

        $data = array(
                    'superMerchantId'=> 968,
                    'merchantLoginId'=> $ekyc_res_mobile,
                    'primaryKeyId'=> $ekyc_res_primaryKeyId,
                    'encodeFPTxnId'=> $ekyc_res_encodeFPTxnId,
                    'requestRemarks'=> "DMSPAY",
                    'cardnumberORUID' => array(
                      'nationalBankIdentificationNumber' => $request->bank_id,
                      'indicatorforUID' => $indicatorforUID,
                      'adhaarNumber' => $user->aadhar_number,
                    ),
                    'captureResponse' => array(
                        'PidDatatype' => $request->PidDatatype,
                        'Piddata' => $request->Piddata, 
                        'ci' => $request->ci, 
                        'dc' => $request->dc, 
                        'dpID' => $request->dpId, 
                        'errCode' => $request->errCode, 
                        'errInfo' => $request->errInfo, 
                        'fCount' => $request->fCount, 
                        'fType' => $request->fType, 
                        'hmac' => $request->hmac, 
                        'iCount' => $request->iCount, 
                        'iType' => null, 
                        'mc' => $request->mc, 
                        'mi' => $request->mi, 
                        'nmPoints' => $request->nmPoints, 
                        'pCount' => $request->pCount, 
                        'pType' => $request->pType, 
                        'qScore' => trim($request->qScore), 
                        'rdsID' => $request->rdsId, 
                        'rdsVer' => $request->rdsVer, 
                        'sessionKey' => $request->sessionKey, 
                    ), 
                );

        //echo '<pre>';print_r($data); exit;
        // exit;

        //dd($request->all());

        $post = json_decode(json_encode($data)); // Request String
        $method = "POST"; // Method

        if(isset($request->dc))
        {
            $dc = $request->dc;
        }
        else
        {
            $dc = "352801082418919"; ;//"352801082418919";
        }
        /* Rendom kye for encryption */
        $key = '';
        $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        foreach ($mt_rand as $chr) {
            $key .= chr($chr);
        }

        $iv =   '06f2f04cc530364f';
        $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
        $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
       
        // $fp = fopen("fingpay_public_production.cer","r");
        $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
        $pub_key_string = fread($fp,8192);
        fclose($fp);
        openssl_public_encrypt($key,$crypttext,$pub_key_string);

        $header = [
          'Content-Type: text/xml',    
          'trnTimestamp:'.date('d/m/Y H:i:s'),
          'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
          'deviceIMEI:'.$dc,
          'eskey:'.base64_encode($crypttext)
        ];

        //echo '<pre>';print_r($header); exit;
        
        $url = "https://fpekyc.tapits.in/fpekyc/api/ekyc/merchant/php/biometric";
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        // CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $request,
        CURLOPT_HTTPHEADER => $header
        ));
        $result = curl_exec($curl);

        //---------- Create Log File Start----------
        $date = date('Y-m-d H:i:s');
        $cryptedText = file_get_contents('php://input');
        $file = public_path().'/logs/ICICI-AEPS-EKYC-Log.txt';

        $log = "\n\n".'Date - '.$date."================================================================ \n";
        $log .= 'Server IP- '.$_SERVER['SERVER_ADDR']."\n\n";
        $log .= 'Request Encode Data- '.json_encode($data)."\n\n";
        $log .= 'Response Encode Data- '.json_encode(json_decode($result))."\n\n";
        file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
        // ---------- Create Log File End------------
        
        // $result = array(
        //             "status" => true,
        //             "message" => "E-KYC Completed Successfully",  
        //             "data"=> null,
        //             "statusCode" => "10000"
        //           );
        // $result = array(
        //             "status" => false,
        //             "message"  => "error message",
        //             "data" => null,
        //             "statusCode" => "10004"
        //           );
          $get_result = json_decode($result);


        $response['status'] = 'success';
        if ($get_result->status == true) {
          // $userstatus = User::where('mobile',request('ekyc_res_mobile'))->first();
          // $userstatus->aeps_ekyc_status = 1;
          // $userstatus->save();
          $user->aeps_ekyc_status = 1;
          $user->save();
          $response['description']['kyc_data'] = json_decode($result);
          // $kyc_complete_response = json_decode(json_encode($result));
        }else{
          //$result['dc'] = $dc;
            //
            $kyc_complete_response = json_decode($result);
            $response['message'] = str_replace("Biometricdatadidnotmatch", "Biometric data did not match. Pleae try again", $kyc_complete_response->message);
            $response['description']['kyc_data'] = json_decode($result);
            

            //echo '<pre>'; print_r($kyc_complete_response); exit;

          // $kyc_complete_response = json_decode(json_encode($result));
        }

        //echo '<pre>';print_r($data); print_r($result); exit;

        return response()->json($response, $this->successStatus);
    }
    

    public function addBenificiaryOld(Request $request){
        $validator = Validator::make($request->all(), [ 
            'account_number' => 'required', 
            'ifsc_code' => 'required',
            'benificiary_name' => 'required',
            'bank_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error'; 
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }
        $user = Auth::user();

        $url ="http://payu.startrecharge.in/Benificiary/AccountReg?token=1BZd0YzLFV110iMVBj4UYj4FqMJUoG&AccountNumber=".$request->account_number."&IFSC=".$request->ifsc_code."&Name=".urlencode($request->benificiary_name)."&BankName=".urlencode($request->bank_name)."&Email=".$request->email."&Mobile=".$request->mobile."&CustomerID=".$user->customer_id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);

        $benificiary_data = json_decode($output);

        if ($benificiary_data->Status == 'Success') {
            $benificiary = new Customer_accounts();
            $benificiary->user_id = $user->created_by;
            $benificiary->created_by = $user->id;
            $benificiary->benificiary_name = $benificiary_data->data->AccountHolder;
            $benificiary->account = $benificiary_data->data->AccountNumber;
            $benificiary->ifsc_code = $benificiary_data->data->IFSC;
            $benificiary->bank_name = $benificiary_data->data->BankName;
            $benificiary->mobile = $benificiary_data->data->Mobile;
            $benificiary->email = $benificiary_data->data->Email;
            $benificiary->status = 1;
            $benificiary->BID = $benificiary_data->data->BID;
            if($benificiary->save()){
                $response['status'] = 'success';
                $response['message'] = 'Benificiary Added Successfully!!!';
            }else{
                $response['status'] = 'success';
                $response['message'] = 'Benificiary Not Added Successfully!!!';
            }
        }
        else{
            $response['status'] = 'error';
            $response['message'] = 'Benificiary Not Added';
        }
        return response()->json($response, $this->successStatus);
    }

    public function addBenificiary(Request $request){
         $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'account_number' => 'required', 
            'ifsc_code' => 'required',
            'benificiary_name' => 'required',
            'bank_name' => 'required', 
            'mobile' => 'required',
            'email' => 'email',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error'; 
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

            $customer_accounts = Customer_accounts::orderBy('created_at', 'desc')->first();
            $benificiary = new Customer_accounts();
            $benificiary->user_id = $user->id;
            $benificiary->created_by = $user->created_by;
            $benificiary->benificiary_name = request('benificiary_name');
            $benificiary->account = request('account_number');
            $benificiary->ifsc_code = request('ifsc_code');
            $benificiary->bank_name = request('bank_name');
            $benificiary->mobile = request('mobile');
            if (!empty(request('email'))) {
                $benificiary->email = request('email');
            }
            if (!empty(request('nick_name'))) {
                $benificiary->nick_name = request('nick_name');
            }
            $benificiary->status = 1;
            if (!empty($customer_accounts->id)) {
              $benificiary->BID = 'DMS'.($customer_accounts->id + 1).date('Y');
            }else{
              $benificiary->BID = 'DMS01'.date('Y');
            }

            if($benificiary->save()){
                $response['status'] = 'success';
                $response['user_status'] = $user->status;
                $response['message'] = 'Benificiary Added Successfully!!!';
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Benificiary Not Added Successfully!!!';
            }
        
       
        return response()->json($response, $this->successStatus);
    }

    public function verifyAccountOld(Request $request){
        $validator = Validator::make($request->all(), [ 
            'account_number' => 'required', 
            'ifsc_code' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $url = "http://payu.startrecharge.in/Bank/AccountVerify?token=1BZd0YzLFV110iMVBj4UYj4FqMJUoG&Account=".$request->account_number."&IFSC=".$request->ifsc_code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);           

        $data = json_decode($output);
        
        if($data->data->AccountExists == 'YES'){
            $response['status'] = 'success';
            $response['message'] = 'Account Founded';
            $response['description']['account_data'] = $data->data->BeneficiaryName;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Account Not Found';
        }
        
        return response()->json($response, $this->successStatus);
    }
    
    public function verifyAccount(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'account_number' => 'required', 
            'ifsc_code' => 'required', 
            'mobile' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        } 
        
        if ($user->ewallet < 3) {
            $response['status'] = 'error';
            $response['message'] = 'You do not have enough amount!';
            return response()->json($response, $this->successStatus);
        }
        $txnid = rand(1000000000,9999999999);
       
        $url = "http://mobilerechargenow.com/api/v2/dmt/bankvalidate.php?username=G207281212&apikey=1410397768&mobile=".$request->mobile."&accountno=".$request->account_number."&ifsccode=".$request->ifsc_code."&txnid=".$txnid;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);            

        $data = json_decode($output);

        if($data->verifyStatus == 'VERIFIED'){

              $data1 = new User_transactions();
              $data1->user_id = $user->id;
              $data1->created_by = Auth::user()->created_by;
              $data1->transaction_id = $data->TxnId;
              $data1->amount = 3;
              $data1->current_balance = $user->ewallet - 3;
              $data1->narration = "Verify Account Success for ".$request->account_number.", Info: ".$data1->transaction_id;
              $data1->type = "debit";
              $data1->label = 'ewallet';
              $data1->status = $data->status;
              $data1->date = date('Y-m-d H:i:s');
                $user->ewallet = $data1->current_balance;
                $user->save();
              $data1->save();
            // $user->ewallet = $user->ewallet - 3;
            // $user->save();
            $response['status'] = 'success';
            $response['message'] = 'Account Founded';
            $response['user_status'] = $user->status;
            $response['description']= $data;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Account Not Found';
        }
        
        return response()->json($response, $this->successStatus);
    }
    

    public function benificiaryList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users',
            // 'keyword' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) { 
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if($user->mobile != request('mobile') || $user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $benificiary_list = Customer_accounts::where('user_id', $user->id)->where('benificiary_name', 'like', '%'.request('keyword').'%')->get();
        if(!empty($benificiary_list[0])){
            $response['status'] = 'success';
            $response['message'] = 'Benificiary List Available';
            $response['user_status'] = $user->status;
            $response['description']['benificiary_data'] = $benificiary_list;
        }else{
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'No Benificiary Added';
        }
        return response()->json($response, $this->successStatus);
    }

    public function manageBenificiaryList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if($user->mobile != request('mobile') || $user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $benificiary_list = Customer_accounts::where('created_by', $user->id)->get();
        if(!empty($benificiary_list[0])){
            $response['status'] = 'success';
            $response['message'] = 'Benificiary List Available';
            $response['user_status'] = $user->status;
            $response['description']['benificiary_data'] = $benificiary_list;
        }else{
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'No Benificiary Added';
        }
        return response()->json($response, $this->successStatus);
    }

    public function deleteBenificiary(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users',
            'bid' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        if($user->mobile != request('mobile') || $user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $benificiary_list = Customer_accounts::where('created_by', $user->id)->where('BID', request('bid'))->delete();
        if(!empty($benificiary_list[0])){
            $response['status'] = 'success';
            $response['message'] = 'Benificiary Deleted Successfully';
            $response['user_status'] = $user->status;
            $response['description']['benificiary_data'] = $benificiary_list;
        }else{
            $response['status'] = 'success';
            $response['message'] = 'Benificiary Not Deleted';
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function addBenificiaryPayoutOld(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'account_number' => 'required', 
            'ifsc_code' => 'required',
            'bank_name' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }
        $user = Auth::user();
        if ($user->bank_name < 2) {
            $url ="http://payu.startrecharge.in/Benificiary/AccountReg?token=1BZd0YzLFV110iMVBj4UYj4FqMJUoG&AccountNumber=".$request->account_number."&IFSC=".$request->ifsc_code."&Name=".urlencode($user->first_name.' '.$user->last_name)."&BankName=".urlencode($request->bank_name)."&Email=".$user->email."&Mobile=".$user->mobile."&CustomerID=".$user->customer_id;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);

            $benificiary_data = json_decode($output);

            if ($benificiary_data->Status == 'Success') {
                $benificiary = new User_accounts();
                $benificiary->user_id = $user->id;
                $benificiary->benificiary_name = $benificiary_data->data->AccountHolder;
                $benificiary->account = $benificiary_data->data->AccountNumber;
                $benificiary->ifsc_code = $benificiary_data->data->IFSC;
                $benificiary->bank_name = $benificiary_data->data->BankName;
                $benificiary->mobile = $benificiary_data->data->Mobile;
                $benificiary->email = $benificiary_data->data->Email;
                $benificiary->status = 1;
                $benificiary->BID = $benificiary_data->data->BID;
                if($benificiary->save()){
                    $user->bank_name = $user->bank_name + 1;
                    if ($user->save()) {
                        $response['status'] = 'success';
                        $response['message'] = 'Benificiary Added Successfully!!!';
                    }else{
                        $response['status'] = 'success';
                        $response['message'] = 'Benificiary Added Successfully!!! But Some Error Occured';
                    }
                }else{
                    $response['status'] = 'success';
                    $response['message'] = 'Benificiary Not Added Successfully!!!';
                }
            }
            else{
                $response['status'] = 'error';
                $response['message'] = 'Benificiary Not Added';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You have already registered 2 bank account. Kindly contact with Admin for more query';
        }
        return response()->json($response, $this->successStatus);
    }

    public function addBenificiaryPayout(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'account_number' => 'required', 
            'ifsc_code' => 'required',
            'benificiary_name' => 'required',
            'bank_name' => 'required', 
            'mobile' => 'required',
            'email' => 'email',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }
        $user = Auth::user();
        if ($user->bank_name < 2) {
            $user_accounts = User_accounts::orderBy('created_at', 'desc')->first();
            $benificiary = new User_accounts();
            $benificiary->user_id = $user->id;
            $benificiary->created_by = $user->created_by;
            $benificiary->benificiary_name = request('benificiary_name');
            $benificiary->account = request('account_number');
            $benificiary->ifsc_code = request('ifsc_code');
            $benificiary->bank_name = request('bank_name');
            $benificiary->mobile = request('mobile');
            if (!empty(request('email'))) {
                $benificiary->email = request('email');
            }
            $benificiary->status = 1;
            if (!empty($user_accounts->id)) {
              $benificiary->BID = 'DMSPAY'.($user_accounts->id + 1).date('Y');
            }else{
              $benificiary->BID = 'DMSPAY01'.date('Y');
            }
            if($benificiary->save()){
                $user->bank_name = $user->bank_name + 1;
                if ($user->save()) {
                    $response['status'] = 'success';
                    $response['message'] = 'Benificiary Added Successfully!!!';
                    $response['user_status'] = $user->status;
                }else{
                    $response['status'] = 'success';
                    $response['message'] = 'Benificiary Added Successfully!!! But Some Error Occured';
                    $response['user_status'] = $user->status;
                }
            }else{
                $response['status'] = 'success';
                $response['message'] = 'Benificiary Not Added Successfully!!!';
                $response['user_status'] = $user->status;
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You have already registered 2 bank account. Kindly contact with Admin for more query';
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function transferFund(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'benificiary_id' => 'required', 
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        $transaction_id = rand(100000000000000, 999999999999999);

        $url = "http://payu.startrecharge.in/Transfer/Payment?token=1BZd0YzLFV110iMVBj4UYj4FqMJUoG&CustomerID=".$user->customer_id."&BenificiaryID=".request('benificiary_id')."&Amount=".request('amount')."&TxnID=".$transaction_id."&PaymentType=".request('payment_type');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);           

        $data = json_decode($output);
        
        if($data->Status == 'Success'){
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'Fund Transfer Successfully!!!';
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Fund Transfer Failed!!! Try Again';
        }
        
        return response()->json($response, $this->successStatus);
    }

    public function checkvalidate(Request $request){
        $user = Auth::user();
        if ($user->onboarding == 1) {
            
        }
        $validator = Validator::make($request->all(), [ 
            "errCode" => 'required',
            "fCount" => 'required',
            "fType" => 'required',
            "nmPoints" => 'required',
            "qScore" => 'required',
            "dpId" => 'required',
            "rdsId" => 'required',
            "rdsVer" => 'required',
            "dc" => 'required',
            "mi" => 'required',
            "mc" => 'required',
            "ci" => 'required',
            "sessionKey" => 'required',
            "hmac" => 'required',
            "PidDatatype" => 'required',
            "Piddata" => 'required',
            "aadhar_number" => 'required',
            "bank_id" => 'required',
            "mobile" => 'required',
            "deviceSRNO" => 'required',
            "amount" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "method" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $number = strval(rand(100000000000000, 999999999999999));
        $deviceTransaction = strval(rand(100000000000000, 999999999999999));
        $postData = array(
            "errCode" => $request->errCode,
            "errInfo" => "",
            "fCount" => $request->fCount,
            "fType" => $request->fType,
            "iCount" => "0",
            "iType" => "0",
            "pCount" => "0",
            "pType" => "0",
            "nmPoints" => $request->nmPoints,
            "qScore" => $request->qScore,
            "dpID" => $request->dpId,
            "rdsID" => $request->rdsId,
            "rdsVer" => $request->rdsVer,
            "dc" => $request->dc,
            "mi" => $request->mi,
            "mc" => $request->mc,
            "ci" => $request->ci,
            "sessionKey" => $request->sessionKey,
            "hmac" => $request->hmac,
            "PidDatatype" => $request->PidDatatype,
            "Piddata" => $request->Piddata,
            "aadhaarNumber" => $request->aadhar_number,
            "nationalBankIdentificationNumber" => $request->bank_id,
            "mobileNumber" => $request->mobile,
            "deviceSRNO" => $request->deviceSRNO,
            "transactionType" => "BE",
            "merchantTransactionId" => $number,
            "deviceTransactionId" => $deviceTransaction,
            "merchantUserName" => "8058995714",
            "merchantPin" => "2565",
            "transactionAmount" => $request->amount,
            "balanceAmount" => 0.0,
            "latitude" => $request->latitude,
            "longitude" => $request->longitude
        );


        $data = json_encode($postData);
        //$data = str_replace("/", "", $data);
        // var_dump($data);
        // exit;
        if ($request->method == 'withdrawal') {
            $url = "https://apiworld.co.in/api/AAEPS/AEPSExpressCashWithdrawal?tokenkey=API772473&password=0404&retailrId=8058995714";
        }elseif ($request->method == 'balance_enquiry') {
            $url = "https://apiworld.co.in/api/AAEPS/AEPSExpressBalanceEnquiry?tokenkey=API772473&password=0404";
        }elseif ($request->method == 'aadhar_pay') {
            $url = "https://apiworld.co.in/api/AAEPS/MiniStatement?tokenkey=API772473&password=0404";
        }elseif ($request->method == 'mini_statement') {
            $url = "https://apiworld.co.in/api/AAEPS/MiniStatement?tokenkey=API772473&password=0404";
        }else{
            return false;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
     
     
        //get response
        $output = curl_exec($ch);
        $getData = json_decode(json_decode($output));

        $response['status'] = 'success';
        $response['message'] = 'Transaction Successfully';
        $response['method'] = $request->method;
        $response['description']['aeps_data'] = array('status' => $getData->details->status, 'message' => $getData->details->message, 'detail' => $getData->details->data);

        return response()->json($response, $this->successStatus);
    }

    public function aepsStatusOldd(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $url = "https://apiworld.co.in/AEPS/AEPSValidateDetails?tokenkey=API772473&password=0404&merchantUserName=".$user->mobile."&merchantPin=".$user->aeps_pin;
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);

        if ($data->Status == 'error') {
               $url = "https://apiworld.co.in/AEPS/GenerateOTP?tokenkey=API772473&password=0404&merchantUserName=".$user->mobile."&merchantPin=".$user->aeps_pin."&superMerchantId=2";
             
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);
             
            //Close the cURL handle.
            curl_close($ch);
            $data1 = json_decode($output);
        
            if ($data1->status == 'sucess') {
                $response['status'] = 'success';
                $response['message'] = 'Send OTP To your Registered Mobile Number';
                $response['description']['aeps'] = 'error';
                $response['description']['aeps_data'] = array('status' => $data1->details->status, 'message' => $data1->details->message, 'detail' => $data1->details->data);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Something Went Wrong. Please Try Again!!!';
            }
        }else{
            $response['status'] = 'success';
            $response['message'] = 'AEPS Available';
            $response['description']['aeps'] = 'success';
        }
        return response()->json($response, $this->successStatus);
    }

    public function aepsStatusOld(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        if ($user->aeps_status == null) {
          $kyc1 = "https://damskart.com/public/".$user->pan_pic;
          $kyc2 = "https://damskart.com/public/".$user->aadhar_pic_front;
          $kyc3 = "https://damskart.com/public/".$user->company_logo;
          $kyc4 = "https://damskart.com/public/".$user->profile_pic;
          $postData = array(
              "username" => 'G207281212',
              "apikey" => '1410397768',
              "firstname" => $user->first_name,
              "midname" => '',
              "lastname" => $user->last_name,
              "emailid" => $user->email,
              "txnid" => '2',
              "phone1" => $user->mobile,
              "phone2" => $user->mobile,
              "dob" => $user->dob,
              "state" => $user->state,
              "district" => $user->district,
              "address" => $user->address,
              "block" => $user->district,
              "city" => $user->district,
              "landmark" => $user->district,
              "loc" => $user->district,
              "mohhalla" => $user->district,
              "pan" => $user->pan_number,
              "pincode" => $user->pin_code,
              "shopname" => $user->company_name,
              "shoptype" => 'open',
              "qualification" => 'pass',
              "population" => '   2',
              "locationtype" => 'Rural',
              "kyc1" => $kyc1,
              "kyc2" => $kyc2,
              "kyc3" => $kyc3,
              "kyc4" => $kyc4
          );
          $data = json_encode($postData);
          $url = "https://mobilerechargenow.com/api/v2/aeps/BCRegistration.php";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
          ));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          //get response
          $output = curl_exec($ch);
          $data1 = json_decode($output);
          if ($data1->Status == "SUCCESS") {
               $user['aeps_status'] = $data1->Status;
               $user['GpID'] = $data1->GPID;
               $user->save();
               $response['status'] = 'success';
               $response['message'] = 'Successfully Registered!!!';

          }else{
               $response['status'] = 'error';
               $response['message'] = 'Something Went Wrong. Please Try Again!!!';

          }
        }elseif($user->aeps_status == "SUCCESS" && $user->GPStatus != 'Active') {
            //checkaepsStatus
            $postData = array(
              "username" => 'G207281212',
              "apikey" => '1410397768',
              "GpID" => $user->GpID
            );
            $data = json_encode($postData);
            $url = "https://www.mobilerechargenow.com/api/v2/aeps/BCStatus.php";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            //get response
            $output = curl_exec($ch);
            $data1 = json_decode($output);
            if ($data1->Status == "SUCCESS") {
               $user['GPStatus'] = $data1->GPStatus;
               $user['BcID'] = $data1->BcID;
               $user->save();
               $response['status'] = 'success';
               $response['message'] = 'Your kyc is pending. Please Contact Administrator.';

            }else{
                $response['status'] = 'error';
                $response['message'] = 'Something Went Wrong. Please Try Again!!!';

            }
        }elseif ($user->GPStatus == "Active") {
          // aepsInitiate
            $postData = array(
                "username" => 'G207281212',
                "apikey" => '1410397768',
                "GpID" => $user->GpID
            );
            $data = json_encode($postData);
            $url = "https://mobilerechargenow.com/api/v2/aeps/aepsData.php";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            //get response
            $output = curl_exec($ch);
            $data1 = json_decode($output);

            
            $response['status'] = 'success';
            $response['message'] = 'Successfully';
            $response['description'] = $data1;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Pending Now!';

        }
         return response()->json($response, $this->successStatus);
    }

    public function aepsStatus(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        // if ($user->onboarding == 0) {
        //     $check_img = "https://damskart.com/public/".$user->passbook_image;
        //     $shop_img = "https://damskart.com/public/".$user->company_logo;
        //     $ekyc_img = "https://damskart.com/public/".$user->aadhar_pic_front;
            
        //     $cancellationCheckImages = base64_encode($check_img);
        //     $shopAndPanImage = base64_encode($shop_img); 
        //     $ekycDocuments = base64_encode($ekyc_img); 
        //     $name = $user->first_name.' '.$user->last_name;       
            
        //     $data = array("username"=>"Damskartd",
        //                   "password"=>md5("1234d"),
        //                   "supermerchantId"=>968,
        //                   "latitude"=>"26.912434",
        //                   "longitude"=>"75.787270",
        //                   "merchants"=>array(array(
        //                       "merchantLoginId"=> $user->mobile,
        //                       "merchantLoginPin"=>$user->aeps_pin,
        //                       "merchantName"=> $name,
        //                       "merchantPhoneNumber"=> $user->mobile,
        //                       "companyLegalName"=> $user->company_name,
        //                       "companyMarketingName"=> $user->company_name,
        //                       "merchantBranch"=>$user->district,
        //                       "emailId"=> $user->email,
        //                       "merchantPinCode"=> $user->pin_code,
        //                       "tan"=>"1234",
        //                       "merchantCityName"=> $user->state,
        //                       "merchantDistrictName"=> $user->district,
        //                       "cancellationCheckImages"=> $cancellationCheckImages,
        //                       "shopAndPanImage"=> $shopAndPanImage,
        //                       "ekycDocuments"=> $ekycDocuments,
        //                       "merchantAddress"=> array(
        //                         "merchantAddress"=> $user->address,
        //                         "merchantState"=>$user->state_id
        //                       ),
        //                       "kyc"=>array(
        //                         "userPan"=>$user->pan_number,
        //                         "aadhaarNumber"=>$user->aadhar_number,
        //                         "gstInNumber"=>"BVTPD810",
        //                         "companyOrShopPan"=>$user->company_pan
        //                       ),
        //                       "settlement"=>array(
        //                         "companyBankAccountNumber"=>"0029101144",
        //                         "bankIfscCode"=>"BKID90056",
        //                         "companyBankName"=>"bank of india",
        //                         "bankBranchName"=>"Jaipur",
        //                         "bankAccountName"=>$name
        //                       )
        //                     ))
        //                   );

        //     // var_dump($data);
        //     // exit;
        //     $post = json_decode(json_encode($data)); // Request String
        //     $method = "POST"; // Method
        //     $dc = "352801082418919";
        //     /* Rendom kye for encryption */
        //     $key = '';
        //     $mt_rand = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        //     foreach ($mt_rand as $chr) {
        //         $key .= chr($chr);
        //     }

        //     $iv =   '06f2f04cc530364f';
        //     $ciphertext_raw = openssl_encrypt(json_encode($post), 'AES-128-CBC', $key, $options=OPENSSL_RAW_DATA, $iv); // generate cipher text
        //     $request = base64_encode($ciphertext_raw);// Encodes data with MIME base64
           
        //     // $fp = fopen("fingpay_public_production.cer","r");
        //     $fp = fopen(public_path()."/keys/fingpay_public_production.txt","r");
        //     $pub_key_string = fread($fp,8192);
        //     fclose($fp);
        //     openssl_public_encrypt($key,$crypttext,$pub_key_string);

        //     $header = [
        //     'Content-Type: text/xml',    
        //     'trnTimestamp:'.date('d/m/Y H:i:s'),
        //     'hash:'.base64_encode(hash("sha256",json_encode($post), True)),
        //     'deviceIMEI:'.$dc,
        //     'eskey:'.base64_encode($crypttext)
        //     ];
           
        //     $url = "https://fingpayap.tapits.in/fpaepsweb/api/onboarding/merchant/creation/php/m1";
          
        //     $curl = curl_init();
        //     curl_setopt_array($curl, array(
        //     CURLOPT_URL => $url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_SSL_VERIFYPEER => true,
        //     CURLOPT_SSL_VERIFYHOST => 2,
        //     // CURLOPT_TIMEOUT => $this->timeout,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => $method,
        //     CURLOPT_POSTFIELDS => $request,
        //     CURLOPT_HTTPHEADER => $header
        //     ));
        //     $result = curl_exec($curl);
        //     // print_r($result);

        //     //---------- Create Log File Start----------
        //     $date = date('Y-m-d H:i:s');
        //     $cryptedText = file_get_contents('php://input');
        //     $file = public_path().'/logs/ICICI-AEPS-Onboarding-Log.txt';

        //     $log = "\n\n".'Date - '.$date."================================================================ \n";
        //     $log .= 'Request Encode Data- '.$result."\n\n";
        //     file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
        //     // ---------- Create Log File End------------
            
        //     // $array = json_decode(json_encode($result), true);
        //     $array = json_decode($result, true);
        //   if ($array['status'] == true) {
        //       $user->onboarding = 1;
        //       $user->save();
        //        $onboarding = $user->onboarding;
        //        $response['status'] = 'success';
        //        $response['message'] = 'Successfully Registered!!!';
        //        $response['description']['onBoarding'] = $onboarding;

        //   }else{
        //        $onboarding = $user->onboarding;
        //        $response['status'] = 'error';
        //        $response['message'] = $array['message'];
        //        $response['user_status'] = $user->status;
        //        $response['description']['onBoarding'] = $onboarding;
        //   }
        // }else{
        //     $onboarding = $user->onboarding;
        //     $response['status'] = 'success';
        //     $response['message'] = 'Onboarding Successful!!!';
        //     $response['user_status'] = $user->status;
        //     $response['description']['onBoarding'] = $onboarding;
        // }

        if($user->aeps_ekyc_status == 1){
            $response['status'] = 'success';
            $response['message'] = 'E-KYC Done Successful!!!';
            $response['user_status'] = $user->status;
            $response['description']['aeps_ekyc_status'] = $user->aeps_ekyc_status;

        }else{
            $response['status'] = 'error';
            $response['message'] = 'E-KYC Not Completed!!!';
            $response['user_status'] = $user->status;
            $response['description']['aeps_ekyc_status'] = $user->aeps_ekyc_status;
        }
        
         return response()->json($response, $this->successStatus);
    }
    


    public function aepsOtpValidate(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'otp' => 'required', 
            'merchantTranId' => 'required', 
            'fingpayTransactionId' => 'required', 
            'tefPkId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $url = "https://apiworld.co.in/AEPS/ValidateOTP?tokenkey=API772473&password=0404&merchantUserName=".$user->mobile."&merchantPin=".$user->aeps_pin."&merchantTranId=".$request->merchantTranId."&fingpayTransactionId=".$request->fingpayTransactionId."&otp=".$request->otp."&tefPkId=".$request->tefPkId."&superMerchantId=2";
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);

        $response['status'] = 'success';
        $response['message'] = 'Successfully';
        $response['user_status'] = $user->status;
        $response['description']['aeps_data'] = array('status' => $data->details->status, 'message' => $data->details->message, 'detail' => $data->details->data);
        return response()->json($response, $this->successStatus);
    }

    public function aepsAadharValidate(Request $request){
        $user= Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "errCode" => 'required',
            "fCount" => 'required',
            "fType" => 'required',
            "nmPoints" => 'required',
            "qScore" => 'required',
            "dpId" => 'required',
            "rdsId" => 'required',
            "rdsVer" => 'required',
            "dc" => 'required',
            "mi" => 'required',
            "mc" => 'required',
            "ci" => 'required',
            "sessionKey" => 'required',
            "hmac" => 'required',
            "PidDatatype" => 'required',
            "Piddata" => 'required',
            "deviceSRNO" => 'required',
            "otp" => 'required',
            "tefPkId" => 'required',
            "fingpayTransactionId" => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $postData = array(
            "errCode" => $request->errCode,
            "errInfo" => "0",
            "fCount" => $request->fCount,
            "fType" => $request->fType,
            "iCount" => "0",
            "iType" => "0",
            "pCount" => "0",
            "pType" => "0",
            "nmPoints" => $request->nmPoints,
            "qScore" => $request->qScore,
            "dpID" => $request->dpId,
            "rdsID" => $request->rdsId,
            "rdsVer" => $request->rdsVer,
            "dc" => $request->dc,
            "mi" => $request->mi,
            "mc" => $request->mc,
            "ci" => $request->ci,
            "sessionKey" => $request->sessionKey,
            "hmac" => $request->hmac,
            "PidDatatype" => $request->PidDatatype,
            "Piddata" => $request->Piddata,
            "aadhaarNumber" => $user->aadhar_number,
            "deviceSRNO" => $request->deviceSRNO,
            "merchantUserName" => $user->mobile,
            "merchantPin" => $user->aeps_pin,
            "latitude" => 26.00,
            "longitude" => 75.00,
            "otp" => $request->otp,
            "tefPkId" => $request->tefPkId,
            "fingpayTransactionId" => $request->fingpayTransactionId
        );


        $data = json_encode($postData);
        //$data = str_replace("/", "", $data);
        // var_dump($data);
        // exit;
        $url = "https://apiworld.co.in/api/AAEPS/AadharVerification?tokenkey=API772473&password=0404";
        

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
     
     
        //get response
        $output = curl_exec($ch);
        $getdata = json_decode(json_decode($output));

        $response['status'] = 'success';
        $response['message'] = 'Successfully';
        $response['description']['aeps'] = $getdata->status;
        $response['user_status'] = $user->status;
        $response['description']['aeps_data'] = array('status' => $getdata->status, 'message' => $getdata->message, 'detail' => $getdata->details->data);

        return response()->json($response, $this->successStatus);
    }

    public function bbpsCategory(){
        $url = "https://startrecharge.in/APIBBPS/bbps_api.aspx?type=categories&apiuserid=API772473&tokenkey=1BZd0YzLFV110iMVBj4UYj4FqMJUoG";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);

        // foreach ($data as $key => $value) {
        //     $input['category_name'] = $value->category_name;
        //     $bbps_categories = Bbps_categories::create($input);
        // }

        $response['status'] = 'success';
        $response['message'] = 'BBPS Category Available!';
        $response['description']['bbps_category'] = $data;
        return response()->json($response, $this->successStatus);
    }

    public function bbpsList(Request $request){
         $bbps_list = Bbps_electricities::all();
        if (!empty($bbps_list)) {
            $response['status'] = 'success';
            $response['message'] = 'Bbps list available.';
            $response['description']['bbps_list'] = $bbps_list;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Bbps list not available!';
        }
        return response()->json($response, $this->successStatus); 
    }

    public function bbpsFetchBill(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "kNumber" => 'required',
            "operator" => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $transactionID = "DMS".rand(100000,999999);

        $url = "https://mobilerechargenow.com/api/bill-fetch.php?username=G207281212&apikey=1410397768&format=json&no=".request('kNumber')."&operator=".request('operator')."&txnid=".$transactionID;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);

        if ($data->status == 'FAILURE') {
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Payment received for the billing period - no bill due';
        }else{
            $response['status'] = 'success';
            $response['message'] = 'Successfully!';
            $response['user_status'] = $user->status;
            $response['description'] = $data;
        }
        return response()->json($response, $this->successStatus);
    }

    public function bbpsPayment(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "kNumber" => 'required',
            "amount" => 'required',
            "operator" => 'required',
            "mobile" => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

       
        $transactionID = "DMS".rand(100000,999999);
        $dmstransID = "DMS".rand(1000000000,9999999999);

        $unique_id = "DMS".date("Ymd").time();
      
        
        if (request('amount') < $user->ewallet) {
            $url = "https://mobilerechargenow.com/billpay/payment.php?username=G207281212&apikey=1410397768&format=json&type=1&no=".request('kNumber')."&amount=".request('amount')."&operator=".request('operator')."&txnid=".$transactionID;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);
             
            //Close the cURL handle.
            curl_close($ch);
            $data = json_decode($output);
              if (!empty($data)) {
                $electricity =  new Electricity_transactions();
                $electricity['user_id'] = request('user_id');
                $electricity['operator_name'] = $data->operatorName;
                $electricity['transaction_id'] = $dmstransID;
                $electricity['operator_code'] =request('operator');
                $electricity['kNumber'] = $data->mobile;
                $electricity['mobile'] = request('mobile');
                $electricity['orderId'] = $data->orderId;
                $electricity['status'] = $data->status;
                $electricity['amount'] = $data->amount;
                $electricity['TransId'] = $data->TransId;
                $electricity['service'] = $data->service;
                $electricity['bal'] = $data->bal;
                $electricity['resText'] = $data->resText;
                $electricity['creditUsed'] = $data->creditUsed;
                $electricity['billName'] = $data->billName;
                $electricity['billAmount'] = $data->billAmount;
                $electricity['txnid'] = $transactionID;
               
                $electricity->save();
                    if ($data->status == "SUCCESS") {
                     $commissionamount = Electricity_commissions::where('package_id',$user->package_id)->where('elec_operator_code',request('operator'))->first();

                      $data1 = new User_transactions();
                      $data1->user_id = $user->id;
                      $data1->created_by = $user->created_by;
                      $data1->transaction_id = $unique_id;
                      $data1->label = 'ewallet';
                      $data1->status = $data->status;
                      $data1->date = date('Y-m-d H:i:s');
                      $data1->amount = request('amount');
                      $data1->current_balance = $user->ewallet - request('amount');
                      $data1->narration =  "Bill Payment - Electricity Success. Info: ".$data1->transaction_id;
                      $data1->type = "debit";
                        $user->ewallet = $data1->current_balance;
                        $user->save();

                      $datacr = new User_transactions();
                      $datacr->user_id = $user->id;
                      $datacr->created_by = $user->created_by;
                      $datacr->transaction_id = $unique_id;
                      $datacr->label = 'ewallet';
                      $datacr->status = $data->status;
                      $datacr->date = date('Y-m-d H:i:s');
                      $datacr->type = "credit";
                      $datacr->narration = "Bill Payment - Electricity Comm. Info: ".$datacr->transaction_id;

                      if ($commissionamount->commission_type == "flat") {
                        $comamount = $commissionamount->commission;
                          $datacr->amount = $commissionamount->commission;
                          $datacr->current_balance = $user->ewallet + $commissionamount->commission;
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $data1->save();
                          $datacr->save();
                      }elseif ($commissionamount->commission_type == "percentage") {
                        $comamount = (request('amount')*$commissionamount->commission)/100;
                          $datacr->amount = (request('amount')*$commissionamount->commission)/100;
                          $datacr->current_balance = $user->ewallet + ((request('amount')*$commissionamount->commission)/100);
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $data1->save(); 
                          $datacr->save();
                      }

                    $electricitycom = Electricity_transactions::where('orderId',$electricity->orderId)->first();
                    $electricitycom->commission = $comamount;
                    $electricitycom->current_balance = $user->ewallet;
                    $electricitycom->save();

                    $response['status'] = 'success';
                    $response['message'] = 'Successfully!!!';
                    $response['user_status'] = $user->status;
                    $response['description'] = $data;
                   
                 }elseif($data->status == ""){
                    $electricitycom = Electricity_transactions::where('orderId',$electricity->orderId)->first();
                    $electricitycom->current_balance = $user->ewallet;
                    $electricitycom->save();
                    
                    $response['status'] = 'error';
                    $response['message'] = $data->message;
                    $response['user_status'] = $user->status;
                    // $response['description'] =  $data;
                 }else{
                    $electricitycom = Electricity_transactions::where('orderId',$electricity->orderId)->first();
                    $electricitycom->current_balance = $user->ewallet;
                    $electricitycom->save();
                    
                    $response['status'] = 'error';
                    $response['message'] = 'Failed';
                    $response['user_status'] = $user->status;
                    $response['description'] =  $data;
                 }
              }else{

                $electricitycom = Electricity_transactions::where('orderId',$electricity->orderId)->first();
                $electricitycom->current_balance = $user->ewallet;
                $electricitycom->save();
                
                $response['status'] = 'error';
                $response['message'] = 'FAILURE';
                $response['user_status'] = $user->status;
                $response['description'] =  $output;
              }
        }else{
             $response['status'] = 'error';
             $response['message'] = 'You do not have enough amount in your wallet for payment!';
             $response['user_status'] = $user->status;
             $response['description'] = array('amount'=>request('amount'), 'knumber'=>request('kNumber'),'orderId'=>$transactionID);
        }

        
        return response()->json($response, $this->successStatus);
    }

    public function rechargeCircle(){
        $circle = Circle::all();
        if (!empty($circle)) {
            $response['status'] = 'success';
            $response['message'] = 'Circle list available.';
            $response['description']['circle_list'] = $circle;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Circle list not available!';
        }
        return response()->json($response, $this->successStatus); 
    }

    public function rechargeOperator(){
        $operators = Operators::all();
        if (!empty($operators)) {
            $response['status'] = 'success';
            $response['message'] = 'Operators list available.';
            $response['description']['operator_list'] = $operators;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Operators list not available!';
        }
        return response()->json($response, $this->successStatus); 
    }

    public function operatorFinder(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "mobile" => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $url = "https://mobilerechargenow.com/api/mobileinfo.php?username=G207281212&apikey=1410397768&format=json&mobile=".request('mobile');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);

        $response['status'] = 'success';
        $response['message'] = 'Successfully!!!';
        $response['description'] = $data;
        return response()->json($response, $this->successStatus);
    }

    
    public function operatorPlan(Request $request){
         $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "operator" => 'required',
            "circle" => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $url = "https://mobilerechargenow.com/recharge-plan.php?username=G207281212&apikey=1410397768&operator=".request('operator')."&circle=".request('circle')."&type=OTR";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
         
        //Close the cURL handle.
        curl_close($ch);
        $data = json_decode($output);
          
        $response['status'] = 'success';
        $response['message'] = 'Successfully!!!';
        $response['user_status'] = $user->status;
        $response['description']['recharge_plan'] = $data;
        
        
        return response()->json($response, $this->successStatus);
    }

    public function rechargePayment(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "mobile" => 'required|numeric',
            "operator" => 'required',
            "circle" => 'required',
            "amount" => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $transactionID = "DMS".rand(100000,999999);
        $dmstransID = "DMS".rand(1000000000,9999999999);

        $unique_id = "DMS".date("Ymd").time();

        if (request('amount') < $user->ewallet){

            $url = "https://mobilerechargenow.com/recharge.php?username=G207281212&apikey=1410397768&type=1&format=json&no=".request('mobile')."&amount=".request('amount')."&operator=".request('operator')."&circle=".request('circle')."&user=".$transactionID;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);
             
            //Close the cURL handle.    
            curl_close($ch);
            $data = json_decode($output);
            $recharge =  new Mobile_recharges();
            $recharge['user_id'] = request('user_id');
            $recharge['transaction_id'] = $dmstransID;
            $recharge['orderId'] = $data->orderId;
            $recharge['operator_code'] = request('operator');
            $recharge['status'] = $data->status;
            $recharge['mobile'] = $data->mobile;
            $recharge['amount'] = $data->amount;
            $recharge['transId'] = $data->TransId;
            $recharge['operatorId'] = $data->operatorId;
            $recharge['service'] = $data->service;
            $recharge['bal'] = $data->bal;
            $recharge['message'] = $data->resText;
            $recharge['billAmount'] = $data->billAmount;
            $recharge->save();
               
            if ($recharge->status == 'SUCCESS') {
                 $commissionamount = Mobile_commissions::where('package_id',$user->package_id)->where('operator_code',request('operator'))->first();

                  $data1 = new User_transactions();
                  $data1->user_id = request('user_id');
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $recharge->status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = request('amount');
                  $data1->current_balance = $user->ewallet - request('amount');
                  $data1->narration = "Recharge Success for ".request('mobile').", Info: ".$data1->transaction_id;
                  $data1->type = "debit";
                    $user->ewallet = $data1->current_balance;
                    $user->save();

                  $datacr = new User_transactions();
                  $datacr->user_id = request('user_id');
                  $datacr->created_by = $user->created_by;
                  $datacr->transaction_id = $unique_id;
                  $datacr->label = 'ewallet';
                  $datacr->status = $recharge->status;
                  $datacr->date = date('Y-m-d H:i:s');
                  $datacr->type = "credit";
                  $datacr->narration = "Recharge Comm. for ".request('mobile').", Info: ".$datacr->transaction_id;

                    if ($commissionamount->commission_type == "flat") {
                       $comamount = $commissionamount->commission;
                        $datacr->amount = $commissionamount->commission;
                        $datacr->current_balance = $user->ewallet + $commissionamount->commission;
                          $user->ewallet = $datacr->current_balance;
                        $data1->save();
                        $datacr->save();
                          $user->save();
                    }elseif ($commissionamount->commission_type == "percentage") {
                       $comamount = (request('amount')*$commissionamount->commission)/100;
                        $datacr->amount = (request('amount')*$commissionamount->commission)/100;
                        $datacr->current_balance = $user->ewallet + ((request('amount')*$commissionamount->commission)/100);
                          $user->ewallet = $datacr->current_balance;
                        $data1->save(); 
                        $datacr->save();
                          $user->save();
                    }
                      $mobilecom = Mobile_recharges::where('orderId',$recharge->orderId)->first();
                      $mobilecom->commission = $comamount;
                      $mobilecom->current_balance = $user->ewallet;
                      $mobilecom->save();

                    $response['status'] = 'success';
                    $response['message'] = 'Successfully!!!';
                    $response['user_status'] = $user->status;
                    $response['description'] = $data;
              }elseif ($recharge->status == 'PENDING') {
                  $data1 = new User_transactions();
                  $data1->user_id = request('user_id');
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $recharge->status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = request('amount');
                  $data1->current_balance = $user->ewallet - request('amount');
                  $data1->narration = "Recharge Pending for ".request('mobile').", Info: ".$data1->transaction_id;
                  $data1->type = "debit";
                    $user->ewallet = $data1->current_balance;
                    $data1->save();
                    $user->save();
                  
                  $mobilecom = Mobile_recharges::where('orderId',$recharge->orderId)->first();
                  $mobilecom->current_balance = $user->ewallet;
                  $mobilecom->save();
                   
                    $response['status'] = 'success';
                    $response['message'] = 'Successfully!!!';
                    $response['user_status'] = $user->status;
                    $response['description'] = $data;
              }else{
                  $mobilecom = Mobile_recharges::where('orderId',$recharge->orderId)->first();
                  $mobilecom->current_balance = $user->ewallet;
                  $mobilecom->save();
                $response['status'] = 'error';
                $response['message'] = 'FAILURE';
                $response['user_status'] = $user->status;
                $response['description'] =  $data;
              }
        }else{
            $data = array('transaction_id'=>$dmstransID, 'mobile'=>request('mobile'),'amount'=>request('amount'),'status'=> "Failed",'operatorId'=> request('operator'),'resText'=> "You do not have enough amount in your wallet for payment!" );
            $response['status'] = 'error';
            $response['message'] = 'You do not have enough amount in your wallet for payment!';
            $response['user_status'] = $user->status;
            $response['description'] = $data;
        }
        return response()->json($response, $this->successStatus);
    }

    public function datacardPayment(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            "mobile" => 'required|numeric',
            "operator" => 'required',
            "circle" => 'required',
            "amount" => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        
       

        $transactionID = "DMS".rand(100000,999999);
        if (request('amount') < $user->ewallet){

            $url = "https://mobilerechargenow.com/recharge.php?username=G207281212&apikey=1410397768&type=3&format=json&no=".request('mobile')."&amount=".request('amount')."&operator=".request('operator')."&circle=".request('circle')."&user=".$transactionID;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);
             
            //Close the cURL handle.    
            curl_close($ch);
            $data = json_decode($output);
            $recharge =  new Mobile_recharges();
            $recharge['user_id'] = request('user_id');
            $recharge['orderId'] = $data->orderId;
            $recharge['status'] = $data->status;
            $recharge['mobile'] = $data->mobile;
            $recharge['amount'] = $data->amount;
            $recharge['transId'] = $data->TransId;
            $recharge['operatorId'] = $data->operatorId;
            $recharge['service'] = $data->service;
            $recharge['bal'] = $data->bal;
            $recharge['message'] = $data->resText;
            $recharge['billAmount'] = $data->billAmount;
            $recharge->save();

            $unique_id = "DMS".date("Ymd").time();

            if ($recharge->status == 'PENDING') {
                $url = "https://mobilerechargenow.com/api-status.php?username=G207281212&apikey=1410397768&format=json&txnid=".$recharge->orderId;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $output = curl_exec($ch);
                 
                //Close the cURL handle.    
                curl_close($ch);
                $data = json_decode($output);
                $recharge['status'] = $data->status;
                $recharge['reqTime'] = $data->reqTime;
                $recharge['message'] = $data->resText;
                $recharge->save();
                
                // $response['status'] = 'success';
                // $response['message'] = 'Status Update successfully!!!';

                    if ($data->status == "SUCCESS") {
                         $commissionamount = User::where('users.id',request('user_id'))->leftJoin('mobile_commissions','mobile_commissions.package_id','users.package_id')->where('mobile_commissions.operator_code','=',request('operator'))->first(['mobile_commissions.mobile_operator_id','mobile_commissions.operator_code','mobile_commissions.commission','mobile_commissions.commission_type','users.*']);
           
                          $data1 = new User_transactions();
                          $data1->user_id = $user->id;
                          $data1->created_by = $user->created_by;
                          $data1->transaction_id = $unique_id;
                          $data1->label = 'ewallet';
                          $data1->status = $data->status;
                          $data1->date = date('Y-m-d H:i:s');
                          $data1->amount = request('amount');
                          $data1->current_balance = $user->ewallet - request('amount');
                          $data1->narration = $data->resText;
                          $data1->type = "debit";
                            $user->ewallet = $data1->current_balance;
                            $user->save();

                          $datacr = new User_transactions();
                          $datacr->user_id = $user->id;
                          $datacr->created_by = $user->created_by;
                          $datacr->transaction_id = $unique_id;
                          $datacr->label = 'ewallet';
                          $datacr->status = $data->status;
                          $datacr->date = date('Y-m-d H:i:s');
                          $datacr->type = "credit";
                          $datacr->narration = $data->resText;

                      if ($commissionamount->commission_type == "flat") {
                          $datacr->amount = $commissionamount->commission;
                          $datacr->current_balance = $user->ewallet + $commissionamount->commission;
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $data1->save();
                          $datacr->save();
                      }elseif ($commissionamount->commission_type == "percentage") {
                          $datacr->amount = (request('amount')*$commissionamount->commission)/100;
                          $datacr->current_balance = $user->ewallet + ((request('amount')*$commissionamount->commission)/100);
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $data1->save(); 
                          $datacr->save();
                      }
                   
                 }
                $response['status'] = 'success';
                $response['message'] = 'Successfully!!!';
                $response['user_status'] = $user->status;
                $response['description'] = $data;
            }

              
            $response['status'] = 'error';
            $response['message'] = 'FAILURE';
            $response['user_status'] = $user->status;
            $response['description'] =  $data;
            
        }else{
            $response['status'] = 'error';
            $response['message'] = 'You do not have enough amount in your wallet for payment!';
            $response['user_status'] = $user->status;
        }
       
        return response()->json($response, $this->successStatus);
    }

    public function yesbankAeps(Request $request){
        $yesbank_access = null;
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        } 
       
        if ($user->outlet_id == NULL && $user->kyc_status == NULL) {
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Not Registered With Bank! You can registser with bank using web portal only.';
            return response()->json($response, $this->successStatus);
        }
        if ($user->kyc_status == 'SUCCESS' && $user->outlet_status == 'Approved') {
                $postData = array(
                    'api_access_key' => 'c838c08f2245376823272c2b227801a6',
                    'outlet_id'=> Auth::user()->outlet_id,
                );
                $url = "https://netpaisa.com/nps/api/aeps/yesbank_outlet_access_key.php";
                 
                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                ));
             
             
                //Ignore SSL certificate verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
             
             
                //get response
                $output = curl_exec($ch);
                $data = json_decode($output);
            if($data->ERR_STATE == 0){
                $response['status'] = 'success';
                $response['message'] = 'Login to Yesbank';
                $response['user_status'] = $user->status;
                $response['description']['yesbank_data'] = 'http://45.114.245.112:7081/AEPS/login?token='.$data->ACCESSKEY;
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Something Went Wrong!!! Please Try Again...';
            }      
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'KYC Not Approved Yet';
        }
        return response()->json($response, $this->successStatus);
       
    }

    public function recharge(Request $request){
        $postData = array(
            'SecurityKey' => 'S305078',
            'EmailId'=> 'damskartindia@gmail.com',
            'Password'=> '123321',
            'APIChkSum'=> md5('damskartindia@gmail.com123321Dm$k01'),
        );

        // var_dump($postData);
        // exit;
        $url = "https://spi.justrechargeit.com/JRICorporateLogin.svc/securelogin/";
         
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
     
     
        //get response
        $output = curl_exec($ch);
        var_dump($output);
        exit;
        $data = json_decode($output);
    }

    public function paytmChecksum(Request $request){
        $validator = Validator::make($request->all(), [ 
            'order_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }
        $paytmParams = array();
        $orderid = "DMS".rand(100000000,999999999);
        // $orderid = request('order_id');
        /* add parameters in Array */
        $paytmParams["MID"] = "DAMSKA23206583524542"; //Damska35887526183548
        $paytmParams["ORDERID"] = $orderid;

        /**
        * Generate checksum by parameters we have
        */
        $paytmChecksum = PaytmChecksum::generateSignature($paytmParams, 'm3Ro0m3QX4w9F_c@'); //bHLZV%8tXEVScLj6
        $isVerifySignature = PaytmChecksum::verifySignature($paytmParams, 'm3Ro0m3QX4w9F_c@', $paytmChecksum);
        // echo sprintf("generateSignature Returns: %s\n", $paytmChecksum);
        if($isVerifySignature) {
            $response['status'] = 'success';
            $response['message'] = 'Checksum Matched';
            $response['order_id'] = $orderid;
            $response['paytmChecksum'] = $paytmChecksum;
            $response['isVerifySignature'] = $isVerifySignature;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Checksum Mismatched';
        }
        return response()->json($response, $this->successStatus);
    }  

    function getpaytmtxntoken(Request $request){
        $validator = Validator::make($request->all(), [ 
            'amount' => 'required|numeric', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        $generatedOrderID = "PYTM_BLINK_".time();
        $amount = request('amount');
        $paytmParams = array();
        $paytmParams["body"] = array(
            "requestType"   => "Payment",
            "mid"           => "DAMSKA23206583524542",
            "websiteName"   => "WEBSTAGING",
            "orderId"       => $generatedOrderID,
            //"paytmSsoToken" =>  "46e42738-71c8-4e26-95a3-f27ddf708200",
            "callbackUrl"   => "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=".$generatedOrderID,
            "txnAmount"     => array(
                "value"     => request('amount'),
                "currency"  => "INR",
            ),
            "userInfo"      => array(
                "custId"    => "CUST_001",
            ),
        );
        // var_dump($paytmParams);
        // exit();
        //print_r($paytmParams);
        /*
        * Generate checksum by parameters we have in body
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
        $checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "m3Ro0m3QX4w9F_c@");
        $paytmParams["head"] = array(
            "signature"   => $checksum
        );
        $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
        /* for Staging */
        $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=DAMSKA23206583524542&orderId=".$generatedOrderID;
        /* for Production */
        // $url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=DAMSKA23206583524542&orderId=".$generatedOrderID;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
        $data = curl_exec($ch);
        $output = json_decode($data);
       
        $response['status'] = 'success';
        $response['order_id'] = $generatedOrderID;
        $response['txnToken'] = $output->body->txnToken;
        return response()->json($response, $this->successStatus);
    
    }

    public function fundTransfer(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'otp' => 'required', 
            // 'order_id' => 'required', 
            'user_id' => 'required', 
            'benificiary_id' => 'required', 
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'general_error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }



        $customer = Customer_accounts::where('BID',request('benificiary_id'))->first();
    
        // $usercommission = User::where('users.id',request('user_id'))->leftJoin('aeps_commissions','aeps_commissions.package_id','users.package_id')->where('aeps_commissions.services','fund_transfer')->first(['aeps_commissions.services','aeps_commissions.commission','users.*']);
        $selected_pay_mode = request('payment_type');
        if (request('payment_type') == "IMPS") {
          $service_type = "6";
        }elseif (request('payment_type') == "NEFT") {
          $service_type = "7";
        }elseif (request('payment_type') == "RTGS") {
          $service_type = "8";
        }
        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id',$service_type)->first();
        $getcommission = json_decode($usercommission->commission);

        if ($user->otp == request('otp')) {
            if (request('amount') < $user->ewallet) {
                foreach ($getcommission as $key => $value) {
                    if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                        if ($value->commission_type == "flat") {
                            $surcharge = $value->commission; 
                            $percentagecom = request('amount') + $value->commission; 
                            if ((request('amount') + $value->commission) <= $user->ewallet) {
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }elseif ($value->commission_type == "percentage") {
                            $surcharge = (request('amount')*$value->commission)/100;
                            $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            if($percentagecom <= $user->ewallet){
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }

                    }else{
                         $surcharge = 0;
                        if (request('amount') <= $user->ewallet) {
                            $response['status'] = 'success';
                        }else{
                            $response['status'] = 'amount_error';
                            $response['user_status'] = $user->status;
                            $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                            return response()->json($response, $this->successStatus);
                        }
                    } 
                }

                //CIB
                  $apiname = 'Transaction';
                  $guid = 'GUID'.date('YmdHis');
                  $amount = request('amount');

                  $bankname = $customer->bank_name;
                  if ($bankname == "ICICI Bank") {
                    $ifsccode = "ICIC0000011";
                      $payment_mode = "TPA";
                  }else{
                    $ifsccode = $customer->ifsc_code;
                    if ($request->payment_type == "IMPS") {
                      $payment_mode = "IFS";
                    }else{
                      $payment_mode = "RGS";
                    }
                  }
                  // if ($request->payment_type == "IMPS") {
                  //   $payment_mode = "IFS";
                  // }else{
                  //   $payment_mode = "RGS";
                  // }

                  $unique_id = "DMS".date("Ymd").time();

                  $params = [
                      "CORPID"    =>  "574327555",
                      "USERID"    =>  "VEERENDR",
                      "AGGRNAME"  =>  "DAMSKART",
                      "AGGRID"    =>  "OTOE0077",
                      "URN"       =>  "SR194499369",
                      "DEBITACC"  =>  "678405600727",  
                      "CREDITACC" =>  $customer->account,
                      "IFSC"      =>  $ifsccode,      
                      "AMOUNT"    =>  $amount,
                      "CURRENCY"  =>  "INR",
                      "TXNTYPE"   =>  $payment_mode,
                      "PAYEENAME" =>  $customer->benificiary_name,
                      "REMARKS"   =>  "prod",
                      "UNIQUEID"  =>  $unique_id
                  ];
                  $source = json_encode($params);
                  $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                  $pub_key_string=fread($fp,8192);
                  fclose($fp);
                  openssl_get_publickey($pub_key_string);
                  openssl_public_encrypt($source,$crypttext,$pub_key_string);
                  $request = json_encode(base64_encode($crypttext));
                  $header = [
                      'apikey:1032da18afb74ccf8247f971aaae24ac',
                      'Content-type:text/plain'
                  ];
                  $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/Transaction';
                  $file = public_path().'/logs/'.$apiname.'.txt';
                  $log = "\n\n".'GUID - '.$guid."================================================================\n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'SERVER IP - '. $_SERVER['SERVER_ADDR']."\n\n";
                  $log .= 'HEADER - '.json_encode($header)."\n\n";
                  $log .= 'REQUEST - '.json_encode($params)."\n\n";
                  $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                  // $log .= 'IP'.$response;
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                      CURLOPT_PORT => "443",
                      CURLOPT_URL => $httpUrl,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 60,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => $request,
                      CURLOPT_HTTPHEADER => $header
                  ));
                  $responsedata = curl_exec($curl);
                  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                  $err = curl_error($curl);
                  curl_close($curl);

                  $fp= fopen(public_path()."/keys/damskart.final.key","r"); //private.pem
                  $priv_key=fread($fp,8192);
                  fclose($fp);
                  $res = openssl_get_privatekey($priv_key, "");

                  openssl_private_decrypt(base64_decode($responsedata), $newsource, $res);
                  $log = "\n\n".'GUID - '.$guid."================================================================ \n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'RESPONSE - '.json_encode($responsedata)."\n\n";
                  $log .= 'REQUEST DECRYPTED - '.$newsource."\n\n";
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $data = json_decode($newsource, TRUE);

                  if ($data['STATUS'] == "SUCCESS") {
                    $a = floor(request('amount') / 5000);
                    $b = request('amount') % 5000;
                    for ($i=1; $i <= $a+1 ; $i++) { 
                       
                       $fund_transactions =  new Fund_transactions();
                       $fund_transactions['user_id'] = $customer->user_id;
                       $fund_transactions['created_by'] = $customer->created_by;
                       $fund_transactions['account'] = $customer->account;
                       $fund_transactions['reference_no'] = NULL;
                       $fund_transactions['dms_txn_id'] =  $unique_id;
                       $fund_transactions['ifsc_code'] = $ifsccode;
                       $fund_transactions['transfer_type'] = "Fund Transfer";
                       $fund_transactions['pay_type'] = "cib";
                       $fund_transactions['purpose'] = "fundtransfer";
                       $fund_transactions['BID'] = $customer->BID;
                       $fund_transactions['orderId'] = $data['UNIQUEID'];
                        if ($a+1 == $i) {
                            $fund_transactions['amount'] = $b;
                         }else{
                            $fund_transactions['amount'] = '5000';
                         }
                        if ($data['STATUS'] == "SUCCESS") {
                            $fund_transactions['surcharge'] = $surcharge;
                         }else{
                            $fund_transactions['surcharge'] = 0;
                         }
                       $fund_transactions['transferMode'] = $payment_mode;
                       $fund_transactions['selected_pay_mode'] = $selected_pay_mode;
                       $fund_transactions['status'] = "ACCEPTED";
                       $fund_transactions['statusMessage'] =$data['RESPONSE'];
                       $fund_transactions['reqid'] = $data['REQID'];
                       $fund_transactions['utrnumber'] = $data['UTRNUMBER'];
                       $fund_transactions->save();

                        
                    }
                }else{
                    $response['status'] = 'general_error';
                    $response['message'] = $data['MESSAGE'];
                    $response['user_status'] = $user->status;
                    return response()->json($response, $this->successStatus);
                }


                $user->otp = NULL;
                $user->save();
                if ($data['STATUS'] == "SUCCESS") {
                    $getcommission = json_decode($usercommission->commission);

                    $is_commission = 0;
                    foreach ($getcommission as $key => $value) {
                      if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {

                              $datacr = new User_transactions();
                              $datacr->user_id = $user->id;
                              $datacr->created_by = $user->created_by;
                              $datacr->transaction_id = $unique_id;
                              $datacr->type = "debit";
                              $datacr->label = 'ewallet';
                              $datacr->status = $data['STATUS'];
                              $datacr->date = date('Y-m-d H:i:s');
                              $datacr->narration = "Fund Transfer Success. Info:".$datacr->transaction_id;
                              $datacr->transaction_type = "Fund Transfer";

                          if ($value->commission_type == "flat") {
                              $datacr->amount = request('amount') + $value->commission;
                              $datacr->current_balance = $user->ewallet - (request('amount') + $value->commission);
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                              $datacr->save();
                          }elseif ($value->commission_type == "percentage") {
                              $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                              $datacr->amount = request('amount') + ((request('amount')*$value->commission)/100);
                              // $datacr->narration = $user->role."-".$user->first_name." ".$user->last_name."(".$user->mobile."): Fund Debited (INR.".number_format($datacr->amount,2, '.', '').")";
                              $datacr->current_balance = $user->ewallet - $percentagecom;
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                              $datacr->save();
                          }

                          $is_commission = 1;
                       }
                    }

                    if($is_commission == 0)
                    {
                          $datacr = new User_transactions();
                          $datacr->user_id = $user->id;
                          $datacr->created_by = $user->created_by;
                          $datacr->transaction_id = $unique_id;
                          $datacr->narration = "Fund Transfer Failed. Info:".$datacr->transaction_id;
                          $datacr->transaction_type = "Fund Transfer";
                          $datacr->type = "debit";
                          $datacr->label = 'ewallet';
                          $datacr->status = 'Failed';
                          $datacr->date = date('Y-m-d H:i:s');
                          $datacr->amount = request('amount');
                          $datacr->current_balance = $user->ewallet;// - request('amount');
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $datacr->save();

                    }
                    $accountname = Customer_accounts::where('account',$fund_transactions->account)->first();
                    $fund_transactions->account_name = $accountname->benificiary_name;
                    $fund_transactions->amount = request('amount');
                    $fund_transactions->transferMode = request('payment_type');

                    $mobile = $user->mobile;
                    $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your Main Wallet debited with INR '.number_format($datacr->amount,2, '.', '').' on '.$datacr->date.'. Info: (Fund Transfer Successful. UTR: '.$fund_transactions->utrnumber.', Txn Id:'.$datacr->transaction_id.'). Avbl Bal:INR '.number_format($user->ewallet,2, '.', ''));
                    $api_key = '25F6EF10FE18AF';
                    $contacts = $mobile;
                    $from = 'DMSKRT';
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                    $output = curl_exec($ch);

                    $current_bal = Fund_transactions::where('orderId',$fund_transactions->orderId)->where('transfer_type','Fund Transfer')->first();
                    $current_bal->current_balance = $user->ewallet;
                    $current_bal->save();

                    $response['status'] = 'success';
                    if (request('payment_type') == "NEFT") {
                        $response['message'] = 'Fund Transfer Successfully, Amount will be credited in 1 hour.';
                    }else{
                        $response['message'] = 'Fund Transfer Successfully!!!';
                    }
                    $response['user_status'] = $user->status;
                    $response['description'] = $fund_transactions;
                 }else{
                    $current_bal = Fund_transactions::where('orderId',$fund_transactions->orderId)->where('transfer_type','Fund Transfer')->first();
                    $current_bal->current_balance = $user->ewallet;
                    $current_bal->save();

                    $response['status'] = 'error';
                    $response['message'] = 'FAILURE';
                    $response['user_status'] = $user->status;
                    $response['description'] =  $fund_transactions;
                 }
                
            }else{
                $response['status'] = 'amount_error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Insufficient Balance!';
            }
        }else{
            $response['status'] = 'otp error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Otp not matched!!!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function expressPayoutTransfer(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'otp' => 'required', 
            // 'order_id' => 'required', 
            'user_id' => 'required', 
            'benificiary_id' => 'required', 
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }



        $customer = Customer_accounts::where('BID',request('benificiary_id'))->first();
        $selected_pay_mode = request('payment_type');
        if (request('payment_type') == "IMPS") {
          $service_type = "3";
        }elseif (request('payment_type') == "NEFT") {
          $service_type = "4";
        }elseif (request('payment_type') == "RTGS") {
          $service_type = "5";
        }
        // $usercommission = User::where('users.id',request('user_id'))->leftJoin('aeps_commissions','aeps_commissions.package_id','users.package_id')->where('aeps_commissions.services','fund_transfer')->first(['aeps_commissions.services','aeps_commissions.commission','users.*']);
        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id',$service_type)->first();
        $getcommission = json_decode($usercommission->commission);

        if ($user->otp == request('otp')) {
            if (request('amount') < $user->ewallet) {
                foreach ($getcommission as $key => $value) {
                    if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                        if ($value->commission_type == "flat") {
                            $surcharge = $value->commission; 
                            $percentagecom = request('amount') + $value->commission; 
                            if ((request('amount') + $value->commission) <= $user->ewallet) {
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }elseif ($value->commission_type == "percentage") {
                            $surcharge = (request('amount')*$value->commission)/100;
                            $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            if($percentagecom <= $user->ewallet){
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }

                    }else{
                         $surcharge = 0;
                        if (request('amount') <= $user->ewallet) {
                            $response['status'] = 'success';
                        }else{
                            $response['status'] = 'amount_error';
                            $response['user_status'] = $user->status;
                            $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                            return response()->json($response, $this->successStatus);
                        }
                    } 
                }

                //CIB
                  $apiname = 'Transaction';
                  $guid = 'GUID'.date('YmdHis');
                  $amount = request('amount');

                  $bankname = $customer->bank_name;
                  if ($bankname == "ICICI Bank") {
                    $ifsccode = "ICIC0000011";
                      $payment_mode = "TPA";
                  }else{
                    $ifsccode = $customer->ifsc_code;
                    if ($request->payment_type == "IMPS") {
                      $payment_mode = "IFS";
                    }else{
                      $payment_mode = "RGS";
                    }
                  }
                  // if ($request->payment_type == "IMPS") {
                  //   $payment_mode = "IFS";
                  // }else{
                  //   $payment_mode = "RGS";
                  // }
                  $unique_id = "DMS".date("Ymd").time();

                  $params = [
                      "CORPID"    =>  "574327555",
                      "USERID"    =>  "VEERENDR",
                      "AGGRNAME"  =>  "DAMSKART",
                      "AGGRID"    =>  "OTOE0077",
                      "URN"       =>  "SR194499369",
                      "DEBITACC"  =>  "678405600727",  
                      "CREDITACC" =>  $customer->account,
                      "IFSC"      =>  $ifsccode,      
                      "AMOUNT"    =>  $amount,
                      "CURRENCY"  =>  "INR",
                      "TXNTYPE"   =>  $payment_mode,
                      "PAYEENAME" =>  $customer->benificiary_name,
                      "REMARKS"   =>  "prod",
                      "UNIQUEID"  =>  $unique_id
                  ];
                  $source = json_encode($params);
                  $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                  $pub_key_string=fread($fp,8192);
                  fclose($fp);
                  openssl_get_publickey($pub_key_string);
                  openssl_public_encrypt($source,$crypttext,$pub_key_string);
                  $request = json_encode(base64_encode($crypttext));
                  $header = [
                      'apikey:1032da18afb74ccf8247f971aaae24ac',
                      'Content-type:text/plain'
                  ];
                  $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/Transaction';
                  $file = public_path().'/logs/'.$apiname.'.txt';
                  $log = "\n\n".'GUID - '.$guid."================================================================\n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'SERVER IP - '. $_SERVER['SERVER_ADDR']."\n\n";
                  $log .= 'HEADER - '.json_encode($header)."\n\n";
                  $log .= 'REQUEST - '.json_encode($params)."\n\n";
                  $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                  // $log .= 'IP'.$response;
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                      CURLOPT_PORT => "443",
                      CURLOPT_URL => $httpUrl,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 60,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => $request,
                      CURLOPT_HTTPHEADER => $header
                  ));
                  $responsedata = curl_exec($curl);
                  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                  $err = curl_error($curl);
                  curl_close($curl);

                  $fp= fopen(public_path()."/keys/damskart.final.key","r");
                  $priv_key=fread($fp,8192);
                  fclose($fp);
                  $res = openssl_get_privatekey($priv_key, "");

                  openssl_private_decrypt(base64_decode($responsedata), $newsource, $res);
                  $log = "\n\n".'GUID - '.$guid."================================================================ \n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'RESPONSE - '.json_encode($responsedata)."\n\n";
                  $log .= 'REQUEST DECRYPTED - '.$newsource."\n\n";
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $data = json_decode($newsource, TRUE);

                  if ($data['STATUS'] == "SUCCESS") {
                    $a = floor(request('amount') / 5000);
                    $b = request('amount') % 5000;
                    for ($i=1; $i <= $a+1 ; $i++) { 
                       
                       $fund_transactions =  new Fund_transactions();
                       $fund_transactions['user_id'] = $customer->user_id;
                       $fund_transactions['created_by'] = $customer->created_by;
                       $fund_transactions['account'] = $customer->account;
                       $fund_transactions['reference_no'] = NULL;
                       $fund_transactions['dms_txn_id'] =  $unique_id;
                       $fund_transactions['ifsc_code'] = $ifsccode;
                       $fund_transactions['transfer_type'] = "Express Payout";
                       $fund_transactions['pay_type'] = "cib";
                       $fund_transactions['purpose'] = "Express Payout";
                       $fund_transactions['BID'] = $customer->BID;
                       $fund_transactions['orderId'] = $data['UNIQUEID'];
                        if ($a+1 == $i) {
                            $fund_transactions['amount'] = $b;
                         }else{
                            $fund_transactions['amount'] = '5000';
                         }
                        if ($data['STATUS'] == "SUCCESS") {
                            $fund_transactions['surcharge'] = $surcharge;
                         }else{
                            $fund_transactions['surcharge'] = 0;
                         }
                       $fund_transactions['transferMode'] = $payment_mode;
                       $fund_transactions['selected_pay_mode'] = $selected_pay_mode;
                       $fund_transactions['status'] = "ACCEPTED";
                       $fund_transactions['statusMessage'] =$data['RESPONSE'];
                       $fund_transactions['reqid'] = $data['REQID'];
                       $fund_transactions['utrnumber'] = $data['UTRNUMBER'];
                       $fund_transactions->save();

                        
                    }
                }else{
                    $response['status'] = 'general_error';
                    $response['message'] = $data['MESSAGE'];
                    $response['user_status'] = $user->status;
                    return response()->json($response, $this->successStatus);
                }


                $user->otp = NULL;
                $user->save();
                 if ($data['STATUS'] == "SUCCESS") {
                    $getcommission = json_decode($usercommission->commission);
                    foreach ($getcommission as $key => $value) {
                      if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {

                              $datacr = new User_transactions();
                              $datacr->user_id = $user->id;
                              $datacr->created_by = $user->created_by;
                              $datacr->transaction_id = $unique_id;
                              $datacr->type = "debit";
                              $datacr->label = 'ewallet';
                              $datacr->status = $data['STATUS'];
                              $datacr->date = date('Y-m-d H:i:s');
                              $datacr->narration = "Express Payout Success. Info:".$datacr->transaction_id;

                          if ($value->commission_type == "flat") {
                              $datacr->amount = request('amount') + $value->commission;
                              $datacr->current_balance = $user->ewallet - (request('amount') + $value->commission);
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                              $datacr->save();
                          }elseif ($value->commission_type == "percentage") {
                              $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                              $datacr->amount = request('amount') + ((request('amount')*$value->commission)/100);
                              // $datacr->narration = $user->role."-".$user->first_name." ".$user->last_name."(".$user->mobile."): Fund Debited (INR.".number_format($datacr->amount,2, '.', '').")";
                              $datacr->current_balance = $user->ewallet - $percentagecom;
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                              $datacr->save();
                          }
                      }else{
                          $datacr = new User_transactions();
                          $datacr->user_id = $user->id;
                          $datacr->created_by = $user->created_by;
                          $datacr->transaction_id = $unique_id;
                          $datacr->narration = "Express Payout Success. Info:".$datacr->transaction_id;
                          $datacr->type = "debit";
                          $datacr->label = 'ewallet';
                          $datacr->status = $data['STATUS'];
                          $datacr->date = date('Y-m-d H:i:s');
                          $datacr->amount = request('amount');
                          $datacr->current_balance = $user->ewallet - request('amount');
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                          $datacr->save();
                      }   
                    }
                    $accountname = Customer_accounts::where('account',$fund_transactions->account)->first();
                    $fund_transactions->account_name = $accountname->benificiary_name;
                    $fund_transactions->amount = request('amount');
                    $fund_transactions->transferMode = request('payment_type');

                    $mobile = $user->mobile;
                    $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your Main Wallet debited with INR '.number_format($datacr->amount,2, '.', '').' on '.$datacr->date.'. Info: (Express Payout Successful. UTR: '.$fund_transactions->utrnumber.', Txn Id:'.$datacr->transaction_id.'). Avbl Bal:INR '.number_format($user->ewallet,2, '.', ''));
                    $api_key = '25F6EF10FE18AF';
                    $contacts = $mobile;
                    $from = 'DMSKRT';
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                    $output = curl_exec($ch);

                    $current_bal = Fund_transactions::where('orderId',$fund_transactions->orderId)->where('transfer_type','Express Payout')->first();
                    $current_bal->current_balance = $user->ewallet;
                    $current_bal->save();

                    $response['status'] = 'success';
                    $response['user_status'] = $user->status;
                    if (request('payment_type') == "NEFT") {
                        $response['message'] = 'Express Payout Successfully, Amount will be credited in 1 hour.';
                    }else{
                        $response['message'] = 'Express Payout Successfully!!!';
                    }
                    $response['description'] = $fund_transactions;
                 }else{
                    $current_bal = Fund_transactions::where('orderId',$fund_transactions->orderId)->where('transfer_type','Express Payout')->first();
                    $current_bal->current_balance = $user->ewallet;
                    $current_bal->save();

                    $response['status'] = 'error';
                    $response['message'] = 'FAILURE';
                    $response['user_status'] = $user->status;
                    $response['description'] =  $fund_transactions;
                 }
                
            }else{
                $response['status'] = 'amount_error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Insufficient Balance!';
            }
        }else{
            $response['status'] = 'otp error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Otp not matched!!!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function payoutTransfer(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'otp' => 'required', 
            // 'order_id' => 'required', 
            'user_id' => 'required', 
            'benificiary_id' => 'required', 
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        $customer = User_accounts::where('BID',request('benificiary_id'))->first();
        // $usercommission = User::where('users.id',request('user_id'))->leftJoin('aeps_commissions','aeps_commissions.package_id','users.package_id')->where('aeps_commissions.services','payout')->first(['aeps_commissions.services','aeps_commissions.commission','users.*']);
        $selected_pay_mode = request('payment_type');
        if (request('payment_type') == "IMPS") {
          $service_type = "10";
        }elseif (request('payment_type') == "NEFT") {
          $service_type = "11";
        }elseif (request('payment_type') == "RTGS") {
          $service_type = "12";
        }
        $usercommission = Aeps_commissions::where('aeps_commissions.package_id',$user->package_id)->where('aeps_commissions.service_id',$service_type)->first();
        $getcommission = json_decode($usercommission->commission);
        
        if ($user->otp == request('otp')) {
            if (request('amount') < $user->ewallet) {
                foreach ($getcommission as $key => $value) {
                    if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                        if ($value->commission_type == "flat") {
                             $surcharge = $value->commission;
                             $percentagecom = request('amount') + $value->commission;
                            if ((request('amount') + $value->commission) <= $user->ewallet) {
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }elseif ($value->commission_type == "percentage") {
                            $surcharge = (request('amount')*$value->commission)/100;
                            $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            if($percentagecom <= $user->ewallet){
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'amount_error';
                                $response['user_status'] = $user->status;
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }
                    }else{
                         $surcharge = 0;
                        if (request('amount') <= $user->ewallet) {
                            $response['status'] = 'success';
                        }else{
                            $response['status'] = 'amount_error';
                            $response['user_status'] = $user->status;
                            $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                            return response()->json($response, $this->successStatus);
                        }
                    }
                }
                //CIB
                  $apiname = 'Transaction';
                  $guid = 'GUID'.date('YmdHis');
                  $amount = request('amount');
                  
                  $bankname = $customer->bank_name;
                  if ($bankname == "ICICI Bank") {
                    $ifsccode = "ICIC0000011";
                      $payment_mode = "TPA";
                  }else{
                    $ifsccode = $customer->ifsc_code;
                    if ($request->payment_type == "IMPS") {
                      $payment_mode = "IFS";
                    }else{
                      $payment_mode = "RGS";
                    }
                  }
                  // if ($request->payment_type == "IMPS") {
                  //   $payment_mode = "IFS";
                  // }else{
                  //   $payment_mode = "RGS";
                  // }
                  $unique_id = "DMS".date("Ymd").time();

                  $params = [
                      "CORPID"    =>  "574327555",
                      "USERID"    =>  "VEERENDR",
                      "AGGRNAME"  =>  "DAMSKART",
                      "AGGRID"    =>  "OTOE0077",
                      "URN"       =>  "SR194499369",
                      "DEBITACC"  =>  "678405600727",  
                      "CREDITACC" =>  $customer->account,
                      "IFSC"      =>  $ifsccode,      
                      "AMOUNT"    =>  $amount,
                      "CURRENCY"  =>  "INR",
                      "TXNTYPE"   =>  $payment_mode,
                      "PAYEENAME" =>  $customer->benificiary_name,
                      "REMARKS"   =>  "prod",
                      "UNIQUEID"  =>  $unique_id
                  ];
                  $source = json_encode($params);
                  $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                  $pub_key_string=fread($fp,8192);
                  fclose($fp);
                  openssl_get_publickey($pub_key_string);
                  openssl_public_encrypt($source,$crypttext,$pub_key_string);
                  $request = json_encode(base64_encode($crypttext));
                  $header = [
                      'apikey:1032da18afb74ccf8247f971aaae24ac',
                      'Content-type:text/plain'
                  ];
                  $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/Transaction';
                  $file = public_path().'/logs/'.$apiname.'.txt';
                  $log = "\n\n".'GUID - '.$guid."================================================================\n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'SERVER IP - '. $_SERVER['SERVER_ADDR']."\n\n";
                  $log .= 'HEADER - '.json_encode($header)."\n\n";
                  $log .= 'REQUEST - '.json_encode($params)."\n\n";
                  $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                  // $log .= 'IP'.$response;
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                      CURLOPT_PORT => "443",
                      CURLOPT_URL => $httpUrl,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 60,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => $request,
                      CURLOPT_HTTPHEADER => $header
                  ));
                  $responsedata = curl_exec($curl);
                  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                  $err = curl_error($curl);
                  curl_close($curl);

                  $fp= fopen(public_path()."/keys/damskart.final.key","r");
                  $priv_key=fread($fp,8192);
                  fclose($fp);
                  $res = openssl_get_privatekey($priv_key, "");
 
                  openssl_private_decrypt(base64_decode($responsedata), $newsource, $res);
                  $log = "\n\n".'GUID - '.$guid."================================================================ \n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'RESPONSE - '.json_encode($responsedata)."\n\n";
                  $log .= 'REQUEST DECRYPTED - '.$newsource."\n\n";
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $data = json_decode($newsource, TRUE);

                    if ($data['STATUS'] == "SUCCESS") {
                       $payout =  new Fund_transactions();
                       $payout['user_id'] = $customer->user_id;
                       $payout['created_by'] = $customer->created_by;
                       $payout['account'] = $customer->account;
                       $payout['ifsc_code'] = $ifsccode;
                       $payout['reference_no'] = NULL;
                       $payout['dms_txn_id'] =  $unique_id;
                       if ($data['STATUS'] == "SUCCESS") {
                            $payout['surcharge'] = $surcharge;
                        }else{
                            $payout['surcharge'] = 0;
                        }
                       $payout['transfer_type'] = "Payout";
                       $payout['purpose'] = "payout";
                       $payout['pay_type'] = "cib";
                       $payout['BID'] = $customer->BID;
                       $payout['orderId'] = $data['UNIQUEID'];
                       $payout['amount'] = $amount;
                       $payout['transferMode'] = $payment_mode;
                       $payout['selected_pay_mode'] = $selected_pay_mode;
                       $payout['status'] = "ACCEPTED";
                       $payout['statusMessage'] = $data['RESPONSE'];
                       $payout['reqid'] = $data['REQID'];
                       $payout['utrnumber'] = $data['UTRNUMBER'];
                       $payout->save();
                       // $respmessage = $data['RESPONSE'];
                    }else{
                        $response['status'] = 'general_error';
                        $response['message'] = $data['MESSAGE'];
                        $response['user_status'] = $user->status;
                        return response()->json($response, $this->successStatus);
                    }
                

                $user->otp = NULL;
                $user->save();

                if ($payout['status'] == "ACCEPTED") {
                    $getcommission = json_decode($usercommission->commission);

                    $is_commission = 0;
                    foreach ($getcommission as $key => $value) {
                      if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                            $datacr = new User_transactions();
                            $datacr->user_id = $user->id;
                            $datacr->created_by = $user->created_by;
                            $datacr->transaction_id = $unique_id;
                            $datacr->narration = "Payout Success. Info:".$datacr->transaction_id;
                            $datacr->type = "debit";
                            $datacr->label = 'ewallet';
                            $datacr->status = $payout['status'];
                            $datacr->date = date('Y-m-d H:i:s');
                            $datacr->amount = request('amount');
                            $datacr->transaction_type = "Payout";
                            $datacr->current_balance = $user->ewallet - request('amount');
                            $user->ewallet = $datacr->current_balance;
                            $user->save();
                            $datacr->save();

                            $datacr = new User_transactions();
                            $datacr->user_id = $user->id;
                            $datacr->created_by = $user->created_by;
                            $datacr->transaction_id = $unique_id;
                            $datacr->type = "debit";
                            $datacr->label = 'ewallet';
                            $datacr->status = $payout['status'];
                            $datacr->date = date('Y-m-d H:i:s');
                            $datacr->transaction_type = "Payout Surcharge";
                            $datacr->narration = "Payout Surcharge Info:".$datacr->transaction_id;

                            if ($value->commission_type == "flat") {
                                $datacr->amount = $value->commission;
                                $datacr->current_balance = $user->ewallet - ($value->commission);
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                                $datacr->save();
                            } elseif ($value->commission_type == "percentage") {
                                $percentagecom = ((request('amount')*$value->commission)/100);
                                $datacr->amount = ((request('amount')*$value->commission)/100);
                                $datacr->current_balance = $user->ewallet - $percentagecom;
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                                $datacr->save();
                            }

                            $current_bal = Fund_transactions::where('orderId',$payout['orderId'])->where('transfer_type','Payout')->first();
                            $current_bal->current_balance = $user->ewallet;
                            $current_bal->save();

                            $is_commission = 1;

                        }
                    }

                    if($is_commission == 0)
                    {
                        $datacr = new User_transactions();
                        $datacr->user_id = $user->id;
                        $datacr->created_by = $user->created_by;
                        $datacr->transaction_id = $unique_id;
                        $datacr->narration = "Payout Transfer Success. Info:".$datacr->transaction_id;
                        $datacr->type = "debit";
                        $datacr->label = 'ewallet';
                        $datacr->status = $payout['status'];
                        $datacr->date = date('Y-m-d H:i:s');
                        $datacr->amount = request('amount');
                        $datacr->transaction_type = "Payout";
                        $datacr->current_balance = $user->ewallet - request('amount');
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                        $datacr->save();

                        $datacr = new User_transactions();
                        $datacr->user_id = $user->id;
                        $datacr->created_by = $user->created_by;
                        $datacr->transaction_id = $unique_id;
                        $datacr->narration = "Payout Surcharge Failed Info:".$datacr->transaction_id;
                        $datacr->transaction_type = "Payout Surcharge";
                        $datacr->type = "debit";
                        $datacr->label = 'ewallet';
                        $datacr->status = 'Failed';
                        $datacr->date = date('Y-m-d H:i:s');
                        $datacr->amount = 0;
                        $datacr->current_balance = $user->ewallet;
                        $datacr->save();
                    }
                    
                    $accountname = User_accounts::where('account',$payout->account)->first();
                    $payout->account_name = $accountname->benificiary_name;
                    $payout->transferMode = request('payment_type');
                    
                    $mobile = $user->mobile;
                    $sms_text = urlencode('Dear '.ucfirst($user->first_name).' '.ucfirst($user->last_name).', Your Main Wallet debited with INR '.number_format($datacr->amount,2, '.', '').' on '.$datacr->date.'. Info: (Payout Successful. UTR: '.$payout->utrnumber.', Txn Id:'.$datacr->transaction_id.'). Avbl Bal:INR '.number_format($user->ewallet,2, '.', ''));
                    $api_key = '25F6EF10FE18AF';
                    $contacts = $mobile;
                    $from = 'DMSKRT';
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
                    $output = curl_exec($ch);

                      $current_bal = Fund_transactions::where('orderId',$payout->orderId)->where('transfer_type','Payout')->first();
                      $current_bal->current_balance = $user->ewallet;
                      $current_bal->save();

                    $response['status'] = 'success';
                    if (request('payment_type') == "NEFT") {
                        $response['message'] = 'Transfer Successfully, Amount will be credited in 1 hour.';
                    }else{
                        $response['message'] = 'Transfer Successfully!!!';
                    }
                    $response['user_status'] = $user->status;
                    $response['description'] = $payout;
                }else{

                      $current_bal = Fund_transactions::where('orderId',$payout->orderId)->where('transfer_type','Payout')->first();
                      $current_bal->current_balance = $user->ewallet;
                      $current_bal->save();
                    $response['status'] = 'error';
                    $response['message'] = 'FAILURE';
                    $response['user_status'] = $user->status;
                    $response['description'] =  $payout;
                }
                
            }else{
                $response['status'] = 'amount_error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Insufficient Balance!';
            }
        }else{
            $response['status'] = 'otp error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Otp not matched!!!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function payoutTransferOld(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'otp' => 'required', 
            'order_id' => 'required', 
            'user_id' => 'required', 
            'benificiary_id' => 'required', 
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        $customer = User_accounts::where('BID',request('benificiary_id'))->first();
        $usercommission = User::where('users.id',request('user_id'))->leftJoin('aeps_commissions','aeps_commissions.package_id','users.package_id')->where('aeps_commissions.service_id','payout')->first(['aeps_commissions.service_id','aeps_commissions.commission','users.*']);
        $getcommission = json_decode($usercommission->commission);
        
        if ($user->otp == request('otp')) {
            if (request('amount') < $user->ewallet) {
                foreach ($getcommission as $key => $value) {
                    if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {
                        if ($value->commission_type == "flat") {
                             $surcharge = $value->commission;
                             $percentagecom = request('amount') + $value->commission;
                            if ((request('amount') + $value->commission) <= $user->ewallet) {
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'error';
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }elseif ($value->commission_type == "percentage") {
                            $surcharge = (request('amount')*$value->commission)/100;
                            $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            if($percentagecom <= $user->ewallet){
                                 $response['status'] = 'success';
                            }else{
                                $response['status'] = 'error';
                                $response['message'] = 'Main Wallet amount can not be less then to entered Amount.';
                                return response()->json($response, $this->successStatus);
                            }
                        }
                    }
                }

                $unique_id = "DMS".date("Ymd").time();
                 
                if (intval($request->amount) <= 25000) { //Paytm
                    $remarks = "OTHERS";
                    $paytmParams = array();
                    $paytmParams["subwalletGuid"]      = "e801f900-ad21-4eb2-8a85-ee7736978963";
                    $paytmParams["orderId"]            = $request->order_id;
                    $paytmParams["beneficiaryAccount"] = $customer->account;
                    $paytmParams["beneficiaryIFSC"]    = $customer->ifsc_code;

                    // $paytmParams["beneficiaryVPA"]             = "test@paytm";
                    $paytmParams["transferMode"]             = $request->payment_type;
                    $paytmParams["amount"]             = $request->amount;
                    $paytmParams["purpose"]            = $remarks;
                    $paytmParams["date"]               = date('Y-M-D');
                    $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
                    /*
                    * Generate checksum by parameters we have in body
                    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
                    */
                    $checksum = PaytmChecksum::generateSignature($post_data, "QQ1@6hsOHMBo0yYc");
                    $x_mid      = "Damska60566991793718";
                    $x_checksum = $checksum;
                    /* for Staging */
                    $url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
                    /* for Production */
                    // $url = "https://dashboard.paytm.com/bpay/api/v1/disburse/order/bank";
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "x-mid: " . $x_mid, "x-checksum: " . $x_checksum)); 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                    $output = curl_exec($ch);
                    $data = json_decode($output); 

                       $payout =  new Fund_transactions();
                       $payout['user_id'] = $customer->user_id;
                       $payout['created_by'] = $user->created_by;
                       $payout['account'] = $customer->account;
                       $payout['ifsc_code'] = $customer->ifsc_code;
                       $payout['reference_no'] = NULL;
                    $payout['dms_txn_id'] = $unique_id;
                         if ($data->status == "ACCEPTED") {
                              $payout['surcharge'] = $surcharge;
                          }else{
                              $payout['surcharge'] = 0;
                          }
                       $payout['transfer_type'] = "Payout";
                       $payout['pay_type'] = "paytm";
                       $payout['purpose'] = $remarks;
                       $payout['BID'] = $customer->BID;
                       $payout['orderId'] = $request->order_id;
                       $payout['amount'] = $request->amount;
                       $payout['transferMode'] = $request->payment_type;
                       $payout['status'] = $data->status;
                       $payout['statusMessage'] = $data->statusMessage;
                       $payout->save();
                }else{ //CIB
                  $apiname = 'Transaction';
                  $guid = 'GUID'.date('YmdHis');
                  $amount = request('amount');
                  if ($request->payment_type == "IMPS") {
                    $payment_mode = "IFS";
                  }else{
                    $payment_mode = "RGS";
                  }
                  $params = [
                      "CORPID"    =>  "574327555",
                      "USERID"    =>  "VEERENDR",
                      "AGGRNAME"  =>  "DAMSKART",
                      "AGGRID"    =>  "OTOE0077",
                      "URN"       =>  "SR194499369",
                      "DEBITACC"  =>  "678405600727",  
                      "CREDITACC" =>  $customer->account,
                      "IFSC"      =>  $customer->ifsc_code,      
                      "AMOUNT"    =>  $amount,
                      "CURRENCY"  =>  "INR",
                      "TXNTYPE"   =>  $payment_mode,
                      "PAYEENAME" =>  $customer->benificiary_name,
                      "REMARKS"   =>  "prod",
                      "UNIQUEID"  =>  $unique_id
                  ];
                  $source = json_encode($params);
                  $fp=fopen(public_path()."/keys/ICICI_PUBLIC_CERT_PROD.txt","r");
                  $pub_key_string=fread($fp,8192);
                  fclose($fp);
                  openssl_get_publickey($pub_key_string);
                  openssl_public_encrypt($source,$crypttext,$pub_key_string);
                  $request = json_encode(base64_encode($crypttext));
                  $header = [
                      'apikey:1032da18afb74ccf8247f971aaae24ac',
                      'Content-type:text/plain'
                  ];
                  $httpUrl = 'https://apibankingone.icicibank.com/api/Corporate/CIB/v1/Transaction';
                  $file = public_path().'/logs/'.$apiname.'.txt';
                  $log = "\n\n".'GUID - '.$guid."================================================================\n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'SERVER IP - '. $_SERVER['SERVER_ADDR']."\n\n";
                  $log .= 'HEADER - '.json_encode($header)."\n\n";
                  $log .= 'REQUEST - '.json_encode($params)."\n\n";
                  $log .= 'REQUEST ENCRYPTED - '.json_encode($request)."\n\n";
                  // $log .= 'IP'.$response;
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                      CURLOPT_PORT => "8443",
                      CURLOPT_URL => $httpUrl,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 60,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => $request,
                      CURLOPT_HTTPHEADER => $header
                  ));
                  $responsedata = curl_exec($curl);
                  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                  $err = curl_error($curl);
                  curl_close($curl);

                  $fp= fopen(public_path()."/keys/damskart_prod_priv.pem","r");
                  $priv_key=fread($fp,8192);
                  fclose($fp);
                  $res = openssl_get_privatekey($priv_key, "");

                  openssl_private_decrypt(base64_decode($responsedata), $newsource, $res);
                  $log = "\n\n".'GUID - '.$guid."================================================================ \n";
                  $log .= 'URL - '.$httpUrl."\n\n";
                  $log .= 'RESPONSE - '.json_encode($responsedata)."\n\n";
                  $log .= 'REQUEST DECRYPTED - '.$newsource."\n\n";
                  file_put_contents($file, $log, FILE_APPEND | LOCK_EX);
                  $data = json_decode($newsource, TRUE);

                    if ($data['STATUS'] == "SUCCESS") {
                       $payout =  new Fund_transactions();
                       $payout['user_id'] = $customer->user_id;
                       $payout['created_by'] = $customer->created_by;
                       $payout['account'] = $customer->account;
                       $payout['ifsc_code'] = $customer->ifsc_code;
                       $payout['reference_no'] = NULL;
                       $payout['dms_txn_id'] =  $unique_id;

                       if ($data['STATUS'] == "SUCCESS") {
                            $payout['surcharge'] = $surcharge;
                        }else{
                            $payout['surcharge'] = 0;
                        }
                       $payout['transfer_type'] = "Payout";
                       $payout['purpose'] = "payout";
                       $payout['pay_type'] = "cib";
                       $payout['BID'] = $customer->BID;
                       $payout['orderId'] = $data['UNIQUEID'];
                       $payout['amount'] = $amount;
                       $payout['transferMode'] = $payment_mode;
                       $payout['status'] = "ACCEPTED";
                       $payout['statusMessage'] = $data['RESPONSE'];
                       $payout['reqid'] = $data['REQID'];
                       $payout['utrnumber'] = $data['UTRNUMBER'];
                       $payout->save();
                       // $respmessage = $data['RESPONSE'];
                    }else{
                        $response['status'] = 'general_error';
                        $response['message'] = $data['MESSAGE'];
                        return response()->json($response, $this->successStatus);
                    }
                }

                $user->otp = NULL;
                $user->save();
                if ($payout->status == "ACCEPTED") {
                    $getcommission = json_decode($usercommission->commission);
                    foreach ($getcommission as $key => $value) {
                      if ($value->start_price <= request('amount') && $value->end_price >= request('amount')) {

                              $datacr = new User_transactions();
                              $datacr->user_id = $user->id;
                              $datacr->created_by = $user->created_by;
                              $datacr->transaction_id = $unique_id;
                              $datacr->type = "debit";
                              $datacr->label = 'ewallet';
                              $datacr->status = $payout->status;
                              $datacr->date = date('Y-m-d H:i:s');
                              $datacr->narration =  "Payout Success. Info:".$datacr->transaction_id;

                          if ($value->commission_type == "flat") {
                              $datacr->amount = request('amount') + $value->commission;
                              $datacr->current_balance = $user->ewallet - (request('amount') + $value->commission);
                                $user->ewallet = $datacr->current_balance;
                                $user->save();
                              $datacr->save();
                          }elseif ($value->commission_type == "percentage") {
                              $percentagecom = request('amount') + ((request('amount')*$value->commission)/100);
                            $datacr->amount =request('amount') + ((request('amount')*$value->commission)/100);
                            $datacr->current_balance = $user->ewallet - $percentagecom;
                              $user->ewallet = $datacr->current_balance;
                              $user->save();
                            $datacr->save();
                          }

                          $current_bal = Fund_transactions::where('orderId',$payout->orderId)->where('transfer_type','Payout')->first();
                          $current_bal->current_balance = $user->ewallet;
                          $current_bal->save();
                      }  
                    }
                    $accountname = User_accounts::where('account',$payout->account)->first();
                    $payout->account_name = $accountname->benificiary_name;
                       
                    $response['status'] = 'success';
                    $response['message'] = 'Transfer Successfully!!!';
                    $response['description'] = $payout;
                }else{

                      $current_bal = Fund_transactions::where('orderId',$payout->orderId)->where('transfer_type','Payout')->first();
                      $current_bal->current_balance = $user->ewallet;
                      $current_bal->save();
                    $response['status'] = 'error';
                    $response['message'] = 'FAILURE';
                    $response['description'] =  $payout;
                }
                
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Transfer Failed!!! Try Again';
            }
        }else{
            $response['status'] = 'otp error';
            $response['message'] = 'Otp not matched!!!';
        }
        return response()->json($response, $this->successStatus);
    }
    
    public function transactionReports(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
            'type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) {  
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $keyword = request('keyword'); 
        $date = request('date');

        $reports = array();

        if (request('type') == "icici-aeps") {
            if(request('date') == 'all'){
                // $reports = Aeps::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
                $reports = Aeps_txn_news::where('user_id', request('user_id'))
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                // $reports = Aeps::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $reports =  DB::table('aeps_txn_news')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $reports = Aeps_txn_news::where('user_id', request('user_id'))
                            ->where(function($q) use ($keyword){
                                $q->where('mobile', 'like', "%{$keyword}%")
                                    ->orWhere('amount', 'like', "%{$keyword}%")
                                    ->orWhere('status', 'like', "%{$keyword}%");
                            })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $reports =  DB::table('aeps')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Aeps_txn_news::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif(request('type') == "fund_transfer"){
            if(request('date') == 'all'){
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                    ->where('fund_transactions.user_id','=',request('user_id'))
                    ->where('fund_transactions.transfer_type','Fund Transfer') 
                    ->orderBy('fund_transactions.created_at','desc')
                    ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                        ->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('fund_transactions')
                ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id', request('user_id'))
                ->where('fund_transactions.transfer_type','Fund Transfer')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                        ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%");
                })
                ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                            ->where('fund_transactions.user_id','=',request('user_id'))
                            ->where('fund_transactions.transfer_type','Fund Transfer')
                            ->where(function($qr) use ($keyword){
                                $qr->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                                    ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%");
                            })
                            ->orderBy('fund_transactions.created_at','desc')
                            ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                                ->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('fund_transactions')
                ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id', request('user_id'))
                ->where('fund_transactions.transfer_type','Fund Transfer')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                    ->where('fund_transactions.user_id','=',request('user_id'))
                    ->where('fund_transactions.transfer_type','Fund Transfer')
                    ->orderBy('fund_transactions.created_at','desc')
                    ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                    ->splice(request('skip'))->take(request('limit'));
            }
        }elseif(request('type') == "payout") {
            if(request('date') == 'all'){
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                    ->where('fund_transactions.user_id','=',request('user_id'))
                    ->where('fund_transactions.transfer_type','Payout') 
                    ->orderBy('fund_transactions.created_at','desc')
                    ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                        ->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('fund_transactions')
                ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id', request('user_id'))
                ->where('fund_transactions.transfer_type','Payout')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
               ->where(function($qr) use ($keyword){
                    $qr->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                        ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                        ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%");
                })      
                ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                            ->where('fund_transactions.user_id','=',request('user_id'))
                            ->where('fund_transactions.transfer_type','Payout')
                            ->where(function($qr) use ($keyword){
                                $qr->where('fund_transactions.transferMode', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.amount', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.account', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.ifsc_code', 'like', "%{$keyword}%")
                                    ->orWhere('fund_transactions.status', 'like', "%{$keyword}%")
                                    ->orWhere('customer_accounts.benificiary_name', 'like', "%{$keyword}%");
                            })  
                            ->orderBy('fund_transactions.created_at','desc')
                            ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                                ->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('fund_transactions')
                ->leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                ->where('fund_transactions.user_id', request('user_id'))
                ->where('fund_transactions.transfer_type','Payout')
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('fund_transactions.created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('fund_transactions.created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('fund_transactions.created_at','desc')->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Fund_transactions::leftJoin('customer_accounts', 'customer_accounts.BID', '=', 'fund_transactions.BID')
                    ->where('fund_transactions.user_id','=',request('user_id'))
                    ->where('fund_transactions.transfer_type','Payout')
                    ->orderBy('fund_transactions.created_at','desc')
                    ->get(['customer_accounts.benificiary_name','customer_accounts.bank_name','customer_accounts.account','customer_accounts.mobile','fund_transactions.*'])
                    ->splice(request('skip'))->take(request('limit'));
            }
        }elseif (request('type') == "mobile_recharge") {
            if(request('date') == 'all'){
               $reports = Mobile_recharges::where('user_id', request('user_id')) 
                           ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('mobile_recharges')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })  
               ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $reports = Mobile_recharges::where('user_id', request('user_id'))
                    ->where(function($qr) use ($keyword){
                        $qr->where('mobile', 'like', "%{$keyword}%")
                            ->orWhere('amount', 'like', "%{$keyword}%")
                            ->orWhere('status', 'like', "%{$keyword}%");
                    })  
                    ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('mobile_recharges')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Mobile_recharges::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif (request('type') == "bbps") {
            if(request('date') == 'all'){
                $reports = Bbps_transactions::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('bbps_transactions')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
               ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('kNumber', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%")
                        ->orWhere('operator_name', 'like', "%{$keyword}%");
                })  
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                $reports = Bbps_transactions::where('user_id', request('user_id'))
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('kNumber', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%")
                        ->orWhere('operator_name', 'like', "%{$keyword}%");
                })  
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('bbps_transactions')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Bbps_transactions::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        } elseif (request('type') == "generate_qr") {
            if(request('date') == 'all'){
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"generate_qr")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"generate_qr")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                // $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"generate_qr")->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('payer_amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));

                $reports = Upi_qr_txns::where('user_id', request('user_id'))
                            ->where('txn_type',"generate_qr")
                             ->where(function($q) use ($keyword){
                                    $q->where('mobile', 'like', "%{$keyword}%")
                                        ->orWhere('amount', 'like', "%{$keyword}%")
                                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                                        ->orWhere('status', 'like', "%{$keyword}%");
                                })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"generate_qr")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"generate_qr")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif (request('type') == "send_request") {
            if(request('date') == 'all'){
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"send_request")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"send_request")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                 ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                // $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"send_request")->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('payer_amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
                $reports = Upi_qr_txns::where('user_id', request('user_id'))
                            ->where('txn_type',"send_request")
                             ->where(function($q) use ($keyword){
                                    $q->where('mobile', 'like', "%{$keyword}%")
                                        ->orWhere('amount', 'like', "%{$keyword}%")
                                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                                        ->orWhere('status', 'like', "%{$keyword}%");
                                })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"send_request")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"send_request")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }elseif (request('type') == "my_qr") {
            if(request('date') == 'all'){
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"my_qr")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"my_qr")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                 ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                // $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"my_qr")->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('payer_amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
                $reports = Upi_qr_txns::where('user_id', request('user_id'))
                            ->where('txn_type',"my_qr")
                             ->where(function($q) use ($keyword){
                                    $q->where('mobile', 'like', "%{$keyword}%")
                                        ->orWhere('amount', 'like', "%{$keyword}%")
                                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                                        ->orWhere('status', 'like', "%{$keyword}%");
                                })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where('txn_type',"my_qr")
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"my_qr")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        } elseif (request('type') == "upi") {
            if(request('date') == 'all'){
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('amount', 'like', "%{$keyword}%")
                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                // $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"generate_qr")->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('payer_amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));

                $reports = Upi_qr_txns::where('user_id', request('user_id'))
                             ->where(function($q) use ($keyword){
                                    $q->where('mobile', 'like', "%{$keyword}%")
                                        ->orWhere('amount', 'like', "%{$keyword}%")
                                        ->orWhere('payer_amount', 'like', "%{$keyword}%")
                                        ->orWhere('status', 'like', "%{$keyword}%");
                                })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('upi_qr_txns')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = Upi_qr_txns::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        } elseif (request('type') == "micro-atm") {
            if(request('date') == 'all'){
                $reports = MicroATM_txn_news::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
               
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('microatm_txn_news')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where(function($qr) use ($keyword){
                    $qr->where('mobile', 'like', "%{$keyword}%")
                        ->orWhere('transaction_amount', 'like', "%{$keyword}%")
                        ->orWhere('balance_amount', 'like', "%{$keyword}%")
                        ->orWhere('status', 'like', "%{$keyword}%");
                })
                ->orderBy('created_at', 'desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
                // $reports = Upi_qr_txns::where('user_id', request('user_id'))->where('txn_type',"generate_qr")->where('mobile', 'like', "%{$keyword}%")->orWhere('amount', 'like', "%{$keyword}%")->orWhere('payer_amount', 'like', "%{$keyword}%")->orWhere('status', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));

                $reports = MicroATM_txn_news::where('user_id', request('user_id'))
                             ->where(function($q) use ($keyword){
                                    $q->where('mobile', 'like', "%{$keyword}%")
                                        ->orWhere('transaction_amount', 'like', "%{$keyword}%")
                                        ->orWhere('balance_amount', 'like', "%{$keyword}%")
                                        ->orWhere('status', 'like', "%{$keyword}%");
                                })
                            ->orderBy('created_at','desc')
                            ->get()->splice(request('skip'))->take(request('limit'));
                
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];
                $reports =  DB::table('microatm_txn_news')
                ->where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at', 'desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $reports = MicroATM_txn_news::where('user_id', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Type not valid!';
        }
        
        if($reports)
        {
            foreach ($reports as $key => $value) {
                if (request('type') == "icici-aeps") {
                    $value->amount = $value->transaction_amount;
                }
                else if (request('type') == "bbps") {
                    $value->amount = $value->bill_amount;
                }
                else if (request('type') == "upi") {
                    $value->amount = $value->payer_amount;
                }



                $value->amount = number_format($value->amount,2, '.', '');
                $value->current_balance = number_format($value->current_balance,2, '.', '');
            }

            if (!empty($reports[0])) {
                $response['status'] = "success";
                $response['message'] = "Successfully";
                $response['user_status'] = $user->status;
                $response['description']['reports_list'] = $reports;
            }else{
                $response['status'] = 'error';
                $response['message'] = 'No transaction made yet for '.request('type');
                $response['user_status'] = $user->status;
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'No transaction made yet for '.request('type');
            $response['user_status'] = $user->status;
        }
        
        return response()->json($response, $this->successStatus);            
    }

     public function downlineMembers(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'skip' => 'required|numeric',
            'limit' => 'required|numeric', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $keyword = request('keyword'); 
        if (!empty($keyword)) {
            $downline_members = User::where('created_by', request('user_id'))->where('first_name', 'like', "%{$keyword}%")->orWhere('last_name', 'like', "%{$keyword}%")->orWhere('mobile', 'like', "%{$keyword}%")->orWhere('email', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%")->orWhere('aadhar_number', 'like', "%{$keyword}%")->orWhere('pan_number', 'like', "%{$keyword}%")->orWhere('company_name', 'like', "%{$keyword}%")->orWhere('company_pan', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            
        }else{
            $downline_members = User::where('created_by', request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
        }


        if (!empty($downline_members[0])) {
            foreach ($downline_members as $key => $value) {
                $value->ewallet = number_format($value->ewallet,3, '.', '');
                $value->aeps_wallet = number_format($value->aeps_wallet,3, '.', '');
            }
            $response['status'] = 'success';
            $response['message'] = 'Downline Member List Available.';
            $response['user_status'] = $user->status;
            $response['description']['downline_members'] = $downline_members;
           
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'You have not added any member yet!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function packageList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'role' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $package_list = Package::where('role',request('role'))->orWhere('role','all')->get(); 
       
        if (!empty($package_list[0])) {
            $response['status'] = 'success';
            $response['message'] = 'Package list available.';
            $response['user_status'] = $user->status;
            $response['description']['package_list'] = $package_list;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Package list not available!';
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function getPackage(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'role' => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        } 
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $role = request('role');
        
        $package = DB::table("packages")->where("role",$role)->orWhere("role",'all')->get();

        if (!empty($package[0])) {
            $response['status'] = 'success';
            $response['message'] = "No Package Found"; 
            $response['user_status'] = $user->status;
            $response['description']['package_list'] = $package;
        }else{
            $response['status'] = 'error';
            $response['message'] = "No Package Found";
        } 

        return response()->json($response, $this->successStatus);
    }

    public function addToken(Request $request){
        $user = Auth::user();

        request()->validate([ 
          'token' => ['required', 'numeric'],
          'price' => ['required', 'numeric'], 
          'role' => 'required',
          'package_id' => 'required'
        ]);

        $unique_id = "DMS".date("Ymd").time();

        if ($user->ewallet >= $request->price && !empty(request('package_id'))) {
            $package = DB::table("packages")->where("role",$request->role)->where("id",request('package_id'))->first();
            if (!empty($package)) {
                $data1 = new User_transactions();
                $data1->user_id = $user->id;
                $data1->created_by = $user->created_by;
                $data1->transaction_id = $unique_id;
                $data1->label = 'ewallet';
                $data1->status = 'success';
                $data1->date = date('Y-m-d H:i:s');
                $data1->amount = $request->price;
                $data1->current_balance = $user->ewallet - $request->price;
                $data1->type = "debit";
                $data1->narration =  "Token Buy - ".$package->package_name." Success. Info: ".$data1->transaction_id;

                $user->ewallet = $data1->current_balance;
                $user->save();
                if ($data1->save()) {
                    $token = new Token();
                    $token->role = $request->role;
                    $token->user_id = Auth::user()->id;
                    $token->package_id = $request->package_id;
                    $token->token = $request->token;
                    $token->price = $request->price;
                    $token->created_by = Auth::user()->id;
                    $token->created_by_role = Auth::user()->role;
                    if ($token->save()) {
                        $response['status'] = 'success';
                        $response['message'] = "Token added Successfully";
                        $response['is_allow'] = 1;
                        $response['user_status'] = $user->status;
                    }else{
                        $response['status'] = 'error';
                        $response['message'] = "Something went wrong. Try Again";
                        $response['user_status'] = $user->status;
                    }
                }else{
                    $response['status'] = 'error';
                    $response['message'] = "Something went wrong. Try Again";
                    $response['user_status'] = $user->status;
                }
            }else{
                $response['status'] = 'error';
                $response['message'] = "Package Not Found. Select Different Package";
                $response['user_status'] = $user->status;
            }

        }else{
            $response['status'] = 'balance_error';
            $response['message'] = "Not have enough amount in you wallet. Recharge your Main Wallet to buy token";
            $response['is_allow'] = 0;
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function getToken(Request $request){
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'package_id' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        } 
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }

        $package_id = request('package_id');

        $package = DB::table("packages")
                   ->where("id",$package_id)
                   ->first();
                   
        $tokens = DB::table("tokens")
                   ->where("package_id",$package_id)
                   ->where("user_id",$user_id)
                   //->where("role",$package->role)
                   ->where("receive",'Received')
                   ->orderBy("created_at",'DESC')
                   ->first();
        if(empty($tokens))
        {
          $tokens = DB::table("tokens")
                   ->where("package_id",$package_id)
                   ->where("user_id",$user_id)
                   //->where("role",$package->role)
                   ->where("created_by_role",'super admin')
                   ->orderBy("created_at",'DESC')
                   ->first();
        }
        $tokens_sum = DB::table("tokens")
                   ->where("package_id",$package_id)
                   ->where("user_id",$user_id)
                   //->where("role",$package->role)
                   ->where("receive",'Received')
                   ->sum('token');

        $tokens_sum2 = DB::table("tokens")
                   ->where("package_id",$package_id)
                   ->where("user_id",$user_id)
                   ->where("receive",'Pending')
                   //->where("role",$package->role)
                   ->where("created_by_role",'super admin')
                   ->sum('token');

        $total_token_sum = $tokens_sum+$tokens_sum2;
        $qparam = [];
        $qparam = ['super admin','admin','manager'];

        if(!empty($tokens))
        {
            $users = DB::table("users")
                   ->where("package_id",$package_id)
                   ->whereNotIn("role",$qparam)
                   ->where("created_by",$user_id)
                   ->where('created_at', '>=',$tokens->created_at)
                   ->get()->count();
        }

        if(empty($tokens)){
            $response['status'] = 'error';
            $response['message'] = 'You don\'t have token for selected package. Please click proceed for buy token first.';
            $response['is_allow'] = 0;
            $response['user_status'] = $user->status;
        }
        else if(!empty($tokens) && $total_token_sum<$users){
            $response['status'] = 'balance_error';
            $response['message'] = 'You have insufficient balance in Main Wallet for buy token. Please Recharge your Main Wallet first.';
            $response['is_allow'] = 0;
            $response['user_status'] = $user->status;
        }else{
            $token_left = $total_token_sum-$users;
            $response['status'] = 'success';
            $response['message'] = 'Available tokens: '.$token_left;
            $response['is_allow'] = 1;
            $response['user_status'] = $user->status;
        }

        return response()->json($response, $this->successStatus);
    }

    public function addMember(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'package_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required|unique:users|max:10|min:10|starts_with:6,7,8,9',
            'email' => 'required|email|unique:users',
            'gender' => 'required|alpha',
            'address' => 'required',
            'state_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'state' => 'required',
            'district' => 'required',
            'pin_code' => 'required|numeric',
            'package' => 'required',
            'role' => 'required',
            'dob' => 'required',
            'pan_pic' => 'required',
            'aadhar_pic_front' => 'required',
            'aadhar_pic_back' => 'required',
            'profile_pic' => 'required',
            'company_pic' => 'required',
            'company_name' => 'required',
            'company_address' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        } 
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $input = $request->all();
        $admin = User::where('id', $input['user_id'])->first();
        $rgscharge = Package::where('id',$request->package_id)->first();
        if ($admin->ewallet < $rgscharge->amount) {
            $response['status'] = 'amount_error';
            $response['message'] = 'Insufficient balance in wallet!';
            return response()->json($response, $this->successStatus);
        }

        $password = $this->randomPassword();
        $otp = rand(100000, 999999);

        $input['created_by'] = $admin->id;
        $input['refer_by'] = $admin->id;
        $input['domain'] = $admin->domain; 
        $input['package'] = $admin->package;
        $input['otp'] = $otp; 
        $input['dob'] = $request->dob; 
        $input['pan_number'] = strtoupper($request->pan_number); 
        // $input['fcm_token'] = $request->fcm_token;
        $input['aeps_pin'] = rand(111111, 999999);
        $input['ewallet'] = 0;
        $input['aeps_wallet'] = 0;
        $input['ewallet_status'] = 1;
        $input['aeps_status'] = NULL;
        $input['fcm_status'] = 1;
        $input['online'] = 0;
        $input['profile_status'] = 0;
        $input['onboarding'] = 0;
        $input['fundtransfer'] = 0;
        $input['yesbank'] = 0;
        $input['verify'] = 0;
        $input['status'] = 1;    
        $input['kyc_update'] = 0;   
        $input['notification'] = 0;   
        $input['digio_kyc_status'] = 0;    
        $input['digio_e_sign_status'] = 0;    
        $input['aeps_ekyc_status'] = 0; 

        $input['role'] = $request->role;
        $input['package_id'] = $request->package_id;
        $input['password'] = bcrypt($password);

        $destinationPath = 'public/images/'; // upload path
        $panpic = $request->file('pan_pic');
        $aadhar_pic_front = $request->file('aadhar_pic_front');
        $aadhar_pic_back = $request->file('aadhar_pic_back');
        $profile_pic = $request->file('profile_pic');
        $company_pic = $request->file('company_pic');

        $panImage = time() . "pan_." . $panpic->getClientOriginalExtension();
        if($panpic->move($destinationPath, $panImage)){
            $input['pan_pic'] = 'images/'.$panImage;
        }

        $aadaharFrontImage = time() . "front_." . $aadhar_pic_front->getClientOriginalExtension();
        if($aadhar_pic_front->move($destinationPath, $aadaharFrontImage)){
            $input['aadhar_pic_front'] = 'images/'.$aadaharFrontImage;
        }

        $aadaharBackImage = time() . "front_." . $aadhar_pic_back->getClientOriginalExtension();
        if($aadhar_pic_back->move($destinationPath, $aadaharBackImage)){
            $input['aadhar_pic_back'] = 'images/'.$aadaharBackImage;
        }

        $profileImage = time() . "front_." . $profile_pic->getClientOriginalExtension();
        if($profile_pic->move($destinationPath, $profileImage)){
            $input['profile_pic'] = 'images/'.$profileImage;
        }

        $companyImage = time() . "front_." . $company_pic->getClientOriginalExtension();
        if($company_pic->move($destinationPath, $companyImage)){
            $input['company_logo'] = 'images/'.$companyImage;
        }

        // $passbookImage = time() . "passbook_cheque." . $passbook_image->getClientOriginalExtension();
        // if($passbook_image->move($destinationPath, $passbookImage)){
        //     $input['passbook_image'] = 'images/'.$passbookImage;
        // }
        
        // $marksheetImage10 = time() . "10th_marksheet." . $marksheet1->getClientOriginalExtension();
        // if($marksheet1->move($destinationPath, $marksheetImage10)){
        //     $input['marksheet_10'] = 'images/'.$marksheetImage10;
        // }
        
        // $marksheetImage12 = time() . "12th_marksheet." . $marksheet2->getClientOriginalExtension();
        // if($marksheet2->move($destinationPath, $marksheetImage12)){
        //     $input['marksheet_12'] = 'images/'.$marksheetImage12;
        // }

        if(!empty(request('company_pan'))){
            $input['company_pan'] = request('company_pan');
        }


        $userdistributor = User::create($input);
        $userdistributor->assignRole($user->role);

        $package_id = $userdistributor->package_id;
        $memberid = $userdistributor->id;
        $mobile_number = $userdistributor->mobile;
        $userservice = Service_packages::all()->where('package_id','=',$package_id);
        $data = [];
        foreach($userservice as  $value) {
          $data['servicepack_id'] = $value['id'];
          $data['user_id'] = $memberid; 
          $data['status'] = $value['status']; 
          User_services::create($data); 
        }

        $api_key = '25F6EF10FE18AF';
        $contacts = $mobile_number;
        $from = 'DMSKRT';
        // $sms_text = urlencode('Thank You For Registration With Damskart. Your Verification Code is : '.$otp);
        $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access Login Transaction is '.$otp.' and Password is '.$password.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone, Not You? Report https://damskart.com. Regards Damskart Business');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207161768584481607&msg=".$sms_text);
        $output = curl_exec($ch);

        $unique_id = "DMS".date("Ymd").time();

        if($user->role != "super admin") {
          $data1 = new User_transactions();
          $data1->user_id = $user->id;
          $data1->created_by = $user->created_by;
          $data1->transaction_id = $unique_id;
          $data1->label = 'ewallet';
          $data1->status = "SUCCESS";
          $data1->date = date('Y-m-d H:i:s');
          $data1->amount = $rgscharge->amount;
          $data1->narration = "Fund Debited For Registrartion: ".ucfirst($userdistributor->first_name)." ".ucfirst($userdistributor->last_name)."(".$userdistributor->mobile."). Info: ".$data1->transaction_id;
          $data1->type = "debit";
          $data1->current_balance = $user->ewallet - $rgscharge->amount;
          $data1->save(); 
          
          $user->ewallet = $user->ewallet - $rgscharge->amount;
          $user->save(); 
            
        }

        $response['status'] = 'success';
        $response['message'] = "Add Member Successfully!!!";
        $response['user_status'] = $user->status;

        return response()->json($response, $this->successStatus);
    }

    public function reportList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        // $packagereport = Package::where('id', Auth::user()->package_id)->where('report', '1')->first();

        // $package_list = Package::where('role',request('role'))->orWhere('role','all')->get(); 

        $service_list = Services::leftJoin('service_packages', 'service_packages.service_id', '=', 'services.id')->leftJoin('user_services', 'user_services.servicepack_id', '=', 'service_packages.id')->where('services.status', 1)->where('services.parent_id', null)->where('service_packages.status', 1)->where('user_services.status', 1)->where('user_services.user_id', $user->id)->where('service_packages.package_id', $user->package_id)->get(['services.id', 'services.service_name', 'services.icon', 'services.url']);
       
        if (!empty($service_list[0])) {
            $response['status'] = 'success';
            $response['message'] = 'Service list available.';
            $response['user_status'] = $user->status;
            $response['description']['report_list'] = $service_list;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Report list not available!';
            $response['user_status'] = $user->status;
        }
        return response()->json($response, $this->successStatus);
    }

    public function updatePassword(Request $request) 
    {
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'password' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $user = User::where('mobile',$request->mobile)->first();
        
        $user->password = bcrypt(request('password'));
        $user->otp = NULL;

        if ($user->save()) {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'Password Change Successfully!';
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Something went wrong';
        }
        
        
        return response()->json($response, $this->successStatus); 
    } 

    public function updatePin(Request $request) 
    {
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'pin' => 'required|numeric', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        $user = User::where('mobile',$request->mobile)->first();
   
        $user->login_pin = request('pin');
        $user->otp = NULL;

        if ($user->save()) {
            $response['status'] = 'success';
            $response['message'] = 'Pin Change Successfully!';
            $response['user_status'] = $user->status;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong';
            $response['user_status'] = $user->status;
        }
       
        
        return response()->json($response, $this->successStatus); 
    }

    public function transactionOtp(Request $request){
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users', 
            'type' => 'required',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        
        $type = request('type');
        $otp = rand(100000, 999999);
        
        // $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access '.$type.' transaction is '.$otp.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone');
        $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access '.$type.' Transaction is '.$otp.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone, Not You? Report https://damskart.com. Regards Damskart Business.');

        $user = User::where('mobile', '=', request('mobile'))->where('role', '!=', 'super_admin')->where('role', '!=', 'admin')->where('role', '!=', 'manager')->first();

        if (!empty($user)) {

            $api_key = '25F6EF10FE18AF';
            $contacts = $user->mobile;
            $from = 'DMSKRT';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207161768584481607&msg=".$sms_text);
            $output = curl_exec($ch);
            
            $user->otp = $otp;
            if ($user->save()) {
                $response['status'] = 'success';
                $response['user_status'] = $user->status;
                $response['message'] = 'OTP Send Successfully!!!';
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Something went wrong';
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Not Registered';
        }
        
        return response()->json($response, $this->successStatus);
    }

    public function addQuery(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric', 
            'name' => 'required',
            'mobile' => 'required|numeric',
            'email' => 'required|email', 
            'query' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error'; 
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }
            
        $addquery = new UserQuery();
        $addquery->user_id = request('user_id');
        $addquery->created_by = $user->created_by;
        $addquery->user_name = request('name');
        $addquery->mobile = request('mobile');
        $addquery->email = request('email');
        $addquery->message = request('query');
        $addquery->status = '1';

        if($addquery->save()){
            $response['status'] = 'success';
            $response['message'] = 'Thanks for contacting us! Your query has been submitted successfully, We will be in touch with you shortly.';
            $response['user_status'] = $user->status;
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong!';
            $response['user_status'] = $user->status;
        }
        
       
        return response()->json($response, $this->successStatus);
    }

    public function queryList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $keyword = request('keyword'); 
        $date = request('date');
         if(request('date') == 'all'){
                $query_list = UserQuery::where('user_id',request('user_id'))->where('mobile', 'like', "%{$keyword}%")->orWhere('email', 'like', "%{$keyword}%")->orWhere('query', 'like', "%{$keyword}%")->orWhere('user_name', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit')); 
            }elseif(!empty($keyword) && !empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $query_list =  UserQuery::where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->where('mobile', 'like', "%{$keyword}%")
                ->orWhere('email', 'like', "%{$keyword}%")
                ->orWhere('query', 'like', "%{$keyword}%") 
                ->orWhere('user_name', 'like', "%{$keyword}%") 
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }elseif (!empty($keyword)) {
               $query_list = UserQuery::where('user_id',request('user_id'))->where('mobile', 'like', "%{$keyword}%")->orWhere('email', 'like', "%{$keyword}%")->orWhere('query', 'like', "%{$keyword}%")->orWhere('user_name', 'like', "%{$keyword}%")->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit')); 
            }elseif(!empty($date)){
                $splitdate = explode(':', $date);
                $startdate = $splitdate[0];
                $enddate = $splitdate[1];

                $query_list =  UserQuery::where('user_id', request('user_id'))
                ->where(function($q) use ($startdate, $enddate){
                    $q->whereDate('created_at', '>=', date('Y-m-d',strtotime($startdate)))
                      ->whereDate('created_at', '<=', date('Y-m-d',strtotime($enddate)));
                })
                ->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit'));
            }else{
                $query_list = UserQuery::where('user_id',request('user_id'))->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit')); 
            }
        if (!empty($query_list[0])) {
            $response['status'] = 'success';
            $response['message'] = 'Query list available.';
            $response['user_status'] = $user->status;
            $response['description']['query_list'] = $query_list;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'You have not submitted any query!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function notificationList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $keyword = request('keyword'); 
        $userrole = $user->role;
        $all = 'all';

        if (!empty($keyword)) {
            $notification_list = Notifications::where(function ($query) use ($userrole, $all) {
                            $query->where('role', '=', $userrole)
                                  ->orWhere('role', '=', $all);
                    })->where(function ($q) use ($keyword) {
                            $q->where('title','like', "%{$keyword}%")
                                  ->orWhere('message','like', "%{$keyword}%");
                    })->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit')); 
        }else{
            $notification_list = Notifications::where(function ($query) use ($userrole, $all) {
                            $query->where('role', '=', $userrole)
                                  ->orWhere('role', '=', $all);
                    })->orderBy('created_at','desc')->get()->splice(request('skip'))->take(request('limit')); 
        }
      
        if (!empty($notification_list[0])) {
            $user->notification = 0;
            $user->save();

            $notificationData = array();

            if($notification_list)
            {
                foreach($notification_list as $row)
                {
                    $row->created_time = date('Y-m-d H:i:s', strtotime($row->created_at));

                    //unset($row->created_at);
                    //$row->created_at = $row->created_time;
                    $notificationData[] = $row;
                }
            }

            $response['status'] = 'success';
            $response['message'] = 'Notifications list available.';
            $response['user_status'] = $user->status;
            $response['description']['notification_list'] = $notificationData;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Notifications list not available!';
        }
        return response()->json($response, $this->successStatus);
    }
    
    // <*****************************************************************************************>
    // <-----------------------------------BBPS API's START-------------------------------------->

    public function bbpsCategoryList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        // $bbps_category_list = Bbps_billers_cats::all(); 

        $bbps_category_list = User_services::leftJoin('service_packages', 'user_services.servicepack_id', '=', 'service_packages.id')->leftJoin('services', 'service_packages.service_id', '=', 'services.id')->selectRaw('service_name as biller_cat_name')->where('user_id', $user->id)->where('services.status', '1')->where('service_packages.package_id', $user->package_id)->where('services.parent_id', '7')->where('user_services.status', '1')->where('service_packages.status', '1')->groupBy('service_packages.service_id')->get();
       
        if (!empty($bbps_category_list[0])) {
            $response['status'] = 'success';
            $response['message'] = 'BBPS Category list available.';
            $response['user_status'] = $user->status;
            $response['description']['bbps_category_list'] = $bbps_category_list;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'BBPS Category list not available!';
        }
        return response()->json($response, $this->successStatus);
    }

    public function bbpsBillerList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'category_name' => 'required', 
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $category_name = request('category_name');
        $keyword = request('keyword');
        if (!empty($keyword)) {
            $bbps_biller_list = Bbps_billers::where('biller_cat_name',$category_name)->where('biller_name', 'like', "%{$keyword}%")->where('biller_alias_name', 'like', "%{$keyword}%")->orderBy('biller_name')->get(['id','biller_cat_name','biller_id','biller_name','biller_alias_name','biller_coverage'])->splice(request('skip'))->take(request('limit'));
        }else{
            $bbps_biller_list = Bbps_billers::where('biller_cat_name',$category_name)->get(['id','biller_cat_name','biller_id','biller_name','biller_alias_name','biller_coverage'])->splice(request('skip'))->take(request('limit')); 
        }
       
        if (!empty($bbps_biller_list[0])) {
            $response['status'] = 'success';
            $response['message'] = 'BBPS Biller list available.';
            $response['user_status'] = $user->status;
            $response['description']['bbps_biller_list'] = $bbps_biller_list;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'BBPS Biller list not available!';
        }
        return response()->json($response, $this->successStatus);
    }
    
    //*********** Encryption Function *********************
    public function encrypt($plainText, $key) {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $secretKey, OPENSSL_RAW_DATA, $initVector);
        $encryptedText = bin2hex($openMode);
        return $encryptedText;
    }
    //*********** Decryption Function *********************
    public function decrypt($encryptedText, $key) {
        $key = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
        return $decryptedText;
    }
    //*********** Padding Function *********************
    public function pkcs5_pad($plainText, $blockSize) {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }
    //********** Hexadecimal to Binary function for php 4.0 version ********
    public function hextobin($hexString) {
        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length) {
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString .= $packedString;
            }
            $count += 2;
        }
        return $binString;
    }
    //********** To generate ramdom String ********
    public function generateRandomString($length = 35) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //bbpsbillerinfo
    public function bbpsbillerinfo(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'billerId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $bbps_biller_details = Bbps_billers::where('biller_id',request('billerId'))->first(); 
        

        // $key = "A549042391A0A97623646364DD5D7150";
        // $encrypt_xml_data = $this->encrypt($plainText, $key);

        // $data['accessCode'] = "AVAF78KX80WT10HOUD";
        // $data['requestId'] = $this->generateRandomString();
        // $data['ver'] = "1.0";
        // $data['instituteId'] = "BA44";

        // $parameters = http_build_query($data);

        // $url = "https://api.billavenue.com/billpay/extMdmCntrl/mdmRequestNew/xml?" . $parameters;
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $encrypt_xml_data);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // $result = curl_exec($ch);

        // $responsedata = $this->decrypt($result, $key);
        // $getdata = htmlentities($responsedata);
        $xml = simplexml_load_string($bbps_biller_details->biller_id_response, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_billerinfo = json_decode($getjson,TRUE);


        if ($bbps_billerinfo) {
            $response['status'] = 'success';
            $response['message'] = 'Biller Details Available.';
            $response['user_status'] = $user->status;
            $param = $bbps_billerinfo['biller']['billerInputParams']['paramInfo'];
            $arr = [];
            $isArray = is_array($param);
            if (is_array($param) && !empty($param[0])) {
                array_push($arr, $param);
                $bbps_billerinfo['biller']['billerInputParams']['paramInfo'] = $param;
            }else{
                array_push($arr, $param);
                $bbps_billerinfo['biller']['billerInputParams']['paramInfo'] = $arr;
            }
            $response['description'] = $bbps_billerinfo;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Biller Details Not Available!';
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsbillFetch
    public function bbpsbillFetch(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'billerId' => 'required', 
        ]);
        
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

       
        $requestdata = request()->all();

        $paramFieldarr = $requestdata['params_list'];
        $plainText = '<billFetchRequest>
           <agentId>CC01BA44AGTU00000001</agentId>
           <agentDeviceInfo>
              <ip>65.0.24.111</ip>
              <initChannel>AGT</initChannel>
              <mac>01-23-45-67-89-ab</mac>
           </agentDeviceInfo>
           <customerInfo>
              <customerMobile>'.$user->mobile.'</customerMobile>
              <customerEmail></customerEmail>
              <customerAdhaar></customerAdhaar>
              <customerPan></customerPan>
           </customerInfo>
           <billerId>'.$requestdata['billerId'].'</billerId>
           <inputParams>';
             for ($i=0; $i < count($paramFieldarr) ; $i++) {
                $plainText .='
              <input>
                 <paramName>'.$paramFieldarr[$i]['key'].'</paramName>
                 <paramValue>'.$paramFieldarr[$i]['value'].'</paramValue>
              </input>';}$plainText .='
           </inputParams>
        </billFetchRequest>';

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_billfetch = json_decode($getjson,TRUE);
        // var_dump($bbps_billfetch);
        // exit;
        if ($bbps_billfetch['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['message'] = 'bbpsbillfetch Success';
            $response['user_id'] = $requestdata['user_id'];
            $response['billerId'] = $requestdata['billerId'];
            $response['requestId'] = $data['requestId'];
            $response['customer_mobile'] = $user->mobile;
            $param = $bbps_billfetch['inputParams']['input'];
            $arr = [];
            $isArray = is_array($param);
            if (is_array($param) && !empty($param[0])) {
                array_push($arr, $param);
                $bbps_billfetch['inputParams']['input'] = $param;
            }else{
                array_push($arr, $param);
                $bbps_billfetch['inputParams']['input'] = $arr;
            }
            $response['description'] = $bbps_billfetch;
            $response['user_status'] = $user->status;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_billfetch['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_billfetch['errorInfo']['error']['errorMessage'];
            }
            // $response['message'] = $bbps_billfetch['errorInfo']['error']['errorMessage'];
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsBillValidation
    public function bbpsBillValidation(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'billerId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

          $user = Auth::user();
          $requestdata = request()->all();
          $biller_id = request('biller_id');
          $plainText = '<billValidationRequest>
                         <agentId>CC01BA44AGTU00000001</agentId>
                         <billerId>'.$requestdata['billerId'].'</billerId>
                         <inputParams>';
                         for ($i=0; $i < count($paramFieldarr) ; $i++) {
                            $plainText .='
                          <input>
                             <paramName>'.$paramFieldarr[$i]['key'].'</paramName>
                             <paramValue>'.$paramFieldarr[$i]['value'].'</paramValue>
                          </input>';}$plainText .='
                         </inputParams>
                        </billValidationRequest>';
                   
          $key = "A549042391A0A97623646364DD5D7150";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVAF78KX80WT10HOUD";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "BA44";
          $parameters = http_build_query($data);
          $url = "https://api.billavenue.com/billpay/extBillValCntrl/billValidationRequest/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);

          // echo $result . "////////////////////";
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);

          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $bbps_BillValidation = json_decode($getjson,TRUE);
       
        if ($bbps_BillValidation['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['message'] = 'Bill Validation Success';
            $response['user_status'] = $user->status;
            $response['description'] = $bbps_BillValidation;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_BillValidation['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_BillValidation['errorInfo']['error']['errorMessage'];
            }
            // $response['message'] = $bbps_BillValidation['errorInfo']['error']['errorMessage'];
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsQuickPay
    public function bbpsQuickPay(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            // 'billerId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $requestdata = request()->all();

        $inputParams = $requestdata['params_list']; 
        $billerId = $requestdata['billerId'];
        $billerdetails = Bbps_billers::where('biller_id',$billerId)->first();
        $biller_name = $billerdetails->biller_name; 
        $customer_mobile = $user->mobile;
        // $requestId = $requestdata['requestId'];
        $payment_mode = "Cash";
        $cust_convfee = 0;
        $amount = ($requestdata['amount'] * 100);
        $total_amount = $requestdata['amount'] + $cust_convfee; 
        
        $plainText = '<billPaymentRequest>
                <agentId>CC01BA44AGTU00000001</agentId>
                <billerAdhoc>true</billerAdhoc>
                <agentDeviceInfo>
                    <ip>65.0.24.111</ip>
                    <initChannel>AGT</initChannel>
                    <mac>01-23-45-67-89-ab</mac>
                </agentDeviceInfo>
                <customerInfo>
                    <customerMobile>'.$customer_mobile.'</customerMobile>
                    <customerEmail></customerEmail>
                    <customerAdhaar></customerAdhaar>
                    <customerPan></customerPan>
                </customerInfo>
                <billerId>'.$billerId.'</billerId>
               <inputParams>';
               for ($i=0; $i < count($inputParams) ; $i++) {
                  $plainText .='
                  <input>
                     <paramName>'.$inputParams[$i]['key'].'</paramName>
                     <paramValue>'.$inputParams[$i]['value'].'</paramValue>
                  </input>';}$plainText .='
               </inputParams>
               <amountInfo>
                   <amount>'.$amount.'</amount>
                   <currency>356</currency>
                   <custConvFee>'.$cust_convfee.'</custConvFee>
                   <amountTags></amountTags>
               </amountInfo>
               <paymentMethod>
                   <paymentMode>'.$payment_mode.'</paymentMode>
                   <quickPay>Y</quickPay>
                   <splitPay>N</splitPay>
               </paymentMethod>
               <paymentInfo>
                   <info>
                       <infoName>Remarks</infoName>
                       <infoValue>Received</infoValue>
                   </info>
               </paymentInfo>
            </billPaymentRequest>';

        $date = date('Y-m-d H:i:s');
        $cryptedText = file_get_contents('php://input');
        $file = public_path().'/logs/BBPS-Prepaid-Transactions.txt';
        $log = "\n\n".'================================(APP)Date - '.$date."================================ \n\n";

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;

        $log .= 'Request ENCRYPTED Data- '.$data['encRequest']."\n\n";
        $log .= 'Request ID- '.$data['requestId']."\n\n";

        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_quickbillpay = json_decode($getjson,TRUE);

        $log .= 'Response XML Data- '.$responsedata."\n\n";
        $log .= 'Response Encoded Data- '.$getjson."\n\n";
        file_put_contents($file, $log, FILE_APPEND | LOCK_EX);

        $unique_id = "DMS".date("Ymd").time();
       
        if ($bbps_quickbillpay['responseCode'] == "000") {
            $txn_dateTime = date('Y-m-d H:i:s');
            $initChannel = "AGT";

            $biller_category = Bbps_billers::where('biller_id',request('billerId'))->first();
            $bbps_billpay = new Bbps_transactions();
            $amount = ($bbps_quickbillpay['RespAmount'] /100);
            
            $bbps_billpay->user_id = $user->id;
            $bbps_billpay->created_by = $user->created_by;
            $bbps_billpay->biller_name = $biller_category->billername;
            $bbps_billpay->biller_id = request('billerId');
            $bbps_billpay->biller_cat_name = $biller_category->biller_cat_name;
            $bbps_billpay->pay_type = "Quick Pay";
            $bbps_billpay->cust_mobile = $customer_mobile;
            $bbps_billpay->payment_mode = $payment_mode;
            $bbps_billpay->txn_date_time = $txn_dateTime;
            $bbps_billpay->dms_txn_id = $unique_id;
            $bbps_billpay->init_channel = $initChannel;

            $bbps_billpay->bbps_txnid = $bbps_quickbillpay['txnRefId'];
            $bbps_billpay->approval_no = $bbps_quickbillpay['approvalRefNumber'];
            $bbps_billpay->cus_name = $bbps_quickbillpay['RespCustomerName'];
            $bbps_billpay->bill_amount = $amount;
            $bbps_billpay->ccf = $bbps_quickbillpay['CustConvFee'];
            $bbps_billpay->payment_status = $bbps_quickbillpay['responseReason'];
            $bbps_billpay->message = $bbps_quickbillpay['responseReason'];

            $bbps_billpay->bbps_request = json_encode($inputParams);
            $bbps_billpay->bbps_response = json_encode($bbps_quickbillpay);

            // $bbps_billpay->commission = $bbps_quickbillpay[''];
            // $bbps_billpay->current_balance = $bbps_quickbillpay[''];
            // $bbps_billpay->save();
            if ($bbps_billpay->save()) {
              $usercommission = Bbps_commissions::where('bbps_commissions.package_id',$user->package_id)->where('bbps_commissions.bbps_biller_id',$bbps_billpay->biller_id)->first();
              if (!empty($usercommission)) {
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay->biller_cat_name." Success. Info: ".$bbps_billpay->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();

                  $datacr = new User_transactions();
                  $datacr->user_id = $user->id;
                  $datacr->created_by = $user->created_by;
                  $datacr->transaction_id = $unique_id;
                  $datacr->label = 'ewallet';
                  $datacr->status = $bbps_billpay->payment_status;
                  $datacr->date = date('Y-m-d H:i:s');
                  $datacr->type = "credit";
                  $datacr->narration = "Bill Payment - ".$bbps_billpay->biller_cat_name." Comm. Info: ".$bbps_billpay->bbps_txnid;

                  if ($usercommission->commission_type == "flat") {
                      $comamount = $usercommission->commission;
                      $datacr->amount = $usercommission->commission;
                      $datacr->current_balance = $user->ewallet + $usercommission->commission;
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();
                      $datacr->save();
                  }elseif ($usercommission->commission_type == "percentage") {
                      $comamount = ($bbps_billpay->bill_amount * $usercommission->commission)/100;
                      $datacr->amount = ($bbps_billpay->bill_amount * $usercommission->commission)/100;
                      $datacr->current_balance = $user->ewallet + (($bbps_billpay->bill_amount * $usercommission->commission)/100);
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();  
                      $datacr->save();
                  }
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay->bbps_txnid)->first();
                  $bbpstxncom->commission = $comamount;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }else{
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay->biller_cat_name." Success. Info: ".$bbps_billpay->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();
                  $data1->save();
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay->bbps_txnid)->first();
                  $bbpstxncom->commission = 0;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }
                  
            }
            // $mobile = $user->mobile;
            //   $sms_text = urlencode('Thank you for payment of '.number_format(1000, 2).' against OTNS, Consumer no 9898990084 Txn Ref ID '.$bbps_billpayrequest['txnRefId'].' on '.date('Y-m-d H:i:s').' vide '.$payment_mode);
            // $api_key = '25F6EF10FE18AF';
            // $contacts = $mobile;
            // $from = 'DMSKRT';
            // $ch = curl_init();
            // curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
            // $output = curl_exec($ch);

            $response['status'] = 'success';
            $response['message'] = 'Thanks for your bill payment. It may take 2-3 business days to reflect payment with the billers portal.';                                                                                             
            $response['biller_id'] = $billerId;
            $response['payment_channel'] = 'AGT';
            $response['date_time'] = date('Y-m-d H:i:s');
            $response['biller_name'] = $biller_name;
            $response['total_amount'] = $total_amount;
            $response['description'] = $bbps_quickbillpay;
            $response['user_status'] = $user->status;
            return response()->json($response, $this->successStatus);
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_quickbillpay['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_quickbillpay['errorInfo']['error']['errorMessage'];
            }
            // $response['message'] = $bbps_quickbillpay['errorInfo']['error']['errorMessage'];
            return response()->json($response, $this->successStatus);
        }
    }
    
    //bbpsbillPayRequest
    public function bbpsbillPayRequest(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            // 'billerId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
       $requestdata = request()->all();
       $inputParams = $requestdata['description']['inputParams']['input']; 
        $amountOptions = $requestdata['description']['billerResponse']['amountOptions']['option'];
        
       if (count($amountOptions) == 0) {
        $amountOptions = NULL;   
       }else{
        $amountOptions = $requestdata['description']['billerResponse']['amountOptions']['option'];
       }
        $additionalInfo = $requestdata['description']['additionalInfo']['info'];
       if (count($additionalInfo) == 0) {
        $additionalInfo = NULL;   
       }else{
        $additionalInfo = $requestdata['description']['additionalInfo']['info'];
       }
       
       $billAmount = $requestdata['description']['billerResponse']['billAmount'];
       $billDate = $requestdata['description']['billerResponse']['billDate'];
       $billNumber = $requestdata['description']['billerResponse']['billNumber'];
       $billPeriod = $requestdata['description']['billerResponse']['billPeriod'];
       $customerName = $requestdata['description']['billerResponse']['customerName'];
       $dueDate = $requestdata['description']['billerResponse']['dueDate'];
       $billerId = $requestdata['billerId'];
       
       $cust_mobile = $requestdata['customer_mobile'];
       $requestId = $requestdata['requestId'];
       $payment_mode = $requestdata['payment_mode'];
       $cust_convfee = $requestdata['cust_convfee'];
        $billerdetails = Bbps_billers::where('biller_id',$billerId)->first();
        $biller_name = $billerdetails->biller_name; 
        $total_amount = (($billAmount/100) + $cust_convfee) * 100; 

       $plainText = '<billPaymentRequest>
                    <agentId>CC01BA44AGTU00000001</agentId>
                    <billerAdhoc>true</billerAdhoc>
                    <agentDeviceInfo>
                        <ip>104.131.109.233</ip>
                        <initChannel>AGT</initChannel>
                        <mac>01-23-45-67-89-ab</mac>
                    </agentDeviceInfo>
                    <customerInfo>
                        <customerMobile>'.$cust_mobile.'</customerMobile>
                        <customerEmail></customerEmail>
                        <customerAdhaar></customerAdhaar>
                        <customerPan></customerPan>
                    </customerInfo>
                    <billerId>'.$billerId.'</billerId>
                   <inputParams>';
                   for ($i=0; $i < count($inputParams) ; $i++) {
                      $plainText .='
                      <input>
                         <paramName>'.$inputParams[$i]['paramName'].'</paramName>
                         <paramValue>'.$inputParams[$i]['paramValue'].'</paramValue>
                      </input>';}$plainText .='
                   </inputParams>
                   <billerResponse>
                        <billAmount>'.$billAmount.'</billAmount>
                        <billDate>'.$billDate.'</billDate>
                        <billNumber>'.$billNumber.'</billNumber>
                        <billPeriod>'.$billPeriod.'</billPeriod>
                        <customerName>'.$customerName.'</customerName>
                        <dueDate>'.$dueDate.'</dueDate>';
                           if ($amountOptions == NULL) {
                              $plainText .='<amountOptions />';}else{$plainText .='
                        <amountOptions>';
                           for ($j=0; $j < count($amountOptions) ; $j++) {
                              $plainText .='
                            <option>
                                <amountName>'.$amountOptions[$j]['amountName'].'</amountName>
                                <amountValue>'.$amountOptions[$j]['amountValue'].'</amountValue>
                            </option>';}$plainText .='
                        </amountOptions>';}$plainText .='
                    </billerResponse>';
                           if ($additionalInfo == NULL) {
                              $plainText .='<additionalInfo />';}else{$plainText .='
                    <additionalInfo>';
                   for ($k=0; $k < count($additionalInfo) ; $k++) {
                      $plainText .='
                        <info>
                            <infoName>'.$additionalInfo[$k]['infoName'].'</infoName>
                            <infoValue>'.$additionalInfo[$k]['infoValue'].'</infoValue>
                        </info>';}$plainText .='
                    </additionalInfo>';}$plainText .='
                    <amountInfo>
                        <amount>'.$total_amount.'</amount>
                        <currency>356</currency>
                        <custConvFee>'.$cust_convfee.'</custConvFee>
                        <amountTags></amountTags>
                    </amountInfo>
                    <paymentMethod>
                        <paymentMode>'.$payment_mode.'</paymentMode>
                        <quickPay>N</quickPay>
                        <splitPay>N</splitPay>
                    </paymentMethod>
                    <paymentInfo>
                        <info>
                            <infoName>Remarks</infoName>
                            <infoValue>Received</infoValue>
                        </info>
                    </paymentInfo>
                </billPaymentRequest>';
        
        $date = date('Y-m-d H:i:s');
        $cryptedText = file_get_contents('php://input');
        $file = public_path().'/logs/BBPS-FetchPay-Transactions.txt';
        $log = "\n\n".'================================(APP)Date - '.$date."================================ \n\n";

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $requestId;
        $data['encRequest'] = $encrypt_xml_data;
        
        $log .= 'Request ENCRYPTED Data- '.$data['encRequest']."\n\n";
        $log .= 'Request ID- '.$data['requestId']."\n\n";
        
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);


        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_billpayrequest = json_decode($getjson,TRUE);

        $log .= 'Response XML Data- '.$responsedata."\n\n";
        $log .= 'Response Encoded Data- '.$getjson."\n\n";
        file_put_contents($file, $log, FILE_APPEND | LOCK_EX);

        $unique_id = "DMS".date("Ymd").time();

        if ($bbps_billpayrequest['responseCode'] == "000") {
            $biller_category = Bbps_billers::where('biller_id',request('billerId'))->first();
            $amount = ($bbps_billpayrequest['RespAmount'] /100);

            $bbps_billpay_fnp = new Bbps_transactions();

            $bbps_billpay_fnp->bbps_request = json_encode($inputParams);
            $bbps_billpay_fnp->bbps_response = json_encode($bbps_billpayrequest);
            $bbps_billpay_fnp->user_id = $user->id;
            $bbps_billpay_fnp->created_by = $user->created_by;
            $bbps_billpay_fnp->biller_name = $billername;
            $bbps_billpay_fnp->biller_id = request('billerId');
            $bbps_billpay_fnp->biller_cat_name = $biller_category->biller_cat_name;
            $bbps_billpay_fnp->pay_type = "Fetch&Pay";
            $bbps_billpay_fnp->cust_mobile = $customer_mobile;
            $bbps_billpay_fnp->payment_mode = $billerpaymentModes;
            $bbps_billpay_fnp->txn_date_time = $txn_dateTime;
            $bbps_billpay_fnp->dms_txn_id = $unique_id;
            $bbps_billpay_fnp->init_channel = $initChannel;
            $bbps_billpay_fnp->bbps_txnid = $bbps_billpayrequest['txnRefId'];
            $bbps_billpay_fnp->approval_no = $bbps_billpayrequest['approvalRefNumber'];
            $bbps_billpay_fnp->cus_name = $bbps_billpayrequest['RespCustomerName'];
            $bbps_billpay_fnp->bill_period = $bbps_billpayrequest['RespBillPeriod'];
            $bbps_billpay_fnp->bill_number = $bbps_billpayrequest['RespBillNumber'];
            $bbps_billpay_fnp->bill_due_date = $bbps_billpayrequest['RespDueDate'];
            $bbps_billpay_fnp->bill_date = $bbps_billpayrequest['RespBillDate'];
            $bbps_billpay_fnp->bill_amount = $amount;
            $bbps_billpay_fnp->ccf = $bbps_billpayrequest['CustConvFee'];
            $bbps_billpay_fnp->payment_status = $bbps_billpayrequest['responseReason'];
            $bbps_billpay_fnp->message = $bbps_billpayrequest['responseReason'];
            // $bbps_billpay_fnp->save();
            if ($bbps_billpay_fnp->save()) {
              $usercommission = Bbps_commissions::where('bbps_commissions.package_id',$user->package_id)->where('bbps_commissions.bbps_biller_id',$bbps_billpay_fnp->biller_id)->first();
              if (!empty($usercommission)) {
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay_fnp->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay_fnp->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay_fnp->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay_fnp->biller_cat_name." Success. Info: ".$bbps_billpay_fnp->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();

                  $datacr = new User_transactions();
                  $datacr->user_id = $user->id;
                  $datacr->created_by = $user->created_by;
                  $datacr->transaction_id = $unique_id;
                  $datacr->label = 'ewallet';
                  $datacr->status = $bbps_billpay_fnp->payment_status;
                  $datacr->date = date('Y-m-d H:i:s');
                  $datacr->type = "credit";
                  $datacr->narration = "Bill Payment - ".$bbps_billpay_fnp->biller_cat_name." Comm. Info: ".$bbps_billpay_fnp->bbps_txnid;

                  if ($usercommission->commission_type == "flat") {
                      $comamount = $usercommission->commission;
                      $datacr->amount = $usercommission->commission;
                      $datacr->current_balance = $user->ewallet + $usercommission->commission;
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();
                      $datacr->save();
                  }elseif ($usercommission->commission_type == "percentage") {
                      $comamount = ($bbps_billpay_fnp->bill_amount * $usercommission->commission)/100;
                      $datacr->amount = ($bbps_billpay_fnp->bill_amount * $usercommission->commission)/100;
                      $datacr->current_balance = $user->ewallet + (($bbps_billpay_fnp->bill_amount * $usercommission->commission)/100);
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();  
                      $datacr->save();
                  }
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay_fnp->bbps_txnid)->first();
                  $bbpstxncom->commission = $comamount;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }else{
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay_fnp->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay_fnp->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay_fnp->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay_fnp->biller_cat_name." Success. Info: ".$bbps_billpay_fnp->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();
                  $data1->save();
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay_fnp->bbps_txnid)->first();
                  $bbpstxncom->commission = 0;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }
                  
            }
            // $mobile = $user->mobile;
            //   $sms_text = urlencode('Thank you for payment of '.number_format(1000, 2).' against OTNS, Consumer no 9898990084 Txn Ref ID '.$bbps_billpayrequest['txnRefId'].' on '.date('Y-m-d H:i:s').' vide '.$payment_mode);
            // $api_key = '25F6EF10FE18AF';
            // $contacts = $mobile;
            // $from = 'DMSKRT';
            // $ch = curl_init();
            // curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
            // $output = curl_exec($ch);

            $response['status'] = 'success';
            $response['message'] = 'Thanks for your bill payment. It may take 2-3 business days to reflect payment with the billers portal.';
             $response['user_id'] = $requestdata['user_id'];
            $response['biller_id'] = $billerId;
            $response['payment_channel'] = 'AGT';
            $response['date_time'] = date('Y-m-d H:i:s');
            $response['biller_name'] = $biller_name;
            $response['total_amount'] = $total_amount;
            $response['user_status'] = $user->status;
            $response['description'] = $bbps_billpayrequest;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_billpayrequest['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_billpayrequest['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsRechargePlan
    public function bbpsRechargePlan(Request $request){

        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'billerId' => 'required', 
            'circle' => 'required', 
        ]);
        
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

       
        $requestdata = request()->all();
        $billerId = request('billerId');
        $circle = request('circle');
        $plainText = '<rechargePlanRequest>
                        <billerId>'.$billerId.'</billerId>
                        <circle>'.$circle.'</circle>
                        </rechargePlanRequest>
                    ';

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extFetchPlans/fetchPlansRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_recharge_plan = json_decode($getjson,TRUE);
        // var_dump($bbps_recharge_plan);
        // exit;
        if ($bbps_recharge_plan['responseCode'] == "000") {
            
            $response['status'] = 'success';
            $response['message'] = 'Plans Available. ';
            $response['user_status'] = $user->status;
            // $response['customer_mobile'] = request('customer_mobile');
            $param = $bbps_recharge_plan['rechargePlan']['rechargePlansDetails'];
            $arr = [];
            $isArray = is_array($param);
            if (is_array($param) && !empty($param[0])) {
                array_push($arr, $param);
                $bbps_recharge_plan['rechargePlan']['rechargePlansDetails'] = $param;
            }else{
                array_push($arr, $param);
                $bbps_recharge_plan['rechargePlan']['rechargePlansDetails'] = $arr;
            }
            $response['description'] = $bbps_recharge_plan;
        }else{

            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            // $response['message'] = $bbps_recharge_plan['errorInfo']['error']['errorMessage'];
            $err = $bbps_recharge_plan['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_recharge_plan['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsPrepaidPay
    public function bbpsPrepaidPay(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            // 'billerId' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $requestdata = request()->all();
        $inputParams = $requestdata['description']['inputParams']['input']; 
        $billerId = $requestdata['billerId'];
        $billerdetails = Bbps_billers::where('biller_id',$billerId)->first();
        $biller_name = $billerdetails->biller_name; 
        $customer_mobile = $requestdata['customer_mobile'];
        // $requestId = $requestdata['requestId'];
        $payment_mode = $requestdata['payment_mode'];
        $cust_convfee = $requestdata['cust_convfee'];
        $amount = ($requestdata['amount'] * 100);
        $total_amount = $requestdata['amount'] + $cust_convfee; 
        
         
        $plainText = '<billPaymentRequest>
                <agentId>CC01BA44AGTU00000001</agentId>
                <billerAdhoc>true</billerAdhoc>
                <agentDeviceInfo>
                    <ip>104.131.109.233</ip>
                    <initChannel>AGT</initChannel>
                    <mac>01-23-45-67-89-ab</mac>
                </agentDeviceInfo>
                <customerInfo>
                    <customerMobile>'.$customer_mobile.'</customerMobile>
                    <customerEmail></customerEmail>
                    <customerAdhaar></customerAdhaar>
                    <customerPan></customerPan>
                </customerInfo>
                <billerId>'.$billerId.'</billerId>
               <inputParams>';
               for ($i=0; $i < count($inputParams) ; $i++) {
                  $plainText .='
                  <input>
                     <paramName>'.$inputParams[$i]['key'].'</paramName>
                     <paramValue>'.$inputParams[$i]['value'].'</paramValue>
                  </input>';}$plainText .='
               </inputParams>
               <amountInfo>
                   <amount>'.$amount.'</amount>
                   <currency>356</currency>
                   <custConvFee>'.$cust_convfee.'</custConvFee>
                   <amountTags></amountTags>
               </amountInfo>
               <paymentMethod>
                   <paymentMode>'.$payment_mode.'</paymentMode>
                   <quickPay>Y</quickPay>
                   <splitPay>N</splitPay>
               </paymentMethod>
               <paymentInfo>
                   <info>
                       <infoName>Remarks</infoName>
                       <infoValue>Received</infoValue>
                   </info>
               </paymentInfo>
            </billPaymentRequest>';

        $date = date('Y-m-d H:i:s');
        $cryptedText = file_get_contents('php://input');
        $file = public_path().'/logs/BBPS-Prepaid-Transactions.txt';
        $log = "\n\n".'================================(APP)Date - '.$date."================================ \n\n";

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;

        $log .= 'Request ENCRYPTED Data- '.$data['encRequest']."\n\n";
        $log .= 'Request ID- '.$data['requestId']."\n\n";
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_prepaidpay = json_decode($getjson,TRUE);

        $log .= 'Response XML Data- '.$responsedata."\n\n";
        $log .= 'Response Encoded Data- '.$getjson."\n\n";
        file_put_contents($file, $log, FILE_APPEND | LOCK_EX);

        $unique_id = "DMS".date("Ymd").time();
       
        if ($bbps_prepaidpay['responseCode'] == "000") {

            $biller_category = Bbps_billers::where('biller_id',request('billerId'))->first();
            $bbps_billpay = new Bbps_transactions();
            $amount = ($bbps_prepaidpay['RespAmount'] /100);
            
            $bbps_billpay->user_id = $user->id;
            $bbps_billpay->created_by = $user->created_by;
            $bbps_billpay->biller_name = $billername;
            $bbps_billpay->biller_id = request('billerId');
            $bbps_billpay->biller_cat_name = $biller_category->biller_cat_name;
            $bbps_billpay->pay_type = "Quick Pay";
            $bbps_billpay->cust_mobile = $customer_mobile;
            $bbps_billpay->payment_mode = $quickpay_mode;
            $bbps_billpay->txn_date_time = $txn_dateTime;
            $bbps_billpay->dms_txn_id = $unique_id;
            $bbps_billpay->init_channel = $initChannel;

            $bbps_billpay->bbps_txnid = $bbps_prepaidpay['txnRefId'];
            $bbps_billpay->approval_no = $bbps_prepaidpay['approvalRefNumber'];
            $bbps_billpay->cus_name = $bbps_prepaidpay['RespCustomerName'];
            $bbps_billpay->bill_amount = $amount;
            $bbps_billpay->ccf = $bbps_prepaidpay['CustConvFee'];
            $bbps_billpay->payment_status = $bbps_prepaidpay['responseReason'];
            $bbps_billpay->message = $bbps_prepaidpay['responseReason'];

            $bbps_billpay->bbps_request = json_encode($inputParams);
            $bbps_billpay->bbps_response = json_encode($bbps_prepaidpay);

            if ($bbps_billpay->save()) {
              $usercommission = Bbps_commissions::where('bbps_commissions.package_id',$user->package_id)->where('bbps_commissions.bbps_biller_id',$bbps_billpay->biller_id)->first();
              if (!empty($usercommission)) {
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay->biller_cat_name." Success. Info: ".$bbps_billpay->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();

                  $datacr = new User_transactions();
                  $datacr->user_id = $user->id;
                  $datacr->created_by = $user->created_by;
                  $datacr->transaction_id = $unique_id;
                  $datacr->label = 'ewallet';
                  $datacr->status = $bbps_billpay->payment_status;
                  $datacr->date = date('Y-m-d H:i:s');
                  $datacr->type = "credit";
                  $datacr->narration = "Bill Payment - ".$bbps_billpay->biller_cat_name." Comm. Info: ".$bbps_billpay->bbps_txnid;

                  if ($usercommission->commission_type == "flat") {
                      $comamount = $usercommission->commission;
                      $datacr->amount = $usercommission->commission;
                      $datacr->current_balance = $user->ewallet + $usercommission->commission;
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();
                      $datacr->save();
                  }elseif ($usercommission->commission_type == "percentage") {
                      $comamount = ($bbps_billpay->bill_amount * $usercommission->commission)/100;
                      $datacr->amount = ($bbps_billpay->bill_amount * $usercommission->commission)/100;
                      $datacr->current_balance = $user->ewallet + (($bbps_billpay->bill_amount * $usercommission->commission)/100);
                        $user->ewallet = $datacr->current_balance;
                        $user->save();
                      $data1->save();  
                      $datacr->save();
                  }
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay->bbps_txnid)->first();
                  $bbpstxncom->commission = $comamount;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }else{
                  $data1 = new User_transactions();
                  $data1->user_id = $user->id;
                  $data1->created_by = $user->created_by;
                  $data1->transaction_id = $unique_id;
                  $data1->label = 'ewallet';
                  $data1->status = $bbps_billpay->payment_status;
                  $data1->date = date('Y-m-d H:i:s');
                  $data1->amount = $bbps_billpay->bill_amount;
                  $data1->current_balance = $user->ewallet - $bbps_billpay->bill_amount;
                  $data1->type = "debit";
                  $data1->narration =  "Bill Payment - ".$bbps_billpay->biller_cat_name." Success. Info: ".$bbps_billpay->bbps_txnid;

                    $user->ewallet = $data1->current_balance;
                    $user->save();
                  $data1->save();
                  $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay->bbps_txnid)->first();
                  $bbpstxncom->commission = 0;
                  $bbpstxncom->current_balance = $user->ewallet;
                  $bbpstxncom->save();
              }
                  
            }

            // $mobile = $user->mobile;
            //   $sms_text = urlencode('Thank you for payment of '.number_format(1000, 2).' against OTNS, Consumer no 9898990084 Txn Ref ID '.$bbps_billpayrequest['txnRefId'].' on '.date('Y-m-d H:i:s').' vide '.$payment_mode);
            // $api_key = '25F6EF10FE18AF';
            // $contacts = $mobile;
            // $from = 'DMSKRT';
            // $ch = curl_init();
            // curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
            // $output = curl_exec($ch);

            $response['status'] = 'success';
            $response['message'] = 'Payment Successful.';                                                                                             
            $response['biller_id'] = $billerId;
            $response['payment_channel'] = 'AGT';
            $response['date_time'] = date('Y-m-d H:i:s');
            $response['biller_name'] = $biller_name;
            $response['total_amount'] = $total_amount;
            $response['description'] = $bbps_prepaidpay;
            $response['user_status'] = $user->status;
            return response()->json($response, $this->successStatus);
        }elseif ($bbps_prepaidpay['responseCode'] == "900") {
              $biller_category = Bbps_billers::where('biller_id',request('billerId'))->first();
              $bbps_billpay = new Bbps_transactions();
              
              $bbps_billpay->user_id = $user->id;
              $bbps_billpay->created_by = $user->created_by;
              $bbps_billpay->biller_name = $billername;
              $bbps_billpay->biller_id = request('billerId');
              $bbps_billpay->biller_cat_name = $biller_category->biller_cat_name;
              $bbps_billpay->pay_type = "Quick Pay";
              $bbps_billpay->cust_mobile = $customer_mobile;
              $bbps_billpay->payment_mode = $quickpay_mode;
              $bbps_billpay->txn_date_time = $txn_dateTime;
              $bbps_billpay->dms_txn_id = $unique_id;
              $bbps_billpay->init_channel = $initChannel;

              $bbps_billpay->bbps_txnid = $bbps_prepaidpay['txnRefId'];
              $bbps_billpay->bill_amount = request('amount');
              $bbps_billpay->ccf = request('quickpay_ccfamount');
              $bbps_billpay->payment_status = "Pending";
              $bbps_billpay->message = $bbps_prepaidpay['responseReason'];

              $bbps_billpay->bbps_request = json_encode($inputParams);
              $bbps_billpay->bbps_response = json_encode($bbps_prepaidpay);

              if ($bbps_billpay->save()) {
                $usercommission = Bbps_commissions::where('bbps_commissions.package_id',$user->package_id)->where('bbps_commissions.bbps_biller_id',$bbps_billpay->biller_id)->first();
                    $data1 = new User_transactions();
                    $data1->user_id = $user->id;
                    $data1->created_by = $user->created_by;
                    $data1->transaction_id = $unique_id;
                    $data1->label = 'ewallet';
                    $data1->status = $bbps_billpay->payment_status;
                    $data1->date = date('Y-m-d H:i:s');
                    $data1->amount = $bbps_billpay->bill_amount;
                    $data1->current_balance = $user->ewallet - $bbps_billpay->bill_amount;
                    $data1->type = "debit";
                    $data1->narration =  "Bill Payment - ".$bbps_billpay->biller_cat_name." Pending. Info: ".$bbps_billpay->bbps_txnid;
                      $user->ewallet = $data1->current_balance;
                      $user->save();
                    $data1->save();

                    $bbpstxncom = Bbps_transactions::where('bbps_txnid',$bbps_billpay->bbps_txnid)->first();
                    $bbpstxncom->current_balance = $user->ewallet;
                    $bbpstxncom->save();

              }

                $response['status'] = 'success';
                $response['message'] = 'Payment Pending!';                                                                                             
                $response['biller_id'] = $billerId;
                $response['payment_channel'] = 'AGT';
                $response['date_time'] = date('Y-m-d H:i:s');
                $response['biller_name'] = $biller_name;
                $response['total_amount'] = $total_amount;
                $response['user_status'] = $user->status;
                $response['description'] = $bbps_prepaidpay;
                return response()->json($response, $this->successStatus);
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_prepaidpay['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_prepaidpay['errorInfo']['error']['errorMessage'];
            }
            return response()->json($response, $this->successStatus);
        }
       
    }

    //bbpscomplaintOtp
    public function complaintOtp(Request $request){
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required|max:10|min:10|starts_with:6,7,8,9|exists:users', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        
        $otp = rand(100000, 999999);
        // $sms_text = urlencode('Otp for your BBPS complaint  Registration is '.$otp.' This is valid for 1 hour');
        $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access Complaint  Registration Transaction is '.$otp.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone, Not You? Report https://damskart.com. Regards Damskart Business.');

        $user = User::where('mobile', '=', request('mobile'))->where('role', '!=', 'super_admin')->where('role', '!=', 'admin')->where('role', '!=', 'manager')->first();

        if (!empty($user)) {
            $api_key = '25F6EF10FE18AF';
            $contacts = $user->mobile;
            $from = 'DMSKRT';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207161768584481607&msg=".$sms_text);
            $output = curl_exec($ch);
            $user->otp = $otp;
            if ($user->save()) {
                $response['status'] = 'success';
                $response['user_status'] = $user->status;
                $response['message'] = 'Otp has been sent to your mobile '.request('mobile');
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $response['message'] = 'Something went wrong';
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Not Registered';
        }
        
        return response()->json($response, $this->successStatus);
    }

    //bbpsverifyComplaintOtp
    public function verifyComplaintOtp(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'otp' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);            
        }
        if ($user->mobile == request('mobile') && $user->otp == request('otp')) {
            $user->otp = NULL;
            $user->save();
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = "verify Successfully!";
        }
        else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = "Otp doesn't match...";
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsComplaintRegistration
    public function bbpsComplaintRegistration(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'complaint_type' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

       // $plainText = '<complaintRegistrationReq>
       //              <complaintType>Transaction</complaintType>
       //                 <participationType />
       //                 <agentId />
       //                 <txnRefId>CC011033AAB000043559</txnRefId>
       //                 <billerId />
       //                 <complaintDesc>Complaint initiated through API</complaintDesc>
       //                 <servReason />
       //                 <complaintDisposition>Transaction Successful, account not updated</complaintDisposition>
       //              </complaintRegistrationReq>';

        $complaint_type = request('complaint_type');
        if ($complaint_type == "Transaction") {
            $txnRefId = request('txn_ref_id');
            $complaintDesc = request('complaint_Desc');
            $complaintDisposition = request('complaint_Disposition');
            $plainText = '<complaintRegistrationReq>
                          <complaintType>'.$complaint_type.'</complaintType>
                             <participationType />
                             <agentId />
                             <txnRefId>'.$txnRefId.'</txnRefId>
                             <billerId />
                             <complaintDesc>'.$complaintDesc.'</complaintDesc>
                             <servReason />
                             <complaintDisposition>'.$complaintDisposition.'</complaintDisposition>
                          </complaintRegistrationReq>';
        } else if($complaint_type == "Service"){
            $service_participation_type = request('service_participation_type');
            $service_agent_id = request('service_agent_id');
            $service_biller_id = request('service_biller_id');
            $service_reason = request('service_reason');
            $service_complaint_Desc = request('service_complaint_Desc');

            $plainText = '<complaintRegistrationReq>
                          <complaintType>'.$complaint_type.'</complaintType>
                          <participationType>'.$service_participation_type.'</participationType>';if($service_participation_type == "AGENT") {$plainText .='
                          <agentId>'.$service_agent_id.'</agentId>
                          <txnRefId />
                          <billerId />';}else{$plainText .='
                          </agentId>
                          <txnRefId />
                          <billerId>'.$service_biller_id.'<billerId />';}$plainText .='
                          <complaintDesc>'.$service_complaint_Desc.'</complaintDesc>
                          <servReason>'.$service_reason.'</servReason>
                          <complaintDisposition />
                          </complaintRegistrationReq>';
        }
          

        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/extComplaints/register/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        // print_r($getjson);
        // exit;
        $bbps_ComplaintRegistration = json_decode($getjson,TRUE);
       
        if ($bbps_ComplaintRegistration['responseCode'] == "000") {

            $mobile = "7339963285";
            // $sms_text = urlencode('Your Complaint has been registered successfully for Txn Ref ID '.$txnRefId.'. Your Complaint ID is '.$bbps_ComplaintRegistration['complaintId'].'. You can track status of your complaint using your Complaint ID.');
            $sms_text = urlencode('Your Customer, Your Complaint has been registered successfully for Txn Ref ID '.$txnRefId.'. Your Complaint ID is '.$bbps_ComplaintRegistration['complaintId'].'. You can track status of your complaint using your Complaint ID. Thank you for connecting with Damskart Business.');
            $api_key = '25F6EF10FE18AF';
            $contacts = $mobile;
            $from = 'DMSKRT';
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207161751976522510&msg=".$sms_text);
            $output = curl_exec($ch);
          

            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'Your Complaint has been registered successfully for Txn Ref ID '.$txnRefId.'. Your Complaint ID is '.$bbps_ComplaintRegistration['complaintId'].'. You can track status of your complaint using your Complaint ID.';
            $response['description'] = $bbps_ComplaintRegistration;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_ComplaintRegistration['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_ComplaintRegistration['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }

    //bbpsComplaintTracking
    public function bbpsComplaintTracking(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'complaint_type' => 'required', 
            'complaint_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

          $user = Auth::user();
          $complaintType = request('complaint_type');
          $complaint_id = request('complaint_id');
          $plainText = '<complaintTrackingReq>
                         <complaintType>'.$complaintType.'</complaintType>
                         <complaintId>'.$complaint_id.'</complaintId>
                        </complaintTrackingReq>';
                   
          $key = "A549042391A0A97623646364DD5D7150";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVAF78KX80WT10HOUD";
          $data['requestId'] = $this->generateRandomString();

          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "BA44";
          $parameters = http_build_query($data);
          $url = "https://api.billavenue.com/billpay/extComplaints/track/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);

          // echo $result . "////////////////////";
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // $responsedata = "<complaintTrackingResp><complaintAssigned>XYZ Bank</complaintAssigned><complaintId>CC0121033899499</complaintId><complaintRemarks>Complaint initiated through API</complaintRemarks><responseCode>000</responseCode><responseReason>SUCCESS</responseReason><complaintStatus>ASSIGNED</complaintStatus></complaintTrackingResp>";
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $bbps_ComplaintTracking = json_decode($getjson,TRUE);
       
        if ($bbps_ComplaintTracking['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['message'] = 'Complaint Tracking Success';
            $response['user_status'] = $user->status;
            $response['description'] = $bbps_ComplaintTracking;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_ComplaintTracking['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_ComplaintTracking['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //bbpsTransactionStatus
    public function bbpsTransactionStatus(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'track_type' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $trackType = request('track_type');
        if ($trackType == "TRANS_REF_ID") {
            $txn_ref_id = request('txn_ref_id');
            $plainText = '<transactionStatusReq>
                         <trackType>TRANS_REF_ID</trackType>
                         <trackValue>'.$txn_ref_id.'</trackValue>
                        </transactionStatusReq>';
        }elseif ($trackType == "MOBILE_NO") {
            $mobile_number = request('mobile');
            // $start_date = date('Y-m-d',strtotime(request('start_date')));
            // $end_date = date('Y-m-d',strtotime(request('end_date')));
            // $searchtype = "MOBILE_NO";
            $start_date = request('start_date');
            $end_date = request('end_date');
            $plainText = '<transactionStatusReq>
                          <trackType>MOBILE_NO</trackType>
                          <trackValue>'.$mobile_number.'</trackValue>
                          <fromDate>'.$start_date.'</fromDate>
                          <toDate>'.$end_date.'</toDate>
                          </transactionStatusReq>';
        }
        $key = "A549042391A0A97623646364DD5D7150";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVAF78KX80WT10HOUD";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "BA44";
        $parameters = http_build_query($data);
        $url = "https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);

        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $bbps_TransactionStatus = json_decode($getjson,TRUE);
      
        if ($bbps_TransactionStatus['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'TransactionStatus Success';
            $response['description'] = $bbps_TransactionStatus;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $bbps_TransactionStatus['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $bbps_TransactionStatus['errorInfo']['error']['errorMessage'];
            }
            // $response['message'] = $bbps_TransactionStatus['errorInfo']['error']['errorMessage'];
        }
        return response()->json($response, $this->successStatus);
    }
    
    //bbpsComplaintTracking
    public function bbpsDepositeEnquiry(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            // 'complaint_track' => 'required', 
            // 'complaint_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

          $user = Auth::user();
          $plainText = '<depositDetailsRequest>
                        <fromDate>2017-08-22</fromDate>
                        <toDate>2017-09-22</toDate>
                        <transType>DR</transType>
                        <agents>
                        <agentId>CC01BA44AGTU00000001</agentId>
                        </agents>
                        </depositDetailsRequest>';
                   
          $key = "A549042391A0A97623646364DD5D7150";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVAF78KX80WT10HOUD";
          $data['requestId'] = $this->generateRandomString();

          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "BA44";
          $parameters = http_build_query($data);
          $url = "https://api.billavenue.com/billpay/enquireDeposit/fetchDetails/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);

          // echo $result . "////////////////////";
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // $responsedata = "<complaintTrackingResp><complaintAssigned>XYZ Bank</complaintAssigned><complaintId>CC0121033899499</complaintId><complaintRemarks>Complaint initiated through API</complaintRemarks><responseCode>000</responseCode><responseReason>SUCCESS</responseReason><complaintStatus>ASSIGNED</complaintStatus></complaintTrackingResp>";
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $bbps_DepositeEnquiry = json_decode($getjson,TRUE);
       
        if ($bbps_DepositeEnquiry) {
            $response['status'] = 'success';
            $response['message'] = 'Bill Validation Success';
            $response['user_status'] = $user->status;
            $response['description'] = $bbps_DepositeEnquiry;
        }
        return response()->json($response, $this->successStatus);
    }
    
    // <*****************************************************************************************>
    // <-----------------------------------BBPS API's END---------------------------------------->

    public function checkSMSStatus(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'shoot_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $api_key = '25F6EF10FE18AF';
        $sms_shoot_id = request('shoot_id');
        $api_url = "http://msg.pwasms.com/app/miscapi/".$api_key."/getDLR/".$sms_shoot_id;
        //Submit to server
        $response = file_get_contents( $api_url);
        $dlr_array = json_decode($response);
        print_r($dlr_array);
    }
    
    // <******************************************************************************************>
    // <-----------------------------------UPI API's START---------------------------------------->

    public function upiGenerateQR(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric', 
            'mobile' => 'required|numeric', 
            'name' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        if (!empty(request('amount'))) {
            $amount = number_format(request('amount'),2, '.', '');
        }else{
            $amount = "1.00";
        }

        $data = [
              "merchantId" => "408257",
              "terminalId" => "6010",
              "merchantTranId" => 'MR'.time(),
              "amount" => $amount,
              "billNumber" => 'BN'.time(),
          ];

        $filepath=fopen(public_path()."/keys/merchantEncryption.pem","r"); // bank public cert path
        $pub_key_string=fread($filepath,8192);
        fclose($filepath);
        openssl_get_publickey($pub_key_string);
        openssl_public_encrypt(json_encode($data),$crypttext,$pub_key_string); // $crypttext is output
        $encryptedRequest = json_encode(base64_encode($crypttext));  // $encryptedRequest final request which need to send to bank

        $header = [
            'Content-type:text/plain'
        ];

        $httpUrl = 'https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/QR/408257';

        $logfile = public_path().'/logs/UPIqrlog.txt';  // folder need to be created but file will be created auto
        $log = "\n\n".'GUID - '.time()."================================================================\n";
        $log .= 'URL - '.$httpUrl."\n\n";
        $log .= 'HEADER - '.json_encode($header)."\n\n";
        $log .= 'REQUEST - '.json_encode($data)."\n\n";
        $log .= 'REQUEST ENCRYPTED - '.json_encode($encryptedRequest)."\n\n";

        file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $httpUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $encryptedRequest,
            CURLOPT_HTTPHEADER => $header
        ));

        $raw_response = curl_exec($curl);
        curl_close($curl);

        $fp= fopen(public_path()."/keys/damskart.final.key","r"); // it can be in any format
        $priv_key_string=fread($fp,8192);
        fclose($fp);
        $private_key = openssl_get_privatekey($priv_key_string, "");

        openssl_private_decrypt(base64_decode($raw_response), $responsedata, $private_key); // $response will be output

        $log = "\n\n".'GUID - '.date('d-m-Y H:i:s')."================================================================ \n";
        $log .= 'URL - '.$httpUrl. "\n\n";
        $log .= 'RESPONSE -'.json_encode($raw_response)."\n\n";
        $log .= 'RESPONSE DECRYPTED - '.$responsedata."\n\n";
        file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);

        $upigenerateQr = json_decode($responsedata);

        $unique_id = "DMS".date("Ymd").time();
       
        if ($upigenerateQr->success == "true") {
           $upi_qrtxns = new Upi_qr_txns();
           $upi_qrtxns->user_id = $user->id;
           $upi_qrtxns->created_by = $user->created_by;
           $upi_qrtxns->txn_type = "generate_qr";
           $upi_qrtxns->dms_txn_id = $unique_id;
           $upi_qrtxns->refId = $upigenerateQr->refId;
           $upi_qrtxns->merchantTranId = $upigenerateQr->merchantTranId;
           $upi_qrtxns->message = $upigenerateQr->message;
           $upi_qrtxns->mobile = request('mobile');
           $upi_qrtxns->name = request('name');
           $upi_qrtxns->status = "Pending";
           $upi_qrtxns->amount = $amount;
           $upi_qrtxns->save();

            $response['status'] = 'success';
            $response['message'] = 'Generate QR Success.';
            $response['user_status'] = $user->status;

            if (!empty(request('amount'))) {
                $response['description'] = array('upi_url' => "upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr=".$upigenerateQr->refId."&am=".$amount."&cu=INR&mc=6010");
            }else{
                $response['description'] =array('upi_url' => "upi://pay?pa=damskartpay@icici&pn=DamskartPay&tr=".$upigenerateQr->refId."&am=&cu=INR&mc=6010");
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = $upigenerateQr->message;
        }
         return response()->json($response, $this->successStatus);
    }

    public function upiSendRequest(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric', 
            'mobile' => 'required|numeric', 
            'name' => 'required', 
            'amount' => 'required', 
            'upi_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $requestmobile = request('mobile');
        $requestname = request('name');
        $requestamount = number_format(request('amount'),2, '.', '');
        $requestupi_id = request('upi_id');
      
        $data = [
            "payerVa" => $requestupi_id,  // production pe user's vpa
            "amount" => $requestamount, // shoul be always 2 decimal
            "note" => 'testing',
            "collectByDate" => date('d/m/Y H:i A', strtotime('+1 day')),//now + 15min,
            "merchantId" => "408257", //will provide by bank,
            "subMerchantId" => $requestmobile, // can send random number or merchantId
            "subMerchantName" => $requestname,  //can send test /satyender
            "merchantName"  => 'DamskartPay', //clients company name which will be displayed at user's upi app.
            'terminalId'    => '5411',
            "merchantTranId"    => 'MR'.time(), //should be unique every time.
            "billNumber"        => 'BN'.time() //should be unique every time.
        ]; 
        //echo json_encode($data);
        $filepath=fopen(public_path()."/keys/merchantEncryption.pem","r"); // bank public cert path
        $pub_key_string=fread($filepath,8192);
        fclose($filepath);
        openssl_get_publickey($pub_key_string);
        openssl_public_encrypt(json_encode($data),$crypttext,$pub_key_string); // $crypttext is output
        $encryptedRequest = json_encode(base64_encode($crypttext));  // $encryptedRequest final request which need to send to bank
        //echo $encryptedRequest;
        $header = [
            'Content-type:text/plain'
        ];

        $httpUrl = 'https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/CollectPay2/408257';

        $logfile = public_path().'/logs/UPIcollectpaylog.txt';  // folder need to be created but file will be created auto
        $log = "\n\n".'GUID - '.time()."================================================================\n";
        $log .= 'URL - '.$httpUrl."\n\n";
        $log .= 'HEADER - '.json_encode($header)."\n\n";
        $log .= 'REQUEST - '.json_encode($data)."\n\n";
        $log .= 'REQUEST ENCRYPTED - '.json_encode($encryptedRequest)."\n\n";

        file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $httpUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $encryptedRequest,
            CURLOPT_HTTPHEADER => $header
        ));

        $raw_response = curl_exec($curl);
        curl_close($curl);
        // echo $raw_response;
        $fp= fopen(public_path()."/keys/damskart.final.key","r"); // it can be in any format
        $priv_key_string=fread($fp,8192);
        
        fclose($fp);
        $private_key = openssl_get_privatekey($priv_key_string, "");

        openssl_private_decrypt(base64_decode($raw_response), $responsedata, $private_key); // $response will be output

        $log = "\n\n".'GUID - '.time()."================================================================ \n";
        $log .= 'URL - '.$httpUrl. "\n\n";
        $log .= 'RESPONSE -'.json_encode($raw_response)."\n\n";
        $log .= 'RESPONSE DECRYPTED - '.$responsedata."\n\n";
        file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);

        $upisendpayrequest = json_decode($responsedata);

        $unique_id = "DMS".date("Ymd").time();
       
        if ($upisendpayrequest->success == "true") {
           $upi_qrtxns = new Upi_qr_txns();
           $upi_qrtxns->user_id = $user->id;
           $upi_qrtxns->created_by = $user->created_by;
           $upi_qrtxns->txn_type = "send_request";
           $upi_qrtxns->dms_txn_id = $unique_id;
           $upi_qrtxns->merchantTranId = $upisendpayrequest->merchantTranId;
           $upi_qrtxns->message = $upisendpayrequest->message;
           $upi_qrtxns->subMerchantId = $upisendpayrequest->subMerchantId;
           $upi_qrtxns->mobile = request('mobile');
           $upi_qrtxns->name = request('name');
           $upi_qrtxns->amount = request('amount');
           $upi_qrtxns->upi_id = request('upi_id');
           $upi_qrtxns->status = "Pending";
           $upi_qrtxns->bankRRN = $upisendpayrequest->BankRRN;
           $upi_qrtxns->save();

            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = "Payment request has been successfully sent to ".request('upi_id');
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = $upisendpayrequest->message;
        }
        return response()->json($response, $this->successStatus);
    }
    // <-----------------------------------UPI API's END----------------------------------------->
    // <*****************************************************************************************>
    
    //dmtSenderOtp
    public function dmtSenderOtp(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'customer_name' => 'required', 
            'customer_mobile' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $customer_name = request('customer_name');
          $customer_mobile = request('customer_mobile');
          $customer_pin = "401203";
          // $customer_pin = request('customer_pin');

          $plainText = '<dmtServiceRequest>
                        <requestType>SenderRegister</requestType>
                        <senderMobileNumber>'.$customer_mobile.'</senderMobileNumber>
                        <txnType>IMPS</txnType>
                        <senderName>'.$customer_name.'</senderName>
                        <senderPin>'.$customer_pin.'</senderPin>
                        </dmtServiceRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);
          // $responsedata = '<dmtServiceResponse>
          //       <additionalRegData>58b9ded4-79f3-409c-a7ab-995b9dc6155a</additionalRegData>
          //       <respDesc>OTP has been sent successfully</respDesc>
          //       <responseCode>000</responseCode>
          //       <responseReason>Successful</responseReason>
          //       <senderMobileNumber>9799594939</senderMobileNumber>
          //   </dmtServiceResponse>';
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $getSenderOtp = json_decode($getjson,TRUE);

        if ($getSenderOtp['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $getSenderOtp['respDesc'];
            $response['description'] = $getSenderOtp;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $getSenderOtp['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $getSenderOtp['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //dmtVerifyOtp
    public function dmtVerifyOtp(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'otp' => 'required', 
            'additionalRegData' => 'required', 
            'senderMobileNumber' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $verify_otp = request('otp');
        $verify_additionalRegData = request('additionalRegData');
        $verify_senderMobileNumber = request('senderMobileNumber');
        $plainText = '<dmtServiceRequest>
                      <requestType>VerifySender</requestType>
                      <senderMobileNumber>'.$verify_senderMobileNumber.'</senderMobileNumber>
                      <txnType>IMPS</txnType>
                      <otp>'.$verify_otp.'</otp>
                      <additionalRegData>'.$verify_additionalRegData.'</additionalRegData>
                      </dmtServiceRequest>';
        
        $key = "5824CF6AAF6C1AD063B7F88B16507226";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "DM03";
        $parameters = http_build_query($data);
        $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);
        // print_r($responsedata);
        // $responsedata = '<dmtServiceResponse>
        //                 <respDesc>Customer Raj has been updated</respDesc>
        //                 <responseCode>000</responseCode>
        //                 <responseReason>Successful</responseReason>
        //                 <senderMobileNumber>7894567890</senderMobileNumber>
        //                 </dmtServiceResponse>';
        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $verifyOTP = json_decode($getjson,TRUE);
        if ($verifyOTP['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $verifyOTP['respDesc'];

            $senderMobileNumber = request('senderMobileNumber');
            $plainText = '<dmtServiceRequest>
                          <requestType>AllRecipient</requestType>
                          <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                          <txnType>IMPS</txnType>
                          </dmtServiceRequest>
                          ';
            
            $key = "5824CF6AAF6C1AD063B7F88B16507226";
            $encrypt_xml_data = $this->encrypt($plainText, $key);
            $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
            $data['requestId'] = $this->generateRandomString();
            $data['encRequest'] = $encrypt_xml_data;
            $data['ver'] = "1.0";
            $data['instituteId'] = "DM03";
            $parameters = http_build_query($data);
            $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            $responsedata = $this->decrypt($result, $key);
            $getdata = htmlentities($responsedata);
            // print_r($responsedata);
            // $responsedata = '<dmtServiceResponse>
            //                   <recipientList>
            //                   <dmtRecipientList>
            //                   <bankAccountNumber>51400100001714</bankAccountNumber>
            //                   <bankCode>BARB</bankCode>
            //                   <bankName>Bank of Baroda</bankName>
            //                   <ifsc>BARB0NAJDEL</ifsc>
            //                   <isVerified>N</isVerified>
            //                   <recipientId>607</recipientId>
            //                   <recipientName>baroda</recipientName>
            //                   <recipientStatus>E</recipientStatus>
            //                   Page 8
            //                   <verifiedName></verifiedName>
            //                   </dmtRecipientList>
            //                   <dmtRecipientList>
            //                   <bankAccountNumber>123801514439</bankAccountNumber>
            //                   <bankCode>ICIC</bankCode>
            //                   <bankName>ICICI Bank</bankName>
            //                   <ifsc>ICIC0001238</ifsc>
            //                   <isVerified>N</isVerified>
            //                   <recipientId>551</recipientId>
            //                   <recipientName>Anagha</recipientName>
            //                   <recipientStatus>E</recipientStatus>
            //                   <verifiedName>Anagha</verifiedName>
            //                   </dmtRecipientList>
            //                   </recipientList>
            //                   <respDesc>Success</respDesc>
            //                   <responseCode>000</responseCode>
            //                   <responseReason>Successful</responseReason>
            //                   <senderMobileNumber>9920010041</senderMobileNumber>
            //                   </dmtServiceResponse>';
            $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
            $getjson = json_encode($xml);
            $allRecipient = json_decode($getjson,TRUE);

            if ($allRecipient['responseCode'] == "000") {
                $param = $allRecipient['recipientList']['dmtRecipientList'];
                $arr = [];
                $isArray = is_array($param);
                if (is_array($param) && !empty($param[0])) {
                    array_push($arr, $param);
                    $allRecipient['recipientList']['dmtRecipientList'] = $param;
                }else{
                    array_push($arr, $param);
                    $allRecipient['recipientList']['dmtRecipientList'] = $arr;
                }
                $response['user_status'] = $user->status;
                $response['description'] = $allRecipient;
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $err = $allRecipient['errorInfo']['error'];
                $isArray = is_array($err);
                if (is_array($err) && !empty($err[0])) {
                    $stack = [];
                    for ($i=0; $i < count($err) ; $i++) {
                        $val = $err[$i]['errorMessage'];
                        array_push($stack, $val);
                    }
                    $finalerr = implode(', ',$stack);
                    $response['message'] = $finalerr;
                }else{
                    $response['message'] = $allRecipient['errorInfo']['error']['errorMessage'];
                }
            }
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $verifyOTP['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $verifyOTP['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //dmtResendOtp
    public function dmtResendOtp(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'senderMobileNumber' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $senderMobileNumber = request('senderMobileNumber');
          $plainText = '<dmtServiceRequest>
                        <requestType>ResendSenderOtp</requestType>
                        <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                        <txnType>IMPS</txnType>
                        </dmtServiceRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);
          // $responsedata = '<dmtServiceResponse>
          //                     <additionalRegData>a18ff704-4d71-4e62-b6b7-d2f65196d603</additionalRegData>
          //                     <respDesc>OTP has been sent successfully</respDesc>
          //                     <responseCode>000</responseCode>
          //                     <responseReason>Successful</responseReason>
          //                     <senderMobileNumber>9799594939</senderMobileNumber>
          //                 </dmtServiceResponse>'; 
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $resendOTP = json_decode($getjson,TRUE);

        if ($resendOTP['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $resendOTP['respDesc'];
            $response['description'] = $resendOTP;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $resendOTP['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $resendOTP['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //dmtAddRecipient
    public function dmtAddRecipient(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'recipient_name' => 'required', 
            'recipient_mobile' => 'required', 
            'recipient_bankcode' => 'required', 
            'recipient_account_number' => 'required', 
            'recipient_ifsc' => 'required', 
            'senderMobileNumber' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $recipient_name = request('recipient_name');
          $recipient_mobile = request('recipient_mobile');
          $recipient_bankcode = request('recipient_bankcode');
          $recipient_account_number = request('recipient_account_number');
          $recipient_ifsc = request('recipient_ifsc');
          $senderMobileNumber = request('senderMobileNumber');

          $plainText = '<dmtServiceRequest>
                    <agentId>CC01CC01513515340681</agentId>
                    <requestType>VerifyBankAcct</requestType>
                    <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                    <bankCode>'.$recipient_bankcode.'</bankCode>
                    <bankAccountNumber>'.$recipient_account_number.'</bankAccountNumber>
                    <ifsc>'.$recipient_ifsc.'</ifsc>
                    <initChannel>AGT</initChannel>
                    </dmtServiceRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);
          // $responsedata = '<dmtServiceResponse>
          //                   <impsName>Priyanka</impsName>
          //                   <respDesc>Recipient verified successfully</respDesc>
          //                   <responseCode>000</responseCode>
          //                   <responseReason>Successful</responseReason>
          //                   <senderMobileNumber>9082868993</senderMobileNumber>
          //                   <txnId></txnId>
          //                   <uniqueRefId>7pm75beda8643mt799e23a69p3pdp40y11</uniqueRefId>
          //                   </dmtServiceResponse>';
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $verifyRecipient = json_decode($getjson,TRUE);

        if ($verifyRecipient['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $plainText = '<dmtServiceRequest>
                            <requestType>RegRecipient</requestType>
                            <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                            <txnType>IMPS</txnType>
                            <recipientName>'.$recipient_name.'</recipientName>
                            <recipientMobileNumber>'.$recipient_mobile.'</recipientMobileNumber>
                            <bankCode>'.$recipient_bankcode.'</bankCode>
                            <bankAccountNumber>'.$recipient_account_number.'</bankAccountNumber>
                            <ifsc>'.$recipient_ifsc.'</ifsc>
                            </dmtServiceRequest>';

              
              $key = "5824CF6AAF6C1AD063B7F88B16507226";
              $encrypt_xml_data = $this->encrypt($plainText, $key);
              $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
              $data['requestId'] = $this->generateRandomString();
              $data['encRequest'] = $encrypt_xml_data;
              $data['ver'] = "1.0";
              $data['instituteId'] = "DM03";
              $parameters = http_build_query($data);
              $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
              $result = curl_exec($ch);
              $responsedata = $this->decrypt($result, $key);
              $getdata = htmlentities($responsedata);
              // print_r($responsedata);
              // $responsedata = '<dmtServiceResponse>
              //                   <recipientList>
              //                   <dmtRecipient>
              //                   <bankAccountNumber>020302191915</bankAccountNumber>
              //                   <bankCode>ICIC</bankCode>
              //                   <bankName>ICICI Bank</bankName>
              //                   <ifsc>ICIC0000003</ifsc>
              //                   <isVerified>N</isVerified>
              //                   <recipientId>637</recipientId>
              //                   <recipientName>Vivek</recipientName>
              //                   <recipientStatus>E</recipientStatus>
              //                   <verifiedName></verifiedName>
              //                   </dmtRecipient>
              //                   </recipientList>
              //                   <respDesc>Recipient added with recipient ID: 637</respDesc>
              //                   <responseCode>000</responseCode>
              //                   <responseReason>Successful</responseReason>
              //                   <senderMobileNumber>7875633330</senderMobileNumber>
              //                   </dmtServiceResponse>';
              $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
              $getjson = json_encode($xml);
              $addRecipient = json_decode($getjson,TRUE);

            if ($addRecipient['responseCode'] == "000") {
                $response['status'] = 'success';
                $response['user_status'] = $user->status;
                $response['message'] = $addRecipient['respDesc'];
                $response['description'] = $addRecipient;
            }else{
                $response['status'] = 'error';
                $response['user_status'] = $user->status;
                $err = $addRecipient['errorInfo']['error'];
                $isArray = is_array($err);
                if (is_array($err) && !empty($err[0])) {
                    $stack = [];
                    for ($i=0; $i < count($err) ; $i++) {
                        $val = $err[$i]['errorMessage'];
                        array_push($stack, $val);
                    }
                    $finalerr = implode(', ',$stack);
                    $response['message'] = $finalerr;
                }else{
                    $response['message'] = $addRecipient['errorInfo']['error']['errorMessage'];
                }
            }

        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $verifyRecipient['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $verifyRecipient['errorInfo']['error']['errorMessage'];
            }
        }
        
          
        return response()->json($response, $this->successStatus);
    }
    
    //dmtAllRecipient
    public function dmtAllRecipient(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'senderMobileNumber' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $senderMobileNumber = request('senderMobileNumber');
        $plainText = '<dmtServiceRequest>
                      <requestType>AllRecipient</requestType>
                      <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                      <txnType>IMPS</txnType>
                      </dmtServiceRequest>
                      ';
        
        $key = "5824CF6AAF6C1AD063B7F88B16507226";
        $encrypt_xml_data = $this->encrypt($plainText, $key);
        $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
        $data['requestId'] = $this->generateRandomString();
        $data['encRequest'] = $encrypt_xml_data;
        $data['ver'] = "1.0";
        $data['instituteId'] = "DM03";
        $parameters = http_build_query($data);
        $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $responsedata = $this->decrypt($result, $key);
        $getdata = htmlentities($responsedata);
        // print_r($responsedata);
        // $responsedata = '<dmtServiceResponse>
        //                   <recipientList>
        //                   <dmtRecipientList>
        //                   <bankAccountNumber>51400100001714</bankAccountNumber>
        //                   <bankCode>BARB</bankCode>
        //                   <bankName>Bank of Baroda</bankName>
        //                   <ifsc>BARB0NAJDEL</ifsc>
        //                   <isVerified>N</isVerified>
        //                   <recipientId>607</recipientId>
        //                   <recipientName>baroda</recipientName>
        //                   <recipientStatus>E</recipientStatus>
        //                   <verifiedName></verifiedName>
        //                   </dmtRecipientList>
        //                   <dmtRecipientList>
        //                   <bankAccountNumber>123801514439</bankAccountNumber>
        //                   <bankCode>ICIC</bankCode>
        //                   <bankName>ICICI Bank</bankName>
        //                   <ifsc>ICIC0001238</ifsc>
        //                   <isVerified>N</isVerified>
        //                   <recipientId>551</recipientId>
        //                   <recipientName>Anagha</recipientName>
        //                   <recipientStatus>E</recipientStatus>
        //                   <verifiedName>Anagha</verifiedName>
        //                   </dmtRecipientList>
        //                   </recipientList>
        //                   <respDesc>Success</respDesc>
        //                   <responseCode>000</responseCode>
        //                   <responseReason>Successful</responseReason>
        //                   <senderMobileNumber>9920010041</senderMobileNumber>
        //                   </dmtServiceResponse>';
        $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
        $getjson = json_encode($xml);
        $allRecipient = json_decode($getjson,TRUE);

        if ($allRecipient['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $allRecipient['respDesc'];
            $param = $allRecipient['recipientList']['dmtRecipientList'];
            $arr = [];
            $isArray = is_array($param);
            if (is_array($param) && !empty($param[0])) {
                array_push($arr, $param);
                $allRecipient['recipientList']['dmtRecipientList'] = $param;
            }else{
                array_push($arr, $param);
                $allRecipient['recipientList']['dmtRecipientList'] = $arr;
            }
            
            $response['description'] = $allRecipient;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $allRecipient['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $allRecipient['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //dmtDeleteRecipient
    public function dmtDeleteRecipient(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'recipient_id' => 'required', 
            'senderMobileNumber' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $recipient_id = request('recipient_id');
          $senderMobileNumber = request('senderMobileNumber');
          $plainText = '<dmtServiceRequest>
                        <requestType>DelRecipient</requestType>
                        <senderMobileNumber>'.$senderMobileNumber.'</senderMobileNumber>
                        <txnType>IMPS</txnType>
                        <recipientId>'.$recipient_id.'</recipientId>
                        </dmtServiceRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);
          // $responsedata = '<dmtServiceResponse>
          //                 <respDesc>This recipient has been deleted</respDesc>
          //                 <responseCode>000</responseCode>
          //                 <responseReason>Successful</responseReason>
          //                 </dmtServiceResponse>';
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $deleteRecipient = json_decode($getjson,TRUE);

        if ($deleteRecipient['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $deleteRecipient['respDesc'];
            $response['description'] = $deleteRecipient;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $deleteRecipient['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $deleteRecipient['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }
    
    //dmtBankList
    public function dmtBankList(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'skip' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }

        $keyword = request('keyword');
        if (!empty($keyword)) {
            $dmt_bank_list = BankDetail::where(function($qr) use ($keyword){
                                $qr->where('bankName', 'like', "%{$keyword}%")
                                    ->orWhere('bankCode', 'like', "%{$keyword}%");
                            })
                            ->get()->splice(request('skip'))->take(request('limit'));
        }else{
            $dmt_bank_list = BankDetail::all()->splice(request('skip'))->take(request('limit')); 
        }
       
        if (!empty($dmt_bank_list[0])) {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = 'Bank list available.';
            $response['description']['dmt_bank_list'] = $dmt_bank_list;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $response['message'] = 'Bank list not available!';
        }
        return response()->json($response, $this->successStatus);
    }

    //dmtBankListUpdate
    public function dmtBankListUpdate(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

       
          $plainText = '<dmtServiceRequest>
                        <requestType>BankList</requestType>
                        <txnType>IMPS</txnType>
                        </dmtServiceRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtServiceReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);
          // $responsedata = '<dmtServiceResponse>
          //                   <bankList>
          //                   <bankInfoArray>
          //                   <accountVerificationAllowed>Y</accountVerificationAllowed>
          //                   <bankCode>SBIN</bankCode>
          //                   <bankName>State Bank of India</bankName>
          //                   <impsAllowed>Y</impsAllowed>
          //                   <neftAllowed>Y</neftAllowed>
          //                   </bankInfoArray>
          //                   <bankInfoArray>
          //                   <accountVerificationAllowed>Y</accountVerificationAllowed>
          //                   <bankCode>PUNB</bankCode>
          //                   <bankName>Punjab National Bank</bankName>
          //                   <impsAllowed>Y</impsAllowed>
          //                   <neftAllowed>Y</neftAllowed>
          //                   </bankInfoArray>
          //                   <bankInfoArray>
          //                   <accountVerificationAllowed>Y</accountVerificationAllowed>
          //                   <bankCode>BKID</bankCode>
          //                   <bankName>Bank of India</bankName>
          //                   <impsAllowed>Y</impsAllowed>
          //                   <neftAllowed>Y</neftAllowed>
          //                   </bankInfoArray>
          //                   <bankInfoArray>
          //                   <accountVerificationAllowed>Y</accountVerificationAllowed>
          //                   <bankCode>BARB</bankCode>
          //                   <bankName>Bank of Baroda</bankName>
          //                   <impsAllowed>Y</impsAllowed>
          //                   <neftAllowed>Y</neftAllowed>
          //                   </bankInfoArray>
          //                   </bankList>
          //                   <respDesc>Success</respDesc>
          //                   <responseCode>000</responseCode>
          //                   <responseReason>Successful</responseReason>
          //                   <txnType>IMPS</txnType>
          //                   </dmtServiceResponse>';
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $dmtbankList = json_decode($getjson,TRUE);

        if ($dmtbankList['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            foreach ($dmtbankList['bankList']['bankInfoArray'] as $key => $value) {
                $updatebankList = new BankDetail();
                // var_dump($value['accountVerificationAllowed']);
                $updatebankList->bankName = $value['bankName'];
                $updatebankList->bankCode = $value['bankCode'];
                $updatebankList->impsAllowed = $value['impsAllowed'];
                $updatebankList->neftAllowed = $value['neftAllowed'];
                $updatebankList->accountVerificationAllowed = $value['accountVerificationAllowed'];
                $updatebankList->save();

            }
            // exit;
            
            $response['message'] = $dmtbankList['respDesc'];
            $response['description'] = $dmtbankList;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $dmtbankList['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $dmtbankList['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }

    //dmtFundTransfer
    public function dmtFundTransfer(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'senderMobileNumber' => 'required', 
            'recipient_id' => 'required', 
            'cust_convfee' => 'required', 
            'amount' => 'required', 
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        if($user->id != request('user_id')){
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                'revoked' => true
            ]);

            $accessToken->revoke();
            $response['status'] = 'error';
            $response['message'] = 'You are not valid user!! Login Again';
            return response()->json($response, $this->successStatus);
        }
        $requestdata = request()->all();

        $recipient_id = request('recipient_id');
        $cust_convfee = request('cust_convfee') * 100;
        $amount = request('amount') * 100;
        $senderMobileNumber = request('senderMobileNumber');
          
          $plainText = '<dmtTransactionRequest>
                        <requestType>FundTransfer</requestType>
                        <senderMobileNo>'.$senderMobileNumber.'</senderMobileNo>
                        <agentId>CC01CC01513515340681</agentId>
                        <initChannel>AGT</initChannel>
                        <recipientId>'.$recipient_id.'</recipientId>
                        <txnAmount>'.$amount.'</txnAmount>
                        <convFee>'.$cust_convfee.'</convFee>
                        <txnType>IMPS</txnType>
                        </dmtTransactionRequest>';

          
          $key = "5824CF6AAF6C1AD063B7F88B16507226";
          $encrypt_xml_data = $this->encrypt($plainText, $key);
          $data['accessCode'] = "AVTQ06LF12JW01DFLJ";
          $data['requestId'] = $this->generateRandomString();
          $data['encRequest'] = $encrypt_xml_data;
          $data['ver'] = "1.0";
          $data['instituteId'] = "DM03";
          $parameters = http_build_query($data);
          $url = "https://stgapi.billavenue.com/billpay/dmt/dmtTransactionReq/xml";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
          $result = curl_exec($ch);
          $responsedata = $this->decrypt($result, $key);
          $getdata = htmlentities($responsedata);
          // print_r($responsedata);

          // $responsedata = '<dmtTransactionResponse>
          //                   <fundTransferDetails>
          //                   <fundDetail>
          //                   <uniqueRefId>TEST000MNPREQ1000000000000000021441</uniqueRefId>
          //                   <bankTxnId>876938414</bankTxnId>
          //                   <custConvFee>2000</custConvFee>
          //                   <DmtTxnId>4887</DmtTxnId>
          //                   <impsName>Selena Gomez</impsName>
          //                   <refId>CCAFCBCCCC69C694CB98AEBB49C276B7BBD</refId>
          //                   <txnAmount>200000</txnAmount>
          //                   <txnStatus>C</txnStatus>
          //                   </fundDetail>
          //                   </fundTransferDetails>
          //                   <respDesc>Transfer of Rs. 2000.0 was successful </respDesc>
          //                   <responseCode>000</responseCode>
          //                   <responseReason>Successful</responseReason>
          //                   <senderMobileNo>9920010041</senderMobileNo>
          //                   <uniqueRefId>TEST000MNPREQ1000000000000000021441</uniqueRefId>
          //                   </dmtTransactionResponse>';

          // $responsedata = '<dmtTransactionResponse>
          //                   <fundTransferDetails>
          //                   <fundDetail>
          //                   <uniqueRefId>TEST000MNPREQ1000000000000000021875</uniqueRefId>
          //                   <bankTxnId>930512472437</bankTxnId>
          //                   <custConvFee>5000</custConvFee>
          //                   <DmtTxnId>5838</DmtTxnId>
          //                   <impsName>MOBILEWARE PVT LTD</impsName>
          //                   <refId>CCA9D7963CD5AE84B2CB0DA2D428FF45940</refId>
          //                   <txnAmount>500000</txnAmount>
          //                   <txnStatus>C</txnStatus>
          //                   </fundDetail>
          //                   <fundDetail>
          //                   <uniqueRefId>TEST000MNPREQ1000000000000000021875</uniqueRefId>
          //                   <bankTxnId>930512472438</bankTxnId>
          //                   <custConvFee>1000</custConvFee>
          //                   <DmtTxnId>5839</DmtTxnId>
          //                   <impsName>MOBILEWARE PVT LTD</impsName>
          //                   <refId>CCA0E893F6B441A4D37A376426D779BEF3D</refId>
          //                   <txnAmount>100000</txnAmount>
          //                   <txnStatus>C</txnStatus>
          //                   </fundDetail>
          //                   </fundTransferDetails>
          //                   <respDesc>Transfer of Rs. 6000.00 was successful </respDesc>
          //                   <responseCode>000</responseCode>
          //                   <responseReason>Successful</responseReason>
          //                   <senderMobileNo>9920010041</senderMobileNo>
          //                   <uniqueRefId>TEST000MNPREQ1000000000000000021875</uniqueRefId>
          //                   </dmtTransactionResponse>';
          $xml = simplexml_load_string($responsedata, "SimpleXMLElement", LIBXML_NOCDATA);
          $getjson = json_encode($xml);
          $dmt_fund_transfer = json_decode($getjson,TRUE);

        if ($dmt_fund_transfer['responseCode'] == "000") {
            $response['status'] = 'success';
            $response['user_status'] = $user->status;
            $response['message'] = $dmt_fund_transfer['respDesc'];

            $param = $dmt_fund_transfer['fundTransferDetails']['fundDetail'];
            $arr = [];
            $isArray = is_array($param);
            if (is_array($param) && !empty($param[0])) {
                array_push($arr, $param);
                $dmt_fund_transfer['fundTransferDetails']['fundDetail'] = $param;
            }else{
                array_push($arr, $param);
                $dmt_fund_transfer['fundTransferDetails']['fundDetail'] = $arr;
            }
            $response['description'] = $dmt_fund_transfer;
        }else{
            $response['status'] = 'error';
            $response['user_status'] = $user->status;
            $err = $dmt_fund_transfer['errorInfo']['error'];
            $isArray = is_array($err);
            if (is_array($err) && !empty($err[0])) {
                $stack = [];
                for ($i=0; $i < count($err) ; $i++) {
                    $val = $err[$i]['errorMessage'];
                    array_push($stack, $val);
                }
                $finalerr = implode(', ',$stack);
                $response['message'] = $finalerr;
            }else{
                $response['message'] = $dmt_fund_transfer['errorInfo']['error']['errorMessage'];
            }
        }
        return response()->json($response, $this->successStatus);
    }

    public function verifyPan(Request $request){
        $validator = Validator::make($request->all(), [ 
            'pan_number' => 'required', 
            'date_of_birth' => 'required', 
            'first_name' => 'required',
            'last_name' => 'required'
        ]);
        $errors = json_decode($validator->errors());
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $message = $value[0];
                break;
            }
        }
        if ($validator->fails()) { 
            $response['status'] = 'error';
            $response['message'] = $message;
            return response()->json($response, $this->successStatus);
        }

        $pan_no = request('pan_number');

        $user_data = User::where('pan_number', $pan_no)->first();

        if (!empty($user_data)) {
            $response['status'] = 'error';
            $response['message'] = 'Pan Card Already Registered';
            return response()->json($response, $this->successStatus);
        }

        $full_name = request('first_name').' '.request('last_name');

        $postData = array(
            "pan_no" => request('pan_number'),
            "date_of_birth" => request('date_of_birth'),
            "full_name" => $full_name
        );

        $data = json_encode($postData);
        $header = [
            'Authorization: Basic QUk2WFJWOFlYTEVKM1lLUEhVRTMyT0FWOUI2VlVKM006M1ZYRkFaN0I1WTk0WFdDTkg2TFBXWVkzQTJZODM1UEY=', 
            'Content-Type: application/json',
        ];
        $httpUrl = 'https://api.digio.in/v3/client/kyc/pan/verify';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $httpUrl,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_RETURNTRANSFER => true
        ));
        $raw_response = curl_exec($curl);
        curl_close($curl);
        $pancard_response = json_decode($raw_response);
        
        if ($pancard_response->is_pan_dob_valid && $pancard_response->name_matched) {
            $response['status'] = 'success';
            $response['message'] = 'Successful Verified';
            $response['pan_response'] = $pancard_response;
        }else if ($pancard_response->is_pan_dob_valid) {
            $response['status'] = 'error';
            $response['message'] = 'Invalid DOB';
            $response['pan_response'] = $pancard_response;
        }else if ($pancard_response->name_matched) {
            $response['status'] = 'error';
            $response['message'] = 'Invalid Name';
            $response['pan_response'] = $pancard_response;
        }else{
            $response['status'] = 'error';
            $response['message'] = $pancard_response->error_message;
            $response['pan_response'] = $pancard_response;
        }
        return response()->json($response);
    }
    
}