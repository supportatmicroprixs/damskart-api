<?php

namespace App\Http\Controllers\API\V1\Admin;

class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Damskart OpenApi Documentation",
     *      description="Damskart OpenApi description",
     *      @OA\Contact(
     *          email="developer@damskart.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url="http://192.168.0.71/damskart-manage/api/v1",
     *      description="Damskart API Server"
     * )

     *
     * @OA\Tag(
     *     name="Projects1",
     *     description="API Endpoints of Projects"
     * )
     */
}
