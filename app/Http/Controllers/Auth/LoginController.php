<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
use App\Models\UserLastLogin;
use Illuminate\Support\Facades\URL;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    public $successStatus = 200;

    public function login(Request $request){
        if (Auth::attempt(['status' => '1', 'mobile' => $request->mobile, 'password' => $request->password, 'domain' => url('/') ])) {
            $retailer = Auth::user();
            if (!$this->checkVerify()) {
                $raw_response = array(
                    "code" => "333",
                    "mobile" => $request->mobile,
                    "message" => "Please Verify Document First!"
                );
                $otp_verification_response = json_decode(json_encode($raw_response));
                return response()->json($otp_verification_response);
            } 
            if ($retailer->profile_status == '1' && $retailer->verify == '0' && $retailer->digio_kyc_status == '1') {
                Auth::logout();
                $user_mobile = $request->mobile;
                $user_password = $request->password;
                $retailer->otp = rand(100000,999999);
                $retailer->save(); 
                
                $otp = $retailer->otp;
                $api_key = '25F6EF10FE18AF';
                $contacts = $user_mobile;
                $from = 'DMSKRT';
                // $sms_text = urlencode('Damskart never calls for OTP. Do not share it with anyone. Login OTP: '.$otp.'. Not You? Report https://damskart.com');
                // $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access Login is '.$otp.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone, Not You? Report https://damskart.com/addquery. Thank you for connecting with Damskart Business.');
                $sms_text = urlencode('Dear '.$retailer->first_name.', 

Your OTP to access login transaction is '.$otp.'. Do not disclose OTP to anyone,

Regards
Damskart Business.');
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&template_id=1207162371995454317&msg=".$sms_text);
                $output = curl_exec($ch);
                
                $raw_response = array(
                "code" => "111",
                "mobile" => $user_mobile,
                "message" => "A OTP(One Time Passcode) has been sent to ".$user_mobile
                );
                $otp_verification_response = json_decode(json_encode($raw_response));
                return response()->json($otp_verification_response);
            }else{
                $ip = NULL; $deep_detect = FALSE;

                if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
                    $ip = $_SERVER["REMOTE_ADDR"];
                    if ($deep_detect) {
                        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                            $ip = $_SERVER['HTTP_CLIENT_IP'];
                    }
                }
                $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);

                $country =  $xml->geoplugin_countryName ;
                $city = $xml->geoplugin_city;
                $area = $xml->geoplugin_areaCode;
                $code = $xml->geoplugin_countryCode;

                $user_agent     =   $_SERVER['HTTP_USER_AGENT'];
                $os_platform    =   "Unknown OS Platform";
                $os_array       =   array(
                    '/windows nt 10/i'     =>  'Windows 10',
                    '/windows nt 6.3/i'     =>  'Windows 8.1',
                    '/windows nt 6.2/i'     =>  'Windows 8',
                    '/windows nt 6.1/i'     =>  'Windows 7',
                    '/windows nt 6.0/i'     =>  'Windows Vista',
                    '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                    '/windows nt 5.1/i'     =>  'Windows XP',
                    '/windows xp/i'         =>  'Windows XP',
                    '/windows nt 5.0/i'     =>  'Windows 2000',
                    '/windows me/i'         =>  'Windows ME',
                    '/win98/i'              =>  'Windows 98',
                    '/win95/i'              =>  'Windows 95',
                    '/win16/i'              =>  'Windows 3.11',
                    '/macintosh|mac os x/i' =>  'Mac OS X',
                    '/mac_powerpc/i'        =>  'Mac OS 9',
                    '/linux/i'              =>  'Linux',
                    '/ubuntu/i'             =>  'Ubuntu',
                    '/iphone/i'             =>  'iPhone',
                    '/ipod/i'               =>  'iPod',
                    '/ipad/i'               =>  'iPad',
                    '/android/i'            =>  'Android',
                    '/blackberry/i'         =>  'BlackBerry',
                    '/webos/i'              =>  'Mobile'
                );
                foreach ($os_array as $regex => $value) {
                    if (preg_match($regex, $user_agent)) {
                        $os_platform    =   $value;
                    }
                }
                $browser        =   "Unknown Browser";
                $browser_array  =   array(
                    '/msie/i'       =>  'Internet Explorer',
                    '/firefox/i'    =>  'Firefox',
                    '/safari/i'     =>  'Safari',
                    '/chrome/i'     =>  'Chrome',
                    '/edge/i'       =>  'Edge',
                    '/opera/i'      =>  'Opera',
                    '/netscape/i'   =>  'Netscape',
                    '/maxthon/i'    =>  'Maxthon',
                    '/konqueror/i'  =>  'Konqueror',
                    '/mobile/i'     =>  'Handheld Browser'
                );
                foreach ($browser_array as $regex => $value) {
                    if (preg_match($regex, $user_agent)) {
                        $browser    =   $value;
                    }
                }
                
                //User::where('mobile','=',$request->mobile)->update(['last_login' => date('Y-m-d H:i:s')]);

                //last login save start
                $userid = User::where('mobile','=',$request->mobile)->first();
                $lastlogin = new UserLastLogin();
                $lastlogin->user_id = $userid->id;
                $lastlogin->mobile = $request->mobile;
                $lastlogin->email = $userid->email;
                $lastlogin->login_from = "website";
                $lastlogin->last_login = date('Y-m-d H:i:s');
                $lastlogin->start_time = date('H:i:s');
                $lastlogin->date = date('Y-m-d');


                $lastlogin->user_ip = $ip;
                $lastlogin->location = $city." - ".$area." - ".$country ." - ".$code;
                $lastlogin->details = $browser.' on '.$os_platform;

                $lastlogin->save(); 
                //last login save end 
                // return redirect('index');
                $raw_response = array(
                "code" => "000",
                "mobile" => $request->mobile,
                "message" => "Login Successfuly."
                );
                $otp_verification_response = json_decode(json_encode($raw_response));
                return response()->json($otp_verification_response);
            }
            
        }else{
            $raw_response = array(
            "code" => "222",
            "mobile" => $request->mobile,
            "message" => "Please enter valid details!".url('/')
            );
            $otp_verification_response = json_decode(json_encode($raw_response));
            return response()->json($otp_verification_response);
            // return redirect('login')->with('danger', 'Please enter valid details!');
        }
    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
