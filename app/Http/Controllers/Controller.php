<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\User;
use App\Models\Catalog;
use App\Models\Package;
use App\Services;
use App\Service_packages;
use App\User_services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\RoleHasPermission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function checkVerify(){
      $retailer = Auth::user();
      // var_dump($retailer);
      // exit;
      if (($retailer->profile_status == '0' && $retailer->digio_kyc_status == '0') 
            || ($retailer->profile_status == '1' && $retailer->digio_kyc_status == '0' )
            || ($retailer->profile_status == '0' && $retailer->digio_kyc_status == '1')) {
        $retailer->otp = rand(100000,999999);
        $retailer->save();

        $otp = $retailer->otp;
        $mobile = "9799594939";
        // $mobile = $retailer->mobile;
        
        //Preparing post parameters
         // $sms_text = urlencode('Damskart never calls for OTP. Do not share it with anyone. Login OTP: '.$otp.'. Not You? Report https://damskart.com');
         $sms_text = urlencode('Dear Customer, One Time Password (OTP) to access Login Transaction is '.$otp.'. Damskart never calls to verify OTP. Do not disclose OTP to anyone, Not You? Report https://damskart.com.
            Regards
            Damskart Business');
        $api_key = '25F6EF10FE18AF';
        $contacts = $mobile;
        $from = 'DMSKRT';
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
        $output = curl_exec($ch);

        
        return false;
      }else{
        return true;
      }
    }

    public function checkPackage(){
        $user = Auth::user();
        if ($user->role == 'super admin' || $user->role == 'manager') {
            return true;
        }
        else
        {
            if($user->package_id != null && $user->role != 'super admin'){
                return true;
            }else{
                return false;
            }
        }   
    }

    public function checkProfileStatus(){
       if (!$this->checkPackage()) {
          return redirect('index')->with('info', 'Package not assign Yet!!!');
       }
       $user = Auth::user();
       if ($user->profile_status) {
          return true;
       }else{
        return false;
       }
    }

    public function checkLoginStatus(){
         $user = Auth::user();
         if (!empty($user)) {
            return true;
         }else{
          return false;
         }
    }
    
    public function checkViewMember(){
         $user = Auth::user();
         if ($user->role == 'super admin' || $user->role == 'manager') {
            return true;
         }else{
          return false;
         }
    }

    public function checkViewMemberPermission($permission){
         $user = Auth::user();
         if ($user->role == 'super admin' || $user->role == 'manager') {
            return true;
         }else{
            $isPermission = $user->hasPermissionTo($permission);
            return $isPermission;
         }
    }

    public function checkUserPermission($permission){
         $user = Auth::user();
         
         if ($user->role == "manager") {
            return true;
            /*if($user->can($permission)) {
              
            }else{
            return false;
           } */
         } else if($user->role == "super admin") {
            return true;
         }else {
            return true;
         }
    }
    
    public function noSuperAdminPermission(){
         $user = Auth::user();
         if ($user->role != 'super admin') {
            return true;
         }else{
          return false;
         }
    }
    
    public function checkReportStatus(){
       $user = Auth::user();
       $packagereport = Package::where('id', $user->package_id)->where('report', '1')->first();
       if ($user->role == "super admin" || $user->role == "manager" || ($user->role != "super admin" && !empty($packagereport))) {
          return true;
       }else{
        return false;
       }
    }
    
    public function userServices($id){
      $user = Auth::user();
      if ($user != null) {
        $url = url()->current();
        $currenturl = str_replace("https://damskart.com/", "", $url);

        $status = false;
        $service_list = Services::leftJoin('service_packages', 'service_packages.service_id', '=', 'services.id')->where('services.status', 1)->where('service_packages.status', 1)->where('service_packages.package_id', $user->package_id)->get(['services.id', 'services.service_name','service_packages.id as sid', 'services.icon', 'services.url']);
            foreach ($service_list as $key => $value) {
              $service_child = User_services::leftJoin('service_packages', 'service_packages.id', '=', 'user_services.servicepack_id')->where('user_services.servicepack_id', $value->sid)->where('user_services.user_id', $user->id)->where('user_services.status', 1)->where('service_packages.status', 1)->first();
                if (!empty($service_child)) {
                  if ($value->id == $id) {
                    $status = true;
                    return true;
                  }
                }
            }
            return false;
      }
    }
    
    public function status_confirm(Request $request){
        $data = [];
        $data['message'] = $request->message;
        $data['id'] = $request->id;
        $data['button_name'] = isset($request->button_name)?$request->button_name:'Delete';
        return view('admin.status_confirm',$data)->render();
    }
}
