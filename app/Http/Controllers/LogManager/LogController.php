<?php

namespace App\Http\Controllers\LogManager;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogManager\Classes\PublicLogViewer;

class LogController extends Controller
{
    /**
     * Lists all log files.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data['files'] = PublicLogViewer::getFiles(true, true);
        $this->data['title'] = trans('backpack::logmanager.log_manager');

        return view('publiclogmanager.logs', $this->data);
    }

    /**
     * Previews a log file.
     *
     * @throws \Exception
     */
    public function preview($file_name)
    {
        PublicLogViewer::setFile(decrypt($file_name));

        $logs = PublicLogViewer::all();
        
        if (count($logs) <= 0) {
            abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
        }

        $this->data['logs'] = $logs;
        $this->data['title'] = trans('backpack::logmanager.preview').' '.trans('backpack::logmanager.logs');
        $this->data['file_name'] = $original = decrypt($file_name);

        return view('publiclogmanager.log_item', $this->data);
    }

    /**
     * Downloads a log file.
     *
     * @param $file_name
     *
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($file_name)
    {
        return response()->download(PublicLogViewer::pathToLogFile(decrypt($file_name)));
    }

    /**
     * Deletes a log file.
     *
     * @param $file_name
     *
     * @throws \Exception
     *
     * @return string
     */
    public function delete($file_name)
    {
        if (config('backpack.logmanager.allow_delete') == false) {
            abort(403);
        }

        if (app('files')->delete(PublicLogViewer::pathToLogFile(decrypt($file_name)))) {
            return 'success';
        }

        abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
    }
}
