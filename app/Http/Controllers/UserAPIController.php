<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class UserAPIController extends Controller
{
    /**
     * @group  ICICI AEPS
     * Withdraw Money
     * @bodyParam merchant_key string required merchant key
     * @bodyParam merchant_secret string required merchant secret
     * @bodyParam service_name string required withdraw
     * @bodyParam device_name string required mantra / morpho
     * @bodyParam mobile_number string required Mobile number
     * @bodyParam aadhar_number string required Aadhar number
     * @bodyParam amount string required Withdraw Amount
     * @bodyParam bank string required Bank name
     * @response scenario=success {
     *  "success": "Form is successfully submitted",
     *  "txn_id": "DMS20210812203700"
     * }
     * @response status=402 {"error": "Service name is not correct"}
     * @response status=404 {"message": "Oops! We are in lost space. Please be patient while we are working on it"}
     */
    public function withdrawICICI(Request $request)
    {
        try
        {
            //, Balance Enquiry, Aadhar Pay, Mini Statement
            $json = array();
            $service_name = $request->service_name;
            $sevice_type = $request->sevice_type;
            $mobile_number = $request->mobile_number;
            $aadhar_number = $request->aadhar_number;
            $amount = $request->amount;
            $bank = $request->bank;

            $error = "";
            if($service_name != "withdraw")
            {
                $error = "Service name is not correct";
            }

            if($error == "")
            {
                return response()->json(['success'=> 'Form is successfully submitted!', 'txn_id' => 'DMS'.date('YmdHis')]);
            }
            else
            {
                return response()->json(['error'=> $error], 402);
            }
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();

            return response()->json(['error'=> $message]);
        }
    }

    /**
     * @group  ICICI AEPS
     * Balance Enquiry
     * @bodyParam merchant_key string required merchant key
     * @bodyParam merchant_secret string required merchant secret
     * @bodyParam service_name string required withdraw
     * @bodyParam device_name string required mantra / morpho
     * @bodyParam mobile_number string required Mobile number
     * @bodyParam aadhar_number string required Aadhar number
     * @bodyParam bank string required Bank name
     * @response scenario=success {
     *  "success": "Form is successfully submitted",
     *  "txn_id": "DMS20210812203700"
     * }
     * @response status=402 {"error": "Service name is not correct"}
     * @response status=404 {"message": "Oops! We are in lost space. Please be patient while we are working on it"}
     */
    public function balanceEnquiryICICI(Request $request)
    {
        try
        {
            //, Balance Enquiry, Aadhar Pay, Mini Statement
            $json = array();
            $service_name = $request->service_name;
            $sevice_type = $request->sevice_type;
            $mobile_number = $request->mobile_number;
            $aadhar_number = $request->aadhar_number;
            $amount = $request->amount;
            $bank = $request->bank;

            $error = "";
            if($service_name != "withdraw")
            {
                $error = "Service name is not correct";
            }

            if($error == "")
            {
                return response()->json(['success'=> 'Form is successfully submitted!', 'txn_id' => 'DMS'.date('YmdHis')]);
            }
            else
            {
                return response()->json(['error'=> $error]);
            }
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();

            return response()->json(['error'=> $message]);
        }
    }

    /**
     * @group  ICICI AEPS
     * Aadhar Pay Money
     * @bodyParam merchant_key string required merchant key
     * @bodyParam merchant_secret string required merchant secret
     * @bodyParam service_name string required withdraw
     * @bodyParam device_name string required mantra / morpho
     * @bodyParam mobile_number string required Mobile number
     * @bodyParam aadhar_number string required Aadhar number
     * @bodyParam amount string required Withdraw Amount
     * @bodyParam bank string required Bank name
     * @response scenario=success {
     *  "success": "Form is successfully submitted",
     *  "txn_id": "DMS20210812203700"
     * }
     * @response status=402 {"error": "Service name is not correct"}
     * @response status=404 {"message": "Oops! We are in lost space. Please be patient while we are working on it"}
     */
    public function aadharPayICICI(Request $request)
    {
        try
        {
            //, Balance Enquiry, Aadhar Pay, Mini Statement
            $json = array();
            $service_name = $request->service_name;
            $sevice_type = $request->sevice_type;
            $mobile_number = $request->mobile_number;
            $aadhar_number = $request->aadhar_number;
            $amount = $request->amount;
            $bank = $request->bank;

            $error = "";
            if($service_name != "withdraw")
            {
                $error = "Service name is not correct";
            }

            if($error == "")
            {
                return response()->json(['success'=> 'Form is successfully submitted!', 'txn_id' => 'DMS'.date('YmdHis')]);
            }
            else
            {
                return response()->json(['error'=> $error]);
            }
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();

            return response()->json(['error'=> $message]);
        }
    }

    /**
     * @group  ICICI AEPS
     * Mini Statement
     * @bodyParam merchant_key string required merchant key
     * @bodyParam merchant_secret string required merchant secret
     * @bodyParam service_name string required withdraw
     * @bodyParam device_name string required mantra / morpho
     * @bodyParam mobile_number string required Mobile number
     * @bodyParam aadhar_number string required Aadhar number
     * @bodyParam bank string required Bank name
     * @response scenario=success {
     *  "success": "Form is successfully submitted",
     *  "txn_id": "DMS20210812203700"
     * }
     * @response status=402 {"error": "Service name is not correct"}
     * @response status=404 {"message": "Oops! We are in lost space. Please be patient while we are working on it"}
     */
    public function miniStatementICICI(Request $request)
    {
        try
        {
            //, Balance Enquiry, Aadhar Pay, Mini Statement
            $json = array();
            $service_name = $request->service_name;
            $sevice_type = $request->sevice_type;
            $mobile_number = $request->mobile_number;
            $aadhar_number = $request->aadhar_number;
            $amount = $request->amount;
            $bank = $request->bank;

            $error = "";
            if($service_name != "withdraw")
            {
                $error = "Service name is not correct";
            }

            if($error == "")
            {
                return response()->json(['success'=> 'Form is successfully submitted!', 'txn_id' => 'DMS'.date('YmdHis')]);
            }
            else
            {
                return response()->json(['error'=> $error]);
            }
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();

            return response()->json(['error'=> $message]);
        }
    }
}
