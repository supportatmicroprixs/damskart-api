<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upi_qr_txns extends Model
{
  

   protected $fillable = [
     'user_id', 'created_by',	'dms_txn_id'	,'refId'	,'merchantTranId',	'message','mobile'	,'name',	'amount','txn_type','bankRRN','upi_id','originalBankRRN','subMerchantId','payerVA', 'txnInitDate' ,'txnCompletionDate' ,'status','refund_status','refund_message','payer_amount','payer_name','terminal_id','payer_mobile','commission','current_balance',
    ];
}
