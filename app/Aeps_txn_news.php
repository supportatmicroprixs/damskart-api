<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;

class Aeps_txn_news extends Model
{
   use RevisionableTrait;
   protected $revisionEnabled = true;
  

   protected $fillable = [
   		'terminal_id','str_ms_bal','dms_txnid','mobile','aadhar_number','ms_structure_model',	'request_transaction_time',	'transaction_amount',	'transaction_status','balance_amount'	,'bankRRN'	,'transaction_type'	,'fingpay_transactionId'	,'merchant_txn_id_ms'	,'message','user_id','created_by','commission','current_balance','fp_transaction_id','merchant_transaction_id','error_message',
    ];
}

