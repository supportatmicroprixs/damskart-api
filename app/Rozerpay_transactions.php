<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rozerpay_transactions extends Model
{
  

   protected $fillable = [
   		'user_id',	'created_by',	'rozerpay_id',	'dms_txn_id',	'amount',	'message',	'status',	'date'	,'current_balance',
    ];
}
