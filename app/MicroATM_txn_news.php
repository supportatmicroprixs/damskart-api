<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;

class MicroATM_txn_news extends Model
{
   protected $table = "microatm_txn_news";
   use RevisionableTrait;
   protected $revisionEnabled = true;
  

   protected $fillable = [
   	  'user_id', 'created_by', 'dms_txn_id', 'mobile', 'aadhar_number', 'terminal_id', 'request_transaction_time', 'request_transaction_time1', 'transaction_amount', 'transaction_status', 'balance_amount commission',
        'current_balance', 'bankRRN', 'transaction_type', 'merchant_txn_id_ms', 'merchant_transaction_id', 'fp_transaction_id',
         'str_ms_bal', 'ms_structure_model', 'message', 'error_message'
    ];
}

