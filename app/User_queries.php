<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_queries extends Model
{
  

   protected $fillable = [
    	'user_id','created_by',	'user_name',	'mobile',	'email'	,'query',	'status',
    ];
}
