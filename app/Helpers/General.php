<?php

namespace App\Helpers;

class General
{
	public static function sendSMS($request_url, $post_fields)
	{
		$ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $output = curl_exec($ch);

		return $output;
	}   
}
