<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bbps_Admin_commissions extends Model
{
  
   protected $table = "bbps_admin_commissions";  

   protected $fillable = [
   		'bbps_sr_id'	,'bbps_biller_id'	,'commission','commission_type',	'status','biller_cat_name','biller_name',
    ];
}
