<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bbps_commissions extends Model
{
  

   protected $fillable = [
   		'package_id',	'bbps_sr_id'	,'bbps_biller_id'	,'commission','commission_type',	'status','biller_cat_name','biller_name',
    ];
}
