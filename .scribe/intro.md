# Introduction

Damkart API description

This documentation aims to provide all the information you need to work with our API.

<aside>You'll see code examples for working with the API in different programming languages .</aside>

> Base URL

```yaml
https://manage.damskart.com
```