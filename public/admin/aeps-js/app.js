var historytable = null;
//sessionStorage.setItem("token", response.token);

function showStatus(text){
	$(".status").remove();
	$(".loadingoverlay").append("<span class='status'>"+text+"</span>");

}


function showStatusLoader(text){
	$(".loadingContent").html(text);

}


function connectDevice(mode) {
	$("#navbar-right a").removeClass("activeLink");
	$(".bi").addClass("activeLink");
	sessionStorage.setItem("txnFlag", 1);

	$('#Dv_Loader').show(); 

	showStatusLoader("Please wait. Connecting Device....");
	$.ajax({
		url : 'http://localhost:8680/matmweb/ConnectDevice',
		headers: {
			"token":sessionStorage.getItem("token"),
			"mode":mode
		},
		success : function(response) {
			//console.log(response);
			if(response.status){
				showStatus(response.message);
				
				showStatusLoader("Processing....");

				var type = $("input[name='method']:checked").val();

				if(type == 'balance_enquiry'){
					sessionStorage.setItem("txnFlag", 1);
				}else if(type == 'withdrawal'){
					sessionStorage.setItem("txnFlag", 2);
				}else if(type == 'mini_statement') {
					sessionStorage.setItem("txnFlag", 4);
				}

				var doTransaction = {};
				doTransaction["mobileNumber"]=$("#mobile").val();     //mandatory
				doTransaction["remarks"]=$("#remarks").val();				//mandatory
				doTransaction["txnType"]=sessionStorage.getItem("txnFlag");//mandatory
				if(sessionStorage.getItem("txnFlag") == 2){
					doTransaction["amount"]=$("#amount").val(); 
				}
				doTransaction["merchantTransactionId"]="fingpay"+ new Date().getUTCMilliseconds();//mandatory
				doTransaction["deviceImei"]="test";//mandatory
				doTransaction["latitude"]="10.0";//mandatory
				doTransaction["longitude"]="10.1";//mandatory
				var myJSON = JSON.stringify(doTransaction);
				myJSON = encrypt(myJSON); 
				//console.log(myJSON);  

				showStatusLoader("Device connected. Please enter your pin.");

				$.ajax({
					url : "http://localhost:8680/matmweb/DoTransaction",    
					headers: {
						"token":sessionStorage.getItem("token")
					},
					type:"post",   
					data:myJSON, 
					success : function(response) {
						//console.log(response);
						if(response.status){
							showStatus(response.message);
							if(response.data.errorCode == "00"){
								$("#statusTag").append('<span class="glyphicon glyphicon-ok successSymbol statusSub"></span>');
							}else{
								$("#statusTag").append('<span class="glyphicon glyphicon-remove statusSub" style="color: #f42a2a; font-size: 20px;"></span></span>');
							}

							$("#transResponse").val(JSON.stringify(response));

							
							showStatusLoader("Processing...");
							/*$("#txnTable").append("<tr><caption><span >Transaction status :</span><span class='glyphicon glyphicon-ok successSymbol'></span></caption><td>Message</td><td>"+response.data.errorMessage+"</td></tr>" +
									"<tr><td>Bank Name</td><td>"+response.data.bankName+"</td></tr>" +
									"<tr><td>Card Number</td><td>"+response.data.cardNumber+"</td></tr>" +
									"<tr><td>Transaction Amount</td><td>"+response.data.transactionAmount+"</td></tr>" +
									"<tr><td>Balance Amount</td><td>"+response.data.balanceAmount+"</td></tr>" +
									"<tr><td>Bank RRN</td><td>"+response.data.bankRRN+"</td></tr>" +
									"<tr><td>TerminalId</td><td>"+response.data.terminalId+"</td></tr>");

							if(response.data.miniStatementStructureModel != null){
								$("#miniStatementList").show();
								$("#miniStatementList tbody tr").remove();
								for(var i = 0; i < response.data.miniStatementStructureModel.length; i++) {
									$("#miniStatementList tbody").append("<tr><td>"+response.data.miniStatementStructureModel[i].date+"</td><td>"+response.data.miniStatementStructureModel[i].txnType+"</td><td>"+response.data.miniStatementStructureModel[i].amount+"</td><td>"+response.data.miniStatementStructureModel[i].narration+"</td></tr>");
								}
							}else{
								$("#miniStatementList").hide();
							}*/

							setTimeout(function(){
								$('#Dv_Loader').hide(); 
								sendEnquiryMicro(type);
							}, 1000);
						}else{
							$('#Dv_Loader').hide(); 
							showStatus("<div class='alert alert-danger'>"+response.message+'</div>');
							setTimeout(function(){
								//$.LoadingOverlay("hide");  
							}, 1000);
						}

					} 
				});
			}else{
				$('#Dv_Loader').hide(); 
				//showStatus("<div class='alert alert-danger'>".response.message+"</div>");
				setTimeout(function(){
					connectDevice(1);
				}, 1000);
			}

		} 
	});
}


$(document).on('click', '#getHistoryBtn', function(event) {
	//$.LoadingOverlay("show");
	showStatus("Processing....");
	var lTable = document.getElementById("table");
	lTable.style.display =  "table";
	var historyReqObj = {}; 
	historyReqObj["fromDate"]=$("#fromDate").val();
	historyReqObj["toDate"]=$("#toDate").val();
	var myJSON = JSON.stringify(historyReqObj);
	//console.log(myJSON);  
	$.ajax({
		url : "History", 
		headers: {
			"token":sessionStorage.getItem("token"),
			"superMerchantId":2
		},
		type:"post",   
		data:myJSON, 
		success : function(response) {
			//console.log(response.data);
			if(response.status){
				if(historytable == null){
					historytable = $('#table').DataTable({
						"data": response.data,

						retrieve:true,
						"columns": [
							{ data:'transactionType',
								render: function (data,type) {
									if(data===1){
										return "CW";

									}
									else if(data===2){
										return "CD";
									}
								} },
								{ "data": "transactionTimestamp" },
								{"data":"transactionStatus"},
								{"data":"amount"}

								]
					});
				}
				else{

					historytable.clear().draw();
					historytable.rows.add(response.data);
					historytable.columns.adjust().draw();

				}
				//$.LoadingOverlay("hide");
			} 
			else{
				$.LoadingOverlay("hide");
				alert(response.message);
			}

		}
	});
});

$(document).on('click', '#HistoryBtn', function(event) {
	$(".inputDiv").hide();
	$(".transactionDiv").hide();
	$(".historyDiv").show();
}); 

function encrypt(data) {
	var keyHex = CryptoJS.enc.Utf8.parse("284908D75CAB6D9C9DE7281CBA76EF9D");
	var encrypted = CryptoJS.AES.encrypt(data, keyHex, {
		mode: CryptoJS.mode.ECB,
		padding: CryptoJS.pad.Pkcs7 
	});
	return encrypted.toString(); 
}
function toHex(str){
	var result = '';
	for (var i=0; i<str.length; i++) {
		result += str.charCodeAt(i).toString(16);
	}
	return result;
}

function ClearCtrlClose()
{
    var type = $("input[name='method']:checked").val();
    if(type == "balance_enquiry")
    {
        $('#myModal').modal('hide');
        $("#myModal").removeClass("in");
        $(".modal-backdrop").remove();
        $("#myModal").hide();
    }
    else
    {
        location.reload();
    }
}
